<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**

 * CodeIgniter

 *

 * An open source application development framework for PHP 5.1.6 or newer

 *

 * @package		CodeIgniter

 * @author		ExpressionEngine Dev Team

 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc.

 * @license		http://codeigniter.com/user_guide/license.html

 * @link		http://codeigniter.com

 * @since		Version 1.0

 * @filesource

 */
// ------------------------------------------------------------------------



/**

 * CodeIgniter Language Helpers

 *

 * @package		CodeIgniter

 * @subpackage	Helpers

 * @category	Helpers

 * @author		ExpressionEngine Dev Team

 * @link		http://codeigniter.com/user_guide/helpers/language_helper.html

 */
// ------------------------------------------------------------------------

/**

 * Lang

 *

 * Fetches a language variable and optionally outputs a form label

 *

 * @access	public

 * @param	string	the language line

 * @param	string	the id of the form element

 * @return	string

 */
function number_to_words($numberValue) {



    $textResult = ''; // so i can use .=

    $numberValue = "$numberValue";



    if ($numberValue[0] == '-') {

        $textResult .= 'سالب ';

        $numberValue = substr($numberValue, 1);
    }



    $numberValue = (int) $numberValue;

    $def = array("0" => 'صفر',
        "1" => 'واحد',
        "2" => 'اثنان',
        "3" => 'ثلاث',
        "4" => 'اربع',
        "5" => 'خمس',
        "6" => 'ست',
        "7" => 'سبع',
        "8" => 'ثمان',
        "9" => 'تسع',
        "10" => 'عشر',
        "11" => 'أحد عشر',
        "12" => 'اثنا عشر',
        "100" => 'مائة',
        "200" => 'مئتان',
        "1000" => 'ألف',
        "2000" => 'ألفين',
        "1000000" => 'مليون',
        "2000000" => 'مليونان');



    // check for defind values    

    if (isset($def[$numberValue])) {

        // checking for numbers from 2 to 10 :reson = 2 to 10 uses 'ة' at the end 

        if ($numberValue < 11 && $numberValue > 2) {

            //if($numberValue != 8) $textResult .= $def[$numberValue].'ة';



            if ($numberValue == 8)
                $textResult .= $def[$numberValue] . 'ية';
            else
                $textResult .= $def[$numberValue] . 'ة';
        }

        else {

            // the rest of the defined numbers

            $textResult .= $def[$numberValue];
        }
    } else {

        $tensCheck = $numberValue % 10;

        $numberValue = "$numberValue";



        for ($x = strlen($numberValue); $x > 0; $x--) {

            $places[$x] = $numberValue[strlen($numberValue) - $x];
        }



        switch (count($places)) {

            case 2: // 2 numbers

            case 1: // or 1 number
                {

                    //$textResult .= ($places[1] != 0) ? $def[$places[1]].(($places[1] > 2 || $places[2] == 1) ? 'ة' : '').(($places[2] != 1) ? ' و' : ' ') : '';



                    $textResult .= ($places[1] != 0) ? $def[$places[1]] . (($places[1] > 2 || $places[2] == 1) ? '' : '') . (($places[2] != 1) ? ' و' : ' ') : ''; //?ة

                    $textResult .= (($places[2] > 2) ? $def[$places[2]] . 'ون' : $def[10] . (($places[2] != 2) ? '' : 'ون'));
                }

                break;

            case 3: // 3 numbers
                {

                    $lastTwo = (int) $places[2] . $places[1];

                    $textResult .= ($places[3] > 2) ? $def[$places[3]] . ' ' . $def[100] : $def[(int) $places[3] . "00"];

                    if ($lastTwo != 0) {

                        $textResult .= ' و' . number_to_words($lastTwo);
                    }
                }

                break;

            case 4: // 4 numbrs
                {

                    $lastThree = (int) $places[3] . $places[2] . $places[1];

                    $textResult .= ($places[4] > 2) ? $def[$places[4]] . 'ة الاف' : $def[(int) $places[4] . "000"];

                    if ($lastThree != 0) {

                        $textResult .= ' و' . number_to_words($lastThree);
                    }
                }

                break;

            case 5: // 5 numbers
                {

                    $lastThree = (int) $places[3] . $places[2] . $places[1];

                    $textResult .= number_to_words((int) $places[5] . $places[4]) . ((((int) $places[5] . $places[4]) != 10) ? ' الفاً' : ' الاف');

                    if ($lastThree != 0) {

                        $textResult .= ' و' . number_to_words($lastThree);
                    }
                }

                break;

            case 6: // 6 numbers
                {

                    $lastThree = (int) $places[3] . $places[2] . $places[1];

                    $textResult .= number_to_words((int) $places[6] . $places[5] . $places[4]) . ((((int) $places[5] . $places[4]) != 10) ? ' الفاً' : ' الاف');

                    if ($lastThree != 0) {

                        $textResult .= ' و' . number_to_words($lastThree);
                    }
                }

                break;

            case 7: // 7 numbers 1 mill
                {

                    $textResult .= ($places[7] > 2) ? $def[$places[7]] . ' ملايين' : $def[(int) $places[7] . "000000"];

                    $textResult .= ' و';

                    $textResult .= number_to_words((int) $places[6] . $places[5] . $places[4] . $places[3] . $places[2] . $places[1]);
                }

                break;

            case 8: // 8 numbers 10 mill

            case 9: // 9 numbers 100 mill
                {

                    $places[9] = (isset($places[9])) ? $places[9] : '';

                    $firstThree = (int) $places[9] . $places[8] . $places[7];

                    $textResult .= number_to_words($firstThree);

                    $textResult .= ($firstThree < 11) ? ' ملايين ' : ' مليونا ';

                    if (((int) $places[6] . $places[5] . $places[4] . $places[3] . $places[2] . $places[1]) != 0) {

                        $textResult .= ' و';

                        $textResult .= number_to_words((int) $places[6] . $places[5] . $places[4] . $places[3] . $places[2] . $places[1]);
                    }
                }

                break;

            default: {

                    $textResult = 'هذا رقم كبير .. ';
                }
        }
    }

    return $textResult;
}

/**
 * Word Limiter
 *
 * Limits a string to X number of words.
 *
 * @access	public
 * @param	string
 * @param	integer
 * @param	string	the end character. Usually an ellipsis
 * @return	string
 */
 if (!function_exists('checkStoreItemQuantity')) {

    function checkStoreItemQuantity($storeid,$itemid)
    {
        $CI = &get_instance();
        $sql = "SELECT * FROM `avg_store_value` AS asv
WHERE asv.`store_id` = '$storeid' AND asv.`item_id` = '$itemid'";

        $VQuery = $CI->db->query($sql);
        if($VQuery->num_rows() > 0) {
            return $VQuery->row();
        }
        else{
            return false;
        }
    }
}
if (!function_exists('word_limiter')) {

    function word_limiter($str, $limit = 100, $end_char = '&#8230;') {
        if (trim($str) == '') {
            return $str;
        }

        preg_match('/^\s*+(?:\S++\s*+){1,' . (int) $limit . '}/', $str, $matches);

        if (strlen($str) == strlen($matches[0])) {
            $end_char = '';
        }

        return rtrim($matches[0]) . $end_char;
    }

}

function getAmountFormat($number) {
    return number_format($number, 3, '.', '');
}

function amount_words_logic_ar($am) {
    $_val = explode('.', $am);

    $_total_char = number_to_words(intval($_val[0])) . ' ريال عُماني'; // OMR
    //$_total_char1 = reconvertText(substr(convertText($_total_char),0,$wl));// substr($_total_char,0,42);
    //if(reconvertText(substr(convertText($_total_char),$wl,140)))  $_total_char1 .= '';//ـ
    //$_total_char2 = reconvertText(substr(convertText($_total_char),$wl,140));// substr($_total_char,42,140)



    $_total_char1 = word_limiter($_total_char, 8, '');

    $wl = strlen(trim($_total_char1));

    $_total_char2 = substr($_total_char, $wl, 150);



    //$wl = 39;

    if (strlen($_val[1]) == 1)
        $_val[1] = $_val[1] * 100;

    elseif (strlen($_val[1]) == 2)
        $_val[1] = $_val[1] * 10;



    elseif (strlen($_total_char1) < $wl)
        $_total_char1 .= ''; // فقط

    elseif (strlen($_total_char1) >= $wl)
        $_total_char2 .= ''; // فقط



    $_total_char1 = 'فقط ' . $_total_char1;

    if ($_val[1])
        $_change = ' و ' . number_to_words(intval($_val[1])) . ' بيسة فقط لاغير'; // BZ فقط

    else {

        if (strlen($_total_char1) < $wl)
            $_total_char1 .=''; //فقط

        elseif (strlen($_total_char1) >= $wl)
            $_total_char2 .=''; //فقط
    }



    if (strlen($_total_char) < 10) {
        $_total_char1 = $_total_char1 . $_change;
        $_change = '';
    }



    if ($_val[1]) {
        $_val[1] = $_val[1] . '.';
        $_valEnd = '';
    } else
        $_valEnd = ' ***';

    strtoupper(str_replace(',', '', $_total_char1));
    echo strtoupper(str_replace(',', '', $_total_char1)) . ' ' . strtoupper(str_replace(',', '', $_total_char2 . $_change));
}

function amount_words_logic_ar_old($paymentamount) {
    $v_ajax_ = "";

    $wl = 40;
    $_val = explode('.', $paymentamount);
    $_total_char = number_to_words(intval($_val[0])) . ' ريال عُماني'; // OMR
    $_total_char1 = word_limiter($_total_char, 8, '');

    $wl = strlen(trim($_total_char1));

    $_total_char2 = substr($_total_char, $wl, 150);



    if (strlen($_val[1]) == 1)
        $_val[1] = $_val[1] * 100;

    elseif (strlen($_val[1]) == 2)
        $_val[1] = $_val[1] * 10;



    elseif (strlen($_total_char1) < $wl)
        $_total_char1 .= ''; // فقط

    elseif (strlen($_total_char1) >= $wl)
        $_total_char2 .= ''; // فقط

    $_total_char1 = 'فقط ' . $_total_char1;

    if ($_val[1])
        $_change = ' و ' . number_to_words(intval($_val[1])) . ' بيسة فقط لاغير'; // BZ فقط

    else {

        if (strlen($_total_char1) < $wl)
            $_total_char1 .=''; //فقط

        elseif (strlen($_total_char1) >= $wl)
            $_total_char2 .=''; //فقط
    }



    if (strlen($_total_char) < 10) {
        $_total_char1 = $_total_char1 . $_change;
        $_change = '';
    }



    if ($_val[1]) {
        $_val[1] = $_val[1] . '.';
        $_valEnd = '';
    } else
        $_valEnd = '***';
    echo strtoupper(str_replace(',', '', $_total_char1));
    // strtoupper(str_replace(',', '', $_total_char2.$_change));
}

function getFromTo($date) {
    $d = $date;

    if (date('H') >= '00' && date('H') <= '03') {

        $d = date('Y-m-d ', (strtotime('-1 day', strtotime($d))));
    }
    $tommorow = date('Y-m-d ', (strtotime('+1 day', strtotime($d))));
    $from = $d . ' 12:01:00';
   
    $to = $tommorow . ' 02:30:00';

    //echo date('Y-m-d h:i:s',strtotime($dateString));
    $ar['from'] = $from;
    $ar['to'] = $to;
    return json_encode($ar);
}

//////////////////////////////////////////////////////////////////////

if (!function_exists('global_permission_select_dropbox')) {



    function global_permission_select_dropbox($name, $value) {

        $CI = & get_instance();

        //$info = userinfo_ppermission();

        $query = "SELECT permission_type_id,permissionhead FROM bs_permission_type  ";

        /* if($info['member_type']=='5')

          {	$query .= ' permission_type_id=1 AND ';	} */

        $query .= '  ORDER BY permission_type_id ASC;';

        //echo $query;

        $VQuery = $CI->db->query($query);

        $dropdown = '<select class="req" name="' . $name . '" id="' . $name . '" onchange="select_add_permission(this.value)">';

        $dropdown .= '<option value="">Select Permissions</option>';

        foreach ($VQuery->result() as $row) {

            $dropdown .= '<option value="' . $row->permission_type_id . '" ';

            if ($value == $row->permission_type_id) {

                $dropdown .= 'selected="selected"';
            }

            $dropdown .= '>' . $row->permissionhead . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('get_modules_add_id')) {


    function get_modules_add_id($typeid) {

        $CI = &get_instance();


        $CI->db->select('add_mod_id');

        $CI->db->from('additional_permissions');

        if ($typeid != "") {
            //$typeid
            $CI->db->where('user_type_id', $typeid);
        } else {
            $CI->db->where('user_type_id', $CI->session->userdata('bs_ownerid'));
        }


        //   $CI->db->where('permission_type_id', $member_type);


        $perm = $CI->db->get();

        $permx = $perm->result_array();


        foreach ($permx as $moduleskey => $modulesdata) {

            $modulesid[] = $modulesdata['add_mod_id'];
        }

        $perm->free_result();

        return $modulesid;
    }

}

function multidimensional_search($parents, $searched) {
    //print_r($parents);
    //print_r($searched);

    $return = array();
    foreach ($parents as $key => $value) {
        $exists = true;
        foreach ($searched as $skey => $svalue) {
            $exists = ($exists && IsSet($parents[$key][$skey]) && $parents[$key][$skey] == $svalue);
        }
        if ($exists) {
            $return[] = $key;
            return $return;
        }
    }

    return 't';
}

//Code written by purpledesign.in Jan 2014
function dateDiff($date) {
    $mydate = date("Y-m-d H:i:s");
    $theDiff = "";
    //echo $mydate;//2014-06-06 21:35:55
    $datetime1 = date_create($date);
    $datetime2 = date_create($mydate);
    $interval = date_diff($datetime1, $datetime2);
    //echo $interval->format('%s Seconds %i Minutes %h Hours %d days %m Months %y Year    Ago')."<br>";
    $min = $interval->format('%i');
    $sec = $interval->format('%s');
    $hour = $interval->format('%h');
    $mon = $interval->format('%m');
    $day = $interval->format('%d');
    $year = $interval->format('%y');
    $ret = array();
    if ($interval->format('%i%h%d%m%y') == "00000") {
        //echo $interval->format('%i%h%d%m%y')."<br>";
        $ret['seconds'] = $sec;
    } else if ($interval->format('%h%d%m%y') == "0000") {
        $ret['min'] = $min;
    } else if ($interval->format('%d%m%y') == "000") {
        $ret['hour'] = $hour;
    } else if ($interval->format('%m%y') == "00") {
        $ret['day'] = $day;
    } else if ($interval->format('%y') == "0") {
        $ret['month'] = $mon;
    } else {
        $ret['year'] = $year;
    }
    return $ret;
}

function getSingelDate($type, $date) {

    if (date('H') >= '00' && date('H') <= '03') {

        //echo   $date =  date('Y-m-d ',(strtotime ( '-1 day' , strtotime ($date) ) ));
        //echo        $d =  date('Y-m-d ',(strtotime ( '-1 day' , strtotime ($date) ) ));
    }
    if ($type == 'from') {
        //$date =  date('Y-m-d ',(strtotime ( '-1 day' , strtotime ($date) ) ));
        $response = $date . ' 12:01:00';
    } else {
        $tommorow = date('Y-m-d ', (strtotime('+1 day', strtotime($date))));
        $response = $tommorow . ' 02:30:00';
    }
    return $response;
}

function amount_word_logic($paymentamount) {
    $total_ch = '';
    $lw = 31;
    $am = $paymentamount;
    $val = explode('.', $am);
    $total_ch.=number_to_words_en($val['0']) . ' Ro';
    $total_ch1 = word_limiter($total_ch, 4, '');
    $wl = strlen(trim($total_ch1));
    $total_ch2 = substr($total_ch, $wl, 150);

    if (strlen($val[1]) == 1)
        $val[1] = $val[1] * 100;
    elseif (strlen($val[1]) == 2)
        $val[1] = $val[1] * 10;

    elseif (strlen($total_ch1) < $lw)
        $total_ch1 .= ''; // Only
    elseif (strlen($total_ch1) >= $lw)
        $total_ch2 .= ''; // Only

    $total_ch1 = 'Only ' . $total_ch1;

    if ($val[1])
        $_change = ' and ' . number_to_words_en(intval($val[1])) . ' BZ'; // BZ Only
    else {
        if (strlen($total_ch1) < $lw)
            $total_ch1 .=''; // Only
        elseif (strlen($total_ch1) >= $lw)
            $total_ch2 .=''; // Only
    }

    if (strlen($total_ch) < 20) { // في حالة إن الرقم الخاص بالريالات صغير وطوله لا يتجاوز رقم 10
        if (strlen($_change) > 17) { // في حالة زيادة طول الفكة عن حجم مكان البوكس المكتوب فيه
            $_change_up = word_limiter($_change, 3, '');
            $_total_ch1 = $total_ch1 . $_change_up;
            $chl = strlen(trim($_change_up));
            $_change = substr(trim($_change), $chl, 150);
        } else {
            $_total_ch1 = $total_ch1 . $_change;
            $_change = '';
        }  // End status text
    } // End Small Number

    if ($val[1]) {
        $val[1] = '.' . $val[1];
        $_valEnd = '';
    } else
        $_valEnd = ' ***';

//    $_date_act  = explode('-',$_cheq['chq_dateact']);
    //  $_date_act1 = $_date_act[0].' '.$date_arry[$_date_act[1]].' '.$_date_act[2];
    //$check_title=strlen($_total_char1)>34?strtoupper(substr($_total_char1,0,43))."<br/>".strtoupper(substr($_total_char1,35,1000000)):strtoupper($_total_char1);
    $check_title = strtoupper($_total_ch1);
    echo str_replace(',', '', $check_title) . ' ' . strtoupper(str_replace(',', '', $total_ch2 . $_change));
}

function getAedDhuram() {
    $url = "https://www.google.com/finance/converter?from=omr&to=aed&a=1&_=1440339894785";
    return $fileData = file_get_contents($url);
    //	echo "<pre>";
    //		print_r($fileData);
    //	$dom = new DOMDocument;
    //	$dom->loadHTML($fileData);
    //$doc = new DOMDocument();
    //$doc->loadHTML($fileData);
    //echo "<pre>";
    //return 	
}

function number_to_words_en($num) {
    list($num, $dec) = explode(".", $num);

    $output = "";

    if ($num{0} == "-") {
        $output = "negative ";
        $num = ltrim($num, "-");
    } else if ($num{0} == "+") {
        $output = "positive ";
        $num = ltrim($num, "+");
    }

    if ($num{0} == "0") {
        $output .= "zero";
    } else {
        $num = str_pad($num, 36, "0", STR_PAD_LEFT);
        $group = rtrim(chunk_split($num, 3, " "), " ");
        $groups = explode(" ", $group);

        $groups2 = array();
        foreach ($groups as $g)
            $groups2[] = convertThreeDigit($g{0}, $g{1}, $g{2});

        for ($z = 0; $z < count($groups2); $z++) {
            if ($groups2[$z] != "") {
                $output .= $groups2[$z] . convertGroup(11 - $z) . ($z < 11 && !array_search('', array_slice($groups2, $z + 1, -1)) && $groups2[11] != '' && $groups[11]{0} == '0' ? " and " : ", ");
            }
        }

        $output = rtrim($output, ", ");
    }

    if ($dec > 0) {
        $output .= " point";
        for ($i = 0; $i < strlen($dec); $i++)
            $output .= " " . convertDigit($dec{$i});
    }

    return $output;
}

function convertGroup($index) {

    switch ($index) {

        case 11: return " decillion";

        case 10: return " nonillion";

        case 9: return " octillion";

        case 8: return " septillion";

        case 7: return " sextillion";

        case 6: return " quintrillion";

        case 5: return " quadrillion";

        case 4: return " trillion";

        case 3: return " billion";

        case 2: return " million";

        case 1: return " thousand";

        case 0: return "";
    }
}

function convertThreeDigit($dig1, $dig2, $dig3) {

    $output = "";



    if ($dig1 == "0" && $dig2 == "0" && $dig3 == "0")
        return "";



    if ($dig1 != "0") {

        $output .= convertDigit($dig1) . " hundred";

        if ($dig2 != "0" || $dig3 != "0")
            $output .= " and ";
    }



    if ($dig2 != "0")
        $output .= convertTwoDigit($dig2, $dig3);

    else if ($dig3 != "0")
        $output .= convertDigit($dig3);



    return $output;
}

function convertTwoDigit($dig1, $dig2) {

    if ($dig2 == "0") {

        switch ($dig1) {

            case "1": return "ten";

            case "2": return "twenty";

            case "3": return "thirty";

            case "4": return "forty";

            case "5": return "fifty";

            case "6": return "sixty";

            case "7": return "seventy";

            case "8": return "eighty";

            case "9": return "ninety";
        }
    } else if ($dig1 == "1") {

        switch ($dig2) {

            case "1": return "eleven";

            case "2": return "twelve";

            case "3": return "thirteen";

            case "4": return "fourteen";

            case "5": return "fifteen";

            case "6": return "sixteen";

            case "7": return "seventeen";

            case "8": return "eighteen";

            case "9": return "nineteen";
        }
    } else {

        $temp = convertDigit($dig2);

        switch ($dig1) {

            case "2": return "twenty-$temp";

            case "3": return "thirty-$temp";

            case "4": return "forty-$temp";

            case "5": return "fifty-$temp";

            case "6": return "sixty-$temp";

            case "7": return "seventy-$temp";

            case "8": return "eighty-$temp";

            case "9": return "ninety-$temp";
        }
    }
}

function convertDigit($digit) {

    switch ($digit) {

        case "0": return "zero";

        case "1": return "one";

        case "2": return "two";

        case "3": return "three";

        case "4": return "four";

        case "5": return "five";

        case "6": return "six";

        case "7": return "seven";

        case "8": return "eight";

        case "9": return "nine";
    }
}

if (!function_exists('left_menu2')) {



    function left_menu2() {

        if (get_set_value('site_lang') != 'english') {

            $menu = '<li class=" expanded"><a href="javascript:void" class="">

                       <span class="nav-icon icon-store"></span>المخزن </a>

                       <ul class="sub-nav2" id="63" style="display: none;">



                            <li class="leaf"><a href="' . base_url() . 'inventory2">المخزن</a></li>

                            <li class="leaf"><a href="' . base_url() . 'inventory2/pending_purchase_list">قائمة طلبات  الشراء</a></li>

                            <li class="leaf"><a href="' . base_url() . 'inventory2/pending_sales_list">قائمة الطلبات المباعة</a></li>

                            <li class="leaf"><a href="' . base_url() . 'inventory2/purchase_receipt">المنتجات المستلمة</a></li>

                            <li class="leaf"><a href="' . base_url() . 'inventory2/sales_recept">المنتجات المباعة</a></li>



                        </ul>

                    </li>';
        } else {

            $menu = '<li class=" expanded"><a href="javascript:void" class="">

                       <span class="nav-icon icon-store"></span>Store </a>

                       <ul class="sub-nav2" id="63" style="display: none;">



                            <li class="leaf"><a href="' . base_url() . 'inventory2">Store</a></li>

                            <li class="leaf"><a href="' . base_url() . 'inventory2/pending_purchase_list">List of purchase orders</a></li>

                            <li class="leaf"><a href="' . base_url() . 'inventory2/pending_sales_list">List orders sold</a></li>

                            <li class="leaf"><a href="' . base_url() . 'inventory2/purchase_receipt">Products received</a></li>

                            <li class="leaf"><a href="' . base_url() . 'inventory2/sales_recept">Products sold</a></li>



                        </ul>

                    </li>';
        }

        return $menu;
    }

}



if (!function_exists('getBranhes')) {





    function getBranhes($company_id, $id) {





        $CI = & get_instance();

        $memtype = get_member_type();

        $CI->db->select('branchid,branchname');

        $CI->db->from('bs_company_branch');

        $CI->db->where('companyid', $company_id);

        $CI->db->order_by("branchid", "ASC");

        $query = $CI->db->get();

        $result = $query->result();

        //echo "<pre>";
        //print_r($result);

        $dropdown = '<select class="pod has-search" name="branch_id" id="branch_id">';

        if ($query->num_rows() > 1) {

            $dropdown .= '<option value="" >' . lang('All') . '</option>';
        }

        foreach ($result as $row) {

            //echo "<pre>";
            //echo $row->branchname;
            //echo $j = unserialize($row->branchname);
            //echo $j = json_decode($row->branchname);
            //print_r($j);
            //print_r($row->branchname);

            $dropdown .= '<option value="' . $row->branchid . '" ';

            if ($id == $row->branchid) {

                $dropdown .= 'selected="selected"';
            }

            //$com = json_decode($row->branchname);

            $com = unserialize($row->branchname);

            //echo "<pre>";
            //print_r($com);

            $dropdown .= '>' . $com['english'] . ' | ' . $com['arabic'] . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

if (!function_exists('addvouchers')) {

    function addvouchers($voucher_type, $vouher_data) {
        $vData['voucher_data'] = serialize($vouher_data);
        $vData['voucher_type'] = $voucher_type;
        $CI = & get_instance();
        $CI->db->insert('vouchers', $vData);
        return $CI->db->insert_id();
    }

}


if (!function_exists('getCashBranhes')) {





    function getCashBranhes($cashstatus, $company_id) {





        $CI = & get_instance();

        $memtype = get_member_type();

        $CI->db->select('id,branch_cash_name');

        $CI->db->from('an_branch_cash');

        // $CI->db->where('company_id',$company_id);

        $CI->db->order_by("id", "ASC");

        $query = $CI->db->get();

        $result = $query->result();

        //echo "<pre>";
        //print_r($result);

        if ($cashstatus == 'from')
            $dropdown = '<select class="pod has-search" name="cash_branch_id" id="cash_branch_id">';
        else
            $dropdown = '<select class="pod has-search" name="cash_to_branch_id" id="cash_to_branch_id">';



        if ($query->num_rows() > 1) {

            $dropdown .= '<option value="   " >' . lang('Choose') . '</option>';
        }

        foreach ($result as $row) {

            //echo "<pre>";
            //echo $row->branchname;
            //echo $j = unserialize($row->branchname);
            //echo $j = json_decode($row->branchname);
            //print_r($j);
            //print_r($row->branchname);

            $dropdown .= '<option value="' . $row->id . '" ';

            /* if ($value == $row->id) {

              $dropdown .= 'selected="selected"';

              } */



            //echo "<pre>";
            //print_r($com);

            $dropdown .= '>' . $row->branch_cash_name . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

if (!function_exists('left_menu')) {



    function left_menu() {

        $CI = & get_instance();

        $memtype = get_member_type();



        //var_dump($memtype);
        //die();



        $modulesid = get_modules_id($memtype);



        //var_dump($modulesid);
        //die();








        $CI->db->select('module_name,module_icon,module_controller,moduleid,module_name_ar');

        $CI->db->from('mh_modules');





        //var_dump($modulesid);
        //die();
        //if ($memtype != 5) {

        if ($modulesid != "") {

            $where = "moduleid IN (" . implode(",", $modulesid) . ",1)";
        } else {

            $where = "moduleid IN (" . implode(",", $modulesid) . "1)";
        }

        $CI->db->where($where);

        //}





        $CI->db->where('module_status', 'A');

        $CI->db->where('module_parent', 0);

        $CI->db->order_by("module_order", "ASC");

        $query = $CI->db->get();



        if ($query->num_rows() > 0) {

            foreach ($query->result() as $parentMenu) {

                if ($CI->uri->segment(1) == $parentMenu->module_controller) {

                    $class = 'active';

                    /* $menu .= '<div class="pointer">

                      <div class="arrow"></div>

                      <div class="arrow_border"></div>

                      </div>';

                     */
                }

                //var_dump($modulesid);

                $childMenu = child_menu($parentMenu->moduleid, $modulesid);

                if ($parentMenu->module_controller == 'dashboard') {

                    $parentUrl = base_url();
                } else {

                    $parentUrl = 'javascript:void';
                }



                $menu .= '<li class="' . $class . ' expanded" >';

                if ($class == 'active') {

                    //$menu .='<script>$(function () {$("#' . $parentMenu->moduleid . '").css("display","block"); });

                    /* $menu .='<div class="pointer">

                      <div class="arrow"></div>

                      <div class="arrow_border"></div>

                      </div>'; */
                }



                if ($childMenu['count'] > 0) {

                    $parentClass = 'dropdown-toggle';
                } else {

                    $parentClass = '';
                }

                //' . $parentUrl . '
                //$menu .= '<a href="javascript:void" class="' . $parentClass . '" >';

                if ($parentMenu->module_icon == "")
                    $ic = '';
                else
                    $ic = $parentMenu->module_icon;

                //$menu .= '<i class="' . $ic . '"></i>';

                if (get_set_value('site_lang') == 'english') {

                    $menu .= '<span class="' . $parentMenu->module_icon . '"></span>' . $parentMenu->module_name . '';
                } else {

                    $menu .= '<span class="' . $parentMenu->module_icon . '"></span>' . $parentMenu->module_name_ar . '';
                }

                if ($childMenu['count'] > 0) {



                    //$menu .='<i class="icon-chevron-down"></i>';
                }

                $menu .= '</a>';

                $menu .= $childMenu['menu'];

                $menu .= '</li>';

                unset($class, $parentUrl);
            }

            echo $menu;
        }
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('child_menu')) {



    function child_menu($parentid, $modulesid) {





        $CI = & get_instance();

//        $CI->db->select('module_name,module_controller,module_name_ar');
//        $CI->db->from('mh_modules');
//        $CI->db->where('module_status', 'A');
//        $CI->db->where('module_parent', $parentid);
//        $CI->db->order_by("module_order", "ASC");
//        $query = $CI->db->get();
        //echo $parentid."<br/>";

        if ($modulesid > 0) {

            $menu = '<ul class="sub-nav2" id="' . $parentid . '" style="display: none;">';

            //foreach ($query->result() as $childMenu) {

            foreach ($modulesid as $module) {

                //echo $module."<br/>";
                //die();

                $CI->db->select('module_name,module_controller,module_name_ar');

                $CI->db->from('mh_modules');

                $CI->db->where('module_status', 'A');

                $CI->db->where('module_parent', $parentid);

                $CI->db->where('moduleid', $module);

                $CI->db->order_by("module_order", "ASC");

                $query = $CI->db->get();

                $childMenu = $query->row();

                if ($childMenu and $module != $parentid) {

                    //echo $childMenu->module_name_ar."<br/>";  
//                    $cchild = $CI->uri->segment(1) . '/' . $CI->uri->segment(2);
//                    if ($cchild == $childMenu->module_controller) {
//                        $chd = 'childactive';
//                    }

                    if (get_set_value('site_lang') == 'english') {

                        $menu .= '<li class="leaf"><a href="' . base_url() . $childMenu->module_controller . '">' . $childMenu->module_name . '</a></li>';
                    } else {

                        $menu .= '<li class="leaf"><a href="' . base_url() . $childMenu->module_controller . '">' . $childMenu->module_name_ar . '</a></li>';
                    }
                }

                unset($chd);
            }







            $menu .= '</ul>';

            return array('menu' => $menu, 'count' => $query->num_rows());
        }
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('global_permission_dropbox')) {



    function global_permission_dropbox($name, $value) {

        $CI = & get_instance();

        //$info = userinfo_ppermission();

        $query = "SELECT permission_type_id,permissionhead FROM bs_permission_type  ";

        /* if($info['member_type']=='5')

          {	$query .= ' permission_type_id=1 AND ';	} */

        $query .= '  ORDER BY permission_type_id ASC;';

        //echo $query;

        $VQuery = $CI->db->query($query);

        $dropdown = '<select class="req" name="' . $name . '" id="' . $name . '" onchange="select_permission(this.value)">';

        $dropdown .= '<option value="">Select Permissions</option>';

        foreach ($VQuery->result() as $row) {

            $dropdown .= '<option value="' . $row->permission_type_id . '" ';

            if ($value == $row->permission_type_id) {

                $dropdown .= 'selected="selected"';
            }

            $dropdown .= '>' . $row->permissionhead . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('category_dropbox')) {



    function category_dropbox($name, $value, $companyid, $cattype) {

        $CI = & get_instance();

        $CI->db->select('catid,catname,cattype');

        $CI->db->from('bs_category');

        //$CI->db->where('companyid', $companyid);
        //$CI->db->where('cattype', $cattype);

        $CI->db->where('catstatus', 'A');

        $CI->db->order_by("catname", "ASC");

        $query = $CI->db->get();



        $dropdown = '<select autocomplete="off" class="pod has-search" name="' . $name . '" id="' . $name . '" style="">';

        $dropdown .= '<option value="">Select Category</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->catid . '" ';

            if ($value == $row->catid) {

                $dropdown .= 'selected="selected"';
            }

            $dropdown .= '>' . _s($row->catname, get_set_value('site_lang')) . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}



//////////////////////////////////////////////////////////////////////

if (!function_exists('unit_dropbox')) {



    function unit_dropbox($name, $value) {

        $CI = & get_instance();

        //$CI->db->select('catid,catname,cattype');

        $CI->db->from('units');

        //$CI->db->where('companyid', $companyid);
        //$CI->db->where('cattype', $cattype);

        $CI->db->where('unit_status', 'A');

        $CI->db->order_by("unit_id", "ASC");

        $query = $CI->db->get();



        //$data=array('KG','Cartons','Box');

        $dropdown = '<select class="pod has-search" name="' . $name . '" id="' . $name . '" style="">';

        $dropdown .= '<option value="">Select</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->unit_id . '" ';

            if ($value == $row->unit_id) {

                $dropdown .= 'selected="selected"';
            }

//            $dropdown .= '>' . _s($row->unit_title, get_set_value('site_lang')) . '</option>';
            $dropdown .= '>' .  _s($row->unit_title, get_set_value('site_lang')) . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}





//////////////////////////////////////////////////////////////////////

if (!function_exists('store_dropbox')) {



    function store_dropbox($name, $value, $companyid) {

        $CI = & get_instance();

        $CI->db->select('storeid,storename');

        $CI->db->from('bs_store');

        if ($companyid != "")

        //$CI->db->where('companyid', $companyid);
            $CI->db->where('storestatus', 'A');

        $CI->db->order_by("storename", "ASC");

        $query = $CI->db->get();



        //  echo "<pre>";
        //   print_r($query->result());
        //   exit;

        $dropdown = '<select class="pod form-control validate[required] has-search" name="' . $name . '" id="' . $name . '">';

        if ($query->num_rows() > 1) {
            $dropdown .= '<option value="0">' . lang('choose') . '</option>';
        }

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->storeid . '" ';

            if ($query->num_rows() == 1) {
                $dropdown .= 'selected="selected"';
            }
            if ($value == $row->storeid) {

                $dropdown .= 'selected="selected"';
            }

            $dropdown .= '>' . _s($row->storename, get_set_value('site_lang')) . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

    function store_dropbox2($name, $value, $companyid, $transfer_id = null) {

        $CI = & get_instance();

        $CI->db->select('storeid,storename');

        $CI->db->from('bs_store');

        if ($companyid != "")

        //$CI->db->where('companyid', $companyid);
            $CI->db->where('storestatus', 'A');

        $CI->db->order_by("storename", "ASC");

        $query = $CI->db->get();

        $sub = "";
        if ($transfer_id) {
            $sub = 'disabled';
        }



        $dropdown = '<select class="pod form-control select has-search" ' . $sub . ' name="' . $name . '" id="' . $name . '">';

        $dropdown .= '<option value="0" >' . lang('choose') . '</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->storeid . '" ';

            if ($value == $row->storeid) {

                $dropdown .= 'selected="selected"';
            }

            $dropdown .= '>' . _s($row->storename, get_set_value('site_lang')) . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('supplier_dropbox')) {



    function supplier_dropbox($name, $value) {

        $CI = & get_instance();

        $ownerid = ownerid();

        $CI->db->select('supplierid,suppliername');

        $CI->db->from('bs_suppliers');

        //$CI->db->where('ownerid',$ownerid);

        $CI->db->order_by("suppliername", "ASC");

        $query = $CI->db->get();



        $dropdown = '<select class="pod" name="' . $name . '" id="' . $name . '">';

        $dropdown .= '<option value="">Select Supplier</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->supplierid . '" ';

            if ($value == $row->supplierid) {

                $dropdown .= 'selected="selected"';
            }

            $dropdown .= '>' . $row->suppliername . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('company_dropbox')) {



    function company_dropbox($name, $value) {





        $CI = & get_instance();

        $memtype = get_member_type();

        $CI->db->select('companyid,company_name');

        $CI->db->from('bs_company');

        /* if ($memtype != 5) {

          $CI->db->where('ownerid', $CI->session->userdata('bs_userid'));

          } */

        $CI->db->order_by("companyid", "ASC");

        $query = $CI->db->get();



        $dropdown = '<select autocomplete="off" class="pod has-search" name="' . $name . '" id="' . $name . '">';

        if ($query->num_rows() > 1) {

            $dropdown .= '<option value="" >' . lang('choose') . '</option>';
        }

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->companyid . '" ';

            if ($query->num_rows() == 1) {
                $dropdown .= 'selected="selected"';
            }
            if ($value == $row->companyid) {

                $dropdown .= 'selected="selected"';
            }

            $com = json_decode($row->company_name, TRUE);

            $dropdown .= '>' . $com['en'] . ' | ' . $com['ar'] . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}



//////////////////////////////////////////////////////////////////////

if (!function_exists('country_dropbox')) {



    function country_dropbox($name, $value) {

        $CI = & get_instance();

        $CI->db->select('countryid,countryname,countrycode');

        $CI->db->from($CI->config->item('table_country'));

        $CI->db->where('countrystatus', 'A');

        $CI->db->order_by("countryname", "ASC");

        $query = $CI->db->get();

        $dropdown = '<select class="req" name="' . $name . '" id="' . $name . '">';

        $dropdown .= '<option value="">Select Country</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->countryid . '" ';

            if ($value == $row->countryid) {

                $dropdown .= 'selected="selected"';
            }

            $dropdown .= '>' . $row->countryname . ' (' . $row->countrycode . ')</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('city_dropbox')) {



    function city_dropbox($name, $value) {

        $CI = & get_instance();

        $CI->db->select('cityid,countryid,cityname,citycode');

        $CI->db->from($CI->config->item('table_city'));

        $CI->db->where('citystatus', 'A');

        $CI->db->order_by("cityname", "ASC");

        $query = $CI->db->get();



        $dropdown = '<select class="req" name="' . $name . '" id="' . $name . '">';

        $dropdown .= '<option value="">Select City</option>';

        if ($value != NULL) {

            foreach ($query->result() as $row) {

                $dropdown .= '<option value="' . $row->cityid . '" ';



                if ($value == $row->cityid) {

                    $dropdown .= 'selected="selected"';
                }

                $dropdown .= '>' . $row->cityname . ' (' . $row->citycode . ')</option>';
            }
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('location_dropbox')) {



    function location_dropbox($name, $value) {

        $CI = & get_instance();

        $memtype = get_member_type();

        $CI->db->select('locationid,location_name');

        $CI->db->from('bs_location');

        $CI->db->where('location_status', 'A');

        $CI->db->order_by("location_name", "ASC");

        $query = $CI->db->get();

        $dropdown = '<select class="req" name="' . $name . '" id="' . $name . '">';

        $dropdown .= '<option value="">Select Location</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->locationid . '" ';

            if ($value == $row->locationid) {

                $dropdown .= 'selected="selected"';
            }

            $dropdown .= '>' . $row->location_name . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('area_dropbox')) {



    function area_dropbox($name, $areavalue, $locationvalue) {

        $CI = & get_instance();

        $CI->db->select('areaname,areaid');

        $CI->db->from('bs_area');

        $CI->db->where('locationid', $locationvalue);

        $CI->db->where('areastatus', 'A');

        $CI->db->order_by("areaname", "ASC");

        $query = $CI->db->get();

        $dropdown = '<option value="">Select Area</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->areaid . '" ';

            if ($areavalue == $row->areaid) {

                $dropdown .= 'selected="selected"';
            }

            $dropdown .= '>' . $row->areaname . '</option>';
        }



        echo($dropdown);
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('financial_type')) {



    function financial_type($name, $val) {

        $CI = & get_instance();

        $CI->db->select('ftypeid,financial');

        $CI->db->from('bs_financial');

        $CI->db->where('financial_status', 'A');

        $CI->db->order_by("financial", "ASC");

        $query = $CI->db->get();

        $radio = '';

        foreach ($query->result() as $row) {

            $radio .= '<input type="radio" ';

            if ($row->ftypeid == $val) {

                $radio .= ' checked="checked" ';
            }

            $radio .= ' name="' . $name . '" value="' . $row->ftypeid . '" id="ftypeid' . $row->ftypeid . '" /><label for="ftypeid' . $row->ftypeid . '">' . $row->financial . '</label><br />';
        }

        echo $radio;
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('company_branch_dropbox')) {



    function company_branch_dropbox($name, $branchvalue, $companyid) {

        $CI = & get_instance();



        $CI->db->select('branchid,branchname');

        $CI->db->from('bs_company_branch');

        $CI->db->where('companyid', $companyid);

        $CI->db->order_by("branchid", "ASC");

        $query = $CI->db->get();





        $dropdown = '<select class="req has-search" name="' . $name . '" id="' . $name . '">';

        if ($query->num_rows() > 1) {

            $dropdown .= '<option value=""  >' . lang('choose') . '</option>';
        }

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->branchid . '" ';

            if ($query->num_rows() == 1) {
                $dropdown .= 'selected="selected"';
            }

            if ($branchvalue == $row->branchid) {

                $dropdown .= 'selected="selected"';
            }

            //$com = json_decode($row->branchname, TRUE);

            $dropdown .= '>' . _s($row->branchname, get_set_value('site_lang')) . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('get_banks')) {



    function get_banks($name, $value, $mesage, $el = 'select') {

        $CI = & get_instance();



        $CI->db->select('bankid,bankname');

        $CI->db->from('bs_bank');

        $CI->db->where('bankstatus', 'A');

        $CI->db->order_by("bankid", "ASC");

        $query = $CI->db->get();

        $dropdown = '<select class="req" name="' . $name . '" id="' . $name . '" data-toggle="tooltip" data-trigger="focus" title="' . $mesage . '">';

        $dropdown .= '<option value="">Select Bank</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->bankid . '" ';

            if ($value == $row->bankid) {

                $dropdown .= 'selected="selected"';
            }

            $com = json_decode($row->bankname, TRUE);

            $dropdown .= '>' . $row->bankname . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

//////////////////////////////////////////////////////////////////////



if (!function_exists('get_banks')) {



    function get_bank($name) {

        $CI = & get_instance();



        $CI->db->select('bankid,bankname');

        $CI->db->from('an_banks');

        $CI->db->where('bankid', $name);



        $query = $CI->db->get();



        echo($query->bankname);
    }

}



//////////////////////////////////////////////////////////////////////



if (!function_exists('validate_user')) {



    function validate_user() {

        $CI = & get_instance();

        if ($CI->session->userdata('bs_userid') == '' && $CI->uri->segment(1) != 'login') {

            $CI->session->sess_destroy();

            redirect(base_url() . 'login');
        } else if ($CI->session->userdata('bs_userid') != '' && $CI->uri->segment(1) == 'login') {

            redirect(base_url() . 'dashboard');
        }
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('redirect_to_login')) {



    function redirect_to_login($errorcode) {

        $CI = & get_instance();

        $CI->session->sess_destroy();

        redirect(base_url() . 'login?e=' . $errorcode);
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('ownerid')) {



    function ownerid() {

        $CI = & get_instance();

        $userid = $CI->session->userdata('bs_userid');

        $ownerid = $CI->session->userdata('bs_ownerid');

        $memtype = $CI->session->userdata('bs_memtype');

        if ($memtype == '5') {

            return $userid;
        } else if ($memtype == '1') {

            return $userid;
        } else {

            return $ownerid;
        }
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('get_modules_id')) {



    function get_modules_id($member_type, $parent = null) {

        $CI = & get_instance();



        $CI->db->select('permission_text');

        $CI->db->from('bs_permission');

        $CI->db->where('owner_id', $CI->session->userdata('bs_ownerid'));

        $CI->db->where('permission_type_id', $member_type);



        $perm = $CI->db->get();

        $permx = $perm->result_array();

        $permission = json_decode($permx[0]['permission_text'], TRUE);



        foreach ($permission as $moduleskey => $modulesdata) {

            $modulesid[] = $moduleskey;
        }

        $perm->free_result();

        return $modulesid;
    }

    function get_modules_id2($member_type, $key) {

        $CI = & get_instance();



        $CI->db->select('permission_text');

        $CI->db->from('bs_permission');

        $CI->db->where('owner_id', $CI->session->userdata('bs_ownerid'));

        $CI->db->where('permission_type_id', $member_type);



        $perm = $CI->db->get();

        $permx = $perm->result_array();

        $permission = json_decode($permx[0]['permission_text'], TRUE);



        foreach ($permission[$key] as $moduleskey => $modulesdata) {

            $modulesid[] = $moduleskey;
        }

        $perm->free_result();

        return $permission[$key];
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('get_member_type')) {



    function get_member_type() {

        $CI = & get_instance();

        return $CI->session->userdata('bs_memtype');
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('get_module_id')) {



    function get_module_id() {

        $CI = & get_instance();

        $CI->db->select('moduleid');

        $CI->db->from('mh_modules');

        $CI->db->where('module_controller', $CI->uri->segment(1));

        $query = $CI->db->get();

        $ppx = $query->result_array();

        $query->free_result();

        return $ppx[0]['moduleid'];
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('get_module_name')) {



    function get_module_name($mid, $c = false) {

        $CI = & get_instance();

        $CI->db->select('module_name,module_name_ar');

        $CI->db->from('mh_modules');

        if ($c == true) {

            $CI->db->where('module_controller', $mid);
        } else {

            $CI->db->where('moduleid', $mid);
        }

        $query = $CI->db->get();

        $ppx = $query->result_array();

        $query->free_result();

        if (get_set_value('site_lang') == 'english') {

            return $ppx[0]['module_name'];
        } else {



            return $ppx[0]['module_name_ar'];
        }
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('breadcramb')) {



    function breadcramb($value = null) {

        $CI = & get_instance();

        if ($value == null) {

            $parent = get_module_name($CI->uri->segment(1), true);

            $child = get_module_name($CI->uri->segment(1) . '/' . $CI->uri->segment(2), true);

            echo $parent;

            if ($child != '') {

                echo ' > ' . $child;
            }
        } else {

            echo $value;
        }
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('userinfo_permission')) {



    function userinfo_permission() {

        $CI = & get_instance();
        ///print_r($CI->session->all_userdata());
        $CI->db->select('*');

        $CI->db->from('bs_users');

        $CI->db->where('userid', $CI->session->userdata('bs_userid'));

        $query = $CI->db->get();

        if ($CI->session->userdata('bs_userid')) {

            if ($query->num_rows() > 0) {

                $ppx = $query->result_array();

                $result = $ppx[0];

                if ($result['status'] == 'D') {

                    //redirect_to_login(4);

                    redirect(base_url() . 'login');
                } else {

                    if ($result['member_type'] == '5') {

                        return $result;
                    } else {

                        if ($CI->uri->segment(1) == 'dashboard') {

                            return $result;
                        } else {

                            $permission = get_modules_id($result['member_type']);

                            //if($CI->uri->segment(2)==null){

                            $checkjson = $CI->db->query("SELECT moduleid FROM mh_modules WHERE module_controller='" . $CI->uri->segment(1) . "'");

                            /* }else{

                              $checkjson = $CI->db->query("SELECT moduleid FROM mh_modules WHERE module_controller='" . $CI->uri->segment(1).'/'.$CI->uri->segment(2). "'");

                              }

                             */

                            $res = $checkjson->result_array();

                            $resx = $res[0]['moduleid'];

                            if (in_array($resx, $permission)) {

                                return $result;
                            } else {

                                redirect(base_url() . 'dashboard?e=3');
                            }
                        }
                    }
                }
            } else {

                //redirect_to_login(3);

                redirect(base_url() . 'dashboard?e=3');
            }
        } else {

            redirect(base_url() . 'login');
        }
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('error_hander')) {



    function error_hander($eid) {

        if ($eid != '') {

            if (get_set_value('site_lang') == 'english') {

                $ErrorCSS = array(
                    'E' => '<div class="notif red alert alert-danger"><strong>OH SNAP!</strong> [HEARTDISK]<span class="icon icon-danger"></span></div>',
                    'S' => '<div class="notif green"><strong>WELL DONE!</strong> [HEARTDISK] <span class="icon icon-ok"></span></div>',
                    'W' => '<div class="notif red"><strong>WARNING!</strong> [HEARTDISK]</div>',
                    'N' => '<div class="notif red"><strong>HEADS UP!</strong> [HEARTDISK]</div>');
            } else {

                $ErrorCSS = array(
                    'E' => '<div class="notif red alert alert-danger"><strong>خطأ!</strong> [HEARTDISK]<span class="icon icon-danger"></span></div>',
                    'S' => '<div class="notif green"><strong>تم!</strong> [HEARTDISK] <span class="icon icon-ok"></span></div>',
                    'W' => '<div class="notif red"><strong>تحذير!</strong> [HEARTDISK]</div>',
                    'N' => '<div class="notif red"><strong>تنبيه!</strong> [HEARTDISK]</div>');
            }

            $CI = & get_instance();

            $q = $CI->db->query("SELECT error_title,error_title_ar,error_type FROM mh_errors WHERE errorid='" . $eid . "' LIMIT 0,1");



            foreach ($q->result() as $error) {



                if (get_set_value('site_lang') == 'english') {

                    echo str_replace('[HEARTDISK]', $error->error_title, $ErrorCSS[$error->error_type]);
                } else {



                    echo str_replace('[HEARTDISK]', $error->error_title_ar, $ErrorCSS[$error->error_type]);
                }
            }
        }
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('do_redirect')) {



    function do_redirect($func) {

        $CI = & get_instance();

        redirect(base_url() . $CI->uri->segment(1) . '/' . $func);
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('form_action_url')) {



    function form_action_url($func) {

        $CI = & get_instance();

        return base_url() . $CI->uri->segment(1) . '/' . $func;
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('sub_permission')) {



    function sub_permission() {

        $pingpong = array('e' => 'مشاهده', 'a' => 'اضافة', 'd' => 'حذف');

        return $pingpong;
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('get_permission_text')) {



    function get_permission_text() {

        $CI = & get_instance();

        $membertype = get_member_type();

        $ownerid = $CI->session->userdata('bs_ownerid');



        $CI->db->select('permission_text');

        $CI->db->from('bs_permission');

        $CI->db->where('owner_id', $ownerid);

        $CI->db->where('permission_type_id', $membertype);

        $perm = $CI->db->get();

        $permx = $perm->result_array();

        $subperm = sub_permission();



        $pingpong = json_decode($permx[0]['permission_text'], TRUE);

        foreach ($pingpong as $jkey => $jvalue) {

            $html .='<li>View / ';

            foreach ($jvalue as $key => $xvalue) {

                $html .= $subperm[$xvalue] . ' / ';
            }

            $html .= '  <strong>' . get_module_name($jkey) . '</strong></li>';
        }

        echo $html;
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('get_module_sub_permission')) {



    function get_module_sub_permission() {

        $CI = & get_instance();

        $membertype = get_member_type();

        $ownerid = $CI->session->userdata('bs_ownerid');

        $moduleid = get_module_id();

        //if ($membertype != 5) {

        $CI->db->select('permission_text');

        $CI->db->from('bs_permission');

        $CI->db->where('owner_id', $ownerid);

        $CI->db->where('permission_type_id', $membertype);

        $perm = $CI->db->get();

        $permx = $perm->result_array();

        $pingpong = json_decode($permx[0]['permission_text'], TRUE);

        //echo $moduleid;    

        return $pingpong[$moduleid];

        /* } else {

          return array('a', 'e', 'd');

          } */
    }

    function get_module_sub_permission2() {

        $CI = & get_instance();

        $membertype = get_member_type();

        $ownerid = $CI->session->userdata('bs_ownerid');

        $moduleid = get_module_id();

        //if ($membertype != 5) {

        $CI->db->select('permission_text');

        $CI->db->from('bs_permission');

        $CI->db->where('owner_id', $ownerid);

        $CI->db->where('permission_type_id', $membertype);

        $perm = $CI->db->get();

        $permx = $perm->result_array();

        $pingpong = json_decode($permx[0]['permission_text'], TRUE);

        //echo $moduleid;   





        $checkjson = $CI->db->query("SELECT moduleid FROM mh_modules WHERE module_controller='" . $CI->uri->segment(1) . "/" . $CI->uri->segment(2) . "'");

        /* }else{

          $checkjson = $CI->db->query("SELECT moduleid FROM mh_modules WHERE module_controller='" . $CI->uri->segment(1).'/'.$CI->uri->segment(2). "'");

          }

         */

        $res = $checkjson->result_array();

        $resx = $res[0]['moduleid'];

        //echo $resx;



        foreach ($pingpong as $moduleskey => $modulesdata) {

            //$modulesid[] = $moduleskey;

            if ($resx == $moduleskey) {



                return $modulesdata;
            }
        }





        //return $modulesid;



        /* } else {

          return array('a', 'e', 'd');

          } */
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('action_buttons')) {



    function action_buttons($function_name, $record_count) {

        $CI = & get_instance();

        //'e'=>'<input type="button" class="edit_icon"/>',

        /* $permHTML = array(

          'a' => '<a href="' . base_url() . $CI->uri->segment(1) . '/' . $function_name . '"><div class="add_icon"></div></a>',

          'd' => '<input type="button" class="delete_icon"/>',

          'p' => '<input type="button" class="print_icon"/>'

          ); */



        if (get_set_value('site_lang') == 'english') {

            $permHTML = array(
                'a' => '<br clear="all"/><a href="' . base_url() . $CI->uri->segment(1) . '/' . $function_name . '" class="btn-flat success pull-right"><i class="icon-pencil"></i>Add</a>',
                'd' => '<button type="button" class="btn-flat danger pull-right" value=""/><i class="icon-trash"></i>Delete</button>',
                'p' => '<button type="button" class="btn-flat gray pull-right" value=""/><i class="icon-print"></i>Print</input>'
            );
        } else {



            $permHTML = array(
                'a' => '<br clear="all"/><a href="' . base_url() . $CI->uri->segment(1) . '/' . $function_name . '" class="btn-flat success pull-right"><i class="icon-pencil"></i>اضافة جديد</a>',
                'd' => '<button type="button" class="btn-flat danger pull-right" value="" style="font-family: arial;font-weight: bold;font-size: 15px;"/><i class="icon-trash"></i>حـذف</button>',
                'p' => '<button type="button" class="btn-flat gray pull-right" value=""/><i class="icon-print"></i>طباعة</input>'
            );
        }





        $html = '';

        foreach (get_module_sub_permission() as $eye => $eyevalue) {

            if ($eyevalue == 'a') {

                $html .= $permHTML['a'];
            } else if ($record_count > 0 && $eyevalue == 'e') {

                $html .= $permHTML['e'];
            } else if ($record_count > 0 && $eyevalue == 'd' && $CI->uri->segment(2) != 'banks') {

                $html .= $permHTML['d'];
            }
        }

        if ($record_count > 0) {

            $html .= $permHTML['p'];
        }



        echo('<div class="groub_buuton">' . $html . '</div>');
    }

}

//////////////////////////////////////////////////////////////////////

if (!function_exists('edit_button')) {



    function edit_button($function_name) {

        $CI = & get_instance();

        //'e'=>'<input type="button" class="edit_icon"/>',

        $permHTML = array(
            'e' => '<a href="' . base_url() . $CI->uri->segment(1) . '/' . $function_name . '"><i class="icon-edit"></i> </a>'
        );

        $html = '';



        //var_dump(get_module_sub_permission2());

        foreach (get_module_sub_permission2() as $eye => $eyevalue) {

            if ($eyevalue == 'e') {

                $html .= $permHTML['e'];
            }
        }

        echo($html);



        //die();
    }

}

////////////////////////////////////////////////////////////////////////////

if (!function_exists('get_status')) {



    function get_status($status) {

        switch ($status) {

            case 'D';

                echo('Deactive');

                break;



            case 'A';

                echo('Active');

                break;



            case 'N';

                echo('No');

                break;



            case 'Y';

                echo('Yes');

                break;



            case 'M';

                return 'Male';

                break;



            case 'F';

                return 'Female';

                break;
        }
    }

}

////////////////////////////////////////////////////////////////////////

if (!function_exists('get_statusdropdown')) {



    function get_statusdropdown($current_value, $name, $opt = 0) {

        if (get_set_value('site_lang') == 'english') {

            if ($opt != 0 && $opt != 1) {

                $values = array('N' => 'No', 'Y' => 'Yes');
            }

            if ($opt == 2) {

                $values = array('0' => 'Select Gender', 'M' => 'Male', 'F' => 'Female');
            } else {

                $values = array('A' => 'Active', 'D' => 'Deactive');
            }
        } else {



            if ($opt != 0 && $opt != 1) {

                $values = array('N' => 'لا', 'Y' => 'نعم');
            }

            if ($opt == 2) {

                $values = array('0' => 'اختر', 'M' => 'ذكر', 'F' => 'انثي');
            } else {

                $values = array('A' => 'فعال', 'D' => 'غير فعال');
            }
        }

        $dropdown = '<select name="' . $name . '" id="' . $name . '" class="select">';

        foreach ($values as $key => $value) {

            $dropdown .= '<option value="' . $key . '" ';

            if ($current_value == $key) {

                $dropdown .= 'selected="selected"';
            }

            $dropdown .= '>' . $value . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

////////////////////////////////////////////////////////////////////////

if (!function_exists('per_value')) {



    function per_value($name, $val, $showvalue = '') {

        if (get_set_value('site_lang') == 'english') {

            $values = array('W' => 'Weeks', 'M' => 'Months', 'Y' => 'Years');
        } else {



            $values = array('W' => 'اسبوعيا', 'M' => 'شهريا', 'Y' => 'سنويا');
        }

        if ($showvalue != '') {

            echo $values[$showvalue];
        } else {

            $dropdown = '<select name="' . $name . '" id="' . $name . '" >';

            foreach ($values as $key => $value) {

                $dropdown .= '<option value="' . $key . '" ';

                if ($val == $key) {

                    $dropdown .= 'selected="selected"';
                }

                $dropdown .= '>' . $value . '</option>';
            }

            $dropdown .= '</select>';

            echo($dropdown);
        }
    }

}

////////////////////////////////////////////////////////////////////////

if (!function_exists('per_unit')) {



    function per_unit($name, $val, $showval = '') {

        if (get_set_value('site_lang') == 'english') {

            $values = array('U' => 'Unit', 'K' => 'Kg');
        } else {

            $values = array('U' => 'وحده', 'K' => 'كيلوجرام');
        }

        if ($showval != '') {

            echo $values[$showval];
        } else {

            foreach ($values as $valx => $data) {

                $radio .= ' <input type="radio" ';

                if ($valx == $val) {

                    $radio .= ' checked="checked" ';
                }

                $radio .= ' name="' . $name . '" value="' . $valx . '" id="ftypeid' . $valx . '" /><label for="ftypeid' . $valx . '"> ' . $data . '</label>';
            }

            echo $radio;
        }
    }

}

////////////////////////////////////////////////////////////////////////

if (!function_exists('product_type')) {



    function product_type($name, $val, $showvalue = '') {

        $values = array('P' => 'Product', 'S' => 'Service');

        if ($showvalue != '') {

            echo $values[$showvalue];
        } else {

            $dropdown = '<select class="pod" name="' . $name . '" id="' . $name . '">';

            foreach ($values as $key => $value) {

                $dropdown .= '<option value="' . $key . '" ';

                if ($val == $key) {

                    $dropdown .= 'selected="selected"';
                }

                $dropdown .= '>' . $value . '</option>';
            }

            $dropdown .= '</select>';

            echo($dropdown);
        }
    }

}

///////////////////////////////////////////////////////////////////////////

function upload_file($filefield, $folderpath, $thumb = false, $w = 0, $h = 0) {

    $CI = & get_instance();

    if (!is_dir($folderpath)) {

        mkdir($folderpath, 0777, true);
    }



    $config['upload_path'] = $folderpath;

    $config['allowed_types'] = '*';

    $config['max_size'] = '5000';

    $config['encrypt_name'] = TRUE;

    $CI->load->library('upload', $config);

    $CI->load->library('image_lib');

    if (!$CI->upload->do_upload($filefield)) {

        $error = array('error' => $CI->upload->display_errors());

        return $error = '';
    } else {

        $image_data = $CI->upload->data();

        if ($thumb == true) {

            $thumb['image_library'] = 'gd2';

            $thumb['source_image'] = $folderpath;

            $thumb['create_thumb'] = TRUE;

            $thumb['maintain_ratio'] = FALSE;

            $thumb['width'] = $w;

            $thumb['height'] = $h;

            $CI->image_lib->clear();

            $CI->image_lib->initialize($thumb);

            $CI->image_lib->resize();
        }

        return $image_data['file_name'];
    }
}

//////////////////////////////////////////////////////////////////////

if (!function_exists('product_dropbox')) {



    function product_dropbox($name, $value, $comid) {

        $CI = & get_instance();

        $CI->db->select('itemid,itemname,serialnumber');

        $CI->db->from('bs_item');

        $CI->db->where('ownerid', ownerid());

        $CI->db->where('comid', $comid);

        $CI->db->where('product_status', 'A');

        $CI->db->order_by("itemname", "ASC");

        $query = $CI->db->get();



        $dropdown = '<select name="' . $name . '" id="' . $name . '">';

        $dropdown .= '<option value="">Select Product</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option data-serial="' . $row->serialnumber . '" value="' . $row->itemid . '" ';

            if ($value == $row->itemid) {

                $dropdown .= 'selected="selected"';
            }

            $dropdown .= '>' . $row->itemname . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

if (!function_exists('get_set_value')) {



    function get_set_value($col = null) {



        $CI = & get_instance();

        /*

          $CI->db->select('*');

          $CI->db->from('setting');

          $CI->db->where('is_default', '1');

          $query = $CI->db->get();

          return $query->row()->$col;

         */

        return $CI->session->userdata('site_lang');

        //echo $CI->session->userdata('site_lang');
    }

}





//Created by M.Ahmed

if (!function_exists('company_bank_dropbox')) {



    function company_bank_dropbox($name, $value, $language, $function = '', $id = null) {

        $CI = & get_instance();

        $CI->db->select('id,bank_name');

        $CI->db->where('status', 'Active');

        $CI->db->from('an_banks');

        $query = $CI->db->get();



        if ($id) {



            $dropdown = '<select name="' . $name . '" id="' . $id . '" ' . $function . '  class="region_class required  valid " val = "" language="' . $language . '">';
        } else {



            $dropdown = '<select name="' . $name . '" id="' . $name . '" ' . $function . '  class="region_class required  valid " val = "" language="' . $language . '">';
        }

        if ($language == 'english')
            $dropdown .= '<option value="">Select Bank</option>';
        else
            $dropdown .= '<option value="">حدد البنك</option>';



        foreach ($query->result() as $row) {

            if ($language == 'english')
                $dropdown .= '<option data-serial="' . $row->bank_name . '" value="' . $row->id . '" ';
            else
                $dropdown .= '<option data-serial="' . $row->bank_name . '" value="' . $row->id . '" ';

            if ($value == $row->id) {

                $dropdown .= 'selected="selected"';
            }

            if ($language == 'english')
                $dropdown .= '>' . $row->bank_name . '</option>';
            else
                $dropdown .= '>' . $row->bank_name . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}



//Created by M.Ahmed

if (!function_exists('expense_charges')) {



    function expense_charges($name, $value, $language, $function = '') {

        $CI = & get_instance();

        $CI->db->select('id,expense_title');

        //$CI->db->where('status', 'Active');

        $CI->db->from('an_expenses_charges');

        $query = $CI->db->get();



        $dropdown = '<select name="' . $name . '" id="' . $name . '" ' . $function . '  class="js-example-basic-single region_class required  valid " val = "" language="' . $language . '">';

        if ($language == 'english')
            $dropdown .= '<option value="">Select</option>';
        else
            $dropdown .= '<option value="">اختر</option>';



        foreach ($query->result() as $row) {

            if ($language == 'english')
                $dropdown .= '<option data-serial="' . $row->expense_title . '" value="' . $row->id . '" ';
            else
                $dropdown .= '<option data-serial="' . $row->expense_title . '" value="' . $row->id . '" ';

            if ($value == $row->id) {

                $dropdown .= 'selected="selected"';
            }

            if ($language == 'english')
                $dropdown .= '>' . $row->expense_title . '</option>';
            else
                $dropdown .= '>' . $row->expense_title . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

//Created by M.Ahmed



function expense_charges2($name, $value, $language, $function = '') {

    $CI = & get_instance();

    $CI->db->select('id,expense_title');

    //$CI->db->where('status', 'Active');

    $CI->db->from('an_expenses_charges');

    $query = $CI->db->get();



    $dropdown = '<select name="' . $name . '" id="' . $name . '" ' . $function . '  class="region_class required  valid " val = "" language="' . $language . '">';

    if ($language == 'english')
        $dropdown .= '<option value="">Select</option>';
    else
        $dropdown .= '<option value="">اختر</option>';



    foreach ($query->result() as $row) {

        if ($language == 'english')
            $dropdown .= '<option data-serial="' . $row->expense_title . '" value="' . $row->id . '" ';
        else
            $dropdown .= '<option data-serial="' . $row->expense_title . '" value="' . $row->id . '" ';

        if ($value == $row->id) {

            $dropdown .= 'selected="selected"';
        }

        if ($language == 'english')
            $dropdown .= '>' . $row->expense_title . '</option>';
        else
            $dropdown .= '>' . $row->expense_title . '</option>';
    }

    $dropdown .= '</select>';

    echo($dropdown);
}

function get_count_ft($t = null, $co = null, $cval = null) {





    $CI = & get_instance();

    $CI->db->where($co, $cval);

    $CI->db->from($t);

    $query = $CI->db->get();

    return $query->num_rows();
}

function post($name = null) {

    $CI = & get_instance();

    return $CI->input->post($name, true);
}

function DoSelect($key, $key2, $type = "") {

    if ($key == $key2) {

        if ($type == '') {

            return 'selected=selected';
        } else {

            return "checked='checked'";
        }
    }
}

function get_store_quantity($id = null) {

    $CI = & get_instance();

    $result = $CI->db->query("SELECT 

             SUM(bi.`quantity`) AS totalquantity,  

             bi.*,

              `bs_category`.`catname`,

             `bs_suppliers`.`suppliername`,

             bi.`created` AS purchase_date,

             `i`.`itemid`,

             i.product_picture,

             `i`.`itemname`,

             `i`.`itemtype`,

             IF(

               i.`itemtype` = 'P',

               `i`.`serialnumber`,

               '-----'

             ) AS `serialnumber`,

             IF(

               i.`itemtype` = 'P',

               CONCAT(`i`.`purchase_price`, ' RO.'),

               '-----'

             ) AS `purchase_price`,

             IF(

               i.`itemtype` = 'P',

               CONCAT(`i`.`sale_price`, ' RO.'),

               '-----'

             ) AS `sale_price`,

             IF(

               i.`itemtype` = 'P',

               `i`.`min_sale_price`,

               '-----'

             ) AS `min_sale_price`,

             IF(

               i.`itemtype` = 'P',

               `i`.`point_of_re_order`,

               '-----'

             ) AS `point_of_re_order` 



           FROM

             `bs_item` AS i 

             INNER JOIN `bs_store_items` AS bi 

               ON bi.`item_id` = i.`itemid` 

             INNER JOIN `bs_category` 

               ON `i`.`categoryid` = `bs_category`.`catid` 

             INNER JOIN `bs_suppliers` 

               ON `i`.`suppliderid` = `bs_suppliers`.`supplierid` 

           WHERE bi.`store_id` = '" . $id . "' 

           GROUP BY i.`itemid`");







    //return $result->result()->totalquantity; 

    $fquantity = 0;

    if ($result->result()) {



        foreach ($result->result() as $quantity) {



            $fquantity+=$quantity->totalquantity;
        }
    }

    return $fquantity;
}

function get_product_purchase($id = null) {

    $CI = & get_instance();

    $CI->db->select('purchase_invoice_items.*,bs_item.*');

    $CI->db->where('purchase_invoice_items.invoice_id', $id);

    $CI->db->join("bs_item", "bs_item.itemid=purchase_invoice_items.product_id", 'left');

    $query = $CI->db->get('purchase_invoice_items');

    return $query->result();



    //var_dump($query->result());
    //die('hihihi');
}

function get_invoice_items($id = null) {

    $CI = & get_instance();

    $CI->db->select('bs_invoice_items.*,bs_item.*');

    $CI->db->select('bs_invoice_items.invoice_item_price as iip');

    $CI->db->select('bs_invoice_items.invoice_item_store_id as store');

    $CI->db->select('invoice_payments.inv_payment_id,invoice_payments.status,invoice_payments.payment_id,invoice_payments.invoice_id');

    $CI->db->select('GROUP_CONCAT(invoice_payments.inv_payment_id) as inv_payment_id_all');

    $CI->db->select('GROUP_CONCAT(invoice_payments.payment_id) as payment_id_all');

    $CI->db->select('Sum(invoice_payments.payment_amount) as totalpay');



    //GROUP_CONCAT(ds.bs_user_id) AS cols





    $CI->db->where('bs_invoice_items.invoice_id', $id);



    $CI->db->join("bs_item", "bs_item.itemid=bs_invoice_items.invoice_item_id", 'LEFT');

    $CI->db->join("invoice_payments", "invoice_payments.invoice_id=bs_invoice_items.invoice_id", 'LEFT');



    $CI->db->group_by('bs_invoice_items.inovice_product_id');

    $query = $CI->db->get('bs_invoice_items');

    return $query->result();
}

function get_purchase_orderitems($id = null) {

    $CI = & get_instance();

    $CI->db->select('bs_purchase_items_order.*,bs_item.*');

    $CI->db->select('bs_purchase_items_order.purchase_item_price as iip');

    $CI->db->select('bs_purchase_items_order.purchase_item_store_id as store');
    $CI->db->select('bs_purchase_items_order.`inovice_product_id` AS ippid');
//,
//    bs_purchase_items_order.`inovice_product_id` AS ippid
    //GROUP_CONCAT(ds.bs_user_id) AS cols





    $CI->db->where('bs_purchase_items_order.purchase_id', $id);



    $CI->db->join("bs_item", "bs_item.itemid=bs_purchase_items_order.purchase_item_id", 'LEFT');


    $CI->db->group_by('bs_purchase_items_order.inovice_product_id');

    $query = $CI->db->get('bs_purchase_items_order');

    return $query->result();
}

function get_purchase_items($id = null) {

    $CI = & get_instance();

    $CI->db->select('bs_purchase_items.*,raw_material.*,raw_store_items.*');

    $CI->db->select('bs_purchase_items.purchase_item_price as iip');

    $CI->db->select('bs_purchase_items.purchase_item_store_id as store');

    $CI->db->select('purchase_payments.inv_payment_id,purchase_payments.status,purchase_payments.payment_id,purchase_payments.purchase_id');

    $CI->db->select('GROUP_CONCAT(purchase_payments.inv_payment_id) as inv_payment_id_all');

    $CI->db->select('GROUP_CONCAT(purchase_payments.payment_id) as payment_id_all');

    $CI->db->select('Sum(purchase_payments.payment_amount) as totalpay');



    //GROUP_CONCAT(ds.bs_user_id) AS cols





    $CI->db->where('bs_purchase_items.purchase_id', $id);



    $CI->db->join("raw_material", "raw_material.id=bs_purchase_items.purchase_item_id", 'LEFT');

    $CI->db->join("purchase_payments", "purchase_payments.purchase_id=bs_purchase_items.purchase_id", 'LEFT');
    $CI->db->join("raw_store_items", "raw_store_items.purchase_id=bs_purchase_items.purchase_id and raw_store_items.item_id=bs_purchase_items.purchase_item_id", 'LEFT');



    $CI->db->group_by('bs_purchase_items.inovice_product_id');

    $query = $CI->db->get('bs_purchase_items');

    return $query->result();
}

function get_quotation_items($id = null) {

    $CI = & get_instance();

    $CI->db->select('bs_quotation_items.*,bs_item.*');

    $CI->db->select('bs_quotation_items.quotation_item_price as iip');

    $CI->db->where('bs_quotation_items.quotation_id', $id);

    $CI->db->join("bs_item", "bs_item.itemid=bs_quotation_items.quotation_item_id", 'LEFT');

    $CI->db->group_by('bs_quotation_items.quotation_product_id');

    $query = $CI->db->get('bs_quotation_items');

    return $query->result();
}

function get_sales_items($id = null, $tb = null) {

    $CI = & get_instance();

    if ($tb == null) {

        $sql = "SELECT 

                bsalesp.*,

                bu.*,

                bsales.*	

             FROM

               `bs_sales_invoice_persons` bsalesp 



               INNER JOIN bs_sales AS bsales  ON bsales.bs_sales_id=bsalesp.sales_id



               INNER JOIN `bs_users` AS bu  ON bu.`userid` = bsales.`bs_user_id` 



               WHERE bsalesp.invoice_id=$id";
    } else {

        $sql = "SELECT 

                bsalesp.*,

                bu.*,

                bsales.*	

             FROM

               `bs_sales_quotation_persons` bsalesp 



               INNER JOIN bs_sales AS bsales  ON bsales.bs_sales_id=bsalesp.sales_id



               INNER JOIN `bs_users` AS bu  ON bu.`userid` = bsales.`bs_user_id` 



               WHERE bsalesp.quotation_id=$id";
    }

    $query = $CI->db->query($sql);

    return $query->result();
}

function get_payment_invoice($id = null) {

    $CI = & get_instance();

    $sql = "SELECT 

            payments.*,

            invoice_payments.*,

            an_cash_management.*,

            an_company_transaction.*,

            an_company_transaction.invoice AS invoice,

            invoice_payments.payment_type AS iptype,

            invoice_payments.payment_amount as ipamount,

            invoice_payments.created as ipcreated,

            invoice_payments.method_payment as ipmethod,

            an_cash_management.invoice_id as icash,

            an_company_transaction.cheque_number as cchequenum,

            an_company_transaction.bank_id as cbank_id,

            `an_banks`.`id`,

            `an_banks`.`bank_name`,

            an_bank_accounts.`account_id`,

            `an_bank_accounts`.`account_name`, 

            an_company_transaction.notes AS cnotes

            FROM

            `invoice_payments` 

            LEFT JOIN payments 

              ON payments.id = invoice_payments.payment_id 

            LEFT JOIN an_cash_management 

              ON an_cash_management.invoice_id = invoice_payments.invoice_id 

            LEFT JOIN an_company_transaction 

              ON an_company_transaction.invoice = invoice_payments.invoice_id 

            LEFT JOIN an_banks 

              ON `an_banks`.`id` = `an_company_transaction`.`bank_id` 

            LEFT JOIN an_bank_accounts 

               ON `an_bank_accounts`.`account_id` = `an_company_transaction`.`account_id` 

            WHERE invoice_payments.invoice_id = '$id' 



            GROUP BY invoice_payments.`inv_payment_id` ";

    $query = $CI->db->query($sql);

    return $query->result();
}

function get_payment_purchase2($id = null) {

    $CI = & get_instance();

    $sql = "SELECT 

            payments.*,

            purchase_payments.*,

            an_cash_management.*,

            an_company_transaction.*,

            an_company_transaction.invoice AS invoice,

            purchase_payments.payment_type AS iptype,

            purchase_payments.payment_amount as ipamount,

            purchase_payments.created as ipcreated,

            purchase_payments.method_payment as ipmethod,

            an_cash_management.invoice_id as icash,

            an_company_transaction.cheque_number as cchequenum,

            an_company_transaction.bank_id as cbank_id,

            `an_banks`.`id`,

            `an_banks`.`bank_name`,

            an_bank_accounts.`account_id`,

            `an_bank_accounts`.`account_name`, 

            an_company_transaction.notes AS cnotes

            FROM

            `purchase_payments` 

            LEFT JOIN payments 

              ON payments.id = purchase_payments.payment_id 

            LEFT JOIN an_cash_management 

              ON an_cash_management.invoice_id = purchase_payments.purchase_id 

            LEFT JOIN an_company_transaction 

              ON an_company_transaction.invoice = purchase_payments.purchase_id 

            LEFT JOIN an_banks 

              ON `an_banks`.`id` = `an_company_transaction`.`bank_id` 

            LEFT JOIN an_bank_accounts 

               ON `an_bank_accounts`.`account_id` = `an_company_transaction`.`account_id` 

            WHERE purchase_payments.purchase_id = '$id' 



            GROUP BY purchase_payments.`inv_payment_id` ";

    $query = $CI->db->query($sql);

    return $query->result();
}

function get_expences_invoice($id = null, $par = null) {

    $CI = & get_instance();

    /* $sql = "SELECT 

      an_expenses.*,

      `an_banks`.`id`,

      `an_banks`.`bank_name`,

      an_bank_accounts.`account_id`,

      `an_bank_accounts`.`account_name`,

      an_expenses_charges.id,

      an_expenses_charges.expense_title as cexpense_title

      FROM

      `an_expenses`

      LEFT JOIN an_banks

      ON `an_banks`.`id` = `an_expenses`.`bank_id`

      LEFT JOIN an_bank_accounts

      ON `an_bank_accounts`.`account_id` = `an_expenses`.`account_id`

      LEFT JOIN an_expenses_charges

      ON `an_expenses_charges`.`id` = `an_expenses`.`expense_charges_id`



      WHERE an_expenses.invoice_id = '$id'



      GROUP BY an_expenses.`id` ";

     */

    if ($par == null) {

        $sql = "SELECT 

            expense_purchase.*,

            an_expenses.*,

            `an_banks`.`id`,

            `an_banks`.`bank_name`,

            an_bank_accounts.`account_id`,

            `an_bank_accounts`.`account_name`,

            an_expenses_charges.id,

            an_expenses_charges.expense_title as cexpense_title

            

          FROM

            `expense_purchase` 

            

            LEFT JOIN an_expenses 

              ON `an_expenses`.`id` = `expense_purchase`.`expense_id` 

            

            LEFT JOIN an_banks 

              ON `an_banks`.`id` = `an_expenses`.`bank_id` 

              

            LEFT JOIN an_bank_accounts 

              ON `an_bank_accounts`.`account_id` = `an_expenses`.`account_id` 

              



            LEFT JOIN an_expenses_charges 

                ON `an_expenses_charges`.`id` = `an_expenses`.`expense_charges_id` 

                

          WHERE expense_purchase.invoice_id = '$id' 

              

          GROUP BY expense_purchase.`expense_id` ";
    } else {



        $sql = "SELECT 

            expense_sales.*,

            an_expenses.*,

            `an_banks`.`id`,

            `an_banks`.`bank_name`,

            an_bank_accounts.`account_id`,

            `an_bank_accounts`.`account_name`,

            an_expenses_charges.id,

            an_expenses_charges.expense_title as cexpense_title

            

          FROM

            `expense_sales` 

            

            LEFT JOIN an_expenses 

              ON `an_expenses`.`id` = `expense_sales`.`expense_id` 

            

            LEFT JOIN an_banks 

              ON `an_banks`.`id` = `an_expenses`.`bank_id` 

              

            LEFT JOIN an_bank_accounts 

              ON `an_bank_accounts`.`account_id` = `an_expenses`.`account_id` 

              



            LEFT JOIN an_expenses_charges 

                ON `an_expenses_charges`.`id` = `an_expenses`.`expense_charges_id` 

                

          WHERE expense_sales.invoice_id = '$id' 

              

          GROUP BY expense_sales.`expense_id` ";
    }

    $query = $CI->db->query($sql);

    return $query->result();
}

function get_invoice_sales($id) {

    $CI = & get_instance();

    $sql = "SELECT 

            bs_sales_invoice_persons.*,  

            `bs_users`.`fullname`,

            `bs_users`.`userid`,

             bs_sales.`bs_sales_id`

            FROM

              `bs_sales_invoice_persons` 

              LEFT JOIN bs_sales 

                ON `bs_sales`.`bs_sales_id` = `bs_sales_invoice_persons`.`sales_id` 

              LEFT JOIN `bs_users` 

                ON `bs_users`.`userid` = `bs_sales`.`bs_user_id` 





            WHERE bs_sales_invoice_persons.invoice_id = '$id' 

            GROUP BY bs_sales_invoice_persons.`bs_sales_invoice_person_id` ";



    $query = $CI->db->query($sql);

    return $query->result();
}

function get_quotation_sales($id) {

    $CI = & get_instance();

    $sql = "SELECT 

            bs_sales_quotation_persons.*,  

            `bs_users`.`fullname`,

            `bs_users`.`userid`,

             bs_sales.`bs_sales_id`

            FROM

              `bs_sales_quotation_persons` 

              LEFT JOIN bs_sales 

                ON `bs_sales`.`bs_sales_id` = `bs_sales_quotation_persons`.`sales_id` 

              LEFT JOIN `bs_users` 

                ON `bs_users`.`userid` = `bs_sales`.`bs_user_id` 





            WHERE bs_sales_quotation_persons.quotation_id = '$id' 

            GROUP BY bs_sales_quotation_persons.`bs_sales_quotation_person_id` ";



    $query = $CI->db->query($sql);

    return $query->result();
}

function get_payment_purchase($id = null) {

    $CI = & get_instance();

    $CI->db->select('purchase_payment.*');

    $CI->db->where('purchase_payment.invoice_id', $id);

    //$CI->db->join("bs_item", "bs_item.itemid=purchase_invoice_items.product_id",'left');

    $query = $CI->db->get('purchase_payment');

    return $query->result();



    //var_dump($query->result());
    //die('hihihi');
}

function get_payment_method() {

    /* $CI = & get_instance();

      $CI->db->select('purchase_payment.*');

      $CI->db->where('purchase_payment.invoice_id',$id);

      //$CI->db->join("bs_item", "bs_item.itemid=purchase_invoice_items.product_id",'left');

      $query = $CI->db->get('purchase_payment');

      return $query->result();

     */

    if (get_set_value('site_lang') == 'english') {



        $op = '<option value="transfer">Transfer</option>

        <option value="deposite">Deposite</option>

        <option value="cheque">Cheque</option>

        <option value="withdraw">Withdraw</option>';

        return $op;
    } else {



        $op = '<option value="transfer">تحويل</option>

        <option value="deposite">ايداع</option>

        <option value="cheque">شيك</option>

        <option value="withdraw">اجل</option>';

        return $op;
    }







    //var_dump($query->result());
    //die('hihihi');
}

function get_c_d_customer($id, $col) {

    $CI = & get_instance();

    $sql = "

            SELECT SUM( p.`payment_amount` ) AS totalpayment, total_invoice_payment

            FROM  `payments` AS p

            LEFT JOIN (



            SELECT i.customer_id, SUM( ip.`payment_amount` ) AS total_invoice_payment

            FROM  `invoice_payments` AS ip

            INNER JOIN  `an_invoice` AS i ON i.`invoice_id` = ip.`invoice_id` 

            WHERE i.`customer_id` =  '$id'

            ) AS ip ON ip.`customer_id` = p.`refrence_id` 

            WHERE p.`refrence_id` =  '$id'

            AND p.`payment_type` =  'receipt'";



    $total = 0;

    $q = $CI->db->query($sql);





    if ($q->num_rows() > 0) {

        //var_dump($q->row());    

        return $q->row()->$col;
    } else {

        return false;
    }
}

function new_dc_customer($id = null) {

    /* $sql="SELECT 

      i.`customer_id`,

      i.`invoice_id` AS invoice_id,

      i.`sales_amount`,

      i.`invoice_total_amount`,

      pamount,

      i.`invoice_date`,

      SUM(pamount) AS amount

      FROM

      `an_invoice` AS i

      INNER JOIN `bs_users` AS c

      ON c.`userid` = i.`customer_id`

      LEFT JOIN

      (SELECT

      SUM(ip.`payment_amount`) AS pamount,

      ip.`invoice_id`

      FROM

      `invoice_payments` AS ip

      INNER JOIN `an_invoice` AS ii

      ON ii.`invoice_id` = ip.`invoice_id`

      INNER JOIN `bs_users` AS c

      ON c.`userid` = ii.`customer_id`

      WHERE c.`userid` = '$id'

      GROUP BY ip.`invoice_id`) d

      ON d.invoice_id = i.`invoice_id`

      WHERE i.customer_id = '$id'  AND i.`invoice_status` = '0'

      GROUP BY i.`invoice_id`";

     */

    $CI = & get_instance();

    $sql = "SELECT 

              i.`customer_id`,



              SUM(i.`sales_amount`),

              i.`invoice_total_amount`,

              pamount,

              i.`invoice_date`,

              SUM(pamount) AS amount ,

              SUM(i.`sales_amount`)-pamount AS totalremaining

            FROM

              `an_invoice` AS i 

              INNER JOIN `bs_users` AS c 

                ON c.`userid` = i.`customer_id` 

              LEFT JOIN 

                (SELECT 

                  SUM(ip.`payment_amount`) AS pamount,

                  ip.`invoice_id` 

                FROM

                  `invoice_payments` AS ip 

                  INNER JOIN `an_invoice` AS ii 

                    ON ii.`invoice_id` = ip.`invoice_id` 

                  INNER JOIN `bs_users` AS c 

                    ON c.`userid` = ii.`customer_id` 

                WHERE c.`userid` = '60' 

                GROUP BY ii.`customer_id`) d 

                ON d.invoice_id = i.`invoice_id` 

            WHERE i.customer_id = '60'  AND i.`invoice_status` = '0'

            GROUP BY i.`customer_id`";

    $q = $CI->db->query($sql);

    if ($q->num_rows()) {



        return $q->row()->totalremaining;
    } else {



        return false;
    }
}

function _s($item, $lang = "en") {



    $str = null;

    if ($item != NULL && @unserialize($item) !== FALSE) {

        $v = @unserialize($item);

        if (key_exists($lang, $v))
            $str = $v[$lang];
        else
            $str = reset($v);
    }

    else {

        $v = @json_decode($item);

        if ($item != NULL && is_object($v)) {



            if (key_exists($lang, $v))
                $str = $v->$lang;

            else {

                $v = (array) $v;

                $str = reset($v);
            }
        }
    }



    return $str;
}

function get_last_add_comapny() {

    $CI = & get_instance();

    $sql = "SELECT 

            * 

          FROM

            bs_company 

          ORDER BY companyid DESC LIMIT 1 ";

    $q = $CI->db->query($sql);

    if ($q->num_rows()) {



        return $q->row()->companyid;
    } else {



        return 0;
    }
}

function get_setting_option() {

    $CI = & get_instance();

    $query = $CI->db->get('setting');

    if ($query->num_rows() > 0) {

        return $query->row();
    }
}

if (!function_exists('getCashBranhes')) {





    function getCashBranhes($cashstatus, $company_id) {





        $CI = & get_instance();

        $memtype = get_member_type();

        $CI->db->select('id,branch_cash_name');

        $CI->db->from('an_branch_cashh');

        //$CI->db->where('company_id',$company_id);

        $CI->db->order_by("id", "ASC");

        $query = $CI->db->get();

        $result = $query->result();

        echo "<pre>";

        print_r($result);

        if ($cashstatus == 'from')
            $dropdown = '<select class="pod has-search" name="cash_branch_id" id="cash_branch_id">';
        else
            $dropdown = '<select class="pod has-search" name="cash_to_branch_id" id="cash_to_branch_id">';



        if ($query->num_rows() > 1) {

            $dropdown .= '<option value="   " >' . lang('Choose') . '</option>';
        }

        foreach ($result as $row) {

            //echo "<pre>";
            //echo $row->branchname;
            //echo $j = unserialize($row->branchname);
            //echo $j = json_decode($row->branchname);
            //print_r($j);
            //print_r($row->branchname);

            $dropdown .= '<option value="' . $row->id . '" ';

            /* if ($value == $row->id) {

              $dropdown .= 'selected="selected"';

              } */



            //echo "<pre>";
            //print_r($com);

            $dropdown .= '>' . $row->branch_cash_name . '</option>';
        }

        $dropdown .= '</select>';

        //echo($dropdown);
    }

}

function store_items_quan($id = '', $pid = '') {

    $CI = & get_instance();

    if ($id) {

        $ex = "where si.store_id='$id'";

        if ($pid) {

            $ex.=" and si.item_id='$pid'";
        }
    } else {

        $ex = "";
    }

    /* $sql = "SELECT 

      s.*,

      bs_company_branch.branchid,

      bs_company_branch.branchname,

      bs_company.companyid,

      bs_company.company_name,

      ssquantity,

      SUM(ssi.`quantity`) AS squantity

      FROM

      `bs_store` AS s



      LEFT JOIN bs_store_items AS ssi ON ssi.`store_id`=s.`storeid`



      LEFT JOIN

      (SELECT

     * ,

      SUM(soldi.`soled_quantity`) AS ssquantity

      FROM

      `store_soled_items` AS soldi) ss

      ON `ss`.`store_id` = '$id' and ss.item_id='$pid'



      LEFT JOIN bs_company

      ON bs_company.companyid = s.companyid

      LEFT JOIN bs_company_branch

      ON bs_company_branch.branchid = s.branchid



      $ex



      GROUP BY s.`storeid`

      ORDER BY s.`storeid` DESC  ";

     */



    $sql = "SELECT 

                    si.*,

                    i.*,

                    ss.`store_id`,

                    ss.`item_id`,

                    u1.userid,

                    u1.fullname,

                    ss.soled_quantity,

                    SUM(si.`quantity`) squantity ,

                    ssquantity,

                    s.`storeid`,

                    s.`storename` 

                  FROM

                    `bs_store_items` AS si 

                    LEFT JOIN `bs_item` AS i 

                      ON i.`itemid` = si.`item_id` 

    

                    LEFT JOIN `bs_store` AS s 

                        ON `s`.`storeid` = `si`.`store_id`     



                    LEFT JOIN `bs_users` AS u1 

                      ON `u1`.`userid` = `si`.`supplier_id` 

                    LEFT JOIN 

                      (SELECT 

                        *,

                        SUM(soldi.`soled_quantity`) AS ssquantity 

                      FROM

                        `store_soled_items` AS soldi 

                      WHERE soldi.`store_id` = '$id' AND soldi.`is_cancel` = '0' GROUP BY soldi.`item_id`) ss 

                      ON `ss`.`store_id` = si.`store_id` 

                      AND ss.`item_id` = si.`item_id` 



                        

                      $ex

 

                  GROUP BY si.`item_id`  ";



    $q = $CI->db->query($sql);

    $rdata = $q->result();

    $rdat = 0;

    foreach ($rdata as $rdat) {



        $squantity+=$rdat->squantity;

        $ssquantity+=$rdat->ssquantity;

        $rdat = $squantity - $ssquantity;
    }

    return $rdat;
}

if (!function_exists('files_categories')) {



    function files_categories($name, $value) {





        $CI = & get_instance();

        $memtype = get_member_type();

        $CI->db->select('*');

        $CI->db->from('files_categories');



        $CI->db->order_by("fid", "ASC");

        $query = $CI->db->get();



        $dropdown = '<select class="pod has-search" name="' . $name . '" id="' . $name . '">';

        $dropdown .= '<option value="   ">' . lang('choose') . '</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->fid . '" ';

            if ($value == $row->fid) {

                $dropdown .= 'selected="selected"';
            }



            $dropdown .= '>' . $row->ftitle . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

function strafter($string, $substring) {

    $pos = strpos($string, $substring);

    if ($pos === false)
        return $string;
    else
        return(substr($string, $pos + strlen($substring)));
}

if (!function_exists('all_users')) {



    function all_users($name, $value, $language, $function = '') {

        $CI = & get_instance();

        $CI->db->select('userid,fullname');

        //$CI->db->where('status', 'Active');

        $CI->db->from('bs_users');

        $query = $CI->db->get();



        $dropdown = '<select name="' . $name . '" id="' . $name . '" ' . $function . '  class="js-example-basic-single region_class required  valid "  language="' . $language . '">';

        //$dropdown = '<select name="">';

        if ($language == 'english')
            $dropdown .= '<option value="">Select</option>';
        else
            $dropdown .= '<option value="">اختر</option>';



        foreach ($query->result() as $row) {



            $dropdown .= '<option data-serial="' . $row->fullname . '" value="' . $row->userid . '" ';

            if ($value == $row->userid) {

                $dropdown .= 'selected="selected"';
            }



            $dropdown .= '>' . $row->fullname . '</option>';
        }

        $dropdown .= '</select>';

        echo($dropdown);
    }

}

if (!function_exists('maxjoin')) {

    function maxjoin() {
        $CI = & get_instance();
        $result = $CI->db->query("SET SQL_BIG_SELECTS=1");
    }

}

function get_unit($id) {

    $CI = & get_instance();

    $CI->db->select('unit_id,unit_title');

    $CI->db->where('unit_id', $id);

    $CI->db->from('units');

    $query = $CI->db->get();

    return _s($query->row()->unit_title, get_set_value('site_lang'));
}

function continue_process($coupon, $order_items, $prices, $quantities) {
    $CI = & get_instance();
    $ret = [];
    $coupon_id = $coupon['coupon_id'];
    //echo $coupon[0]['coupon_id'];
    if ($coupon['item_id']) {
        $coupon_items = explode(',', $coupon['item_id']);
    } else {
        if ($coupon['category_id']) {
            $query = "select itemid from bs_item where categoryid = " . $coupon['category_id'];
        } else {
            $query = "select itemid from bs_item";
        }
        $sql = $CI->db->query($query);
        $result = $sql->result();
        $coupon_items = [];
        foreach ($result as $row) {
            array_push($coupon_items, $row->itemid);
        }
    }
    $result = array_intersect($order_items, $coupon_items);
    $total_discount = 0;
    foreach ($result as $key => $value) {
        $discount = (floatval($coupon['value']) / 100) * $prices[$key];
        $total_discount += ($discount * $quantities[$key]);
        $ret[$value] = ($prices[$key] - $discount) * $quantities[$key];
    }
    return [$ret, $total_discount, $coupon_id];
}

function pocess_coupon($coupon, $order_items, $prices, $quantities) {
    $CI = & get_instance();
    if (count($coupon) > 0) {
        $coupon_id = $coupon['coupon_id'];
        $total_discount = 0;
        if ($coupon['dis_type'] === 'omr') {
            $total_discount = $coupon['value'];
            return [[], $total_discount, $coupon_id];
        } else {
            return continue_process($coupon, $order_items, $prices, $quantities);
        }
    } else {
        return [[], 0];
    }
}

function insertRawData($arr, $itemid, $bs_userid, $insertid, $storeID, $qty) {
    $CI = & get_instance();
        foreach ($arr as $item) {
            $data = array(
                'item_id' => $itemid,
                'raw_id' => $item->raw_id,
                'soled_quantity' => $item->qty,
                'store_id' => $storeID,
                'userid' => $bs_userid,
                'recipt_id' => $insertid,
                'is_cancel' => 0
            );
            $CI->db->insert('raw_soled_items', $data);
            $query = $CI->db->get_where('store_value', array('item_id' => $item->raw_id));
            if ($query->num_rows() > 0) {
                $quantity = $item->qty * $qty;
                $id = $query->row()->item_id;
                $query = "update store_value set totalq = totalq-$quantity where item_id = $id and store_id = $storeID";
                $CI->db->query($query);
            }
        }
    }
    
    if ( ! function_exists('multidimensional_search_array')) {
    function multidimensional_search_array($parents, $searched) {
        $result = array();
        if (empty($searched) || empty($parents)) {
            return false;
        }
        //        echo "<pre>";
        ///print_r($parents);
        //exit;
        foreach ($parents as $key => $value) {
            $exists = true;
            foreach ($searched as $skey => $svalue) {
                //  echo   $key;
                //echo $skey;
                //echo $parents[$key]->$skey;
                //echo $svalue;
                //echo $parents[$key][$skey];
                $exists = ($exists && IsSet($parents[$key][$skey]) && $parents[$key][$skey] == $svalue);
                if($exists)
                    $result[] = $key;

            }

        }

        if($exists){ return $result; }
        return $result;
    }
}

// ------------------------------------------------------------------------

/* End of file language_helper.php */

/* Location: ./system/helpers/language_helper.php */