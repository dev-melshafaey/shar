<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Language Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/language_helper.html
 */

// ------------------------------------------------------------------------

/**
 * Lang
 *
 * Fetches a language variable and optionally outputs a form label
 *
 * @access	public
 * @param	string	the language line
 * @param	string	the id of the form element
 * @return	string
 */
if ( ! function_exists('suppliers_popup'))
{
	function suppliers_popup()
	{
			$html = '<div id="osx2-modal-content">';
			$html .= '<form name="frm_suppliers" action="'.base_url().'inventory/add_suppliers" id="frm_suppliers" method="post" autocomplete="off">';
			$html .= '<input type="hidden" id="ownerid" class="owner_id" name="ownerid" value="'.ownerid().'" />';
			$html .= '<div id="osx2-modal-title">Add New Supplier</div>';
			$html .= '<div class="close"><a href="#" class="simplemodal-close">x</a></div>';
			$html .= '<div id="osx2-modal-data">';
			$html .= '<div class="raw form-group">';
			$html .= '<div class="form_title">Supplier Name</div>';
			$html .= '<div class="form_field">';
			$html .= '<input name="suppliername" id="suppliername" type="text"  class="formtxtfield"/>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="raw form-group">';
			$html .= '<div class="form_title">Phone  Number</div>';
			$html .= '<div class="form_field">';
			$html .= '<input name="phone_number" id="phone_number" type="text"  class="formtxtfield"/>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="raw form-group">';
			$html .= '<div class="form_title">Fax  Number</div>';
			$html .= '<div class="form_field">';
			$html .= '<input name="fax_number" id="fax_number" type="text"  class="formtxtfield"/>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="raw form-group">';
			$html .= '<div class="form_title">Office Number</div>';
			$html .= '<div class="form_field">';
			$html .= '<input name="office_number" id="office_number" type="text"  class="formtxtfield"/>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="raw form-group">';
			$html .= '<div class="form_title">Notes</div>';
			$html .= '<div class="form_field">';
			$html .= '<textarea name="notes" id="notes" cols="" rows="" class="formareafield"></textarea>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="raw form-group" align="center">';
			$html .= '<input name="submit_supplier" type="submit" class="submit_btn" value="Submit" />';
			$html .= '<input name="" type="reset" class="reset_btn" value="Reset" />';
			$html .= '</div>';
			$html .= '<!--end of raw--> ';
			$html .= '</div>';
			$html .= '</form> ';    
			$html .= '</div>';

			echo $html;
	}
}

// ------------------------------------------------------------------------
/* End of file language_helper.php */
/* Location: ./system/helpers/language_helper.php */