<?php
require __DIR__ . '/../autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

if (isset($_GET)) {
    //  print_r($_GET);
    $invid = $_GET['invid'];
}
$link = mysqli_connect('localhost', 'root', '');
$db = mysqli_select_db($link,'mishkak_new');

function multidimensional_search($parents, $searched) {
    //print_r($parents);
    //print_r($searched);

    $return = array();
    foreach ($parents as $key => $value) {
        $exists = true;
        foreach ($searched as $skey => $svalue) {
            $exists = ($exists && IsSet($parents[$key][$skey]) && $parents[$key][$skey] == $svalue);
        }
        if ($exists) {
            $return[] = $key;
            return $return;
        }
    }

    return false;
}

$sql = "SELECT
  adval,
  aditemsnames,
   aditemsnamesar,
    u.`fullname` AS refno,
	u2.`fullname` AS uname,
  it.`inovice_product_id`,
  o.*,
  item.*,
  it.*,u.fullname as csname
FROM
  `orders` AS o
  INNER JOIN `an_invoice` AS i
    ON i.`invoice_id` = o.`invoice_id`
  INNER JOIN `bs_invoice_items` AS it
    ON it.`invoice_id` = i.`invoice_id`
  INNER JOIN `bs_users` AS u
  ON u.`userid` = i.customer_id
  LEFT JOIN
    (SELECT
      pai.pos_item_id,
      pai.`invoice_itempk_id`,
      GROUP_CONCAT(pai.`additional_item_val`) AS adval,
      GROUP_CONCAT(ai.`item_eng_name`) AS aditemsnames,
      GROUP_CONCAT(ai.`item_ar_name`) AS aditemsnamesar
    FROM
      `pos_additional_items` AS pai
      INNER JOIN `additional_items` AS ai
        ON ai.`ad_item_id` = pai.`additional_item_id`
    GROUP BY pai.`invoice_itempk_id`) AS poi
    ON poi.invoice_itempk_id = it.`inovice_product_id`
  INNER JOIN `bs_item` AS item
    ON item.`itemid` = it.`invoice_item_id`
          INNER JOIN `bs_users` AS u2
    ON u2.`userid` = i.`user_id`
WHERE o.`invoice_id` = '$invid'
ORDER BY o.`order_id` ASC";
//echo $sql;
$result = mysqli_query($link,$sql) or die(mysql_error());
$invItems = array();
//echo "<pre>";
while ($row = mysql_fetch_assoc($result)) {
    // print_r($row);
    $invproid = $row['inovice_product_id'];

    $invItems[] = $row;

    $sql = "SELECT
  ai.*
FROM
  `more_items` AS mi
  LEFT JOIN `link_items` AS li
    ON li.`link_id` = mi.`link_id`
  LEFT JOIN `additional_items` AS ai
    ON ai.`ad_item_id` = li.`additional_item_id`
WHERE mi.`invoice_item_id` = '" . $invproid . "'";
//echo $sql;

    $result2 = mysql_query($sql) or die(mysql_error());
    while ($row2 = mysql_fetch_assoc($result2)) {
        $invmoreItems[$invproid][] = $row2;
    }
}

//echo "<pre>";
//print_r($invmoreItems);
//exit;

$sql = "SELECT * FROM `pos_additional_items` as posa
inner join additional_items as ad on ad.ad_item_id = posa.`additional_item_id`
WHERE posa.`invoice_id` = '$invid'";
$result = mysql_query($sql) or die(mysql_error());
$addinvItems = array();
while ($row = mysql_fetch_assoc($result)) {
    //print_r($row);
    $addinvItems[] = $row;
}

foreach ($invItems as $inv) {
    $invit = $inv['inovice_product_id'];
    $searched = array('invoice_itempk_id' => $invit);
    //print_r($searched);
    $ar = multidimensional_search($addinvItems, $searched);
    //print_r($ar);
}
//print_r($addinvItems);
//exit;
//echo "<pre>";
//print_r($invItems);
//exit;
/* Fill in your own connector here */
$connector = new NetworkPrintConnector("192.168.1.10", 9100);
//print_r($connector);
/* Information for the receipt */
if (!empty($invItems)) {
    $totalq = 0;
    $totalp = 0;
    foreach ($invItems as $i => $itmData) {

        $itemnames = $itmData['itemname'];

        $data = preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $itemnames);
        $tt = unserialize($data);
        $itemname = $tt['english'];
        echo $itproid = $itmData['inovice_product_id'];
        $itemq = $itmData['invoice_item_quantity'];
        $totalq = $totalq + $itemq;
        $itemtp = $itmData['invoice_item_price'] * $itemq;
        $totalp = $totalp + $itemtp;
        $item_data = new item($itemname, $itemq, number_format($itemtp, 3, '.', ''));
        $items [] = $item_data;
        $invit = $inv['inovice_product_id'];

        $searched = array('invoice_itempk_id' => $invit);
        $ar = multidimensional_search($addinvItems, $searched);
        if (!empty($ar)) {
            foreach ($ar as $adit) {
                //echo $adit;
                //print_r($addinvItems[$adit]);
                //	exit;
                $aditmname = $addinvItems[$adit]['item_eng_name'];
                $aditmval = $addinvItems[$adit]['additional_item_val'];
                //$aditmname = $adit['item_name'];
                //$aditmval  = $adit['item_val'];
                $item_data = new item($aditmname, $aditmval, '');
                $items [] = $item_data;
            }
        }

        if (isset($invmoreItems[$itproid])) {
            //$additemsname = $additionalItems[$tmid]['item_name'];
            //$additemsval = $additionalItems[$tmid]['item_val'];
            //$additemsprice = $additionalItems[$tmid]['item_price'];
            $additemsname = $invmoreItems[$itproid];
            //print_r($additemsname);
            //$additemsval = $additionalItems[$tmid];
            // $additemsprice = $additionalItems[$tmid];
            // $additemsval = $additionalItems[$tmid]['item_val'];

            foreach ($additemsname as $adit) {
                $aditmname = $adit['item_eng_name'];
                //$aditmval  = $adit['item_val'];
                $item_data = new item($aditmname, '', '');
                $mitems [$i][] = $item_data;
            }
        }


        //exit;
        //multidimensional_search($parents, $searched);
    }
}
//$subtotal = new item('Subtotal', '12.95');
//$tax = new item('A local tax', '1.30');
$total = new item('Total', $totalq, number_format($totalp, 3, '.', ''));
/* Date is kept the same for testing */
$date = date('l jS \of F Y h:i:s A');
//$date = "Monday 6th of April 2015 02:56:25 PM";

/* Start the printer */
//$logo = EscposImage::load("resources/escpos-php.png", false);
$printer = new Printer($connector);

/* Print top logo */
$printer->setJustification(Printer::JUSTIFY_CENTER);
//$printer -> graphics($logo);
$printer->selectPrintMode(Printer::MODE_FONT_A);

/* Name of shop */
$printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
$printer->text("Shawarma Factory \n");
$printer->selectPrintMode();
$printer->text("Tell: 71141147\n");
$printer->feed();

/* Title of receipt */
$printer->setEmphasis(true);
//$printer -> text("SALES INVOICE\n");
$printer->setEmphasis(false);

/* Items */
$printer->setJustification(Printer::JUSTIFY_LEFT);
$printer->setEmphasis(true);
$d = date('d-m-Y', strtotime($invItems[0]['create']));
$tm = date('H:i:s', strtotime($invItems[0]['create']));
$printer->text(new item('Date: ' . $d, '', $tm));
$printer->setEmphasis(true);
$printer->text("------------------------------------------------\n");
//$printer -> text(new item('-------', '--------'));
$printer->setEmphasis(true);
//$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer->text("Invoice No: " . $invItems[0]['invoice_id'] . "\n");
$printer->setJustification(Printer::JUSTIFY_LEFT);
$printer->setEmphasis(true);
$printer->text(new item('Ref: ' . $invItems[0]['refno'], '', ''));
$printer->setEmphasis(true);
$printer->text(new item('Serverd By: ' . $invItems[0]['uname'], '', ''));
$printer->setEmphasis(true);

$printer->text("------------------------------------------------\n");
//$printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
$printer->selectPrintMode();
$printer->setEmphasis(false);

$printer->setEmphasis(true);
$printer->text(new item("Name", "Q", "P"));
$printer->text("------------------------------------------------\n");
foreach ($items as $i => $item) {

    //print_r($mitems[$i]);
    if (isset($mitems[$i]) && $mitems[$i] != "") {
        $printer->text($item);
        $mit = $mitems[$i];
        foreach ($mit as $it) {
            $printer->text($it);
        }
        $printer->feed();
        $printer->text("------------------------------------------------\n");
    } else {
        $printer->text($item);
        $printer->feed();
        $printer->text("------------------------------------------------\n");
    }
}
$printer->setEmphasis(true);
//$printer -> text($subtotal);
$printer->setEmphasis(false);
$printer->feed();

/* Tax and total */
//$printer -> text($tax);
$printer->text("------------------------------------------------\n");
//$printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
$printer->text($total);
$printer->selectPrintMode();

/* Footer */
$printer->feed(2);
$printer->setJustification(Printer::JUSTIFY_CENTER);
$printer->text("Thank you & Come Again \n");
//$printer -> text("For trading hours, please visit example.com\n");
$printer->feed(2);
//$printer -> text($date . "\n");

/* Cut the receipt and open the cash drawer */
$printer->cut();
$printer->pulse();

$printer->close();

/* A wrapper to do organise item names & prices into columns */

class item {

    private $name;
    private $price;
    private $dollarSign;

    public function __construct($name = '', $size = '', $price = '') {
        $this->name = $name;
        $this->price = $price;
        $this->size = $size;
    }

    public function __toString() {
        $rightCols = 10;
        $leftCols = 28;
        if ($this->dollarSign) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this->name, $leftCols);
        $middle = str_pad($this->size, $rightCols);
        $right = str_pad($this->price, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$middle$right\n";
    }

}
