/* global config */

// JavaScript Document


function checkEnterPressed(obj, event) {
    //console.log(obj, event.keyCode);
    //we are checking for ENTER key here, 
    //you can check for other key Strokes here as well
    if (event.keyCode === 13) {
        alert("ENTER key!");
    }


}

//calculae Price
function calculatePrice() {
    qntity = $("#quantity").val();
    pProduct = $("#total_price").val();
    totalPrice = qntity * pProduct;
    $("#item_total_price").val(totalPrice);

}

//get Product data
function getProductData(product_id)
{
    if (product_id != "") {
        $.ajax({
            url: config.BASE_URL + "ajax/getProductData",
            dataType: 'json',
            type: 'post',
            data: {pid: product_id},
            cache: false,
            success: function (data)
            {
                //console.log('data');
                dd = data;
                console.log(dd);

                // dd.product.sale_price;

                //mi_descr
                purchase_price = dd.product.purchase_price;
                min_sale_result = (dd.product.min_sale_price / 100) * purchase_price;
                sale_result = (dd.product.sale_price / 100) * purchase_price;

                sale_result = Number(purchase_price) + Number(sale_result);
                min_sale_result = Number(purchase_price) + Number(min_sale_result);

                $("#min_price").val(min_sale_result.toFixed(2));
                $("#total_price").val(sale_result.toFixed(2));
                $("#item_total_price").val(sale_result.toFixed(2));
                $("#serialnumber").val(dd.product.serialnumber);
                $("#quantity").val(1);
                //$("#price_product").val(dd.product.sale_price);
                
                
                $("#product_comment").val(dd.product.notes);
                //`notes`
                // $( "#branches" ).fadeOut( "slow" );

            }
        });
    }
}



///confrlm remove
function confirmRemove(id) {
    var r = confirm("Are you Sure You wnt to remove ?");
    if (r == true) {
        val = $("#productTotal" + id).val();
        //alert(val);
        removeValueFromNet(val);
        alterNetTotal();
        $("#tr_" + id).remove();
        $(".pi" + id).remove();
        calculateNetTotal();
        calculateProfitLoss();
        //array.splice(index, 1)
    } else {
        txt = "You pressed Cancel!";
    }
}

//for table data which has been added to the table 
function calculateCart(obj, id) {
    console.log(obj);
    console.log(id);
    ob = obj;
    //obj.id
    //var val = $("#quantity").text();
    var val = $("#tr_"+id+" #"+obj.id+"").html();

    //alert(val);
    if (obj.id == 'quantity') {
        quantity = val;
        //$("#tr_"+id+" #"+obj.id+"").html();
        //geteditProductData(id,quantity,'quantity');
        calculateTableProductData(id, quantity, 'quantity');
    }


}



function updateInput(obj) {


    $('#productQuantity' + obj.productId).val();
    $('#productTotal' + obj.productId).val();
    $('#productDiscount' + obj.productId).val();
    $('#productDiscription' + obj.productId).val();
    $('#productNotes' + obj.productId).val();

}
//discountType: "1"discription: ""id: 1length: 0minimum: "22.00"notes: "23.00"perPrice: "23.00"quantity: "2 "total: 46totalWithQ: 46__proto__: Array[0] function.js:110
function updateRow(nn) {
    console.log(nn);
    cv = nn;

    if (nn.discountType == 1) {
        //dscType = " %";
        dscType ="";
    }
    else {
        //dscType = "OMR";
        dscType ="";
    }

    if (nn.discount != "") {
    }
    else {
        discount = 0;
    }
    disc = discount + dscType;

    $('#tr_' + nn.id + ' #quantity').html(nn.quantity);
    $('#tr_' + nn.id + ' #totalAmount').html(nn.totalWithQ);
    $('#tr_' + nn.id + ' #discount').html(disc);
    //$('#tr_'+nn.id+' #discount').val();
    $('#tr_' + nn.id + ' #description').html(nn.discription);
    $('#tr_' + nn.id + ' #salePrice').html(nn.salePrice);
    
    
    $('#netTotal').html(nn.salePrice);
    $('#totalnet').val(nn.salePrice);

}


//get  table procut data to eidit
function geteditProductData(id) {

    //productQuantity"+obj.productId+";
    //alert($("#productQuantity"+id).val());
    //alert($("#productQuantity"+id).val());
    /*
    editArray['qid'] = id;
    editArray['qquantity'] = $("#productQuantity" + id).val();
    editArray['qdiscount'] = $("#productDiscount" + id).val();
    editArray['qdiscountType'] = $("#productDiscountType" + id).val();
    editArray['qdiscription'] = $("#productDiscription" + id).val();
    editArray['qperPrice'] = $("#productPerPrice" + id).val();
    editArray['qnotes'] = $("#productTotal" + id).val();
    editArray['qminimum'] = $("#minimum" + id).val();
    */
    editArray['id'] = id;
    editArray['quantity'] = $('#tr_' + id + ' #quantity').text();
    editArray['discount'] = $('#tr_' + id + ' #discount').text();
    editArray['discountType'] = $('#tr_' + id + ' #discountType').text();
    editArray['description'] = $('#tr_' + id + ' #description').text();
    editArray['perPrice'] = $('#tr_' + id + ' #totalAmount').text();
    editArray['notes'] = $('#tr_' + id + ' #notes').text();
    editArray['minimum'] = $('#tr_' + id + ' #minimum').text();
    editArray['salePrice'] = $('#tr_' + id + ' #totalAmount').text();
    
    
    
    //alert(id);
    /*editArray['quantity'] = 5;
    editArray['discount'] = 1;
    editArray['discountType'] = 2;
    editArray['discription'] = 22;
    editArray['perPrice'] = 29;
    editArray['notes'] = 22;
    editArray['minimum'] = 12;
    */

    return editArray;
    //productPerPrice"+obj.productId+"



}


function maintenaceHml(mainten) {
    //mainten
    htm = '<div class="invoice_raw" style="padding: 20px;margin-top:8px;"><div class="invoice_mant"><strong>' + mainten + '</strong></div>';
    htm += '<div class="invoice_mant"><strong>Every</strong><div class="formtxtfield_filter_small"><input name="maintanaceTime[]" id="maintanaceTime[]" type="text"  class="filter_field_small"/>';
    htm += '<div class="ui-select_filter"><div class="styled-select_filter"><select name=""><option selected="selected" >Days </option><option value="weeks">Weeks</option><option value="months">Months</option><option value="years">Years</option></select></div></div></div>';
    htm += '<div class="invoice_mant"><strong>From : </strong>';
    htm += '<input name="" type="text"  class="formtxtfield from_date form-control" style="width:110px;"/>';
    htm += ' <div class="date_icon" style=""><a href="#"><img src="' + config.BASE_URL + 'images/internal/date_icon.png" width="22" height="24" border="0" /></a></div></div>';
    htm += '<div class="invoice_mant"><strong>Maintenance Period  : </strong><div class="formtxtfield_filter_small"><input name="" type="text"  class="filter_field_small"/><div class="ui-select_filter"><div class="styled-select_filter"><select name=""><option selected="selected" >Days </option><option value="weeks">Weeks</option><option value="months">Months</option><option  value="years">Years</option></select></div></div></div></div></div>';
    $("#product_maintenance").append(htm);
    $(".from_date").datepicker({
        defaultDate: "today",
        changeMonth: true,
        numberOfMonths: 1,
    });

}


function addMaintenance(id, mobj) {

    ck = $(mobj).is(":checked");
    //alert(dv);
    b = mobj;
    //b.attr("class");
    //$(mobj +".mainten").
    //$("#tr_"+id)
    name = $("#tr_" + id + " td").eq(2).html();
    if (ck)
        //maintenaceHml(id);
        if ($('#product_maintenance').css('display') == 'none') {
            $('#product_maintenance').show();
        }
    maintenaceHml(name);
    $('html,body').animate({scrollTop: $(".mant_title").offset().top}, 800);

}
//calculate only table prduct discount
function calculateTableProductData(id, value, tp) {
	
    //alert(geteditProductData(id));
    var editArray = geteditProductData(id);
    console.log(editArray);
    
    //geteditProductData(id);
    
    if (tp == "quantity") {
        editArray['quantity'] = value;
    }

    quantity = editArray['quantity'];
    discount = editArray['discount'];
    type = editArray['discountType'];
    total = editArray['perPrice'];
    minimum = editArray['minimum'];
    description = editArray['description'];
    total = total * quantity;
    //editArray['totalWithQ'] = total;
    if (type == 1) {
        //alert(discount);
        if (discount != "") {
            final = parseInt(total) * parseInt(discount);
            //alert(final);
            final = final / 100;
            //alert(final);
            final_total = total - final;

        }
        else {
            final_total = total;
        }
    }else {
        if (discount != "") {

            
            
            if (parseInt(discount) < total ) {
                //om = total / discount;
                //final = om*100;
                //alert(parseInt(discount));
                //alert(total);
                final_total = total - parseInt(discount);
                
            }else {
                alert("Discount is greater then total");
            }
            
        }
        else {
            final_total = total;
        }

    }

    
    editArray['totalWithQ'] = total;
    editArray['total'] = total;
    editArray['totalnet'] = final_total;
    editArray['netTotal'] = final_total;
    editArray['salePrice'] = final_total;
    //console.log(editArray);
    updateRow(editArray);
    updateInput(editArray);
    //alert(final_total);
    //editArray['totalPrice'] = final_total;

}

// add new row in the table
function addrow(productData) {
    tbHtml = '';
    console.log(productData);
    //console.log('productData');
    type = '';
    if (productData.discountType == '1') {
        if (productData.discount != "")
            disc = productData.discount + '%';
        else
            disc = '0 %';
    }
    else
    {

        if (productData.discount != "")
            disc = productData.discount + 'RO';
        else
            disc = productData.discount + 'RO';
    }

    totalProductAmount = productData.total * productData.quantity;
    //productData.storeId;
    rowCounter++;


    tbHtml += '<tr id="tr_' + prdocutCounter + '"><td >' + rowCounter + '</td>';
    tbHtml += '<td ><img src="' + config.BASE_URL + '/uploads/item/' + productData.productImage + '" width="50" height="50" /></td>';
    tbHtml += '<td style="text-align:center"> ' + productData.productName + ' </td>';
    tbHtml += '<td id="quantity" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculateCart(this,' + prdocutCounter + ')">' + productData.quantity + ' </td>';
    tbHtml += '<td id="description" contentEditable="true">' + productData.discription + ' </td>';
    tbHtml += '<td id="totalAmount" style="text-align:center" contentEditable="true">' + totalProductAmount + '</td>';
    tbHtml += '<td id="discount" style="text-align:center" contentEditable="true">' + disc + '</td>';
    tbHtml += '<td id="salePrice" style="text-align:center" contentEditable="true">' + productData.salePrice + '</td>';
    tbHtml += '<td style="text-align:center;cursor:pointer;" ><img src="' + config.BASE_URL + '/images/internal/delete_small.png" width="7" height="8" border="0"   onclick="confirmRemove(' + prdocutCounter + ')"/></td>';
    tbHtml += '<td align="center" valign="middle" style="text-align:center"><input class="mainten" name="editmainten" id="editmainten" type="checkbox" value="" onchange="addMaintenance(' + productData.productId + ',this)" /></td>';
    tbHtml += '</tr>';

    netTotals[netCouner] = productData.salePrice;
    netCouner++;

    alterNetTotal();
    //$("#product_list").append(tbHtml);
    //console.log(tbHtml);
    $('#product_list tr.grand-total').before(tbHtml);

    $("#productid").val('');
    $("#productname").val('');
    $("#serialnumber").val('');
    $("#min_price").val('');
    $("#quantity").val('');
    $("#total_price").val('');
    $("#item_total_price").val('');
    $("#imageProduct" + pid).val();
    $("#nameProduct" + pid).val();
    $("#item_discount").val('');
    $("#product_comment").val('');
    $("#product_notes").val('');
    
    
    
}

function calculatePaymentData() {
    //alert('asdasd');	
    total_pay = 0;
    if (jQuery('[name*="products[pay]"]').length > 0) {
        jQuery('[name*="products[pay]"]').each(function (index, value) {
            //var option = jQuery( this );
            //alert(index);
            //alert(value);
            //paymentValue = $(this).eq(index).val();
            pValue = jQuery('[name*="products[pay]"]').eq(index).val();
            total_pay = Number(total_pay) + Number(pValue);
            //alert(pValue);

            //alert(remainingAmount);
            //console.log(paymentValue+'paymentValue');
            //console.log(value+'value');
        })
    }
    $("#receiverd_amount").val(total_pay);
}

function addpurchaserow(productData) {
    tbHtml = '';
    console.log(productData);
    //console.log('productData');
    type = '';

    totalProductAmount = productData.unit_price * productData.quantity;
    //productData.storeId;
    rowCounter++;


    html = "<input type='text' class='pay" + rowCounter + " payment' id='productId" + rowCounter + "'  name='products[pay][]' value='' onchange='calculatePaymentData(" + prdocutCounter + ")'>";

    tbHtml += '<tr id="tr_' + prdocutCounter + '"><td >' + rowCounter + '</td>';
    tbHtml += '<td ><img src="' + config.BASE_URL + '/uploads/item//' + productData.productImage + '" width="50" height="50" /></td>';
    tbHtml += '<td style="text-align:center"> ' + productData.productName + ' </td>';
    tbHtml += '<td id="quantity" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculateCart(this,' + productData.productId + ')">' + productData.quantity + ' </td>';
    tbHtml += '<td id="description" contentEditable="true">' + productData.discription + ' </td>';
    tbHtml += '<td id="totalAmount" style="text-align:center">' + totalProductAmount + '</td>';
    tbHtml += '<td style="text-align:center;cursor:pointer;" ><img src="' + config.BASE_URL + '/images/internal/delete_small.png" width="7" height="8" border="0"   onclick="confirmRemove(' + prdocutCounter + ')"/></td>';
    tbHtml += '<td align="center" valign="middle" style="text-align:center"><input class="mainten" name="editmainten" id="editmainten" type="checkbox" value="" onchange="addMaintenance(' + productData.productId + ',this)" /></td>';
    tbHtml += '</tr>';

    netTotals[netCouner] = totalProductAmount;
    netCouner++;
    alterNetTotal();
    //$("#product_list").append(tbHtml);
    //console.log(tbHtml);
    $('#product_list tr.grand-total').before(tbHtml);

    $("#productid").val('');
    $("#quantity").val('');
    $("#total_price").val('');
    $("#item_total_price").val('');
    $("#imageProduct" + pid).val();
    $("#nameProduct" + pid).val();
    $("#item_discount").val('');
    $("#product_comment").val('');
    $("#product_notes").val('');
}

//remove vlaue from net total
function removeValueFromNet(val) {
    ind = netTotals.indexOf(val);
    // alert(ind);
    netTotals.splice(ind, 1);
}

// change the value of net total
function alterNetTotal() {
    totalLength = netTotals.length;
    //alert(totalLength);
    if (totalLength > 0) {
        total_value = 0;
        for (a = 0; a < totalLength; a++) {
            //alert(netTotals[a]);
            total_value = Number(total_value) + Number(netTotals[a]);

        }
        //parseInt(total_value);
        $("#netTotal").html(Number(total_value) + " RO.");
        $("#totalnet").val(total_value);
    }
    else {
        $("#netTotal").html("0 RO.");
    }
}


///calculate net total
function calculateNetTotal() {
    discountAmount = $("#totalDiscount").val();
    total_value = $("#totalnet").val();
    net_total = total_value - discountAmount;
    $("#net_total").html(net_total);
    $("#rest_amount").html('');
    $("#receiverd_amount").val('');
}

//caculate rest amount
function calculatePurchaseRestAmount() {

    receiveAmount = $("#receiverd_amount").val();
    rest_amount = total_value - receiveAmount;
    rest_amount = rest_amount.toFixed(2);
    $("#rest_amount").html(rest_amount + " RO");
}

function calculateRestAmount() {

    receiveAmount = $("#receiverd_amount").val();
    t_net = $("#net_total").text();
    //t_net = $("#totalnet").val();
    rest_amount = t_net - receiveAmount;
    $("#rest_amount").html(rest_amount + " RO");
}


netCouner = 0;
//adding new record in array
function addInput(obj) {
    //alert(counter);	
    // var newdiv = document.createElement('div');
    html = "<input type='hidden' class='pi" + obj.productId + "' id='productId" + obj.productId + "'  name='products[productId][]' value='" + obj.productId + "'>";
    //document.getElementById(divName).appendChild(newdiv);
    $(".invoice_raw").append(html);

    html = "<input type='hidden' class='pi" + obj.productId + "' id='productQuantity" + obj.productId + "'  name='products[quantity][]' value='" + obj.quantity + "'>";
    $(".invoice_raw").append(html);

    html = "<input type='hidden' class='pi" + obj.productId + "' id='productTotal" + obj.productId + "'  name='products[productTotal][]' value='" + obj.salePrice + "'>";
    $(".invoice_raw").append(html);

    html = "<input type='hidden' class='pi" + obj.productId + "' id='minimum" + obj.productId + "'  name='products[minimum][]' value='" + obj.minPrice + "'>";
    $(".invoice_raw").append(html);

    html = "<input type='hidden' class='pi" + obj.productId + "' id='productPerPrice" + obj.productId + "'  name='products[productperPrice][]' value='" + obj.total + "'>";
    $(".invoice_raw").append(html);

    html = "<input type='hidden' class='pi" + obj.productId + "' id='productDiscount" + obj.productId + "'  name='products[productDiscount][]' value='" + obj.discount + "'>";
    $(".invoice_raw").append(html);
    html = "<input type='hidden' class='pi" + obj.productId + "' id='productDiscountType" + obj.productId + "'  name='products[productDiscountType][]' value='" + obj.discountType + "'>";
    $(".invoice_raw").append(html);
    html = "<input type='hidden' class='pi" + obj.productId + "' id='productDiscription" + obj.productId + "'  name='products[productDiscription][]' value='" + obj.discription + "'>";
    $(".invoice_raw").append(html);
    html = "<input type='hidden' class='pi" + obj.productId + "' id='productNotes" + obj.productId + "'  name='products[productNotes][]' value='" + obj.note + "'>";
    $(".invoice_raw").append(html);

    html = "<input type='hidden' class='pi" + obj.productId + "' id='supplier" + obj.supplier_id + "'  name='products[supplier_id][]' value='" + obj.supplier_id + "'>";
    $(".invoice_raw").append(html);

    counter++;
}





//calculate discout for slected new table
function calculateDis() {
    minimum = $("#min_price").val();
    discount = $("#item_discount").val();
    total = $("#total_price").val();
    type = $('input[name=type]:checked').val();
    //alert(type);
    total = total * $("#quantity").val();
    //alert(total);
    if (discount != "")
    {
        if (type == 1) {
            //alert(discount);
            final = Number(total) * Number(discount);
            //alert(final);
            final = final / 100;
            //alert(final);
            final_total = total - final;
        }
        else {
            if (discount < total) {
                //om = total / discount;
                //final = om*100;
                final_total = total - discount;

            }
            else {
                alert("Discount is greater then total");
            }
        }
        
        
        //alert(final_total);
        //alert(minimum);
        if (final_total > minimum) {
            alert("Your Price is less then minimum Price");
        }
        //alert(final_total);
        $("#afterDiscount").val(final_total.toFixed(2));
        //$("#item_total_price").val(final_total.toFixed(2));
    }
}

//change paymetn lable with type
function showPaymentLabel(method, ind) {
    if (method == 1) {
       
       $("#bank").eq(ind).hide(); 
       $("#payment_number_1").eq(ind).hide(); 
       
    }
    if (method == 2) {
       $("#payment_number_1 .col-lg-2").eq(0).html(config.ptype1);
       $(".bank").eq(ind).show(); 
       $("#payment_number_1").eq(ind).show(); 
       
    }
    if (method == 3) {
        var text = config.Cnum;
        $("#payment_number_1 .col-lg-2").eq(0).html(config.ptype2);
        $("#bank").eq(ind).show();
        //$(".label_class").eq(0).html(text);
        $("#payment_number_1").eq(ind).show(); 
    }
    if (method == 4) {
        //var text = 'Receipt  Number';
        var text = config.Rnum;
        $("#payment_number_1 .col-lg-2").eq(0).html(config.ptype3);
        $("#bank").eq(ind).show();
        //$(".label_class").eq(0).html(text);
        $("#payment_number_1").eq(ind).show(); 
    }



}
//change paymetn lable with type
function showPaymentLabel2(method, ind) {
    //alert(config.ptype1);
    //alert(config.ptype2);
    //alert(config.ptype3);
    if (method == 1) {
        //alert(method);
       $(".more_add_new_payment"+ind+" #bank").hide(); 
       $(".more_add_new_payment"+ind+" #payment_number_1").hide(); 
       
    }
    if (method == 2) {
        //alert(method);
       $(".more_add_new_payment"+ind+" #payment_number_1 .col-lg-2").html(config.ptype1);
       $(".more_add_new_payment"+ind+" #bank").show(); 
       $(".more_add_new_payment"+ind+" #payment_number_1").show(); 
       
    }
    if (method == 3) {
       //alert(method);
        var text = config.Cnum;
        $(".more_add_new_payment"+ind+" #payment_number_1 .col-lg-2").html(config.ptype2);
        $(".more_add_new_payment"+ind+" #bank").show();
        //$(".label_class").eq(0).html(text);
        $(".more_add_new_payment"+ind+" #payment_number_1").show(); 
    }
    if (method == 4) {
        //alert(method);
        //var text = 'Receipt  Number';
        var text = config.Rnum;
        $(".more_add_new_payment"+ind+" #payment_number_1 .col-lg-2").html(config.ptype3);
        $(".more_add_new_payment"+ind+" #bank").show();
        //$(".label_class").eq(0).html(text);
        $(".more_add_new_payment"+ind+" #payment_number_1").show(); 
    }
    


}



//adding purchase price adn salre price will return you pr
function calculateProfitLoss() {
    len = allProductsData.length;
    totalpPrice = 0;
    totalProfit = 0;
    for (prc = 0; prc < len; prc++) {
        console.log(allProductsData[prc]['productPrice'] + "ppval");
        totalpPrice = parseFloat(totalpPrice) + parseFloat(allProductsData[prc]['productPrice']);
        tempTotal = parseFloat(allProductsData[prc]['salePrice']) - parseFloat(allProductsData[prc]['productPrice']);
        totalProfit = parseFloat(totalProfit) + tempTotal;
    }
    total_profit = totalProfit;
    //console.log(totalpPrice+"totalval");
    totalp = totalpPrice + ' RO';
    $("#total_purchase_price").html(totalp);
    totalProfit = totalProfit + ' RO';
    $("#total_profit").html(totalProfit);

}
function addSalesHml(salerId, finalSales) {


    salerrname = $("#salesName" + salerId).val();
    salesH = '<div class="raw" id="purchase_div' + salerId + '">';
    salesH += '<div class="form_title">' + config.SLang + ': </div>';
    salesH += '<div class="form_field form-group" id="total_purchase_price">' + salerrname + '</div>';
    salesH += '<div class="form_title">' + config.SMLang + ': </div>';
    salesH += '<div class="form_field form-group" id="total_purchase_price">' + finalSales + ' RO</div></div>';
    $("#sales_persons").append(salesH);
}

function addInvoice() {
    alert("asd");
    //allPData = json_encode(allProductsData);
    $("#invoiceData").val(JSON.stringify(allProductsData));
    $("#supplier_id").attr("disabled", false);
    $("#frm_invoice").submit();
}
function addSales() {

    salerId = $("#sales_list").val();
    //alert(salerId);
    saler = $("#salerId" + salerId).length;
    if (saler == 0) {
        html = "<input type='hidden' class='salerId' id='salerId" + salerId + "'  name='salers[]' value='" + salerId + "'>";
        $("#fff").append(html);
        finalSales = addSalerAmount(salerId);
        addSalesHml(salerId, finalSales);

    }
    else {
        alert("You have already added this saler");
    }
}
totalAmountexComision = '';
totalnetAmountexComision = '';
salesCounter = 0;
netProfitCountSum = "";
netProfitCounter = 0;
function addSalerAmount(id) {
    //total = $("#total_price").val();

    comisison = $("#salescomission" + id).val();
    comisionType = $("#salescomissionType" + id).val();

    salesLen = allSalesData.length;
    for (a = 0; a < salesLen; a++) {
        id = allSalesData[a].id;
        comision = allSalesData[a].comision;
        saleType = allSalesData[a].type;
        if (saleType == "total") {

            final = parseInt(total_value) * parseInt(comision);
            //alert(final);
            final_sales = final / 100;
            //alert(final);
            if (totalAmountexComision == "") {
                totalAmountexComision = total_value;
            }

            totalAmountexComision = totalAmountexComision - final_sales;

        }
        else {
            //netProfitCountSum;
            netProfitData = {salerId: id, comision: comisison};
            netProfitCountSum[netProfitCounter] = netProfitData;
            netProfitData++;
        }


    }

    if (comisionType = "total") {
        final = parseInt(total_value) * parseInt(comisison);
        //alert(final);
        final_sales = final / 100;
        //alert(final);
        if (totalAmountexComision == "") {
            totalAmountexComision = total_value;
        }


        //allSalesData

        totalAmountexComision = total_value - final_sales;
    }
    else {

        final = parseInt(total_profit) * parseInt(comisison);
        //alert(final);
        final_sales = final / 100;
        //alert(final);
        //final_sales = total_profit-final;
    }

    salesData = {salerId: id, comision: comisison, type: comisionType};

    allSalesData[salesCounter] = salesData;
    salesCounter++;

    //alert(final);
    return final_sales;


}
function addingNewSales() {
    mergData = '';
    compId = $("#companyid").val();
    branchid = $("#branchid").val();
    ownerId = $("#ownerid").val();
    //alert(ownerId);
    if (ownerId != "")
        mergData += "&ownerId=" + ownerId + "";

    if (compId != "")
        mergData += "&companyid=" + compId;

    if (branchid != "")
        mergData += "&branchid=" + branchid + "";

    sales_data = $("#sales_form").serialize() + mergData;
    $.ajax({
        url: config.BASE_URL + "ajax/addSalesData",
        dataType: 'json',
        type: 'post',
        data: {sales: sales_data},
        cache: false,
        success: function (data)
        {
            salData = data;
            //salData =  $.parseJSON(data); 
            fName = $("#sales_form #fullname").val();
            htmlSales = '<option value="' + salData.saler_id + '">' + fName + '</option>';
            //$("#sales_form").serilaize();;
            $("#sales_list").append(htmlSales);
            html = "<input type='hidden'  id='salesCode" + salData.saler_id + "' value='" + salData.employee_code + "'>";
            $("#sales_names_list").append(html);

            html = "<input type='hidden' id='salesName" + salData.saler_id + "' value='" + fName + "'>";
            $("#sales_names_list").append(html);


            $("#osx2-overlay").trigger('click');
        }
    });


}

//codeid
function showCode(id) {
    salCode = $("#salesCode" + id).val();
    $("#sale_code").val(salCode);

}

//adding new record in table and array
prdocutCounter = 0;
function addItem() {

    pid = $("#productid").val();
    //alert(pid);
    if (pid != "") {
        qty = $("#quantity").val();
        tPrice = $("#total_price").val();
        //totalPrice = $("#item_total_price").val();
        totalPrice = $("#afterDiscount").val();
        pImage = $("#imageProduct" + pid).val();
        pPrice = $("#purchasePrice" + pid).val();
        storeId = $("#store_id").val();
        pName = $("#productname").val();
        itemDiscount = $("#item_discount").val();
        type = $('input[name=type]:checked').val();
        comment = $("#product_comment").val();
        minPrice = $("#min_price").val();
        notes = $("#product_notes").val();
        productData = {productId: pid, quantity: qty, salePrice: totalPrice, discount: itemDiscount, discountType: type, discription: comment, note: notes, productImage: dd.product.product_picture, total: tPrice, productName: pName, storeId: storeId, minPrice: minPrice, productPrice: dd.product.purchase_price};

        allProductsData[prdocutCounter] = productData;
        prdocutCounter++;
        addInput(productData);
        console.log(productData);
        addrow(productData);
        calculateProfitLoss();
    }
}

function addPurchaseItem() {

    pid = $("#productid").val();
    if (pid != "") {
        qty = $("#quantity").val();
        unit_price = $("#unit_price").val();
        pImage = $("#imageProduct" + pid).val();
        pPrice = $("#purchasePrice" + pid).val();
        storeId = $("#store_id").val();
        pName = $("#nameProduct" + pid).val();
        itemDiscount = $("#item_discount").val();
        supplier_id = $("#supplier_id").val();
        product_comment = $("#product_comment").val();
        minPrice = $("#min_price").val();
        notes = $("#product_notes").val();

        //console.log(dd+'ggggg');
        //alert(dd.product_picture+'ppp');
        productData = {productId: pid, quantity: qty, discription: product_comment, note: notes, productImage: dd.product.product_picture, productName: dd.product.itemname, storeId: storeId, supplier_id: supplier_id, unit_price: unit_price, payment_amount: ''};

        allProductsData[prdocutCounter] = productData;
        prdocutCounter++;
        //addInput(productData);
        console.log(productData);
        //addrow(productData);
        addpurchaserow(productData);
    }
    $("#productname").val('');
}


function addPaymetnHtml() {
    $("#osx2-overlay").trigger('click');

    if (payment_omr != "") {
        omrPayment = $("#payment_omr").val();
        percePayment = $("#payment_percentage").val();
        bankT = $("#bank_t").val();
        payment_label_numberT = $("#payment_label_number_t").val();
        //payment_lgabel_numberT = $("#payment_label_number_t").val();
        //period_t = $("#period_t").val();
        period_t = $("#period_t").val();
        paymentNumbers_t = $("#paymentNumbers_t").val();
        //paymentNumbers_t = $("#paymentNumbers_t").val();
        payment_date_t = $("#payment_date_t").val();
        omrVal = 'omr';
        perVal = 'percentage';
        pHtml = '<div class="form_field form-group payment_div"> ';
        pHtml += '<input name="total_payment_omr[]" type="text" id="total_payment_omr[]" onchange="calculatePayment(' + 1 + ',this)"  class="formtxtfield_small" value="' + omrPayment + '"/>';
        pHtml += '<span>RO.</span> </div>';
        pHtml += '<div class="form_field form-group payment_div">';
        pHtml += '<input name="total_payment_percentage[]" id="total_payment_percentage[]" type="text" onchange="calculatePayment(' + 2 + ',this)"  class="formtxtfield_small" value="' + percePayment + '"/>';
        pHtml += '<span>%</span> </div>';
        pHtml += '<div class="raw_payments"><div class="form_title">Type </div>';
        pHtml += '<div class="form_field form-group"><div class="dropmenu">';
        pHtml += '<div class="styled-select"><select name="payment_type" onchange="showPaymentLabel(this.value)"><option selected="selected" >Select Payment Method </option><option value="1">Cash</option><option  value="2">Cheque</option></select></div></div>';
        pHtml += '</div><div  class="form_title2" style="width:60px;">Bank </div><div class="form_field form-group">';
        pHtml += '<input name="bank" id="bank" type="text"  class="formtxtfield" value="' + bankT + '"/></div></div>';
        pHtml += '<div class="raw_payments">';
        pHtml += '<div class="form_title" id="payment_method_label">Cheque Number </div>';
        pHtml += '<div class="form_field form-group"><input  type="text" id="payment_label_number" name="payment_label_number"  class="formtxtfield_small" value="' + payment_label_numberT + '"/></div>';
        pHtml += '<div class="form_title2"style="width:60px;">Period </div><div class="form_field form-group">';
        pHtml += '<div class="formtxtfield_filter_small2"><input name="" type="text"  class="filter_field_small" value="' + period_t + '"/>';

        pHtml += '<div class="dropmenu_filter"><div class="styled-select_filter">';
        pHtml += '<select name=""><option selected="selected"  value="days">Days </option><option value="weeks">Weeks</option><option value="months">Months</option><option  value="years">Years</option></select></div></div></div></div>';
        pHtml += '<div class="form_title2">Payment  Numbers </div><div class="form_field form-group"> <input name="input2" type="text"  class="formtxtfield_small" value="' + paymentNumbers_t + '"/></div>';

        pHtml += '<div class="raw"><div class="form_title" id="payment_method_label">Payment Date </div>';
        pHtml += '<div class="form_field form-group"><input  type="text" id="payment_date" name="payment_date[]"  class="formtxtfield_small payment_date" value="' + payment_date_t + '"/></div></div></div>';
        //pHtml+='<div class="form_field form-group"><input  type="text" id="payment_date" name="payment_date[]"  class="formtxtfield_small payment_date"/>';
        $("#paymemts_div").append(pHtml);



        $(".payment_date").datepicker({
            defaultDate: "today",
            changeMonth: true,
            numberOfMonths: 1,
        });

        $("#payment_omr").val('');
        $("#payment_percentage").val('');
        $("#bank_t").val('');
        $("#payment_label_number_t").val('');
        //payment_label_numberT = $("#payment_label_number_t").val();
        //period_t = $("#period_t").val();
        $("#period_t").val('');
        $("#paymentNumbers_t").val('');
        //paymentNumbers_t = $("#paymentNumbers_t").val();
        $("#payment_date_t").val('');

        remainingAmount = getRemainingPaymentAmount();
        $("#remaining_amount").html(remainingAmount);
        $("#remaining_t").html(remainingAmount);
    }
}

function calculatePaymentByAmountType() {

    calculatePaymentAmountPercentage();
    remainingAmount = getRemainingPaymentAmount();
    $("#remaining_amount").html(parseFloat(remainingAmount));
}


function calculatePaymentAmountPercentage() {
    remainingAmount = getRemainingPaymentAmount();
    if (jQuery('[name*="payments[total_payment_omr]"]').length > 0) {
        jQuery('[name*="total_payment_omr[]"]').each(function (index, value) {
            //var option = jQuery( this );
            //alert(index);
            //paymentValue = $(this).eq(index).val();
            pValue = jQuery('[name*="total_payment_omr[]"]').eq(index).val();
            //alert(pValue);
            if (pValue != "") {
                //remainingAmount = remainingAmount - pValue;
                final = parseInt(pValue) / parseInt(remainingAmount);
                final = final * 100;
                final = parseInt(final);
                final.toFixed(2);
                jQuery('[name*="total_payment_percentage[]"]').eq(index).val(final);
            }
            else {
                total_payment_percentage = jQuery('[name*="total_payment_percentage[]"]').eq(index).val();//$("#total_payment_percentage").val();
                final = parseInt(total_payment_percentage) * parseInt(remainingAmount);
                final = final / 100;
                jQuery('[name*="total_payment_omr[]"]').eq(index).val(final);
            }
            //alert(remainingAmount);
            //console.log(paymentValue+'paymentValue');
            //console.log(value+'value');
        })
    }
}
function getRemainingPaymentAmount() {
    ind = 0;
    pType = $('input[name=paymentAmountType]:checked').val();

    if (pType == 1) {
        remainingAmount = net_total;
    }
    else {
        //net_total = ;
        remainingAmount = net_total - $("#receiverd_amount").val();
    }
    if (jQuery('[name*="total_payment_omr"]').length > 0) {
        jQuery('[name*="total_payment_omr"]').each(function (index, value) {
            //var option = jQuery( this );
            //alert(index);
            paymentValue = $(this).eq(index).val();
            pValue = jQuery('[name*="total_payment_omr"]').eq(index).val();
            //alert(pValue);

            remainingAmount = remainingAmount - pValue;
            //alert(remainingAmount);
            //console.log(paymentValue+'paymentValue');
            //console.log(value+'value');
        });
    }
    //alert(remainingAmount);
    payment_omr = $("#payment_omr").val();
    if (payment_omr != "") {
        remainingAmount = remainingAmount - payment_omr;
    }
    return remainingAmount;
}
///calculate payments 
totalpaymentOmr = 0;
function calculatePayment(paymentType, pOBj) {

    pp = pOBj;
    //alert('calculate');
    pId = $(pOBj).attr('id');
    pIndex = $(pOBj).index();
    receiveAmount = $("#receiverd_amount").val();
    if (pId == 'total_payment_omr[]') {
        //alert('asd');
        if (jQuery('[name*="total_payment_omr"]').length > 0) {
            jQuery('[name*="total_payment_omr"]').each(function (index, value) {
                //var option = jQuery( this );
                //alert(index);

                //paymentValue = $(this).eq(index).val();
                pValue = jQuery('[name*="total_payment_omr"]').eq(index).val();
                //alert(pValue);
                totalpaymentOmr = Number(totalpaymentOmr) + Number(pValue);

                //alert(pValue);

                //alert(remainingAmount);
                //console.log(paymentValue+'paymentValue');
                //console.log(value+'value');
            })
            remainingAmount = receiveAmount - totalpaymentOmr;
            perc = $(pOBj).val() / receiveAmount;
            percantage = perc * 100;

            $(".rem_amount").eq(pIndex).html(remainingAmount.toFixed(2));
            jQuery('[name*="total_payment_percentage"]').eq(pIndex).val(percantage);
            //$('#payment_percentage').val(percantage);
        }

    }

} 
var counter=0;
function add_new_payment(){
    var content=$(".hide_form_payment").html();
    counter++;
    $(".more_add_new_payment_main").append("<div class='more_add_new_payment"+counter+"'><hr style='clear: right;border: 1px solid;padding-top: 1px'>"+content+"</div>");
    //alert($(".more_add_new_payment"+counter+" .styled-select select").text());
    $(".more_add_new_payment"+counter+" .styled-select select").removeClass("1 "+counter+"");
    $(".more_add_new_payment"+counter+" .styled-select select").addClass(""+counter+"");
    
    
    $(".more_add_new_payment"+counter+" .styled-select select").attr("onchange","showPaymentLabel2(this.value,"+counter+")");
    
    //alert(".more_add_new_payment"+counter+" .styled-select select");
    
}
/*function calculatePayment(paymentType,pOBj){
 
 pp = pOBj;
 //alert(paymentType);
 pIndex  = $(pOBj).index();
 pId = $(pOBj).attr('id');
 
 //alert(pId);
 remainingAmount = getRemainingPaymentAmount();
 //alert(remainingAmount);
 //alert(remainingAmount);
 if(pId == 'payment_omr[]'){
 
 totalpaymentOmr = totalpaymentOmr++$(pOBj).val();
 final = parseInt(totalpaymentOmr)/parseInt(remainingAmount);
 final = final*100;
 final = parseInt(final);
 final.toFixed(2);
 
 //alert(final);
 $('#payment_percentage').val(final);
 
 }
 else if(pId == 'payment_percentage'){
 
 total_payment_percentage = $('#payment_percentage').val();//$("#total_payment_percentage").val();
 final = parseInt(total_payment_percentage)*parseInt(remainingAmount);
 //alert(final);
 final = final/100;
 //alert(final);
 //final_total = total-final;
 $('#payment_omr').val(final);
 
 
 }
 else{
 
 if(paymentType == 1){
 
 
 //total_payment_percentage = $("#total_payment_percentage").val();
 totalpaymentOmr = $(pOBj).eq(pIndex).val();
 final = parseInt(totalpaymentOmr)/parseInt(remainingAmount);
 final = final*100;
 final = parseInt(final);
 final.toFixed(2);
 
 //alert(final);
 $('[name*="total_payment_percentage[]"]').eq(pIndex).val(final);
 }
 else{
 total_payment_percentage = $(pOBj).eq(pIndex).val();//$("#total_payment_percentage").val();
 final = parseInt(total_payment_percentage)*parseInt(remainingAmount);
 //alert(final);
 final = final/100;
 //alert(final);
 //final_total = total-final;
 
 $('[name*="total_payment_omr[]"]').eq(pIndex).val(final);
 
 }		
 
 }
 
 remainingAmount = getRemainingPaymentAmount();	
 $("#remaining_amount").html(parseFloat(remainingAmount));
 $("#remaining_t").html(parseFloat(remainingAmount));
 }*/






