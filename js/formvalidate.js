

$().ready(function (e) {

    
//    $('#frm_category').bootstrapValidator({
//        fields: {
//            catname: {
//                validators: {
//                    notEmpty: {
//                        message: 'Please Enter Category'
//                    }
//                }
//            }
//        }
//    });

});

var ij = 0;
AddContainer = function () {
    ij++;
    var htm = '<br /><input name="container_number' + ij + '" id="container_number' + ij + '" value="" type="text"  class="formtxtfield"/>';
    $('#count_container').val(ij);
    $('#div_container').append(htm);
}


$(".port_type").click(function () {
    //	alert('click');
    getCharges();
});

showdiv_status_waiting = function () {
    var job_status = $("#job_status option:selected").val();
    if (job_status == "waiting_for_customer") {
        $('#email_btn').show();
        //$('#div_status_waiting').hide();
    } else {
        getChargesforOneShipment();
        $('#email_btn').hide();
        //$('#div_status_waiting').show();
    }
    if (job_status == 'close') {
        var job_id = $('#id').val();
        console.log(job_id);
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/jobs/checkMandatoryFile",
            type: 'post',
            data: {job_id: job_id},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                console.log(data);
                if (data == 'present') {
                } else {
                    $("#job_status").val('');
                    alert('Please attached Mandatory File.');
                }
            }
        });


        //var len = $('#demo-files').text().length;
        //var len = $('#div_mandatory_attachment').html();
        //if(len){
        //$( "#job_status" ).val('');
        //console.log(len);
        //console.log($('#div_mandatory_attachment:has(img)').length);
        //alert('Please attached Mandatory File.');
        //}
    }
}


getvalueofcharge = function () {
    var chargeSelectorfixed = $("#chargeSelectorfixed option:selected").val();
    if (chargeSelectorfixed != '') {
        //console.log($('#divs_'+chargeSelectorfixed).html());
        $('#fixed_append').prepend($('#divs_' + chargeSelectorfixed).html());
        $("#chargeSelectorfixed option[value='" + chargeSelectorfixed + "']").remove();
        $('#divs_' + chargeSelectorfixed).html('')
    }
}


getvalueofchargecontract = function () {
    var chargeSelectorcontract = $("#chargeSelectorcontract option:selected").val();
    if (chargeSelectorcontract != '') {
        console.log($('#divs_' + chargeSelectorcontract).html());
        $('#contract_append').prepend($('#divs_' + chargeSelectorcontract).html());
        $("#chargeSelectorcontract option[value='" + chargeSelectorcontract + "']").remove();
        $('#divs_' + chargeSelectorcontract).html('')
    }
}

// created by M.Ahmed
// created for load datatable records with seraching.

updateChargesDataTableDiv = function () {
    var from_date_filter = $("#from_date_filter").val();
    var to_date_filter = $("#to_date_filter").val();

    if (from_date_filter != "" && to_date_filter != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "charges/charges/optionsDataTable",
            type: 'post',
            data: {from_date_filter: from_date_filter, to_date_filter: to_date_filter},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#printdiv').html(data);
            }
        });
    }
}


// created by M.Ahmed
// created for load datatable records with seraching.

updateBranchesDataTableDiv = function () {
    var from_date_filter = $("#from_date_filter").val();
    var to_date_filter = $("#to_date_filter").val();

    if (from_date_filter != "" && to_date_filter != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "branches/branches/optionsDataTable",
            type: 'post',
            data: {from_date_filter: from_date_filter, to_date_filter: to_date_filter},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#printdiv').html(data);
            }
        });
    }
}


// created by M.Ahmed
// created for load datatable records with seraching.

updateUsersDataTableDiv = function () {
    var from_date_filter = $("#from_date_filter").val();
    var to_date_filter = $("#to_date_filter").val();

    if (from_date_filter != "" && to_date_filter != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "users/users/all_usersDataTable",
            type: 'post',
            data: {from_date_filter: from_date_filter, to_date_filter: to_date_filter},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#printdiv').html(data);
            }
        });
    }
}

// created by M.Ahmed
// created for load datatable records with seraching.

updateTransactionsDataTableDiv = function () {
    var from_date_filter = $("#from_date_filter").val();
    var to_date_filter = $("#to_date_filter").val();

    if (from_date_filter != "" && to_date_filter != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "transaction_management/transaction_management/indexDataTable",
            type: 'post',
            data: {from_date_filter: from_date_filter, to_date_filter: to_date_filter},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#printdiv').html(data);
            }
        });
    }
}

// created by M.Ahmed
// created for load datatable records with seraching.
updateBanksDataTableDiv = function () {
    var from_date_filter = $("#from_date_filter").val();
    var to_date_filter = $("#to_date_filter").val();

    if (from_date_filter != "" && to_date_filter != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "transaction_management/transaction_management/banksWithFilters",
            type: 'post',
            data: {from_date_filter: from_date_filter, to_date_filter: to_date_filter},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#printdiv').html(data);
            }
        });
    }
}

// created by M.Ahmed
// created for load datatable records with seraching.
updateEachBranchCashDataTableDiv = function () {
    var from_date_filter = $("#from_date_filter").val();
    var to_date_filter = $("#to_date_filter").val();

    if (from_date_filter != "" && to_date_filter != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "cash_management/cash_management/indexDataTable",
            type: 'post',
            data: {from_date_filter: from_date_filter, to_date_filter: to_date_filter},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#printdiv').html(data);
            }
        });
    }
}

// created by M.Ahmed
// created for load datatable records with seraching.

updateBranchCashDataTableDiv = function () {
    var from_date_filter = $("#from_date_filter").val();
    var to_date_filter = $("#to_date_filter").val();

    if (from_date_filter != "" && to_date_filter != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "cash_management/cash_management/cashWithFilter",
            type: 'post',
            data: {from_date_filter: from_date_filter, to_date_filter: to_date_filter},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#printdiv').html(data);
            }
        });
    }
}

// created by M.Ahmed
// created for load datatable records with seraching.

updateCustomersDataTableDiv = function () {
    var from_date_filter = $("#from_date_filter").val();
    var to_date_filter = $("#to_date_filter").val();

    if (from_date_filter != "" && to_date_filter != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "customers/customers/loadCustomersDataTable",
            type: 'post',
            data: {from_date_filter: from_date_filter, to_date_filter: to_date_filter},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#printdiv').html(data);
            }
        });
    }
}


// created by M.Ahmed
// created for load datatable records with seraching.

updateExpireJobsDataTableDiv = function () {
    var from_date_filter = $("#from_date_filter").val();
    var to_date_filter = $("#to_date_filter").val();

    if (from_date_filter != "" && to_date_filter != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/jobs/loadExpireJobsDataTable",
            type: 'post',
            data: {from_date_filter: from_date_filter, to_date_filter: to_date_filter},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#printdiv').html(data);
            }
        });
    }
}


// created by M.Ahmed
// created for load datatable records with seraching.

updateCancelJobsDataTableDiv = function () {
    var from_date_filter = $("#from_date_filter").val();
    var to_date_filter = $("#to_date_filter").val();

    if (from_date_filter != "" && to_date_filter != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/jobs/loadCancelJobsDataTable",
            type: 'post',
            data: {from_date_filter: from_date_filter, to_date_filter: to_date_filter},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#printdiv').html(data);
            }
        });
    }
}


// created by M.Ahmed
// created for load datatable records with seraching.

updateDataTableDiv = function () {
    var from_date_filter = $("#from_date_filter").val();
    var to_date_filter = $("#to_date_filter").val();

    if (from_date_filter != "" && to_date_filter != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/jobs/loadDataTable",
            type: 'post',
            data: {from_date_filter: from_date_filter, to_date_filter: to_date_filter},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#printdiv').html(data);
            }
        });
    }
}


searcReports = function (report_type) {

    var from_date_filter = $("#from_date_filter").val();
    var to_date_filter = $("#to_date_filter").val();

    if (from_date_filter != "" && to_date_filter != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "dashboard/search_reports",
            type: 'post',
            data: {from_date_filter: from_date_filter, to_date_filter: to_date_filter, report_type: report_type},
            cache: false,
            success: function (data)
            {
                if (report_type == 'profitloss') {
                    $('#proft_data').html(data);

                }
                else {
                    $('#balance_data').html(data);

                }
                $('#loading_image_div').hide();
                //$('#printdiv').html(data);
            }
        });

    }
}

// created by M.Ahmed
// created for load accounts related to bank id.

loadtranserBankaccounts = function () {
    var account_id = $("#account_id option:selected").val();
    var payment_type = $("#payment_type option:selected").val();
    $('#div_tr_accounts').hide();
    if (account_id != "" && payment_type == "transfer") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "transaction_management/loadtranserBankaccounts",
            type: 'post',
            data: {account_id: account_id},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#div_tr_accounts').show();
                $('#div_tr_accounts_responce').html(data);
            }
        });
    }
}



// created by M.Ahmed
// created for load accounts related to bank id.



getBankAccounts = function () {
    var bank_id = $("#bank_id option:selected").val();
    $('#div_accounts').hide();
    if (bank_id != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "transaction_management/transaction_management/getBankAccounts",
            type: 'post',
            data: {bank_id: bank_id},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#div_accounts').show();
                $('#div_accounts_responce').html(data);
            }
        });
    }
}

getBankAccounts2 = function () {
    var bank_id = $("#bank_id option:selected").val();
    $('#div_accounts').hide();
    if (bank_id != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "transaction_management/transaction_management/getBankAccounts2",
            type: 'post',
            data: {bank_id: bank_id},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#div_accounts').show();
                $('#div_accounts_responce').html(data);
            }
        });
    }
}

getBankAccounts3 = function (value,ind) {
    var bank_id = $(".more_add_new_payment" + ind + " #arr_bank option:selected").val();
    //$('#div_accounts').hide();
    
    if (bank_id != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "transaction_management/transaction_management/getBankAccounts2",
            type: 'post',
            data: {bank_id: bank_id},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $(".more_add_new_payment" + ind + " #div_accounts").show();
                $(".more_add_new_payment" + ind + " #div_accounts_responce").html(data);
            }
        });
    }
}

getBankAccounts4 = function (value,ind) {
    var bank_id = $(".more_add_new_expences" + ind + " #bank_id_ex option:selected").val();
    //$('#div_accounts').hide();
    
    if (bank_id != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "transaction_management/transaction_management/getBankAccounts3",
            type: 'post',
            data: {bank_id: bank_id},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $(".more_add_new_expences" + ind + " #div_accounts_ex").show();
                $(".more_add_new_expences" + ind + " #div_accounts_responce_ex").html(data);
            }
        });
    }
}


getBankAccounts5 = function () {
    var bank_id = $("#bank_id_ex option:selected").val();
    //$('#div_accounts').hide();
    if (bank_id != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "transaction_management/getBankAccounts3",
            type: 'post',
            data: {bank_id: bank_id},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                //$('#div_accounts').show();
                $('.div_accounts_responce_ex').html(data);
            }
        });
    }
}

// created by M.Ahmed
// created for load accounts related to bank id.

getAllBankAccountsForCharges = function (id) {
    var way_of_payment = $("#way_of_payment" + id + " option:selected").val();
    $('#span' + id).hide();
    //console.log(id);
    if (way_of_payment == 'Accounts') {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/jobs/getAllBankAccountsForCharges",
            type: 'post',
            data: {id: id},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#span' + id).show();
                $('#div_accounts_responce' + id).html(data);
            }
        });
    }
}

// created by M.Ahmed
// created for load accounts related to bank id.

getAllBankAccounts = function () {
    var way_of_payment = $("#way_of_payment option:selected").val();
    $('#div_accounts').hide();
    //console.log(id);
    if (way_of_payment == 'Accounts') {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/jobs/getAllBankAccounts",
            type: 'post',
            data: {bank_id: ''},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#div_accounts').show();
                $('#div_accounts_responce').html(data);
            }
        });
    }
}

setFreightCharges = function () {
    var shipment_standard = $("#shipment_standard option:selected").val();
    var document_charges = $('#document_charges option:selected').val();

    //$('#freight_charges').val(0);
    $('#total_cost_of_goods').val(0);
    $('#insurance').val(0);
    $('#total_value').val(0);
    $('#duty_charges').val(0);
    if (shipment_standard == 'FOB') {
        $('#div_freight_charges').show();
        $('#div_insurance').show();
    } else if (shipment_standard == 'CIF') {
        $('#div_freight_charges').hide();
        $('#div_insurance').hide();
        $("#div_insured_charges").hide();

    } else {

        $('#div_freight_charges').hide();
        $('#div_insurance').show();
    }
    if (document_charges == 'duplicate_document') {
        $('#lbl_doc').html('20');
    } else {
        $('#lbl_doc').html('1');
    }
    getChargesforOneShipment();
}


// created by M.Ahmed
// created for load customers related to branch id.

loadCustomers = function () {
    var branch_id = $("#branch_id option:selected").val();
    $("#job_status").val("");
    $('#div_jobs').hide();
    $('#div_charges').hide();
    $('#div_customers').hide();
    $("#customer_id").val('');
    if (branch_id != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/jobs/get_customers_by_branch",
            type: 'post',
            data: {branch_id: branch_id},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#div_customers').show();
                $('#div_customers_responce').html(data);
            }
        });
    }
}

// created by M.Ahmed
// created for load customers related to branch id.

loadEmployees = function () {
    var branch_id = $("#rec_branch_id option:selected").val();
    $('#div_employee').hide();
    $('#loading_image_div').hide();
    if (branch_id != '') {
        $.ajax({
            url: config.BASE_URL + "jobs/jobs/get_emp_by_branch",
            type: 'post',
            data: {branch_id: branch_id},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#div_employee').show();
                $('#div_employee_responce').html(data);
            }
        });
    }
}

// created by M.Ahmed
// created for load customers related to branch id.

loadCustomersWithEmployeesAndBranches = function () {
    var cash_type = $('#cash_type option:selected').val();
    var branch_id = $("#branch_id option:selected").val();
    $("#job_status").val("");
    $('#div_jobs').hide();
    $('#div_charges').hide();
    $('#div_branch').hide();
    $('#div_customers').hide();
    $("#customer_id").val('');
    if (branch_id != "" && cash_type == 'Direct') {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/jobs/get_customers_by_branch",
            type: 'post',
            data: {branch_id: branch_id},
            cache: false,
            success: function (data)
            {
                //$('#loading_image_div').hide();
                $('#div_customers').show();
                $('#div_customers_responce').html(data);

                $.ajax({
                    url: config.BASE_URL + "jobs/jobs/get_branches",
                    type: 'post',
                    data: {branch_id: branch_id},
                    cache: false,
                    success: function (data)
                    {
                        $('#loading_image_div').hide();
                        $('#div_branch').show();
                        $('#div_branch_responce').html(data);

                    }
                });

            }
        });
    } else if (cash_type != 'Direct') {
        $('#div_job_status').hide();
        $('#div_customers').hide();
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/jobs/get_branches",
            type: 'post',
            data: {branch_id: branch_id},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#div_branch').show();
                $('#div_branch_responce').html(data);

            }
        });



    }
}

// created by M.Ahmed
// created for load customers related to branch id.

loadCustomersWithEmployees = function () {
    var branch_id = $("#branch_id option:selected").val();
    $("#job_status").val("");
    $('#div_jobs').hide();
    $('#div_charges').hide();
    $('#div_customers').hide();
    $("#customer_id").val('');
    if (branch_id != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/jobs/get_customers_by_branch",
            type: 'post',
            data: {branch_id: branch_id},
            cache: false,
            success: function (data)
            {
                //$('#loading_image_div').hide();
                $('#div_customers').show();
                $('#div_customers_responce').html(data);
                $.ajax({
                    url: config.BASE_URL + "jobs/jobs/get_emp_by_branch",
                    type: 'post',
                    data: {branch_id: branch_id},
                    cache: false,
                    success: function (data)
                    {
                        $('#loading_image_div').hide();
                        $('#div_employee').show();
                        $('#div_employee_responce').html(data);

                    }
                });
            }
        });
    }
}




loadjobsWithStatus = function () {
    var customer_id = $("#customer_id option:selected").val();
    var job_status = $("#job_status option:selected").val();
    $('#div_charges').hide();
    if (job_status == 'inprocess') {
        //$('#submit_transaction').hide();
    } else {
        //$('#submit_transaction').show();
    }
    if (customer_id != "" && job_status != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/jobs/loadjobsWithStatus",
            type: 'post',
            data: {customer_id: customer_id, job_status: job_status},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#div_jobs').show();
                $('#div_jobs_responce').html(data);
            }
        });
    }
}

// created by M.Ahmed
// created for load charges according to jobid.

loadChargesWithStatus = function () {
    var job_id = $("#job_id option:selected").val();
    var job_status = $("#job_status option:selected").val();
    if (job_id != "" && job_status != "") {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/jobs/loadChargesWithStatus",
            type: 'post',
            data: {job_id: job_id, job_status: job_status},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#div_charges').show();
                $('#div_charges_responce').html(data);
            }
        });
    }
}



// created by M.Ahmed
// created for add customers modules for adding dynamic textboxes.

showPopUpForEmail = function () {
    //$('#light').html($('#dynamicDiv').html());
    $('#loading_image_div').show();
    $.ajax({
        url: config.BASE_URL + "jobs/jobs/loadEmailView",
        type: 'post',
        data: {data: 'data'},
        cache: false,
        success: function (data)
        {
            $('#loading_image_div').hide();
            $('#light').html(data);
            $('#light').show();
            $('#fade').show();
        }
    });
}


sendEmail = function () {
    var customer_id = $("#customer_id option:selected").val();
    var additional_charges = $("#additional_charges").val();
    var duty = $('#duty_charges').val();
    if (additional_charges == "" || additional_charges == 0) {
        alert("Add additional Charges");
        return false;
    }
    var dutys = parseFloat(duty) + parseFloat(additional_charges);
    if (customer_id != '' && dutys != '') {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/loadEmailView",
            type: 'post',
            data: {duty_charges: dutys, customer_id: customer_id},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#light').html(data);
                $('#light').show();
                $('#fade').show()
            }
        });
    }

}

sendEmailReady = function (e) {
    //e.preventDefault();
    var customer_id = $("#hidden_customer_id").val();
    var duty_charges = $("#hidden_duty_charges").val();
    var txt_heading = $("#txt_heading").val();
    var txt_detail = $("#txt_detail").val();
    var fd = '';
    //var fd = new FormData(document.getElementById("forminfo"));
    if (fd == '') {
        fd = '';
    }
    $('#err_heading').hide();
    $('#err_detail').hide();

    if (txt_heading == '') {
        $('#err_heading').show();
    }
    if (txt_detail == '') {
        $('#err_detail').show();
    }

    if (customer_id != '' && duty_charges != '' && txt_heading != '' && txt_detail != '') {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "jobs/sendEmail",
            type: 'post',
            data: {duty_charges: duty_charges, customer_id: customer_id, txt_heading: txt_heading, txt_detail: txt_detail, fd: fd},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#email_btn').hide();
                $('#spanemail_send').html('<input type="hidden" name="email_send" id="email_send" value="1" />');
                $('#light').hide();
                $('#fade').hide()

                //$('#email_send').val('1');
                alert('Email send successfully.');
            }
        });
    }
}

uncheckcheckbox = function () {
    if ($("#sea_port").is(':checked')) {
        $('#sea_port').attr('checked', false);
    }
    if ($("#air_port").is(':checked')) {
        $('#air_port').attr('checked', false);
    }
    if ($("#land_port").is(':checked')) {
        $('#land_port').attr('checked', false);
    }
    getCharges();
}
$('#account_type').on('change', function () {

    var account_type = $("#account_type option:selected").val(); //cash, contract/ fixed
    var divId = 'div_charges';
    if (account_type == 'cash') {
        $('#' + divId).hide();
        $('#div_result').html('');
        $('#div_contactPeriod').hide();
        uncheckcheckbox();
    } else if (account_type == 'contract') {
        $('#' + divId).show();
        $('#div_contactPeriod').show();
        uncheckcheckbox();
    } else if (account_type == 'fixed') {
        $('#' + divId).show();
        $('#div_contactPeriod').hide();
        uncheckcheckbox();
    }
})


$('#total_cost_of_goods').change(function () {
    var shipment_standard = $("#shipment_standard option:selected").val();
    var document_selection = $("#document_charges option:selected").val();
    var type_of_shipment = $('#type_of_shipment option:selected').val();
    var additional_charges = $('#additional_charges').val();
    var document_charges = '1';
    var definity_charges = '';
    var insured_charges = '';
    if (document_selection == 'duplicate_document') {
        document_charges = '20';
    } else {
        document_charges = '1';
    }
    var val = $(this).val();
    var insurance = '0'; // insurance is 1 %
    var total_value = '0'; // total value = value+insurance
    var duty_charges = '0'; // duty charges is 5% of total value.
    regancy_fees = '1'; // regincy fees to add in dutycharges
    if (shipment_standard == 'FOB') {
        var fob = $('#freight_charges').val();
        //+parseFloat(document_charges)
        total_value = parseFloat(insurance) + parseFloat(val) + parseFloat(fob);
        //alert(total_value)
        ;
        insurance = total_value / 100;
        duty_charges = ((total_value / 100) * 5).toFixed(3);
        duty_charges = parseFloat(duty_charges) + parseFloat(additional_charges);
        insurance = insurance.toFixed(3);
    } else if (shipment_standard != 'CIF' && shipment_standard != 'FOB') {
        insurance = val / 100;
        total_value = parseFloat(insurance) + parseFloat(val);
        duty_charges = ((total_value / 100) * 5).toFixed(3);
        duty_charges = parseFloat(duty_charges) + parseFloat(additional_charges);
        insurance = insurance.toFixed(3);
    } else if (shipment_standard == 'CIF') {
        //+parseFloat(document_charges)
        total_value = parseFloat(val);
        duty_charges = ((total_value / 100) * 5).toFixed(3);
        duty_charges = parseFloat(duty_charges) + parseFloat(additional_charges);
    }


    total_value = total_value.toFixed(3);

    if (type_of_shipment == 'import') {
        dc_charges = $("#document_charges").val();
        if (dc_charges == 'duplicate_document') {
            dcCharge = 20;
        }
        else {
            dcCharge = 1;
        }


        new_duty_charges = duty_charges;

        duty_charges = duty_charges + dcCharge + parseInt(regancy_fees);
        //alert(duty_charges);
    }


    if (type_of_shipment == 'export' || type_of_shipment == 'temp_export' || type_of_shipment == 'temp_re_export') {
        duty_charges = 1;
        duty_charges = parseFloat(duty_charges) + parseFloat(additional_charges);
        insured_charges = parseFloat(val) + parseFloat(duty_charges);

    } else {
        if (type_of_shipment == 'import') {
            insured_charges = parseFloat(document_charges);

        } else if (type_of_shipment == 'temp_import' || type_of_shipment == 'temp_re_import') {
            insured_charges = parseFloat(document_charges) + parseFloat(duty_charges);
        }
    }

    definity_charges = duty_charges;
    if (definity_charges != '') {
        $('#unique_charges').val('1');
    }

    $('#insurance').val(insurance);
    $('#total_value').val(total_value);
    $('#duty_charges').val(duty_charges);
    $('#lbl_ttt').html(duty_charges);

    $('#insured_charges').val(insured_charges);
    $('#definity_charges').val(definity_charges);

    $('#lbl_doc').html(document_charges);
    getChargesforOneShipment();
})

getCharges = function () {
    var sea_port = false;
    var air_port = false;
    var land_port = false;
    var test = 0;
    var account_type = $("#account_type option:selected").val();
    if ($("#sea_port").is(':checked')) {
        sea_port = 1;
        test++;
    } else {
        sea_port = false;
    }
    if ($("#air_port").is(':checked')) {
        air_port = 1;
        test++;
    } else {
        air_port = false;
    }
    if ($("#land_port").is(':checked')) {
        land_port = 1;
        test++;
    } else {
        land_port = false;
    }

    if (test > 0) {
        jb = $("#jobs").val();
        //alert('asd');
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "ajax/getChargesForSelection",
            type: 'post',
            data: {sea_port: sea_port, air_port: air_port, land_port: land_port, account_type: account_type, jobid: jb},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#div_result').html('');
                $('#div_res').hide();
                $('#div_result').html(data);
                $('#div_res').show();
            }
        });
    } else {
        $('#div_result').html('');
        $('#div_res').hide();
    }

}


gettotalsof = function () {
    console.log('yes');
    var totalspur = 0;
    var totalssale = 0;
    var tot = 0;
    $(".totalpur").each(function () {
        console.log($(this).val());
        if ($(this).val() != 0) {
            totalspur = parseFloat(totalspur) + parseFloat($(this).val());
        }
    });
    $(".totalsale").each(function () {
        console.log($(this).val());
        if ($(this).val() != 0) {
            totalssale = parseFloat(totalssale) + parseFloat($(this).val());
        }
    });
    console.log(totalspur);
    $('#lbl_pur').html('Purchase = ' + totalspur + ' &nbsp;&nbsp;');
    $('#lbl_sales').html('Sales = ' + totalssale + ' &nbsp;&nbsp;');
    tot = (totalssale) - (totalspur);
    $('#lbl_profit').html('Profit = ' + tot + ' OMR &nbsp;&nbsp;');

}


calculate_total_cost = function (count) {

    var pervalue = $('#txt_' + count).val();
    var no_of_charges = $('#txtno_' + count).val();

    var total = pervalue * no_of_charges;
    console.log('#txt_' + pervalue);
    $('#txttotal_' + count).val(total);
}

// created by M.Ahmed
// created for load loadjobs With respect to Status.
loadCashType = function () {
    var cash_type = $("#cash_type option:selected").val();

    if (cash_type == 'Direct') {
        $('#div_jobs').show();
        $('#div_banks').hide();
        jb_id = $("#jobs").val();
        if (jb_id != "") {
            $('#div_jobs').show();
            $("#bl_div").show();
            $("#div_charges").show();
        }
        $('#div_payment_purpose').hide();
    } else if (cash_type == 'Indirect') {
        $('#div_payment_purpose').show();
        $('#div_customers').hide();
        $('#div_jobs').hide();
        $("#bl_div").hide();
        $("#div_charges").hide();
    }

}

calculate_total_cost_charges = function (count) {

    var pervalue = $('#txt_' + count).val();
    var no_of_charges = $('#txtpercharge_' + count).val();

    var total = pervalue * no_of_charges;
    $('#txttotal_' + count).val(total);
}

getBranchEmployees = function () {
    var branch_id = $("#branch_id option:selected").val();
    $('#loading_image_div').show();
    $.ajax({
        url: config.BASE_URL + "jobs/get_all_employees",
        type: 'post',
        data: {name: 'driver_id', value: '', branch_id: branch_id},
        cache: false,
        success: function (data)
        {
            $('#loading_image_div').hide();
            $('#emp_id_div').html(data);
            $('#emp_id_main_div').show();
        }
    });
}
selectedValueToNull = function () {
    $('#job_status').val('');
    $('#job_id').val('');
}
assignValueForCharge = function () {
    var charges_selection = $("#charges_selection option:selected").val();
    var vals = $("#charges_selection option:selected").attr('vals');
    if (charges_selection != "") {
        $('#value').val(vals);
    }
}

getWilayat_id = function () {
    var region_id = $("#region_id option:selected").val();
    $('#loading_image_div').show();
    $.ajax({
        url: config.BASE_URL + "jobs/getWilayat_id",
        type: 'post',
        data: {name: 'wilayat_id', value: '', region_id: region_id, language: 'english'},
        cache: false,
        success: function (data)
        {
            $('#loading_image_div').hide();
            $('#wilayat_id_div').html(data);
            $('#wilayat_id_main_div').show();
        }
    });
}

toggleBranches = function () {
    var payment_type = $("#payment_type option:selected").val();
    if (payment_type == 'transfer') {

        $('#div_branch').show();
        $('#div_employee').show();
        $('#div_checkque').hide();

    } else {
        $('#rec_branch_id').val('');
        $('#div_checkque').show();
        $('#div_branch').hide();
        $('#div_employee').hide();
    }
}

BankOrCashToggle = function () {
    var payment_type = $("#payment_type option:selected").val();
    $('#div_checkque').hide();
    $('#div_tr_accounts').hide();
    if (payment_type == 'transfer') {
        $('#bank_id').val('');
        $('#div_banks').show();
        $('#div_checkque').hide();
        //$('#div_accounts').show();
    } else if (payment_type == 'deposite') {
        $('#bank_id').val('');
        $('#div_banks').show();


        $('#div_accounts').hide();
        $('#div_checkque').show();
    }

    if (payment_type == 'cheque') {

        $('#cheque_div_date').show();
        $('#cheque_div_number').show();


    } else if (payment_type != 'cheque') {


        $('#cheque_div_date').hide();
        $('#cheque_div_number').hide();
    }
}

$('#additional_charges').change(function () {
    var ne = parseFloat($('#duty_charges').val()) + parseFloat($('#additional_charges').val());
    //alert(ne);
    $('#lbl_ttt').html(ne);

})

$('#duty_charges').change(function () {
    var ne = parseFloat($('#duty_charges').val()) + parseFloat($('#additional_charges').val());
    //alert(ne);
    $('#lbl_ttt').html(ne);

})

getChargesforOneShipment = function () {

    var test = 0;
    var type_of_shipment = $("#type_of_shipment option:selected").val();
    var port = $("#way_of_shipment option:selected").val();
    var customer_id = $('#customer_id option:selected').val();
    var job_status = $('#job_status option:selected').val();
    var duty_charge = $('#duty_charges').val();

    if (type_of_shipment == 'export' || type_of_shipment == 'temp_export' || type_of_shipment == 'temp_re_export') {
        $('#div_deninity_charge').hide();
        $('#div_unique_charges').hide();
        $('#div_insured_charges').hide();
    } else {
        $('#div_deninity_charge').show();
        //$('#div_unique_charges').show();
        ship_standard = $("#shipment_standard").val();
        if (ship_standard != "CIF")
            $('#div_insured_charges').show();
    }
    if (type_of_shipment == 'export') {
        $('#div_shipper_name').html('Shipper Name');
    } else {
        $('#div_shipper_name').html('Consignee Name');
    }
    if (port != 0 && job_status != 'waiting_for_customer' && customer_id != '' && duty_charge != 0) {
        test++;
    }
    if (port == 'sea') {
        $('#voyage_flight_div').html('Voyage.');
        $('#div_air').hide();
        $('#div_bl').show();
        $('#div_ctn').show();
    } else if (port == 'air') {

        if (type_of_shipment == 'export' || type_of_shipment == 'temp_export' || type_of_shipment == 'temp_re_export') {
            $('#div_insurance').hide();
            $('#div_doc_charges').hide();


        } else {

            shipment_standard = $("#shipment_standard").val();
            if (shipment_standard == 'CIF') {
                $('#div_insurance').hide();
            }
            else {
                $('#div_insurance').show();
            }

            $('#div_doc_charges').show();

        }

        $('#voyage_flight_div').html('Flight No.');

        $('#div_air').show();
        $('#div_bl').hide();
    } else {
        $('#div_voyage_flight').hide();
        $('#div_bl').hide();
        $('#div_air').hide();
    }

    return false;
    if (test > 0) {
        $('#loading_image_div').show();
        $.ajax({
            url: config.BASE_URL + "customers/get_customer_charges_info",
            type: 'post',
            data: {port: port, customer_id: customer_id, duty_charge: duty_charge},
            cache: false,
            success: function (data)
            {
                $('#loading_image_div').hide();
                $('#div_result').html('');
                $('#div_res').hide();
                $('#div_result').html(data);
                $('#div_res').show();
            }
        });
    } else {
        $('#div_result').html('');
        $('#div_res').hide();
    }

}

$('#customer_id').change(function () {
    getChargesforOneShipment();
})



/*
$('#form_cashs').bootstrapValidator({
    fields: {
        cash_type: {
            validators: {
                notEmpty: {
                    message: 'Please Select Cash Type'
                }
            }
        }, branch_id: {
            validators: {
                notEmpty: {
                    message: 'Please Select Branch'
                }
            }
        }, payment_type: {
            validators: {
                notEmpty: {
                    message: 'Please Select Payment Type'
                }
            }
        }
    }
});

$('#form_transactions').bootstrapValidator({
    fields: {
        branch_id: {
            validators: {
                notEmpty: {
                    message: 'Please Select Branch'
                }
            }
        },
        job_status: {
            validators: {
                notEmpty: {
                    message: 'Please Select Job Status'
                }
            }
        },
        charge_id: {
            validators: {
                notEmpty: {
                    message: 'There must be some charges.'
                }
            }
        }
    }
});

$('#frm_users').bootstrapValidator({
    fields: {
        companyid: {
            validators: {
                notEmpty: {
                    message: 'Please Select Company'
                }
            }
        },
        email_address: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Email'
                },
                emailAddress: {
                    message: 'The email address is not valid'
                }
            }
        },
        branchid: {
            validators: {
                notEmpty: {
                    message: 'Please Select Branch'
                }
            }
        },
        fullname: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Full Name'
                }
            }
        },
        username: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Username'
                }
            }
        },
        upassword: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Password'
                }
            }
        },
        confpassword: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Confirm Password'
                },
                identical: {
                    field: 'upassword',
                    message: 'The password and its confirm are not the same'
                }
            }
        },
        employeecode: {
            validators: {
                notEmpty: {
                    message: 'Please enter employee code'
                }
            }
        },
        phone_number: {
            validators: {
                notEmpty: {
                    message: 'Please enter phone number'
                },
                integer: {
                    message: 'Phone number should be a number'
                }
            }
        },
        office_number: {
            validators: {
                notEmpty: {
                    message: 'Please enter office number'
                },
                integer: {
                    message: 'Office number should be a number'
                }
            }
        },
        fax_number: {
            validators: {
                notEmpty: {
                    message: 'Please enter fax number'
                },
                integer: {
                    message: 'Fax number should be a number'
                }
            }
        }
    }
});


$('#frm_branch').bootstrapValidator({
    fields: {
        branch_name: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Branch Name'
                }
            }
        },
        contact_person: {
            validators: {
                notEmpty: {
                    message: 'Please Enter contact person name'
                }
            }
        },
        phone_number: {
            validators: {
                notEmpty: {
                    message: 'Please enter contact number'
                },
                integer: {
                    message: 'Contact number should be a number'
                }
            }
        },
        fax_number: {
            validators: {
                notEmpty: {
                    message: 'Please enter fax number'
                },
                integer: {
                    message: 'Fax number should be a number'
                }
            }
        },
        branch_email: {
            validators: {
                emailAddress: {
                    message: 'The email address is not valid'
                }
            }
        },
        branch_website: {
            validators: {
                notEmpty: {
                    message: 'Please Enter branch website'
                }
            }
        },
        branch_code: {
            validators: {
                notEmpty: {
                    message: 'Please enter branch code'
                }
            }
        }
    }
});

$('#frm_invoice').bootstrapValidator({      
 fields: {           
 companyid: {
 validators: {
 notEmpty: {
 message: 'Please Select Company'
 }
 }
 },
 branchid: {
 validators: {
 notEmpty: {
 message: 'Please Select Branch'
 }
 }
 } 
 
 }
 });

$('#frm_charge').bootstrapValidator({
    fields: {
        charge_name: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Charge Name'
                }
            }
        }
    }
});

$('#frm_job').bootstrapValidator({
    fields: {
        customer_id: {
            validators: {
                notEmpty: {
                    message: 'Please Select customer.'
                }
            }
        }, way_of_shipment: {
            validators: {
                notEmpty: {
                    message: 'Please Select Way of Shipment.'
                }
            }
        },
        total_cost_of_goods: {
            validators: {
                notEmpty: {
                    message: 'Please Select total cost of goods.'
                }
            }
        },
        attachment: {
            validators: {
                file: {
                    extension: 'jpeg,png,JPEG,jpg,PNG',
                    type: 'image/jpeg,image/JPEG,image/jpg,image/png,image/PNG',
                    maxSize: 2097152, // 2048 * 1024
                    message: 'The selected file is not valid'
                }
            }
        }

    }
});


$('#frm_customer').bootstrapValidator({
    fields: {
        branch_id: {
            validators: {
                notEmpty: {
                    message: 'Please Select Branch'
                }
            }
        },
        customer_name: {
            validators: {
                notEmpty: {
                    message: 'Please Customer Name'
                }
            }
        },
        contact_number: {
         validators: {
         notEmpty: {
         message: 'Please Enter Contact Number'
         },
         integer: {
         message: 'Contact number should be a number'
         }
         }
         }
         ,
        mobile_number: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Mobile Number'
                },
                integer: {
                    message: 'Mobile number should be a number'
                }
            }
        },
         fax: {
         validators: {
         notEmpty: {
         message: 'Please Enter Fax Number'
         },
         integer: {
         message: 'Fax number should be a number'
         }
         }
         },			
         email: {
         validators: {
         notEmpty: {
         message: 'Please Enter email address'
         },
         emailAddress: {
         message: 'The email address is not valid'
         }
         }
         },
         address: {
         validators: {
         notEmpty: {
         message: 'Please enter Customer Address'
         }
         }
         }
    }
});

$('#frm_supplier').bootstrapValidator({
    fields: {
        branch_id: {
            validators: {
                notEmpty: {
                    message: 'Please Select Branch'
                }
            }
        },
        customer_name: {
            validators: {
                notEmpty: {
                    message: 'Please Customer Name'
                }
            }
        },
        mobile_number: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Mobile Number'
                },
                integer: {
                    message: 'Mobile number should be a number'
                }
       }
        },
    }
});

$('#transaction_form_submit').bootstrapValidator({
    fields: {
        companyid: {
            validators: {
                notEmpty: {
                    message: 'Please Select Company'
                }
            }
        },
        branchid: {
            validators: {
                notEmpty: {
                    message: 'Please Select Branch'
                }
            }
        },
        fullname: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Full Name'
                }
            }
        },
        transaction_type: {
            validators: {
                notEmpty: {
                    message: 'Please Select Transaction Type'
                }
            }
        },
        payment_type: {
            validators: {
                notEmpty: {
                    message: 'Please Select Payment Type'
                }
            }
        },
        cheque_number: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Check Number'
                },
                integer: {
                    message: 'Check number should be a number'
                },
            }
        },
        cheque_amount: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Amount'
                },
                integer: {
                    message: 'Amount should be a number'
                },
            }
        }
    }
});

$('#frm_country').bootstrapValidator({
    fields: {
        countryname: {
            validators: {
                notEmpty: {
                    message: 'Please enter country name'
                }
            }
        },
        countrycode: {
            validators: {
                notEmpty: {
                    message: 'Please enter country code'
                },
                stringLength: {
                    max: 2,
                    min: 2,
                    message: 'Country code should be 2 character'
                }
            }
        }
    }
});

$('#frm_city').bootstrapValidator({
    fields: {
        countryid: {
            validators: {
                notEmpty: {
                    message: 'Please select country'
                }
            }
        },
        cityname: {
            validators: {
                notEmpty: {
                    message: 'Please enter city name'
                }
            }
        },
        citycode: {
            validators: {
                notEmpty: {
                    message: 'Please enter city code'
                },
                stringLength: {
                    max: 3,
                    min: 3,
                    message: 'City code should be 3 character'
                }
            }
        }
    }
});

$('.frm_company').bootstrapValidator({
    fields: {
        company_name_ar: {
            validators: {
                notEmpty: {
                    message: 'Please enter company name in arabic'
                }
            }
        },
        company_name_en: {
            validators: {
                notEmpty: {
                    message: 'Please enter company name in english'
                }
            }
        },
        bankid: {
            validators: {
                notEmpty: {
                    message: 'Please select bank'
                }
            }
        },
        account_number: {
            validators: {
                notEmpty: {
                    message: 'Please enter account number'
                },
                integer: {
                    message: 'Account number should be a number'
                },
                remote: {
                    url: config.BASE_URL + 'ajax/account_exist',
                    // Send { email: 'its value', username: 'its value' } to the back-end
                    data: function (validator) {
                        return {
                            bankid: validator.getFieldElements('bankid').val(),
                            account_number: validator.getFieldElements('account_number').val()
                        };
                    },
                    message: 'This Account Number already in use'
                }
            }
        },
        company_phone: {
            validators: {
                notEmpty: {
                    message: 'Please enter phone number'
                },
                integer: {
                    message: 'Phone number should be a number'
                }
            }
        },
        company_mobile: {
            validators: {
                notEmpty: {
                    message: 'Please enter office number'
                },
                integer: {
                    message: 'Office number should be a number'
                }
            }
        },
        company_fax: {
            validators: {
                notEmpty: {
                    message: 'Please enter fax number'
                },
                integer: {
                    message: 'Fax number should be a number'
                }
            }
        },
        company_email: {
            validators: {
                notEmpty: {
                    message: 'The email address is required'
                },
                emailAddress: {
                    message: 'The email address is not valid'
                }
            }
        },
        company_website: {
            validators: {
                notEmpty: {
                    message: 'Please enter website'
                },
                uri: {
                    message: 'The website address is not valid'
                }
            }
        },
        company_pobox: {
            validators: {
                notEmpty: {
                    message: 'Please enter po box'
                },
                integer: {
                    message: 'The po box number is not valid'
                }
            }
        },
        company_address: {
            validators: {
                notEmpty: {
                    message: 'Please enter company address'
                }
            }
        },
        about_company: {
            validators: {
                notEmpty: {
                    message: 'Please enter about company'
                }
            }
        }

    }
});

$('#frm_location').bootstrapValidator({
    fields: {
        location_name: {
            validators: {
                notEmpty: {
                    message: 'Please enter location'
                }
            }
        }
    }
});

$('#frm_financial').bootstrapValidator({
    fields: {
        financial: {
            validators: {
                notEmpty: {
                    message: 'Please enter financial type'
                }
            }
        }
    }
});

$('#frm_bank').bootstrapValidator({
    fields: {
        bankname: {
            validators: {
                notEmpty: {
                    message: 'Please enter bank name'
                }
            }
        }
    }
});

$('#frm_area').bootstrapValidator({
    fields: {
        locationid: {
            validators: {
                notEmpty: {
                    message: 'Please select location'
                }
            }
        },
        areaname: {
            validators: {
                notEmpty: {
                    message: 'Please enter area name'
                }
            }
        }
    }
});

$('#frm_suppliers').bootstrapValidator({
    fields: {
        suppliername: {
            validators: {
                notEmpty: {message: 'Please enter supplier name'}
            }
        },
        phone_number: {
            validators: {
                notEmpty: {message: 'Please enter phone number'},
                integer: {message: 'Phone must be a number'}
            }
        },
        fax_number: {
            validators: {
                notEmpty: {message: 'Please enter fax number'},
                integer: {message: 'Fax must be a number'}
            }
        },
        office_number: {
            validators: {
                notEmpty: {message: 'Please enter office number'},
                integer: {message: 'Office number must be a number'}
            }
        }
    }
}).on('success.form.bv', function (e) {
    // Prevent form submission
    e.preventDefault();

    // Get the form instance
    var $form = $(e.target);

    // Get the BootstrapValidator instance
    var bv = $form.data('bootstrapValidator');

    // Use Ajax to submit form data
    $.post($form.attr('action'), $form.serialize(), function (result) {
        if ($.trim(result) == '1')
        {
            $.ajax({
                url: config.BASE_URL + "ajax/supplier_dropbox",
                type: 'post',
                data: {ownerid: $('.owner_id').val()},
                cache: false,
                success: function (data)
                {
                    $('#suppliderid').html(data);
                    $('#frm_suppliers')[0].reset();
                    $('.simplemodal-close').click();
                }
            });
        }
    }, 'json');
});


$('#frm_store').bootstrapValidator({
    fields: {
        companyid: {
            validators: {
                notEmpty: {
                    message: 'Please select company'
                }
            }
        },
        storename: {
            validators: {
                notEmpty: {
                    message: 'Please enter store name'
                }
            }
        },
        locationid: {
            validators: {
                notEmpty: {
                    message: 'Please select location'
                }
            }
        },
        areaid: {
            validators: {
                notEmpty: {
                    message: 'Please select area'
                }
            }
        },
        address: {
            validators: {
                notEmpty: {
                    message: 'Please enter address'
                }
            }
        },
        ftypeid: {
            validators: {
                notEmpty: {
                    message: 'Please select financial type'
                }
            }
        },
        rent: {
            validators: {
                notEmpty: {
                    message: 'Please enter rent'
                },
                digits: {
                    message: 'Rent must be a number'
                }
            }
        },
        rent_per: {
            validators: {
                notEmpty: {
                    message: 'Please enter rent'
                },
                integer: {
                    message: 'Rent must be a number'
                }
            }
        },
        dateofpaid: {
            validators: {
                notEmpty: {
                    message: 'Please select paid date'
                },
                date: {
                    format: 'YYYY-MM-DD',
                    message: 'The value is not a valid date'
                }
            }
        },
        ownerphone: {
            validators: {
                notEmpty: {
                    message: 'Please enter owner phone number'
                },
                integer: {
                    message: 'Owner phone must be a number'
                }
            }
        },
        storemanager: {
            validators: {
                notEmpty: {
                    message: 'Please enter store manager name'
                }
            }
        },
        managerphone: {
            validators: {
                notEmpty: {
                    message: 'Please enter manager phone number'
                },
                integer: {
                    message: 'Manager phone must be a number'
                }
            }
        },
        storenumber: {
            validators: {
                notEmpty: {
                    message: 'Please enter store phone number'
                },
                integer: {
                    message: 'Store phone must be a number'
                }
            }
        },
        email: {
            validators: {
                notEmpty: {
                    message: 'Please enter email address'
                },
                emailAddress: {
                    message: 'The email address is not valid'
                }
            }
        },
        capcity: {
            validators: {
                notEmpty: {
                    message: 'Please enter capcity'
                },
                integer: {
                    message: 'capcity must be a number'
                }
            }
        },
        capcity_unit: {
            validators: {
                notEmpty: {
                    message: 'Please select capcity unit'
                }
            }
        }

    }
});

$('#frm_item').bootstrapValidator({
    fields: {
        comid: {
            validators: {
                notEmpty: {
                    message: 'Please select company'
                }
            }
        },
        itemname: {
            validators: {
                notEmpty: {
                    message: 'Please enter item name'
                }
            }
        },
        categoryid: {
            validators: {
                notEmpty: {
                    message: 'Please select category'
                }
            }
        },
        itemtype: {
            validators: {
                notEmpty: {
                    message: 'Please select item type'
                }
            }
        },
        storeid: {
            validators: {
                notEmpty: {
                    message: 'Please select store'
                }
            }
        },
        serialnumber: {
            validators: {
                notEmpty: {
                    message: 'Please select serial number'
                }, integer: {
                    message: 'Serial number must be a number'
                }
            }
        },
        purchase_price: {
            validators: {
                notEmpty: {
                    message: 'Please enter purchase price'
                },
                digits: {
                    message: 'Purchase price must be a number'
                }
            }
        },
        quantity: {
            validators: {
                notEmpty: {
                    message: 'Please enter quantity'
                },
                integer: {
                    message: 'Quantity must be a number'
                }
            }
        }
    }
});



$('#frm_point_of_order').bootstrapValidator({
    fields: {
        comid: {
            validators: {
                notEmpty: {
                    message: 'Please Select Company'
                }
            }
        },
        categoryid: {
            validators: {
                notEmpty: {
                    message: 'Please Select Category'
                }
            }
        },
        storeid: {
            validators: {
                notEmpty: {
                    message: 'Please Select Store'
                }
            }
        },
        supplierid: {
            validators: {
                notEmpty: {
                    message: 'Please Select Supplier'
                }
            }
        },
        paid_date: {
            validators: {
                notEmpty: {
                    message: 'Please enter paid date'
                },
                date: {
                    format: 'YYYY-MM-DD',
                    message: 'The value is not a valid date'
                }
            }
        },
        productid: {
            validators: {
                notEmpty: {
                    message: 'Please Select Product'
                }
            }
        },
        quanitity: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Quanitity'
                },
                integer: {
                    message: 'quanitity should be a number'
                },
            }
        }
        ,
        unit_price: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Unit Price'
                },
                integer: {
                    message: 'Unit Price should be a number'
                },
            }
        }
    }
});




*/
// M.Ahmed 
// POP up css


showPopUp = function () {
    //$('#light').html($('#dynamicDiv').html());
    $('#loading_image_div').show();
    $.ajax({
        url: config.BASE_URL + "jobs/jobs/loadCurrencyView",
        type: 'post',
        data: {data: 'data'},
        cache: false,
        success: function (data)
        {

            $('#loading_image_div').hide();
            $('#light').html(data);
            $('#light').show();
            $('#fade').show();
            $('#currency-quantity').val($('#total_cost_of_goods').val());
        }
    });
}


// M.Ahmed 
// POP up css





showPopUpForFreightCharges = function () {
    //$('#light').html($('#dynamicDiv').html());
    $('#loading_image_div').show();
    $.ajax({
        url: config.BASE_URL + "jobs/jobs/loadCurrencyViewFreightCharges",
        type: 'post',
        data: {data: 'data'},
        cache: false,
        success: function (data)
        {
            $('#loading_image_div').hide();
            $('#light').html(data);
            $('#light').show();
            $('#fade').show();
            $('#currency-quantity').val($('#freight_charges').val());
        }
    });
}

hidePopUP = function () {
    $('#light').hide();
    $('#fade').hide();
}




swapme = function () {
    cs_name = $("#customer_name").val();
    cs_id = $("#customer_id").val();
    if (cs_id != "") {
        $("#customer_id").val(cs_id);
        $("#customer_name").val(cs_name);
        //showInvocices();	
    }
}

swapme2 = function () {
    cs_name = $("#customer_name").val();
    cs_id = $("#customer_id").val();
    if (cs_id != "") {
        $("#customer_name").val(cs_id);
        $("#customer_id").val(cs_name);
        //showPurchaseInvocices();	
    }
}

swapgeneral = function () {
    //alert('asd');
    cs_name = $("#expense_title").val();
    cs_id = $("#expense_id").val();
    if (cs_id != "") {
        $("#expense_title").val(cs_id);
        $("#expense_id").val(cs_name);
    }
}

