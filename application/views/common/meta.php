



<!doctype html>

<html lang="en">

    <head>

        <meta charset="UTF-8">

        <title>Dashboard</title>

        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, minimum-scale=1, maximum-scale=1"/>

        <link rel="shortcut icon" href="favicon.png">

        <?php if (get_set_value('site_lang') == 'english'): ?>

            <!---CSS Files-->

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/core.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/style.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/ui.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/inputs.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/icons.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/anims.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/global.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/icons-ini.css">

            <!---jQuery Files-->

            <script src="<?php echo base_url(); ?>durarthem/ltr/js/jquery.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/ltr/js/jquery-ui.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/ltr/js/functions.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/ltr/js/inputs.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/ltr/js/sparkline.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/ltr/js/flot.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/ltr/js/justgage.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/ltr/js/tablesorter.js"></script>





            <link rel="stylesheet" type="text/css" href="<?PHP echo base_url(); ?>durarthem/ltr/databale/media/css/jquery.dataTables.css">

            <link rel="stylesheet" type="text/css" href="<?PHP echo base_url(); ?>durarthem/ltr/databale/examples/resources/syntax/shCore.css">

            <script type="text/javascript" language="javascript" src="<?PHP echo base_url(); ?>durarthem/ltr/databale/media/js/jquery.dataTables.js"></script> 

            <script type="text/javascript" language="javascript" src="<?PHP echo base_url(); ?>durarthem/ltr/databale/examples/resources/syntax/shCore.js"></script> 



        <?php else: ?>

            <!---CSS Files-->

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/core.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/style.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/ui.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/inputs.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/icons.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/anims.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/global.css">

            <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/icons-ini.css">

            <!---jQuery Files-->

            <script src="<?php echo base_url(); ?>durarthem/rtl/js/jquery.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/rtl/js/jquery-ui.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/rtl/js/functions.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/rtl/js/inputs.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/rtl/js/sparkline.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/rtl/js/flot.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/rtl/js/justgage.js"></script>

            <script src="<?php echo base_url(); ?>durarthem/rtl/js/tablesorter.js"></script>



            <link rel="stylesheet" type="text/css" href="<?PHP echo base_url(); ?>durarthem/rtl/databale/media/css/jquery.dataTables.css">
<link href="<?php echo base_url(); ?>css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
            <link rel="stylesheet" type="text/css" href="<?PHP echo base_url(); ?>durarthem/rtl/databale/examples/resources/syntax/shCore.css">

            <script type="text/javascript" language="javascript" src="<?PHP echo base_url(); ?>durarthem/rtl/databale/media/js/jquery.dataTables.js"></script> 

            <script type="text/javascript" language="javascript" src="<?PHP echo base_url(); ?>durarthem/rtl/databale/examples/resources/syntax/shCore.js"></script> 

        <?php endif; ?>



        <script type="text/javascript">

            var config =

                    {

                        BASE_URL: '<?php echo base_url(); ?>',

                        CURRENT_URL: '<?php echo current_url(); ?>',

                        USER_TYPE: '<?php echo get_member_type(); ?>',

                        A_1: '<?php echo $this->uri->segment(1); ?>',

                        A_2: '<?php echo $this->uri->segment(2); ?>',

                        SLang: '<?php echo lang('Sales-Name') ?>',

                        SMLang: '<?php echo lang('Sales-Amount') ?>',

                        Cnum: '<?php echo lang('Cheque-Number') ?>',

                        Rnum: '<?php echo lang('Receipt-Number') ?>',

                        show_datatable: '<?php echo lang('show_datatable') ?>',

                        Previous: '<?php echo lang('Previous') ?>',

                        First: '<?php echo lang('First') ?>',

                        Last: '<?php echo lang('Last') ?>',

                        Next: '<?php echo lang('Next') ?>',

                        Search: '<?php echo lang('Search') ?>',

                        show_bylist: '<?php echo lang('show_bylist') ?>',

                        ptype1: '<?php echo lang('ptype1') ?>',

                        ptype2: '<?php echo lang('ptype2') ?>',

                        ptype3: '<?php echo lang('ptype3') ?>',

                        maint1: '<?php echo lang('maint1') ?>',

                        maint2: '<?php echo lang('maint2') ?>',

                        maint3: '<?php echo lang('maint3') ?>',

                        maint4: '<?php echo lang('maint4') ?>',

                    }







        </script>

        <script type="text/javascript">



            



            $(document).ready(function () {

                // binds form submission and fields to the validation engine

                //$('#form1').validationEngine('validate');

                //$("#form1").validationEngine();



                $(document).keypress(function (e) {

                    istat = $("input").is(":focus");

                    //alert(e.which);

                    if (e.which == '13') {

                        searchFormData();

                    }

                    if (!istat) {

                        //alert(e.which);

                        if (e.which == 83) {

                            //alert('asdasd');

                            $("#search_btn").trigger('click');

                        }

                        if (e.which == 65) {

                            //redirectPage('jobs/add_jobs');



                            //$("#site_lang").val('arabic');

                            //$("#form1_setting").submit();

                            //document.getElementById('form1').submit();

                            //alert('65');

                            //window.location = "<?php echo base_url(); ?>setting/general_setting/arabic";



                            $.ajax({

                                type: 'POST',

                                url: "<?php echo base_url(); ?>setting/general_setting/",

                                data: {"site_lang": "arabic", "ajax": "ajax"},

                                success: function () {

                                    window.location.href = "<?php echo base_url(); ?>setting/general_setting/?e=11";

                                }

                            });

                            return false;



                        }

                        if (e.which == 69) {

                            //redirectPage('jobs/add_jobs');

                            //$("#site_lang").val('english');

                            //$("#form1_setting").submit();

                            //document.getElementById('form1').submit();

                            //alert('69');

                            $.ajax({

                                type: 'POST',

                                url: "<?php echo base_url(); ?>setting/general_setting/",

                                data: {"site_lang": "english", "ajax": "ajax"},

                                success: function () {

                                    window.location.href = "<?php echo base_url(); ?>setting/general_setting/?e=11";

                                }

                            });

                            return false;

                        }



                    }

                });



                //addsearch();	

                //setTimeout('addsearch()',1000);

            });





            function addsearch() {

                //	alert('ccc');

                hhttm = '<button class="mini mod-tg" id="search_btn" data-modal="#demo-modalnn">Modal Window</button>';

                $("#main-content").prepend(hhttm);

                //htm = '<div id="demo-modalnn" class="modal box"><div class="header">Modal Window</div><div class="mod-body pad-l"></div><div class="mod-act"><button class="close">Close</button></div></div>';

                //$("#main-content").prepend(htm);

            }



            function closediv() {

                $("#demo-modalnn").fadeOut();

                $(".modal-ov").removeClass('show');

                $(".modal-ov").addClass('hide');

            }



            function searchFormData() {

                ssid = $("#search_id").val();

                if (ssid != "")

                    $("#search_form").submit();

                else {

                    alert('Please Enter Search id');

                }



            }



        </script> 

        <style>

            .content-box-all {

                margin: 0 auto;

                width: 98%;

            }

            .remove_print_page{position: absolute;left: 0px;margin-left: 45px;margin-top: -25px;}

            .fancybox-overlay{z-index:99999 !important}

        </style>

    </head>

<style>
    .dhram_price{
        display: none;

    }
</style>

    <body>
 <input type="hidden" id="dharamval" name="dharamval"  value="">
<div id="conversiontest"  style="display:none;"></div>


        <div id="wrapper">
        
