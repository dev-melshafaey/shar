<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php breadcramb();  ?><?php //echo lang('main') ?> | Business Solutions</title>
        <head>

            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                

                    <!-- bootstrap -->
                    <link href="<?php echo base_url(); ?>css/bootstrap/bootstrap.css" rel="stylesheet" />
                    <link href="<?php echo base_url(); ?>css/bootstrap/bootstrap-overrides.css" type="text/css" rel="stylesheet" />

                    <!-- libraries -->
                    <link href="<?php echo base_url(); ?>css/lib/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />
                    <link href="<?php echo base_url(); ?>css/lib/font-awesome.css" type="text/css" rel="stylesheet" />

                    <link href="<?php echo base_url(); ?>css/bootstrap/bootstrap-overrides.css" type="text/css" rel="stylesheet" />

                    <!-- libraries -->
                    <link href="<?php echo base_url(); ?>css/lib/uniform.default.css" type="text/css" rel="stylesheet" />
                    <link href="<?php echo base_url(); ?>css/lib/select2.css" type="text/css" rel="stylesheet" />

                    <!-- global styles -->
                    <?php if (get_set_value('site_lang') == 'english'): ?>
                        <link href="<?php echo base_url(); ?>css/bootstrap/bootstrap.css" rel="stylesheet" />
                        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/compiled/layout.css" />
                        <link rel="stylesheet" href="<?php echo base_url(); ?>js/tabs/easy-responsive-tabs.css" type="text/css"/>
                        
                    <!-- Fonts CSS: -->
                    <link rel="stylesheet" href="<?php echo base_url(); ?>css/helvatice.css" type="text/css" />

                    <?php else: ?>
                        <link href="<?php echo base_url(); ?>css/bootstrap/bootstrap-rtl.css" rel="stylesheet" />
                        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/compiled/layout_rlt.css" />
                        <link rel="stylesheet" href="<?php echo base_url(); ?>js/tabs/easy-responsive-tabs_rtl.css" type="text/css"/>
                    <?php endif; ?>

                    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/compiled/elements.css" />
                    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/compiled/icons.css" />



                    <link rel="stylesheet" href="<?php echo base_url(); ?>css/compiled/form-wizard.css" type="text/css" media="screen" />

                    <link href="<?php echo base_url(); ?>css/lib/uniform.default.css" type="text/css" rel="stylesheet" />
                    <link href="<?php echo base_url(); ?>css/lib/select2.css" type="text/css" rel="stylesheet" />
                    <link href="<?php echo base_url(); ?>css/lib/bootstrap.datepicker.css" type="text/css" rel="stylesheet" />
                    <link href="<?php echo base_url(); ?>css/lib/font-awesome.css" type="text/css" rel="stylesheet" />


                    <!-- this page specific styles -->
                    <link rel="stylesheet" href="<?php echo base_url(); ?>css/compiled/index.css" type="text/css" media="screen" />

                    <!-- open sans font -->
                    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

                    <!-- lato font -->
                    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css' />
                    
					<!-- Fonts CSS: -->
                    <link rel="stylesheet" href="helvatice/css/helvatice.css" type="text/css" />
                    <!--[if lt IE 9]>
                      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                    <![endif]-->
                    </head>
                    <!-- BEGIN JAVASCRIPTS -->
                    <script type="text/javascript">
                        var config =
                                {
                                    BASE_URL: '<?php echo base_url(); ?>',
                                    CURRENT_URL: '<?php echo current_url(); ?>',
                                    USER_TYPE: '<?php echo get_member_type(); ?>',
                                    A_1: '<?php echo $this->uri->segment(1); ?>',
                                    A_2: '<?php echo $this->uri->segment(2); ?>',
                                    SLang: '<?php echo lang('Sales-Name') ?>',
                                    SMLang: '<?php echo lang('Sales-Amount') ?>',
                                    Cnum: '<?php echo lang('Cheque-Number') ?>',
                                    Rnum: '<?php echo lang('Receipt-Number') ?>',
                                    show_datatable: '<?php echo lang('show_datatable') ?>',
                                    Previous: '<?php echo lang('Previous') ?>',
                                    First: '<?php echo lang('First') ?>',
                                    Last: '<?php echo lang('Last') ?>',
                                    Next: '<?php echo lang('Next') ?>',
                                    Search: '<?php echo lang('Search') ?>',
                                    show_bylist: '<?php echo lang('show_bylist') ?>',
                                    ptype1: '<?php echo lang('ptype1') ?>',
                                    ptype2: '<?php echo lang('ptype2') ?>',
                                    ptype3: '<?php echo lang('ptype3') ?>'
                                }


                                 
                    </script>

                    <style>
                        .filter_field_small{
                            width: 75%;
                            float: left;
                        }
                        .dropmenu_filter{float: right;}
                    </style>
                    <script src="<?php echo base_url(); ?>js/jquery.latest.js"></script>
                    
                    </head>
                    <body onload="">