
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/fancyBox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/fancyBox/source/jquery.fancybox.css?v=2.1.5" media="screen" />

<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/fancyBox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/fancyBox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>


<script type="text/javascript">

function exportExcel(){
			//alert('excel');
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#new_data_table_wrapper').html()));
            //e.preventDefault();
			 $("#new_data_table").table2excel({
	   		name: "Excel Document Name"
	  		}); 
}
    $(document).ready(function () {
        /*
         *  Simple image gallery. Uses default settings
         */
		
		///exportExcel();
		
        $('table#new_data_table .aimg').fancybox();
        $('table#product_list .aimg').fancybox();


    });
</script>   


<script src="<?php echo base_url(); ?>durarthem/rtl/js/jquery.highlight-4.js" type="text/javascript"></script> 



<script src="<?php echo base_url(); ?>durarthem/rtl/js/function.js" type="text/javascript"></script> 

<link rel="stylesheet" href="<?php echo base_url(); ?>css/jqcode.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/buttons.dataTables.min.css">
<!--<link rel="stylesheet" href="<?PHP echo base_url(); ?>js/validator/dist/css/bootstrapValidator.css"/>-->
<!--<script type="text/javascript" src="<?PHP echo base_url(); ?>js/validator/vendor/jquery/jquery.min.js"></script>-->
<!--<script type="text/javascript" src="<?PHP echo base_url(); ?>js/validator/vendor/bootstrap/js/bootstrap.min.js"></script>-->
<!--<script type="text/javascript" src="<?PHP echo base_url(); ?>js/validator/dist/js/bootstrapValidator.js"></script>-->

<!--<script type="text/javascript" src="http://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js"></script>-->


<script type="text/javascript" src="<?PHP echo base_url(); ?>exportcss/tableExport.js"></script>
<script type="text/javascript" src="<?PHP echo base_url(); ?>exportcss/jquery.base64.js"></script>
<script type="text/javascript" src="<?PHP echo base_url(); ?>exportcss/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="<?PHP echo base_url(); ?>exportcss/jspdf/jspdf.js"></script>
<script type="text/javascript" src="<?PHP echo base_url(); ?>exportcss/jspdf/libs/base64.js"></script>

<!--<script src="http://localhost/root/bs27augast/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>-->
<!--<script src="http://localhost/root/bs27augast/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>-->
    <script type="text/javascript" src="<?PHP echo base_url(); ?>js/dataTables.bootstrap.min.js"></script>
   <script type="text/javascript" src="<?PHP echo base_url(); ?>js/jquery.dataTables.min.js"></script>
   <script type="text/javascript" src="<?PHP echo base_url(); ?>js/dataTables.buttons.min.js"></script>
 <script type="text/javascript" src="<?PHP echo base_url(); ?>js/buttons.html5.min.js"></script>
 <script type="text/javascript" src="<?PHP echo base_url(); ?>js/buttons.flash.js"></script>
 <script type="text/javascript" src="<?PHP echo base_url(); ?>js/buttons.flash.min.js"></script>
 <script type="text/javascript" src="<?PHP echo base_url(); ?>js/buttons.print.min.js"></script>
<script>
    //Writing by Noor 
    if ($('#new_data_table').length > 0)
    {
        var bFilter = false;
        if($('#new_data_table #remin').length > 0){
            //bFilter = true;
            //alert('found');
            //$("#new_data_table #remin").prop("class","sorting_disabled");
            //$("#new_data_table #remin").toggleClass('sorting');
        }
    
        var thleng = $('#new_data_table thead th').length - 1;
        $('#new_data_table thead th').each(function (index, value) {
            var title = $.trim($(this).html());
            var attr_id = $('#new_data_table thead th').eq($(this).index()).attr('id');
            if (index != '0' && thleng != index)
            {
                $(this).html(title + '<input type="search" class="form-control ' + attr_id + ' search_filter" placeholder="' + title + '" />');
            }
        });
        /*
        var new_data_table = $('#new_data_table').DataTable({
            "ordering": false,
            columnDefs: [ {
                className: 'no-sort',
                orderable: true,
                targets:   11
            } ],
            order: [ 11, 'asc' ],
            //"scrollY": "200px",
            "oLanguage": {
                "sSearch": "",
                "oPaginate": {"sNext": "التالی", "sPrevious": "السابق"}
            }
        });
        */
        var new_data_table = $('#new_data_table').DataTable({
            "ordering": false,
           dom: 'Bfrtip',
            "iDisplayLength": 100,
            "lengthMenu": [ [10, 100, 1000, -1], [10, 100, 1000, "All"] ],
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
            "columnDefs": [{
                "orderSequence": ["desc", "asc"],
                "searchable": false,
                "orderable": false,
                "targets": 4
            }]
            ,
           "order": [[5, 'desc' ]]
        } );
        
        
        new_data_table.on( 'order.dt search.dt', function () {
            new_data_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        }).draw();
        
        /*
        new $.fn.DataTable.FixedColumns(new_data_table, {
            leftColumns: 2,
            rightColumns: 0
        });
        */
        
        new_data_table.columns().eq(0).each(function (colIdx)
        {
            $('input', new_data_table.column(colIdx).header()).on('keyup change', function ()
            {
                new_data_table.column(colIdx).search(this.value).draw();
            });
            
            

        });
        
        
        
        $('.search_filter').keyup(function () {
            $('#new_data_table td').removeHighlight().highlight($(this).val());
        });

        new_data_table.on('draw', function () {
            //tiptop();
        });
    }



    if ($('#new_data_table_search').length > 0)
    {
        var bFilter = false;
        if($('#new_data_table_search #remin').length > 0){
            //bFilter = true;
            //alert('found');
            //$("#new_data_table #remin").prop("class","sorting_disabled");
            //$("#new_data_table #remin").toggleClass('sorting');
        }

        var thleng = $('#new_data_table_search thead th').length - 1;
        $('#new_data_table_search thead th').each(function (index, value) {
            var title = $.trim($(this).html());
            var attr_id = $('#new_data_table_search thead th').eq($(this).index()).attr('id');
            if (index != '0' && thleng != index)
            {
                $(this).html(title + '<input type="search" class="form-control ' + attr_id + ' search_filter" placeholder="' + title + '" />');
            }
        });
        /*
         var new_data_table = $('#new_data_table').DataTable({
         "ordering": false,
         columnDefs: [ {
         className: 'no-sort',
         orderable: true,
         targets:   11
         } ],
         order: [ 11, 'asc' ],
         //"scrollY": "200px",
         "oLanguage": {
         "sSearch": "",
         "oPaginate": {"sNext": "التالی", "sPrevious": "السابق"}
         }
         });
         */
        var new_data_table_search = $('#new_data_table_search').DataTable({
            "ordering": false,
            "iDisplayLength": 10,
            "scrollY":        "200px",
            "scrollCollapse": true,
            paging:         false,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "columnDefs": [{
                "orderSequence": ["desc", "asc"],
                "searchable": false,
                "orderable": false,
                "targets": 4
            }]
            ,
            "order": [[5, 'desc' ]]
        } );


        new_data_table_search.on( 'order.dt search.dt', function () {
            new_data_table_search.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        }).draw();

        /*
         new $.fn.DataTable.FixedColumns(new_data_table, {
         leftColumns: 2,
         rightColumns: 0
         });
         */

        new_data_table_search.columns().eq(0).each(function (colIdx)
        {
            $('input', new_data_table_search.column(colIdx).header()).on('keyup change', function ()
            {
                new_data_table_search.column(colIdx).search(this.value).draw();
            });



        });



        $('.search_filter').keyup(function () {
            $('#new_data_table_search td').removeHighlight().highlight($(this).val());
        });

        new_data_table_search.on('draw', function () {
            //tiptop();
        });
    }


    if ($('#new_data_table3').length > 0)
    {
        var thleng = $('#new_data_table3 thead th').length - 1;
        $('#new_data_table3 thead th').each(function (index, value) {
            var title = $.trim($(this).html());
            var attr_id = $('#new_data_table3 thead th').eq($(this).index()).attr('id');
            $(this).html(title + '<input type="search" class="form-control ' + attr_id + ' search_filter" placeholder="' + title + '" />');

        });

        var new_data_table3 = $('#new_data_table3').DataTable({
            "ordering": false,
            "oLanguage": {
                "sSearch": "",
                "oPaginate": {"sNext": "التالی", "sPrevious": "السابق"}
            }
        });

        new_data_table3.columns().eq(0).each(function (colIdx)
        {
            $('input', new_data_table3.column(colIdx).header()).on('keyup change', function ()
            {
                new_data_table3.column(colIdx).search(this.value).draw();
            });
        });

        $('#new_data_table3 .search_filter').keyup(function () {
            $('#new_data_table3 td').removeHighlight().highlight($(this).val());
        });

        new_data_table3.on('draw', function () {
            //tiptop();
        });
    }
//
    if ($('#new_data_table2').length > 0)
    {
        var thleng = $('#new_data_table2 thead th').length - 1;
        $('#new_data_table2 thead th').each(function (index, value) {
            var title = $.trim($(this).html());
            var attr_id = $('#new_data_table2 thead th').eq($(this).index()).attr('id');
            $(this).html(title + '<input type="search" class="form-control ' + attr_id + ' search_filter" placeholder="' + title + '" />');
        });

        var new_data_table2 = $('#new_data_table2').DataTable({
            "ordering": false,
            "oLanguage": {
                "sSearch": "",
                "oPaginate": {"sNext": "التالی", "sPrevious": "السابق"}
            }
        });

        new_data_table2.columns().eq(0).each(function (colIdx)
        {
            $('input', new_data_table2.column(colIdx).header()).on('keyup change', function ()
            {
                new_data_table2.column(colIdx).search(this.value).draw();
            });
        });

        $('#new_data_table2 .search_filter').keyup(function () {
            $('#new_data_table2 td').removeHighlight().highlight($(this).val());
        });

        new_data_table2.on('draw', function () {
            //tiptop();
        });
    }

    $(document).ready(function () {
        $('ul ul').hide();
        //$('ul li.expanded a').removeAttr("href");
        $('ul li.expanded > a').click(function (event) {
            $('ul ul').hide('slow');
            $(this).parent().find('ul').toggle('slow');
        });
        $("#content").mousedown(function () {
            $('ul ul').hide();
        });


    });

    $(document).ready(function() {
        //alert('asdasd');
       // getDaysLimitNotifcation();
         audioElement = document.createElement('audio');
       //alert(audioElement);
        audioElement.setAttribute('src', '<?php echo base_url() ?>durarthem/sounds-937-job-done.mp3');

        audioElement.addEventListener('ended', function() {
           // this.currentTime = 0;
           // this.play();
        }, false);

        $('#play').click(function() {
            audioElement.play();
        });

        $('#pause').click(function() {
            audioElement.pause();
        });
    });
   
</script>       




<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script> 




<script>
//User Filter
    $(".login_account").fadeOut();
    $("#ad_account").click(function () {
        //alert('click');
        r = $(this).is(':checked');
        //alert(r);
        if (r) {
            $(".login_account").fadeIn();
        }
        else {
            $(".login_account").fadeOut();
        }
    });



</script> 


<script type="text/javascript">

    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });


    var base_url = '<?php echo base_url(); ?>';

</script> 


<script type="text/javascript">

    function generateBarcode() {

        checked_status = $("#barcode_generate").is(':checked');
        if (checked_status) {

            //var itemname = $("#serialnumber").val();
            var itemname = $("#barcodenumber").val();
			 // var box_barcodenumber = $("#box_barcodenumber").val();
            $.ajax({
                url: config.BASE_URL + "ajax/create_barcode",
                type: 'post',
                data: {itemname: itemname},
                cache: false,
                success: function (data)
                {
                    imagename = itemname + ".gif";
                    imagepath = config.BASE_URL + 'barcode_image/' + itemname + ".gif";
                    $("#image_preview").attr('src', imagepath);
				    //$('#employeecode').val(data);
                    $(".barcode_img").show();
                }
            });
        }
        else {
            $("#image_preview").show();
        }
    }
    function generateBoxBarcode() {

        checked_status = $("#barcode_boxgenerate").is(':checked');
        if (checked_status) {

            //var itemname = $("#serialnumber").val();
            var itemname = $("#barcode_boxgenerate").val();
            var box_barcodenumber = $("#box_barcodenumber").val();
            $.ajax({
                url: config.BASE_URL + "ajax/create_box_barcode",
                type: 'post',
                data: {box_barcodenumber:box_barcodenumber},
                cache: false,
                success: function (data)
                {

                    imagename = box_barcodenumber + ".gif";
                    imagepath = config.BASE_URL + 'barcode_box/' + box_barcodenumber + ".gif";
                    $("#image_box_preview").attr('src', imagepath);
                    $("#barcode_box_image").val(imagename);
                    $(".barcode_box_img").show();

                    //$('#employeecode').val(data);
                }
            });
        }
        else {
            $("#image_preview").show();
        }
    }

    function getproductsBySupplier(suplier) {
        //alert('asd');
        $("#supplier_id").attr("disabled", true);
        if (suplier) {

            $.ajax({
                url: config.BASE_URL + "ajax/getProductBySuplier",
                type: 'post',
                data: {supplier_id: suplier},
                cache: false,
                success: function (data)
                {
                    console.log(data);
                    if (data)
                        $("#productid").html(data);



                }
            });
        }
        else {
            $("#image_preview").show();
        }
    }




</script> 
<script>
    //Writing by Noor 

    function getPendingPurchaseDetail(id) {
        //$("#modal_data")
        //getPurcahseItems
        $("#modal_data").load(config.BASE_URL + "purchase_management/getPurcahseItems/" + id, {id: id});
    }
    function getCustomerData(id)
    {
        $("#modal_data_comes_here").load(config.BASE_URL + "customers/getCustomerData_ajax/" + id, {id: id});
    }

    function addCustomerData() {

        $("#modal_data_comes_here").load(config.BASE_URL + "customers/addnewcustomer2/", adduser_option());

    }
    function adduser_option() {

        // $("#ad_account").click(function () {
        /* $( "#ad_account[type=checkbox]" ).on( "click",function(){
         alert('click');
         r = $(this).is(':checked');
         //alert(r);
         if (r) {
         $(".login_account").fadeIn();
         }
         else {
         $(".login_account").fadeOut();
         }
         });*/



    }
    $('.datapic_input').datepicker({dateFormat: 'yy-mm-dd'});
</script> 









<script type="text/javascript" src="<?php echo base_url(); ?>js/formvalidate.js"></script>

<!--<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>durarthem/rtl/multiselect/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>durarthem/rtl/multiselect/jquery.multiselect.filter.css" />
<!--<script type="text/javascript" src="<?php echo base_url(); ?>durarthem/rtl/multiselect/prettify.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>durarthem/rtl/multiselect/jquery.multiselect.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>durarthem/rtl/multiselect/jquery.multiselect.filter.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/pnotify.custom.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/pnotify.custom.min.js"></script>



<!--<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/formValidation.css"/>-->




<div class="modal-ov">

    <div id="notif-mod" class="modal mini box modslideright">
        <div class="header">
            <span class="icon icon-subtractshape"></span>Notifications &amp; Alerts
            <button class="mini close"><span class="icon icon-remove no-mg"></span></button>
        </div>
        <div class="mod-body">
            <p class="alert warn">Low disk space remaining on partition E:/</p>
            <p class="alert info">MySQL updated to newest version (v5.6)</p>
            <p class="alert danger">Server #3 reporting high temperatures</p>
        </div>
    </div>

    <div id="ctrl-set" class="modal box modslideupalt">
        <div class="header"><span class="icon icon-cog"></span>Settings</div>
        <div class="mod-body no-pad">
            <div class="row ig8 bd-right"><label>Automatic Updates</label><input type="checkbox" class="checkbox big" checked></div>
            <div class="row ig8"><label>Automatic Backup</label><input type="checkbox" class="checkbox big" checked></div>
            <div class="row">
                <label>Color Scheme</label>
                <div id="color-scheme" class="buttons btn-radio">
                    <button class="light gray pressed ig8"><span class="icon icon-brightnessfull"></span>Light</button>
                    <button class="black ig8"><span class="icon icon-brightness"></span>Dark</button>
                </div>
            </div>
            <div id="sidebar-pos-row" class="row">
                <label>Sidebar Position</label>
                <div id="sidebar-pos" class="buttons btn-radio">
                    <button class="mini left pressed ig4">Left</button>
                    <button class="mini top ig4">Top</button>
                    <button class="mini right ig4">Right</button>
                    <button class="mini bottom ig4">Bottom</button>
                </div>
            </div>
            <div class="row">
                <label>Sidebar Color</label>
                <div class="inp-cont no-pad">
                    <input id="sidebar-color" value="#5b5a5a" data-defaultValue="#5b5a5a" placeholder="#5b5a5a">
                </div>
            </div>
            <div class="row">
                <label>Header Color</label>
                <div class="inp-cont no-pad">
                    <input id="header-color" placeholder="Transparent">
                </div>
            </div>
        </div>
        <div class="mod-act">
            <button id="res-set" class="red"><span class="icon icon-refresh"></span>Reset settings</button>
            <button class="close green"><span class="icon icon-ok"></span>Close</button>
        </div>
    </div>

    <div id="chat-mod" class="modal box chat-box modslidedownalt">
        <div class="mod-body nav-box left nav-big">
            <div class="nav-cont chat-cont">
                <div id="usr1-msg" class="chat-msg nav-item show">
                    <p class="sent">What's up, how are you liking your new office?<span class="msg-info">Yesterday, 11:48 PM</span></p>
                    <p class="rec">It's pretty good, definitely better than my old one. Thanks!<span class="msg-info">8:35 AM</span></p>
                    <p class="sent">No problem, keep up the good work and this one will be your old one.<span class="msg-info">23 minutes ago</span></p>
                    <p class="rec">Sure thing, you know me!<span class="msg-info">6 minutes ago</span></p>
                </div>
                <div id="usr2-msg" class="chat-msg nav-item">
                    <p class="rec">Guys, some help please? I deleted some files to make room for... uhm... some HD movies, and now I'm getting all kinds of errors.<span class="msg-info">8:53 AM</span></p>
                    <p class="sent">Dammit Johnny, I told you to stop downloading porn on company PC's.<span class="msg-info">9:30 AM</span></p>
                </div>
                <div id="lisa-msg" class="chat-msg nav-item">
                    <p class="rec">Aww crap I spilled coffee on my favorite kittens sweater!<span class="msg-info">21 minutes ago</span></p>
                    <p class="sent">Well, I know what I'm getting you for your birthday.<span class="msg-info">15 minutes ago</span></p>
                </div>
            </div>
<!--            <ul class="nav dark">
                <li data-nav="#usr1-msg" id="chat-usr1" class="sel bdrad-top-l">
                    <div class="contact-img"><img src="img/avatars/michael.jpg" alt="Contact avatar"></div>
                    <div class="contact-name">Michael</div>
                </li>
                <li data-nav="#usr2-msg" id="chat-usr2">
                    <div class="contact-img"><img src="img/avatars/johnny.jpg" alt="Contact avatar"></div>
                    <div class="contact-name">Johnny</div>
                </li>
                <li data-nav="#lisa-msg">
                    <div class="contact-img"><span class="icon icon-pictureframe"></span></div>
                    <div class="contact-name">Lisa</div>
                </li>
            </ul>-->
        </div>
        <div class="mod-act">
            <form class="chat-form">
                <input type="text" class="chat-inp" placeholder="Type in your message...">
                <button type="submit" class="chat-send green"><span class="icon icon-exit"></span>SEND</button>
            </form>
            <button class="close">Close</button>
        </div>
    </div>

</div>

</div><!--END WRAPPER-->



<!---jQuery Code-->
<script type='text/javascript'>

    $.fn.loadfns(function () { // PUT FUNCTIONS TO BE EXECUTED ON DOCUMENT READY HERE
        $('.spark').sparkline('html', {
            type: 'bar', height: '40px', barSpacing: 4, barColor: '#89A824', negBarColor: '#d6692f'
        });
        $.fn.dashGages();
        $.fn.dashChart();
        $('#intro').nanoScroller();
//$('#sidebar').nanoScroller();
        $("#table").tablesorter();
    });

// TOP BAR CHAT WIDGET

    $('#reply-btn').click(function () {
        setTimeout(function () {
            $('#chat-reply-input').focus();
        }, 1000);
    });

    $("#chat-reply-input").keyup(function (e) {
        if (e.keyCode === 13 && $(this).val()) {
            $('#chat-load').fadeIn(300).delay(500).fadeOut(300, function () {
                $('#chat-reply-input').val('').blur();
                $('#chat-confirm').fadeIn(300).delay(800).fadeOut(300);
            });
        }
        ;
    });

// TOP BAR CALENDAR WIDGET

    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

    $('#cal-day').text('' + day + ' ' + months[month] + '');

// GAUGES

    $.fn.dashGages = function () {
        /*cpu = new JustGage({ id:"cpu", value:25,min:0,max:100,title:"CPU (%)"});
         ram = new JustGage({ id:"ram", value:1.6,min:0,max:8.2,title:"RAM (GB)"});
         mem = new JustGage({ id:"mem", value:1.4,min:0,max:2.4,title:"HDD (TB)"});
         setInterval( function() {
         cpu.refresh(getRandomInt(10,50));
         ram.refresh(getRandomInt(0.9,4.2));
         }, 4000);*/
    };

    $('#chk-up-btn').click(function () {
        $('#chk-up-btn').attr('disabled', true);
        $('#sys-up-ok').fadeOut(150, function () {
            $('#sys-up-check').fadeIn(300).delay(2000).fadeOut(300, function () {
                $('#sys-up-ok').fadeIn(300);
                $('#chk-up-btn').attr('disabled', false);
            })
        })
    });

// TASKS & TO-DO LIST

    $('#task-box ul').sortable({
        placeholder: 'sort-placeholder',
        containment: 'parent',
        tolerance: 'pointer',
        axis: 'y'
    });
    $('#task-box ul').disableSelection();

    $('#task-box li:not(.task)').click(function () {
        $(this).toggleClass('done');
    });

// CHART PLOTTING

    $.fn.dashChart = function () {
        var d1 = [[0, 2], [2, 4], [4, 7], [6, 8], [8, 10], [10, 10], [12, 13], [14, 14], [16, 16], [18, 15], [20, 17]];
        d2 = [[4, 0], [8, 2], [10, 4], [14, 5], [16, 4], [18, 7], [20, 8]];
        $.plot($("#dash-chart"), [{data: d1, color: "#a0c420"}, {data: d2, color: "#d47c2f"}], {
            series: {
                lines: {show: true, fill: true},
                points: {show: true},
                resize: false},
            xaxis: {ticks: false},
            yaxis: {ticks: false},
            grid: {borderWidth: 0, hoverable: true}
        });


    };

// CONTROL PANEL SETTINGS

    $('#sidebar-pos .left').click(function () {
        $('#wrapper').removeClass('sidebar-hz sidebar-top sidebar-right sidebar-bottom');
    });
    $('#sidebar-pos .top').click(function () {
        $('#wrapper').addClass('sidebar-hz sidebar-top').removeClass('sidebar-right sidebar-bottom');
    });
    $('#sidebar-pos .right').click(function () {
        $('#wrapper').addClass('sidebar-right').removeClass('sidebar-hz sidebar-top sidebar-bottom');
    });
    $('#sidebar-pos .bottom').click(function () {
        $('#wrapper').addClass('sidebar-hz sidebar-bottom').removeClass('sidebar-right sidebar-top');
    });

// COLOR SCHEME SETTINGS

    var storageStatus = $.storage();
    if (storageStatus) {
        $('#res-set').click(function () {
            localStorage.clear();
            location.reload(true);
        });
        var storedColorScheme = localStorage.getItem('color-scheme');
        storedSidebarColor = localStorage.getItem('sidebar-color');
        storedHeaderColor = localStorage.getItem('header-color');
        userName = localStorage.getItem('user-name');
    } else {
        var storedColorScheme,
                storedSidebarColor,
                storedHeaderColor,
                userName;
        $('#res-set').attr('disabled', 'disabled');
    }

    var sidebarColor = $('#sidebar').css('background-color');
    function rgb2hex(rgb) {
        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        return "#" +
                ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2).toLowerCase();
    }
    $.minicolors.defaults.position = 'top right';

    if (storedColorScheme) {
        $('#color-scheme .black').toggleBtn();
    }
    ;
    if (storedSidebarColor) {
        $('#sidebar-color').val(storedSidebarColor)
    }
    ;
    if (storedHeaderColor) {
        $('#header-color').val(storedHeaderColor)
    }
    ;
    if ($('body').hasClass('dark') && !storedHeaderColor) {
        $('#header-color').val('#707070').attr('placeholder', '#707070')
    }
    ;
    $('#color-scheme .black').click(function () {
        if (!$('body').hasClass('dark')) {
            $('#load').fadeIn(400, function () {
                $('body').addClass('dark');
                $('#load').delay(400).fadeOut(800);
            });
            if (!storedHeaderColor)
                $('#header-color').val('#707070').attr('placeholder', '#707070');
            if (storageStatus)
                localStorage.setItem('color-scheme', 'dark');
        }
    });
    $('#color-scheme .light').click(function () {
        if ($('body').hasClass('dark')) {
            $('#load').fadeIn(400, function () {
                $('body').removeClass('dark');
                $('#load').delay(400).fadeOut(800);
            });
            if (!storedHeaderColor)
                $('#header-color').val('').attr('placeholder', 'Transparent');
            if (storageStatus)
                localStorage.removeItem('color-scheme');
        }
    });
    $('#sidebar-color').minicolors({
        change: function (hex) {
            $('#sidebar').css('background-color', hex).lightOrDark();
            if (storageStatus) {
                localStorage.setItem('sidebar-color', hex);
                if ($('#sidebar').hasClass('light')) {
                    localStorage.setItem('sidebar-light', 1)
                } else {
                    localStorage.removeItem('sidebar-light')
                }
                ;
            }
        }
    });
    $('#header-color').minicolors({
        change: function (hex) {
            $('.box:not(.mini) .header').css('background-color', hex).lightOrDark();
            if (storageStatus) {
                localStorage.setItem('header-color', hex);
                if ($('.box .header').hasClass('dark')) {
                    localStorage.setItem('header-dark', 1)
                } else {
                    localStorage.removeItem('header-dark')
                }
                ;
                if (!$(this).val()) {
                    localStorage.removeItem('header-dark');
                }
                ;
            }
            if (!$(this).val()) {
                $('.box .header').removeClass('dark');
            }
            ;
        }
    });

    $.fn.reverseChatMsg = function () {
        if ($(this).hasClass('rec')) {
            $(this).removeClass('rec').addClass('sent');
        } else if ($(this).hasClass('sent')) {
            $(this).removeClass('sent').addClass('rec');
        }
        ;
    };
    if (userName == 'm1chael') {
        $('#chat-usr1 img').attr('src', 'img/avatars/alex.jpg');
        $('#chat-usr1 .contact-name').text('Alex');
        $('#usr1-msg p').each(function () {
            $(this).reverseChatMsg();
        });
    }
    ;
    if (userName == 'Johnny 1337') {
        $('#chat-usr2 img').attr('src', 'img/avatars/alex.jpg');
        $('#chat-usr2 .contact-name').text('Alex');
        $('#usr2-msg p').each(function () {
            $(this).reverseChatMsg();
        });
    }
    ;

</script>







<link rel="stylesheet" href="<?php echo base_url(); ?>css/compiled/form-wizard.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url(); ?>css/compiled/form-wizard_rtl.css" type="text/css" media="screen" />

<style>

    .slider-frame.primary {
        background-color: rgba(91, 158, 214, 0.9);
    }
    .slider-frame {
        position: relative;
        display: inline-block;
        margin: 0 auto;
        width: 67px;
        background-color: #d5dde4;
        height: 23px;
        -moz-border-radius: 15px;
        border-radius: 15px;
        box-shadow: inset 0px 1px 5px 0px rgba(0, 0, 0, 0.3);
    }
    .slider-button {
        display: block;
        width: 37px;
        height: 23px;
        line-height: 23px;
        background: #fff;
        border: 1px solid #d0dde9;
        -moz-border-radius: 9px;
        border-radius: 9px;
        -webkit-transition: all 0.25s ease-in-out;
        -moz-transition: all 0.25s ease-in-out;
        transition: all 0.25s ease-in-out;
        color: #000;
        font-family: sans-serif;
        font-size: 11px;
        font-weight: bold;
        text-align: center;
        cursor: pointer;
    }

    .progress
    {
        background-color: #f5f5f5;
        border-radius: 4px;
        box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
        height: 20px;
        margin-bottom: 20px;
        overflow: hidden;
        webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
    }
    .progress-bar
    {
        background-color: #428bca;
        box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
        color: #fff;
        float: left;
        font-size: 12px;
        height: 100%;
        line-height: 20px;
        o-transition: width .6s ease;
        text-align: center;
        transition: width .6s ease;
        webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
        webkit-transition: width .6s ease;
        width: 0;
    }
    .progress-striped .progress-bar,.progress-bar-striped
    {
        background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
        background-image: -o-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
        background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
        background-size: 40px 40px;
        webkit-background-size: 40px 40px;
    }
    .progress.active .progress-bar,.progress-bar.active
    {
        animation: progress-bar-stripes 2s linear infinite;
        o-animation: progress-bar-stripes 2s linear infinite;
        webkit-animation: progress-bar-stripes 2s linear infinite;
    }
    .progress-bar[aria-valuenow="1"],.progress-bar[aria-valuenow="2"]
    {
        min-width: 30px;
    }
    .progress-bar[aria-valuenow="0"]
    {
        background-color: transparent;
        background-image: none;
        box-shadow: none;
        color: #777;
        min-width: 30px;
        webkit-box-shadow: none;
    }
    .progress-bar-success
    {
        background-color: #5cb85c;
    }
    .progress-striped .progress-bar-success
    {
        background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
        background-image: -o-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
        background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
    }
    .progress-bar-info
    {
        background-color: #5bc0de;
    }
    .progress-striped .progress-bar-info
    {
        background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
        background-image: -o-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
        background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
    }
    .progress-bar-warning
    {
        background-color: #f0ad4e;
    }
    .progress-striped .progress-bar-warning
    {
        background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
        background-image: -o-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
        background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
    }
    .progress-bar-danger
    {
        background-color: #d9534f;
    }
    .progress-striped .progress-bar-danger
    {
        background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
        background-image: -o-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
        background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
    }
</style>
<script src="<?php echo base_url(); ?>js/fuelux.wizard.js"></script>
<script type="text/javascript">
    $(function () {
        //alert('asd');
        /*$('#defaultForm').formValidation({
         message: 'This value is not valid',
         icon: {
         valid: 'glyphicon glyphicon-ok',
         invalid: 'glyphicon glyphicon-remove',
         validating: 'glyphicon glyphicon-refresh'
         },
         fields: {
         company_name_ar: {
         validators: {
         notEmpty: {
         message: 'The Arabic Name is required'
         }
         }
         },
         company_email: {
         validators: {
         notEmpty: {
         message: 'The email address is required'
         },
         emailAddress: {
         message: 'The input is not a valid email address'
         }
         }
         },
         }
         });
         */
        $wizard = $('#fuelux-wizard'),
                $btnPrev = $('.wizard-actions .btn-prev'),
                $btnNext = $('.wizard-actions .btn-next'),
                $btnFinish = $(".wizard-actions .btn-finish");

        $wizard.wizard().on('finished', function (e) {
            // wizard complete code
            window.location.assign("company_management")
        }).on("changed", function (e) {
            var step = $wizard.wizard("selectedItem");
            // reset states
            $btnNext.removeAttr("disabled");
            $btnPrev.removeAttr("disabled");
            $btnNext.show();
            $btnFinish.hide();

            if (step.step === 1) {
                $btnPrev.attr("disabled", "disabled");
            } else if (step.step === 3) {
                $btnNext.hide();
                $('.wizard-actions .next span').html('حفظ');

                //$btnFinish.show();
            }
        });

        $btnPrev.on('click', function () {
            $wizard.wizard('previous');
        });
        $btnNext.on('click', function () {
            $wizard.wizard('next');
        });
        /*$btnFinish.on('click', function () {
         $wizard.wizard('finished');
         });*/
    });
</script> 
<script type="text/javascript">

    $(function () {

        $(".next").click(function () {
            $(".step-pane.active").attr('id');
            var step = $wizard.wizard("selectedItem");
            stpno = step.step;
            frmno = stpno;
            validateFormOne(frmno);

            return false;

        });



        $('.slider-button').click(function () {
            if ($(this).hasClass("on")) {
                $(this).removeClass('on').html($(this).data("off-text"));
            } else {
                $(this).addClass('on').html($(this).data("on-text"));
            }
        });




    });
    $('.sample-form').validate();

</script> 





<script type="text/javascript" src="<?php echo base_url(); ?>js/script.js"></script> 


<style>
    .select2-container{width:100% !important;}
</style>
<link href="<?php echo base_url() ?>js/2select/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url() ?>js/2select/select2.min.js"></script>
<script>
    $(document).ready(function () {
		//alert('ready');
		getConvetData();
        $(".js-example-basic-single").select2();
    });
</script>


<div class="modal-ov">



    <script>

        function save_newitem() {

            comid = $("#newcomid").val();
            brid = $("#newbranchid").val();
            iname = $("#newitemname").val();
            categid = $("#newcategoryid").val();
            categtype = $("#newitemtype").val();
            newserial = $("#serialnumber").val();
            //newbarcode = $("#barcodenumber").val();
            checked_status = $("#barcode_generate2").is(':checked');
            if (iname != "" && categid != "") {

                $.ajax({
                    url: "<?php echo base_url() ?>ajax/save_new_item",
                    type: "POST",
                    data: $("#newitemform").serialize(),
                    success: function (e) {
                        if (e) {

                            //$(".mod-body").html("item has been added....");
                            alert("item has been added....");
                            $('.modal').removeClass('show').parents('.modal-ov').removeClass('show');

                        }
                        //$('#storequantity').val(e);
                    }
                });

            } else {
                alert("Please Enter Data To Save Item!");
            }
        }

        function save_newcustomer() {

            comid = $("#newcomid").val();
            brid = $("#newbranchid").val();

            fullname = $("#newfullname").val();
            phone_number = $("#newphone_number").val();


            categtype = $("#newitemtype").val();
            newserial = $("#serialnumber").val();
            //newbarcode = $("#barcodenumber").val();
            //checked_status = $("#barcode_generate2").is(':checked');
            if (fullname != "" && phone_number != "") {

                $.ajax({
                    url: "<?php echo base_url() ?>ajax/save_new_customer",
                    type: "POST",
                    data: $("#newitemform").serialize(),
                    success: function (e) {
                        if (e) {

                            //$(".mod-body").html("item has been added....");
                            alert("item has been added....");
                            $('.modal').removeClass('show').parents('.modal-ov').removeClass('show');

                        }
                        //$('#storequantity').val(e);
                    }
                });

            } else {
                alert("Please Enter Data To Save Customer!");
            }
        }

    </script>

    <div id="demo-modal" class="modal box">
        <div class="header">Modal Window</div>
        <div class="mod-body pad-l">

        </div>
        <div class="mod-act">
            <button class="close">Close</button>
        </div>
    </div>
    <div id="demo-modal-wait" class="modal box">
        <div class="header">Message</div>
        <div class="mod-body pad-l">
				Please Wait
        </div>
        <div class="mod-act">
            <button class="close">Close</button>
        </div>
    </div>
    <div id="demo-modalnn" class="modal box"><div class="header">Search</div><form id="search_form" action="<?php echo base_url(); ?>customers/search_data" method="post"><div class="mod-body pad-l" style="text-align:center;"><label>Search</label><input type="text" id="search_id" name="search_id" /> <br /> <div class="g3"><input type="button"  class="tag green" value="Search" onclick="searchFormData()" /></div></div></form><div class="mod-act"><button onclick="closediv()" id="closediv" class="close">Close</button>
        </div></div> 

</div>

</body>
</html>









