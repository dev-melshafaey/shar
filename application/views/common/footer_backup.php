<!--footer-->

<footer>
  <div class="footer">
    <div class="copyrights"><a href="http://www.durar-it.com" target="_blank">Designed & Developed by <img src="<?php echo base_url();?>images/login/durar_it.png" width="15" height="15" /> Durar Smart Solutions </a></div>
    <div class="reserved">Copyright � <?php echo date('Y'); ?></div>
  </div>
</footer>
<!--end of footer-->

</div>
<!--end  of body bg--> 

<!-- BEGIN JAVASCRIPTS --> 
<!-- Load javascripts at bottom, this will reduce page load time --> 
<script src="//oss.maxcdn.com/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.nicescroll.js" type="text/javascript"></script> 
<!--<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script> -->
<!-- ie8 fixes --> 
<!--[if lt IE 9]>
   <script src="<?php echo base_url();?>js/excanvas.js"></script>
   <script src="<?php echo base_url();?>js/respond.js"></script>
   <![endif]--> 

<!--<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script> -->
<script src="<?php echo base_url();?>js/common-scripts.js"></script> 

<!--<script src="<?php echo base_url();?>js/script.js"></script> 
-->
<!--menu--> 
<!--<script type="text/javascript" src="<?php echo base_url();?>js/javascript.js"></script> -->

<!-- Load jQuery, SimpleModal and Basic JS files --> 

<script type='text/javascript' src='<?php echo base_url();?>js/jquery.simplemodal.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>js/basic.js'></script> 
<script type="text/javascript">//<![CDATA[ 
$(window).load(function(){
$(".closediv").click(function() {
    $(this).parent().remove();
});
});//]]>  

var base_url = '<?php echo base_url(); ?>';

</script> 
</body></html>