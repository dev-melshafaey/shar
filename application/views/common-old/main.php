<?php $this->load->view('common/meta'); ?>
<!--body with bg-->


<div class="body">
    <header>
        <?php $this->load->view('common/header'); ?>

        <link href="<?php echo base_url(); ?>css/lib/jquery.dataTables.css" type="text/css" rel="stylesheet" />

        <!-- this page specific styles -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/compiled/datatables.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
        <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>fileUpload/css/jquery.fileupload.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>fileUpload/css/jquery.fileupload-ui.css">
        <!-- CSS adjustments for browsers with JavaScript disabled -->
        <link href="<?php echo base_url(); ?>css/lib/bootstrap.datepicker.css" type="text/css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>jasny-bootstrap/css/jasny-bootstrap.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/validationEngine.jquery.css" type="text/css"/>

    </header>
    <?php $this->load->view('common/left-navigations'); ?>
    <div class="content">





        <!-- settings changer -->
        <!--<div class="skins-nav">
            <a href="#" class="skin first_nav selected">
                <span class="icon"></span><span class="text">Default</span>
            </a>
            <a href="#" class="skin second_nav" data-file="css/compiled/skins/dark.css">
                <span class="icon"></span><span class="text">Dark skin</span>
            </a>
        </div>-->   
        <div id="pad-wrapper">


            <?php $this->load->view($inside); ?>




        </div>
        </div>
                <div class="row">  <div class="footer-img fcont" ><img src="<?php echo base_url(); ?>images/logo-footer.png" /></div>  </div>       
    </div> 
</div>  

<script>

</script>   
<?php $this->load->view('common/footer'); ?>
		
