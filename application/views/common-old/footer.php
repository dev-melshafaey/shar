<div id="loading_image_div" style="position:fixed;
     width:50px; /*image width */
     height:39px; /*image height */
     left:50%; 
     top:50%;
     margin-left:-25px; /*image width/2 */
     margin-top:-20px; /*image height/2 */; display:none; "> <img src="<?php echo base_url(); ?>images/486.gif" /> </div>


<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.js"></script> 


<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script src="<?php echo base_url(); ?>js/formvalidate.js"></script> 
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script> 

<!-- knob --> 
<script src="<?php echo base_url(); ?>js/jquery.knob.js"></script> 
<!-- flot charts --> 
<script src="<?php echo base_url(); ?>js/jquery.flot.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.flot.stack.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.flot.resize.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.uniform.min.js"></script> 
<script src="<?php echo base_url(); ?>js/select2.min.js"></script> 
<script src="<?php echo base_url(); ?>js/theme.js"></script> 
<script src="<?php echo base_url(); ?>js/fuelux.wizard.js"></script> 
<script src="<?php echo base_url(); ?>js/wysihtml5-0.3.0.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/formValidation.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/formValidation.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/framework/bootstrap.js"></script> 
<script src="<?php echo base_url(); ?>fileUpload/js/vendor/jquery.ui.widget.js"></script> 

<script src="<?php echo base_url(); ?>fileUpload/js/jquery.iframe-transport.js"></script> 
<!-- The basic File Upload plugin --> 
<script src="<?php echo base_url(); ?>fileUpload/js/jquery.fileupload.js"></script> 
<!-- The File Upload processing plugin --> 
<script src="<?php echo base_url(); ?>fileUpload/js/jquery.fileupload-process.js"></script> 
<!-- The File Upload image preview & resize plugin --> 
<script src="<?php echo base_url(); ?>fileUpload/js/jquery.fileupload-image.js"></script> 
<!-- The File Upload audio preview plugin --> 
<script src="<?php echo base_url(); ?>fileUpload/js/jquery.fileupload-audio.js"></script> 
<!-- The File Upload video preview plugin --> 
<script src="<?php echo base_url(); ?>fileUpload/js/jquery.fileupload-video.js"></script> 
<!-- The File Upload validation plugin --> 
<script src="<?php echo base_url(); ?>fileUpload/js/jquery.fileupload-validate.js"></script> 
<!-- The File Upload user interface plugin --> 
<script src="<?php echo base_url(); ?>fileUpload/js/jquery.fileupload-ui.js"></script> 
<!-- The main application script --> 
<script src="<?php echo base_url(); ?>js/bootstrap.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?PHP echo base_url(); ?>js/databale/media/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo base_url(); ?>js/databale/examples/resources/syntax/shCore.css">
<script type="text/javascript" language="javascript" src="<?PHP echo base_url(); ?>js/databale/media/js/jquery.dataTables.js"></script> 
<script type="text/javascript" language="javascript" src="<?PHP echo base_url(); ?>js/databale/examples/resources/syntax/shCore.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script> 
<script src="<?php echo base_url(); ?>js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script> 
<script src="<?php echo base_url(); ?>assets/prettify/run_prettify.js"></script>

<script src="<?php echo base_url(); ?>js/tabs/easy-responsive-tabs.js" type="text/javascript"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>js/script.js"></script> 
<script>
    $('#horizontalTab').easyResponsiveTabs({
//Types: default, vertical, accordion     
        type: 'default',
//auto or any width like 600px
        width: 'auto',
// 100% fit in a container
        fit: true,
// Start closed if in accordion view
        closed: 'accordion',
// Callback function if tab is switched
        activate: function () {
        }

    });
</script> 
<script type="text/javascript">


    $(document).ready(function () {
        // binds form submission and fields to the validation engine
        //$('#form1').validationEngine('validate');
        $("#form1").validationEngine();
		
		$(document).keypress(function(e) {
			istat = $("input").is(":focus");
			if(!istat){
			//		alert(e.which);
				if(e.which == 65) {
	 					//redirectPage('jobs/add_jobs');
						$("#site_lang").val('arabic');
						$("#change_lang").submit();
    			}
				if(e.which == 69) {
	 					//redirectPage('jobs/add_jobs');
						$("#site_lang").val('english');
						$("#change_lang").submit();
    			}
				
			}
		});
    });


</script> 
<script>
//User Filter
    $(document).ready(function () {

        $("#ad_account").click(function () {
            //alert('click');
            r = $(this).is(':checked');
            //alert(r);
            if (r) {
                $(".login_account").fadeIn();
            }
            else {
                $(".login_account").fadeOut();
            }
        });


    });
</script> 

<!--<script type='text/javascript' src='<?php echo base_url(); ?>js/basic.js'></script> -->
<!--<script type='text/javascript' src='<?php echo base_url(); ?>js/osx.js'></script> -->
<!--<script type='text/javascript' src='<?php echo base_url(); ?>js/script.js'></script> --> 

<script type="text/javascript">

    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
   

    var base_url = '<?php echo base_url(); ?>';

</script> 
<script type="text/javascript">
    $(function () {
        //alert('asd');
        $('#defaultForm').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                company_name_ar: {
                    validators: {
                        notEmpty: {
                            message: 'The Arabic Name is required'
                        }
                    }
                },
                company_email: {
                    validators: {
                        notEmpty: {
                            message: 'The email address is required'
                        },
                        emailAddress: {
                            message: 'The input is not a valid email address'
                        }
                    }
                },
            }
        });
        $wizard = $('#fuelux-wizard'),
                $btnPrev = $('.wizard-actions .btn-prev'),
                $btnNext = $('.wizard-actions .btn-next'),
                $btnFinish = $(".wizard-actions .btn-finish");

        $wizard.wizard().on('finished', function (e) {
            // wizard complete code
        }).on("changed", function (e) {
            var step = $wizard.wizard("selectedItem");
            // reset states
            $btnNext.removeAttr("disabled");
            $btnPrev.removeAttr("disabled");
            $btnNext.show();
            $btnFinish.hide();

            if (step.step === 1) {
                $btnPrev.attr("disabled", "disabled");
            } else if (step.step === 4) {
                $btnNext.hide();
                $btnFinish.show();
            }
        });

        $btnPrev.on('click', function () {
            $wizard.wizard('previous');
        });
        $btnNext.on('click', function () {
            $wizard.wizard('next');
        });
    });
</script> 
<script type="text/javascript">

    $(function () {

        $(".next").click(function () {
            $(".step-pane.active").attr('id');
            var step = $wizard.wizard("selectedItem");
            stpno = step.step;
            frmno = stpno;
            validateFormOne(frmno);

            return false;

        });

        $('.slider-button').click(function () {
            if ($(this).hasClass("on")) {
                $(this).removeClass('on').html($(this).data("off-text"));
            } else {
                $(this).addClass('on').html($(this).data("on-text"));
            }
        });

        // jQuery Knobs
        $(".knob").knob();



        // jQuery UI Sliders
        $(".slider-sample1").slider({
            value: 100,
            min: 1,
            max: 500
        });
        $(".slider-sample2").slider({
            range: "min",
            value: 130,
            min: 1,
            max: 500
        });
        $(".slider-sample3").slider({
            range: true,
            min: 0,
            max: 500,
            values: [40, 170],
        });



        // jQuery Flot Chart
        var visits = [[1, 50], [2, 40], [3, 45], [4, 23], [5, 55], [6, 65], [7, 61], [8, 70], [9, 65], [10, 75], [11, 57], [12, 59]];
        var visitors = [[1, 25], [2, 50], [3, 23], [4, 48], [5, 38], [6, 40], [7, 47], [8, 55], [9, 43], [10, 50], [11, 47], [12, 39]];

        var plot = $.plot($("#statsChart"),
                [{data: visits, label: "Signups"},
                    {data: visitors, label: "Visits"}], {
            series: {
                lines: {show: true,
                    lineWidth: 1,
                    fill: true,
                    fillColor: {colors: [{opacity: 0.1}, {opacity: 0.13}]}
                },
                points: {show: true,
                    lineWidth: 2,
                    radius: 3
                },
                shadowSize: 0,
                stack: true
            },
            grid: {hoverable: true,
                clickable: true,
                tickColor: "#f9f9f9",
                borderWidth: 0
            },
            legend: {
                // show: false
                labelBoxBorderColor: "#fff"
            },
            colors: ["#a7b5c5", "#30a0eb"],
            xaxis: {
                ticks: [[1, "JAN"], [2, "FEB"], [3, "MAR"], [4, "APR"], [5, "MAY"], [6, "JUN"],
                    [7, "JUL"], [8, "AUG"], [9, "SEP"], [10, "OCT"], [11, "NOV"], [12, "DEC"]],
                font: {
                    size: 12,
                    family: "Open Sans, Arial",
                    variant: "small-caps",
                    color: "#697695"
                }
            },
            yaxis: {
                ticks: 3,
                tickDecimals: 0,
                font: {size: 12, color: "#9da3a9"}
            }
        });

        function showTooltip(x, y, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 30,
                left: x - 50,
                color: "#fff",
                padding: '2px 5px',
                'border-radius': '6px',
                'background-color': '#000',
                opacity: 0.80
            }).appendTo("body").fadeIn(200);
        }

        var previousPoint = null;
        $("#statsChart").bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(0),
                            y = item.datapoint[1].toFixed(0);

                    var month = item.series.xaxis.ticks[item.dataIndex].label;

                    showTooltip(item.pageX, item.pageY,
                            item.series.label + " of " + month + ": " + y);
                }
            }
            else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        });







    });


</script> 
<script type="text/javascript">

    function generateBarcode() {

        checked_status = $("#barcode_generate").is(':checked');
        if (checked_status) {

            //var itemname = $("#serialnumber").val();
            var itemname = $("#barcodenumber").val();
            $.ajax({
                url: config.BASE_URL + "ajax/create_barcode",
                type: 'post',
                data: {itemname: itemname},
                cache: false,
                success: function (data)
                {
                    imagename = itemname + ".gif";
                    imagepath = config.BASE_URL + 'barcode_image/' + itemname + ".gif";
                    $("#image_preview").attr('src', imagepath);
                    $("#image_preview").show();
                    $("#barcode_image").val(imagename);
                    //$('#employeecode').val(data);
                }
            });
        }
        else {
            $("#image_preview").show();
        }
    }

    function getproductsBySupplier(suplier) {
        //alert('asd');
        $("#supplier_id").attr("disabled", true);
        if (suplier) {

            $.ajax({
                url: config.BASE_URL + "ajax/getProductBySuplier",
                type: 'post',
                data: {supplier_id: suplier},
                cache: false,
                success: function (data)
                {
                    console.log(data);
                    if (data)
                        $("#productid").html(data);



                }
            });
        }
        else {
            $("#image_preview").show();
        }
    }




</script> 
<script>
	//Writing by Noor 
	if($('#new_data_table').length > 0)
	{
		var thleng = $('#new_data_table thead th').length-1;
		 $('#new_data_table thead th').each( function (index, value) {				
				 var title = $.trim($(this).html());
				 var attr_id = $('#new_data_table thead th').eq( $(this).index() ).attr('id');
				 if(index!='0' && thleng!=index)
				 {				 
				 	$(this).html(title+'<input type="search" class="form-control '+attr_id+' search_filter" placeholder="'+title+'" />' );
				 }
		});
		
		 var new_data_table = $('#new_data_table').DataTable({
			 "ordering": false,
			 "oLanguage": {
				 "sSearch": "",
				 "oPaginate": { "sNext": "التالی", "sPrevious": "السابق" }
				}
			});
		
		new_data_table.columns().eq(0).each(function (colIdx) 
		{
			$('input', new_data_table.column(colIdx).header()).on('keyup change', function() 
			{
				new_data_table.column(colIdx).search(this.value).draw();
			});
		});	
		
		$('.search_filter').keyup(function(){
			$('#new_data_table td').removeHighlight().highlight($(this).val());
		});	
		
		new_data_table.on( 'draw', function () {
    		//tiptop();
		} );
	}
	
	function getPendingPurchaseDetail(id){
		//$("#modal_data")
		//getPurcahseItems
		$( "#modal_data" ).load(config.BASE_URL + "purchase_management/getPurcahseItems/"+id, { id: id }); 	}
	function getCustomerData(id)
	{	$( "#modal_data_comes_here" ).load(config.BASE_URL + "customers/getCustomerData_ajax/"+id, { id: id }); 	}
        
        function addCustomerData(){
            
            $( "#modal_data_comes_here" ).load(config.BASE_URL + "customers/addnewcustomer2/",adduser_option());
           
        }
        function adduser_option(){
            
            // $("#ad_account").click(function () {
            /* $( "#ad_account[type=checkbox]" ).on( "click",function(){
                alert('click');
                r = $(this).is(':checked');
                //alert(r);
                if (r) {
                    $(".login_account").fadeIn();
                }
                else {
                    $(".login_account").fadeOut();
                }
            });*/
             
       
            
        }
    $('.datapic_input').datepicker({dateFormat: 'yy-mm-dd'});    
</script> 
<script src="<?php echo base_url(); ?>js/jquery.highlight-4.js" type="text/javascript"></script> 


<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>-->
<script>
    /*$(document).ready(function () {*/
        //$(".styled-select select").select2();
        //$(".js-example-basic-single2").select2();
    //});
</script>
    

<script src="<?php echo base_url(); ?>js/function.js" type="text/javascript"></script> 
                
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <script>$(document).ready(function () { $( "#ad_account[type=checkbox]" ).on( "click", function(){alert('ddd')}); });</script> 
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" style="text-align:right;">مشاهده</h4>
            </div>
            <div class="modal-body" id="modal_data_comes_here">
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              
            </div>
          </div>
        </div>
    </div>
    
<div class="modal fade" id="myModalN" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" style="text-align:right;">View Detail</h4>
            </div>
            <div class="modal-body" id="modal_data">
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              
            </div>
          </div>
        </div>
    </div>
    </div>