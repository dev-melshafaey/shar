<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Miscellaneous Configuration
|--------------------------------------------------------------------------
|
| Define miscellaneous configuration here
|
*/

$config['project_name'] = "Business Solution";

$config['upload'] = "uploads/";


/*
|--------------------------------------------------------------------------
| Database Tables
|--------------------------------------------------------------------------
|
| Define all database tables name here
|
*/

//Companies database table

$config['table_banks'] 						= 'bs_bank';
$config['table_modules'] 					= 'bs_modules';
$config['table_customers'] 					= 'bs_customers';
$config['table_services'] 					= 'bs_services';
$config['table_companies'] 					= 'bs_company';
$config['table_company_branches'] 			= 'bs_company_branch';
$config['table_permissions'] 				= 'bs_permission';
$config['table_permission_types'] 			= 'bs_permission_type';
$config['table_users'] 						= 'bs_users';
$config['table_users_bank_transactions']	= 'bs_users_bank_transaction';
$config['table_cat']						= 'bs_category';
$config['table_location']					= 'bs_location';
$config['table_area']						= 'bs_area';
$config['table_financial']					= 'bs_financial';
$config['table_country']					= 'bs_country';
$config['table_city']						= 'bs_city';
$config['table_store']						= 'bs_store';
$config['table_suppliers']					= 'bs_suppliers';
$config['table_item']						= 'bs_item';


/* End of file project.php */
/* Location: ./application/config/project.php */