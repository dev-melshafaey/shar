<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Outcome extends CI_Controller
{
	/*
	* Properties
	*/

    private $_data	=	array();

//----------------------------------------------------------------------
	/*
	* Constructor
	*/
   public function __construct()
   {
		parent::__construct();
		
		//load Home Model
	   $this->load->model('outcome_model','outcome');
	   $this->_data['udata'] = userinfo_permission();
   }
   
//----------------------------------------------------------------------

	/*
	* Expensis Listing Page
	*/
   public function index()
   {
	   // Load Home View
	   $this->load->view('expenses', $this->_data);
   }
//----------------------------------------------------------------------

	/*
	* Add New Expensis Form
	*/
   public function add_expenses()
   {
	   //echo "<pre>";
	   //print_r($_POST);
	   //exit;
	   // Load Home View
	   if($this->input->post()){
				echo "<pre>";
				$postData = $this->input->post();
				print_r($postData);
				exit;
				if($postData['expense_id'] !=""){
					//an_expenses
					$data['type'] = $postData['select2'] ;
					$this->db->insert('expenses_charges', $data);
				}
				else{
					$data['expense_name'] = $postData['expense_title'] ;
					$this->db->insert('expenses_charges',$data);
				}
				//print_r($postData);
	   }
	   $this->load->view('add-expenses', $this->_data);
   }
   
   
   function getAutoSearch(){
		// Data could be pulled from a DB or other source
		
		
		$term = trim(strip_tags($_GET['term']));
		$customers = $this->outcome->getExpenseByTitle($term);
		// Cleaning up the term
		
		// Rudimentary search
		foreach($customers as $eachloc){
				// Add the necessary "value" and "label" fields and append to result set
				$eachloc['value'] = $eachloc['id'];
				$eachloc['label'] = "{$eachloc['cus']}";
				$matches[] = $eachloc;
		}
		// Truncate, encode and return the results
		$matches = array_slice($matches, 0, 5);
		print json_encode($matches); 
   }

//----------------------------------------------------------------------
}
?>