<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sales extends CI_Controller {
    /*
     * Properties
     */

    private $_data = array();

//----------------------------------------------------------------------
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        //load Home Model
        $this->load->model('sales_model', 'sales');
        $this->load->model('users/users_model', 'users');
        $this->lang->load('main', get_set_value('site_lang'));
        $this->_data['udata'] = userinfo_permission();
    }

//----------------------------------------------------------------------

    /*
     * Customer Listing  Page
     */
    /* public function index()
      {
      // Load Home View
      $this->load->view('customers',	$this->_data);
      } */
    public function index() {

        $this->options();
    }

    public function view_all() {
        // Load Home View

        $this->_data['users'] = $this->sales->get_all_users();
        $this->_data['inside2'] = 'sales';
        $this->_data['inside'] = 'add-sales-form';
        $this->load->view('common/tabs', $this->_data);
    }

    public function statics($id = null) {
        // Load Home View


        $this->_data['customerData'] = $this->sales->getCustomerInvoicesPayments($id);
        $this->_data['customerbalalnce'] = $this->sales->getCustomerBalance($id);
        //echo "<pre>";
        //print_r($this->_data['customerData ']);
        $this->_data['inside'] = 'statics';
        $this->load->view('common/main', $this->_data);
    }

    public function getCustomerData_ajax($customerid) {
        $customerData = $this->sales->users_list($customerid);
        $cus = $customerData[0];

        $html = '<div  style="text-align:right !important; direction:rtl;">';
        $html .= '<h3>' . $cus->cfullname . '<br />';
        $html .= lang('Code') . ' : #' . $userdata->employeecode . '</h3>';
        $html .= '<code> <span class="pop_title">' . lang('Mobile-Number') . ' : </span>';
        $html .= '<div class="pop_txt">' . $cus->phone_number . '</div>';
        $html .= '</code> ';
        $html .= '<!--line--> ';
        $html .= '<code> <span class="pop_title">' . lang('Contact-Number') . ' : </span>';
        $html .= '<div class="pop_txt">' . $cus->office_number . '</div>';
        $html .= '</code> ';
        $html .= '<!--line--> ';
        $html .= '<code> <span class="pop_title">' . lang('Fax-Number') . ': </span>';
        $html .= '<div class="pop_txt">' . $cus->fax_number . '</div>';
        $html .= '</code> ';
        $html .= '<!--line--> ';
        $html .= '<code> <span class="pop_title">' . lang('Email-Address') . ' : </span>';
        $html .= '<div class="pop_txt">' . $cus->email_address . '</div>';
        $html .= '</code> ';
        $html .= '<!--line--> ';
        $html .= '<code> <span class="pop_title">' . lang('Address') . ' : </span>';
        $html .= '<div class="pop_txt">' . $cus->address . '</div>';
        $html .= '</code> ';
        $html .= '<!--line--> ';
        $html .= '<code> <span class="pop_title">' . lang('Type') . ' : </span>';
        $html .= '<div class="pop_txt">' . $cus->permissionhead . '</div>';
        $html .= '</code> <code> <span class="pop_title">' . lang('Responsable-Name') . ' : </span>';
        $html .= '<div class="pop_txt">' . $cus->responsable_name . '</div>';
        $html .= '</code> <code> <span class="pop_title">' . lang('Responsable-Phone') . ' : </span>';
        $html .= '<div class="pop_txt">' . $cus->responsable_phone . '</div>';
        $html .= '</code> <code> <span class="pop_title">' . lang('Notes') . ' : </span>';
        $html .= '<div class="pop_txt">' . $cus->notes . '</div>';
        $html .= '</code> ';
        $html .= '<!--line--> ';
        $html .= '<code> <span class="pop_title">' . lang('Status') . ' :</span>';
        $html .= '<div class="pop_txt">' . $cus->status . '</div>';
        $html .= '</code><br clear="all">';
        $html .= '</div>';
        echo $html;
    }

    public function invoices($customerid = NULL) {
        // Load Home View
        $this->_data['sale_invoices'] = $this->sales->getSaleInvocies($customerid);
        $this->_data['inside'] = 'customer-invoices';
        $this->load->view('common/main', $this->_data);
    }

//----------------------------------------------------------------------

    /*
     * Add Customer
     */
    public function add($customerid = NULL) {


        if ($this->input->post('submit_customer')) {
            $data['ownerid'] = ownerid();
            $data = $this->input->post();

            //print_r($data);
            //exit;


            unset($data['submit_customer']);


            // insert data into database
            if ($customerid != '') {
                $this->sales->update_customer($data, $customerid);
            } else {
                $this->sales->add_customer($data);
            }

            //redirect(base_url().'customers/options');
            //exit();
        } else {
            if ($customerid != '') {
                $this->_data['customer'] = $this->sales->get_customer_detail($customerid);
            }

            $this->_data['company_id'] = $this->sales->get_company_id(ownerid());
            $this->_data['branch_id'] = $this->sales->get_branch_id($this->_data['company_id']);
            //$this->_data['emCode'];
            // Load Home View
            //$this->load->view('add-customer-form', $this->_data);
            $this->_data['inside'] = 'add-customer-form';
            $this->load->view('common/main', $this->_data);
        }
    }

    function addsales($id = null) {

        if ($this->input->post()) {
            $data = array();
            //$data = $this->input->post();
            //print_r($data);
            //exit;
            //unset($data['submit_customer']);
            //$data['userid']=  post('bs_sales_id');
            $data['bs_user_id'] = post('bs_user_id');
            $data['bs_sales_commession_type'] = post('bs_sales_commession_type');
            $data['bs_sales_commession_how'] = post('bs_sales_commession_how');
            $data['bs_sales_commession'] = post('bs_sales_commession');

            // insert data into database
            if (post('userid') != '') {
                $this->_data['sdata'] = $this->sales->get_sales(post('userid'));
                $this->sales->update('bs_sales', $data, post('userid'));
                redirect(base_url() . 'sales/addsales/' . post('userid') . "?e=10");
            } else {
                $this->sales->inserData('bs_sales', $data);
                redirect(base_url() . 'sales/addsales/' . $id . "?e=11");
            }

            //redirect(base_url().'customers/options');
            //exit();
        } else {
            if ($id != '') {
                $this->_data['sdata'] = $this->sales->get_sales($id);
            }

            //$this->_data['emCode'];
            // Load Home View
            $this->_data['users'] = $this->sales->get_all_users();
            //$this->load->view('add-customer-form', $this->_data);
            
            $this->_data['inside2'] = 'sales';
            $this->_data['inside'] = 'add-sales-form';
            $this->load->view('common/tabs', $this->_data);
        }
    }

    /*     * *
     * 
     */

    public function delete_customers() {
        $all_ids = $this->input->post('ids');


        if (!empty($all_ids)) {

            $this->sales->delete_customers($all_ids);

            $this->session->set_flashdata('success', 'Record has been deleted.');

            redirect(base_url() . 'customers/options');
            exit();
        } else {
            $this->session->set_flashdata('error', 'There is No record Selected.');

            redirect(base_url() . 'customers/options');
            exit();
        }
    }

    function getCustomeDues() {
        $post = $this->input->post();
        //	echo "<pre>";
        //	print_r($post);
        //$post['customer_id'];
        $dues = $this->sales->getCustomersDues($post['customer_id']);
        echo json_encode($dues);
    }

    function pay() {

        $this->_data['charges_all'] = $this->sales->getAllcharges();
        //$this->load->view('add_payment',$this->_data);
        $this->_data['inside'] = 'add_payment';
        $this->load->view('common/main', $this->_data);
    }

    function add_payment_post() {
        $post = $this->input->post();
        //echo "<pre>";
        //print_r($post);
        //exit;
        $customerData = $this->sales->getCustomerPaymetns($post['customer_id']);
        //print_r($customerData);
        if ($customerData->totalpayment > $customerData->totalcharges) {
            $remaining = $customerData->totalpayment - $customerData->totalcharges;
        }
        if ($remaining > 0) {

            $last_payment = $this->sales->getLastpaymentIdByCustomerMultiple($post['customer_id'], '');
            if ($last_payment->payment_amount < $remaining) {

                if ($post['payment_amount'] > $last_payment->payment_amount)
                    $last_payment = $this->sales->getLastpaymentIdByCustomerMultiple($post['customer_id'], true);
                else {
                    $last_payment->payment_amount;
                }
            }

        }

        
        //echo $post['invoice_payments'];
        
        //print_r($post);

        $data['user_id'] = $this->session->userdata('userid');

        if (isset($post['invoice_payments']))
            $jobcharges = $post['invoice_payments'];



        $userid = $this->session->userdata('bs_userid');
        //$this->session->userdata('bs_userid');
        $user = $this->users->get_user_detail($userid);
        $data['refrence_id'] = $post['customer_id'];
        $data['payment_amount'] = $post['payment_amount'];
        $data['payment_type'] = 'purchase';
        ///print_r($data);
        //exit;

        if ($post['refrence_by'] == 'against_reference') {

            $data['refernce_type'] = 'against';
        } elseif ($post['refrence_by'] == 'on_account') {

            $data['refernce_type'] = 'onaccount';
        } elseif ($post['refrence_by'] == 'clear_dues') {

            $data['refernce_type'] = 'onaccount';
        } else {
            $data['refernce_type'] = $post['refrence_by'];
        }



        if (trim($post['refrence_by']) != "clear_dues" && trim($post['deduct_from']) != "from_account") {
            //echo "if";
            $payment_id = $this->sales->insert($data, 'payments');
        }

        if ($post['refrence_by'] == "clear_dues" && $post['deduct_from'] != "from_account") {
             echo "if2";
            //print_r($data);
            //$payment_id = $this->sales->insert($data, 'payments');
        }
        //exit;

        if ($post['payment_method'] == 'bank') {
            $transaction_data['branch_id'] = $user->branchid;
            $transaction_data['transaction_by'] = 'invoice';
            $transaction_data['transaction_type'] = 'debit';
            $transaction_data['value'] = $post['payment_amount'];
            $transaction_data['amount_type'] = 'sales';
            $transaction_data['amount_type'] = $post['payment_type'];
            $transaction_data['account_id'] = $post['account_id'];
            $transaction_data['notes'] = $post['notes'];
            $transaction_data['clearance_date'] = date('Y-m-d');
            //$transaction_id = $this->sales->insert($transaction_data, 'an_company_transaction');
        }


        if (!empty($jobcharges) && $post['refrence_by'] == 'against_reference') {
            
            //echo "against_reference";
            //exit;
            $this->on_against($post, $last_payment, $jobcharges, $payment_id);
        } elseif ($post['refrence_by'] == 'clear_dues') {
            $this->on_dues($post, $last_payment, $job_data);
            //echo 'clear_dues';
        }

        //die('die');
        //redirect(base_url() . 'customers/pay_reciep');
        do_redirect('sales/pay?e=10');
        exit();
    }

    function on_against($post, $last_payment, $jobcharges, $payment_id) {

        //var_dump($post);
        //echo '<hr/>';
        if ($post['deduct_from'] == "from_account") {
            if (count($last_payment) > 1) {
                
            } else {
                $payment_amount = $post['payment_amount'];
                $payment_id = $last_payment->id;
            }
        } else {
            $payment_amount = $post['payment_amount'];
        }

        $total = count($jobcharges['invoice_id']);
        //$jobcharges = $post['purchase_payments'];
        $payment_amount = $post['payment_amount'];
        
        
        //echo $payment_amount;
        
        $invoice_status = 0;
        
        //echo $total;
        
        //die();
        for ($a = 0; $a < $total; $a++) {
            
            
            
            if ($payment_amount > 0) {

                
                //var_dump($post['invoice_payments']['invoice_id']);
        
                if ($payment_amount >= $post['invoice_payments']['total_amount'][$a]) {
                    //echo "if";
                    $paid_invoice_amount = $post['invoice_payments']['total_amount'][$a];
                    $payment_amount = $payment_amount - $post['invoice_payments']['total_amount'][$a];
                    $invoice_status = 1;
                } elseif ($payment_amount < $post['invoice_payments']['total_amount']) {
                    //echo "else";
                    $paid_invoice_amount = $payment_amount;
                    $payment_amount = $payment_amount - $payment_amount;
                    $invoice_status = 0;
                    $prev_payment = $this->sales->getpaidInvoiceAmount($post['invoice_payments']['invoice_id'][$a]);
                    $total_payment = $paid_invoice_amount + $prev_payment;
                    //echo $post['invoice_payments']['total_amount'][$a];
                    if ($total_payment >= $post['invoice_payments']['total_amount'][$a]) {
                        $invoice_status = 1;
                    }
                }

                //echo "Paid to invoice".$paid_invoice_amount."<br>";
                //echo "remaingin".$payment_amount."<br>";
                //echo "status".$invoice_status."<br>";

                $inv_pay['purchase_id'] = $jobcharges['invoice_id'][$a];
                $inv_pay['payment_id'] = $payment_id;
                $inv_pay['payment_amount'] = $paid_invoice_amount;
                //echo "<pre>";
                //print_r($inv_pay);
                //exit;
                
                $this->sales->insert($inv_pay, 'purchase_payments');
                
                $job_data['purchase_id'] = $jobcharges['invoice_id'][$a];
                $job_data['payment_id'] = $payment_id;


                $chargesData['payment_amount'] = $post['payment_amount'];
                $job_data['invoice_paid'] = $jobcharges['invoice_paid'][$a];
                $job_data['total_amount'] = $jobcharges['total_amount'][$a];
                $paid_amount = $job_data['invoice_paid'] + $post['payment_amount'];

                if ($job_data['total_amount'] > $paid_amount) {
                    $invoice_main['status'] = 2;
                }
                $invoice_main['purchase_status'] = $invoice_status;

                if (isset($invoice_main)) {
                    $this->db->where('purchase_id', $job_data['invoice_id']);
                    $this->db->update('an_purchase', $invoice_main);
                }
            }else{
                
                
                //echo '2';
            }


            if ($remaining >= 0) {

                /* $job_data['job_id'] = $jobcharges['jobid'][$a];
                  $job_data['payment_amount'] = $charges_rem;
                  $job_data['customer_id'] = $post['customer_id'];
                  $job_data['payment_purpose'] = 'sales';
                  $job_data['payment_by'] = 'Job';
                  $job_data['parent_id'] = $payment_id;
                  $job_data['customer_id'] = $post['customer_id'];
                 */
            }
        }
    }

    function on_account() {
        
    }

    function on_dues($post, $last_payment, $job_data) {


        //payment_amount
        $invData = $this->sales->getCustomersDues($post['customer_id']);
        //echo "<pre>";
        //print_r($invData);
        //print_r($last_payment);
        //exit;

        $job_data['purchase_id'] = $invData->invoice_id;
        if (isset($payment_id))
            $job_data['payment_id'] = $payment_id;

        if ($post['deduct_from'] != 'from_direct') {
            if (isset($last_payment) && !empty($last_payment)) {
                //	$last_payment['payment_amount'];

                if ($post['payment_amount'] != "") {

                    $job_data['payment_amount'] = $post['payment_amount'];
                    if ($last_payment->payment_amount != "" && $last_payment->payment_amount > 0) {
                        if ($last_payment->payment_amount != "" && $last_payment->payment_amount != 0 && $post['payment_amount'] <= $last_payment->payment_amount) {
                            //echo "lasttt";
                            $job_data['payment_amount'] = $post['payment_amount'];
                        }
                    } else {
                        $job_data['payment_amount'] = $post['payment_amount'];
                    }


                    $job_data['payment_id'] = $last_payment->id;
                }
            }
        } else {

            $job_data['payment_amount'] = $post['payment_amount'];
        }

        //print_r($job_data);
        // exit;
        //$this->sales->insert($job_data, 'purchase_payments');
        
        //echo 'insert';

        if ($job_data['payment_amount'] == $invData->sales_amount)
            $invoice_main['purchase_status'] = 1;

        if (isset($invoice_main)) {
            //$this->db->where('purchase_id', $invData->invoice_id);
            //$this->db->update('an_purchase', $invoice_main);
            //echo 'update';
        }
    }

    function getCustomePayments() {

        $post = $this->input->post();
        $this->_data['payment_data'] = $this->sales->getCustomerPaymetns($post['customer_id']);
        echo json_encode($this->_data['payment_data']);
    }

    public function getAutoSearchJob() {
        // Data could be pulled from a DB or other source


        $term = trim(strip_tags($_GET['term']));
        $customers = $this->sales->getJobs($term);
        // Cleaning up the term
        // Rudimentary search
        foreach ($customers as $eachloc) {
            // Add the necessary "value" and "label" fields and append to result set
            $eachloc['value'] = $eachloc['id'];
            $eachloc['label'] = "{$eachloc['id']}";
            $matches[] = $eachloc;
        }
        // Truncate, encode and return the results
        $matches = array_slice($matches, 0, 5);
        print json_encode($matches);
    }

    function view_payments($customerid = "") {

        //$this->_data
        if ($customerid != "") {

            $this->_data['payment_data'] = $this->sales->getCustomerPaymetns($customerid);
            $this->_data['invoice_data'] = $this->sales->getCusmer_invoices($customerid);
            ///print_r($this->_data['invoice_data']);
        }

        //exit;
        $this->load->view('view_payments', $this->_data);
    }

//----------------------------------------------------------------------
}

?>