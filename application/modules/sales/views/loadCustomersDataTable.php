<div style="float: left; margin-left: 12px; margin-top: 4px;">
                From Date 
                <input type="text" id="from_date_filter" value="<?php echo $from_date_filter;?>"> 
                To Date 
                <input type="text" id="to_date_filter" value="<?php echo $to_date_filter;?>" >
                <input type="button" onclick="updateCustomersDataTableDiv();" value="Search">
            </div>
<table width="100%" align="left" id="usertable">
              <thead>
              <tr>

                <th width="1%" id="no_filter"><label for="checkbox"></label></th>
                <th width="20%">Customer Name</th>
                <th width="12%" >Customer Code</th>
                <th width="12%">Status</th>
                <th width="5%">Account Type</th>
                <th width="5%">Mobile</th>
                <th width="5%">Email</th>
                <th width="5%">Contact</th>
                <th width="9%">Branch</th>
                <th width="7%" id="no_filter">&nbsp;</th>
              </tr>
              </thead>
              <tfoot>
              <tr>
                <td  style="background-color:#afe2ee"><input type="checkbox" name="checkbox" id="checkboxall" /></td>
                <td  style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield2" id="textfield2"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield3" id="textfield3"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield4" id="textfield4"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"></td>
                <td style="background-color:#afe2ee"></td>
                <td  style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"></td>
              </tr>
              </tfoot>
              <?php
			  	$cnt = 0;
              	foreach($this->customers->getCustomerListsWithFilter($from_date_filter,$to_date_filter) as $userdata) {
					$cnt++;
			  ?>
              <tr>
                <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $userdata->id; ?>" value="<?php echo $userdata->id; ?>" /></td>
                <td ><a href="<?php // echo base_url().customers/invoices;?>/<?php // echo $userdata->id; ?>"><?php echo $userdata->customer_name; ?></a></td>
              
                <td><?php echo $userdata->branch_code.$userdata->customer_code; ?></td>
                <td><?php echo $userdata->status; ?></td>
               	<td><?php echo ucfirst($userdata->account_type); ?></td>
               	<td><?php echo $userdata->mobile_number; ?></td>
               	<td><?php echo $userdata->email; ?></td>
               	<td><?php echo $userdata->contact_number; ?></td>
                <td><?php echo ucfirst($userdata->branch_name); ?></td>
                <td><?php edit_button('add/'.$userdata->id); ?> <a id="<?php echo $userdata->id; ?>" href='#' class='basic'><img src="<?php echo base_url(); ?>images/internal/i_info.png" width="28" height="18" border="0" /></a>          <a href="<?php echo base_url();?>customers/customers/deleteCustomerById/<?php echo $userdata->id; ?>" onclick="window.confirm('Are you Sure to delete this customer?')">Delete</a>
                  <!-- modal content -->
                  
                  <div id="basic-modal-content" class="dig<?php echo $userdata->id; ?>">
                    <h3><?php echo $userdata->customer_name; ?><br />
                      Code : #<?php echo $userdata->branch_code.$userdata->customer_code; ?></h3>
                    
                    <code> <span class="pop_title">Customer code : </span>
                    <div class="pop_txt"><?php echo $userdata->branch_code.$userdata->customer_code; ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Status : </span>
                    <div class="pop_txt"><?php echo $userdata->status; ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Account type : </span>
                    <div class="pop_txt"><?php echo $userdata->account_type; ?></div>
                    </code>
                    <!--line--> 
                    <code> <span class="pop_title">Mobile number : </span>
                    <div class="pop_txt"><?php echo $userdata->mobile_number; ?></div>
                    </code> 
                   <!--line--> 
                    <code> <span class="pop_title">Email : </span>
                    <div class="pop_txt"><?php echo $userdata->email; ?></div>
                    </code> 
                     <!--line--> 
                    <code> <span class="pop_title">Contact : </span>
                    <div class="pop_txt"><?php echo $userdata->contact_number; ?></div>
                    </code> 
                    <code> <span class="pop_title">Branch name : </span>
                    <div class="pop_txt"><?php echo($userdata->branch_name); ?></div>
                    </code>
                   <code> <span class="pop_title">Fax : </span>
                    <div class="pop_txt"><?php echo($userdata->fax); ?></div>
                    </code>
                    <code> <span class="pop_title">Address : </span>
                    <div class="pop_txt"><?php echo($userdata->address); ?></div>
                    </code>
                    <!--line--> 
                    <code> <span class="pop_title">Created :</span>
                    <div class="pop_txt"><?php echo($userdata->created); ?></div>
                    </code> 
                    	<?php
							//get_permission_text();
						?>
                        
                       
                      </ul>
                    </div>
                    </code>
                    </div></td>
              </tr>
              <?php } ?>
            </table>
<script>
$(document).ready(function(){	

$( "#from_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to_date_filter" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from_date_filter" ).datepicker( "option", "maxDate", selectedDate );
}
});


	if($('#usertable').length > 0)
	{
		 $('#usertable tfoot td').each( function () {
			var title = $('#usertable thead th').eq( $(this).index() ).text();
			var attr_id = $('#usertable thead th').eq( $(this).index() ).attr('id');
			$(this).html( '<input class="'+attr_id+' searchfilter" type="text" placeholder="'+title+'" />' );		
		 });
		
		
		
		var user_table = $('#usertable').DataTable();	
		user_table.columns().eq(0).each( function ( colIdx ) {		
			$( 'input', user_table.column( colIdx ).footer() ).on( 'keyup change', function () {
				user_table
					.column( colIdx )
					.search( this.value )
					.draw();
			} );
			
		} );
		$('.no_filter, #usertable_filter').hide();
		
		$('.searchfilter').keyup(function(){
			console.log($(this).val());
			$('#usertable td').removeHighlight().highlight($(this).val());
		});
	}


	$('.basic').click(function() {
	
		var id = $(this).attr('id');
			$('.dig'+id).modal();

		return false;
	});
	
});
		
		 </script>
          