
<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" type="text/css"/>
<div class="row head">
    <div class="col-md-12">
        <h4>

            <div class="title"> <span><?php breadcramb(); ?> >> فاتوره مرتجعه</span> </div>


        </h4>
        <?php error_hander($this->input->get('e')); ?>
    </div>
</div>

<div id="horizontalTab">

    <ul class="resp-tabs-list">

        <li><?php echo lang('Customer') ?></li>
        <li><?php echo lang('tproduct') ?></li>


    </ul>
    <div class="resp-tabs-container">

        <div>
            <div class="col-md-12 form-group" id="customer">


                <div class="col-md-3 form-group">
                    <label class="text-warning"><?php echo lang('fullname') ?></label>
                    <div><?php echo $invoice[0]->fullname ?></div>

                </div>

                <div class="col-md-3 form-group">
                    <label class="text-warning"><?php echo lang('Email-Address') ?></label>
                    <div><?php echo $invoice[0]->email_address ?></div>

                </div>

                <div class="col-md-3 form-group">
                    <label class="text-warning"><?php echo lang('Phone') ?></label>
                    <div><?php echo $invoice[0]->phone_number ?></div>

                </div>
                <br clear="all"/>
                <br clear="all"/>


                <?php if (get_member_type() == '1' OR get_member_type() == '5'): ?>
                    <div class="col-md-4 form-group">
                        <label class="text-warning"><?php echo lang('Company-Name') ?> :</label>
                        <br>
                        <div  class="ui-select" style="width:100%">
                            <div class="styled-select " style="width:100%">
                                <?php company_dropbox('companyid', $invoice[0]->companyid, $user->companyid); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form-group">
                        <div class="field-box">
                            <label class="text-warning"><?php echo lang('Branch-Name') ?> :</label>
                            <br>
                            <div  class="ui-select" style="width:100%">
                                <div class="styled-select " style="width:100%">
                                    <?php company_branch_dropbox('branchid', $invoice[0]->branchid, $user->branchid, $user->companyid); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?PHP endif; ?>
                <div class="col-md-2 form-group">
                    <input id="customerid-val" name="customerid-val" type="hidden" value=''/>
                    <input id="customerid" name="customerid" type="hidden" value=''/>
                    <label class="text-warning"><?php echo lang('Customer') ?> :</label>
                    <br>
                    <input id="customername" disabled="disabled" name="customername" value="<?php echo $invoice[0]->fullname ?>" type="text" class="form-control"/>
                </div>
                <div class="col-md-2 form-group">
                    <label class="text-warning"></label>


                </div>


                <script>

                    $(document).ready(function () {



                        $(".get_balance").load(config.BASE_URL + "sales_management/get_balance/" +<?php echo $invoice[0]->customer_id ?>);


                        $.ajax({
                            url: config.BASE_URL + "ajax/branch_list",
                            type: 'post',
                            data: {companyid: "<?php echo $invoice[0]->companyid ?>", branch:<?php echo $invoice[0]->branchid ?>},
                            cache: false,
                            //dataType:"json",
                            success: function (data)
                            {
                                var response = $.parseJSON(data);

                                $('#branchid').html(response.dropdown);
                                $('#storeid').html(response.store);
                                $('#customerid').html(response.customer);
                            }
                        });





                    });

                </script>
                <div class="col-md-12 get_balance">
                    <div class="col-md-3 green"><?php echo lang('balance1') ?> 0</div>
                    <div class="col-md-3 green"><?php echo lang('balance2') ?> 0</div>
                    <div class="col-md-3 red"><?php echo lang('balance4') ?> 0</div>
                    <div class="col-md-3 red"><?php echo lang('balance3') ?> 0</div>
                </div>


            </div>
            <br clear="all"/>
        </div>

        <div>
            <form action="<?php echo base_url(); ?>sales_management/do_return_in/<?php echo $id?>" method="post">    
            <table class="table table-bordered invoice-table mb20">
                <thead>
                <th>#</th>
                <th><?php echo lang('Image') ?></th>
                <th><?php echo lang('Product-Name') ?></th>
                <th><?php echo lang('Discription') ?></th>
                <th><?php echo lang('sforone') ?></th>
                <th><?php echo lang('Quantity') ?></th>
                <th><?php echo lang('Total') ?></th>

                </tr>
                </thead>
                <tbody>
                    <?php $get_product = get_invoice_items($id) ?>
                    <?php if ($get_product): ?>
                        <?php $count = 1 ?>
                        <?php $totalp = 0 ?>
                        <?php foreach ($get_product as $product): ?>
                            <tr id="tr_<?php echo $product->itemid ?>">
                                <td ><?php echo $product->itemid ?></td>
                                <td ><img src="<?php echo base_url() ?>uploads/item/<?php echo $product->store ?>/<?php echo $product->product_picture ?>" width="50" height="50" /></td>



                                <td style="text-align:center"> <?php echo $product->itemname ?> </td>
                                <td id="description" contentEditable="true"><?php echo $product->notes ?> </td>


                                <td id="oneitem<?php echo $product->itemid ?>" style="text-align:center" contentEditable="true"><?php echo $product->iip ?></td>



                                <td id="quantity_tp<?php echo $product->itemid ?>" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup=""><?php echo $product->invoice_item_quantity ?> </td>

                                <td id="totalAmount" style="text-align:center" contentEditable="true" ><?php echo $product->iip ?></td>
                                
                                <input type="hidden" name="invoice_payment" value="<?php echo $product->inv_payment_id_all ?>"/>
                                <input type="hidden" name="payment" value="<?php echo $product->payment_id_all ?>"/>
                                <input type="hidden" name="totalpay" value="<?php echo $product->totalpay; ?>"/>
                            </tr>
                            <?php //$totalp+=$product->iip; ?>
                        <?php endforeach; ?>

                    <?php else: ?>        
                        <tr><td colspan="7"><?php echo lang('no-data') ?></td></tr>
                    <?php endif; ?>
                    <tr  class="grand-total">



                        <td colspan="1" >

                        </td> 
                        <td></td>

                        <td><div id="totalmin" style="display: none"></div></td>
                        <td>   


                        </td>

                        <td></td>                  
                        <td><?php echo lang('discount') ?></td>
                        <td><strong id="netTotal"><?php echo $invoice[0]->invoice_totalDiscount; ?></strong></td>   



                    </tr>
                    <tr  class="grand-total">



                        <td colspan="1" >

                        </td> 
                        <td></td>

                        <td><div id="totalmin" style="display: none"></div></td>
                        <td>   


                        </td>

                        <td></td>

                        <td><?php echo lang('Total-Price') ?></td>
                        <td><strong id="netTotal"><?php echo $invoice[0]->invoice_total_amount; ?></strong></td>    



                    </tr>
                    <?PHP $get_sales = get_invoice_sales($id); ?>
                    <?php if ($get_sales): ?>
                        <?php foreach ($get_sales as $get_sale): ?>
                            <tr  class="">



                                <td colspan="1" >

                                </td> 
                                <td><?php echo lang('Sales-Name') ?></td>
                                <td><?php echo $get_sale->fullname ?></td>
                                <td><?php echo lang('Payment-amount') ?></td>
                                <td><?php echo $get_sale->amount ?></td>
                                <td></td>
                                <td></td>    



                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>


                    <tr  class="">



                        <td colspan="1" >

                        </td> 
                        <td></td>

                        <td></td>
                        <td>   


                        </td>

                        <td></td>

                        <td><?php //echo lang('Total-Price')  ?></td>
                        <td><strong id="netTotal"><?php //echo $invoice[0]->invoice_total_amount;  ?></strong>
                            
                                <input type="checkbox" name="return_tobalance" value="1"/>
                                <input type="hidden" name="return_tobalance_value" value="<?php echo $invoice[0]->invoice_total_amount; ?>"/>
                                <input type="hidden" name="customerid" value="<?php echo $invoice[0]->customer_id; ?>"/>
                                
                                <span>ارجاع المبلغ لحساب العميل</span>
                                <br/>
                                <br/>
                                <input type="submit" class="btn-flat primary" value="تنفيذ" name=""/>
                            

                        </td>    



                    </tr>
                </tbody>
            </table>
            </form>
        </div>
    </div>
</div>    