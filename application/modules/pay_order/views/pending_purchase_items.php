<script type="text/javascript">
    function check_status(obj, id) {
        is_val = $(obj).val();
        if (is_val == '1') {
            $("#rej_res" + id).hide();
        }
        else {
            $("#rej_res" + id).show();
        }
    }
</script>
<div class="table-wrapper users-table">
    <form action="<?php echo base_url(); ?>inventory/add_confirmation_items" method="post" id="form1" class="" name="frm_customer" autocomplete="off">
        <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
        <input type="hidden" name="userid" id="userid" value="<?php echo $user->userid; ?>" />
        <input type="hidden" name="invoiceid" id="invoiceid" value="<?php if (!empty($purchase_invocie)) {
    echo $purchase_invocie[0]->invoice_id;
} ?>" />

        <div class="row head">
            <div class="col-md-12">
                <h4>

                    <div class="title"> <span><?php echo lang('main') ?><?php //breadcramb();   ?></span> </div>


                </h4>
<?php error_hander($this->input->get('e')); ?>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <table id="new_data_table">
                    <thead>
                        <tr>
                            <th width="1%" id="no_filter"><label for="checkbox"></label></th>
                            <th width="20%"><?php echo lang('item_name') ?></th>
                            <th width="20%"><?php echo lang('Quantity') ?></th>
                            <th width="5%"><?php echo lang('Date') ?></th>
                            <th width="7%" id="no_filter">&nbsp;</th>
                        </tr>
                    </thead>

                    <?php
                    if ($purchase_invocie) {
                        foreach ($purchase_invocie as $purchasedata) {
                            //echo "<pre>";
                            //print_r($purchasedata);
                            $cnt++;
                            ?>
                            <tr>
                                <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $purchasedata->id; ?>" value="<?php echo $purchasedata->id; ?>" /></td>
                                <td ><a href="<?php echo base_url(); ?>purchase_management/purchase_invoices/<?php echo $purchasedata->supplier_id; ?>"><?php echo $purchasedata->itemname; ?></a></td>
                                <td><?php echo $purchasedata->quantity; ?></td>
                                <td><?php echo $purchasedata->submissiondate; ?></td>
                                <td><?php //edit_button('addnewcustomer/'.$userdata->userid);   ?>
                                    <input type="radio" name="receipt[<?php echo $i; ?>]" value="1" id="accept<?php echo $purchasedata->piv_id; ?>"   onclick="check_status(this, '<?php echo $purchasedata->piv_id; ?>')"/><span style="padding-left: 5px;"><?php echo lang('complete') ?></span>
                                    <input type="radio" name="receipt[<?php echo $i; ?>]" value="0" id="reject<?php echo $purchasedata->piv_id; ?>"  onclick="check_status(this, '<?php echo $purchasedata->piv_id; ?>')"/><span style="padding-left: 5px;"><?php echo lang('incomplete') ?></span>
                                    
                                    <input type="hidden" name="store_id<?php echo $purchase->piv_id; ?>" id="store_id" value="<?php echo $purchasedata->store; ?>" />
                                    <input type="hidden" id="reason<?php echo $purchase->piv_id; ?>" name="reason<?php echo $purchase->piv_id; ?>"/>
                                    <input type="hidden" id="quantiy_avail<?php echo $purchase->piv_id; ?>" name="quantiy_avail<?php echo $purchase->piv_id; ?>" />
                                    <input type="hidden" id="trigger<?php echo $purchase->piv_id; ?>" name="trigger<?php echo $purchase->piv_id; ?>" />

                                    <!-- modal content -->

                                </td>

                            </tr>
    <?php } ?>
<?php } ?>
                </table>
            </div>
        </div>
        <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>


<!--<input type="submit" class="send_icon" value=""/>-->

    </form>
</div>
