<form id="newitemform" class="sample-form" action="" method="post">
    <div class="g8">
        <label class=""><?php echo lang('Company-Name') ?></label>
        <div class="">
            <div class="ui-select" >
                <div class="">
                    <?php company_dropbox('newcomid', $hd->comid); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="g8">
        <label class=""><?php echo lang('Branch-Name') ?></label>
        <div class="">
            <div class="ui-select" >
                <div class="">

                    <?php //company_branch_dropbox('branchid', $user->branchid, $user->companyid); ?>
                    <?php company_branch_dropbox('newbranchid', $user->branchid, $user->companyid); ?>
                    <span class="arrow arrowselectbox">&amp;</span>
                </div>
            </div>
        </div>



    </div>   
    <br clear="all"/>
    <div class="g8">
        <label class=""><?php echo lang('Product-Name-ar')  ?></label>
        <div class="">
            <input name="newitemname[arabic]" id="newitemname[arabic]" type="text" value="<?php echo $hd->itemname; ?>"  class="required  valid form-control" onblur="generateBarcode()"/>
        </div>
    </div>
    <div class="g8">
        <label class=""><?php echo lang('Product-Name-en')  ?></label>
        <div class="">
            <input name="newitemname[english]" id="newitemname[english]" type="text" value="<?php echo $hd->itemname; ?>"  class="required  valid form-control" onblur="generateBarcode()"/>
        </div>
    </div>
    <br clear="all"/>
    <div class="g16">
        <label class=""><?php echo lang('Category') ?></label><br clear="all"/>
        <div class="col-md-10">
            <div class="ui-select" >
                <div class="">
                    <?php category_dropbox('newcategoryid', $hd->categoryid, $hd->comid, $hd->itemtype); ?>
                    <span class="arrow arrowselectbox">&amp;</span>
                </div>
            </div>
        </div>
    </div>
    <br clear="all"/>
    <div class="g8">
        <label class=""><?php echo lang('Category-Type') ?></label>
        <div class="">
            <div class="ui-select" >
                <div class="">
                    <?php product_type('newitemtype', $hd->itemtype); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="g8">
        <label class=""><?php echo lang('Serial-No')  ?></label>
        <div class="">
            <input name="serialnumber" id="serialnumber"  value="<?php echo $hd->serialnumber; ?>" type="text"  class="form-control field2014"/>
        </div>
    </div>
<br clear="all"/>
    <div class="g8">
        <label class=""><?php echo lang('BarCode-Number')  ?></label>
        <div class="">
            <input name="barcodenumber" id="barcodenumber"  value="<?php echo $hd->barcodenumber; ?>" type="text"  class="form-control field2014"/>
        </div>
    </div>
    <div class="g8">
        <label class="">Generate-Bar-Code <?php //echo lang('Generate-Bar-Code')   ?>  </label><br clear="all"/>
        <div class="">
            <input name="barcode_generate" id="barcode_generate" value=""  type="checkbox" onclick="generateBarcode()"  class="form-control" style="width: 10%"/>
        </div>

    </div>
    <br clear="all"/>
    <div class="col-md-9">
        <img  src=""  id="image_preview" style="display:none;"/>	
        <input name="barcode_image" id="barcode_image" value=""  type="hidden"   class="form-control"/>
    </div>
    
    
    <input name="" type="button" class="submit_btn btn-glow green" value="<?php echo lang('Add')  ?>" onclick="save_newitem()"  />
</form>