
<div class="table-wrapper users-table">
    <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">
        <div class="row head">
            <div class="col-md-12">
                <h4>

                    <div class="title"> <span><?php echo lang('main') ?><?php //breadcramb();  ?></span> </div>


                </h4>
                <?php error_hander($this->input->get('e')); ?>
            </div>
        </div>
		


        <div class="row">
            <div class="col-md-12">
                <table id="example">
                    <thead>
                        <tr>
                            <th width="1%" id="no_filter"><label for="checkbox"></label></th>
                            <th width="20%"><?php echo lang('customer_name')?></th>
                            <th width="20%"><?php echo lang('Invoice-No')?></th>
                            <th width="12%"><?php echo lang('Total-Price')?> </th>
                            <th width="5%"><?php echo lang('Total-Paid')?></th>
                            <th width="5%"><?php echo lang('Invoice-Date')?></th>
                            <th width="7%" id="no_filter">&nbsp;</th>
                        </tr>
                    </thead>

                    <?php
                    $cnt = 0;
                    $total_amount = 0;
                    $total_paid = 0;
					$userid = $this->session->userdata('bs_userid');
					$pendingPurchase = $this->inventory_model->getPendingSales($userid);	
                    if($pendingPurchase){
                    foreach ($pendingPurchase as $purchasedata) {
                        //echo "<pre>";
                        //print_r($purchasedata);
                        $cnt++;
                        ?>
                        <tr>
                            <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $purchasedata->id; ?>" value="<?php echo $purchasedata->id; ?>" /></td>
                            <td ><a href="<?php echo base_url(); ?>purchase_management/purchase_invoices/<?php echo $purchasedata->supplier_id; ?>"><?php echo $purchasedata->supplier_name; ?></a></td>
                            <td><?php echo $purchasedata->invoicenumber; ?></td>
                            <td><?php echo $purchasedata->totalamount;
                    $total_amount+=$purchasedata->totalamount; ?></td>
                            <td><?php echo $purchasedata->paid;
                    $total_paid+=$purchasedata->paid; ?></td>
                            <td><?php echo $purchasedata->invoicedate; ?></td>
                            <!--<td><?php echo date('d/m/Y', strtotime($purchasedata->invoice_bill_date)); ?></td>-->
                            <td><?php //edit_button('addnewcustomer/'.$userdata->userid);  ?>
                                												
                               <div class="groub_buuton"><br clear="all"><a class="btn-flat success pull-right" href="<?php echo base_url(); ?>purchase_management/getPurcahseItems/<?php echo $purchasedata->invoiceid; ?>">Confirm order</a></div>
                                <!-- modal content -->

                                </td>
                                
                        </tr>
<?php } ?>
<?php } ?>
                </table>
            </div>
        </div>
        <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>

        
 <!--<input type="submit" class="send_icon" value=""/>-->
    </form>
</div>
