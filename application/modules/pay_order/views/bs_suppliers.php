

<div class="table-wrapper users-table">
    <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">
        <div class="row head">
            <div class="col-md-12">
                <h4>

                    <div class="title"> <span><?php breadcramb(); ?></span> </div>


                </h4>
                <?php error_hander($this->input->get('e')); ?>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">
 <?php action_buttons('add_supplier', $cnt); ?>
                <table id="new_data_table">
                    <form action="<?php echo form_action_url('bulk_delete'); ?>" id="listing" method="post" autocomplete="off">
                        <thead>
                            <tr>
                                <th id="no_filter"><label for="checkbox"></label><?php echo lang('All') ?></th>
                                <th ><?php echo lang('Supplier-Nam') ?></th>
                                <th ><?php echo lang('Invoice-No') ?></th>
                                <th  ><?php echo lang('Mobile-Number') ?></th>
                                <th  ><?php echo lang('Fax-Number') ?></th>
                                <th  ><?php echo lang('Contact-Number') ?></th>
                                <th ><?php echo lang('Notes') ?></th>
                                <th  id="no_filter"></th>
                            </tr>
                        </thead>

                        <?php
                        $cnt = 0;
                        if($suppliers){
                        foreach ($suppliers as $supplier) {
                            $cnt++;
                            ?>
                            <tr>
                                <td ><input type="checkbox" class="allcb" name="u[]" id="u_<?php echo $supplier->supplierid; ?>" value="<?php echo $supplier->supplierid; ?>" /></td>
                                <td ><a href="<?php echo base_url(); ?>purchase_management/supplier_invoices/<?php echo $supplier->supplierid; ?>"><?php echo $supplier->suppliername; ?></a></td>
                                <td><?php echo get_count_ft('purchase_invoice_items','supplier_id',$supplier->supplierid) ?></td>
                                <td><?php echo $supplier->phone_number; ?></td>
                                <td><?php echo $supplier->fax_number; ?></td>
                                <td><?php echo $supplier->office_number; ?></td>
                                <td><?php echo $supplier->notes; ?></td>
                                <td >
                                    <?php edit_button('add_supplier/' . $supplier->supplierid); ?>

                                </td>

                            </tr>
                        </form>
                    <?php } ?>
                    <?php } ?>
                </table>
            </div>
        </div>
        <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>
       
</div>
<!-- END PAGE --> 



