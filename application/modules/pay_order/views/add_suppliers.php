

<?php $this->load->view('common/meta'); ?>
<!--body with bg-->


<div class="body">
    <header>
        <?php $this->load->view('common/header'); ?>

        <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
        <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>fileUpload/css/jquery.fileupload.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>fileUpload/css/jquery.fileupload-ui.css">
        <!-- CSS adjustments for browsers with JavaScript disabled -->
        <link href="<?php echo base_url(); ?>css/lib/bootstrap.datepicker.css" type="text/css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>jasny-bootstrap/css/jasny-bootstrap.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/validationEngine.jquery.css" type="text/css"/>

    </header>
    <?php $this->load->view('common/left-navigations'); ?>

    <div class="content">
        <?php
//	echo "<pre>";
//	print_r($company);
        ?>

        <!-- settings changer -->
        <div class="skins-nav">
            <a href="#" class="skin first_nav selected">
                <span class="icon"></span><span class="text">Default</span>
            </a>
            <a href="#" class="skin second_nav" data-file="<?php echo base_url(); ?>css/compiled/skins/dark.css">
                <span class="icon"></span><span class="text">Dark skin</span>
            </a>
        </div>

        <div id="pad-wrapper">
            <div class="row form-wrapper">
                <div class="col-md-12 col-xs-12">



                    <div id="main-content" class="main_content">
                        <div class="title title alert alert-info">
                            <span>Add Supplier</span>
                        </div>

                        <div class="notion title title alert alert-info">* Please Understand  Clearly All The Data Before  Entering</div>
                        
                        
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
 <script type="text/javascript">
 
 	function removeCharges(ind){
		$("#charge_index"+ind).remove();
	} 
	
	function removeChargesFromDb(ind,id){
		
			suplier_id = $("#id").val();
			$.ajax({
				url: "<?php echo base_url(); ?>purchase_management/delete_supplier_charges/",
				type: 'post',
				dataType:"html",
				data: {charges_id:id,supplier_id:suplier_id},
				cache: false,
				success: function(data){
							$("#charge_index"+ind).remove();
				}
			});
	}
 	$(document).ready(function(){
			
	 pausecontent = new Array();
	
    <?php foreach($charges_all as $key => $val){ ?>
        pausecontent.push('<?php echo $val['charge_name']; ?>');
    <?php } ?>
	
	
	$( "#current_charges" ).autocomplete({
source: pausecontent
});
	
		charges_index = 0;
	<?php
		if(isset($supplier_charge) && !empty($supplier_charge)){
				?>
				charges_index = <?php echo count($supplier_charge); ?>
				<?php
		}
	?>

	charge_val = 0;
	$("#current_charges").keydown(function(){
			c_val = $(this).val();
			if(c_val !=""){	
				$.ajax({
				url: "<?php echo base_url(); ?>jobs/get_charges_id/",
				type: 'post',
				dataType:"html",
				data: {charges_id:c_val},
				cache: false,
				success: function(data){
						console.log(data);
						if(!isNaN(data)){
							charge_val= data;
						
							chareges_name = $("#current_charges").val();
								
													}
				}
			});
			}
	});
	
	
	$(document).keypress(function(e) {
    if(e.which == 13) {
     //   alert('You pressed enter!');
		if($("#current_charges").is(":focus")){
			//  alert('You pressed enter!');
				if($("#current_charges").val() != ""){
							
							html_hd = '<div id="charge_index'+charges_index+'" class="invoice_no"><strong>'+chareges_name+'</strong><a href="javascript:void(0)" onclick="removeCharges('+charges_index+')">X</a> <input type="hidden" name="charges_ids[]" id="charges_ids['+charges_index+']" value="'+charge_val+'" class="soil'+charges_index+'"></div>';
			
							//$("#current_charges_id").val(data);
							//html_hd += '<input type="hidden" name="charges_ids['+charges_index+']" id="charges_ids['+charges_index+']" value="'+data+'" class="soil'+charges_index+'">';
							charges_index++;			
							$("#selected_charges").append(html_hd);
							
							$("#current_charges").val('');
				}
				
				e.stopPropagation();
				return false;
				
				
		}
		
    }
});
	});
	
 </script> 
 <style>
 .invoice_no{
	 background:#FFF !important;
}
.invoice_no a{
	padding-left:8px !important;
}
 </style>
            <form action="<?php echo base_url(); ?>purchase_management/add_new_supplier" method="post" id="frm_supplier" name="frm_supplier" autocomplete="off">
                <input type="hidden" name="id" id="id" value="<?php echo (isset($supplier->id))?$supplier->id:''; ?>" />
        		<div class="form">
                
                    <div class="raw field-box form-group">
                        <label class="col-lg-3">Supplier Name:</label>
                        <div class="col-lg-4">
                            <input name="supplier_name" id="supplier_name" value="<?php echo isset($supplier->supplier_name)?$supplier->supplier_name:''; ?>" type="text"  class="form-control"/>
                        </div>
                    </div>
                    
                    <div class="raw field-box form-group">
                    	<label class="col-lg-3">Mobile Number</label>
                    	<div class="col-lg-4">
                    	  <input name="mobile_number" id="mobile_number" value="<?php echo isset($supplier->mobile_number)?$supplier->mobile_number:''; ?>" type="text"  class="form-control"/>
                    	</div>
                    </div>
                    
                    <div class="raw field-box form-group">
                        <label class="col-lg-3">Contact Number</label>
                        <div class="col-lg-4">
                          <input name="contact_number" id="contact_number" value="<?php echo isset($supplier->contact_number)?$supplier->contact_number:''; ?>" type="text"  class="form-control"/>
                        </div>
                    </div>
                    
                    <div class="raw field-box form-group">
                        <label class="col-lg-3">Fax Number</label>
                        <div class="col-lg-4">
                          <input name="fax" id="fax" value="<?php echo isset($supplier->fax)?$supplier->fax:''; ?>" type="text"  class="form-control"/>
                        </div>
                    </div>
                    
                    <div class="raw field-box form-group">
                        <label class="col-lg-3">Email Address</label>
                        <div class="col-lg-4">
                          <input name="email" id="email" value="<?php echo isset($supplier->email)?$supplier->email:''; ?>" type="text"  class="form-control"/>
                        </div>
                    </div>
                    
                    
                    <div class="raw field-box form-group">
                        <label class="col-lg-3">Address</label>
                        <div class="col-lg-4">
                          <textarea name="address" id="address" cols="" rows="" class="form-control"><?php echo isset($supplier->address)?$supplier->address:''; ?></textarea>
                        </div>
                    </div> 
						<div class="raw">
                        <label class="col-lg-3">Notes</label>
                        <div class="col-lg-4">
                        	<textarea name="notes" id="notes" cols="" rows="" class="form-control"><?php echo isset($supplier->notes)?$supplier->notes:''; ?></textarea>
                        </div>
                    </div>
                    <div class="raw field-box form-group" style="display:none;">
                        <label class="col-lg-3">Charges</label>
                        <div class="col-lg-4">
                          <input name="current_charges" id="current_charges" value="" type="text"  class="form-control"/>
                        </div>
                    </div>
                    <div class="raw field-box form-group"><div  id="selected_charges" class="col-lg-4"><?php
							if(isset($supplier_charge)){
								foreach($supplier_charge as $i=>$sc){
									?>
                             		<div id="charge_index<?php echo $i; ?>" class="invoice_no"><strong><?php echo $sc->charge_name; ?></strong><a href="javascript:void(0)" onclick="removeChargesFromDb('<?php echo $i; ?>','<?php echo $sc->charge_id; ?>')">X</a> <input type="hidden" name="charges_ids[]" id="charges_ids['<?php echo $i; ?>]" value="<?php echo $sc->id; ?>" class="soil<?php echo $i; ?>"></div>       
                                    <?php
								}
							}						
					 ?></div></div>
                            
                    <br clear="all"        />
                    <div class="raw" align="center">
                    <?php
					$btn_text = isset($supplier->id)?"Update":"Add";
					?>
                        <input name="sub_mit" id="sub_mit" type="submit" class="btn-glow primary next" value="<?php echo $btn_text;?>" />
                        <?php if(!isset($supplier->id)) { ?>
                        <input name="sub_reset" type="reset" class="btn-glow primary next reset_btn" value="Reset" />
                        <?php } ?>
                    </div>
                    <!--end of raw--> 
        		</div>
        	</form>
        </div>
    	<!-- END PAGE -->  
	</div>
	</div>
	</div>
	</div>
	</div>

<!-- End Section-->
<!--footer-->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(function() {
$( "#from" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from" ).datepicker( "option", "maxDate", selectedDate );
}
});
});
</script>
<?php $this->load->view('common/footer');?>