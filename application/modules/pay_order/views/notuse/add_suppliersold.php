

<?php $this->load->view('common/meta'); ?>
<!--body with bg-->


<div class="body">
    <header>
        <?php $this->load->view('common/header'); ?>

        <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
        <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>fileUpload/css/jquery.fileupload.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>fileUpload/css/jquery.fileupload-ui.css">
        <!-- CSS adjustments for browsers with JavaScript disabled -->
        <link href="<?php echo base_url(); ?>css/lib/bootstrap.datepicker.css" type="text/css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>jasny-bootstrap/css/jasny-bootstrap.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/validationEngine.jquery.css" type="text/css"/>

    </header>
    <?php $this->load->view('common/left-navigations'); ?>

    <div class="content">
        <?php
//	echo "<pre>";
//	print_r($company);
        ?>

        <!-- settings changer -->
        <div class="skins-nav">
            <a href="#" class="skin first_nav selected">
                <span class="icon"></span><span class="text">Default</span>
            </a>
            <a href="#" class="skin second_nav" data-file="<?php echo base_url(); ?>css/compiled/skins/dark.css">
                <span class="icon"></span><span class="text">Dark skin</span>
            </a>
        </div>

        <div id="pad-wrapper">
            <div class="row form-wrapper">
                <div class="col-md-12 col-xs-12">



                    <div id="main-content" class="main_content">
                        <div class="title title alert alert-info">
                            <span>Add Customer</span>
                        </div>

                        <div class="notion title title alert alert-info">* Please Understand  Clearly All The Data Before  Entering</div>
            <form action="<?php echo base_url(); ?>customers/add_new_customer" method="post" id="frm_customer" name="frm_customer" autocomplete="off">
                <input type="hidden" name="id" id="id" value="<?php echo (isset($customer->id))?$customer->id:''; ?>" />
        		<div class="form">
                
                    <div class="raw form-group">
                        <div class="form_title">Branch Name</div>
                        <div class="form_field">
                            <div class="dropmenu">
                                <div class="styled-select">
	                                <?php company_branch_dropbox('branch_id',isset($customer->branch_id)?$customer->branch_id:''); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="raw form-group">
                        <div class="form_title">Customer Name</div>
                        <div class="form_field">
                            <input name="customer_name" id="customer_name" value="<?php echo isset($customer->customer_name)?$customer->customer_name:''; ?>" type="text"  class="formtxtfield"/>
                        </div>
                    </div>
                    
                    <div class="raw form-group">
                    	<div class="form_title">Mobile Number</div>
                    	<div class="form_field">
                    	  <input name="mobile_number" id="mobile_number" value="<?php echo isset($customer->mobile_number)?$customer->mobile_number:''; ?>" type="text"  class="formtxtfield"/>
                    	</div>
                    </div>
                    
                    <div class="raw form-group">
                        <div class="form_title">Contact Number</div>
                        <div class="form_field">
                          <input name="contact_number" id="contact_number" value="<?php echo isset($customer->contact_number)?$customer->contact_number:''; ?>" type="text"  class="formtxtfield"/>
                        </div>
                    </div>
                    
                    <div class="raw form-group">
                        <div class="form_title">Fax Number</div>
                        <div class="form_field">
                          <input name="fax" id="fax" value="<?php echo isset($customer->fax)?$customer->fax:''; ?>" type="text"  class="formtxtfield"/>
                        </div>
                    </div>
                    
                    <div class="raw form-group">
                        <div class="form_title">Email Address</div>
                        <div class="form_field">
                          <input name="email" id="email" value="<?php echo isset($customer->email)?$customer->email:''; ?>" type="text"  class="formtxtfield"/>
                        </div>
                    </div>
                    
                    <div class="raw">
                        <div class="form_title">Account Type</div>
                        <div class="form_field">
                          <div class="dropmenu">
                            <div class="styled-select">
                              <?php get_statusdropdown(isset($customer->account_type)?$customer->account_type:'','account_type','account_type',isset($customer->id)?'disabled="disabled"':""); ?>
                            </div>
                          </div>
                        </div>
                    </div>
                    
                    <div class="raw form-group" id="div_contactPeriod" style="display:<?php echo (isset($customer->account_type) && $customer->account_type=='contract')?'block':'none';?>">
                        <div class="form_title">Contract Period</div>
                        <div class="form_field">
                            <input name="from" id="from" value="<?php echo isset($customer->from)?$customer->from:'';?>" type="text"  class="formtxtfield"/>
                            <input style="margin-left:9px;" name="to" id="to" value="<?php echo isset($customer->to)?$customer->to:'';?>" type="text"  class="formtxtfield"/>
                        </div>
                    </div>
                    
                    <div class="raw" id="div_charges" style="display:<?php echo (isset($fixed_customer)||isset($contract_customer))?'block':'none';?>;">
                        <div class="form_title">Charges</div>
                        <div class="form_field">
                          <div class="form_field">      
                            Sea Port <input <?php echo isset($customer->id)?'disabled="disabled"':"";?> type="checkbox" value="sea" id="sea_port"  name="sea_port" <?php echo (isset($customer_selected_ports->sea) && $customer_selected_ports->sea==1)?'checked="checked"':''; ?> /> &nbsp;&nbsp;
                            Air Port <input <?php echo isset($customer->id)?'disabled="disabled"':"";?> type="checkbox" value="air" id="air_port"  name="air_port"  <?php echo (isset($customer_selected_ports->air) && $customer_selected_ports->air==1)?'checked="checked"':''; ?> /> &nbsp;&nbsp;
                            Border <input <?php echo isset($customer->id)?'disabled="disabled"':"";?> type="checkbox" value="land" id="land_port"  name="land_port"  <?php echo (isset($customer_selected_ports->land) && $customer_selected_ports->land==1)?'checked="checked"':''; ?> />&nbsp;&nbsp;
                            <?php 
							if(!isset($customer->id)){ 
								?>
                            	<input type="button" onclick="getCharges();" value="Get Charges" />
                            	<?php
							}
							?>
                          </div>
                        </div>
                    </div>
                    
                    
                    <?php
					if(isset($fixed_customer)){
					?>
                    <div style="" id="div_res" class="raw">
                        <div class="form_title"></div>
                        <div class="form_field">
                            <div id="div_result" class="form_field"> 
								<?php
                                
                                    //echo '<pre>';
                                    //print_r($fixed_customer);
                                    $i=1;
                                    foreach($fixed_customer as $result){
                                        ?>
                                        <div style="height:38px;clear:left;" id="div_<?php echo $result['charge_id'];?>"><label style="width:200px;float:left"><?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label><input placeholder="Enter charge."  disabled="disabled" class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" value="<?php echo $result['charge_value']?>" />  <?php /*?><a class="del_txt" id="anc_<?php echo $result['charge_id'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php */?></div>
                                        <?php
                                        $i++;
                                    }
                                
                                ?>
                        	</div>
                        </div>
                    </div>
                    <?php
					}
					?>
                    
                    <?php
					
					if(isset($contract_customer)){
					?>						
                    <div style="" id="div_res" class="raw">
                        <div class="form_title"></div>
                        <div class="form_field">
                            <div id="div_result" class="form_field"> 
								<?php
                                
                                    //echo '<pre>';
                                    //print_r($contract_customer);
                                    
									$i=1;
									foreach($contract_customer as $result){
										?>


										<div style="height:38px;" id="div_<?php echo $result['charge_id'];?>"><label style="width:200px;float:left"><?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label><input placeholder="Enter quantity"  class="formtxtfield" disabled="disabled"  type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" value="<?php echo $result['charge_quaintity'];?>" />&nbsp;&nbsp; &nbsp;&nbsp;<input type="text"  class="formtxtfield" placeholder="Enter per expense charges." disabled="disabled" name="txtpercharge_<?php echo $result['charge_id'];?>" id="txtpercharge_<?php echo $result['charge_id'];?>" value="<?php echo $result['per_charge_amount'];?>" style="margin-left:10px;" />&nbsp;&nbsp;<?php /*?><a class="del_txt" id="anc_<?php echo $result['per_charge_amount'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php */?></div>
										<?php
										$i++;
									}
                                
                                ?>
                        	</div>
                        </div>
                    </div>
                    <?php
					}
					?>
                    
                    <div class="raw" id="div_res" style="display:none">
                        <div class="form_title"></div>
                        <div class="form_field">
                          <div class="form_field" id="div_result">      
                             
                          </div>
                        </div>
                    </div>
                    
                    <div class="raw form-group">
                        <div class="form_title">Address</div>
                        <div class="form_field">
                          <textarea name="address" id="address" cols="" rows="" class="formareafield"><?php echo isset($customer->address)?$customer->address:''; ?></textarea>
                        </div>
                    </div> 
                    
                    <div class="raw">
                        <div class="form_title">Status</div>
                        <div class="form_field">
                        	<div class="dropmenu">
                      	    	<div class="styled-select">
                           			<?php get_statusdropdown(isset($customer->status)?$customer->status:'','status','status'); ?>
                            	</div>
                          	</div>
                        </div>
                    </div>
                    
                    <div class="raw">
                        <div class="form_title">Notes</div>
                        <div class="form_field">
                        	<textarea name="notes" id="notes" cols="" rows="" class="formareafield"><?php echo isset($customer->notes)?$customer->notes:''; ?></textarea>
                        </div>
                    </div>
                    
                    <div class="raw" align="center">
                    <?php
					$btn_text = isset($customer->id)?"Update":"Add";
					?>
                        <input name="sub_mit" id="sub_mit" type="submit" class="submit_btn" value="<?php echo $btn_text;?>" />
                        <?php if(!isset($customer->id)) { ?>
                        <input name="sub_reset" type="reset" class="reset_btn" value="Reset" />
                        <?php } ?>
                    </div>
                    <!--end of raw--> 
        		</div>
        	</form>
        </div>
    	<!-- END PAGE -->  
	</div>
	</div>
	</div>
	</div>
	</div>

<!-- End Section-->
<!--footer-->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(function() {
$( "#from" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from" ).datepicker( "option", "maxDate", selectedDate );
}
});
});
</script>
<?php $this->load->view('common/footer');?>