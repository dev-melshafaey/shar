<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Pay_order extends CI_Controller {

    /*

     * Properties

     */



    private $_data = array();



//----------------------------------------------------------------------

    /*

     * Constructor

     */

    public function __construct() {

        parent::__construct();

        $this->lang->load('main', get_set_value('site_lang'));

        $this->load->model('pay_management_model', 'purchase_management');

        $this->_data['udata'] = userinfo_permission();

    }



//----------------------------------------------------------------------



    /*

     * Home Page

     */

    public function index() {

        // Load Home View

        $this->load->view('purchase-management');

    }



    function viewexpenceinvoices($expenseid) {



        $this->_data['sale_purchases'] = $this->purchase_management->getInvoiceByExpenseId($expenseid);

        //$this->load->view('purchase_invoice', $this->_data);

        $this->_data['inside'] = 'purchase_invoice';

        $this->load->view('common/main', $this->_data);

    }



    function view_invoice_items($inv_id, $pencevalue = '') {

        $get = $this->input->get();







        if ($inv_id != "") {



            //echo "<pre>";

            //print_r($get);





            $this->_data['purcase_items'] = $this->purchase_management->getInvoiceitems($inv_id);

            $this->_data['invid'] = $inv_id;

            $this->_data['total'] = base64_decode($get['val']);

            $this->_data['percentage'] = base64_decode($get['pval']);

            $this->_data['inside'] = 'purchase_invoiceee';

            $this->load->view('common/main', $this->_data);

        }

    }



    function getAddnew() {

        $this->load->view('add_newitem', $this->_data);

    }



    function save_new_item() {

        $post = $this->input->post();

        //	echo "<pre>";

        //print_r($post);

        $items['comid'] = $post['newcomid'];

        $items['branchid'] = $post['newbranchid'];

        $items['itemname'] = serialize($post['newitemname']);

        $items['categoryid'] = $post['newcategoryid'];

        $items['itemtype'] = $post['newitemtype'];

        $items['serialnumber'] = $post['newserialnumber'];

        $items['barcodenumber'] = $post['newbarcodenumber'];

        $items['barcode_image'] = $post['newbarcode_image'];

        echo $this->db->insert('bs_item', $items);

    }



    function postInvoices() {



     //  echo "<pre>";
      // print_r($_POST);
      //  exit;



        $invoiceData = $_POST['invoiceData'];

        $invoiceResponse = json_decode($invoiceData);

        unset($_POST['products']);



        if (post('totalnet') != null && post('customerid') != null) {





            if ($_POST['totalnet']) {

                //print_r($_POST['p_type']);

                //$invoice['ownerid'] = $_POST['ownerid'];

                $invoice['generated_by'] = $this->_data['udata']['userid'];

                $invoice['companyid'] = $_POST['companyid'];

                $invoice['branchid'] = $_POST['branchid'];

                //$invoice['customer_id'] = $_POST['customerid'];

                //$invoice['discountamount'] = $_POST['totalDiscount'];

                $invoice['purchase_total_amount'] = $_POST['totalnet'];

                $invoice['sales_amount'] = $_POST['totalnet'];

                $invoice['purchase_totalDiscount'] = $_POST['totalDiscount'];
                $invoice['oldinvoice_num'] = $_POST['oldinvoice_num'];
                $invoice['oldinvoice_date'] = $_POST['oldinvoice_date'];





                $invoice['purchase_totalDiscount_type'] = $_POST['type_ds'];





                $invoice['supplier_id'] = $_POST['customerid'];

                $invoice['customer_id'] = $_POST['customerid'];



                $invoice['print_option_desc'] = $_POST['print_option_desc'];

                $invoice['print_option_note'] = $_POST['print_option_note'];

                $invoice['print_option_netprice'] = $_POST['print_option_netprice'];

                $invoice['print_option_dicount'] = $_POST['print_option_dicount'];

                $invoice['print_option_total'] = $_POST['print_option_total'];

                $invoice['print_option_pt'] = $_POST['print_option_pt'];

                if (post('receiverd_amount') == "0") {

                    $invoice['purchase_status'] = '1';

                } else {

                    $invoice['purchase_status'] = '0';

                }



                //1

                //print_r($invoice);

                $invoice_id = $this->purchase_management->inserData('an_purchase_order', $invoice);

            }





            /*

              $invoice_items = array();

              if ($invoiceResponse) {

              foreach ($invoiceResponse as $invresponsive) {

              //print_r($invresponsive);

              $invoice_items['invoice_id'] = $invoice_id;

              $invoice_items['invoice_item_id'] = $invresponsive->productId;

              $invoice_items['invoice_item_discount_type'] = $invresponsive->discountType;

              $invoice_items['invoice_item_discount'] = $invresponsive->discount;

              $invoice_items['invoice_item_notes'] = $invresponsive->note;

              $invoice_items['invoice_item_quantity'] = $invresponsive->quantity;

              $invoice_items['invoice_item_price'] = $invresponsive->salePrice;

              $invoice_items['invoice_item_store_id'] = $invresponsive->storeId;

              $invoice_items['invoice_item_price_purchase'] = $invresponsive->productPrice;





              //2

              $invitem_id_item2 [] = $this->sales_management->inserData('bs_invoice_items', $invoice_items);



              //print_r($invoice_items);

              }

              }

             */

            if (post('items')['productId']) {

                foreach (post('items')['productId'] as $key => $invresponsive) {

                    //print_r($invresponsive);

                    $invoice_items['purchase_id'] = $invoice_id;



                    /*

                      $invoice_items['quotation_item_id'] = $invresponsive->productId;

                      $invoice_items['quotation_item_discount_type'] = $invresponsive->discountType;

                      $invoice_items['quotation_item_discount'] = $invresponsive->discount;

                      $invoice_items['quotation_item_notes'] = $invresponsive->note;

                      $invoice_items['quotation_item_quantity'] = $invresponsive->quantity;

                      $invoice_items['quotation_item_price'] = $invresponsive->salePrice;

                      $invoice_items['quotation_item_store_id'] = $invresponsive->storeId;

                      $invoice_items['quotation_item_price_purchase'] = $invresponsive->productPrice;

                     */



                    $invoice_items['purchase_item_id'] = post('items')['productId'][$key];

                    $invoice_items['purchase_item_discount_type'] = post('items')['productDiscountType'][$key];

                    $invoice_items['purchase_item_discount'] = post('items')['productDiscount'][$key];

                    $invoice_items['purchase_item_notes'] = post('items')['productDiscription'][$key];

                    $invoice_items['purchase_item_quantity'] = post('items')['quantity'][$key];

                    $invoice_items['purchase_item_price'] = post('items')['productTotal'][$key];



                    $invoice_items['purchase_item_store_id'] = post('items')['store_id'][$key];

                    $invoice_items['purchase_item_price_purchase'] = post('items')['productPrice'][$key];


                    $invoice_items['purchase_item_aed_price'] = post('items')['productAedTotal'][$key];

                    $invoice_items['invoice_itype'] = post('items')['invoice_itype'][$key];


                    $invoice_items['supplier_id'] = $_POST['customerid'];

                    $invoice_items['expireddate'] = $_POST['expireddate'];

                    $invitem_id_item = $this->purchase_management->inserData('bs_purchase_items_order', $invoice_items);

                    if (post('sale_direct_store') == '1') {


                        $store_items['store_id'] = post('items')['store_id'][$key];

                        $store_items['item_id'] = post('items')['productId'][$key];

                        $store_items['quantity'] = post('items')['quantity'][$key];

                        $store_items['purchase_id'] = $invoice_id;

                        $store_items['supplier_id'] = $_POST['customerid'];


                        $up1['confirm_status'] = 1;

                        $this->purchase_management->updateData('an_purchase_order', $up1, $invoice_id, 'purchase_id');



                        $up1['confirm_status'] = 1;

                        $this->purchase_management->updateData('bs_purchase_items_order', $up1, $invoice_id, 'purchase_id');

                    }



                }

            }




            unset($_POST['products']);

            //print_r($_POST);

            $ownerId = $_POST['ownerid'];

            $companyId = $_POST['companyid'];

            $branchId = $_POST['branchid'];


            $redata['success']=1;

            $redata['redir']=base_url() . 'pay_order/printed/'.$invoice_id;

            $redata['redir2']=base_url() . 'pay_order/purchase_invoices?e=10';

        } else {



            $redata['success']=2;

            $redata['message']='<div class="notif red alert "> <span>خطأ يجب ادخال بيانات لتتم العملية</span></div>';

        }  

        echo json_encode($redata);

    }



    function getPurcahseItems($inv_id) {

        $this->_data['purchase_invocie'] = $this->purchase_management->getPurchaseInvoiceItems($inv_id);

        $this->_data['inside'] = 'pending_purchase_items';

        $this->load->view('common/main', $this->_data);

        // $this->load->view('pending_purchase_items', $this->_data);

    }



    function add_confirmation_items() {

        $data = $this->input->post();

        echo "<pre>";

        print_r($data);

    }



    function test() {



        $purchase_invoice['invoicenumber'] = '12123';

        $purchase_invoice['ownerid'] = '';

        $purchase_invoice['company_id'] = '5';

        $purchase_invoice['branch_id'] = '';

        $purchase_invoice['invoice_bill_date'] = date('Y-m-d');

        $purchase_invoice['creatorid'] = $this->_data['udata']['userid'];

        $purchase_invoice['supplier_id'] = '8';

        $purchase_invoice['store_id'] = 4;

        $purchase_invoice['totalamount'] = 222;

        $purchase_invoice['recievedamount'] = 222;

        if ($purchase_invoice['totalamount'] == $purchase_invoice['receiverd_amount'])

            $purchase_invoice['invoicstatus'] = 'C';

        elseif ($purchase_invoice['totalamount'] >= $purchase_invoice['receiverd_amount'])

            $purchase_invoice['invoicstatus'] = 'I';

        else

            $purchase_invoice['invoicstatus'] = 'E';



        $invoice_id = $this->purchase_management->inserData('bs_purchase_invoice', $purchase_invoice);





        $invoicePurchase['quantity'] = '2';

        //$invoicePurchase['supplier_id'] = $retrn->storeId;

        $invoicePurchase['supplier_id'] = 7;

        $invoicePurchase['unit_price'] = 22;

        $invoicePurchase['product_id'] = 1;

        $invoicePurchase['note'] = '';

        $invoicePurchase['invoice_id'] = $invoice_id;

        $invoicePurchase['discription'] = 'asdasd';



        $this->purchase_management->inserData('purchase_invoice_items', $invoicePurchase);











        $payment['payment_amount'] = '222';

        $payment['supplier_id'] = 7;

        $payment['invoice_id'] = $invoice_id;

        $payment_id = $this->purchase_management->inserData('purchase_payment', $payment);







        //$store_purchase_id = $this->purchase_management->inserData('bs_store_items', $storeItems);

    }



//----------------------------------------------------------------------



    /*

     * Add New Purchase

     */

    public function add_purchase($id = null) {

        // Load Home View

        $this->_data['suppliers'] = $this->purchase_management->getSupplier_list();

        $this->_data['categories'] = $this->purchase_management->getCategories();

        //echo "<pre>";

        //print_r($this->_data['categories']);

        //$this->load->view('add-invoice-purchase', $this->_data);

        //$this->_data['pdata'] = $this->purchase_management->getpurchase($id);

        $this->_data['inside'] = 'add-invoice-purchase';

        $this->load->view('common/main', $this->_data);

        //  $this->load->view('add-purchase',	$this->_data);

    }



    function add_new_supplier() {

        $data = $this->input->post();





        //$charges_ids = 	$data['charges_ids'];



        if (!empty($data)) {



            unset($data['id']);

            $data['suppliername'] = $data['supplier_name'];

            $data['phone_number'] = $data['mobile_number'];

            $data['office_number'] = $data['contact_number'];

            $data['fax_number'] = $data['fax'];

            unset($data['supplier_name']);

            unset($data['mobile_number']);

            unset($data['contact_number']);

            unset($data['email']);

            unset($data['fax']);

            unset($data['current_charges']);

            unset($data['sub_mit']);

            $this->db->insert('bs_suppliers', $data);

            $supplier_id = $this->db->insert_id();

            /* if(isset($charges_ids)){

              foreach($charges_ids as $charge_id){

              $supplier_charges['charge_id'] = $charge_id;

              $supplier_charges['supplier_id'] = $supplier_id;

              $this->db->insert('supplier_charges',$supplier_charges);

              }



              } */

            //	redirect(base_url().'purchase_management/options');

            //do_redirect('view_supplier?e=10');

            redirect(base_url() . 'purchase_management/suppliers?e=10');

        } else {



            $this->db->where('id', $data['id']);

            $this->db->update('an_suppliers', $data);



            /* if(isset($charges_ids)){

              foreach($charges_ids as $charge_id){

              $supplier_charges['charge_id'] = $charge_id;

              $supplier_charges['supplier_id'] = $data['id'];

              $returnData = $this->purchase->checkSupplierCharge($data['id'],$charge_id);

              if(empty($returnData))

              $this->db->insert('supplier_charges',$supplier_charges);



              }



              } */

            //do_redirect('view_supplier?e=13');

            redirect(base_url() . 'purchase_management/suppliers?e=13');

        }

    }



//----------------------------------------------------------------------



    /*

     * Sale Invoice

     */

    public function purchase_invoices($id = '', $type = null) {

        // Load Home View

        $this->_data['id'] = $id;

        $this->_data['type'] = $type;

        $this->_data['sale_purchases'] = $this->purchase_management->getSaleInvocies_new($id);

        //$this->load->view('purchase_invoice', $this->_data);

        $this->_data['inside'] = 'purchase_invoice';

        $this->load->view('common/main', $this->_data);

    }



    function suppliers() {

        $this->_data['suppliers'] = $this->purchase_management->getSupplier_list();

        //$this->load->view('bs_suppliers', $this->_data);

        $this->_data['inside'] = 'bs_suppliers';

        $this->load->view('common/main', $this->_data);

    }



    function supplier_invoices($id) {



        $this->_data['supplier_invoices'] = $this->purchase_management->getSupplierPayments($id);

        $this->_data['id'] = $id;

        //$this->load->view('bs_supplier_invoices', $this->_data);

        $this->_data['inside'] = 'bs_supplier_invoices';

        $this->load->view('common/main', $this->_data);

    }



    function return_purchase($id = null) {



        //$this->_data['supplier_invoices'] = $this->purchase_management->getSupplierPayments($id);

        $this->_data['id'] = $id;

        //$this->load->view('bs_supplier_invoices', $this->_data);

        $this->_data['inside'] = 'return-purchase-invoices';

        $this->load->view('common/main', $this->_data);

    }



    function add_supplier($id) {



        $this->load->view('add_suppliers', $this->_data);

    }



//----------------------------------------------------------------------



    /*

     * Sale Invoice

     */

    public function return_purchase_invoices() {

        // Load Home View

        $this->load->view('return-purchase-invoices', $this->_data);

    }



    function get_store_quantity() {

        //$CI = & get_instance();

        $id = $_POST['val'];

        $result = $this->db->query("SELECT 

                   SUM(bi.`quantity`) AS totalquantity,  

                   bi.*,

                    `bs_category`.`catname`,

                   `bs_suppliers`.`suppliername`,

                   bi.`created` AS purchase_date,

                   `i`.`itemid`,

                   i.product_picture,

                   `i`.`itemname`,

                   `i`.`itemtype`,

                   IF(

                     i.`itemtype` = 'P',

                     `i`.`serialnumber`,

                     '-----'

                   ) AS `serialnumber`,

                   IF(

                     i.`itemtype` = 'P',

                     CONCAT(`i`.`purchase_price`, ' RO.'),

                     '-----'

                   ) AS `purchase_price`,

                   IF(

                     i.`itemtype` = 'P',

                     CONCAT(`i`.`sale_price`, ' RO.'),

                     '-----'

                   ) AS `sale_price`,

                   IF(

                     i.`itemtype` = 'P',

                     `i`.`min_sale_price`,

                     '-----'

                   ) AS `min_sale_price`,

                   IF(

                     i.`itemtype` = 'P',

                     `i`.`point_of_re_order`,

                     '-----'

                   ) AS `point_of_re_order` 



                 FROM

                   `bs_item` AS i 

                   INNER JOIN `bs_store_items` AS bi 

                     ON bi.`item_id` = i.`itemid` 

                   INNER JOIN `bs_category` 

                     ON `i`.`categoryid` = `bs_category`.`catid` 

                   INNER JOIN `bs_suppliers` 

                     ON `i`.`suppliderid` = `bs_suppliers`.`supplierid` 

                 WHERE bi.`store_id` = '" . $id . "' 

                 GROUP BY i.`itemid`");







        //return $result->result()->totalquantity; 

        $fquantity = 0;

        if ($result->result()) {



            foreach ($result->result() as $quantity) {



                $fquantity+=$quantity->totalquantity;

            }

        }

        echo $fquantity;

    }



//----------------------------------------------------------------------



    function get_last_balance() {

        $this->load->model('customers/customers_model', 'customers');

        //$post = $this->input->post();

        if ($_POST['customerid']) {

            $post = array('customer_id' => $_POST['customerid']);

        } else {

            $post = array('customer_id' => 78);

        }



        $customerData = $this->customers->getCustomerPaymetns($post['customer_id']);

        // print_r($customerData);

        if ($customerData->totalpayment > $customerData->totalcharges) {

            $remaining = $customerData->totalpayment - $customerData->totalcharges;

        }

        if ($remaining > 0) {

            $last_payment = $this->customers->getLastpaymentIdByCustomer($post['customer_id'], 'opening');

            //print_r($last_payment);

            //exit;

            if (!empty($last_payment) && $last_payment->payment_amount > 0) {

                //echo "if";

                if ($last_payment->paidamount > 0)

                    $last_payment->payment_amount;

                if ($last_payment->payment_amount > $last_payment->paidamount) {

                    $last_payment->payment_amount = $last_payment->payment_amount - $last_payment->paidamount;

                } else {

                    $last_payment = $this->customers->getLastpaymentIdByCustomer($post['customer_id'], 'advance');

                    if ($last_payment->payment_amount > 0) {



                        if ($last_payment->paidamount > 0)

                            $last_payment->payment_amount;

                        elseif ($last_payment->payment_amount > $last_payment->paidamount) {

                            $last_payment->payment_amount = $last_payment->payment_amount - $last_payment->paidamount;

                        }

                    }

                }

            } else {

                // echo "else";

                $last_payment = $this->customers->getLastpaymentIdByCustomer($post['customer_id'], 'onaccount');

                if ($last_payment->payment_amount) {

                    if ($last_payment->paidamount > 0) {

                        $last_payment->payment_amount = $last_payment->payment_amount - $last_payment->paidamount;

                    }

                }

            }

        }

        /*

          echo "<pre>";

          print_r($last_payment);

          exit; */

//        //echo "<pre>";

//        $openbalance = $this->sales_management->openbalance($post['customer_id']);

//        //exit;

//        $customerData = $this->customers->getCustomerPaymetns($post['customer_id']);

//        //print_r($customerData);

//        if ($customerData->totalpayment > $customerData->totalcharges) {

//            $remaining = $customerData->totalpayment - $customerData->totalcharges;

//        }

//        if ($remaining > 0) {

//            $last_payment = $this->customers->getLastpaymentIdByCustomer($post['customer_id'], 'onaccount');

//            if (!empty($last_payment) && $last_payment->payment_amount>0) {

//                //echo "if";

//                if ($last_payment->paidamount != "")

//                    $last_payment->payment_amount;

//                elseif ($last_payment->payment_amount > $last_payment->paidamount) {

//                    $last_payment->payment_amount = $last_payment->payment_amount - $last_payment->paidamount;

//                } else {

//                    $last_payment = $this->customers->getLastpaymentIdByCustomer($post['customer_id'], 'onaccount');

//                    if ($last_payment->paidamount != "")

//                        $last_payment->payment_amount;

//                    elseif ($last_payment->payment_amount > $last_payment->paidamount) {

//                        $last_payment->payment_amount = $last_payment->payment_amount - $last_payment->paidamount;

//                    }

//                }

//            } else {

//                //echo "else";

//                $last_payment = $this->customers->getLastpaymentIdByCustomer($post['customer_id'], 'onaccount');

//                if ($last_payment->paidamount != "")

//                    $last_payment->payment_amount;

//                elseif ($last_payment->payment_amount > $last_payment->paidamount) {

//                    $last_payment->payment_amount = $last_payment->payment_amount - $last_payment->paidamount;

//                }

//            }

//        }

//        

        $openbalance = $this->sales_management->openbalance($post['customer_id']);

        if ($openbalance->remain) {

            $rem = $openbalance->remain;

        } else {

            $rem = $openbalance->totalpayment;

        }

        $ret = array('id' => $last_payment->id, 'payment_amount' => $rem);

        echo json_encode($ret);

        //print_r($last_payment);

    }



    function get_balance($vid = null) {



        if ($vid == null) {

            $id = $_POST['val'];

        } else {

            $id = $vid;

        }



        /*

          $openbalance = $this->sales_management->openbalance($id);

          $invoice = $this->sales_management->get_invoice_total($id);

         */



        $get_calculation = $this->purchase_management->get_calculation($id);





        //var_dump($invoice);

        //die();





        /*

          //$Debit=$this->sales_management->Debit($id);

          //echo $currentbalance;

          //$balance1=$invoice->invoice_total_amount+$openbalance->totalcharges;

          $balance4 = $openbalance->remain + $openbalance->totalcharges;



          $balance1 = $balance4 + $openbalance->remain;

          //$balance1 = $openbalance->totalpayment;

          //$balance2=$openbalance;

          if ($openbalance->remain) {

          $balance2 = $openbalance->remain;

          } else {

          $balance2 = $openbalance->totalpayment;

          }



          //$balance3=$openbalance->totalcharges-$invoice->invoice_total_amount;

          //$balance3 = $openbalance->totalpayment - $openbalance->totalcharges;





          if ($openbalance->remain) {



          $balance3 = ($balance4 - $get_calculation->totalpayment) - $openbalance->remain;

          //$balance3="<br/>".$balance4."<br/>".$get_calculation->totalpayment."<br/>".$openbalance->remain;

          } else {



          $balance3 = $get_calculation->woamount - $get_calculation->totalpayment;

          //$balance3 = "w-t";

          }

         */





        $balance1 = $get_calculation->totalinvpayment + $get_calculation->totalpayment2 + $get_calculation->remainng;

        //$balance2 = $get_calculation->balance;

        $balance2 = $get_calculation->totalinvpayment + $get_calculation->totalpayment2;

        //$balance3 = $get_calculation->totalsales;

        $balance4 = $get_calculation->remainng;













        echo '<div class="g4 green tag">' . lang('balance1') . $balance1 . '</div>';



        echo '<div class="g4 green tag">' . lang('balance2') . $balance2 . '</div>';



        //echo '<div class="col-md-3 green">' . lang('balance4') . $balance3 . '</div>';



        echo '<div class="g4 red tag">' . lang('balance3') . $balance4 . ' </div>';

    }



//----------------------------------------------------------------------







    function viewp($id = null) {



        //$this->_data['invoice'] = $this->sales_management->getSaleInvocies($id);

        //$this->_data['p_invoices'] = $this->sales_management->p_invoices($id);

        $this->_data['invoice'] = $this->purchase_management->invoice_data($id);

        $this->_data['inside'] = 'viewp';

        $this->_data['id'] = $id;

        $this->load->view('common/main', $this->_data);

    }

    function delete_purchase_order(){
        $purchase_item_id = post('purchase_item_id');

        //$this->purchase_managment->delete_pay_order_item($purchase_item_id);
        echo true;
    }
    function update_p_order(){
        $post = post();
      //  echo "<pre>";
      //  print_r($post);

        if($post['purchase_item_quantity']){
            $pitemQuantity = $post['purchase_item_quantity'];
            $purchase_item_price = $post['purchase_item_price'];
            $purchase_item_aedprice = $post['purchase_item_aedprice'];

             $totalpuchaseprice = 0;
            $totalpuchaseaedprice = 0;
            if($pitemQuantity){
                foreach($pitemQuantity as $in=>$pq){
                    $pItem['purchase_item_quantity'] = $pq;
                    $this->db->where('inovice_product_id', $in);
                    $this->db->update('bs_purchase_items_order',$pItem);

                    $totalpuchaseprice = $totalpuchaseprice+$purchase_item_price[$in]*$pq;
                    $totalpuchaseaedprice = $totalpuchaseaedprice+$purchase_item_aedprice[$in]*$pq;

                }
                 // $post['order_id'];
                $pOrders['purchase_total_amount'] = $totalpuchaseaedprice;
                $this->db->where('purchase_id', $post['order_id']);
                $this->db->update('an_purchase_order',$pOrders);
            }
            if($post['generate_invoice']){
                     $ordid = $post['order_id'];
                $invData = $this->purchase_management->getprInvoiceData($ordid);
                $invItemData =   $this->purchase_management->getprInvoiceDataItem($ordid);
                    //$invData = $this->purchase_managment->getprInvoiceData($post['order_id']);
                    //$invItemData = $this->purchase_managment->getprInvoiceDataItem($post['order_id']);
                   // exit;
                   // echo "<pre>";
                   // print_r($invData);
                    unset($invData->purchase_id);
                    $this->db->insert('an_purchase', $invData);
                  $invId =   $this->db->insert_id();
                    if($invItemData){
                        foreach($invItemData as $invItem){
                            unset($invItem->inovice_product_id);
                            $invItem->purchase_id = $invId;

                            $this->db->insert('bs_purchase_items', $invItem);
                        }
                    }

            }
        }

        //if($post['generate_invoice'] =)
        echo true;
    }
    function edit_invoice($id = null) {



        //$this->_data['invoice'] = $this->sales_management->getSaleInvocies($id);

        //$this->_data['p_invoices'] = $this->sales_management->p_invoices($id);

        $this->_data['invoice'] = $this->purchase_management->invoice_data($id);

        $this->_data['inside'] = 'viewpedit';

         $this->_data['id'] = $id;

        $this->load->view('common/main', $this->_data);

    }


    function printed($id = null) {



        $this->_data['purchase'] = $this->purchase_management->p_invoice($id);

        $this->_data['p_purchases'] = $this->purchase_management->p_invoices($id);

		//echo "<pre>";
		//print_r($this->_data['p_purchases']);
		if(isset($this->_data['p_purchases']) && !empty($this->_data['p_purchases'])){
					$this->_data['store_name'] = $this->_data['p_purchases'][0]->storename;
					
		}
		else{
			$this->_data['store_name'] = '';
		}
        //$this->_data['inside'] = 'printed';

        $this->load->view('printed', $this->_data);

    }



    function add_note() {



        if (post('notes')) {

            //var_dump(post('notes'));

            foreach (post('notes')['title'] as $key => $notes) {



                $sdata['purchase_id'] = post('invoice_id');

                $sdata['in_title'] = post('notes')['title'][$key];

                $sdata['in_notes'] = post('notes')['note'][$key];



                //var_dump($sdata);

                $this->purchase_management->inserData('purchase_notes', $sdata);

            }

            //die();

            redirect(base_url() . 'purchase_management/viewp/' . post('invoice_id') . "/?e=11");

        } else {

            redirect(base_url() . 'purchase_management/viewp/' . post('invoice_id') . "/?e=14");

        }

    }



    function uploadify() {



        $targetFolder = $this->config->item('bath'); // Relative to the root



        $verifyToken = md5('unique_salt' . $_POST['timestamp']);



        if (!empty($_FILES) && $_POST['token'] == $verifyToken) {

            $tempFile = $_FILES['Filedata']['tmp_name'];

            $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;

            $targetFile = rtrim($targetPath, '/') . '/' . $_FILES['Filedata']['name'];



            // Validate the file type

            $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // File extensions

            $fileParts = pathinfo($_FILES['Filedata']['name']);



            if (in_array($fileParts['extension'], $fileTypes)) {



                //$file=move_uploaded_file($tempFile,$targetFile);

                $path = APPPATH . '../uploads/invoicefiles/';

                $file_name = upload_file('Filedata', $path, true, 50, 50);



                $sdata['purchase_id'] = post('invoi_id');

                $sdata['ido_file'] = $file_name;



                //var_dump($sdata);

                $this->sales_management->inserData('purchase_document', $sdata);





                //print_r($file_name);

                //echo $tempFile."<br/>";

                //echo $targetFile;



                echo '1';

            } else {

                echo '<script>Invalid file type</script>';

            }

        }

    }



    function getfiles($id) {

        $invoice = $this->purchase_management->get_files($id);

        if ($invoice) {

            foreach ($invoice as $d_invoice) {

                echo '

                        <tr>

                            <td>' . $d_invoice->ido_id . '</td>

                            <td><a href="' . base_url() . "/uploads/invoicefiles/" . $d_invoice->ido_file . '">' . $d_invoice->ido_file . '</a></td>

                            <td>' . $d_invoice->ido_date . '</td>

                       </tr>';

            }

        }

    }



    function add_expences() {



        $expense = array();

        if (post('expense')['expense_title']) {



            foreach (post('expense')['expense_title'] as $key => $sale) {



                $expense['expense_title'] = post('expense')['expense_title'][$key];

                $expense['expense_charges_id'] = post('expense')['expense_charges_id'][$key];

                $expense['type'] = 'direct';

                $expense['expense_type'] = post('expense')['expense_type'][$key];

                $expense['expense_period'] = post('expense')['expense_period'][$key];

                $expense['value'] = post('expense')['value'][$key];

                $expense['account_id'] = post('expense')['account_id'][$key];

                $expense['bank_id'] = post('expense')['bank_id_ex'][$key];

                $expense['Notes'] = post('expense')['Notes'][$key];

                $expense['invoice_id'] = post('invoice_id');

                //8

                //print_r($expense);

                $expense_id = $this->purchase_management->inserData('an_expenses', $expense);







                $inv['expense_id'] = $expense_id;

                $inv['invoice_id'] = post('invoice_id');

                $this->purchase_management->inserData('expense_purchase', $inv);





                $expenseid_bank['customer_id'] = $_POST['customerid'];

                $expenseid_bank['payment_id'] = "";

                $expenseid_bank['amount_type'] = 'expense';

                $expenseid_bank['refrence_type'] = 'expense';

                $expenseid_bank['transaction_type'] = 'credit';



                //$expense_Accunts['branch_id'] = post('branch_id');



                $expenseid_bank['cheque_number'] = post('expense')['p_numper'][$key];

                $expenseid_bank['deposite_recipt'] = post('expense')['p_numper'][$key];



                $expenseid_bank['notes'] = post('expense')['Notes'][$key];

                $expenseid_bank['purpose_of_payment'] = post('expense')['Notes'][$key];





                $expenseid_bank['cheque_date'] = date("Y-m-d");

                //$expenseid_bank['clearance_date'] = date("Y-m-d");



                $expenseid_bank['clearance_date'] = date('Y-m-d', strtotime(post('expense')['clearance_date'][$key]));

                //$expense_Accunts['refrence_type'] = 'expense';



                $expenseid_bank['value'] = post('expense')['value'][$key];



                $expenseid_bank['bank_id'] = post('expense')['bank_id_ex'][$key];

                $expenseid_bank['account_id'] = post('expense')['account_id'][$key];





                //6

                $this->purchase_management->inserData('an_company_transaction', $expenseid_bank);

                //echo 's';

            }

            //echo post('invoice_id');

            //die();

            redirect(base_url() . 'purchase_management/viewp/' . post('invoice_id') . "/?e=11");

            exit();

        } else {

            //echo 's3';

            redirect(base_url() . 'purchase_management/viewp/' . post('invoice_id') . "/?e=11");

            exit();

        }

        //echo 's4';

    }



    function do_return_in($id) {



        if (post('return_tobalance') == '1') {



            $invoicedata = array('invoice_status' => '0');

            $this->purchase_management->updateData('an_invoice', $invoicedata, $id, 'invoice_id');



            $inpays = explode(',', post('invoice_payment'));

            foreach ($inpays as $inpay) {

                $invoicedata2 = array('status' => '0');

                //echo "inpay:".$inpay."<br/>";

                $this->purchase_management->updateData('invoice_payments', $invoicedata2, $inpay, 'inv_payment_id');

            }

            $pays = explode(',', post('payment'));

            foreach ($pays as $pay) {

                $invoicedata3 = array('status' => '0');

                //echo "pay:".$pay."<br/>";

                $this->purchase_management->updateData('payments', $invoicedata3, $pay, 'id');

            }

            $mpayment['payment_type'] = 'receipt';

            $mpayment['refernce_type'] = 'onaccount';

            $mpayment['payment_amount'] = post('totalpay');

            $mpayment['refrence_id'] = post('customerid');

            $mpayment['payment_date'] = date('Y-m-d');

            $mpayment['status'] = '1';



            //var_dump($mpayment);

            $this->purchase_management->inserData('payments', $mpayment);

        }

        redirect(base_url() . 'purchase_management/return_sales_invoices?e=10');

    }



}



?>