<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Pay_management_model extends CI_Model {

    /*

     * Properties

     */



//----------------------------------------------------------------------



    /*

     * Constructor

     */



    function __construct() {

        parent::__construct();



        //Load Table Names from Config

    }



    function getpurchase($id) {

        $this->db->select('*');

        $this->db->where('invoiceid', $id);

        //$this->db->order_by("supplierid", "DESC");

        $query = $this->db->get('an_purchase');

        return $query->row();

    }


    function delete_pay_order_item($id){
        $this->db->where('inovice_product_id', $id);
        $this->db->delete('bs_purchase_items_order');
    }

    ///get supplier list

    public function getSupplier_list() {

        $ownerid = ownerid();

        $this->db->select('*');

        $this->db->from('bs_suppliers');

        $this->db->order_by("supplierid", "DESC");

        $query = $this->db->get();

        return $query->result();

    }



    function getSupplierPayments($supplier_id) {

        $sql = "SELECT piv.`quantity` AS purchaseq, piv.`store_id` AS store, i.*,piv.*, piv.`id` AS piv_id, s.`storename` FROM `purchase_invoice_items` AS piv

					INNER JOIN `bs_item`  AS i

					ON i.`itemid` = piv.`product_id`

					INNER JOIN `bs_store` AS s ON s.`storeid`=piv.`store_id`

					WHERE piv.`invoice_id`=" . $supplier_id . "";

        $q = $this->db->query($sql);



        if ($q->num_rows() > 0) {

            $response = $q->result();

        } else {

            return false;

        }



        return $response;

    }



    function inserData($table, $data) {

        $this->db->insert($table, $data);

        return $this->db->insert_id();

    }



    function getCategories() {

        $sql = "SELECT * FROM `bs_category`";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }



    function checkItem($store_id, $item_id) {

        $sql = "SELECT * FROM bs_store_items AS bsi  WHERE 

bsi.`store_id` = '" . $store_id . "' AND bsi.`item_id` = '" . $item_id . "'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->row()->store_item_id;

        } else {

            return false;

        }

    }



    function updateItemQuantity($itempkid, $quantiiy) {

        $sql = "UPDATE `bs_store_items` SET `quantity` = quantity + " . $quantiiy . " WHERE `store_item_id` = '" . $itempkid . "'; ";

        return $q = $this->db->query($sql);

    }



    function getPendingPurchase($userid) {

        /* echo  $sql = 'SELECT bpi.*,sp.* FROM `bs_store` AS s

          INNER JOIN `bs_purchase_invoice` AS bpi

          ON bpi.`store_id` = s.`storeid`

          INNER JOIN `an_suppliers` AS sp ON sp.`id` = bpi.`ownerid`

          WHERE s.`ownerid` = ' . $userid . ' AND bpi.`confirm_status` = "0"

          GROUP BY s.`storeid`'; */



        $sql = "SELECT 

                bpi.*,

                u.userid,u.fullname,

                s.storeid,

                p.*

              FROM

                `bs_purchase_items` AS bpi 







              INNER JOIN `bs_users` AS u ON u.`userid` = bpi.`supplier_id` 



              INNER JOIN `bs_store` AS s ON s.`storeid` = bpi.`purchase_item_store_id`



              INNER JOIN `an_purchase` AS p ON p.`purchase_id` = bpi.`purchase_id` 



              WHERE p.`confirm_status` = '0' 



              GROUP BY bpi.inovice_product_id



              ORDER BY bpi.`purchase_id` DESC ";

        $query = $this->db->query($sql);



        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 



        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }



    function getSendingPurchase($userid) {

        $sql = 'SELECT 

				  bpi.*,

				  sp.* 

				FROM

				  `bs_store` AS s 

				  INNER JOIN `bs_purchase_invoice` AS bpi 

					ON bpi.`store_id` = s.`storeid` 

				  INNER JOIN `bs_users` AS sp 

					ON sp.`userid` = bpi.`ownerid` 

				WHERE s.`ownerid` = ' . $userid . ' 

				  AND bpi.`confirm_status` = "0" 

				GROUP BY s.`storeid` ';

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }



    function getPurchaseInvoiceItems($inv_id) {

        /* $sql= 'SELECT i.*,piv.*, piv.`id` AS piv_id FROM `purchase_invoice_items` AS piv

          INNER JOIN `bs_item`  AS i

          ON i.`itemid` = piv.`product_id`

          WHERE piv.`invoice_id`='.$inv_id.''; */

        $sql = "SELECT piv.`store_id` AS store, i.*,piv.*, piv.`purchase_id` AS piv_id, s.`storename` FROM `bs_purchase_items` AS piv

                INNER JOIN `bs_item`  AS i

                ON i.`itemid` = piv.`purchase_item_id`

                INNER JOIN `bs_store` AS s ON s.`storeid`=piv.`purchase_item_store_id`

                WHERE piv.`purchase_id`=" . $inv_id . "";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }



    function getPurchaseInvoices($Id, $type) {

        $subsql = '';



        if ($Id && $type == 'item') {



            $subsql = " WHERE bi.`itemid` = '" . $Id . "'";

        } elseif ($Id && $type == null) {



            $subsql = " WHERE s.`supplierid` = '" . $Id . "'";

        } elseif ($Id == null && $type != 'item') {



            //$subsql = " WHERE bi.`return_purchase` = '" . $type . "'";

        }



        $sql = "SELECT 

									  bi.*,s.*,

									  SUM(pp.`payment_amount`)  AS paid

									  

									FROM

									  `an_purchase` bi 

									  LEFT JOIN `purchase_payments` AS pp 

										ON pp.`purchase_id` = bi.`purchase_id` 

									  INNER JOIN `bs_suppliers` AS s 

										ON s.`supplierid` = bi.`supplier_id` 

									" . $subsql . " GROUP BY bi.`purchase_id` Order BY  bi.`purchase_id`DESC   ";

        $q = $this->db->query($sql);

        return $q->result();

    }



    function getSaleInvocies_new($id = '') {
        $subq = '';
        if ($id) {

            $subq = "WHERE c.`userid` = '" . $id . "'";

        }

        $get = $this->input->get();
        if($get){
            if($subq !=""){
                if ($get['frm']!=null && $get['to']!=null) {
                    $subq = " AND DATE(i.`purchase_date`) >= DATE('" . $get['frm'] . "') AND DATE(i.`purchase_date`) <= DATE('" . $get['to'] . "') ";

                }
                if(isset($get['bid']) && $get['bid']!="")
                    $subq.= ' AND i.branchid = '.$get['bid'];
            }
            else{
                if ($get['frm']!=null && $get['to']!=null) {
                    $subq = " Where DATE(i.`purchase_date`) >= DATE('" . $get['frm'] . "') AND DATE(i.`purchase_date`) <= DATE('" . $get['to'] . "') ";

                }
                if($subq !="") {
                    if (isset($get['bid']) && $get['bid'] != "")
                        $subq.= ' AND i.branchid = ' . $get['bid'];
                }
                elseif (isset($get['bid']) && $get['bid'] != ""){
                    $subq.= ' Where  i.branchid = ' . $get['bid'];
                }
            }
        }



      





         $sql="SELECT 

                i.`customer_id`,
  i.`purchase_id` AS in_id,
  i.`oldinvoice_num`,
  i.`sales_amount`,
  i.`purchase_total_amount`,
  i.`qid`,
  c.*,
  ex.invoice_id,
  ex.value,
  i.`purchase_date` AS i_date,
  SUM(ex.value) AS sumexvalue,
  suminvoicepurchase AS iipp,
  sum_invoiceprice AS sum_invoiceprice,
  sum_invoicediscount,
  purchase_totalDiscount AS alldiscount,
  sumipurchase,
  (sum_invoiceprice) AS remaining

              FROM

                `an_purchase_order` AS i 

                LEFT JOIN `bs_users` AS c 

                  ON c.`userid` = i.`customer_id` 


                LEFT JOIN 

                  (SELECT 

                    bii.*,

                    SUM(

                      bii.purchase_item_price_purchase

                    ) AS suminvoicepurchase,

                    SUM(

                      bii.purchase_item_price * bii.purchase_item_quantity

                    ) AS sum_invoiceprice,

                    SUM(

                      bii.purchase_item_discount * bii.purchase_item_quantity

                    ) AS sum_invoicediscount,

                  SUM(bs_item.purchase_price*bii.purchase_item_quantity) AS sumipurchase 

                  FROM bs_purchase_items_order AS bii 

                    LEFT JOIN bs_item ON bs_item.itemid = bii.purchase_item_id 

                  GROUP BY bii.`purchase_id`) iii 

                  ON iii.purchase_id = i.purchase_id 


                LEFT JOIN an_expenses AS ex 

                  ON ex.invoice_id = i.purchase_id
               ".$subq."

              GROUP BY i.`purchase_id` 

              ORDER BY i.`purchase_id` DESC  ";

        $query = $this->db->query($sql);

        return $query->result();

    }



//----------------------------------------------------------------------



    function get_calculation($id) {

        /*

          $sql="SELECT

          SUM(i.`sales_amount`) AS totalpayment,

          SUM(i.`sales_amount`) - pamount AS totalremaining

          FROM

          `an_invoice` AS i

          LEFT JOIN

          (SELECT

          (p.`payment_amount`) AS pamount,

          ip.`invoice_id`

          FROM

          `invoice_payments` AS ip

          INNER JOIN `an_invoice` AS ii

          ON ii.`invoice_id` = ip.`invoice_id`

          INNER JOIN `bs_users` AS c

          ON c.`userid` = '$id'

          INNER JOIN `payments` AS p

          ON p.`id` = ip.`payment_id`

          GROUP BY ii.`customer_id`,

          p.id) d

          ON d.invoice_id = i.`invoice_id`

          AND i.`invoice_status` = '0'

          WHERE i.`customer_id` = '$id' ";

         */

        /*

          $sql="SELECT

          sales,

          SUM(i.`sales_amount`) AS totalpayment,

          SUM(pamount) AS totalpaid



          FROM

          `an_invoice` AS i

          LEFT JOIN

          (SELECT

          (p.`payment_amount`) AS pamount,

          ip.`invoice_id`

          FROM

          `invoice_payments` AS ip

          INNER JOIN `an_invoice` AS ii

          ON ii.`invoice_id` = ip.`invoice_id`

          INNER JOIN `bs_users` AS c

          ON c.`userid` = '$id'

          INNER JOIN `payments` AS p

          ON p.`id` = ip.`payment_id`

          GROUP BY ii.`customer_id`,

          p.id) d

          ON d.invoice_id = i.`invoice_id`

          AND i.`invoice_status` = '0'

          LEFT JOIN

          (SELECT

          SUM(ii.`sales_amount`) AS sales,

          ii.`customer_id`

          FROM

          `an_invoice` AS ii

          GROUP BY ii.`customer_id`) AS id

          ON id.customer_id = i.`customer_id`

          WHERE i.`customer_id` = '$id' ";

         */

        $sql = "SELECT 

                c.`userid`,

                totalpayment2,

                IF(

                  totalpayment IS NULL,

                  0,

                  totalpayment

                ) AS totalpayment,

                IF(

                  totalinvpayment IS NULL,

                  0,

                  totalinvpayment

                ) AS totalinvpayment,

                IF(

                  totalsales IS NULL,

                  0,

                  totalsales

                ) AS totalsales,

                IF(

                  totalsales IS NULL,

                  0,

                  totalsales

                )-IF(

                  totalinvpayment IS NULL,

                  0,

                  totalinvpayment

                ) AS remainng,

                IF(

                  totalpayment IS NULL,

                  0,

                  totalpayment

                ) -IF(

                  totalinvpayment IS NULL,

                  0,

                  totalinvpayment

                ) AS balance

              FROM

                `bs_users` AS c 

                LEFT JOIN 

                  (SELECT 

                    SUM(p.`payment_amount`) AS totalpayment,

                    p.`refrence_id` 

                  FROM

                    `payments` AS p 

                  WHERE p.`refrence_id` = '$id') AS pp 

                  ON pp.refrence_id = c.userid 

                LEFT JOIN 

                  (SELECT 

                    SUM(ip.`payment_amount`) AS totalinvpayment,

                    ii.`customer_id` 

                  FROM

                    `purchase_payments` AS ip 

                    INNER JOIN `an_purchase` AS ii 

                      ON ii.`purchase_id` = ip.`purchase_id` 

                  WHERE ii.`customer_id` = '$id') AS ipp 

                  ON ipp.customer_id = c.userid 

                   LEFT JOIN 

                  (

              SELECT 

                SUM(i.`sales_amount`) AS totalsales,

                i.`customer_id` 

              FROM

                `an_purchase` AS i 

              WHERE i.`customer_id` = '$id') AS inv 

                  ON inv.customer_id = c.userid 

                  

            LEFT JOIN 

             (SELECT 

               SUM(p2.`payment_amount`) AS totalpayment2,

               p2.`refrence_id` 

             FROM

               `payments` AS p2 

             WHERE p2.`refrence_id` = '$id' AND p2.refernce_type='onaccount') AS pp2 

             ON pp2.refrence_id = c.userid 



              WHERE c.`userid` = '$id' 

              ";



        $q = $this->db->query($sql);



        if ($q->num_rows() > 0) {

            return $q->row();

        } else {

            return false;

        }

    }




    function updateData($table, $data, $id, $par = null) {

        if ($par == null) {

            $this->db->where('id', $id);

        } else {

            $this->db->where($par, $id);

        }

        $this->db->update($table, $data);

        //return $this->db->insert_id();

        return true;

    }



    function get_files($id) {

        $sql = "SELECT 

                purchase_document.*

              FROM

                `purchase_document` 



                  

                 WHERE purchase_document.purchase_id='$id'    

              ";

        $query = $this->db->query($sql);

        return $query->result();

    }



    function invoice_data($id) {

        /*

          $sql = "SELECT

          i.`customer_id`,

          i.`invoice_id` AS invoice_id,

          i.`sales_amount`,

          i.`invoice_total_amount`,

          i.`companyid` as company,

          i.`branchid` as branch,

          i.`invoice_totalDiscount` as invoice_totalDiscount,

          c.*,

          pamount,

          bii.invoice_item_discount_type AS discount_type,

          i.`invoice_date` AS i_date,

          SUM(pamount) AS amount,

          SUM(invoice_item_discount) AS iidiscount,

          SUM(invoice_item_price) AS iiprice,

          SUM(invoice_item_price_purchase) AS iiprice_purchase,

          sales_amount - pamount AS remaining,

          GROUP_CONCAT(ds.bs_user_id) AS cols,

          GROUP_CONCAT(bii.inovice_product_id) AS itemcols,

          SUM(bs_sales_commession) AS salescommession

          FROM

          `an_invoice` AS i

          INNER JOIN `bs_users` AS c

          ON c.`userid` = i.`customer_id`

          LEFT JOIN

          (SELECT

          SUM(ip.`payment_amount`) AS pamount,

          ip.`invoice_id`

          FROM

          `invoice_payments` AS ip

          INNER JOIN `an_invoice` AS ii

          ON ii.`invoice_id` = ip.`invoice_id`

          INNER JOIN `bs_users` AS c

          ON c.`userid` = ii.`customer_id`

          GROUP BY ip.`invoice_id`) d

          ON d.invoice_id = i.`invoice_id`



          LEFT JOIN bs_invoice_items AS bii

          ON bii.invoice_id = i.invoice_id

          LEFT JOIN bs_sales_invoice_persons AS spi

          ON spi.invoice_id = i.invoice_id

          LEFT JOIN bs_sales AS ds

          ON ds.bs_sales_id = spi.sales_id



          WHERE i.invoice_id='$id'

          GROUP BY i.`invoice_id`";

         */

        $sql = "SELECT 

                i.*,

                c.`userid`,

                c.`fullname`,

                c.`email_address`,

                c.`phone_number`,

                ino.*

              FROM

               `an_purchase_order` AS i

                INNER JOIN `bs_users` AS c 

                  ON c.`userid` = i.`customer_id` 

                  

                LEFT JOIN bs_purchase_items_order AS bii

                  ON bii.purchase_id = i.purchase_id 

                  

                LEFT JOIN bs_sales_invoice_persons AS spi 

                  ON spi.invoice_id = i.purchase_id 

                  

                LEFT JOIN bs_sales AS ds 

                  ON ds.bs_sales_id = spi.sales_id 

                  

                LEFT JOIN purchase_notes AS ino 

                  ON ino.purchase_id = i.purchase_id 

                  

              WHERE i.purchase_id='$id'    

              ";

        $query = $this->db->query($sql);

        return $query->result();

    }

    function getprInvoiceData($id){
         $sql = "SELECT * FROM `an_purchase_order` AS pr WHERE pr.`purchase_id` = '$id'";
       // exit;
        $query = $this->db->query($sql);

        return $query->row();
    }


    function getprInvoiceDataItem($id){
        $sql = "SELECT * FROM `bs_purchase_items_order` AS pr WHERE pr.`purchase_id` = '$id'";
        $query = $this->db->query($sql);

        return $query->result();
    }

    //////////////////////////////////////////////////////



    function getInvoiceitems($invoice_id) {

        $sql = "SELECT * FROM `bs_purchase_items`  AS bpi

        INNER JOIN `bs_item` AS i 

        ON i.`itemid` = bpi.`purchase_item_id`
        LEFT JOIN `units` AS u ON u.`unit_id` = i.`type_unit`
        WHERE bpi.`purchase_id` = " . $invoice_id . "";

        $query = $this->db->query($sql);

        return $query->result();

    }



    function getInvoiceByExpenseId($id) {

        $sql = "SELECT 

                TRIM(@t1),

                @t1 := (SELECT 

                SUM(ap.`purchase_total_amount`) AS total 

              FROM

                `an_purchase` AS ap 

                INNER JOIN `expense_purchase` AS epp 

                  ON epp.`invoice_id` = ap.`purchase_id` 

              WHERE epp.`expense_id` = exxxxid

              GROUP BY epp.`expense_id` ) AS ttttt,

                exxxxid,

                expenseval,

                @t2 := expenseval * 100 /@t1 AS expense_percantage,

                @t2/100*i.purchase_total_amount AS perpercentagevalue,

                ep.`invoice_id` AS epinvocieid,

                ep.`expense_id` AS expenseid,

                i.`customer_id`,

                i.`purchase_id` AS purchase_id,

                i.`sales_amount`,

                i.`purchase_total_amount`,

                c.*,

                pamount,

                bii.purchase_item_discount_type AS discount_type,

                i.`purchase_date` AS i_date,

                SUM(pamount) AS amount,

                SUM(purchase_item_discount) AS iidiscount,

                SUM(purchase_item_price) AS iiprice,

                SUM(purchase_item_price_purchase) AS iiprice_purchase,

                sales_amount - pamount AS remaining 

              FROM

                `an_purchase` AS i 

                INNER JOIN `bs_users` AS c 

                  ON c.`userid` = i.`customer_id` 

                LEFT JOIN 

                  (SELECT 

                    SUM(ip.`payment_amount`) AS pamount,

                    ip.`purchase_id` 

                  FROM

                    `purchase_payments` AS ip 

                    INNER JOIN `an_purchase` AS ii 

                      ON ii.`purchase_id` = ip.`purchase_id` 

                    INNER JOIN `bs_users` AS c 

                      ON c.`userid` = ii.`customer_id` 

                  GROUP BY ip.`purchase_id`) d 

                  ON d.purchase_id = i.`purchase_id` 

                LEFT JOIN bs_purchase_items AS bii 

                  ON bii.purchase_id = i.purchase_id 

                LEFT JOIN `expense_purchase` AS ep 

                  ON ep.`invoice_id` = i.purchase_id 

                LEFT JOIN 

                  (SELECT 

                    an_expenses.`id` AS exxxxid,

                    an_expenses.`value` AS expenseval

                   FROM

                    `an_expenses` 

                    INNER JOIN `expense_purchase` AS exp2 

                      ON exp2.`expense_id` = an_expenses.`id`ORDER BY exp2.invoice_id) AS ex 

                  ON ex.exxxxid = ep.`expense_id`            

                  WHERE  ep.`expense_id` = '" . $id . "'    

                        GROUP BY i.`purchase_id`";



        $query = $this->db->query($sql);

        return $query->result();

    }



    /*     * */



    function p_invoice($id = null) {



        $this->db->select('an_purchase_order.*');

        $this->db->select('bs_users.*');

        $this->db->select('bs_company.*');

        $this->db->select('company_logos.*');

        $this->db->where('an_purchase_order.purchase_id', $id);

        $this->db->join('bs_company', 'bs_company.companyid=an_purchase_order.companyid', 'LEFT');

        $this->db->join('bs_users', 'bs_users.userid=an_purchase_order.supplier_id', 'LEFT');

        $this->db->join('company_logos', 'company_logos.company_id=bs_company.companyid', 'LEFT');

        //$this->db->order_by();

        $Q = $this->db->get('an_purchase_order');

        return $Q->row();

    }



    function p_invoices($id = null) {



        $this->db->select('bs_purchase_items_order.*,bs_item.*,units.*,bs_store.*');

        $this->db->where('bs_purchase_items_order.purchase_id', $id);

        $this->db->join('bs_item', 'bs_item.itemid=bs_purchase_items_order.purchase_item_id', 'LEFT');
        $this->db->join('units', 'units.unit_id=bs_item.type_unit', 'LEFT');
		$this->db->join('bs_store', 'bs_store.storeid = bs_purchase_items_order.purchase_item_store_id', 'LEFT');
        //$this->db->order_by();

        $Q = $this->db->get('bs_purchase_items_order');

        return $Q->result();

    }



}

