<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
      <div class="title">
<span>New Payment</span>
</div>

<form action="" method="get">
<div class="form">
<div class="raw">
<div class="form_title">Customer</div>
<div class="form_field">
<!--dropmenu-->
<div class="dropmenu">
<div class="styled-select">
<select name="">
<option selected="selected" >Select Customer </option>
<option>Customer 2</option>
<option >Customer 3</option>
</select>
</div>
</div>
<!--end of dropmenu-->
</div>
</div>
<div class="raw">
<div class="form_title">Date</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
<div class="date_icon"><a href="#"><img src="<?php echo base_url();?>images/internal/date_icon.png" width="22" height="24" border="0" /></a></div>
</div>
<div id="osx2-modal-content">
  <div id="osx2-modal-title">Add New Gategory</div>
			<div class="close"><a href="#" class="simplemodal-close">x</a></div>
		<div id="osx2-modal-data">
<div class="raw">
<div class="form_title">Category Name</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Description</div>
<div class="form_field">
<textarea name="" cols="" rows="" class="formareafield"></textarea>
</div>
</div>
                    <div class="raw" align="center">
                    <input name="" type="submit" class="submit_btn" value="Submit" />
                    <input name="" type="reset" class="reset_btn" value="Reset" />
                    </div>
<!--end of raw-->

</div>
		</div>
</div>
<div class="raw">
<div class="form">
<div class="CSSTableGenerator" >
                <table width="100%" align="left" >
                    <tr>
                      <td width="1%"><label for="checkbox"></label>
                        All</td>
                        <td width="5%">Number</td>
                        <td width="8%">Total Price</td>
                        <td width="8%">Net Price</td>
                        <td width="19%" >Customer</td>
                        <td width="12%" >Date</td>
                        <td width="8%" >Status</td>
                        <td width="10%">Payments</td>
                        <td width="18%">Sales Responsble</td>
                        <td width="1%">&nbsp;</td>
                    </tr>
                    <tr>
                      <td  style="background-color:#afe2ee"><input type="checkbox" name="checkbox" id="checkbox" /></td>
                      <td  style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee">&nbsp;</td>
                    </tr>
                    <tr>
                      <td ><input type="checkbox" name="checkbox2" id="checkbox2" /></td>
                        <td >1112554</td>
                        <td style="text-align:center">296 RO.</td>
                        <td style="text-align:center">256 RO.</td>
                        <td>Ahmed Ali Mohmed</td>
                        <td>22 March 2013</td>
                        <td>Semi-Paid</td>
                        <td align="center">Cash</td>
                        <td>Said Alzokobly</td>
                        <td>	
			<a href="<?php echo base_url();?>payment_management/add_invoice" ><img src="<?php echo base_url();?>images/internal/view_icon.png" width="18" height="18" border="0" /></a>
        </td>
		
                        		
	
                    </tr>
                    <tr>
                      <td ><input type="checkbox" name="checkbox4" id="checkbox13" /></td>
                      <td >1112554</td>
                      <td style="text-align:center">296 RO.</td>
                      <td style="text-align:center">256 RO.</td>
                      <td>Ahmed Ali Mohmed</td>
                      <td>22 March 2013</td>
                      <td>Semi-Paid</td>
                      <td align="center"> Cheque</td>
                      <td>Said Alzokobly</td>
                      <td><a href="<?php echo base_url();?>payment_management/add_invoice" ><img src="<?php echo base_url();?>images/internal/view_icon.png" width="18" height="18" border="0" /></a></td>
                    </tr>
                    <tr>
                      <td ><input type="checkbox" name="checkbox4" id="checkbox6" /></td>
                      <td >1112554</td>
                      <td style="text-align:center">296 RO.</td>
                      <td style="text-align:center">256 RO.</td>
                      <td>Ahmed Ali Mohmed</td>
                      <td>22 March 2013</td>
                      <td>Semi-Paid</td>
                      <td align="center">Cash</td>
                      <td>Said Alzokobly</td>
                      <td><a href="<?php echo base_url();?>payment_management/add_invoice" ><img src="<?php echo base_url();?>images/internal/view_icon.png" width="18" height="18" border="0" /></a></td>
                    </tr>
                    <tr>
                      <td ><input type="checkbox" name="checkbox4" id="checkbox16" /></td>
                      <td >1112554</td>
                      <td style="text-align:center">296 RO.</td>
                      <td style="text-align:center">256 RO.</td>
                      <td>Ahmed Ali Mohmed</td>
                      <td>22 March 2013</td>
                      <td>Semi-Paid</td>
                      <td align="center"> Cheque</td>
                      <td>Said Alzokobly</td>
                      <td><a href="<?php echo base_url();?>payment_management/add_invoice" ><img src="<?php echo base_url();?>images/internal/view_icon.png" width="18" height="18" border="0" /></a></td>
                    </tr>
                    <tr>
                      <td ><input type="checkbox" name="checkbox4" id="checkbox21" /></td>
                      <td >1112554</td>
                      <td style="text-align:center">296 RO.</td>
                      <td style="text-align:center">256 RO.</td>
                      <td>Ahmed Ali Mohmed</td>
                      <td>22 March 2013</td>
                      <td>Semi-Paid</td>
                      <td align="center">Cash</td>
                      <td>Said Alzokobly</td>
                      <td><a href="<?php echo base_url();?>payment_management/add_invoice" ><img src="<?php echo base_url();?>images/internal/view_icon.png" width="18" height="18" border="0" /></a></td>
                    </tr>
                    <tr>
                      <td ><input type="checkbox" name="checkbox4" id="checkbox20" /></td>
                      <td >1112554</td>
                      <td style="text-align:center">296 RO.</td>
                      <td style="text-align:center">256 RO.</td>
                      <td>Ahmed Ali Mohmed</td>
                      <td>22 March 2013</td>
                      <td>Semi-Paid</td>
                      <td align="center">Cash</td>
                      <td>Said Alzokobly</td>
                      <td><a href="<?php echo base_url();?>payment_management/add_invoice" ><img src="<?php echo base_url();?>images/internal/view_icon.png" width="18" height="18" border="0" /></a></td>
                    </tr>
                    <tr>
                      <td colspan="3" ><strong>Total</strong></td>
                      <td style="background-color:#335a85; color:#FFF; text-align:center;">1500 RO.</td>
                      <td colspan="6">&nbsp;</td>
                    </tr>
                </table>
            </div>
            
</div>
</div>

<div class="raw">
<div class="form_title">Paid</div>
<div class="form_field">
  <input name="input" type="text"  class="formtxtfield_small"/> <span> RO.</span>
</div>

</div>
<div class="raw">
<div class="form_title"> On Credit</div>
<div class="form_field">  <input name="input" type="text"  class="formtxtfield_small"/> <span> RO.</span></div>
</div>
<div class="raw">
<div class="form_title">Method</div>
<div class="form_field">
<div class="dropmenu">
<div class="styled-select">
<select name="">
<option selected="selected" >Select Method </option>
<option>Cash</option>
<option >Cheque</option>
<option >Deposit</option>
<option >Receipt</option>
</select>
</div>
</div>
</div>

</div>
<div class="raw">
<div class="form_title">Cheque Number</div>
<div class="form_field">
  <input name="input" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Cheque Date</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
<div class="date_icon"><a href="#"><img src="<?php echo base_url();?>images/internal/date_icon.png" width="22" height="24" border="0" /></a></div>
</div>
</div>
<div class="raw">
<div class="form_title">Bank</div>
<div class="form_field">
<div class="dropmenu">
<div class="styled-select">
<select name="">
<option selected="selected" >Select Bank </option>
<option>Muscat</option>
<option >International Oman</option>
<option >HSBC</option>
<option >Dhofar</option>
</select>
</div>
</div>

</div>

</div>


<div class="raw" align="center">
        <input name="" type="submit" class="submit_btn" value="Add" />
                    <input name="" type="reset" class="reset_btn" value="Reset" />
                    </div>
<!--end of raw-->
</div>
</form>

</div>
      <!-- END PAGE -->  
   </div>
</section>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
