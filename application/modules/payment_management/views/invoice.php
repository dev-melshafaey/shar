<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
      <div class="title">
<span><a href="invoices.html">Invoices</a></span>
<div class="subtitle">Invoice</div>
</div>
<div class="invoice">
<h2 class="invoice_number"><strong>Invoice Number :</strong> <span class="invoice_num">253625</span>
</h2>
<Div class="invoice_raw">
<div class="invoice_customer"><strong>Name :</strong> nAhmed Mohamed Ali Albosaidy</div>



<div class="invoice_customer_code"><strong>Customer Code :</strong> 555226</div>
</Div>
<!--end raw--->
<div class="invoice_raw">
<div class="invoice_no"><strong>Phone  No :</strong> 588 55 888</div>
<div class="invoice_no"><strong>Office  No :</strong> 588 55 888</div>
<div class="invoice_no"><strong>Fax  No :</strong> 588 55 888</div>
<div class="invoice_type"><strong>Type :</strong> Company</div>
</div>
<!--end raw--->
<div class="invoice_raw">
<Div class="invoice_no"><strong>Pruduct :</strong> Filters</Div>
<Div class="invoice_no"><strong>Quantity : </strong>25</Div>
<div class="invoice_type"><strong>Serial :</strong> #5552</div>
</div>
<!--end raw--->
<div class="invoice_raw">
<div class="invoice_dis">
<span>Discription</span>
Write your discription here about Product and everything that will be effective to user and customer it is entered by admin by C-Panel
</div>
</div>
<!--end raw--->
<div class="invoice_raw">
<div class="invoice_dis">
<span>Notes</span>
Write your notes here about Product and everything that will be effective to user and customer it is entered by admin by C-Panel
</div>
</div>
<!--end raw--->
<div class="invoice_raw">
<div class="invoice_table" >
                <table width="100%" align="left" >
                    <tr>
                      <td width="5%">#
                        <label for="checkbox"></label></td>
                        <td width="10%">Image</td>
                        <td width="17%" >Name</td>
                        <td width="10%" >Quantity</td>
                        <td width="20%" >Discription</td>
                        <td width="12%">Total</td>
                        <td width="12%">Discount</td>
                        <td width="12%">Net</td>
                        <td width="2%">&nbsp;</td>
                      </tr>
                    <tr>
                      <td >1</td>
                      <td ><img src="images/internal/product_icon.jpg" width="50" height="50" /></td>
                      <td>Filter 55-R</td>
                      <td align="center" valign="middle" style="text-align:center">25</td>
                      <td>filter and water system</td>
                      <td style="text-align:center">150 RO.</td>
                      <td style="text-align:center">10%</td>
                      <td style="text-align:center">135 RO.</td>
                      <td align="center" valign="middle" style="text-align:center"><img src="images/internal/maintenance.png" width="26" height="26" /></td>
                    </tr>
                    <tr>
                      <td >2</td>
                      <td ><img src="images/product_small.jpg" width="50" height="50" /></td>
                      <td> Filter NC</td>
                      <td align="center" valign="middle" style="text-align:center">5</td>
                      <td>filter and water system</td>
                      <td style="text-align:center">255 RO.</td>
                      <td style="text-align:center">10%</td>
                      <td style="text-align:center">230 RO.</td>
                      <td align="center" valign="middle" style="text-align:center">&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="7" bgcolor="#FFFFFF" ><strong>Total Price</strong></td>
                      <td bgcolor="#FFFFFF" style="text-align:center"><strong>365 RO.</strong></td>
                      <td align="center" valign="middle" bgcolor="#FFFFFF" style="text-align:center">&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="7" bgcolor="#FFFFFF" >Discount</td>
                      <td bgcolor="#FFFFFF" style="text-align:center">10%</td>
                      <td align="center" valign="middle" bgcolor="#FFFFFF" style="text-align:center">&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="7" bgcolor="#FFFFFF" ><strong>Net Price</strong></td>
                      <td bgcolor="#FFFFFF" style="text-align:center"><strong>329 RO.</strong></td>
                      <td align="center" valign="middle" bgcolor="#FFFFFF" style="text-align:center">&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="7" bgcolor="#FFFFFF" >Recieved</td>
                      <td bgcolor="#FFFFFF"  style="text-align:center"><div class="blue_txt">350 RO.</div></td>
                      <td align="center" valign="middle" bgcolor="#FFFFFF" style="text-align:center">&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="7" bgcolor="#FFFFFF" >Rest of Amount</td>
                      <td bgcolor="#FFFFFF"  style="text-align:center"><div class="blue_txt">11 RO.</div></td>
                      <td align="center" valign="middle" bgcolor="#FFFFFF" style="text-align:center">&nbsp;</td>
                    </tr>
                </table>
            </div>
</div>
<!--end raw--->
<Div class="raw">
<div class="mant_title">Products Maintenance</div>
<div class="invoice_raw">
<div class="invoice_mant"><strong>Filter 55R</strong></div>
<div class="invoice_mant"><strong>Every</strong> 10 Days</div>
<div class="invoice_mant"><strong>From : </strong>15 March 2012</div>
<div class="invoice_mant"><strong>Maintenance Period  : </strong>1 year</div>
</div>
<div class="invoice_raw">
<div class="invoice_mant"><strong>Filter 55R</strong></div>
<div class="invoice_mant"><strong>Every</strong> 10 Days</div>
<div class="invoice_mant"><strong>From : </strong>15 March 2012</div>
<div class="invoice_mant"><strong>Maintenance Period  : </strong>1 year</div>
</div>
</Div>
<!--end raw--->
<!--end raw--->
<!--end raw--->
</div>
<div class="form">
<div class="raw_payments">
<div class="form_title"><input name="" type="checkbox" value="" /> Payments</div>
<div class="form_field">
    2555 RO.
  </div>
</div>
<div class="raw_payments">
  <div class="form_title">Type
</div>
<div class="form_field">
Cheque
</div>


  
<div class="form_title2" style="width:60px;">Bank
</div>
  <div class="form_field">
    Muscat Bank

  </div>

</div>
<div class="raw_payments">
  <div class="form_title">Cheque Number
</div>
<div class="form_field">
    253633232
</div>


  
<div class="form_title2" style="width:60px;">Period
</div>
  <div class="form_field">
    253 Days
  </div>
  <div class="form_title2">Payment  Numbers
</div>
  <div class="form_field">
    #25952
  </div>
</div>
<div class="raw">
  <div class="form_title">  From Customer Account</div>
  <!--
<div class="rest_money_min">- 25 RO.</div>-->
<div class="rest_money_plus">+ 25 RO.</div>
</div>
<div class="raw">
<div class="form_title">Data Entry Name</div>
<div class="form_field">
Ahmed Mohamed</div>
<div class="form_title2">Code</div>
<div class="form_field">
# 2555
</div>

</div>
<div class="raw">
<div class="form_title">Responsable  Name</div>
<div class="form_field">
Said Alhodoo

</div>
<div class="form_title2">Code</div>
<div class="form_field">
#55588
</div>

</div>
<div class="raw">
  <div class="form_title">Total Profit</div>
<div class="form_field">
225 Ro.
</div>
</div>
<div class="raw">
<div class="form_title">commission</div>
<div class="form_field">
10 %
</div>
</div>
<div class="raw">
<div class="form_title">Purchase amount </div>
<div class="form_field">
200 Ro.
</div>
</div>


<div class="raw">
<div class="form_title">Net Profit</div>
<div class="form_field">
25 RO.
</div>
</div>
</div>
<input type="submit" class="print_icon" value=""/>
<input type="submit" class="delete_icon" value=""/>
<input type="submit" class="edit_icon" value=""/>
<a href="<?php echo base_url();?>payment_management/add_invoice">
<div class="add_icon">
</div>
</a>
<!--<input type="submit" class="add_icon" value=""/>-->
<input type="submit" class="cancel_icon" value=""/>
<!--<input type="submit" class="send_icon" value=""/>-->
</div>
      <!-- END PAGE -->  
   </div>
</section>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
