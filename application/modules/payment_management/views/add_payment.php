
<script type="text/javascript">

    function showRemaining() {
        pay_amount = $("#payment_amount").val();
        refrence_val = $(".refrence_by:checked").val();

        if (pay_amount) {

            if (refrence_val == 'against_reference') {
                rem = $("#total_remaining").val();
                pay_remianing = pay_amount - rem;
                if (parseInt(pay_amount) >= parseInt(rem)) {
                    cl_status = $("#rem_payment").hasClass('rest_money_min')
                    if (cl_status) {
                        $("#rem_payment").removeClass('rest_money_min');
                    }
                    $("#rem_payment").addClass('rest_money_plus');
                    if (pay_remianing) {
                        p_rm = pay_remianing.toFixed(2);
                        $("#rem_payment").html('+' + p_rm + ' RO');
                    }
                }
                else {
                    //alert('else');
                    cl_status = $("#rem_payment").hasClass('rest_money_plus')
                    if (cl_status) {
                        $("#rem_payment").removeClass('rest_money_plus');
                    }
                    p_rm = pay_remianing.toFixed(2);
                    $("#rem_payment").addClass('rest_money_min');
                    $("#rem_payment").html(+p_rm + ' RO');
                }
            }
            if (refrence_val == 'on_account') {

                $("#bl_div").hide();
                pay_remianing = pay_amount - charges;
                if (parseInt(pay_amount) >= parseInt(charges)) {
                    cl_status = $("#rem_payment").hasClass('rest_money_min')
                    if (cl_status) {
                        $("#rem_payment").removeClass('rest_money_min');
                    }
                    $("#rem_payment").addClass('rest_money_plus');
                    if (pay_remianing) {
                        p_rm = pay_remianing.toFixed(2);
                        $("#rem_payment").html('+' + p_rm + ' RO');
                    }
                }
                else {
                    //alert('else');
                    cl_status = $("#rem_payment").hasClass('rest_money_plus')
                    if (cl_status) {
                        $("#rem_payment").removeClass('rest_money_plus');
                    }
                    p_rm = pay_remianing.toFixed(2);
                    $("#rem_payment").addClass('rest_money_min');
                    $("#rem_payment").html(+p_rm + ' RO');
                }

            }

        }
    }

    function showInvocices() {

        refrence_val = $(".refrence_by:checked").val();
        customer_id = $("#customer_id").val();

        if (refrence_val == 'against_reference') {
            $.ajax({
                url: config.BASE_URL + "ajax/calculate_pending_jobs",
                type: 'post',
                data: {customer_id: customer_id},
                cache: false,
                success: function (data) {
                    console.log(data);
                    $("#inv_tbl").html(data);
                    $("#inv_tbl").show();
                    $("#bl_div").hide();
                    $("#charges_due").hide();
                }
            });
        }
        else if (refrence_val == 'advance') {
            $("#inv_tbl").hide();
            $("#bl_div").show();
            $("#charges_due").hide();
        }
        else if (refrence_val == 'on_account') {
            $("#bl_div").hide();
            $("#inv_tbl").hide();

            $.ajax({
                url: config.BASE_URL + "customers/getCustomePayments/",
                type: 'post',
                data: {customer_id: customer_id},
                cache: false,
                success: function (data) {
                    res = $.parseJSON(data);
                    console.log(res);
                    charges = res.totalcharges;
                    if (res.totalpayment) {
                        charges = res.totalcharges - res.totalpayment;
                    }

                    if (charges) {
                        $("#charges_due").show();
                        $("#due_charges").html(charges);

                    }

                    //	$("#inv_tbl").html(data);
                    //	$("#inv_tbl").show();
                    //	$("#bl_div").hide();
                }
            });
            //$("#inv_tbl").hide();
            //$("#bl_div").hide();
        }

        else {
            //$("#inv_tbl").hide();			
        }
    }

    $(document).ready(function () {

        $(".refrence_by").change(function () {

            pay_amount = $(this).val();
            refrence_val = $(".refrence_by:checked").val();
            customer_id = $("#customer_id").val();
            if (refrence_val == 'against_reference') {
                $.ajax({
                    url: config.BASE_URL + "ajax/calculate_pending_jobs",
                    type: 'post',
                    data: {pay_amount: pay_amount, customer_id: customer_id},
                    cache: false,
                    success: function (data) {
                        console.log(data);
                        $("#inv_tbl").html(data);
                        $("#inv_tbl").show();
                    }
                });
            }

        });

        $("#charges_job").change(function () {

            c_id = $(this).val();
            jobId = $("#bl_number").val();

            $.ajax({
                url: config.BASE_URL + "ajax/check_charges",
                type: 'post',
                data: {c_id: c_id, job_id: jobId},
                cache: false,
                success: function (data) {

                    //console.log(data+'data');
                    res = $.parseJSON(data);
                    console.log(res + 'res');
                    if (res.jb_c_id) {
                        $("#job_charges_id").val(res.jb_c_id);
                    }

                    if (res.is_actual == '1') {
                        //alert(res.is_actual);

                        /*if(res.charges_advance){
                         $("#advance").val(res.charges_advance);	
                         }*/

                        if (res.charges_advance) {
                            $("#advance").val(0);
                        }

                        $("#advance_payment").show();
                    }
                    else {
                        $("#advance_payment").hide();
                    }

                    if (res.total_value) {
                        $("#payment_amount").val(res.total_value);
                    }
                    $("#payment_payment").show();


                    //$("#charges_job").html(data);
                    //$("#div_charges").fadeIn();
                }
            });

            $("#submit_cash").click(function () {
                $(".jobs_way").remove();
            });

        })

        $("#payment_type").change(function () {
            val = $(this).val();
            //alert(val);
            if (val == 'accounts') {
                $("#account").show();
            }
            else {
                $("#account").hide();
            }
        })

        $(".payment_by").click(function () {
            transaction_val = $(this).val();
            if (transaction_val == 'bank') {
                $("#div_banks").show();
            }
            else {
                $("#div_banks").hide();
            }

        });

        var ac_config = {
            source: "<?php echo base_url(); ?>customers/getAutoSearchCustomer",
            select: function (event, ui) {
                $("#customer_name").val(ui.item.cus);
                $("#customer_id").val(ui.item.cus);
                console.log(ui);
                //swapme();
                setTimeout('swapme()', 500);
            },
            minLength: 1
        };

        $("#customer_name").autocomplete(ac_config);


        var ac_config = {
            source: "<?php echo base_url(); ?>customers/getAutoSearchJob",
            select: function (event, ui) {
                $("#bl_number").val(ui.item.cus);
                console.log(ui);
                //swapme();
                $("#div_charges").show();

            },
            minLength: 1
        };

        $("#bl_number").autocomplete(ac_config);

        $('#cheque_date').datepicker({dateFormat: 'yy-mm-dd'});

    })


</script>
</header>

<div class="table-wrapper users-table " style="margin-top:0px" >

    <div class="row head">
        <div class="col-md-12">
            <h4>

                <div class="title"> <span><?php breadcramb(); ?></span> </div>


            </h4>
            <?php error_hander($this->input->get('e')); ?>
        </div>
    </div>


    <form id="form_cashs" action="<?php echo base_url(); ?>customers/add_payment_post" method="post" enctype="multipart/form-data">
        <div class="form" id="form-refresh"></div>



        <input type="hidden" id="job_charges_id" name="job_charges_id" />
        <?php
        //echo "<pre>";
        //print_r($payment);
        //print_r($remaing);
        ?>
        <div class="col-md-4 form-group">
            <label class="col-lg-12">Customer </label>

            <input name="customer_name" id="customer_name" type="text"  class="form-control"  />
            <input type="hidden" name="customer_id" id="customer_id" />

        </div>
        
        <div class="col-md-4 form-group">
            <label class="col-lg-12">Type </label>
            <div class="defaultP">
                <input type="radio" name="refrence_by" class="refrence_by" value="on_account" id="refrence_id1" onclick="showInvocices()"> <label for="refrence_id1"  style="margin-right: 8px;">on Account</label>
                <input type="radio" name="refrence_by" class="refrence_by" value="advance" id="refrence_id2" onclick="showInvocices()"> <label for="refrence_id2"  style="margin-right: 8px;">Advance</label><input type="radio" name="refrence_by" class="refrence_by" value="against_reference" id="reference_id3" onclick="showInvocices()"> <label for="refrence_id3"  style="margin-right: 8px;">Against Reference</label></div>

        </div>
        <br clear="all"/>
        <div id="inv_tbl" class="invoice_table" style="display:none;"></div>
        <div class="col-md-4 form-group" id="bl_div" style="display:none;">
            <label class="col-lg-12">Job / Bl Number </label>

            <input name="bl_number" id="bl_number" type="text"  class="form-control"/>

        </div>

        <div class="col-md-4 form-group" id="div_charges" style="display:none;">
            <label class="col-lg-12">Charges</label>

            <div class="dropmenu">
                <div class="styled-select" id="div_charges_responce">
                    <select name="charges_job" id="charges_job">
                        <?php
                        foreach ($charges_all as $charges) {
                            ?>
                            <option value="<?php echo $charges->charge_id; ?>"><?php echo $charges->charge_name; ?></option>  
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>

        </div>
        <div class="col-md-4 form-group" id="charges_due" style="display:none;">
            <label class="col-lg-12">Total Due Charges </label>

            <span id="due_charges"></span>
            <span>RO.</span>

        </div>
        <div class="col-md-4 form-group">
            <label class="col-lg-12">Value </label>

            <input name="payment_amount" id="payment_amount" type="text"  class="form-control" onchange="showRemaining()"/>
            <span>RO.</span>

        </div>
        <div class="col-md-4 form-group">        
            <label class="col-lg-12"></label>
            <div  id="rem_payment"></div>
        </div>
        
        <br clear="all"/>
        <div class="col-md-4 form-group">   
            <label class="col-lg-12">Payment Method</label>

            <div class="defaultP">
                <input type="radio" name="payment_method" class="payment_by" value="bank" id="payment_type1"> <label for="payment_type1">Bank</label>
                <input type="radio" name="payment_method" class="payment_method" value="cash" id="payment_type2"> <label for="payment_type2">Cash</label></div>

        </div>
        <div class="col-md-4 form-group">
            <label class="col-lg-12">Payment Type</label>

            <div class="dropmenu">
                <div class="styled-select">
                    <select name="payment_type" id="payment_type" class="form-control" onchange="BankOrCashToggle();">
                        <option value="0">Select Payment Way</option>
                        <option value="transfer">Transfer</option>
                        <option value="deposite">Deposite</option>
                        <option value="cheque">Cheque</option>
                        <option value="withdraw">Withdraw</option>
                    </select>
                </div>
            </div>

        </div>
         <br clear="all"/>
        <div class="col-md-4 form-group" id="div_banks" style="display:none;">
            <label class="col-lg-12">Banks </label>

            <div class="dropmenu">
                <div class="styled-select">
                    <?php company_bank_dropbox('bank_id', '', 'english', ' onchange="getBankAccounts();" '); ?>
                </div>
            </div>

        </div>

        <div class="col-md-4 form-group" style="display:none" id="div_accounts">
            <label class="col-lg-12">Accounts </label>

            <div class="dropmenu">
                <div class="styled-select" id="div_accounts_responce">

                </div>
            </div>

        </div>
        <div class="col-md-4 form-group" style="display:none" id="div_tr_accounts">
            <label class="col-lg-12">Accounts </label>

            <div class="dropmenu">
                <div class="styled-select" id="div_tr_accounts_responce">

                </div>
            </div>

        </div>

        <div class="col-md-4 form-group" style="display:none" id="cheque_div_date">
            <label class="col-lg-12">Cheque Date </label>

            <input name="cheque_date" id="cheque_date" type="text"  class="form-control"/>


        </div>
        <div class="col-md-4 form-group" style="display:none" id="cheque_div_number">
            <label class="col-lg-12">Cheque Number </label>

            <input name="cheque_number" id="cheque_nummber" type="text"  class="form-control"/>


        </div>
        <div class="col-md-4 form-group" id="div_checkque" style="display:none;">
            <label class="col-lg-12">Deposite Recipt </label>

            <div class="dropmenu">
                <div class="styled-select">
                    <input type="file"  class="form-control" name="deposite_recipt" id="deposite_recipt" />
                </div>
            </div>

        </div>
        <div class="raw">
            <label class="col-lg-12">Notes </label>

            <textarea name="notes" id="notes" cols="" rows="" class="form-control"></textarea>

        </div>

        <br clear="all"/> 
        <div class="raw" align="center">
            <input name="submit_cash" id="submit_cash" type="submit" class="btn-flat primary" value="Add" />
            <!-- <input name="id" type="hidden"  value="" /> -->

        </div>
        <!--end of raw-->
</div>
</form>
<!-- END PAGE -->  
</div>
