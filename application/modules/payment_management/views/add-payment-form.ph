<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/left-navigations');?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content" class="main_content">
    	<div class="title">
    		<span><a href="banks.html">Banks</a></span>
    		<span class="subtitle">New Bank Transaction</span>
    	</div>

		<form id="form_transactions" action="<?php echo base_url();?>transaction_management/add_transaction" method="post" enctype="multipart/form-data">
			<div class="form" id="form-refresh">
          
                <div class="raw form-group">
                    <div class="form_title">Branch Name</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
                          <?php company_branch_dropbox('branch_id',isset($user->branchid)?$user->branchid:'',' onchange="loadCustomers()" '); ?>
                        </div>
                      </div>
                    </div>
                </div>
                  
                <div class="raw form-group" id="div_customers" style="display:none;">
                    <div class="form_title">Customer</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select" id="div_customers_responce">
                            <select id="customer_id" name="customer_id" <?php echo isset($job->customer_id)?'disabled="disabled"':'';?>>
                            
								<?php
                                foreach($customers as $eachCus){
                                
									?>
									<option <?php echo (isset($job->customer_id) && $eachCus->id == $job->customer_id)?'selected="selected"':'';?> value="<?php echo $eachCus->id; ?>"><?php echo $eachCus->customer_name; ?></option>
									<?php
                                }
                                ?>
                            </select>
                        </div>
                      </div>
                    </div>
                </div>
                  
                <div class="raw form-group">
                    <div class="form_title">Job Status</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
                        	<?php get_statusdropdown((isset($transaction->job_status) && $transaction->job_status !='' )?$transaction->job_status:'','job_status','transaction_type',false,' onchange="loadjobsWithStatus()" ');?>
                        </div>
                      </div>
                    </div>
                </div>
                
                <div class="raw form-group" id="div_jobs" style="display:none;">
                    <div class="form_title">Jobs</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select" id="div_jobs_responce">
                           
                        </div>
                      </div>
                    </div>
                </div>
                
                <div class="raw form-group" id="div_charges" style="display:none;">
                    <div class="form_title">Charges</div>
                    <div class="form_field">
                        <div class="dropmenu">
                            <div class="styled-select" id="div_charges_responce">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="raw form-group">
                    <div class="form_title">Transaction Type</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
                          <select name="transaction_type" id="transaction_type">
                            <option value="debit">Debit</option>
                            <option value="credit">Credit</option>
                          </select>
                        </div>
                      </div>
                    </div>
                </div>
                
                <div class="raw form-group">
                    <div class="form_title">Payment Type</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
                          <select name="payment_type" id="payment_type">
                            <option value="deposit">DEPOSIT</option>
                            <option value="cheque">CHEQUE</option>
                            <option value="cash">CASH</option>
                            <option value="Recipt">RECIPT</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                <div class="raw form-group">
                    <div class="form_title" id="change-text">Cheque Number</div>
                    <div class="form_field">
                    <input name="cheque_number" id="cheque_number" type="text"  class="formtxtfield"/>
                    </div>
                  </div>
        
                <div class="raw form-group">
                    <div class="form_title">Value </div>
                    <div class="form_field">
                        <input name="value" id="value" type="text"  class="formtxtfield_small"/>
                        <span>RO.</span>
                    </div>
                </div>
                <div class="raw form-group">
                    <div class="form_title">Account Name </div>
                    <div class="form_field">
                        <input name="account_name" id="account_name" type="text"  class="formtxtfield"/>
                        
                    </div>
                </div>
                
                <div class="raw form-group">
                    <div class="form_title">Bank Name </div>
                    <div class="form_field">
                        <input name="bank_name" id="bank_name" type="text"  class="formtxtfield"/>
                        
                    </div>
                </div>
                
                <div class="raw form-group">
                    <div class="form_title">Account Number </div>
                    <div class="form_field">
                        <input name="account_number" id="account_number" type="text"  class="formtxtfield"/>
                        
                    </div>
                </div>
                
                <div class="raw">
                    <div class="form_title">Notes </div>
                    <div class="form_field">
                        <textarea name="notes" id="notes" cols="" rows="" class="formareafield"></textarea>
                    </div>
                </div>
                
                <div class="raw" align="center">
                    <input name="submit_transaction" id="submit_transaction" type="submit" class="submit_btn" value="Add" />
                    <!-- <input name="id" type="hidden"  value="" /> -->
                    
                </div>
                <!--end of raw-->
			</div>
		</form>
		<!-- END PAGE -->  
	</div>
    <!-- END PAGE -->  
</div>
</section>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
