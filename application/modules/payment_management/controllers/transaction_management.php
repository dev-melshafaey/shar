<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction_management extends CI_Controller
{
	/*
	* Properties
	*/

    private $_data	=	array();

//----------------------------------------------------------------------
	/*
	* Constructor
	*/
   public function __construct()
   {
		parent::__construct();
		
		//load Home Model
	   $this->load->model('transaction_management_model','transaction_management');
	   $this->load->model('jobs/jobs_model','jobs');	 
	   $this->load->model('customers/customers_model','customers');	 
	   $this->load->model('users/users_model','users');	  
	   $this->_data['udata'] = userinfo_permission();
   }
   
//----------------------------------------------------------------------

	/*
	* transaction Home Page
	*/
   public function index()
   {
	   // Load Home View
	   $this->load->view('transactions',$this->_data);
   }
//----------------------------------------------------------------------

	/*
	* Add transaction Form Page
	*/
   public function add_transaction()
   {
	   // Load Home View
	   $data = $this->input->post();
	   unset($data['job_status'],$data['submit_transaction']);
	   $data['charge_id'] = $data['charges_selection'];
	   unset($data['charges_selection']);
	   $id = $this->transaction_management->insertNewtransaction($data);
	   if($id){
	       $this->transaction_management->udpateJobStatus($data['job_id']);
	   }
	   redirect(base_url().'transaction_management');
   }
//----------------------------------------------------------------------


	/*
	* Add transaction Form Page
	*/
   public function new_transaction()
   {
	   // Load Home View
	   $this->_data['customers'] = $this->jobs->get_all_customers();
	   $this->load->view('add-transaction-form',$this->_data);
   }


	/*
	* Add transaction Form Page
	*/
   public function transaction_methods()
   {
	   // Load Home View
	   $this->load->view('transaction-methods',$this->_data);
   }
 //----------------------------------------------------------------------

	/*
	* Add New Invoice
	*/
   public function add_invoice()
   {
	   // Load Home View
	   $this->load->view('add-invoice',	$this->_data);
   }
 //----------------------------------------------------------------------

	/*
	* Add New Invoice
	*/
   public function invoice()
   {
	   // Load Home View
	   $this->load->view('invoice',	$this->_data);
   }
//----------------------------------------------------------------------
}
?>