<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_management extends CI_Controller {
    /*
     * Properties
     */

    private $_data = array();

//----------------------------------------------------------------------
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        //load Home Model
        $this->load->model('payment_management_model', 'payment_management');
        $this->load->model('jobs/jobs_model', 'jobs');
        $this->load->model('payment_management/payment_management_model', 'payment_management');
        $this->load->model('users/users_model', 'users');
        $this->_data['udata'] = userinfo_permission();
    }

//----------------------------------------------------------------------

    /*
     * payment Home Page
     */
    public function index() {
        // Load Home View
        $this->load->view('payments', $this->_data);
    }

//----------------------------------------------------------------------

    /*
     * Add payment Form Page
     */
    public function add_payment() {
        // Load Home View
        $data = $this->input->post();
        unset($data['job_status'], $data['submit_payment']);
        $data['charge_id'] = $data['charges_selection'];
        unset($data['charges_selection']);
        $id = $this->payment_management->insertNewpayment($data);
        if ($id) {
            $this->payment_management->udpateJobStatus($data['job_id']);
        }
        redirect(base_url() . 'payment_management');
    }

//----------------------------------------------------------------------


    /*
     * Add payment Form Page
     */
    public function new_payment_old() {
        // Load Home View
        $this->_data['payment_management'] = $this->jobs->get_all_payment_management();
        $this->load->view('add-payment-form', $this->_data);
    }

    /*
     * Add payment Form Page
     */

    public function payment_methods() {
        // Load Home View
        //$this->load->view('payment-methods', $this->_data);
        
        $this->_data['inside'] = 'payment-methods';
        $this->load->view('common/main', $this->_data);
    }

    //----------------------------------------------------------------------

    /*
     * Add New Invoice
     */
    public function add_invoice() {
        // Load Home View
        $this->load->view('add-invoice', $this->_data);
    }

    //----------------------------------------------------------------------

    /*
     * Add New Invoice
     */
    public function invoice() {
        // Load Home View
        $this->load->view('invoice', $this->_data);
    }

    function new_payment() {

        $this->_data['charges_all'] = $this->payment_management->getAllcharges();
        //$this->load->view('add_payment',$this->_data);

        $this->_data['inside'] = 'add_payment';
        $this->load->view('common/main', $this->_data);
    }

    function add_payment_post() {
        $post = $this->input->post();
        //echo "<pre>";
        //print_r($post);
        //exit;
        $data['user_id'] = $this->session->userdata('userid');

        if (isset($post['invoice_payments']))
            $jobcharges = $post['invoice_payments'];

        $userid = $this->session->userdata('userid');
        $user = $this->users->get_user_detail($userid);
        //echo "<pre>";
        //print_r($user);
        //exit;
        $data['refrence_id'] = $post['customer_id'];
        $data['payment_amount'] = $post['payment_amount'];
        $data['payment_type'] = 'receipt';
        if ($post['refrence_by'] == 'against_reference') {
            $data['refernce_type'] = 'against';
        } elseif ($post['refrence_by'] == 'on_account') {
            $data['refernce_type'] = $post['onaccount'];
        } elseif ($post['refrence_by'] == 'on_account') {
            $data['refernce_type'] = $post['onaccount'];
        } else {
            $data['refernce_type'] = $post['refrence_by'];
        }


        $payment_id = $this->payment_management->insert($data, 'payments');


        if ($post['payment_method'] == 'bank') {
            $payment_data['branch_id'] = $user->branchid;
            $payment_data['payment_by'] = 'invoice';
            $payment_data['payment_type'] = 'debit';
            $payment_data['value'] = $post['payment_amount'];
            $payment_data['amount_type'] = 'sales';
            $payment_data['amount_type'] = $post['payment_type'];
            $payment_data['account_id'] = $post['account_id'];
            $payment_data['notes'] = $post['notes'];
            $payment_id = $this->payment_management->insert($payment_data, 'an_company_payment');
        }
        if (!empty($jobcharges) && $post['refrence_by'] == 'against_reference') {
            //echo "<pre>";
            //print_r($post);
            $total = count($jobcharges['invoice_id']);
            for ($a = 0; $a < $total; $a++) {

                $inv_pay['invoice_id'] = $jobcharges['invoice_id'][$a];
                $inv_pay['payment_id'] = $payment_id;
                $inv_pay['payment_amount'] = $post['payment_amount'];
                $this->payment_management->insert($job_data, 'invoice_payments');
                $job_data['invoice_id'] = $jobcharges['invoice_id'][$a];
                $job_data['payment_id'] = $payment_id;


                $chargesData['payment_amount'] = $post['payment_amount'];
                $job_data['invoice_paid'] = $jobcharges['invoice_paid'][$a];
                $job_data['total_amount'] = $jobcharges['total_amount'][$a];
                $paid_amount = $job_data['invoice_paid'] + $post['payment_amount'];
                if ($job_data['total_amount'] > $paid_amount) {
                    $invoice_main['status'] = 2;
                }

                if (isset($invoice_main)) {
                    $this->db->where('invoice_id', $job_data['invoice_id']);
                    $this->db->update('an_invoice', $invoice_main);
                }


                //			print_r($job_data);
                //	exit;

                /* if(isset($remaining)){
                  if($remaining >= $jobcharges['charges_amount'][$a]){

                  if($jobcharges['paymant_amount'][$a]){
                  $charges_rem = $jobcharges['charges_amount'][$a] - $jobcharges['paymant_amount'][$a];
                  $charges_rem  = $remaining - $charges_rem;
                  }

                  //echo "index".$a."amount".$charges_rem."if1";

                  }
                  else{
                  $remaining = $remaining;
                  $charges_rem = $remaining;
                  //echo "index".$a."amount".$charges_rem."else1";
                  }
                  }
                  else{

                  //echo "else";

                  if($jobcharges['paymant_amount'][$a]){
                  //echo "ifinner";
                  $charges_rem = $jobcharges['charges_amount'][$a] - $jobcharges['paymant_amount'][$a];
                  if($charges_rem>$total_payment){
                  $remaining  = $charges_rem -$total_payment;
                  $charges_rem = $total_payment;
                  }
                  else
                  $remaining  = $total_payment - $charges_rem;


                  //echo "index".$a."amount".$charges_rem."if2";
                  }
                  else{
                  //echo "elseinner";
                  $remaining = $total_payment - $jobcharges['charges_amount'][$a];
                  $charges_rem = $charges_amount;
                  //	echo "index".$a."amount".$charges_rem."else2";
                  //		exit;
                  }

                  } */
                //echo $remaining;
                //exit;
                if ($remaining >= 0) {

                    /* $job_data['job_id'] = $jobcharges['jobid'][$a];
                      $job_data['payment_amount'] = $charges_rem;
                      $job_data['customer_id'] = $post['customer_id'];
                      $job_data['payment_purpose'] = 'sales';
                      $job_data['payment_by'] = 'Job';
                      $job_data['parent_id'] = $payment_id;
                      $job_data['customer_id'] = $post['customer_id'];
                     */
                }
            }
        } elseif ($post['refrence_by'] == 'advance') {


            $job_data['job_id'] = $post['bl_number'];
            $job_data['payment_amount'] = $post['payment_amount'];
            $job_data['customer_id'] = $post['customer_id'];
            $job_data['payment_purpose'] = 'advance';
            $job_data['payment_by'] = 'Job';
            $job_data['parent_id'] = $payment_id;
            $job_data['customer_id'] = $post['customer_id'];
            $this->payment_management->insert($job_data, 'job_payment');

            $chargesData['payment_amount'] = $post['payment_amount'];
            $this->db->where('jb_c_id', $post['job_charges_id']);
            $this->db->update('jobs_charges', $chargesData);
        } else {
            //refrence_by
            $this->load->model('ajax/ajax_model', 'ajax');
            $jobs_data = $this->ajax->getCusmer_invoices($post['customer_id']);
            if (!empty($jobs_data)) {
                foreach ($jobs_data as $jb) {

                    //echo "<pre>";
                    //print_r($jb);
                    //$jb['sales_amount'];
                    //$jb['amount'];

                    if ($post['payment_amount'] > 0) {
                        if ($jb['sales_amount'] > $jb['amount']) {
                            $remaiining_payment = $jb['sales_amount'] - $jb['amount'];
                            //$remaiining_payment

                            if ($remaiining_payment >= $post['payment_amount'])
                                $post['payment_amount'] = $remaiining_payment - $post['payment_amount'];
                            else {
                                $post['payment_amount'] = $post['payment_amount'] - $remaiining_payment;
                            }


                            $inv_data['invoice_id'] = $jb['invoice_id'];
                            $inv_data['payment_id'] = $payment_id;
                            $this->payment_management->insert($inv_data, 'invoice_payments');
                        }
                    }


                    //$data['payment_amount'];
                    //	$data['payment_amount'];	
                }
            }
        }


        redirect(base_url() . 'payment_management/pay_reciept');
        exit();
    }

    function getCustomePayments() {

        $post = $this->input->post();
        //echo "<pre>";
        //print_r($post);

        $this->_data['payment_data'] = $this->payment_management->getCustomerPaymetns($post['customer_id']);
        echo json_encode($this->_data['payment_data']);
    }

    function view_payments($customerid = "") {

        //$this->_data
        if ($customerid != "") {

            $this->_data['payment_data'] = $this->payment_management->getCustomerPaymetns($customerid);
            $this->_data['invoice_data'] = $this->payment_management->getCusmer_invoices($customerid);
            ///print_r($this->_data['invoice_data']);
        }

        //exit;
        $this->load->view('view_payments', $this->_data);
    }

//----------------------------------------------------------------------
}

?>