<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Transaction_management_model extends CI_Model {
	
	/*
	* Properties
	*/

//----------------------------------------------------------------------
    
	/*
	* Constructor
	*/
	
	function __construct()
    {
        parent::__construct();
		
		//Load Table Names from Config
    }
	
	function insertNewtransaction($data='')
	{
		$this->db->insert('an_company_transaction',$data);
		 return $this->db->insert_id();
	}
	
	function udpateJobStatus($job_id='')
	{
		$data['job_status'] = 'close';
		$this->db->where('id', $job_id);
		$this->db->update('an_jobs',$data);
		return true;
	}
	
	
//----------------------------------------------------------------------	
}
?>