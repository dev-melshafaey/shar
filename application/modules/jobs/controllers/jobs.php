<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Jobs extends CI_Controller
{
	/*
	* Properties
	*/
    private $_data	=	array();
//----------------------------------------------------------------------
	/*
	* Constructor
	*/
   public function __construct()
   {
		parent::__construct();
		
		//load Home Model
	   $this->load->model('jobs_model','jobs');	 
	   $this->load->model('customers/customers_model','customers');	 
	   $this->load->model('users/users_model','users');	  
	   $this->_data['udata'] = userinfo_permission();
	   
   }
   
//----------------------------------------------------------------------
   
   //created by M.Ahmed
   public function loadjobsWithStatus()
   {
		$customer_id = $this->input->post('customer_id');
		$job_status = $this->input->post('job_status');
		$jobs = $this->jobs->loadjobsWithStatus($customer_id,$job_status);
		?>
		<select id="job_id" name="job_id" onchange="loadChargesWithStatus();">
			<option value="">Select Job Id</option>
			<?php
			foreach($jobs as $eachJob){
				?>
				<option value="<?php echo $eachJob->id; ?>"><?php echo $eachJob->id; ?></option>
				<?php
			}
			?>
		</select>
		<?php
   }
   
   //created by M.Ahmed
   public function loadDataTable()
   {
	   $this->_data['from_date_filter'] = $this->input->post('from_date_filter');
	   $this->_data['to_date_filter'] = $this->input->post('to_date_filter');
	   $this->_data['mythis'] = $this;
	   $this->load->view('loadDataTable.php',$this->_data);
   }
   
      //created by M.Ahmed
   public function loadCancelJobsDataTable()
   {
	   $this->_data['from_date_filter'] = $this->input->post('from_date_filter');
	   $this->_data['to_date_filter'] = $this->input->post('to_date_filter');
	   $this->load->view('loadCancelJobsDataTable.php',$this->_data);
   }
   
         //created by M.Ahmed
   public function loadExpireJobsDataTable()
   {
	   $this->_data['from_date_filter'] = $this->input->post('from_date_filter');
	   $this->_data['to_date_filter'] = $this->input->post('to_date_filter');
	   $this->load->view('loadExpireJobsDataTable.php',$this->_data);
   }
   
   
   public function checkMandatoryFile(){
	   $job_id = $this->input->post('job_id');
       return $this->jobs->checkMandatoryFile($job_id);
   }
  
   public function cancelJobWithId($job_id){
       $this->jobs->updateJobStatusToCancel($job_id);
	   //redirect(base_url().'jobs/options');
	   exit();
   }
    
   
   public function image_uploads_emails()
   {				
	    //echo "<pre>";
		//print_r($_FILES);
	
		//exit;
		
		$job_id = $this->input->post('job_id');
		$config['upload_path'] = './uploads/email/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '1000';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';
	
			$this->load->library('upload', $config);
	
			if ( ! $this->upload->do_upload('file'))
			{
				$error = array('error' => $this->upload->display_errors());
				echo json_encode(array('status' => $error));
			}
			else
			{
				$upload_data = $this->upload->data(); 
				
  				$file_name = $upload_data['file_name'];
				//$exts = split("[/\\.]", $file_name);
				//$n    = count($exts)-1;
				//$ext  = $exts[$n];
				
				$datas['email_attachment'] = $file_name;
				$datas['created_by'] = $this->session->userdata('userid');
				
				
				$this->jobs->updateImageIndbEmail($datas);
				echo json_encode(array('status' => 'ok'));
				//do_redirect('options?e=10');
			}
		
   }
   
   public function image_uploads()
   {
	    //echo "<pre>";
		//print_r($_FILES);
		
		//exit;
		
		$job_id = $this->input->post('jobId');
		$config['upload_path'] = './uploads/jobs_mandatory/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '1000';
			$config['max_width']  = '10240';
			$config['max_height']  = '7680';
	
			$this->load->library('upload', $config);
	
			if ( ! $this->upload->do_upload('file'))
			{
				$error = array('error' => $this->upload->display_errors());
				echo json_encode(array('status' => $error));
			}
			else
			{
				$upload_data = $this->upload->data(); 
				
  				$file_name = $upload_data['file_name'];
				//$exts = split("[/\\.]", $file_name);
				//$n    = count($exts)-1;
				//$ext  = $exts[$n];
				
				$datas['mandatory_attachment'] = $file_name;
				$datas['created'] = date('Y-m-d');
				
				$this->jobs->updateImageIndb($datas,$job_id );
				
				$this->jobs->updatedoc_recipt_date($datas['created'],$job_id);
				$this->jobs->updateJobStatusToClose($job_id);
				echo json_encode(array('status' => 'ok'));
				//do_redirect('options?e=10');
			}
		
   }
   
      public function attachmentsJobs()
   {
	    //echo "<pre>";
		//print_r($_FILES);
		
		//exit;
		
		$job_id = $this->input->post('jobId');
		$config['upload_path'] = './uploads/jobs/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '1000';
			$config['max_width']  = '10240';
			$config['max_height']  = '7680';
	
			$this->load->library('upload', $config);
	
			if ( ! $this->upload->do_upload('file'))
			{
				$error = array('error' => $this->upload->display_errors());
				echo json_encode(array('status' => $error));
			}
			else
			{
				$upload_data = $this->upload->data(); 
				
  				$file_name = $upload_data['file_name'];
				//$exts = split("[/\\.]", $file_name);
				//$n    = count($exts)-1;
				//$ext  = $exts[$n];
				
				$datas['attachment'] = $file_name;
				$datas['created'] = date('Y-m-d');
				
				$this->jobs->updateImageIndb($datas,$job_id );
				echo json_encode(array('status' => 'ok'));
				//do_redirect('options?e=10');
			}
		
   }
   
   //created by M.Ahmed
   public function loadCurrencyView()
   {
	   $this->load->view('converter.php');
   }
   
      //created by M.Ahmed
   public function loadCurrencyViewFreightCharges()
   {
	   $this->load->view('converterFreightCharges.php');
   }
   
   public function getJobTypevalue($jobId=''){
       $res = $this->jobs->getJobSalesAndProfit($jobId);
	   return $res;
   }
   
      //created by M.Ahmed
   public function loadChargesWithStatus()
   {
		$job_id = $this->input->post('job_id');
		$job_status = $this->input->post('job_status'); //open, waiting for customer
		//first check the type of job
		// 1 fixed, 2 contract, 3 cash
		if($job_status=='open'){
			$job_type = $this->jobs->getJobTypePaidorUnpaid($job_id,$job_status);
			if($job_type){
				?>
                <select id="charges_selection" name="charges_selection" onchange="assignValueForCharge()">
                	<option value="">Select Charges</option>
                <?php
				foreach($job_type as $each_charge){
					?>
                    <option vals="<?php echo $each_charge->total_charge;?>" value="<?php echo $each_charge->charge_id;?>"><?php echo $each_charge->charge_name ;?></option>
                    <?php /*?><label>Charge Name</label>
                    <input type="text" readonly="readonly" value="<?php echo $each_charge->charge_name?>" />
                    <label style="margin-left:10px">Per Charge</label>
                    <input style="width:80px;" type="text" readonly="readonly" value="<?php echo $each_charge->per_charge_amount;?>" />
                    <label style="margin-left:10px">Quaintity</label>
                    <input  style="width:80px;" type="text" readonly="readonly" value="<?php echo $each_charge->charge_quaintity;?>"/>
                    <label style="margin-left:10px">Total</label>
                    <input  style="width:80px;" type="text" readonly="readonly" value="<?php echo $each_charge->total_charge;?>"/><br /><?php */?>
                    <?php
				}
				?>
                </select>
                <?php
			}
		}else{
			$job_type_table = $this->jobs->getDutyChargeswithJobType($job_id,'7');
			//$job_type_table = $this->jobs->getDutyChargeswithJobTypeNotPaid($job_id,'7');
			//$table = $job_type_table->tablename;
			
			//$r = $this->jobs->getDutyCharge($job_id);
			?>
                        <select id="charges_selection" name="charges_selection" onchange="assignValueForCharge()">
                			<option value="">Select Charges</option>
                            <?php
			//$duty_charges = $r->duty_charges;
			if($job_type_table){
				//foreach($job_type as $each_charge){
					//if($each_charge->charge_id==4){
						?>
                        
                    		<option vals="<?php echo $job_type_table->total_charge;?>" value="<?php echo '7';?>"><?php echo 'Duty Charges' ;?></option>
                        
                        
                        
                       <?php /*?> <input type="text" readonly="readonly" value="<?php echo $each_charge->total_charge;?>" /><?php */?>
                        <?php
					//}
				//}
			}
			?>
            </select>
            <?php
		}
		
   }
   
 
   //created by M.Ahmed
   public function get_branches()
   {
		$branch_id = $this->input->post('branch_id');
		$branches = $this->jobs->get_all_brancheswith($branch_id);
		?>
		<select id="rec_branch_id" name="rec_branch_id" onchange="loadEmployees();">
			<option value="">Select Branch</option>
			<?php
			foreach($branches as $eachCus){
				
				?>
				<option value="<?php echo $eachCus->branch_id; ?>"><?php echo $eachCus->branch_name; ?></option>
				<?php
			}
			?>
		</select>
		<?php
   }
   
   
   //created by M.Ahmed
   public function get_customers_by_branch()
   {
		$branch_id = $this->input->post('branch_id');
		$customers = $this->jobs->get_all_customers($branch_id);
		?>
		<select id="customer_id" name="customer_id" onchange="selectedValueToNull();">
			<option value="">Select Customers</option>
			<?php
			foreach($customers as $eachCus){
				
				?>
				<option <?php echo (isset($job->customer_id) && $eachCus->id == $job->customer_id)?'selected="selected"':'';?> value="<?php echo $eachCus->id; ?>"><?php echo $eachCus->customer_name; ?></option>
				<?php
			}
			?>
		</select>
		<?php
   }
   
      
   //created by M.Ahmed
   public function get_emp_by_branch()
   {
		$branch_id = $this->input->post('branch_id');
		$employees = $this->jobs->get_all_employees($branch_id,'all');
		?>
		<select id="emp_id" name="emp_id" >
			<option value="">Select Employee</option>
			<?php
			foreach($employees as $eachEmp){
				
				?>
				<option value="<?php echo $eachEmp->userid; ?>"><?php echo $eachEmp->fullname; ?></option>
				<?php
			}
			?>
		</select>
		<?php
   }
   
   public function getTotalProfitAndLoss(){
       $sales = 0;
	   $pur = 0;
	   $profit = 0;
	   $rec = $this->jobs->getTotalProfitAndLoss();
	   foreach($rec as $eacheac){
	       	$sales = $sales+$eacheac->sales;
			$pur = $pur+$eacheac->purchase;
			$profit = $profit+$eacheac->profit;
	   }
	   $arr = array('profit' => $profit, 'sales' => $sales, 'pur' => $pur);
	   return $arr;
   }
   
   public function options($cotumerId = '')
   {
   	  // Load Home View
	   //$this->jobs->update_job($data,$job_id);
	   $this->_data['mythis'] = $this;
		$this->_data['customer_id'] = $cotumerId;
	   $this->load->view('jobs',	$this->_data);
   }
   
   public function pay($jobid = '')
   {
	   $this->_data['payment'] = $this->jobs->gePayemnts($jobid);
		$this->_data['remaing'] = $this->jobs->getRemainng($jobid);
	  // echo "<pre>";
	 //  print_r($this->_data['remaing'] );
	   //exit;
	   $this->load->view('add_payment',	$this->_data);
   }
   
   function add_payment(){
		//$_POST[''];
				unset($_POST['submit_cash']);
				$this->db->insert('job_payment',$_POST);
   					redirect(base_url().'jobs/?e=11');
   }
   
   public function expire_jobs()
   {
   	  // Load Home View
	   //$this->jobs->update_job($data,$job_id);
	   $this->load->view('jobs_expire',	$this->_data);
   }   
   
   public function cancel_jobs()
   {
   	  // Load Home View
	   //$this->jobs->update_job($data,$job_id);
	   $this->load->view('jobs_cancel',	$this->_data);
   }
   
 
//----------------------------------------------------------------------
	/*
	* Add jobs
	*/
   public function add_jobs($job_id	=	NULL)
   {       
	   
	   
	   $this->load->helper(array('form', 'url'));
	   
	   $this->_data['charges_all'] =$this->customers->get_all_charges_by_type($type = '');
	   
		if($this->input->post('sub_mit'))
		{
			$data =	$this->input->post();
			//echo "<pre>";
			//print_r($data);
			//exit;
			unset($data['sub_mit']);
			unset($data['contact_person_id']);
			$job_id = $this->input->post('id');
			// insert data into database
			if($job_id	!=	'')
			{
							
							
						//echo "<pre>";
						//print_r($data);
						//$jobs_charges = array();
						if(isset($data['products']) && !empty($data['products'])){
								//$jb_charges = $data['jb_c_id'];
								$jb_charges = $data['products'];
								//exit;
								//echo "<pre>";
								//print_r($data);
								//exit;
								$userid = $this->session->userdata('userid');	   
	    						$user = $this->users->get_user_detail($userid);
								//print_r($user);
								//echo "<pre>";
								//print_r($jb_charges);
							//	exit;
								 $total_chages = count($jb_charges);
								
								for($a=0;$a<$total_chages;$a++){
										
										//echo $a;
										if(!isset($data['products'][$a]['jb_c_id'])){
										//echo "if";
										$jobs_charges['charges_id'] 	=	$data['products'][$a]['charges_id'];
										$jobs_charges['charges_advance'] =	$data['products'][$a]['advance'];
										$jobs_charges['sales_value']      =	$data['products'][$a]['total'];
										$jobs_charges['purchase_value']   =	$data['products'][$a]['purchase_value'];
										$jobs_charges['comment']      	=	$data['products'][$a]['comment'];
										$jobs_charges['notes']   		  =		$data['products'][$a]['notes'];
										$jobs_charges['job_id']   		=	$job_id;		
										$jobs_charges['purchase_id']   =	$this->jobs->getSupplierIdByName($data['products'][$a]['supplier_name']);	
									}
										
											//print_r($jobs_charges);
										
										if(!isset($data['products'][$a]['jb_c_id'])){
											//echo "<pre>";
											//print_r($jobs_charges);
											//echo "else";
											//exit;
											if(isset($jobs_charges)){
												$this->db->insert('jobs_charges', $jobs_charges);
												$job_charge_id = $this->db->insert_id();
												$trucks_d = $data['trucks'];
												if($jobs_charges['charges_id'] == 10){
													if(!empty($trucks_d)){
													
													foreach($trucks_d as $i=>$truck){
														$truck_data['truck_id'] = $truck;
														$truck_data['job_id'] = $job_id;
														$truck_data['job_charge_id'] = $job_charge_id;
														$truck_data['container_number'] = $data['containers'][$i]; 
														$truck_data['purchase_amount'] = $jobs_charges['purchase_value'];
														$truck_data['sale_diesel_amount'] = $jobs_charges['sales_value'];
														$truck_data['user_id'] = $userid;
														
														$this->db->insert('jobs_trucks',$truck_data);
														$job_truck_id = $this->db->insert_id();
													//job_charge_id
													}
												}
												}


											}
										}
										
										unset($data['trucks']);
										unset($data['containers']);
										if(isset($payment) && $payment['advance']){
											$this->db->insert('job_payment',$payment);
											$payment_id = $this->db->insert_id();
											if(isset($payment['payment_type']) &&  $payment['payment_type'] == 'cash'){
												$cash['payment_id'] = $payment_id;
												$cash['branch_id'] = $user->branchid;
												$cash['transaction_type'] = 'credit';
												$cash['job_id'] = $job_id;
												$this->db->insert('an_cash_management',$cash);
											}
											elseif(isset($payment['payment_type']) &&  $payment['payment_type'] == 'accounts'){
												$account['payment_id'] = $payment_id;
												$account['branch_id'] = $data['products'][$a]['bankid'];
												$account['account_id'] = $data['products'][$a]['account'];
												$account['transaction_type'] = 'credit';
												$account['job_id'] = $job_id;
												$this->db->insert('an_company_transaction',$account);
											
											}
										
										}
										
								}
								
										$jobData['discount'] = $data['all_discount'];
										//$jobData['job_step_no'] = $data['job_step_no']+1;
										$this->db->where('id',$job_id);
										$this->db->update('an_jobs', $jobData);
									
							unset($data['payment_type']);
							
						
				}
				
					$this->jobs->update_job($data,$job_id);
									
				redirect(base_url().'jobs/add_jobs/'.$job_id.'?e=11');
				exit();
			}
			else
			{	
				if($data['job_status']=='waiting_for_customer'){
					$this->jobs->add_job($data);
				}else{
					$this->jobs->add_job($data);
				}
				
				
				exit();
			}
		}
		else
		{
			$this->_data['customers'] = $this->jobs->get_all_customers();
			
			$this->_data['employee'] = $this->jobs->get_all_employees();
			if($job_id	!=	''){
				$jbos = $this->jobs->get_job_detail($job_id);
			if(!isset($jbos->customer_id))
			{
				redirect(base_url().'jobs/options');
				exit();
			}
			$customerid = $jbos->customer_id;
			$this->_data['current_customers'] =$current_customers = $this->customers->get_customer_detail($customerid);
			//echo "<pre>";
			//print_r($current_customers);
			
				//echo $current_customers->account_type;
				if(isset($current_customers->account_type) && $current_customers->account_type == 'fixed')
				$this->_data['charges_customer'] = $this->customers->get_jobs_customers_rec($customerid,'an_fixed_job_charges','fixed',$job_id);
				if(isset($current_customers->account_type) && $current_customers->account_type == 'cash')
				$this->_data['charges_customer'] = $this->customers->get_jobs_customers_rec($customerid,'an_cash_job_charges','cash',$job_id);
				if(isset($current_customers->account_type) && $current_customers->account_type == 'contract')
				$this->_data['charges_customer'] = $this->customers->get_jobs_customers_rec($customerid,'an_contract_job_charges','contract',$job_id);
				$this->_data['job_containers'] = $this->jobs->getjob_containers($job_id);
				$this->_data['job'] = $this->jobs->get_job_detail($job_id);
				//echo "<pre>";
				//print_r($this->_data['job']);
				//exit;
				$way = $this->_data['job']->way_of_shipment;
				if($way == 'air' ){
					$type = 'air_port';
				}
				if($way == 'sea' ){
					$type = 'sea_port';
				}
				if($way == 'land' ){
					$type = 'land_port';
				}
			
				$this->_data['charges_all'] =$this->customers->get_all_charges_by_type($type);
				$this->_data['jobs_charges'] = $this->jobs->getChargesData($job_id);
				$this->_data['jobs_payments'] = $this->jobs->getPaymentsData($job_id);
				
				
			//	print_r($this->_data['charges_all']);
			}else{
   				$this->_data['job_id']	=	'';
			}
			$this->_data['mine'] = $this;
		 	$this->load->view('add-job-form',	$this->_data);
		}
		
		
   }
   function get_charges_id(){
	   	//echo "<pre>";
		//print_r($_POST);
		echo $this->jobs->getChargesById($_POST['charges_id']);  
	}
   public function add_jobs_ajax($job_id	=	NULL)
   {
	   //print_r($_POST);
	   //exit;
	   $this->load->helper(array('form', 'url'));
		if($this->input->post())
		{
			$data =	$this->input->post();
			unset($data['sub_mit']);
			unset($data['contact_person_id']);
			$job_id = $this->input->post('id');
			// insert data into database
			if($job_id	!=	'')
			{
				
								
				$this->jobs->update_job($data,$job_id);
				$customers=$this->_data['customers'] = $this->jobs->get_all_customers();
				
				$employee = $this->_data['employee'] = $this->jobs->get_all_employees();
				
				if($job_id	!=	'')
				{
					$jbos = $this->jobs->get_job_detail($job_id);
					if(!isset($jbos->customer_id))
					{
						redirect(base_url().'jobs/options');
						exit();
					}
					$customerid = $jbos->customer_id;
					$this->_data['current_customers'] =$current_customers = $this->customers->get_customer_detail($customerid);
					//echo "<pre>";
					//print_r($current_customers);
					
						if(isset($current_customers->account_type) && $current_customers->account_type == 'fixed')
						$charges_customer = $this->_data['charges_customer'] = $this->customers->get_jobs_customers_rec($customerid,'an_fixed_job_charges','fixed',$job_id);
						if(isset($current_customers->account_type) && $current_customers->account_type == 'cash')
						$charges_customer = $this->_data['charges_customer'] = $this->customers->get_jobs_customers_rec($customerid,'an_cash_job_charges','cash',$job_id);
						if(isset($current_customers->account_type) && $current_customers->account_type == 'contract')
						$charges_customer = $this->_data['charges_customer'] = $this->customers->get_jobs_customers_rec($customerid,'an_contract_job_charges','contract',$job_id);
						
						$job = $this->_data['job'] = $this->jobs->get_job_detail($job_id);
				}else{
					$job_id = $this->_data['job_id']	=	'';
				}
				$mine = $this->_data['mine'] = $this;
				//load view start
				if($job->job_step_no=='' || $job->job_step_no=='1'){
				?>
				
					<input type="hidden" name="id" id="id" value="<?php echo (isset($job->job_id))?$job->job_id:''; ?>" />
					<input type="hidden" name="job_step_no" id="job_step_no" value="2" />
	
					<div class="raw form-group">
	
							<div class="form_title">Type of Shipment</div>
	
							<div class="form_field">
	
							<div class="dropmenu">
	
									<div class="styled-select">
	
								<?php get_statusdropdown(isset($job->type_of_shipment)?$job->type_of_shipment:'','type_of_shipment','type_of_shipment',' onchange="setFreightCharges();" '); ?>  
	
								</div>
	
								</div>
	
							</div>
	
						</div>
	
					<div class="raw form-group">
	
							<div class="form_title">Way of shipment</div>
	
							<div class="form_field">
	
							<div class="dropmenu">
	
									<div class="styled-select">
	
							  <?php get_statusdropdown(isset($job->way_of_shipment)?$job->way_of_shipment:'','way_of_shipment','way_of_shipment','','onchange="getChargesforOneShipment()"');?>
	
							  </div>
	
							  </div>
	
							</div>
	
						</div>                    
	
					<div class="raw form-group">
					
						<div class="form_title">Customer Name</div>
					
						<div class="form_field">
					
							<div class="dropmenu">
					
								<div class="styled-select">
					
									<select id="customer_id" name="customer_id" <?php echo isset($job->customer_id)?'disabled="disabled"':'';?>>
					
									<option value="">Select Customer</option>
					
									
					
									<?php
					
									foreach($customers as $eachCus){
					
										
					
										?>
					
										<option <?php echo (isset($job->customer_id) && $eachCus->id == $job->customer_id)?'selected="selected"':'';?> value="<?php echo $eachCus->id; ?>"><?php echo $eachCus->customer_name; ?></option>
					
										<?php
					
									}
					
									?>
					
									</select>
					
								</div>
					
							</div>
					
						</div>
					
					</div>                    
	
					<div class="raw form-group">
					
						<div class="form_title">Shipment Standards</div>
					
						<div class="form_field">
					
						<div class="dropmenu">
					
								<div class="styled-select">
					
						  <?php get_statusdropdown(isset($job->shipment_standard)?$job->shipment_standard:'','shipment_standard','shipment_standard',isset($job->shipment_standard)?'disabled="disabled"':'onchange="setFreightCharges();"');?>
					
						  </div>
					
						  </div>
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="div_freight_charges" style="display:<?php echo (isset($job->shipment_standard) && $job->shipment_standard=='FOB')?'':'none'; ?>">
					
						<div class="form_title">Freight Charges</div>
					
						<div class="form_field">
					
						  <input name="freight_charges" id="freight_charges" value="<?php echo isset($job->freight_charges)?$job->freight_charges:'0'; ?>" <?php echo isset($job->freight_charges)?'readonly="readonly"':''; ?>  type="text"  class="formtxtfield"/><label style="float:left; margin-right:21px">O.R.</label>
					
						  <input style="display:<?php echo isset($job->freight_charges)?'none':''; ?>" type="button" value="Currency Converter" id="currency_btn" onclick="showPopUpForFreightCharges();" />
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="">
					
						<div class="form_title">Document Type</div>
					
						<div class="form_field">
					
							<div class="dropmenu">
					
								<div class="styled-select">
					
									<?php get_statusdropdown('duplicate_document','document_charges','document_charges','', ' onchange="setFreightCharges();" '); ?>
					
								</div>
					
							</div>
					
						</div>
					
					</div>
					
					<div id="dynamicDiv" style="display:none">
					
					<div class="currency-wrapper">
					
						<div style="position:relative">
					
							<div style="position:absolute; right:-16px; top:-16px;">
					
								<a href="#_" onclick="hidePopUP();"><img src="<?php echo base_url();?>images/close.png" /></a>
					
							</div>
					
						</div>
					
						<!-- Start of content -->
					
						
					
						
					
						<!-- End of content -->
					
						
					
					</div><!-- End of currency-wrapper -->
					
					
					
					</div>
					
					<div class="raw form-group">
					
						<div class="form_title">Total Cost of Goods</div>
					
						<div class="form_field">
					
						  <input name="total_cost_of_goods" id="total_cost_of_goods" value="<?php echo isset($job->total_cost_of_goods)?$job->total_cost_of_goods:''; ?>" type="text"  class="formtxtfield"/><label style="float:left; margin-right:21px">O.R.</label>
					
						  <input style="display:<?php echo isset($job->total_cost_of_goods)?'none':''; ?>" type="button" value="Currency Converter" id="currency_btn" onclick="showPopUp();" />
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="div_insurance">
					
						<div class="form_title">Insurance</div>
					
						<div class="form_field">
					
						  <input readonly="readonly" name="insurance" id="insurance" value="<?php echo isset($job->insurance)?$job->insurance:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="div_doc_charges">
					
						<div class="form_title">Regional fees</div>
					
						<div class="form_field">
					
						  <label id="lbl_doc">20</label>
					
						</div>
					
					</div>                
					
					<div class="raw form-group" id="div_bl" style="display:<?php echo (isset($job->way_of_shipment)&&$job->way_of_shipment=='sea')?'':'none';?>">
					
						<div class="form_title">display:<?php echo (isset($job->way_of_shipment)&&$job->way_of_shipment=='sea')?'Regnacy fees':'Booking Number';?></div>
					
						<div class="form_field">
					
							<input name="bl_number" id="bl_number" value="<?php echo isset($job->bl_number)?$job->bl_number:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="div_air" style="display:none;">
					
						<div class="form_title">Air Way number</div>
					
						<div class="form_field">
					
							<input name="air_way_number" id="air_way_number" value="<?php echo isset($job->air_way_number)?$job->air_way_number:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
					
					<div class="raw form-group">
					
						<div class="form_title">Regency fees</div>
					
						<div class="form_field">
					
						  <input  readonly="readonly" name="duty_charges" id="duty_charges" value="<?php echo isset($job->duty_charges)?$job->duty_charges:''; ?>" type="text"  class="formtxtfield"/>
					
						  <div class="form_title" style="line-height:0.7px; width:146px;">Additional charges</div>
					
						  <input name="additional_charges" id="additional_charges" value="<?php echo isset($job->additional_charges)?$job->additional_charges:'0'; ?>" type="text"  class="formtxtfield"/>
					
						  
					
						  <input onclick="sendEmail()" type="button" value="Send Email" id="email_btn"  style="margin-left:21px;display:none<?php //echo (isset($job->email_send)&&($job->email_send==1))?'none':'' ?>"  />
					
						  
					
						  <span id="spanemail_send"><input type="hidden" name="email_send" id="email_send" value="0" /></span>
					
						</div>
					
					</div>
					
					<div class="raw form-group">
					
						<div class="form_title">Total Value</div>
					
						<div class="form_field">
					
						  <input  readonly="readonly" name="total_value" id="total_value" value="<?php echo isset($job->total_value)?$job->total_value:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="div_deninity_charge">
					
						<div class="form_title">Definity Charges</div>
					
						<div class="form_field">
					
						  <input  readonly="readonly" name="definity_charges" id="definity_charges" value="<?php echo isset($job->definity_charges)?$job->definity_charges:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="div_unique_charges">
					
						<div class="form_title">Unique Charges</div>
					
						<div class="form_field">
					
						  <input  readonly="readonly" name="unique_charges" id="unique_charges" value="<?php echo isset($job->unique_charges)?$job->unique_charges:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
	
					<div class="raw form-group" id="div_insured_charges">
					
						<div class="form_title">Insured charges</div>
					
						<div class="form_field">
					
						  <input  readonly="readonly" name="insured_charges" id="insured_charges" value="<?php echo isset($job->insured_charges)?$job->insured_charges:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
					
					<div class="raw form-group">
					
						<div class="form_title">Email Review</div>
					
						<div class="form_field">
					
						  
					
						  <textarea  style="height:auto" rows="4" cols="50" name="review" id="review" class="formtxtfield"><?php echo isset($job->review)?$job->review:'';?></textarea>
					
						</div>
					
					</div>
					
					<div class="raw">
					
						<div class="form_title">Review Status</div>
					
						<div class="form_field">
					
							<div class="dropmenu">
					
								<div class="styled-select">
					
									<?php
					
									if(isset($job->review_status) && ($job->review_status=='waiting_for_customer')){
					
										$review_status = "waiting_for_customer";
					
									}
					
									
					
									?>
					
									<?php get_statusdropdown(isset($job->review_status)?$job->review_status:'waiting_for_customer','review_status','review_status',''); ?>
					
								</div>
					
							</div>
					
						</div>
					
					</div>
					
					<div class="raw" align="center">
					
					<?php
					
					$btn_text = isset($job->id)?"Update":"Add";
					
					?>
					
						<input name="sub_mit" id="sub_mit" type="submit" class="submit_btn" value="<?php echo $btn_text;?>" />
					
						
					
					</div>
					   
				<?php
				} else if($job->job_step_no=='2'){
				?>
				
					<div class="raw form-group" style="display:none;">
					
					<div class="form_title">Job Type</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select">
					
					<?php get_statusdropdown(isset($job->job_type)?$job->job_type:'','job_type','job_type',''); ?>
					
					</div>
					
					</div>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title" id="div_shipper_name">Shipper Name</div>
					
					<div class="form_field">
					
					<input name="consignee_name" id="consignee_name" value="<?php echo isset($job->consignee_name)?$job->consignee_name:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none">
					
					<div class="form_title">Container type</div>
					
					<div class="form_field">
					
					<input name="container_type" id="container_type" value="<?php echo isset($job->container_type)?$job->container_type:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group" id="div_ctn" style="display:<?php echo (isset($job->way_of_shipment)&&$job->way_of_shipment=='sea')?'':'none';?>">
					
					<div class="form_title">Container number</div>
					
					<div class="form_field" id="div_container">
					
					<input id="countmeContainer" name="count_container" type="text"  class="formtxtfield"/>
					
					<input type="button" id="btn_container" onclick="AddCtn();" value="Add" />
					
					</div>
					
					
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title"></div>
					
					<div class="form_field" id="containertablediv" style="display:none;">
					
					
					
					</div>
					
					
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Gross weight</div>
					
					<div class="form_field">
					
					<input name="gross_weight" id="gross_weight" value="<?php echo isset($job->gross_weight)?$job->gross_weight:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Quantity</div>
					
					<div class="form_field">
					
					<input name="quaintity" id="quaintity" value="<?php echo isset($job->quaintity)?$job->quaintity:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none;">
					
					<div class="form_title">Regency fees Recieve Date</div>
					
					<div class="form_field">
					
					<input name="receive_duty_charges" id="receive_duty_charges" value="<?php echo isset($job->receive_duty_charges)?$job->receive_duty_charges:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none;">
					
					<div class="form_title">Document recipt date</div>
					
					<div class="form_field">
					
					<input name="doc_recipt_date" id="doc_recipt_date" value="<?php echo isset($job->doc_recipt_date)?$job->doc_recipt_date:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Pickup Location</div>
					
					<div class="form_field">
					
					<input name="pickup_location" id="pickup_location" value="<?php echo isset($job->pickup_location)?$job->pickup_location:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Delivery Location</div>
					
					<div class="form_field">
					
					<input name="delivery_location" id="delivery_location" value="<?php echo isset($job->delivery_location)?$job->delivery_location:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none;">
					
					<div class="form_title">Region </div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select">
					
					<?php
					
					$region_id = isset($job->region_id)?$job->region_id:'';
					
					$language = 'english';
					
					$name = 'wilayat_id';
					
					$value = isset($job->wilayat_id)?$job->wilayat_id:'';
					
					?>
					
					<?php region_dropbox('region_id',isset($job->region_id)?$job->region_id:'','english','onchange="getWilayat_id()"'); ?>  
					
					</div>
					
					</div>  
					
					</div>
					
					</div>
					
					<div class="raw form-group" id="wilayat_id_main_div" style="display:none<?php //echo isset($job->wilayat_id)?'block':'none'?>">
					
					<div class="form_title">Wilayat</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select" id="wilayat_id_div">
					
					<?php wilayat_dropbox('wilayat_id',isset($job->wilayat_id)?$job->wilayat_id:'','english'); ?>  
					
					</div>
					
					</div>  
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none">
					
					<div class="form_title">Destination</div>
					
					<div class="form_field">
					
					<input name="destination" id="destination" value="<?php echo isset($job->destination)?$job->destination:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none">
					
					<div class="form_title">Area</div>
					
					<div class="form_field">
					
					<input name="area" id="area" value="<?php echo isset($job->area)?$job->area:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Select Branch</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select">
					
					<?php company_branch_dropbox('branch_id',isset($job->branch_id)?$job->branch_id:'','onchange=getBranchEmployees()');?>
					
					</div>
					
					</div>
					
					</div>
					
					</div>
					
					<div class="raw form-group" id="emp_id_main_div" style="display:none;">
					
					<div class="form_title">Driver Name</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select" id="emp_id_div">
					
					<input name="driver_id" id="driver_id" value="<?php echo isset($job->driver_id)?$job->driver_id:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Estimated time depart</div>
					
					<div class="form_field">
					
					<input name="estimated_time_depart" id="estimated_time_depart" value="<?php echo isset($job->estimated_time_depart)?$job->estimated_time_depart:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Arrival time</div>
					
					<div class="form_field">
					
					<input name="arrival_time" id="arrival_time" value="<?php echo isset($job->arrival_time)?$job->arrival_time:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Remarks</div>
					
					<div class="form_field">
					
					<textarea style="height:auto"   rows="4" cols="50" id="remarks" name="remarks"><?php echo isset($job->remarks)?$job->remarks:''; ?></textarea>
					
					</div>
					
					</div>  
					
					<div class="raw form-group">
					
					<div class="form_title">bill of entry</div>
					
					<div class="form_field">
					
					<input name="bill_of_calling_no" id="bill_of_calling_no" value="<?php echo isset($job->bill_of_calling_no)?$job->bill_of_calling_no:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" id="div_mrn_no" style="display:none">
					
					<div class="form_title">mrn no</div>
					
					<div class="form_field">
					
					<input name="mrn_number" id="mrn_number" value="<?php echo isset($job->mrn_number)?$job->mrn_number:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" id="div_delivery_order_no" style="display:none">
					
					<div class="form_title">delivery order no</div>
					
					<div class="form_field">
					
					<input name="delivery_order_no" id="delivery_order_no" value="<?php echo isset($job->delivery_order_no)?$job->delivery_order_no:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" id="div_booking_no" style="display:none">
					
					<div class="form_title">booking no</div>
					
					<div class="form_field">
					
					<input name="booking_no" id="booking_no" value="<?php echo isset($job->booking_no)?$job->booking_no:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" id="div_commercial_reg_no" style="display:none">
					
					<div class="form_title">commercial registration no.</div>
					
					<div class="form_field">
					
					<input name="commercial_reg_no" id="commercial_reg_no" value="<?php echo isset($job->commercial_reg_no)?$job->commercial_reg_no:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" id="div_delivery_order_no" style="display:none;">
					
					<div class="form_title">delivery order no.</div>
					
					<div class="form_field">
					
					<input name="delivery_order_no" id="delivery_order_no" value="<?php echo isset($job->delivery_order_no)?$job->delivery_order_no:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" style="display:none;">
					
					<div class="form_title">Capt.</div>
					
					<div class="form_field">
					
					<input name="capt" id="capt" value="<?php echo isset($job->capt)?$job->capt:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group">
					
					<div class="form_title">Carriers name.</div>
					
					<div class="form_field">
					
					<input name="carriers_name" id="carriers_name" value="<?php echo isset($job->carriers_name)?$job->carriers_name:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" id="div_voyage_flight">
					
					<div class="form_title" id="voyage_flight_div">Voyage / flight no.</div>
					
					<div class="form_field">
					
					<input name="voyage_flight_no" id="voyage_flight_no" value="<?php echo isset($job->voyage_flight_no)?$job->voyage_flight_no:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group">
					
					<div class="form_title">Port of loading.</div>
					
					<div class="form_field">
					
					<input name="port_of_loading" id="port_of_loading" value="<?php echo isset($job->port_of_loading)?$job->port_of_loading:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group">
					
					<div class="form_title">Port of Discharge.</div>
					
					<div class="form_field">
					
					<input name="loading_place" id="loading_place" value="<?php echo isset($job->loading_place)?$job->loading_place:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group">
					
					<div class="form_title">offloading place.</div>
					
					<div class="form_field">
					
					<input name="offloading_place" id="offloading_place" value="<?php echo isset($job->offloading_place)?$job->offloading_place:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
	
				<?php
				} else if($job->job_step_no=='3'){
				?>
	
					<?php
					
					//if(isset($job->job_id) && $job->job_id!=''){
					
					?>
					
					
					
					<?php /*?>                <div id="div_status_waiting" style="display:<?php if(isset($job->job_status) && $job->job_status=='waiting_for_customer') echo 'none'; else if(!isset($job->job_status)) echo 'none'; else echo 'block';?>"><?php */?>
					
					<?php
					//echo '<pre>';
					//print_r($charges_customer);
					//exit;
					if(isset($charges_customer) && $charges_customer!='')
					{
					
					?>
					
					<div class="raw">
					
					<div class="form_title"></div>
					
					<div class="form_field">
					
					<div class="form_field"> 
					
					<?php
					
					//print_r($current_customers);
					
					$i=1;
					
					if(isset($current_customers->account_type) && ($current_customers->account_type == 'cash' || $current_customers->account_type == 'fixed')){
					
					?>
					
					<select id="chargeSelectorfixed" onchange="getvalueofcharge();">
					<option value="">Select Charges</option>
					<?php    
					foreach($charges_customer as $result){
						if($result['charge_id']=='7'){continue;}
						?>
						<option value="<?php echo $result['charge_id'];?>"><?php echo $result['charge_name'];?></option>
						<?php
					}
						?>
						</select>
						
					<?php    
					foreach($charges_customer as $result){
					
						if($result['total_charge']==0){
					
							//continue;
					
						}
					
						$readonly = (isset($result['is_company_paid']) && $result['is_company_paid']!="0")?' disabled="disabled"':'';
					
						?>
					
						<?php
					
						if($result['charge_id']=='7'){
					
							?>
					
							<?php /*?><div style="height:<?php echo ($readonly!='')?'87px':'96px'; ?>;clear:left;" id="div_<?php echo $result['charge_id'];?>">
					
					<label style="visibility:hidden;width:200px;float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input  <?php echo $readonly;?> placeholder="Enter per charge."  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>"  onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  value="<?php echo $result['per_charge_amount']?>" style="visibility:hidden;width:100px" /> &nbsp;&nbsp;
					
					<label style="visibility:hidden;width:200px;float:left">No. of  <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter no." onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  class="formtxtfield" type="text" name="txtno_<?php echo $result['charge_id'];?>" id="txtno_<?php echo $result['charge_id'];?>" value="<?php echo $result['charge_quaintity']?>" style="visibility:hidden;width:100px" /> &nbsp;&nbsp;
					
					<label style="width:200px;float:left">Total <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="total value"  class="formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" value="<?php echo $result['total_charge']?>" style="width:100px" /> 
					
					<span style="margin-top:15px;display:<?php echo ($readonly!='')?'none':'';?>">
					
					<!-- <br /><br />-->
					
					<label style="width:200px;float:left">Select Payment Way &nbsp;&nbsp;</label>
					
					<div class="dropmenu" >
					
					<div class="styled-select">
					
					<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
					
					</div>
					
					</div>
					
					
					
					<span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
					
					<label style="width:200px;float:left">Select Account &nbsp;&nbsp;</label>
					
					<div class="dropmenu">
					
					<div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
					
					
					
					</div>
					
					</div>
					
					</span>
					
					</span>
					
					<?php /*?><a class="del_txt" id="anc_<?php echo $result['charge_id'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php * /?></div><?php */?>
					
							<?php
					
							continue;
					
						}
					
						?>
					
						
						<div style="display:none;" id="divs_<?php echo $result['charge_id'];?>">
						<div  style="height:<?php echo ($readonly!='')?'87px':'96px'; ?>;clear:left;" id="div_<?php echo $result['charge_id'];?>">
					
					<label style="float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input  <?php echo $readonly;?> placeholder="Enter per charge."  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>"  onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  value="<?php echo $result['per_charge_amount']?>" style="width:5%" /> &nbsp;&nbsp;
					
					<label style="float:left">No. of  <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter no." onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  class="formtxtfield" type="text" name="txtno_<?php echo $result['charge_id'];?>" id="txtno_<?php echo $result['charge_id'];?>" value="<?php echo $result['charge_quaintity']?>" style="width:5%" /> &nbsp;&nbsp;
					
					<label style="float:left">Total Purchase Charges&nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="total purchase"  onchange="gettotalsof();" class="totalpur formtxtfield" type="text" name="txtpur_<?php echo $result['charge_id'];?>" id="txtpur_<?php echo $result['charge_id'];?>" value="<?php echo $result['pur_charge']?>" style="width:5%" /> 
					
					
					
					<label style="float:left">Total Sales Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="total value"  class="totalsale formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" value="<?php echo $result['total_charge']?>"  onchange="gettotalsof();" style="width:5%" /> 
					
					
					
					<span style="margin-top:15px;display:<?php echo ($readonly!='')?'none':'';?>">
					
					<!-- <br /><br />-->
					
					<label style="float:left">Select Payment Way &nbsp;&nbsp;</label>
					
					<div class="dropmenu" >
					
					<div class="styled-select">
					
					<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
					
					</div>
					
					</div>
					
					
					
					<span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
					
					<label style="float:left">Select Account &nbsp;&nbsp;</label>
					
					<div class="dropmenu">
					
					<div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
					
					
					
					</div>
					
					</div>
					
					</span>
					
					</span>
					
					<?php /*?><a class="del_txt" id="anc_<?php echo $result['charge_id'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php */?></div>
						</div>
						
					
						<?php /*?><div style="height:38px;clear:left;" id="div_<?php echo $result['charge_id'];?>"><label style="width:200px;float:left"><?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label><input placeholder="Enter charge."  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" disabled="disabled" value="<?php echo $result['per_charge_amount']?>" />  <?php /*?><a class="del_txt" id="anc_<?php echo $result['charge_id'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php *?></div><?php */?>
					
						<?php
					
						$i++;
					
					}
					
					?>
					<div id="fixed_append"></div>
					<?php
					
					}else if(isset($current_customers->account_type) && $current_customers->account_type == 'contract'){
					
					?>
					<select id="chargeSelectorcontract" onchange="getvalueofchargecontract();">
					<option value="">Select Charges</option>
					<?php    
					foreach($charges_customer as $result){
						if($result['charge_id']=='7'){continue;}
						?>
						<option value="<?php echo $result['charge_id'];?>"><?php echo $result['charge_name'];?></option>
						<?php
					}
						?>
						</select>
						
					
					<?php
					foreach($charges_customer as $result){
					
						
					
						if($result['total_charge']==0){
					
							//continue;
					
						}
					
						//$readonly = (isset($result['is_company_paid']) && $result['is_company_paid']!="0")?' readonly="readonly"':'';
						$readonly = (isset($result['is_company_paid']) && $result['is_company_paid']!="0")?' disabled="disabled"':'';
					
						
					
						 //$valus1 = $this->customers->get_contract_customer_values($customer->id,$result['charge_id']);
					
						 $charge_quaintity = $result['charge_quaintity'];
					
						 $per_charge_amount = $result['per_charge_amount']
					
						
					
						?>
					
						 <?php
					
						if($result['charge_id']=='7'){
					
							?>
					
							<?php /*?><div style="height:38px;" id="div_<?php echo $result['charge_id'];?>">
					
					<label style="visibility:hidden;width:200px;float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> type="text"  class="formtxtfield" placeholder="Enter per expense charges." name="txtpercharge_<?php echo $result['charge_id'];?>" id="txtpercharge_<?php echo $result['charge_id'];?>" value="<?php echo $per_charge_amount;?>" style="visibility:hidden;margin-left:10px; width:5%;" />&nbsp;&nbsp;
					
					
					
					<label style="visibility:hidden;width:200px;float:left">No. of <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter quantity"  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" style="visibility:hidden;width:5%"  value="<?php echo $charge_quaintity;?>" />&nbsp;&nbsp; &nbsp;&nbsp;
					
					<label style="width:200px;float:left">Total <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter quantity"  class="formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" style="width:5%"  value="<?php echo ($charge_quaintity*$per_charge_amount);?>" />
					
					
					
					<span style="margin-top:15px;display:<?php echo ($readonly!='')?'none':'';?>">
					
					<!-- <br /><br />-->
					
					<label style="width:200px;float:left">Select Payment Way &nbsp;&nbsp;</label>
					
					<div class="dropmenu" >
					
					<div class="styled-select">
					
					<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
					
					</div>
					
					</div>
					
					
					
					<span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
					
					<label style="width:200px;float:left">Select Account &nbsp;&nbsp;</label>
					
					<div class="dropmenu">
					
					<div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
					
					
					
					</div>
					
					</div>
					
					</span>
					
					</span>
					
					
					
					&nbsp;&nbsp; &nbsp;&nbsp;
					
					</div><?php */?>
					
							<?php
					
							continue;
					
						}
					
						?>
					<div style="display:none" id="divs_<?php echo $result['charge_id'];?>">
						<div style="height:38px;" id="div_<?php echo $result['charge_id'];?>">
					
					<label style="float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> type="text"  class="formtxtfield" placeholder="Enter per expense charges." name="txtpercharge_<?php echo $result['charge_id'];?>" id="txtpercharge_<?php echo $result['charge_id'];?>" value="<?php echo $per_charge_amount;?>" style="margin-left:10px; width:5%;" />&nbsp;&nbsp;
					
					
					
					<label style="float:left">No. of <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter quantity"  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" style="width:5%"  value="<?php echo $charge_quaintity;?>" />&nbsp;&nbsp; &nbsp;&nbsp;
					
					
					
					<label style="float:left">Total Purchase Charges&nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter purchase" onchange="gettotalsof();"  class="totalpur formtxtfield" type="text" name="txtpur_<?php echo $result['charge_id'];?>" id="txtpur_<?php echo $result['charge_id'];?>" style="width:5%"  value="<?php echo $result['pur_charge'];?>" />
					
					
					
					
					
					<label style="float:left">Total Sales Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter Sales"  onchange="gettotalsof();" class="totalsale formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" style="width:5%"  value="<?php echo ($charge_quaintity*$per_charge_amount);?>" />
					
					
					
					
					
					<span style="margin-top:15px;display:<?php echo ($readonly!='')?'none':'';?>">
					
					<!-- <br /><br />-->
					
					<label style="float:left">Select Payment Way &nbsp;&nbsp;</label>
					
					<div class="dropmenu" >
					
					<div class="styled-select">
					
					<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
					
					</div>
					
					</div>
					
					
					
					<span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
					
					<label style="float:left">Select Account &nbsp;&nbsp;</label>
					
					<div class="dropmenu">
					
					<div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
					
					
					
					</div>
					
					</div>
					
					</span>
					
					</span>
					
					
					
					&nbsp;&nbsp; &nbsp;&nbsp;
					
					<?php /*?><a class="del_txt" id="anc_<?php echo $result['per_charge_amount'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php */?></div>
					</div>
						
					
						
					
						<?php /*?><div style="height:38px;" id="div_<?php echo $result['charge_id'];?>"><label style="width:200px;float:left"><?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label><input placeholder="Enter quantity"  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" disabled="disabled" value="<?php echo $charge_quaintity;?>" />&nbsp;&nbsp; &nbsp;&nbsp;<input type="text"  class="formtxtfield" placeholder="Enter per expense charges." disabled="disabled" name="txtpercharge_<?php echo $result['charge_id'];?>" id="txtpercharge_<?php echo $result['charge_id'];?>" value="<?php echo $per_charge_amount;?>" style="margin-left:10px;" />&nbsp;&nbsp;<?php /*?><a class="del_txt" id="anc_<?php echo $result['per_charge_amount'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php *?></div><?php */?>
					
						<?php
					
						$i++;
					
					}
					
					?>
					<div id="contract_append"></div>
					<?php							
					
					}
					
					?>
					
					</div>
					
					</div>
					
					</div>
					
					<?php
					
					?>
					
					<input <?php //echo (isset($readonly)?'disabled="disabled"':'')?>  type="hidden" name="count_txt" id="count_txt" value="<?php echo  $result['charge_id'];?>" />
					
					<br />
					
					<label id="lbl_pur" style='float:left'>Purchase = 0 &nbsp;&nbsp;</label>  
					
					<label id="lbl_sales" style='float:left'>Sales = 0 &nbsp;&nbsp;</label> 
					
					<label id="lbl_profit" style='float:left'>Profit = 0 OMR &nbsp;&nbsp;</label>
					
					<?php
					
					}
					else 
					{
					$newcustomerid = isset($job->customer_id)?$job->customer_id:'';
					$newport = isset($job->way_of_shipment)?$job->way_of_shipment:'';
					$newduty_charge = isset($job->duty_charges)?$job->duty_charges:'';
					
					?>
					<div id="<?php echo (isset($charges_customer) && $charges_customer!='')?'div_re':'div_res'?>" class="raw">
					
					<div class="form_title"></div>
					
					<div class="form_field">
					
					<div id="<?php echo isset($charges_customer)?'div_re':'div_result'?>" class="form_field"> 
					
					<?php
					echo $mine->get_customer_charges_info_new($newcustomerid,$newport,$newduty_charge);?>	
					
					</div>
					
					</div>
					
					</div>
					
					<?php
					
					}
					
					?>
					
					<div style="display:none" id="<?php echo (isset($charges_customer) && $charges_customer!='')?'div_re':'div_res'?>" class="raw">
					
					<div class="form_title"></div>
					
					<div class="form_field">
					
					<div id="<?php echo isset($charges_customer)?'div_re':'div_result'?>" class="form_field"> 
					
					
					
					</div>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none">
					
					<div class="form_title">Way of payment</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select">
					
					<?php get_statusdropdown(isset($job->way_of_payment)?$job->way_of_payment:'','way_of_payment','way_of_payment',(isset($job->way_of_payment)&& ($job->way_of_payment!=""))?' disabled="disabled" ':'',' onchange="getAllBankAccounts();" '); ?>
					
					</div>
					
					</div>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none" id="div_accounts">
					
					<div class="form_title">Accounts </div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select" id="div_accounts_responce">
					
					
					
					</div>
					
					</div>
					
					</div>
					
					</div>
	
				<?php
				} else if($job->job_step_no=='4'){
				?>
					
					<div class="raw form-group">
					
					<div class="form_title">Attachment</div>
					
					<div class="form_field">
					
					<?php /*?><?php if(isset($job->attachment)) {?>
					
					<img src="<?php echo base_url();?>uploads/jobs/<?php echo $job->attachment;?>" width="200" height="200"/><br />
					
					<?php }?>
					
					<input style="display:<?php echo isset($job->attachment)?'none':'';?> " type="file" name="attachment" id="attachment" multiple=""  class="formtxtfield" /><?php */?>
					
					
					
					
					
					<div id="drag-and-drop" class="uploader">            
					
					<div class="browser" id="div_attachment">
					
					
					<?php if(isset($job->attachment)) {?>
					
					<img src="<?php echo base_url();?>uploads/jobs/<?php echo $job->attachment;?>" width="200" height="200"/><br />
					</div>
					
					</div>
					<?php }else{?>
					
					<label>
					
					<span>Click to open the file Browser</span>
					
					<input type="file" name="files"  accept="image/*" title='Click to add Images'>
					
					</label>
					
					
					</div>
					
					</div>
					
					<div class="panel-body demo-panel-files" id='demo-filess'>
					
					<span class="demo-note">No Files have been selected/droped yet...</span>
					
					</div>
					<?php }?>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Mandatory Attachment</div>
					
					<div class="form_field">
					
					
					
					<div id="drag-and-drop-zone" class="uploader">            
					
					<div class="browser" id="div_mandatory_attachment">
					
					<?php if(isset($job->mandatory_attachment)) {?>
					
					<img src="<?php echo base_url();?>uploads/jobs_mandatory/<?php echo $job->mandatory_attachment;?>" width="200" height="200"/><br />
					</div>
					
					</div>
					<?php }else{?>
					<label>
					
					<span>Click to open the file Browser</span>
					
					<input type="file" name="files[]"  accept="image/*" title='Click to add Images'>
					
					</label>
					
					</div>
					
					</div>
					
					<div class="panel-body demo-panel-files" id='demo-files'>
					
					<span class="demo-note">No Files have been selected/droped yet...</span>
					
					</div>
					<?php }?>
					
					</div>
					
					<div class="raw" style="display:none;">
					
					<div class="form_title">Status</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select">
					
					<?php
					
					
					
					if(isset($job->job_status)){
					
						$setvalue = $job->job_status;
					
					}else{
					
						$setvalue = '';
					
					}
					
					
					
					/*if(isset($job->job_status) && ($job->job_status=='waiting_for_customer')){
					
						$setvalue = "open";
					
					}else if(isset($job->job_status) && ($job->job_status!='waiting_for_customer')){
					
						$setvalue = $job->job_status;
					
					}else{
					
						$setvalue = "waiting_for_customer";
					
					}*/
					
					
					
					?>
					
					<?php get_statusdropdown($setvalue,'job_status','job_status', ' onchange="showdiv_status_waiting();" '); ?>
					
					</div>
					
					</div>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Cargo status</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select">
					
					<?php get_statusdropdown(isset($job->cargo_status)?$job->cargo_status:'','cargo_status','cargo_status',''); ?>  
					
					</div>
					
					</div>  
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Cargo Delivery date</div>
					
					<div class="form_field">
					
					<input name="cargo_delivery_date" id="cargo_delivery_date" value="<?php echo isset($job->cargo_delivery_date)?$job->cargo_delivery_date:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Pickup by</div>
					
					<div class="form_field">
					
					<input name="pickup_by" id="pickup_by" value="<?php echo isset($job->pickup_by)?$job->pickup_by:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<!--  </div>-->
					
					<?php /*?>                    <div class="raw" align="center">
					
					<?php
					
					$btn_text = isset($job->id)?"Update":"Add";
					
					?>
					
					<input name="sub_mit" id="sub_mit" type="submit" class="submit_btn" value="<?php echo $btn_text;?>" />
					
					
					
					</div><?php */?>
					
					<!--end of raw--> 
					
				   
				<?php
				}
				//load view end
				
				//$this->load->view('jobs/job_sub_section',$this->_data,true);
					//redirect(base_url().'jobs/add_jobs/'.$job_id.'?e=11');
					exit();
				}
				else
				{	
					// add chages for ajax
					$job_id = $this->jobs->add_job($data);
					$customers = $this->_data['customers'] = $this->jobs->get_all_customers();
				
					$employee = $this->_data['employee'] = $this->jobs->get_all_employees();
					if($job_id	!=	'')
					{
						$jbos = $this->jobs->get_job_detail($job_id);
						if(!isset($jbos->customer_id))
						{
							//redirect(base_url().'jobs/options');
							exit();
						}
						$customerid = $jbos->customer_id;
						$current_customer = $this->_data['current_customers'] =$current_customers = $this->customers->get_customer_detail($customerid);
						//echo "<pre>";
						//print_r($current_customers);
						
							if(isset($current_customers->account_type) && $current_customers->account_type == 'fixed')
							$charges_customer = $this->_data['charges_customer'] = $this->customers->get_jobs_customers_rec($customerid,'an_fixed_job_charges','fixed',$job_id);
							if(isset($current_customers->account_type) && $current_customers->account_type == 'cash')
							$charges_customer = $this->_data['charges_customer'] = $this->customers->get_jobs_customers_rec($customerid,'an_cash_job_charges','cash',$job_id);
							if(isset($current_customers->account_type) && $current_customers->account_type == 'contract')
							$charges_customer = $this->_data['charges_customer'] = $this->customers->get_jobs_customers_rec($customerid,'an_contract_job_charges','contract',$job_id);
							
							$job = $this->_data['job'] = $this->jobs->get_job_detail($job_id);
					}else{
						$job_id = $this->_data['job_id']	=	'';
					}
					$mine = $this->_data['mine'] = $this;
					//load view start
				if($job->job_step_no=='' || $job->job_step_no=='1'){
				?>
				
					<input type="hidden" name="id" id="id" value="<?php echo (isset($job->job_id))?$job->job_id:''; ?>" />
					<input type="hidden" name="job_step_no" id="job_step_no" value="2" />
	
					<div class="raw form-group">
	
							<div class="form_title">Type of Shipment</div>
	
							<div class="form_field">
	
							<div class="dropmenu">
	
									<div class="styled-select">
	
								<?php get_statusdropdown(isset($job->type_of_shipment)?$job->type_of_shipment:'','type_of_shipment','type_of_shipment',' onchange="setFreightCharges();" '); ?>  
	
								</div>
	
								</div>
	
							</div>
	
						</div>
	
					<div class="raw form-group">
	
							<div class="form_title">Way of shipment</div>
	
							<div class="form_field">
	
							<div class="dropmenu">
	
									<div class="styled-select">
	
							  <?php get_statusdropdown(isset($job->way_of_shipment)?$job->way_of_shipment:'','way_of_shipment','way_of_shipment','','onchange="getChargesforOneShipment()"');?>
	
							  </div>
	
							  </div>
	
							</div>
	
						</div>                    
	
					<div class="raw form-group">
					
						<div class="form_title">Customer Name</div>
					
						<div class="form_field">
					
							<div class="dropmenu">
					
								<div class="styled-select">
					
									<select id="customer_id" name="customer_id" <?php echo isset($job->customer_id)?'disabled="disabled"':'';?>>
					
									<option value="">Select Customer</option>
					
									
					
									<?php
					
									foreach($customers as $eachCus){
					
										
					
										?>
					
										<option <?php echo (isset($job->customer_id) && $eachCus->id == $job->customer_id)?'selected="selected"':'';?> value="<?php echo $eachCus->id; ?>"><?php echo $eachCus->customer_name; ?></option>
					
										<?php
					
									}
					
									?>
					
									</select>
					
								</div>
					
							</div>
					
						</div>
					
					</div>                    
	
					<div class="raw form-group">
					
						<div class="form_title">Shipment Standards</div>
					
						<div class="form_field">
					
						<div class="dropmenu">
					
								<div class="styled-select">
					
						  <?php get_statusdropdown(isset($job->shipment_standard)?$job->shipment_standard:'','shipment_standard','shipment_standard',isset($job->shipment_standard)?'disabled="disabled"':'onchange="setFreightCharges();"');?>
					
						  </div>
					
						  </div>
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="div_freight_charges" style="display:<?php echo (isset($job->shipment_standard) && $job->shipment_standard=='FOB')?'':'none'; ?>">
					
						<div class="form_title">Freight Charges</div>
					
						<div class="form_field">
					
						  <input name="freight_charges" id="freight_charges" value="<?php echo isset($job->freight_charges)?$job->freight_charges:'0'; ?>" <?php echo isset($job->freight_charges)?'readonly="readonly"':''; ?>  type="text"  class="formtxtfield"/><label style="float:left; margin-right:21px">O.R.</label>
					
						  <input style="display:<?php echo isset($job->freight_charges)?'none':''; ?>" type="button" value="Currency Converter" id="currency_btn" onclick="showPopUpForFreightCharges();" />
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="">
					
						<div class="form_title">Document Type</div>
					
						<div class="form_field">
					
							<div class="dropmenu">
					
								<div class="styled-select">
					
									<?php get_statusdropdown('duplicate_document','document_charges','document_charges','', ' onchange="setFreightCharges();" '); ?>
					
								</div>
					
							</div>
					
						</div>
					
					</div>
					
					<div id="dynamicDiv" style="display:none">
					
					<div class="currency-wrapper">
					
						<div style="position:relative">
					
							<div style="position:absolute; right:-16px; top:-16px;">
					
								<a href="#_" onclick="hidePopUP();"><img src="<?php echo base_url();?>images/close.png" /></a>
					
							</div>
					
						</div>
					
						<!-- Start of content -->
					
						
					
						
					
						<!-- End of content -->
					
						
					
					</div><!-- End of currency-wrapper -->
					
					
					
					</div>
					
					<div class="raw form-group">
					
						<div class="form_title">Total Cost of Goods</div>
					
						<div class="form_field">
					
						  <input name="total_cost_of_goods" id="total_cost_of_goods" value="<?php echo isset($job->total_cost_of_goods)?$job->total_cost_of_goods:''; ?>" type="text"  class="formtxtfield"/><label style="float:left; margin-right:21px">O.R.</label>
					
						  <input style="display:<?php echo isset($job->total_cost_of_goods)?'none':''; ?>" type="button" value="Currency Converter" id="currency_btn" onclick="showPopUp();" />
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="div_insurance">
					
						<div class="form_title">Insurance</div>
					
						<div class="form_field">
					
						  <input readonly="readonly" name="insurance" id="insurance" value="<?php echo isset($job->insurance)?$job->insurance:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="div_doc_charges">
					
						<div class="form_title">Regional fees</div>
					
						<div class="form_field">
					
						  <label id="lbl_doc">20</label>
					
						</div>
					
					</div>                
					
					<div class="raw form-group" id="div_bl" style="display:<?php echo (isset($job->way_of_shipment)&&$job->way_of_shipment=='sea')?'':'none';?>">
					
						<div class="form_title">display:<?php echo (isset($job->way_of_shipment)&&$job->way_of_shipment=='sea')?'Regnacy fees':'Booking Number';?></div>
					
						<div class="form_field">
					
							<input name="bl_number" id="bl_number" value="<?php echo isset($job->bl_number)?$job->bl_number:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="div_air" style="display:none;">
					
						<div class="form_title">Air Way number</div>
					
						<div class="form_field">
					
							<input name="air_way_number" id="air_way_number" value="<?php echo isset($job->air_way_number)?$job->air_way_number:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
					
					<div class="raw form-group">
					
						<div class="form_title">Regency fees</div>
					
						<div class="form_field">
					
						  <input  readonly="readonly" name="duty_charges" id="duty_charges" value="<?php echo isset($job->duty_charges)?$job->duty_charges:''; ?>" type="text"  class="formtxtfield"/>
					
						  <div class="form_title" style="line-height:0.7px; width:146px;">Additional charges</div>
					
						  <input name="additional_charges" id="additional_charges" value="<?php echo isset($job->additional_charges)?$job->additional_charges:'0'; ?>" type="text"  class="formtxtfield"/>
					
						  
					
						  <input onclick="sendEmail()" type="button" value="Send Email" id="email_btn"  style="margin-left:21px;display:none<?php //echo (isset($job->email_send)&&($job->email_send==1))?'none':'' ?>"  />
					
						  
					
						  <span id="spanemail_send"><input type="hidden" name="email_send" id="email_send" value="0" /></span>
					
						</div>
					
					</div>
					
					<div class="raw form-group">
					
						<div class="form_title">Total Value</div>
					
						<div class="form_field">
					
						  <input  readonly="readonly" name="total_value" id="total_value" value="<?php echo isset($job->total_value)?$job->total_value:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="div_deninity_charge">
					
						<div class="form_title">Definity Charges</div>
					
						<div class="form_field">
					
						  <input  readonly="readonly" name="definity_charges" id="definity_charges" value="<?php echo isset($job->definity_charges)?$job->definity_charges:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
					
					<div class="raw form-group" id="div_unique_charges">
					
						<div class="form_title">Unique Charges</div>
					
						<div class="form_field">
					
						  <input  readonly="readonly" name="unique_charges" id="unique_charges" value="<?php echo isset($job->unique_charges)?$job->unique_charges:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
	
					<div class="raw form-group" id="div_insured_charges">
					
						<div class="form_title">Insured charges</div>
					
						<div class="form_field">
					
						  <input  readonly="readonly" name="insured_charges" id="insured_charges" value="<?php echo isset($job->insured_charges)?$job->insured_charges:''; ?>" type="text"  class="formtxtfield"/>
					
						</div>
					
					</div>
					
					<div class="raw form-group">
					
						<div class="form_title">Email Review</div>
					
						<div class="form_field">
					
						  
					
						  <textarea  style="height:auto" rows="4" cols="50" name="review" id="review" class="formtxtfield"><?php echo isset($job->review)?$job->review:'';?></textarea>
					
						</div>
					
					</div>
					
					<div class="raw">
					
						<div class="form_title">Review Status</div>
					
						<div class="form_field">
					
							<div class="dropmenu">
					
								<div class="styled-select">
					
									<?php
					
									if(isset($job->review_status) && ($job->review_status=='waiting_for_customer')){
					
										$review_status = "waiting_for_customer";
					
									}
					
									
					
									?>
					
									<?php get_statusdropdown(isset($job->review_status)?$job->review_status:'waiting_for_customer','review_status','review_status',''); ?>
					
								</div>
					
							</div>
					
						</div>
					
					</div>
					
					<div class="raw" align="center">
					
					<?php
					
					$btn_text = isset($job->id)?"Update":"Add";
					
					?>
					
						<input name="sub_mit" id="sub_mit" type="submit" class="submit_btn" value="<?php echo $btn_text;?>" />
					
						
					
					</div>
					   
				<?php
				} else if($job->job_step_no=='2'){
				?>
				
					<div class="raw form-group" style="display:none;">
					
					<div class="form_title">Job Type</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select">
					
					<?php get_statusdropdown(isset($job->job_type)?$job->job_type:'','job_type','job_type',''); ?>
					
					</div>
					
					</div>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title" id="div_shipper_name">Shipper Name</div>
					
					<div class="form_field">
					
					<input name="consignee_name" id="consignee_name" value="<?php echo isset($job->consignee_name)?$job->consignee_name:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none">
					
					<div class="form_title">Container type</div>
					
					<div class="form_field">
					
					<input name="container_type" id="container_type" value="<?php echo isset($job->container_type)?$job->container_type:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group" id="div_ctn" style="display:<?php echo (isset($job->way_of_shipment)&&$job->way_of_shipment=='sea')?'':'none';?>">
					
					<div class="form_title">Container number</div>
					
					<div class="form_field" id="div_container">
					
					<input id="countmeContainer" name="count_container" type="text"  class="formtxtfield"/>
					
					<input type="button" id="btn_container" onclick="AddCtn();" value="Add" />
					
					</div>
					
					
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title"></div>
					
					<div class="form_field" id="containertablediv" style="display:none;">
					
					
					
					</div>
					
					
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Gross weight</div>
					
					<div class="form_field">
					
					<input name="gross_weight" id="gross_weight" value="<?php echo isset($job->gross_weight)?$job->gross_weight:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Quantity</div>
					
					<div class="form_field">
					
					<input name="quaintity" id="quaintity" value="<?php echo isset($job->quaintity)?$job->quaintity:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none;">
					
					<div class="form_title">Regency fees Recieve Date</div>
					
					<div class="form_field">
					
					<input name="receive_duty_charges" id="receive_duty_charges" value="<?php echo isset($job->receive_duty_charges)?$job->receive_duty_charges:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none;">
					
					<div class="form_title">Document recipt date</div>
					
					<div class="form_field">
					
					<input name="doc_recipt_date" id="doc_recipt_date" value="<?php echo isset($job->doc_recipt_date)?$job->doc_recipt_date:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Pickup Location</div>
					
					<div class="form_field">
					
					<input name="pickup_location" id="pickup_location" value="<?php echo isset($job->pickup_location)?$job->pickup_location:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Delivery Location</div>
					
					<div class="form_field">
					
					<input name="delivery_location" id="delivery_location" value="<?php echo isset($job->delivery_location)?$job->delivery_location:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none;">
					
					<div class="form_title">Region </div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select">
					
					<?php
					
					$region_id = isset($job->region_id)?$job->region_id:'';
					
					$language = 'english';
					
					$name = 'wilayat_id';
					
					$value = isset($job->wilayat_id)?$job->wilayat_id:'';
					
					?>
					
					<?php region_dropbox('region_id',isset($job->region_id)?$job->region_id:'','english','onchange="getWilayat_id()"'); ?>  
					
					</div>
					
					</div>  
					
					</div>
					
					</div>
					
					<div class="raw form-group" id="wilayat_id_main_div" style="display:none<?php //echo isset($job->wilayat_id)?'block':'none'?>">
					
					<div class="form_title">Wilayat</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select" id="wilayat_id_div">
					
					<?php wilayat_dropbox('wilayat_id',isset($job->wilayat_id)?$job->wilayat_id:'','english'); ?>  
					
					</div>
					
					</div>  
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none">
					
					<div class="form_title">Destination</div>
					
					<div class="form_field">
					
					<input name="destination" id="destination" value="<?php echo isset($job->destination)?$job->destination:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none">
					
					<div class="form_title">Area</div>
					
					<div class="form_field">
					
					<input name="area" id="area" value="<?php echo isset($job->area)?$job->area:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Select Branch</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select">
					
					<?php company_branch_dropbox('branch_id',isset($job->branch_id)?$job->branch_id:'','onchange=getBranchEmployees()');?>
					
					</div>
					
					</div>
					
					</div>
					
					</div>
					
					<div class="raw form-group" id="emp_id_main_div" style="display:none;">
					
					<div class="form_title">Driver Name</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select" id="emp_id_div">
					
					<input name="driver_id" id="driver_id" value="<?php echo isset($job->driver_id)?$job->driver_id:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Estimated time depart</div>
					
					<div class="form_field">
					
					<input name="estimated_time_depart" id="estimated_time_depart" value="<?php echo isset($job->estimated_time_depart)?$job->estimated_time_depart:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Arrival time</div>
					
					<div class="form_field">
					
					<input name="arrival_time" id="arrival_time" value="<?php echo isset($job->arrival_time)?$job->arrival_time:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Remarks</div>
					
					<div class="form_field">
					
					<textarea style="height:auto"   rows="4" cols="50" id="remarks" name="remarks"><?php echo isset($job->remarks)?$job->remarks:''; ?></textarea>
					
					</div>
					
					</div>  
					
					<div class="raw form-group">
					
					<div class="form_title">bill of entry</div>
					
					<div class="form_field">
					
					<input name="bill_of_calling_no" id="bill_of_calling_no" value="<?php echo isset($job->bill_of_calling_no)?$job->bill_of_calling_no:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" id="div_mrn_no" style="display:none">
					
					<div class="form_title">mrn no</div>
					
					<div class="form_field">
					
					<input name="mrn_number" id="mrn_number" value="<?php echo isset($job->mrn_number)?$job->mrn_number:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" id="div_delivery_order_no" style="display:none">
					
					<div class="form_title">delivery order no</div>
					
					<div class="form_field">
					
					<input name="delivery_order_no" id="delivery_order_no" value="<?php echo isset($job->delivery_order_no)?$job->delivery_order_no:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" id="div_booking_no" style="display:none">
					
					<div class="form_title">booking no</div>
					
					<div class="form_field">
					
					<input name="booking_no" id="booking_no" value="<?php echo isset($job->booking_no)?$job->booking_no:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" id="div_commercial_reg_no" style="display:none">
					
					<div class="form_title">commercial registration no.</div>
					
					<div class="form_field">
					
					<input name="commercial_reg_no" id="commercial_reg_no" value="<?php echo isset($job->commercial_reg_no)?$job->commercial_reg_no:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" id="div_delivery_order_no" style="display:none;">
					
					<div class="form_title">delivery order no.</div>
					
					<div class="form_field">
					
					<input name="delivery_order_no" id="delivery_order_no" value="<?php echo isset($job->delivery_order_no)?$job->delivery_order_no:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" style="display:none;">
					
					<div class="form_title">Capt.</div>
					
					<div class="form_field">
					
					<input name="capt" id="capt" value="<?php echo isset($job->capt)?$job->capt:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group">
					
					<div class="form_title">Carriers name.</div>
					
					<div class="form_field">
					
					<input name="carriers_name" id="carriers_name" value="<?php echo isset($job->carriers_name)?$job->carriers_name:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group" id="div_voyage_flight">
					
					<div class="form_title" id="voyage_flight_div">Voyage / flight no.</div>
					
					<div class="form_field">
					
					<input name="voyage_flight_no" id="voyage_flight_no" value="<?php echo isset($job->voyage_flight_no)?$job->voyage_flight_no:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group">
					
					<div class="form_title">Port of loading.</div>
					
					<div class="form_field">
					
					<input name="port_of_loading" id="port_of_loading" value="<?php echo isset($job->port_of_loading)?$job->port_of_loading:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group">
					
					<div class="form_title">Port of Discharge.</div>
					
					<div class="form_field">
					
					<input name="loading_place" id="loading_place" value="<?php echo isset($job->loading_place)?$job->loading_place:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
					
					<div class="raw form-group">
					
					<div class="form_title">offloading place.</div>
					
					<div class="form_field">
					
					<input name="offloading_place" id="offloading_place" value="<?php echo isset($job->offloading_place)?$job->offloading_place:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div> 
	
				<?php
				} else if($job->job_step_no=='3'){
				?>
	
					<?php
					
					//if(isset($job->job_id) && $job->job_id!=''){
					
					?>
					
					
					
					<?php /*?>                <div id="div_status_waiting" style="display:<?php if(isset($job->job_status) && $job->job_status=='waiting_for_customer') echo 'none'; else if(!isset($job->job_status)) echo 'none'; else echo 'block';?>"><?php */?>
					
					<?php
					//echo '<pre>';
					//print_r($charges_customer);
					//exit;
					if(isset($charges_customer) && $charges_customer!='')
					{
					
					?>
					
					<div class="raw">
					
					<div class="form_title"></div>
					
					<div class="form_field">
					
					<div class="form_field"> 
					
					<?php
					
					//print_r($current_customers);
					
					$i=1;
					
					if(isset($current_customers->account_type) && ($current_customers->account_type == 'cash' || $current_customers->account_type == 'fixed')){
					
					?>
					
					<select id="chargeSelectorfixed" onchange="getvalueofcharge();">
					<option value="">Select Charges</option>
					<?php    
					foreach($charges_customer as $result){
						if($result['charge_id']=='7'){continue;}
						?>
						<option value="<?php echo $result['charge_id'];?>"><?php echo $result['charge_name'];?></option>
						<?php
					}
						?>
						</select>
						
					<?php    
					foreach($charges_customer as $result){
					
						if($result['total_charge']==0){
					
							//continue;
					
						}
					
						$readonly = (isset($result['is_company_paid']) && $result['is_company_paid']!="0")?' disabled="disabled"':'';
					
						?>
					
						<?php
					
						if($result['charge_id']=='7'){
					
							?>
					
							<?php /*?><div style="height:<?php echo ($readonly!='')?'87px':'96px'; ?>;clear:left;" id="div_<?php echo $result['charge_id'];?>">
					
					<label style="visibility:hidden;width:200px;float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input  <?php echo $readonly;?> placeholder="Enter per charge."  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>"  onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  value="<?php echo $result['per_charge_amount']?>" style="visibility:hidden;width:100px" /> &nbsp;&nbsp;
					
					<label style="visibility:hidden;width:200px;float:left">No. of  <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter no." onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  class="formtxtfield" type="text" name="txtno_<?php echo $result['charge_id'];?>" id="txtno_<?php echo $result['charge_id'];?>" value="<?php echo $result['charge_quaintity']?>" style="visibility:hidden;width:100px" /> &nbsp;&nbsp;
					
					<label style="width:200px;float:left">Total <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="total value"  class="formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" value="<?php echo $result['total_charge']?>" style="width:100px" /> 
					
					<span style="margin-top:15px;display:<?php echo ($readonly!='')?'none':'';?>">
					
					<!-- <br /><br />-->
					
					<label style="width:200px;float:left">Select Payment Way &nbsp;&nbsp;</label>
					
					<div class="dropmenu" >
					
					<div class="styled-select">
					
					<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
					
					</div>
					
					</div>
					
					
					
					<span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
					
					<label style="width:200px;float:left">Select Account &nbsp;&nbsp;</label>
					
					<div class="dropmenu">
					
					<div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
					
					
					
					</div>
					
					</div>
					
					</span>
					
					</span>
					
					<?php /*?><a class="del_txt" id="anc_<?php echo $result['charge_id'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php * /?></div><?php */?>
					
							<?php
					
							continue;
					
						}
					
						?>
					
						
						<div style="display:none;" id="divs_<?php echo $result['charge_id'];?>">
						<div  style="height:<?php echo ($readonly!='')?'87px':'96px'; ?>;clear:left;" id="div_<?php echo $result['charge_id'];?>">
					
					<label style="float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input  <?php echo $readonly;?> placeholder="Enter per charge."  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>"  onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  value="<?php echo $result['per_charge_amount']?>" style="width:5%" /> &nbsp;&nbsp;
					
					<label style="float:left">No. of  <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter no." onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  class="formtxtfield" type="text" name="txtno_<?php echo $result['charge_id'];?>" id="txtno_<?php echo $result['charge_id'];?>" value="<?php echo $result['charge_quaintity']?>" style="width:5%" /> &nbsp;&nbsp;
					
					<label style="float:left">Total Purchase Charges&nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="total purchase"  onchange="gettotalsof();" class="totalpur formtxtfield" type="text" name="txtpur_<?php echo $result['charge_id'];?>" id="txtpur_<?php echo $result['charge_id'];?>" value="<?php echo $result['pur_charge']?>" style="width:5%" /> 
					
					
					
					<label style="float:left">Total Sales Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="total value"  class="totalsale formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" value="<?php echo $result['total_charge']?>"  onchange="gettotalsof();" style="width:5%" /> 
					
					
					
					<span style="margin-top:15px;display:<?php echo ($readonly!='')?'none':'';?>">
					
					<!-- <br /><br />-->
					
					<label style="float:left">Select Payment Way &nbsp;&nbsp;</label>
					
					<div class="dropmenu" >
					
					<div class="styled-select">
					
					<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
					
					</div>
					
					</div>
					
					
					
					<span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
					
					<label style="float:left">Select Account &nbsp;&nbsp;</label>
					
					<div class="dropmenu">
					
					<div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
					
					
					
					</div>
					
					</div>
					
					</span>
					
					</span>
					
					<?php /*?><a class="del_txt" id="anc_<?php echo $result['charge_id'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php */?></div>
						</div>
						
					
						<?php /*?><div style="height:38px;clear:left;" id="div_<?php echo $result['charge_id'];?>"><label style="width:200px;float:left"><?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label><input placeholder="Enter charge."  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" disabled="disabled" value="<?php echo $result['per_charge_amount']?>" />  <?php /*?><a class="del_txt" id="anc_<?php echo $result['charge_id'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php *?></div><?php */?>
					
						<?php
					
						$i++;
					
					}
					
					?>
					<div id="fixed_append"></div>
					<?php
					
					}else if(isset($current_customers->account_type) && $current_customers->account_type == 'contract'){
					
					?>
					<select id="chargeSelectorcontract" onchange="getvalueofchargecontract();">
					<option value="">Select Charges</option>
					<?php    
					foreach($charges_customer as $result){
						if($result['charge_id']=='7'){continue;}
						?>
						<option value="<?php echo $result['charge_id'];?>"><?php echo $result['charge_name'];?></option>
						<?php
					}
						?>
						</select>
						
					
					<?php
					foreach($charges_customer as $result){
					
						
					
						if($result['total_charge']==0){
					
							//continue;
					
						}
					
						//$readonly = (isset($result['is_company_paid']) && $result['is_company_paid']!="0")?' readonly="readonly"':'';
						$readonly = (isset($result['is_company_paid']) && $result['is_company_paid']!="0")?' disabled="disabled"':'';
					
						
					
						 //$valus1 = $this->customers->get_contract_customer_values($customer->id,$result['charge_id']);
					
						 $charge_quaintity = $result['charge_quaintity'];
					
						 $per_charge_amount = $result['per_charge_amount']
					
						
					
						?>
					
						 <?php
					
						if($result['charge_id']=='7'){
					
							?>
					
							<?php /*?><div style="height:38px;" id="div_<?php echo $result['charge_id'];?>">
					
					<label style="visibility:hidden;width:200px;float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> type="text"  class="formtxtfield" placeholder="Enter per expense charges." name="txtpercharge_<?php echo $result['charge_id'];?>" id="txtpercharge_<?php echo $result['charge_id'];?>" value="<?php echo $per_charge_amount;?>" style="visibility:hidden;margin-left:10px; width:5%;" />&nbsp;&nbsp;
					
					
					
					<label style="visibility:hidden;width:200px;float:left">No. of <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter quantity"  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" style="visibility:hidden;width:5%"  value="<?php echo $charge_quaintity;?>" />&nbsp;&nbsp; &nbsp;&nbsp;
					
					<label style="width:200px;float:left">Total <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter quantity"  class="formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" style="width:5%"  value="<?php echo ($charge_quaintity*$per_charge_amount);?>" />
					
					
					
					<span style="margin-top:15px;display:<?php echo ($readonly!='')?'none':'';?>">
					
					<!-- <br /><br />-->
					
					<label style="width:200px;float:left">Select Payment Way &nbsp;&nbsp;</label>
					
					<div class="dropmenu" >
					
					<div class="styled-select">
					
					<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
					
					</div>
					
					</div>
					
					
					
					<span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
					
					<label style="width:200px;float:left">Select Account &nbsp;&nbsp;</label>
					
					<div class="dropmenu">
					
					<div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
					
					
					
					</div>
					
					</div>
					
					</span>
					
					</span>
					
					
					
					&nbsp;&nbsp; &nbsp;&nbsp;
					
					</div><?php */?>
					
							<?php
					
							continue;
					
						}
					
						?>
					<div style="display:none" id="divs_<?php echo $result['charge_id'];?>">
						<div style="height:38px;" id="div_<?php echo $result['charge_id'];?>">
					
					<label style="float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> type="text"  class="formtxtfield" placeholder="Enter per expense charges." name="txtpercharge_<?php echo $result['charge_id'];?>" id="txtpercharge_<?php echo $result['charge_id'];?>" value="<?php echo $per_charge_amount;?>" style="margin-left:10px; width:5%;" />&nbsp;&nbsp;
					
					
					
					<label style="float:left">No. of <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter quantity"  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" style="width:5%"  value="<?php echo $charge_quaintity;?>" />&nbsp;&nbsp; &nbsp;&nbsp;
					
					
					
					<label style="float:left">Total Purchase Charges&nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter purchase" onchange="gettotalsof();"  class="totalpur formtxtfield" type="text" name="txtpur_<?php echo $result['charge_id'];?>" id="txtpur_<?php echo $result['charge_id'];?>" style="width:5%"  value="<?php echo $result['pur_charge'];?>" />
					
					
					
					
					
					<label style="float:left">Total Sales Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
					
					<input <?php echo $readonly;?> placeholder="Enter Sales"  onchange="gettotalsof();" class="totalsale formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" style="width:5%"  value="<?php echo ($charge_quaintity*$per_charge_amount);?>" />
					
					
					
					
					
					<span style="margin-top:15px;display:<?php echo ($readonly!='')?'none':'';?>">
					
					<!-- <br /><br />-->
					
					<label style="float:left">Select Payment Way &nbsp;&nbsp;</label>
					
					<div class="dropmenu" >
					
					<div class="styled-select">
					
					<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
					
					</div>
					
					</div>
					
					
					
					<span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
					
					<label style="float:left">Select Account &nbsp;&nbsp;</label>
					
					<div class="dropmenu">
					
					<div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
					
					
					
					</div>
					
					</div>
					
					</span>
					
					</span>
					
					
					
					&nbsp;&nbsp; &nbsp;&nbsp;
					
					<?php /*?><a class="del_txt" id="anc_<?php echo $result['per_charge_amount'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php */?></div>
					</div>
						
					
						
					
						<?php /*?><div style="height:38px;" id="div_<?php echo $result['charge_id'];?>"><label style="width:200px;float:left"><?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label><input placeholder="Enter quantity"  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" disabled="disabled" value="<?php echo $charge_quaintity;?>" />&nbsp;&nbsp; &nbsp;&nbsp;<input type="text"  class="formtxtfield" placeholder="Enter per expense charges." disabled="disabled" name="txtpercharge_<?php echo $result['charge_id'];?>" id="txtpercharge_<?php echo $result['charge_id'];?>" value="<?php echo $per_charge_amount;?>" style="margin-left:10px;" />&nbsp;&nbsp;<?php /*?><a class="del_txt" id="anc_<?php echo $result['per_charge_amount'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php *?></div><?php */?>
					
						<?php
					
						$i++;
					
					}
					
					?>
					<div id="contract_append"></div>
					<?php							
					
					}
					
					?>
					
					</div>
					
					</div>
					
					</div>
					
					<?php
					
					?>
					
					<input <?php //echo (isset($readonly)?'disabled="disabled"':'')?>  type="hidden" name="count_txt" id="count_txt" value="<?php echo  $result['charge_id'];?>" />
					
					<br />
					
					<label id="lbl_pur" style='float:left'>Purchase = 0 &nbsp;&nbsp;</label>  
					
					<label id="lbl_sales" style='float:left'>Sales = 0 &nbsp;&nbsp;</label> 
					
					<label id="lbl_profit" style='float:left'>Profit = 0 OMR &nbsp;&nbsp;</label>
					
					<?php
					
					}
					else 
					{
					$newcustomerid = isset($job->customer_id)?$job->customer_id:'';
					$newport = isset($job->way_of_shipment)?$job->way_of_shipment:'';
					$newduty_charge = isset($job->duty_charges)?$job->duty_charges:'';
					
					?>
					<div id="<?php echo (isset($charges_customer) && $charges_customer!='')?'div_re':'div_res'?>" class="raw">
					
					<div class="form_title"></div>
					
					<div class="form_field">
					
					<div id="<?php echo isset($charges_customer)?'div_re':'div_result'?>" class="form_field"> 
					
					<?php
					echo $mine->get_customer_charges_info_new($newcustomerid,$newport,$newduty_charge);?>	
					
					</div>
					
					</div>
					
					</div>
					
					<?php
					
					}
					
					?>
					
					<div style="display:none" id="<?php echo (isset($charges_customer) && $charges_customer!='')?'div_re':'div_res'?>" class="raw">
					
					<div class="form_title"></div>
					
					<div class="form_field">
					
					<div id="<?php echo isset($charges_customer)?'div_re':'div_result'?>" class="form_field"> 
					
					
					
					</div>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none">
					
					<div class="form_title">Way of payment</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select">
					
					<?php get_statusdropdown(isset($job->way_of_payment)?$job->way_of_payment:'','way_of_payment','way_of_payment',(isset($job->way_of_payment)&& ($job->way_of_payment!=""))?' disabled="disabled" ':'',' onchange="getAllBankAccounts();" '); ?>
					
					</div>
					
					</div>
					
					</div>
					
					</div>
					
					<div class="raw form-group" style="display:none" id="div_accounts">
					
					<div class="form_title">Accounts </div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select" id="div_accounts_responce">
					
					
					
					</div>
					
					</div>
					
					</div>
					
					</div>
	
				<?php
				} else if($job->job_step_no=='4'){
				?>
					
					<div class="raw form-group">
					
					<div class="form_title">Attachment</div>
					
					<div class="form_field">
					
					<?php /*?><?php if(isset($job->attachment)) {?>
					
					<img src="<?php echo base_url();?>uploads/jobs/<?php echo $job->attachment;?>" width="200" height="200"/><br />
					
					<?php }?>
					
					<input style="display:<?php echo isset($job->attachment)?'none':'';?> " type="file" name="attachment" id="attachment" multiple=""  class="formtxtfield" /><?php */?>
					
					
					
					
					
					<div id="drag-and-drop" class="uploader">            
					
					<div class="browser" id="div_attachment">
					
					
					<?php if(isset($job->attachment)) {?>
					
					<img src="<?php echo base_url();?>uploads/jobs/<?php echo $job->attachment;?>" width="200" height="200"/><br />
					</div>
					
					</div>
					<?php }else{?>
					
					<label>
					
					<span>Click to open the file Browser</span>
					
					<input type="file" name="files"  accept="image/*" title='Click to add Images'>
					
					</label>
					
					
					</div>
					
					</div>
					
					<div class="panel-body demo-panel-files" id='demo-filess'>
					
					<span class="demo-note">No Files have been selected/droped yet...</span>
					
					</div>
					<?php }?>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Mandatory Attachment</div>
					
					<div class="form_field">
					
					
					
					<div id="drag-and-drop-zone" class="uploader">            
					
					<div class="browser" id="div_mandatory_attachment">
					
					<?php if(isset($job->mandatory_attachment)) {?>
					
					<img src="<?php echo base_url();?>uploads/jobs_mandatory/<?php echo $job->mandatory_attachment;?>" width="200" height="200"/><br />
					</div>
					
					</div>
					<?php }else{?>
					<label>
					
					<span>Click to open the file Browser</span>
					
					<input type="file" name="files[]"  accept="image/*" title='Click to add Images'>
					
					</label>
					
					</div>
					
					</div>
					
					<div class="panel-body demo-panel-files" id='demo-files'>
					
					<span class="demo-note">No Files have been selected/droped yet...</span>
					
					</div>
					<?php }?>
					
					</div>
					
					<div class="raw" style="display:none;">
					
					<div class="form_title">Status</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select">
					
					<?php
					
					
					
					if(isset($job->job_status)){
					
						$setvalue = $job->job_status;
					
					}else{
					
						$setvalue = '';
					
					}
					
					
					
					/*if(isset($job->job_status) && ($job->job_status=='waiting_for_customer')){
					
						$setvalue = "open";
					
					}else if(isset($job->job_status) && ($job->job_status!='waiting_for_customer')){
					
						$setvalue = $job->job_status;
					
					}else{
					
						$setvalue = "waiting_for_customer";
					
					}*/
					
					
					
					?>
					
					<?php get_statusdropdown($setvalue,'job_status','job_status', ' onchange="showdiv_status_waiting();" '); ?>
					
					</div>
					
					</div>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Cargo status</div>
					
					<div class="form_field">
					
					<div class="dropmenu">
					
					<div class="styled-select">
					
					<?php get_statusdropdown(isset($job->cargo_status)?$job->cargo_status:'','cargo_status','cargo_status',''); ?>  
					
					</div>
					
					</div>  
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Cargo Delivery date</div>
					
					<div class="form_field">
					
					<input name="cargo_delivery_date" id="cargo_delivery_date" value="<?php echo isset($job->cargo_delivery_date)?$job->cargo_delivery_date:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<div class="raw form-group">
					
					<div class="form_title">Pickup by</div>
					
					<div class="form_field">
					
					<input name="pickup_by" id="pickup_by" value="<?php echo isset($job->pickup_by)?$job->pickup_by:''; ?>" type="text"  class="formtxtfield"/>
					
					</div>
					
					</div>
					
					<!--  </div>-->
					
					<?php /*?>                    <div class="raw" align="center">
					
					<?php
					
					$btn_text = isset($job->id)?"Update":"Add";
					
					?>
					
					<input name="sub_mit" id="sub_mit" type="submit" class="submit_btn" value="<?php echo $btn_text;?>" />
					
					
					
					</div><?php */?>
					
					<!--end of raw--> 
					
				   
				<?php
				}
				//load view end
					//$this->load->view('job_sub_section',	$this->_data,true);
					// add chages for ajax
					exit();
				}
		}
		else
		{
			$this->_data['customers'] = $this->jobs->get_all_customers();
			
			$this->_data['employee'] = $this->jobs->get_all_employees();
			if($job_id	!=	'')
			{
				$jbos = $this->jobs->get_job_detail($job_id);
			if(!isset($jbos->customer_id))
			{
				redirect(base_url().'jobs/options');
				exit();
			}
			$customerid = $jbos->customer_id;
			$this->_data['current_customers'] =$current_customers = $this->customers->get_customer_detail($customerid);
			//echo "<pre>";
			//print_r($current_customers);
			
				if(isset($current_customers->account_type) && $current_customers->account_type == 'fixed')
				$this->_data['charges_customer'] = $this->customers->get_jobs_customers_rec($customerid,'an_fixed_job_charges','fixed',$job_id);
				if(isset($current_customers->account_type) && $current_customers->account_type == 'cash')
				$this->_data['charges_customer'] = $this->customers->get_jobs_customers_rec($customerid,'an_cash_job_charges','cash',$job_id);
				if(isset($current_customers->account_type) && $current_customers->account_type == 'contract')
				$this->_data['charges_customer'] = $this->customers->get_jobs_customers_rec($customerid,'an_contract_job_charges','contract',$job_id);
				
				$this->_data['job'] = $this->jobs->get_job_detail($job_id);
			}else{
   				$this->_data['job_id']	=	'';
			}
			$this->_data['mine'] = $this;
		 	$this->load->view('add-job-form',	$this->_data);
		}
   }
   
   public function getAllBankAccountsForCharges()
   {
	   $id = $this->input->post('id');
	   $res = $this->jobs->LoadAllBankAccount();
	   ?>
       <select id="account_id<?php echo $id;?>" name="account_id<?php echo $id;?>" onchange="" >
       	   <option value="">Select Bank Account</option>
		   <?php
           foreach($res as $eachRes){
               ?>
               <option value="<?php echo $eachRes->account_id;?>"><?php echo $eachRes->account_name;?></option>
               <?php
           }
           ?>
       </select>
       <?php
	   
   }
   
   public function get_customer_charges_info_new($customerid='',$port='',$duty_charge='')
	{
		//$customerid=$this->input->post('customer_id');
		//$port=$this->input->post('port');
		//$duty_charge = $this->input->post('duty_charge');
			if($customerid	!=	'')
			{
				$customer = $this->_data['customer'] = $this->customers->get_customer_detail($customerid);
				$charges_by_port = $this->customers->get_charges_by_port($port,$customer->account_type);
				$i=1;
				
				if($customer->account_type == 'cash'){
					foreach($charges_by_port as $result){
						?>
                        <div style="display:none" id="divs_<?php echo $result['charge_id'];?>">
						<div style="height:89px;clear:left; border-top: 1px solid rgb(255, 255, 255); padding: 7px;" id="div_<?php echo $result['charge_id'];?>">
                        <label style="width:200px;float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter per charge."  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>"  onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  value="0" style="width:100px" /> &nbsp;&nbsp;
                        <label style="width:200px;float:left">No. of  <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter no." onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  class="formtxtfield" type="text" name="txtno_<?php echo $result['charge_id'];?>" id="txtno_<?php echo $result['charge_id'];?>" value="0" style="width:100px" /> &nbsp;&nbsp;
                        
                        <label style="width:200px;float:left">Total Purchase Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="total purchase"  class="totalpur formtxtfield" type="text" name="txtpur_<?php echo $result['charge_id'];?>" onchange="gettotalsof();" id="txtpur_<?php echo $result['charge_id'];?>" value="0" style="width:100px" /> 
                        <span style="margin-top:15px;">
                        <label style="width:200px;float:left">Total Sales Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="total value"  class="totalsale formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" value="0" style="width:100px"  /> 
                        </span>
                        <?php
						if($result['is_actual']=='1'){
							?>
                            
							<span style="margin-top:15px;">
						   <!-- <br /><br />-->
							<label style="width:200px;float:left">Select Payment Way &nbsp;&nbsp;</label>
							<div class="dropmenu" >
									<div class="styled-select">
							<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
									</div>
							</div>
							
							<span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
							<label style="width:200px;float:left">Select Account &nbsp;&nbsp;</label>
							<div class="dropmenu">
								<div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
								  
								</div>
							  </div>
							</span>
							</span>
							<?php
						}else{
							?>
                            <input type="hidden" name="<?php echo 'way_of_payment'.$result['charge_id'];?>" id="<?php echo 'way_of_payment'.$result['charge_id'];?>" value="Cash" />
                            <?php
						}
						?>
						<?php /*?><a class="del_txt" id="anc_<?php echo $result['charge_id'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php */?></div>
						</div>
						<?php
						$i++;
					}
				}else if($customer->account_type == 'fixed'){
					foreach($charges_by_port as $result){
						 $valus1 = $this->customers->get_fixed_customer_values($customer->id,$result['charge_id']);
						 $valus = isset($valus1->charge_value)?$valus1->charge_value:0;
						//print_r($result['is_actual']);
						?>
                        <div style="display:none" id="divs_<?php echo $result['charge_id'];?>">
						<div style="height:89px;clear:left; border-top: 1px solid rgb(255, 255, 255); padding: 7px;" id="div_<?php echo $result['charge_id'];?>">
                        <label style="width:200px;float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter per charge." class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" <?php echo ($valus>0)?'readonly="readonly"':'';?> value="<?php echo $valus;?>"   onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  style="width:100px"   />  
						<label style="width:200px;float:left">No. of <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter no." class="formtxtfield" type="text" name="txtno_<?php echo $result['charge_id'];?>" id="txtno_<?php echo $result['charge_id'];?>" value="0"   onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  style="width:100px"  />  
                        <label style="width:200px;float:left">Total Purchase Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="total purchase"  onchange="gettotalsof();" class="totalpur formtxtfield" type="text" name="txtpur_<?php echo $result['charge_id'];?>" id="txtpur_<?php echo $result['charge_id'];?>" style="width:100px"  value="0" />  
                        <span style="margin-top:15px;">
                        <label style="width:200px;float:left">Total Sales Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="total value." class="totalsale formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" style="width:100px"  value="0" /> 
                        </span> 
						<?php
						if($result['is_actual']=='1'){
							?>
                            
                        <span style="margin-top:15px;">
                       <!-- <br /><br />-->
						<label style="width:200px;float:left">Select Payment Way &nbsp;&nbsp;</label>
						<div class="dropmenu" >
                      	    	<div class="styled-select">
						<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
                        		</div>
                        </div>
                        
                        <span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
                        <label style="width:200px;float:left">Select Account &nbsp;&nbsp;</label>
                        <div class="dropmenu">
                            <div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
                              
                            </div>
                          </div>
                        </span>
                        </span>
                        <?php
                        }else{
							?>
                            <input type="hidden" name="<?php echo 'way_of_payment'.$result['charge_id'];?>" id="<?php echo 'way_of_payment'.$result['charge_id'];?>" value="Cash" />
                            <?php
						}
						?>
						<?php /*?><a class="del_txt" id="anc_<?php echo $result['charge_id'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php */?>
                        </div>
                        </div>
						<?php
						$i++;
					}
				}else if($customer->account_type == 'contract'){
					foreach($charges_by_port as $result){
						
						 $valus1 = $this->customers->get_contract_customer_values($customer->id,$result['charge_id']);
						 $charge_quaintity = isset($valus1->charge_quaintity)?$valus1->charge_quaintity:0;
						 $per_charge_amount = isset($valus1->per_charge_amount)?$valus1->per_charge_amount:0;
						
						?>
						<div style="display:none" id="divs_<?php echo $result['charge_id'];?>">
                        <div style="height:89px;border-top: 1px solid rgb(255, 255, 255); padding: 7px;" id="div_<?php echo $result['charge_id'];?>">
                        <label style="width:200px;float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input type="text"  class="formtxtfield" placeholder="Enter per expense charges." name="txtpercharge_<?php echo $result['charge_id'];?>" id="txtpercharge_<?php echo $result['charge_id'];?>" value="<?php echo $per_charge_amount;?>"  onchange="calculate_total_cost_charges('<?php echo $result['charge_id'];?>');"  style="margin-left:10px; width:100px;" />&nbsp;&nbsp;
                        
                        <label style="width:200px;float:left">No. of <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input  onchange="calculate_total_cost_charges('<?php echo $result['charge_id'];?>');"  placeholder="Enter quantity"  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" style="width:100px"  value="<?php echo $charge_quaintity;?>" />&nbsp;&nbsp; &nbsp;&nbsp;
                        
                        <label style="width:200px;float:left">Total Purchase Charges&nbsp;&nbsp;</label>
                        <input placeholder="Enter purchases" onchange="gettotalsof();"  class="totalpur formtxtfield" type="text" name="txtpur_<?php echo $result['charge_id'];?>" id="txtpur_<?php echo $result['charge_id'];?>" style="width:100px"  value="" />&nbsp;&nbsp; &nbsp;&nbsp;
                        <span style="margin-top:15px;">
                        <label style="width:200px;float:left">Total Sales Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter quantity"  class="totalsale formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" style="width:100px"  value="<?php echo ($charge_quaintity*$per_charge_amount);?>" />&nbsp;&nbsp; &nbsp;&nbsp;
                        </span>
                        <?php
						if($result['is_actual']=='1'){
							?>
                            
                        <span style="margin-top:15px;">
                       <!-- <br /><br />-->
						<label style="width:200px;float:left">Select Payment Way &nbsp;&nbsp;</label>
						<div class="dropmenu" >
                      	    	<div class="styled-select">
						<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
                        		</div>
                        </div>
                        
                        <span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
                        <label style="width:200px;float:left">Select Account &nbsp;&nbsp;</label>
                        <div class="dropmenu">
                            <div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
                              
                            </div>
                          </div>
                        </span>
                        </span>
                        <?php
                        }else{
							?>
                            <input type="hidden" name="<?php echo 'way_of_payment'.$result['charge_id'];?>" id="<?php echo 'way_of_payment'.$result['charge_id'];?>" value="Cash" />
                            <?php
						}
						?>
						<?php /*?><a class="del_txt" id="anc_<?php echo $result['per_charge_amount'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php */?></div>
                        </div>
						<?php
						$i++;
					}
				}
				?>
                <?php
                //=======================================
                
					//$result['charge_id'] = 7;// 7 is for duty charges.
					//$result['charge_name'] =  "Duty Charges";
						?>
						<?php /*?><div style="height:89px;clear:left; border-top: 1px solid rgb(255, 255, 255); padding: 7px;" id="div_<?php echo $result['charge_id'];?>">
                        <label style="width:200px;float:left; visibility:hidden">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter per charge."  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>"  onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  value="0" style="width:100px; visibility:hidden" /> &nbsp;&nbsp;
                        <label style="width:200px;float:left; visibility:hidden">No. of  <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter no." onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  class="formtxtfield" type="text" name="txtno_<?php echo $result['charge_id'];?>" id="txtno_<?php echo $result['charge_id'];?>" value="0" style="width:100px; visibility:hidden" /> &nbsp;&nbsp;
                        <label style="width:200px;float:left">Total <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="total value"  class="formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" value="<?php echo $duty_charge;?>" style="width:100px" readonly="readonly" /> 
                        <span style="margin-top:15px;">
                       <!-- <br /><br />-->
						<label style="width:200px;float:left">Select Payment Way &nbsp;&nbsp;</label>
						<div class="dropmenu" >
                      	    	<div class="styled-select">
						<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
                        		</div>
                        </div>
                        
                        <span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
                        <label style="width:200px;float:left">Select Account &nbsp;&nbsp;</label>
                        <div class="dropmenu">
                            <div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
                              
                            </div>
                          </div>
                        </span>
                        </span>
						<?php /*?><a class="del_txt" id="anc_<?php echo $result['charge_id'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php * /?></div><?php */?>
						<?php
						//$i++;
					
				
                //=======================================
                ?>
				<input type="hidden" name="count_txt" id="count_txt" value="<?php echo $i;?>" />
                <br />
                <label id="lbl_pur" style='float:left'>Purchase = 0 &nbsp;&nbsp;</label>  
                <label id="lbl_sales" style='float:left'>Sales = 0 &nbsp;&nbsp;</label> 
                <label id="lbl_profit" style='float:left'>Profit = 0 OMR &nbsp;&nbsp;</label>
                <?php
				
			}
	}
   
   public function getAutoSearch(){
		// Data could be pulled from a DB or other source
		
		$loc = $this->jobs->getAllLocations();
		// Cleaning up the term
		$term = trim(strip_tags($_GET['term']));
		// Rudimentary search
		$matches = array();
		foreach($loc as $eachloc){
			if(stripos($eachloc['loc'], $term) !== false){
				// Add the necessary "value" and "label" fields and append to result set
				$eachloc['value'] = $eachloc['loc'];
				$eachloc['label'] = "{$eachloc['loc']}";
				$matches[] = $eachloc;
			}
		}
		// Truncate, encode and return the results
		$matches = array_slice($matches, 0, 5);
		print json_encode($matches); 
   }
   
   
   public function getAutoSupplier(){
		// Data could be pulled from a DB or other source
		$term = trim(strip_tags($_GET['term']));
		$suppliers = $this->jobs->getSupplierData();
		foreach($suppliers as $eachloc){
			if(stripos($eachloc['sup'], $term) !== false){
				// Add the necessary "value" and "label" fields and append to result set
				$eachloc['value'] = $eachloc['sup'];
				$eachloc['label'] = "{$eachloc['sup']}";
				$matches[] = $eachloc;
			}
		}
		// Truncate, encode and return the results
		$matches = array_slice($matches, 0, 5);
		print json_encode($matches); 
   }
   
   public function getAllBankAccounts()
   {
	   //$id = $this->input->post('id');
	   $res = $this->jobs->LoadAllBankAccount();
	   ?>
       <select id="account_id" name="account_id" onchange="" >
       	   <option value="">Select Bank Account</option>
		   <?php
           foreach($res as $eachRes){
               ?>
               <option value="<?php echo $eachRes->account_id;?>"><?php echo $eachRes->account_name;?></option>
               <?php
           }
           ?>
       </select>
       <?php
	   
   }
   
   
   
   
   
   
   
   
   //----------------------------------------------------------------------
	/**
	 * 
	 * Enter description here ...
	 * Created by M.Ahmed
	 */   
	
	
	/***
	 * 
	 */
	 
	function getWilayat_id()
	{
		
		$name = $this->input->post('name');
		$value = $this->input->post('value');
		$region_id  = $this->input->post('region_id');
		$language = $this->input->post('language');	
		//$this->db->select('wilayat_arabic_name,wilayat_name,wilayat_id');
		//$this->db->from('an_wilayats');		
		//$this->db->where('region_id',$region_id);
		//$query = $this->db->get();
		
		$sql = "select wilayat_arabic_name,wilayat_name,wilayat_id from an_wilayats where region_id = $region_id ";
	
		$query = $this->db->query($sql);
		
		
		$dropdown = '<select name="'.$name.'" id="'.$name.'">';
		if($language=='english')
			$dropdown .= '<option value="">Select Wilayat</option>';
		else
			$dropdown .= '<option value="">اختر ولاية</option>';
		
			foreach($query->result() as $row)
			{
				if($language=='english')
					$dropdown .= '<option data-serial="'.$row->wilayat_name.'" value="'.$row->wilayat_id.'" ';
				else
					$dropdown .= '<option data-serial="'.$row->wilayat_arabic_name.'" value="'.$row->wilayat_id.'" ';
				if($value==$row->wilayat_id)
				{
					$dropdown .= 'selected="selected"';
				}
				if($language=='english')
					$dropdown .= '>'.$row->wilayat_name.'</option>';
				else
					$dropdown .= '>'.$row->wilayat_arabic_name.'</option>';
			}
		
		$dropdown .= '</select>';
		echo($dropdown);
	} 
	
	
	function get_all_employees(){
		$branch_id = $this->input->post('branch_id');
		$name = 'driver_id';//$this->input->post('name');
		$value = $this->input->post('value');
		$language = 'english';
		$res = $this->jobs->get_all_employees($branch_id);
		
		$dropdown = '<select name="'.$name.'" id="'.$name.'">';
		if($language=='english')
			$dropdown .= '<option value="">Select Driver</option>';
		else
			$dropdown .= '<option value=""></option>';
		
			foreach($res as $row)
			{
				if($language=='english')
					$dropdown .= '<option data-serial="'.$row->fullname.'" value="'.$row->userid.'" ';
				else
					$dropdown .= '<option data-serial="'.$row->fullname.'" value="'.$row->userid.'" ';
				if($value==$row->userid)
				{
					$dropdown .= 'selected="selected"';
				}
				if($language=='english')
					$dropdown .= '>'.$row->fullname.'</option>';
				else
					$dropdown .= '>'.$row->fullname.'</option>';
			}
		
		$dropdown .= '</select>';
		echo($dropdown);
		
	}
	
	public function loadEmailView()
	{
		$this->_data['customer_id'] = $this->input->post('customer_id');
		$this->_data['duty_charges'] = $this->input->post('duty_charges');
		$this->load->view('emailView',	$this->_data);
	}
	
	public function sendEmail()
	{
		//echo "<pre>";
		//print_r($_FILES);
		//exit;
		$customer_id = $this->input->post('customer_id');
		$duty_charges = $this->input->post('duty_charges');
		
		$txt_heading = $this->input->post('txt_heading');
		$txt_detail = $this->input->post('txt_detail');
		$res = $this->jobs->get_customer_by_id($customer_id);
		$email = $res->email;
		$config = Array(
					  'protocol' => 'smtp',
					  'smtp_host' => 'ssl://smtp.googlemail.com',
					  'smtp_port' => 465,
					  'smtp_user' => 'alnowras@gmail.com', // change it to yours
					  'smtp_pass' => 'alnowras', // change it to yours
					  'mailtype' => 'html',
					  'charset' => 'iso-8859-1',
					  'wordwrap' => TRUE
					);
					
		$message = $txt_detail.' \r\n';
		$message .= 'Duty Charges '.$duty_charges;
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('alnowras@gmail.com'); // change it to yours
		$this->email->to($email);// change it to yours
		$this->email->subject($txt_heading);
		$this->email->message($message);
		if($this->email->send())
		{
			//update column for emial send.
			//$this->jobs->updateEmailSend($job_id);
			//echo 'Email sent.';
		}
		else
		{
			//show_error($this->email->print_debugger());
		}
		return true;
	}
	 
	//----------------------------------------------------------------------
	/**
	 * 
	 * Enter description here ...
	 * Created by M.Ahmed
	 */   
	
	
	/***
	 * 
	 */
	public function delete_job()
	{
		$all_ids = $this->input->post('ids');
		
		
		if(!empty($all_ids))
		{
			
			$this->jobs->delete_jobs($all_ids);
			
			$this->session->set_flashdata('success', 'Record has been deleted.');
			
			redirect(base_url().'jobs/options');
			exit();
		}
		else 
		{
			$this->session->set_flashdata('error', 'There is No record Selected.');
			
			redirect(base_url().'jobs/options');
			exit();
		}
			
	}
//----------------------------------------------------------------------
}
?>