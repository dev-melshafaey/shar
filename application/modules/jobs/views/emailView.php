        <style>
		.white_content{
			left:25% !important;
			width:50% !important;
			background-color:#f2ad21 !important;
		}
		#demo-filesss img{width:100px !important; height:100px !important}
		</style>
        <script>
		 $('#drag-and').dmUploader({
        url: '<?php echo base_url();?>jobs/image_uploads_emails',
        dataType: 'json',
        allowedTypes: 'image/*',
        onInit: function(){
          $.danidemo.addLog('#demo-debugss', 'default', 'Plugin initialized correctly');
        },
        onBeforeUpload: function(id){
          $.danidemo.addLog('#demo-debugss', 'default', 'Starting the upload of #' + id);

          $.danidemo.updateFileStatus(id, 'default', 'Uploading...');
        },
        onNewFile: function(id, file,jobid){
          $.danidemo.addFile('#demo-filesss', id, file);

          /*** Begins Image preview loader ***/
          if (typeof FileReader !== "undefined"){
            
            var reader = new FileReader();

            // Last image added
            var img = $('#demo-filesss').find('.demo-image-preview').eq(0);

            reader.onload = function (e) {
              img.attr('src', e.target.result);
            }

            reader.readAsDataURL(file);

          } else {
            // Hide/Remove all Images if FileReader isn't supported
            $('#demo-filesss').find('.demo-image-preview').remove();
          }
          /*** Ends Image preview loader ***/

        },
        onComplete: function(){
          $.danidemo.addLog('#demo-debugss', 'default', 'All pending tranfers completed');
        },
        onUploadProgress: function(id, percent){
          var percentStr = percent + '%';

          $.danidemo.updateFileProgress(id, percentStr);
        },
        onUploadSuccess: function(id, data){
          $.danidemo.addLog('#demo-debugss', 'success', 'Upload of file #' + id + ' completed');

          $.danidemo.addLog('#demo-debugss', 'info', 'Server Response for file #' + id + ': ' + JSON.stringify(data));

          $.danidemo.updateFileStatus(id, 'success', 'Upload Complete');

          $.danidemo.updateFileProgress(id, '100%');
        },
        onUploadError: function(id, message){
          $.danidemo.updateFileStatus(id, 'error', message);

          $.danidemo.addLog('#demo-debugss', 'error', 'Failed to Upload file #' + id + ': ' + message);
        },
        onFileTypeError: function(file){
          $.danidemo.addLog('#demo-debugss', 'error', 'File \'' + file.name + '\' cannot be added: must be an image');
        },
        onFileSizeError: function(file){
          $.danidemo.addLog('#demo-debugss', 'error', 'File \'' + file.name + '\' cannot be added: size excess limit');
        },
        onFallbackMode: function(message){
          $.danidemo.addLog('#demo-debugss', 'info', 'Browser not supported(do something else here!): ' + message);
        }
      });
		</script>
        
        <div style="background:none repeat scroll 0 0 #f2ad21 !important; margin-bottom:34px;padding:10px 1%">
        <div class="title">
       	    <span>Send Email</span>
        </div>
        <div class="notion">* Please Understand Clearly All The Data Before Entering</div>
        <form id="forminfo" name="forminfo" method="post" enctype="multipart/form-data">
        <div class="form">
        <div class="raw form-group">
            <div class="form_title">Heading</div>
            <div class="form_field">
              <input type="text" id="txt_heading" name="txt_heading"  class="formtxtfield" />
            </div>
            <small id="err_heading" class="help-block" style="display:none">Please Enter Heading</small>
        </div>

        
        <div class="raw form-group">
            <div class="form_title">Detail</div>
            <div class="form_field">
                <textarea style="height:100px"  id="txt_detail" name="txt_detail" class="formtxtfield"></textarea>
            </div>
            <small id="err_detail" class="help-block" style="display:none">Please Enter Detail</small>
        </div>
        
        <div class="raw form-group">
            <div class="form_title">Attachment</div>
            <div class="form_field">
               
                <div id="drag-and" class="uploader">            
                                <div class="browser" id="div_attachments">
                                  <label>
                                    <span>Click to open the file Browser</span>
                                    <input type="file" name="files"  accept="image/*" title='Click to add Images'>
                                  </label>
                                </div>
                              </div>
                          	  <div class="panel-body demo-panel-files" id='demo-filesss'>
                      			<span class="demo-note">No Files have been selected/droped yet...</span>
                    	      </div>
                        </div>
                    </div>
            </div>
        </div>
        
        
        <input type="hidden" name="hidden_customer_id"  id="hidden_customer_id" value="<?php echo $customer_id;?>" />
        <input type="hidden" name="hidden_duty_charges" id="hidden_duty_charges" value="<?php echo $duty_charges;?>" />

                    <div class="raw" align="center">
            <input type="button" value="Send" class="submit_btn" style="width:105px; padding-left:32px;margin:15px 0px 19px 35px" onclick="sendEmailReady();" />
            <input type="button" value="Cancel" onclick="hidePopUP();" class="submit_btn" style="width:105px; padding-left:32px; margin:15px 0px 19px 35px" />
            </div>
            </div>
        </form>
        </div>
       