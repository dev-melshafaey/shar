<!DOCTYPE HTML>

<html>

<head>

  <!--<link rel="stylesheet" href="<?php //echo base_url();?>currency/docsupport/style.css">-->

  <link rel="stylesheet" href="<?php echo base_url();?>currency/docsupport/prism.css">

  <link rel="stylesheet" href="<?php echo base_url();?>currency/chosen.css">

  <!--<script src="jquery-1.7.1.min.js"></script>-->

  <script src="<?php echo base_url();?>currency/chosen.jquery.js" type="text/javascript"></script>

  <script src="<?php echo base_url();?>currency/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>

<script>

$(document).ready(function(){



	$('#btnConvert').click(function(){

		var myfrom = $('#currency-from option:selected' ).val();

		var myto = $('#currency-to option:selected' ).val();

		var currency_quantity = $('#currency-quantity').val();
		
		$.ajax({
				url:"http://www.google.com/finance/converter",
				type: 'GET',
				data:{from : myfrom, to: myto,a:currency_quantity},
				cache: false,
				success: function(data)
				{
					//alert(data);
					$("#convertertest").html(data);
					ccv = $("#currency_converter_result .bld").html();
					
					ccv = parseFloat(ccv);
					newvalus = ccv.toFixed(3);
					
					$('#freight_charges').val(newvalus);

					$('#freight_charges').trigger("change");

					
					hidePopUP();
				}
			});

			

	});

});

</script>

</head>

<body>
<?php
/*
$.ajax({url:"http://rate-exchange.appspot.com/currency?",

			type: "GET", 

			contentType: "application/json; charset=utf-8",	 

			data : {from : myfrom, to: myto}, 

			dataType: "jsonp", 

			success: function(json) { 	

				var scurr = json.from;

				var rates = json.rate;

				var newvalus = rates*currency_quantity;

				newvalus = newvalus.toFixed(3);

				$('#freight_charges').val(newvalus);

				$('#freight_charges').trigger("change");

				hidePopUP();

				//alert(newvalus);

			}, 

			error: function() 

			{ 

				alert("Erro fetching data!"); 

			} 

		});
*/
?>

<div class="wrappers">

    <div class="theme-demo last" id="blue">

    	<div class="currency-wrappers">

    		<p class="currency-headers">Currency Converter</p>

    		<div class="currency-content">

    			<div style="display:none" class="currency-error-container">

    				<span class="currency-error">Server temporary unvailabe</span>

    				<button class="currency-backBtn">New conversion</button>

   				</div><!-- End of currency-error-container -->

    			<div style="display:none;" class="currency-loading">

                    <span class="currency-loading-phrase">Loading...</span>

                    <img class="currency-loading-gif" src="img/loader.gif">

                </div><!-- End of currency-loading -->

    			<div style="display:none;" class="currency-back">

                    <table class="currency-back-table">

                        <tbody>

                            <tr>

                            	<td class="currency-result-from" colspan="3"></td>

                            </tr>

                            <tr>

                            	<td class="currency-equals" colspan="3">=</td>

                            </tr>

                            <tr>

                            	<td class="currency-result-to" colspan="3"></td>

                            </tr>

                            <tr>

                                <td class="currency-result-compare1"></td>

                                <td class="currency-back-gap"></td>

                                <td class="currency-result-compare2"></td>

                            </tr>

                        </tbody>

                    </table>

   					<button class="currency-backBtn">New conversion</button>

    			</div><!-- End of currency-back -->

    			<div class="currency-front">

                    <form class="currency-form">

                    <input type="text" placeholder="" value="100" name="currency-quantity" id="currency-quantity" class="currency-quantity">

                    <span class="currency-from-label">From</span>

    



                        <select id="currency-from" name="currency-from" class="currency-from">

                           <option value="AED">Arab Emirates Dirham (AED)</option>

                           <option value="AFN">Afghan Afghani (AFN)</option>

                           <option value="ALL">Albanian Lek (ALL)</option>

                           <option value="AMD">Armenian Dram (AMD)</option>

                           <option value="ANG">Neth. Antillean Guilder (ANG)</option>

                           <option value="AOA">Angolan Kwanza (AOA)</option>

                           <option value="ARS">Argentine Peso (ARS)</option>

                           <option value="AUD">Australian Dollar (A$)</option>

                           <option value="AWG">Aruban Florin (AWG)</option>

                           <option value="AZN">Azerbaijani Manat (AZN)</option>

                           <option value="BAM">Bosnia-Her. Convertible Mark (BAM)</option>

                           <option value="BBD">Barbadian Dollar (BBD)</option>

                           <option value="BDT">Bangladeshi Taka (BDT)</option>

                           <option value="BGN">Bulgarian Lev (BGN)</option>

                           <option value="BHD">Bahraini Dinar (BHD)</option>

                           <option value="BIF">Burundian Franc (BIF)</option>

                           <option value="BMD">Bermudan Dollar (BMD)</option>

                           <option value="BND">Brunei Dollar (BND)</option>

                           <option value="BOB">Bolivian Boliviano (BOB)</option>

                           <option value="BRL">Brazilian Real (R$)</option>

                           <option value="BSD">Bahamian Dollar (BSD)</option>

                           <option value="BTN">Bhutanese Ngultrum (BTN)</option>

                           <option value="BWP">Botswanan Pula (BWP)</option>

                           <option value="BYR">Belarusian Ruble (BYR)</option>

                           <option value="BZD">Belize Dollar (BZD)</option>

                           <option value="CAD">Canadian Dollar (CA$)</option>

                           <option value="CDF">Congolese Franc (CDF)</option>

                           <option value="CHF">Swiss Franc (CHF)</option>

                           <option value="CLP">Chilean Peso (CLP)</option>

                           <option value="CNY">Chinese Yuan (CN¥)</option>

                           <option value="COP">Colombian Peso (COP)</option>

                           <option value="CRC">Costa Rican Colón (CRC)</option>

                           <option value="CUP">Cuban Peso (CUP)</option>

                           <option value="CVE">Cape Verdean Escudo (CVE)</option>

                           <option value="CZK">Czech Republic Koruna (CZK)</option>

                           <option value="DEM">German Mark (DEM)</option>

                           <option value="DJF">Djiboutian Franc (DJF)</option>

                           <option value="DKK">Danish Krone (DKK)</option>

                           <option value="DOP">Dominican Peso (DOP)</option>

                           <option value="DZD">Algerian Dinar (DZD)</option>

                           <option value="EGP">Egyptian Pound (EGP)</option>

                           <option value="ERN">Eritrean Nakfa (ERN)</option>

                           <option value="ETB">Ethiopian Birr (ETB)</option>

                           <option value="EUR">Euro (€)</option>

                           <option value="FJD">Fijian Dollar (FJD)</option>

                           <option value="FKP">Falkland Islands Pound (FKP)</option>

                           <option value="GBP">British Pound Sterling (₤)</option>

                           <option value="GEL">Georgian Lari (GEL)</option>

                           <option value="GHS">Ghanaian Cedi (GHS)</option>

                           <option value="GIP">Gibraltar Pound (GIP)</option>

                           <option value="GMD">Gambian Dalasi (GMD)</option>

                           <option value="GNF">Guinean Franc (GNF)</option>

                           <option value="GTQ">Guatemalan Quetzal (GTQ)</option>

                           <option value="GYD">Guyanaese Dollar (GYD)</option>

                           <option value="HKD">Hong Kong Dollar (HK$)</option>

                           <option value="HNL">Honduran Lempira (HNL)</option>

                           <option value="HRK">Croatian Kuna (HRK)</option>

                           <option value="HUF">Hungarian Forint (HUF)</option>

                           <option value="HTG">Haitian Gourde (HTG)</option>

                           <option value="IDR">Indonesian Rupiah (IDR)</option>

                           <option value="ILS">Israeli New Sheqel (₪)</option>

                           <option value="INR">Indian Rupee (Rs.)</option>

                           <option value="IQD">Iraqi Dinar (IQD)</option>

                           <option value="IRR">Iranian Rial (IRR)</option>

                           <option value="ISK">Icelandic Króna (ISK)</option>

                           <option value="JMD">Jamaican Dollar (JMD)</option>

                           <option value="JOD">Jordanian Dinar (JOD)</option>

                           <option value="JPY">Japanese Yen (¥)</option>

                           <option value="KES">Kenyan Shilling (KES)</option>

                           <option value="KGS">Kyrgystani Som (KGS)</option>

                           <option value="KHR">Cambodian Riel (KHR)</option>

                           <option value="KMF">Comorian Franc (KMF)</option>

                           <option value="KPW">North Korean Won (KPW)</option>

                           <option value="KRW">South Korean Won (₩)</option>

                           <option value="KWD">Kuwaiti Dinar (KWD)</option>

                           <option value="KYD">Cayman Islands Dollar (KYD)</option>

                           <option value="KZT">Kazakhstani Tenge (KZT)</option>

                           <option value="LAK">Laotian Kip (LAK)</option>

                           <option value="LBP">Lebanese Pound (LBP)</option>

                           <option value="LKR">Sri Lankan Rupee (LKR)</option>

                           <option value="LRD">Liberian Dollar (LRD)</option>

                           <option value="LSL">Lesotho Loti (LSL)</option>

                           <option value="LTL">Lithuanian Litas (LTL)</option>

                           <option value="LYD">Libyan Dinar (LYD)</option>

                           <option value="MAD">Moroccan Dirham (MAD)</option>

                           <option value="MDL">Moldovan Leu (MDL)</option>

                           <option value="MGA">Malagasy Ariary (MGA)</option>

                           <option value="MKD">Macedonian Denar (MKD)</option>

                           <option value="MMK">Myanmar Kyat (MMK)</option>

                           <option value="MNT">Mongolian Tugrik (MNT)</option>

                           <option value="MOP">Macanese Pataca (MOP)</option>

                           <option value="MRO">Mauritanian Ouguiya (MRO)</option>

                           <option value="MUR">Mauritian Rupee (MUR)</option>

                           <option value="MVR">Maldivian Rufiyaa (MVR)</option>

                           <option value="MWK">Malawian Kwacha (MWK)</option>

                           <option value="MXN">Mexican Peso (MX$)</option>

                           <option value="MYR">Malaysian Ringgit (MYR)</option>

                           <option value="MZN">Mozambican Metical (MZN)</option>

                           <option value="NAD">Namibian Dollar (NAD)</option>

                           <option value="NGN">Nigerian Naira (NGN)</option>

                           <option value="NIO">Nicaraguan Córdoba (NIO)</option>

                           <option value="NOK">Norwegian Krone (NOK)</option>

                           <option value="NPR">Nepalese Rupee (NPR)</option>

                           <option value="NZD">New Zealand Dollar (NZ$)</option>

                           <option value="OMR">Omani Rial (OMR)</option>

                           <option value="PAB">Panamanian Balboa (PAB)</option>

                           <option value="PEN">Peruvian Nuevo Sol (PEN)</option>

                           <option value="PGK">Papua New Guinean Kina (PGK)</option>

                           <option value="PHP">Philippine Peso (Php)</option>

                           <option value="PKR">Pakistani Rupee (PKR)</option>

                           <option value="PLN">Polish Zloty (PLN)</option>

                           <option value="PYG">Paraguayan Guarani (PYG)</option>

                           <option value="QAR">Qatari Rial (QAR)</option>

                           <option value="RON">Romanian Leu (RON)</option>

                           <option value="RSD">Serbian Dinar (RSD)</option>

                           <option value="RUB">Russian Ruble (RUB)</option>

                           <option value="RWF">Rwandan Franc (RWF)</option>

                           <option value="SAR">Saudi Riyal (SAR)</option>

                           <option value="SBD">Solomon Islands Dollar (SBD)</option>

                           <option value="SCR">Seychellois Rupee (SCR)</option>

                           <option value="SDG">Sudanese Pound (SDG)</option>

                           <option value="SEK">Swedish Krona (SEK)</option>

                           <option value="SGD">Singapore Dollar (SGD)</option>

                           <option value="SHP">Saint Helena Pound (SHP)</option>

                           <option value="SLL">Sierra Leonean Leone (SLL)</option>

                           <option value="SOS">Somali Shilling (SOS)</option>

                           <option value="SRD">Surinamese Dollar (SRD)</option>

                           <option value="STD">São Tomé and Príncipe Dobra (STD)</option>

                           <option value="SVC">Salvadoran Colón (SVC)</option>

                           <option value="SYP">Syrian Pound (SYP)</option>

                           <option value="SZL">Swazi Lilangeni (SZL)</option>

                           <option value="THB">Thai Baht (฿)</option>

                           <option value="TJS">Tajikistani Somoni (TJS)</option>

                           <option value="TMT">Turkmenistani Manat (TMT)</option>

                           <option value="TND">Tunisian Dinar (TND)</option>

                           <option value="TOP">Tongan Paʻanga (TOP)</option>

                           <option value="TRY">Turkish Lira (TRY)</option>

                           <option value="TTD">Trinidad and Tobago Dollar (TTD)</option>

                           <option value="TWD">New Taiwan Dollar (NT$)</option>

                           <option value="TZS">Tanzanian Shilling (TZS)</option>

                           <option value="UAH">Ukrainian Hryvnia (UAH)</option>

                           <option value="UGX">Ugandan Shilling (UGX)</option>

                           <option value="USD" selected="">US Dollar ($)</option>

                           <option value="UYU">Uruguayan Peso (UYU)</option>

                           <option value="UZS">Uzbekistan Som (UZS)</option>

                           <option value="VEF">Venezuelan Bolívar (VEF)</option>

                           <option value="VND">Vietnamese Dong (₫)</option>

                           <option value="VUV">Vanuatu Vatu (VUV)</option>

                           <option value="WST">Samoan Tala (WST)</option>

                           <option value="XOF">CFA Franc BCEAO (CFA)</option>

                           <option value="XCD">East Caribbean Dollar (EC$)</option>

                           <option value="XDR">Special Drawing Rights (XDR)</option>

                           <option value="XPF">CFP Franc (CFPF)</option>

                           <option value="YER">Yemeni Rial (YER)</option>

                           <option value="ZAR">South African Rand (ZAR)</option>

                           <option value="ZMK">Zambian Kwacha (ZMK)</option>

                        </select>

                        <span class="currency-to-label">To</span>

                        <select name="currency-to" id="currency-to" class="currency-to">

                           

                           <option value="OMR">Omani Rial (OMR)</option>

                           

                        </select>



						<input style="float:left; margin-left:19px;" type="button" id="btnConvert" class="currency-convertBtn" value="Convert" />

    					<!--<button name="currency-convert" id="" >Convert</button>-->

                        <button  onclick="hidePopUP();" class="currency-convertBtn" name="currency-convert">Cancel</button>

                        </form>

                    </div><!-- End of currency-front -->

    			</div><!-- End of currency-content -->

    

    		</div><!-- End of currency-wrapper -->

    	</div>

	</div>





	

</body>

</html>

<style>

body

{

	/*background: url(../img/bg-body.gif) repeat 0 0;

	color: #333;

	font-family: arial,helvetica,sans-serif;

	font-size: 14px;

	line-height: 23px;

	margin: 0;

	min-height: 100%;

	padding: 0;

	position: relative;*/

}

#wrappers

{

	background: #fff;

	border: 1px solid #ebebeb;

	margin: 0 auto;

	width: 762px;

}

#headers

{

	display: block;

	min-height: 40px;

	overflow: hidden;

}

.anchors

{

	padding-top: 50px;

}

#social-container

{

	margin: 10px auto 20px auto;

}



h1

{

	font-family: 'Lustria',serif;

	font-size: 34px;

	font-weight: bold;

	font-weight: normal;

	padding-top: 20px;

}

h2

{

	border-bottom: 1px dashed #ebebeb;

}

h3

{

	font-size: 14px;

	padding: 5px 0;

}

h4

{

	line-height: 100%;

	margin: 0;

	padding: 0;

}



headers

{

	background-color: #1b1b1b;

	background-image: linear-gradient(to bottom,#222,#111);

	background-image: -moz-linear-gradient(top,#222,#111);

	background-image: -o-linear-gradient(top,#222,#111);

	background-image: -webkit-gradient(linear,0 0,0 100%,from(#222),to(#111));

	background-image: -webkit-linear-gradient(top,#222,#111);

	background-repeat: repeat-x;

	border: 1px solid #252525;

	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff222222',endColorstr='#ff111111',GradientType=0);

	left: 0;

	position: fixed;

	top: 0;

	width: 100%;

	z-index: 9999;

	zoom: 1;

}

ul.nav

{

	margin: 0;

	padding: 0;

	text-align: center;

}

ul.nav li

{

	display: inline;

	list-style: none;

	margin: 0;

	padding: 0;

}

ul.nav li a

{

	color: #999;

	line-height: 40px;

	padding: 0 10px;

	text-decoration: none;

	text-shadow: 0 -1px 0 rgba(0,0,0,0.25);

}

ul.nav li a:hover

{

	color: #fff;

	text-decoration: none;

}

ul.nav li a.active

{

	color: #fff;

}

hr

{

	border-bottom: 2px dashed #ebebeb;

	border-left: 0;

	border-right: 0;

	border-top: 0;

}

td.code

{

	padding-left: 50px;

	vertical-align: top;

}

.example

{

	margin: 50px 0;

}

.theme-demo

{

	float: left;

	/*margin: 0 20px 20px 0;*/

}

.last

{

	margin-right: 0!important;

}

#themes

{

	margin: 20px 0;

}

a

{

	color: #333;

	font-weight: bold;

	text-decoration: none;

}

table.browser-compatibility td

{

	font-size: 11px;

	padding: 0 20px 0 0;

	text-align: center;

}

table.options

{

	border-collapse: collapse;

	font-size: 12px;

	padding: 0;

	width: 100%;

}

table.options th

{

	padding: 8px 20px;

}

table.options td

{

	padding: 5px;

}

table.options th,table.options td

{

	border: 1px solid #d3d3d3;

}

table.options th

{

	background-color: #f5f5f5;

	background-image: linear-gradient(to bottom,#fff,#e6e6e6);

	background-image: -moz-linear-gradient(top,#fff,#e6e6e6);

	background-image: -o-linear-gradient(top,#fff,#e6e6e6);

	background-image: -webkit-gradient(linear,0 0,0 100%,from(#fff),to(#e6e6e6));

	background-image: -webkit-linear-gradient(top,#fff,#e6e6e6);

	background-repeat: repeat-x;

	border: 1px solid #ccc;

	color: #333;

	filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);

	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff',endColorstr='#ffe6e6e6',GradientType=0);

	font-size: 14px;

	font-weight: bold;

	text-align: left;

	text-shadow: 0 1px 1px #fff;

}

.clear-fix

{

	clear: both;

}

#example1,#example2,#example3,#default,#green,#blue,#lightblue,#orange,#red

{

	width: 223px;

}

.currency-content

{

	/*min-height: 220px;*/

	padding: 5px 10px;

}

.currency-wrappers

{

	background-clip: padding-box;

	background-color: #f4f5f6;

	border: 1px solid #e6e6e6;

	border-radius: 5px;

	box-shadow: inset 0 0 0 2px #fff;

	moz-background-clip: padding;

	moz-border-radius: 5px;

	moz-box-shadow: inset 0 0 0 2px #fff;

	webkit-background-clip: padding-box;

	webkit-border-radius: 5px;

	webkit-box-shadow: inset 0 0 0 2px #fff;

}

.currency-headers

{

	background-color: #f5f5f5;

	background-image: linear-gradient(to bottom,#fff,#e6e6e6);

	background-image: -moz-linear-gradient(top,#fff,#e6e6e6);

	background-image: -o-linear-gradient(top,#fff,#e6e6e6);

	background-image: -webkit-gradient(linear,0 0,0 100%,from(#fff),to(#e6e6e6));

	background-image: -webkit-linear-gradient(top,#fff,#e6e6e6);

	background-repeat: repeat-x;

	border-bottom: 1px solid #b3b3b3;

	border-top-left-radius: 5px;

	border-top-right-radius: 5px;

	box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);

	color: #333;

	filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);

	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff',endColorstr='#ffe6e6e6',GradientType=0);

	font-size: 18px;

	font-weight: bold;

	margin: 0 0 20px 0;

	moz-border-radius-topleft: 5px;

	moz-border-radius-topright: 5px;

	moz-box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);

	padding: 5px 0;

	text-align: center;

	text-shadow: 0 1px 1px #fff;

	webkit-border-top-left-radius: 5px;

	webkit-border-top-right-radius: 5px;

	webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);

}

.currency-form select

{

	width: 100%;

}

.currency-form input,.currency-form select

{

	border: 1px solid #e6e6e6;

	border-radius: 5px;

	color: #333;

	display: block;

	font-size: 11px;

	moz-border-radius: 5px;

	padding: 5px;

	webkit-border-radius: 5px;

}

.currency-form input

{

	margin: 0 auto;

	text-align: center;

}

.currency-form span

{

	display: block;

	font-size: 11px;

	font-weight: bold;

	padding: 10px 0 0 3px;

}

.currency-convertBtn,.currency-backBtn

{

	background-color: #f5f5f5;

	background-image: linear-gradient(to bottom,#fff,#e6e6e6);

	background-image: -moz-linear-gradient(top,#fff,#e6e6e6);

	background-image: -o-linear-gradient(top,#fff,#e6e6e6);

	background-image: -webkit-gradient(linear,0 0,0 100%,from(#fff),to(#e6e6e6));

	background-image: -webkit-linear-gradient(top,#fff,#e6e6e6);

	background-repeat: repeat-x;

	border: 1px solid #ccc;

	border-bottom-color: #b3b3b3;

	border-color: #e6e6e6 #e6e6e6 #bfbfbf;

	border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);

	border-radius: 5px;

	box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);

	color: #333;

	cursor: pointer;

	display: block;

	filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);

	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff',endColorstr='#ffe6e6e6',GradientType=0);

	font-size: 12px;

	font-weight: bold;

	line-height: 20px;

	margin: 15px auto;

	moz-border-radius: 5px;

	moz-box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);

	padding: 4px 12px;

	text-align: center;

	text-shadow: 0 1px 1px rgba(255,255,255,0.75);

	vertical-align: middle;

	webkit-border-radius: 5px;

	webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);

}

.currency-convertBtn[disabled]

{

	background-image: none;

	box-shadow: none;

	cursor: default;

	filter: alpha(opacity=65);

	moz-box-shadow: none;

	opacity: .65;

	webkit-box-shadow: none;

}

.currency-convertBtn:hover,.currency-convertBtn:focus,.currency-backBtn:hover,.currency-backBtn:focus

{

	background-position: 0 -15px;

	color: #333;

	moz-transition: background-position .1s linear;

	o-transition: background-position .1s linear;

	text-decoration: none;

	transition: background-position .1s linear;

	webkit-transition: background-position .1s linear;

}

.currency-convertBtn:focus,.currency-backBtn:focus

{

	outline: 5px auto -webkit-focus-ring-color;

	outline: thin dotted #333;

	outline-offset: -2px;

}

.currency-convertBtn:hover,.currency-convertBtn:focus,.currency-convertBtn:active,.currency-convertBtn.active,.currency-convertBtn.disabled,.currency-convertBtn[disabled]

{

	background-color: #d9d9d9;

	background-color: #e6e6e6;

	color: #333;

}

.currency-convertBtn:active,.currency-convertBtn.active

{

	background-color: #ccc \9;

}

.currency-footer

{

	padding-bottom: 3px;

	text-align: center;

}

.currency-footer a

{

	color: #b1b1b1;

	font-weight: normal;

	text-decoration: none;

}

.currency-loading-phrase,.currency-error,.currency-loading-gif

{

	display: block;

	margin: 0 auto;

	padding-bottom: 10px;

	text-align: center;

}

.currency-back-table

{

	line-height: 100%;

	margin: 0 auto;

	padding-top: 10px;

}

.currency-result-from,.currency-equals,.currency-result-to

{

	font-weight: bold;

	text-align: center;

}

.currency-result-compare1,.currency-back-gap,.currency-result-compare2

{

	font-size: 9px;

	padding-top: 30px;

	text-align: center;

}

#blue .currency-headers

{

	background-color: #006dcc;

	background-image: linear-gradient(to bottom,#08c,#04c);

	background-image: -moz-linear-gradient(top,#08c,#04c);

	background-image: -o-linear-gradient(top,#08c,#04c);

	background-image: -webkit-gradient(linear,0 0,0 100%,from(#08c),to(#04c));

	background-image: -webkit-linear-gradient(top,#08c,#04c);

	background-repeat: repeat-x;

	border: 1px solid #002a80;

	border-top-left-radius: 5px;

	border-top-right-radius: 5px;

	box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);

	color: #fff;

	filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);

	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#0088cc',endColorstr='#0044cc',GradientType=0);

	font-size: 18px;

	font-weight: bold;

	margin: 0 0 20px 0;

	moz-border-radius-topleft: 5px;

	moz-border-radius-topright: 5px;

	moz-box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);

	padding: 5px 0;

	text-align: center;

	text-shadow: 0 -1px 0 rgba(0,0,0,0.25);

	webkit-border-top-left-radius: 5px;

	webkit-border-top-right-radius: 5px;

	webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);

}

#blue .currency-result-from,#blue .currency-result-to

{

	color: #04c;

	font-weight: bold;

	text-align: center;

}

#blue .currency-convertBtn,#blue .currency-backBtn

{

	background-color: #006dcc;

	background-image: linear-gradient(to bottom,#08c,#04c);

	background-image: -moz-linear-gradient(top,#08c,#04c);

	background-image: -o-linear-gradient(top,#08c,#04c);

	background-image: -webkit-gradient(linear,0 0,0 100%,from(#08c),to(#04c));

	background-image: -webkit-linear-gradient(top,#08c,#04c);

	background-repeat: repeat-x;

	border-color: #04c #04c #002a80;

	border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);

	border-radius: 5px;

	color: #fff;

	cursor: pointer;

	display: block;

	filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);

	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc',endColorstr='#ff0044cc',GradientType=0);

	font-size: 12px;

	font-weight: bold;

	line-height: 20px;

	margin: 15px auto;

	moz-border-radius: 5px;

	padding: 4px 12px;

	text-align: center;

	text-shadow: 0 -1px 0 rgba(0,0,0,0.25);

	vertical-align: middle;

	webkit-border-radius: 5px;

}

#blue .currency-convertBtn[disabled]

{

	background-color: #04c;

	background-image: none;

	box-shadow: none;

	color: #fff;

	cursor: default;

	filter: alpha(opacity=65);

	moz-box-shadow: none;

	opacity: .65;

	webkit-box-shadow: none;

}

#blue .currency-form span

{

	color: #04c;

	display: block;

	font-size: 11px;

	font-weight: bold;

	padding: 10px 0 0 3px;

}



#lightblue .currency-headers

{

	background-color: #49afcd;

	background-image: linear-gradient(to bottom,#5bc0de,#2f96b4);

	background-image: -moz-linear-gradient(top,#5bc0de,#2f96b4);

	background-image: -o-linear-gradient(top,#5bc0de,#2f96b4);

	background-image: -webkit-gradient(linear,0 0,0 100%,from(#5bc0de),to(#2f96b4));

	background-image: -webkit-linear-gradient(top,#5bc0de,#2f96b4);

	background-repeat: repeat-x;

	border: 1px solid #1f6377;

	border-top-left-radius: 5px;

	border-top-right-radius: 5px;

	box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);

	color: #fff;

	filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);

	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de',endColorstr='#ff2f96b4',GradientType=0);

	font-size: 18px;

	font-weight: bold;

	margin: 0 0 20px 0;

	moz-border-radius-topleft: 5px;

	moz-border-radius-topright: 5px;

	moz-box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);

	padding: 5px 0;

	text-align: center;

	text-shadow: 0 -1px 0 rgba(0,0,0,0.25);

	webkit-border-top-left-radius: 5px;

	webkit-border-top-right-radius: 5px;

	webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);

}

#lightblue .currency-result-from,#lightblue .currency-result-to

{

	color: #49afcd;

	font-weight: bold;

	text-align: center;

}

#lightblue .currency-convertBtn,#lightblue .currency-backBtn

{

	background-color: #49afcd;

	background-image: linear-gradient(to bottom,#5bc0de,#2f96b4);

	background-image: -moz-linear-gradient(top,#5bc0de,#2f96b4);

	background-image: -o-linear-gradient(top,#5bc0de,#2f96b4);

	background-image: -webkit-gradient(linear,0 0,0 100%,from(#5bc0de),to(#2f96b4));

	background-image: -webkit-linear-gradient(top,#5bc0de,#2f96b4);

	background-repeat: repeat-x;

	border-color: #2f96b4 #2f96b4 #1f6377;

	border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);

	border-radius: 5px;

	color: #fff;

	cursor: pointer;

	display: block;

	filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);

	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de',endColorstr='#ff2f96b4',GradientType=0);

	font-size: 12px;

	font-weight: bold;

	line-height: 20px;

	margin: 15px auto;

	moz-border-radius: 5px;

	padding: 4px 12px;

	text-align: center;

	text-shadow: 0 -1px 0 rgba(0,0,0,0.25);

	vertical-align: middle;

	webkit-border-radius: 5px;

}

#lightblue .currency-convertBtn[disabled]

{

	background-color: #2a85a0;

	background-color: #2f96b4;

	background-image: none;

	box-shadow: none;

	color: #fff;

	cursor: default;

	filter: alpha(opacity=65);

	moz-box-shadow: none;

	opacity: .65;

	webkit-box-shadow: none;

}

#lightblue .currency-form span

{

	color: #49afcd;

	display: block;

	font-size: 11px;

	font-weight: bold;

	padding: 10px 0 0 3px;

}



.syntaxhighlighter a,.syntaxhighlighter div,.syntaxhighlighter code,.syntaxhighlighter table,.syntaxhighlighter table td,.syntaxhighlighter table tr,.syntaxhighlighter table tbody,.syntaxhighlighter table thead,.syntaxhighlighter table caption,.syntaxhighlighter textarea

{

	background: none!important;

	border: 0!important;

	bottom: auto!important;

	box-sizing: content-box!important;

	float: none!important;

	font-family: "Consolas","Bitstream Vera Sans Mono","Courier New",Courier,monospace!important;

	font-size: 1em!important;

	font-style: normal!important;

	font-weight: normal!important;

	height: auto!important;

	left: auto!important;

	line-height: 1.1em!important;

	margin: 0!important;

	min-height: auto!important;

	min-height: inherit!important;

	moz-border-radius: 0 0 0 0!important;

	outline: 0!important;

	overflow: visible!important;

	padding: 0!important;

	position: static!important;

	right: auto!important;

	text-align: left!important;

	top: auto!important;

	vertical-align: baseline!important;

	webkit-border-radius: 0 0 0 0!important;

	width: auto!important;

}

.syntaxhighlighter

{

	background: #f7f7f7;

	border: 1px solid #ebebeb;

	font-size: 11px!important;

	margin: 1em 0 1em 0!important;

	padding: 5px 0;

	position: relative!important;

	width: 100%!important;

}

.syntaxhighlighter.source

{

	overflow: hidden!important;

}

.syntaxhighlighter .bold

{

	font-weight: bold!important;

}

.syntaxhighlighter .italic

{

	font-style: italic!important;

}

.syntaxhighlighter .line

{

	white-space: pre!important;

}

.syntaxhighlighter table

{

	width: 100%!important;

}

.syntaxhighlighter table caption

{

	padding: .5em 0 .5em 1em!important;

	text-align: left!important;

}

.syntaxhighlighter table td.code

{

	width: 100%!important;

}

.syntaxhighlighter table td.code .container

{

	position: relative!important;

}

.syntaxhighlighter table td.code .container textarea

{

	background: white!important;

	border: none!important;

	box-sizing: border-box!important;

	height: 100%!important;

	left: 0!important;

	overflow: hidden!important;

	padding-left: 1em!important;

	position: absolute!important;

	top: 0!important;

	white-space: pre!important;

	width: 100%!important;

}

.syntaxhighlighter table td.gutter .line

{

	padding: 0 .5em 0 1em!important;

	text-align: right!important;

}

.syntaxhighlighter table td.code .line

{

	padding: 0 1em!important;

}

.syntaxhighlighter.nogutter td.code .container textarea,.syntaxhighlighter.nogutter td.code .line

{

	padding-left: 0!important;

}

.syntaxhighlighter.show

{

	display: block!important;

}

.syntaxhighlighter.collapsed table

{

	display: none!important;

}

.syntaxhighlighter.collapsed .toolbar

{

	font-size: 1em!important;

	height: auto!important;

	padding: .1em .8em 0 .8em!important;

	position: static!important;

	width: auto!important;

}

.syntaxhighlighter.collapsed .toolbar span

{

	margin-right: 1em!important;

}

.syntaxhighlighter.collapsed .toolbar span a

{

	display: none!important;

	padding: 0!important;

}

.syntaxhighlighter.collapsed .toolbar span a.expandSource

{

	display: inline!important;

}

.syntaxhighlighter .toolbar

{

	display: none!important;

	font-size: 10px!important;

	height: 11px!important;

	position: absolute!important;

	right: 1px!important;

	top: 1px!important;

	width: 11px!important;

	z-index: 10!important;

}

.syntaxhighlighter .toolbar span.title

{

	display: inline!important;

}

.syntaxhighlighter .toolbar a

{

	display: block!important;

	padding-top: 1px!important;

	text-align: center!important;

	text-decoration: none!important;

}

.syntaxhighlighter .toolbar a.expandSource

{

	display: none!important;

}

.syntaxhighlighter.ie

{

	font-size: .9em!important;

	padding: 1px 0 1px 0!important;

}

.syntaxhighlighter.ie .toolbar

{

	line-height: 8px!important;

}

.syntaxhighlighter.ie .toolbar a

{

	padding-top: 0!important;

}

.syntaxhighlighter.printing .line.alt1 .content,.syntaxhighlighter.printing .line.alt2 .content,.syntaxhighlighter.printing .line.highlighted .number,.syntaxhighlighter.printing .line.highlighted.alt1 .content,.syntaxhighlighter.printing .line.highlighted.alt2 .content

{

	background: none!important;

}

.syntaxhighlighter.printing .line .number

{

	color: #bbb!important;

}

.syntaxhighlighter.printing .line .content

{

	color: black!important;

}

.syntaxhighlighter.printing .toolbar

{

	display: none!important;

}

.syntaxhighlighter.printing a

{

	text-decoration: none!important;

}

.syntaxhighlighter.printing .plain,.syntaxhighlighter.printing .plain a

{

	color: black!important;

}

.syntaxhighlighter.printing .comments,.syntaxhighlighter.printing .comments a

{

	color: #008200!important;

}

.syntaxhighlighter.printing .string,.syntaxhighlighter.printing .string a

{

	color: blue!important;

}

.syntaxhighlighter.printing .keyword

{

	color: #069!important;

	font-weight: bold!important;

}

.syntaxhighlighter.printing .preprocessor

{

	color: gray!important;

}

.syntaxhighlighter.printing .variable

{

	color: #a70!important;

}

.syntaxhighlighter.printing .value

{

	color: #090!important;

}

.syntaxhighlighter.printing .functions

{

	color: #ff1493!important;

}

.syntaxhighlighter.printing .constants

{

	color: #06c!important;

}

.syntaxhighlighter.printing .script

{

	font-weight: bold!important;

}

.syntaxhighlighter.printing .color1,.syntaxhighlighter.printing .color1 a

{

	color: gray!important;

}

.syntaxhighlighter.printing .color2,.syntaxhighlighter.printing .color2 a

{

	color: #ff1493!important;

}

.syntaxhighlighter.printing .color3,.syntaxhighlighter.printing .color3 a

{



	color: red!important;

}

.syntaxhighlighter.printing .break,.syntaxhighlighter.printing .break a

{

	color: black!important;

}

.syntaxhighlighter

{

	background-color: white!important;

}

.syntaxhighlighter .line.alt1

{

	background-color: white!important;

}

.syntaxhighlighter .line.alt2

{

	background-color: white!important;

}

.syntaxhighlighter .line.highlighted.alt1,.syntaxhighlighter .line.highlighted.alt2

{

	background-color: #e0e0e0!important;

}

.syntaxhighlighter .line.highlighted.number

{

	color: black!important;

}

.syntaxhighlighter table caption

{

	color: black!important;

}

.syntaxhighlighter .gutter

{

	color: #afafaf!important;

}

.syntaxhighlighter .gutter .line

{

	border-right: 3px solid #6ce26c!important;

}

.syntaxhighlighter .gutter .line.highlighted

{

	background-color: #6ce26c!important;

	color: white!important;

}

.syntaxhighlighter.printing .line .content

{

	border: none!important;

}

.syntaxhighlighter.collapsed

{

	overflow: visible!important;

}

.syntaxhighlighter.collapsed .toolbar

{

	background: white!important;

	border: 1px solid #6ce26c!important;

	color: blue!important;

}

.syntaxhighlighter.collapsed .toolbar a

{

	color: blue!important;

}

.syntaxhighlighter.collapsed .toolbar a:hover

{

	color: red!important;

}

.syntaxhighlighter .toolbar

{

	background: #6ce26c!important;

	border: none!important;

	color: white!important;

}

.syntaxhighlighter .toolbar a

{

	color: white!important;

}

.syntaxhighlighter .toolbar a:hover

{

	color: black!important;

}

.syntaxhighlighter .plain,.syntaxhighlighter .plain a

{

	color: black!important;

}

.syntaxhighlighter .comments,.syntaxhighlighter .comments a

{

	color: #008200!important;

}

.syntaxhighlighter .string,.syntaxhighlighter .string a

{

	color: blue!important;

}

.syntaxhighlighter .keyword

{

	color: #069!important;

}

.syntaxhighlighter .preprocessor

{

	color: gray!important;

}

.syntaxhighlighter .variable

{

	color: #a70!important;

}

.syntaxhighlighter .value

{

	color: #090!important;

}

.syntaxhighlighter .functions

{

	color: #ff1493!important;

}

.syntaxhighlighter .constants

{

	color: #06c!important;

}

.syntaxhighlighter .script

{

	background-color: none!important;

	color: #069!important;

	font-weight: bold!important;

}

.syntaxhighlighter .color1,.syntaxhighlighter .color1 a

{

	color: gray!important;

}

.syntaxhighlighter .color2,.syntaxhighlighter .color2 a

{

	color: #ff1493!important;

}

.syntaxhighlighter .color3,.syntaxhighlighter .color3 a

{

	color: red!important;

}

.syntaxhighlighter .keyword

{

	font-weight: bold!important;

}

.closes{

	right:0;

	top:0;

	position:absolute;

}

</style>