<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
      <div class="title"> <span><?php breadcramb(); ?></span> </div>
      <?php error_hander($this->input->get('e')); ?>
      <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">
        <div class="form">
          <div class="CSSTableGenerator " id="printdiv" >
          	<div style="float: left; margin-left: 12px; margin-top: 4px;">
            From Date 
            <input type="text" id="from_date_filter"> 
            To Date 
            <input type="text" id="to_date_filter" >
            <input type="button" onclick="updateExpireJobsDataTableDiv();" value="Search"></div>
              
            <table width="100%" align="left" id="usertable">
              <thead>
              <tr>

                <th width="1%" id="no_filter"><label for="checkbox"></label></th>
                <th width="7%">Job Id</th>
                <th width="15%">Way Of Shipment</th>
                <th width="20%">Customer Name</th>
                <th width="20%">Shipment Standards</th>
                <th width="12%" >Status</th>
                <th width="12%">Duty Charges</th>
                <th width="5%">Total value</th>
                <th width="7%" id="no_filter">&nbsp;</th>
              </tr>
              </thead>
              <tfoot>
              <tr>
                <td  style="background-color:#afe2ee"><input type="checkbox" name="checkbox" id="checkboxall" /></td>
                <td  style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
                <td  style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
                <td  style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield2" id="textfield2"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield4" id="textfield4"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"></td>
              </tr>
              </tfoot>
              <?php
			  //echo "<pre>";
			  //print_r($this->jobs->getjobsLists());
			  
			  	$cnt = 0;
              	foreach($this->jobs->getExpirejobsLists() as $jobdata) {
					$cnt++;
					
			  ?>
              <tr>
                <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $jobdata->job_id; ?>" value="<?php echo $jobdata->job_id; ?>" /></td>
                <td><?php echo '0000'.$jobdata->job_id; ?></td>
                <td><?php echo ucfirst($jobdata->way_of_shipment); ?></td>
                <td><?php echo ucfirst($jobdata->customer_name); ?></td>
                <td><?php echo $jobdata->shipment_standard; ?></td>
                <td><?php if($jobdata->job_status=='waiting_for_customer'){echo 'Waiting For Customer';}else{echo ucfirst($jobdata->job_status);} ?></td>
               	<td><?php echo ($jobdata->duty_charges); ?></td>
               	<td><?php echo ($jobdata->total_value)?$jobdata->total_value:0; ?></td>
                <td><?php edit_button('add_jobs/'.$jobdata->job_id); ?> <a id="<?php echo $jobdata->job_id; ?>" href='#' class='basic'><img src="<?php echo base_url(); ?>images/internal/i_info.png" width="28" height="18" border="0" /></a>          
                  <!-- modal content -->
                  
                  <div id="basic-modal-content" class="dig<?php echo $jobdata->job_id; ?>">
                    <h3><?php echo $jobdata->customer_name; ?><br />
                      Shipment Standard : #<?php echo $jobdata->shipment_standard ?></h3>
                    
                    <code> <span class="pop_title">Shipment Standard : </span>
                    <div class="pop_txt"><?php echo $jobdata->shipment_standard; ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Way Of Shipment : </span>
                    <div class="pop_txt"><?php echo $jobdata->way_of_shipment; ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Status : </span>
                    <div class="pop_txt"><?php if($jobdata->job_status=='waiting_for_customer'){echo 'Waiting For Customer';}else{echo ucfirst($jobdata->job_status);} ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Duty Charges : </span>
                    <div class="pop_txt"><?php echo $jobdata->duty_charges; ?></div>
                    </code>
                    <!--line--> 
                    <code> <span class="pop_title">Total Value : </span>
                    <div class="pop_txt"><?php echo isset($jobdata->total_value)?round($jobdata->total_value,2):0; ?></div>
                    </code> 
                   <!--line--> 
                    <code> <span class="pop_title">Customer Name : </span>
                    <div class="pop_txt"><?php echo $jobdata->customer_name; ?></div>
                    </code>
                    
                    <code> <span class="pop_title">Consignee Name : </span>
                    <div class="pop_txt"><?php echo $jobdata->consignee_name; ?></div>
                    </code>
                    
                    <code> <span class="pop_title">Created Date : </span>
                    <div class="pop_txt"><?php echo $jobdata->created; ?></div>
                    </code>
                    <code> <span class="pop_title">Email : </span>
                    <div class="pop_txt"><?php echo $jobdata->email; ?></div>
                    </code>
                    <?php
					if($jobdata->job_status=='waiting_for_customer'){
						?>
						<code> <span class="pop_title">Remarks Of Email : </span>
						<div class="pop_txt"><?php echo $jobdata->review; ?></div>
						</code>
						<?php
					}
					?>
                    
                    <code> <span class="pop_title">Created By : </span>
                    <div class="pop_txt"><?php echo $jobdata->employee_name; ?></div>
                    </code> 
                     <!--line--> 
                   
                   
                    	<?php
							//get_permission_text();
						?>
                      </ul>
                    </div>
                    
                    </td>
              </tr>
              <?php } ?>
            </table>
          </div>
        </div>
      
       
      </form>
    </div>
      <!-- END PAGE -->  
   </div>
</section>
<!-- End Section-->
<!--footer-->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(function() {
$( "#from_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to_date_filter" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from_date_filter" ).datepicker( "option", "maxDate", selectedDate );
}
});
});
</script>
<?php $this->load->view('common/footer');?>
