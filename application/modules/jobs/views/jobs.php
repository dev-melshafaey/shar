<?php $this->load->view('common/meta');?>
<!--body with bg-->
</script>
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>
<style>
tr.border_bottom td {
  border-bottom:1pt solid #859cb6 #335a85 !important;
}
</style>
<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
      <div class="title"> <span><?php breadcramb(); ?></span><input type="button" class="cancel_icon_up cnbtn" value=""/><input type="button" class="print_icon_up" value=""/>
<input type="button" class="delete_icon_up dlbtn" value=""/>
<input type="button" class="edit_icon_up edbtn" value=""/>
<input type="button" class="add_icon_up adbtn" value="" id="jobs/add_jobs"/>
<input type="button" id="btn_listing" style="font-weight: bold; font-size: 13px; border-top-width: 0px; border-bottom-width: 0px; border-left-width: 0px; height: 57px; cursor: pointer; float: right; border-radius: 5px;" class="" value="Generate Invoice"> </div>
      <?php error_hander($this->input->get('e')); ?>
      <form action="<?php echo base_url().'invoices/invoices/getAllInvoices'?>" id="listing" method="post" autocomplete="off">
        <div class="form" style="background:none;">
          <div class="CSSTableGenerator " id="printdiv" >
          	<div style="float: left; margin-left: 12px; margin-top: 4px;">
            From Date 
            <input type="text" id="from_date_filter"> 
            To Date 
            <input type="text" id="to_date_filter" >
            <input type="button" onclick="updateDataTableDiv();" value="Search"></div>
              
            <table width="100%" align="left" id="usertable">
              <thead>
              <tr>
                <th width="1%" id="no_filter"><label for="checkbox">Select All</label><input type="checkbox" name="selectall" id="selectall" /></th>
                <th width="7%">Job Id</th>
                <th width="15%">Way Of Shipment</th>
                <th width="20%">Customer Name</th>
                <th width="10%">Shipment Standards</th>
                <th width="12%" >Status</th>
                <th width="12%">Review</th>
                <th width="12%">Duty Charges</th>
                <th width="5%">Total Advance</th>
                <th width="5%">Total Purchase</th>
                <th width="5%">Total Sales</th>
                <th width="5%">Total Profit</th>
                <th width="5%">Total Paid</th>
				<th width="5%">Total Remaining</th>
                <th width="13%" id="no_filter">&nbsp;</th>
              </tr>
              </thead>
              <tfoot>
              <tr>
                <td  style="background-color:#afe2ee"><input type="checkbox" name="checkbox" id="checkboxall" /></td>
                <td  style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
                <td  style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
                <td  style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield2" id="textfield2"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield2" id="textfield2"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield4" id="textfield4"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
				
                <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                
                <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>                	
                
                <td style="background-color:#afe2ee"></td>
              </tr>
              </tfoot>
              <?php
			 // echo "<pre>";
			 // print_r($this->jobs->getjobsLists());
			  
			  	$cnt = 0;
              	foreach($this->jobs->getjobsLists($customer_id) as $jobdata) {
					$cnt++;
					
			  ?>
              <tr class="border_bottom">
				<input type="hidden" id="edit<?php echo $jobdata->job_id; ?>" value="<?php echo $jobdata->job_id.'?pr='.$jobdata->job_step_no ?>" />
                <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $jobdata->job_id; ?>" value="<?php echo $jobdata->job_id; ?>" /></td>
                <td><?php echo $jobdata->job_id; ?></td>
                <td><?php echo ucfirst($jobdata->way_of_shipment); ?></td>
                <td><?php echo ucfirst($jobdata->customer_name); ?></td>
                <td><?php echo $jobdata->shipment_standard; ?></td>
                <td><?php if($jobdata->job_status=='waiting_for_customer'){echo 'Waiting For Customer';}else{echo '';} ?></td>
                <td><?php if($jobdata->review_status=='waiting_for_customer'){echo 'Waiting For Customer';}else if($jobdata->review_status=='under_process'){echo 'Under Process';}else if($jobdata->review_status=='under_transportation'){echo 'Under Transportation';}else if($jobdata->review_status=='under_custom_clearance'){echo 'Under Custom Clearance';} ?></td>
               	<td><?php echo ($jobdata->duty_charges); ?></td>
               	<td><?php //echo ($jobdata->advance)?$jobdata->advance:0; ?></td>
                <td><?php $rese = $mythis->getJobTypevalue($jobdata->job_id);
						//	echo ($jobdata->purchase!='')? $jobdata->purchase/2:'0';
				?></td>
                <td><?php // echo ($jobdata->sales!='')?$jobdata->sales/2:0; ?></td>
                <td><?php //echo  $jobdata->sales/2-$jobdata->purchase/2; ?></td>
                <td><?php //if($jobdata->payment) echo  $jobdata->payment; else echo '0'; ?></td>
				<td><?php //if($jobdata->payment) $rem = $jobdata->sales-$jobdata->payment+$jobdata->advance; else $rem= $jobdata->sales-$jobdata->advance; echo $rem; ?></td>
                <td><?php edit_button('add_jobs/'.$jobdata->job_id); ?><a  href='<?php echo base_url() ?>jobs/pay/<?php echo $jobdata->job_id; ?>'>Pay</a> <a id="<?php echo $jobdata->job_id; ?>" href='#' class='basic'><img src="<?php echo base_url(); ?>images/internal/i_info.png" width="28" height="18" border="0" /></a>
                <?php          
                if($this->session->userdata('an_memtype')==1||$this->session->userdata('an_memtype')==2){
				?>
				<a href="<?php echo base_url();?>jobs/cancelJobWithId/<?php echo $jobdata->job_id; ?>" style="text-decoration:underline;">Cancel Job</a>
                  <?php
				}
				  ?>
				  <!-- modal content -->
                  
                  <div id="basic-modal-content" class="dig<?php echo $jobdata->job_id; ?>">
                    <h3><?php echo $jobdata->customer_name; ?><br />
                      Shipment Standard : #<?php echo $jobdata->shipment_standard ?></h3>
                    
                    <code> <span class="pop_title">Shipment Standard : </span>
                    <div class="pop_txt"><?php echo $jobdata->shipment_standard; ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Way Of Shipment : </span>
                    <div class="pop_txt"><?php echo $jobdata->way_of_shipment; ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Status : </span>
                    <div class="pop_txt"><?php if($jobdata->job_status=='waiting_for_customer'){echo 'Waiting For Customer';}else{echo ucfirst($jobdata->job_status);} ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Duty Charges : </span>
                    <div class="pop_txt"><?php echo $jobdata->duty_charges; ?></div>
                    </code>
                    <!--line--> 
                    <code> <span class="pop_title">Total Value : </span>
                    <div class="pop_txt"><?php echo isset($jobdata->total_value)?$jobdata->total_value:0; ?></div>
                    </code> 
                   <!--line--> 
                    <code> <span class="pop_title">Customer Name : </span>
                    <div class="pop_txt"><?php echo $jobdata->customer_name; ?></div>
                    </code>
                    
                    <code> <span class="pop_title">Consignee Name : </span>
                    <div class="pop_txt"><?php echo $jobdata->consignee_name; ?></div>
                    </code>
                    
                    <code> <span class="pop_title">Created Date : </span>
                    <div class="pop_txt"><?php echo $jobdata->created; ?></div>
                    </code>
                    <code> <span class="pop_title">Email : </span>
                    <div class="pop_txt"><?php echo $jobdata->email; ?></div>
                    </code>
                    <?php
					if($jobdata->job_status=='waiting_for_customer'){
						?>
						<code> <span class="pop_title">Remarks Of Email : </span>
						<div class="pop_txt"><?php echo $jobdata->review; ?></div>
						</code>
						<?php
					}
					?>
                    
                    <code> <span class="pop_title">Created By : </span>
                    <div class="pop_txt"><?php echo $jobdata->employee_name; ?></div>
                    </code> 
                     <!--line--> 
                   
                   
                    	<?php
							//get_permission_text();
						?>
                      </ul>
                    </div>
                    
                    </td>
              </tr>
              <?php } ?>
            </table>
            <?php
			if($this->session->userdata('an_memtype')==1 || $this->session->userdata('an_memtype')==2){
			?>
            
            <table style="">
                <?php
	   $arrs = $mythis->getTotalProfitAndLoss();
	   ?>
                     <tbody><tr>
                        <td style="width: 13%; padding:5px;">Total Jobs Profit</td><td style="width: 9%; padding:5px;"><?php echo round($arrs['profit'],2);?></td>
                        <td style="width: 12%; padding:5px;">Total Jobs Sales</td><td style="width: 9%; padding:5px;"><?php echo round($arrs['sales'],2);?></td>
                        <td style="width: 14%; padding:5px;">Total Jobs Purchase</td><td style="width: 9%; padding:5px;"><?php echo round($arrs['pur'],2);?></td>
                        
                     
                     </tr>
                </tbody></table>
                
                <?php
				
			}?>
          </div>
          
        </div>
      
       <input type="button" class="cancel_icon_bottom cnbtn" value=""/>
		<input type="button" class="print_icon_bottom pbtn" value=""/>
<input type="button" class="delete_icon_bottom dlbtn" value=""/>
<input type="button" class="edit_icon_bottom edbtn" value="" id="jobs/add_jobs" />
<input type="button" class="add_icon_bottom adbtn" value="" id="jobs/add_jobs"/>
<input type="button" id="btn_listing" style="font-weight: bold; font-size: 13px; border-top-width: 0px; border-bottom-width: 0px; border-left-width: 0px; height: 57px; cursor: pointer; float: right; border-radius: 5px; margin-top:10px" class="" value="Generate Invoice">
      </form>
    </div>
      <!-- END PAGE -->  
   </div>
</section>
<!-- End Section-->
<!--footer-->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(function() {

$( "#from_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to_date_filter" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from_date_filter" ).datepicker( "option", "maxDate", selectedDate );
}
});
/*$('#selectall').click(function(e) {
var table = $(e.target).parents('table:first');
$('td input:checkbox', table).attr('checked', e.target.checked);
});*/
/*$('#selectall').click(function(){
  var checked_status = this.checked;
  $(this).closest('table').find('input:checkbox').each(function(){
    this.checked = checked_status;
  });
})*/
/*$("#selectall").each(function() {
    // attach a click event on the checkbox in the header in each of table
    $(this).find("th:first input:checkbox").click(function() {
        var val = this.checked;
        // when clicked on a particular header, get the reference of the table & hence all the rows in that table, iterate over each of them.
        $(this).parents("table").find("tr").each(function() {
            // access the checkbox in the first column, and updates its value.
            $(this).find("td:first input:checkbox")[0].checked = val;
        });
    });
});*/
$('#selectall').change(function() {
                        var isSelected = $(this).is(':checked');
                        if(isSelected){
                            $('.allcb').prop('checked', true);   
                        }else{
                            $('.allcb').prop('checked', false);
                        }
                    });
					
					$('#btn_listing').click(function(){
						$('#listing').submit();
					})
					$('#btn_listing2').click(function(){
						$('#listing').submit();
					})
});
</script>
<?php $this->load->view('common/footer');?>
