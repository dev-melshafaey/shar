<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
    
    <script type="text/javascript">
    $(document).ready(function(){
		
			$("#payment_type").change(function(){
					val = $(this).val();
					//alert(val);
					if(val == 'accounts'){
						$("#account").show();
					}
					else{
							$("#account").hide();
					}
				})
		})
    </script>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/left-navigations');?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content" class="main_content">
    	<div class="title">
    		<span><a href="">Add Payment</a></span>
    		<span class="subtitle">New Payment</span>
    	</div>

		<form id="form_cashs" action="<?php echo base_url();?>jobs/add_payment" method="post" enctype="multipart/form-data">
			<div class="form" id="form-refresh">
          
          
               
                
                <?php
				//echo "<pre>";
				//print_r($payment);
				//print_r($remaing);
				?>
                <input type="hidden" value="<?php echo $remaing->job_id; ?>" id="job_id"  name="job_id"/> 
                <div class="raw form-group" id="div_charges">
                    <div class="form_title">Remaining</div>
                    <div class="form_field">
                        <?php  if(isset($payment) && isset($payment->payment)){ echo $remaiing = $remaing->sales-$payment->payment ; }else{ echo $remaiing = $remaing->sales; } ?>
                    </div>
                </div>
                
                
        
                <div class="raw form-group">
                    <div class="form_title">Value </div>
                    <div class="form_field">
                        <input name="payment_amount" id="payment_amount" type="text"  class="formtxtfield_small"/>
                        <span>RO.</span>
                    </div>
                </div>
                
                <div class="raw form-group">
                <div class="form_title">Payment Type</div>
                    <div class="form_field">
                        <div class="dropmenu">
                            <div class="styled-select">
                                <select name="payment_type" id="payment_type" onchange="toggleBranches();">
                                    <option value="">Select Payment Way</option>
                                    <option value="accounts">deposit</option>
                                    <option value="cash">Cash</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="raw form-group"  id="account" style="display:none;">
                <div class="form_title">Account number:</div>
                    <div class="form_field">
                            <div class="styled-select">
                                <input type="text" id="account_id" name="account_id" /> 
                              </div>
                       </div>
                </div>
                
                
                <?php /*?><div class="raw form-group" id="div_banks" style="display:none">
                    <div class="form_title">Banks </div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
                          <?php company_bank_dropbox('bank_id','','english',' onchange="getBankAccounts();" ');?>
                        </div>
                      </div>
                    </div>
                </div><?php */?>
                
                
                
               
                
<!--                <div class="raw form-group" id="div_checkque" style="display:none">
                    <div class="form_title">Deposite Recipt </div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
	                        <input type="file"  class="formtxtfield" name="deposite_recipt" id="deposite_recipt" />
                        </div>
                      </div>
                    </div>
                </div>-->
                
               
                
                
                
                
                <div class="raw">
                    <div class="form_title">Notes </div>
                    <div class="form_field">
                        <textarea name="notes" id="notes" cols="" rows="" class="formareafield"></textarea>
                    </div>
                </div>
                
                
                <div class="raw" align="center">
                    <input name="submit_cash" id="submit_cash" type="submit" class="submit_btn" value="Add" />
                    <!-- <input name="id" type="hidden"  value="" /> -->
                    
                </div>
                <!--end of raw-->
			</div>
		</form>
		<!-- END PAGE -->  
	</div>
    <!-- END PAGE -->  
</div>
</section>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
