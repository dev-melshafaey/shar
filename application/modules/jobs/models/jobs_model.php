<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Jobs_model extends CI_Model {
	
	/*
	* Properties
	*/
		
	private $_table_job;
//----------------------------------------------------------------------
    
	/*
	* Constructor
	*/
	
	function __construct()
    {
        parent::__construct();
		
		//Load Table Names from Config
		$this->_table_users						=	$this->config->item('table_users');
		$this->_table_banks						=	$this->config->item('table_banks');
		$this->_table_customers					=	$this->config->item('table_customers');
		$this->_table_modules					=	$this->config->item('table_modules');
		$this->_table_permissions				=	$this->config->item('table_permissions');
		$this->_table_companies					=	$this->config->item('table_companies');
		$this->_table_company_jobs			=	$this->config->item('table_company_jobs');
		$this->_table_users_bank_transactions	=	$this->config->item('table_users_bank_transactions');
    }
	
//----------------------------------------------------------------------
	function get_all_employees($branch_id='',$all="")
	{
		$sql = "SELECT 
				  * 
				FROM
				  an_users AS u ,
				  an_branch AS b ,
				  an_permission_type AS pt
				WHERE b.`branch_id` = u.`branchid`
				AND pt.`permission_type_id` = u.`member_type`
				AND u.`status` = 'A' 
				AND u.branchid='".$branch_id."' ";
		if($all==""){
			$sql .= " AND pt.`permissionhead` = 'Driver'; ";
		}
		$q = $this->db->query($sql);
		return $q->result();
		
	}
	
	
	
	function getChargesById($charges_id){
		
		
		$this->db->select('charge_id');
		$this->db->from('an_charges');
		$this->db->where('charge_name',$charges_id);
		$query = $this->db->get();
		if($query->num_rows() > 0){
				$returnData = $query->row();
				return $returnData->charge_id;
		}
		else{
			return false;	
		}
	}
	
	function getSearchChargesByName($name){
		$sql = "SELECT * FROM `an_charges` AS c WHERE c.`charge_name` LIKE '".$name."%'";
		$q = $this->db->query($sql);
		
		$returnData = $query->row();
		return $returnData->charge_id;
	}
	
	
	function getSupplierIdByName($name){
		
		$sql="SELECT s.`id` FROM `an_suppliers` AS s WHERE s.`supplier_name` = '".$name."'";	
		$q = $this->db->query($sql);
		$supplier =  $q->row();
		return $supplier->id;
	}
	function gePayemnts($jobId){
		$sql="SELECT    SUM(jp.`payment_amount`) AS payment FROM  `job_payment` AS jp 
WHERE  jp.`job_id` = ".$jobId."
GROUP BY jp.job_id";	
		$q = $this->db->query($sql);
		return $q->row();
	}
	function getRemainng($jobId){
		$sql = "SELECT   
jc.`job_id`,SUM(jc.`sales_value`) AS sales,
  SUM(jc.`purchase_value`) AS purchase,
  SUM(jc.`charges_advance`) AS advance FROM  `jobs_charges` AS jc 
WHERE  Jc.`job_id` = ".$jobId."
GROUP BY jc.`job_id`";
					
		$q = $this->db->query($sql);
		return $q->row();
		
	}
	/*
	* Update jobs
	* @param $data array
	* @param $job_id
	* return TRUE
	* Created M.Ahmed
	*/	
	
	function update_job($data,$job_id)
	{
		$res = $this->get_job_detail($job_id);
		//echo "<pre>";
		//print_r($data);
		//exit;
		$customer_id = $res->customer_id;
		$job_statuss = isset($data['job_status'])?$data['job_status']:$res->job_status;
		$duty_chares_for_job = $res->duty_charges;
		$payment_type = isset($data['way_of_payment'])?$data['way_of_payment']:$res->way_of_payment;
		$branches_id = $this->getCustomerBranch($customer_id);//$res->branch_id;
		$account_id = isset($data['account_id'])?$data['account_id']:'';
		unset($data['account_id']);
		//exit;
		$data['customer_id'] = $res->customer_id;
		
		
			$new_id = $data['customer_id'];
			if(isset($data['count_txt']) && $data['count_txt'] > 0){
				$res = $this->customers->get_customer_detail($res->customer_id);
				$data['account_type'] = $res->account_type;
				
			for($i=0;$i<=$data['count_txt'];$i++){
				if($data['account_type'] == 'fixed'){
					$getdata = array();
					
					if(isset($data['txt_'.$i]) && isset($data['txtno_'.$i]) && isset($data['txttotal_'.$i]) && isset($data['txtpur_'.$i]) ){	
						
						//$new_ids = $this->getLastJobId();
						//$getdata['job_id'] = ($new_ids->Auto_increment);
						
						$getdata['job_id'] = $job_id;
						$getdata['customer_id'] = $new_id;
						$getdata['charge_id'] =  $i;
						//$getdata['charge_value'] = $data['txt_'.$i] ;
						$getdata['per_charge_amount'] = isset($data['txt_'.$i])?$data['txt_'.$i]:'' ;
						$getdata['charge_quaintity'] = isset($data['txtno_'.$i])?$data['txtno_'.$i]:'';
						$getdata['total_charge'] = isset($data['txttotal_'.$i])?$data['txttotal_'.$i]:'' ;
						$getdata['pur_charge'] = isset($data['txtpur_'.$i])?$data['txtpur_'.$i]:'' ;
						//$getdata['created'] = date('Y-m-d H:i:s');
						//$getdata['created_by'] = $this->session->userdata('userid');
						//$port_data['customer_id'] = $getdata['customer_id'];
						//$port_data['created'] = date('Y-m-d H:i:s');
						
						//$this->db->insert('an_customer_port_selection',$port_data);
						$sql = "select * from an_fixed_job_charges where job_id = '$job_id' and customer_id = '$new_id' and charge_id = '$i' ";
						
						$q = $this->db->query($sql);
						if($q->num_rows() > 0)
						{
							$this->db->where('job_id', $job_id);
							$this->db->where('customer_id', $new_id);
							$this->db->where('charge_id', $i);
							$this->db->update('an_fixed_job_charges', $getdata);
						}else{
							$this->db->insert('an_fixed_job_charges',$getdata);
						}
						
					//==================payment start branch cash=========================================
					
						
					if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Cash'){	
						$this->UpdateBranchCash($branches_id,$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_fixed_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_cash_management',$companyDate);
					}
    				
					//==================payment end branch cash===========================================
						
					//==================payment start Account Cash=========================================
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Accounts' && isset($data['account_id'.$i])){	
						//$total_cash += $eachRec->total_charge;
						$this->UpdateAccountCash($data['account_id'.$i],$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_fixed_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['account_id'] = $data['account_id'.$i];
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_company_transaction',$companyDate);
					}
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='direct' && isset($data['account_id'.$i])){	
						//$total_cash += $eachRec->total_charge;
						//$this->UpdateAccountCash($data['account_id'.$i],$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_fixed_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						/*$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['account_id'] = $data['account_id'.$i];
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_company_transaction',$companyDate);*/
					}
					
					//==================payment end Account cash===========================================
						
						
						unset($data['txt_'.$i],$data['txttotal_'.$i],$data['txtpur_'.$i],$data['txtno_'.$i]);
						unset($data['way_of_payment'.$i],$data['account_id'.$i]);
					}
				}else if($data['account_type'] == 'cash'){
					
				    if(isset($data['txt_'.$i]) && isset($data['txtno_'.$i]) && isset($data['txttotal_'.$i])  && isset($data['txtpur_'.$i])){
						$getdata = array();
						
						//$new_ids = $this->getLastJobId();
						//$getdata['job_id'] = ($new_ids->Auto_increment);
						
						$getdata['job_id'] = $job_id;
						$getdata['customer_id'] = $new_id;
						$getdata['charge_id'] =  $i;
						$getdata['per_charge_amount'] = $data['txt_'.$i] ;
						$getdata['charge_quaintity'] = $data['txtno_'.$i] ;
						$getdata['total_charge'] = $data['txttotal_'.$i] ;
						$getdata['pur_charge'] = $data['txtpur_'.$i] ;
						//$getdata['created'] = date('Y-m-d H:i:s');
						//$getdata['created_by'] = $this->session->userdata('userid');
						//$port_data['customer_id'] = $getdata['customer_id'];
						//$port_data['created'] = date('Y-m-d H:i:s');
						//print_r($getdata);
						//exit;
						//$this->db->insert('an_customer_port_selection',$port_data);
						$sql = "select * from an_cash_job_charges where job_id = '$job_id' and customer_id = '$new_id' and charge_id = '$i' ";
						//echo $sql;exit;
						$q = $this->db->query($sql);
						if($q->num_rows() > 0)
						{
							$this->db->where('job_id', $job_id);
							$this->db->where('customer_id', $new_id);
							$this->db->where('charge_id', $i);
							$this->db->update('an_cash_job_charges', $getdata);
						}else{
							$this->db->insert('an_cash_job_charges',$getdata);
						}
						
						
						//==================payment start branch cash=========================================
					
						
					if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Cash'){	
						$this->UpdateBranchCash($branches_id,$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_cash_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_cash_management',$companyDate);
					}
    				
					//==================payment end branch cash===========================================
						
					//==================payment start Account Cash=========================================
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Accounts' && isset($data['account_id'.$i])){	
						//$total_cash += $eachRec->total_charge;
						$this->UpdateAccountCash($data['account_id'.$i],$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_cash_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['account_id'] = $data['account_id'.$i];
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_company_transaction',$companyDate);
					}
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='direct'){	
						//$this->UpdateBranchCash($branches_id,$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_cash_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						/*$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_cash_management',$companyDate);*/
					}
					
					//==================payment end Account cash===========================================
						
						
						//$this->db->where('job_id', $job_id);
						//$this->db->where('customer_id', $new_id);
						//$this->db->where('charge_id', $i);
						//$this->db->update('an_cash_job_charges', $getdata);
						unset($data['txt_'.$i],$data['txtno_'.$i],$data['txttotal_'.$i],$data['txtpur_'.$i]);
						unset($data['way_of_payment'.$i],$data['account_id'.$i]);
					}
				}else if($data['account_type'] == 'contract'){
					
					if(isset($data['txt_'.$i]) && isset($data['txtpercharge_'.$i]) && isset($data['txttotal_'.$i])  && isset($data['txtpur_'.$i])){	
						$getdata = array();
						
						
						//$new_ids = $this->getLastJobId();
						//$getdata['job_id'] = ($new_ids->Auto_increment);
						
						$getdata['job_id'] = $job_id;
						$getdata['customer_id'] = $new_id;
						$getdata['charge_id'] =  $i;
						$getdata['charge_quaintity'] = $data['txt_'.$i] ;
						$getdata['per_charge_amount'] = $data['txtpercharge_'.$i] ;
						$getdata['total_charge'] = $data['txttotal_'.$i] ;
						$getdata['pur_charge'] = $data['txtpur_'.$i] ;
						
						//$getdata['created'] = date('Y-m-d H:i:s');
						//$getdata['created_by'] = $this->session->userdata('userid');
						//$port_data['customer_id'] = $getdata['customer_id'];
						//$port_data['created'] = date('Y-m-d H:i:s');
						//$this->db->insert('an_customer_port_selection',$port_data);
						$sql = "select * from an_contract_job_charges where job_id = '$job_id' and customer_id = '$new_id' and charge_id = '$i' ";
						//echo $sql;exit;
						$q = $this->db->query($sql);
						if($q->num_rows() > 0)
						{
							$this->db->where('job_id', $job_id);
							$this->db->where('customer_id', $new_id);
							$this->db->where('charge_id', $i);
							$this->db->update('an_contract_job_charges', $getdata);
						}else{
							$this->db->insert('an_contract_job_charges',$getdata);
						}
					
					//==================payment start branch cash=========================================
					
						
					if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Cash'){	
						$this->UpdateBranchCash($branches_id,$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_contract_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_cash_management',$companyDate);
					}
    				
					//==================payment end branch cash===========================================
						
					//==================payment start Account Cash=========================================
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Accounts' && isset($data['account_id'.$i])){	
						//$total_cash += $eachRec->total_charge;
						$this->UpdateAccountCash($data['account_id'.$i],$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_contract_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['account_id'] = $data['account_id'.$i];
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_company_transaction',$companyDate);
					}
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='direct'){	
						//$this->UpdateBranchCash($branches_id,$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_contract_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						/*$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_cash_management',$companyDate);*/
					}
					
					//==================payment end Account cash===========================================
						//$this->db->where('job_id', $job_id);
						//$this->db->where('customer_id', $new_id);
						//$this->db->where('charge_id', $i);
						//$this->db->update('an_contract_job_charges', $getdata);
						unset($data['txt_'.$i]);
						unset($data['txtpercharge_'.$i],$data['txttotal_'.$i],$data['txtpur_'.$i]);
						unset($data['way_of_payment'.$i],$data['account_id'.$i]);
					}
				}
				//unset($data['txt_'.$i]);
				//unset($data['txtpercharge_'.$i],$data['txttotal_'.$i],$data['txtno_'.$i]);
				
				
			}
			unset($data['count_txt']);
			unset($data['account_type'],$data['type_of_shipment']);
			
		} 
/*
			for($j=0;$j<=$i;$j++){
				unset($data['txt_'.$j]);
				unset($data['txtpercharge_'.$j],$data['txttotal_'.$j],$data['txtpur_'.$j],$data['txtno_'.$j]);
				unset($data['way_of_payment'.$j],$data['account_id'.$j]);
			}
*/
			
			
			unset($data['jb_c_id']);
			unset($data['charge_name']);
			unset($data['charges_advance']);
			unset($data['charges_value']);
			unset($data['purchase_value']);
			unset($data['sales_value']);
			
			//charge_name
			if(isset($data['count_container'])){
				//echo $data['count_container'];
				//echo count($data['count_container']);
				//exit;
				for($m=1;$m<=$data['count_container'];$m++){
					//echo $m;
					if(isset($data['container_number'.$m]) && $data['container_number'.$m]!=''){
						
						
						
							
						$containerData['size'] = $data['size'.$m];
						$containerData['truck_number'] = $data['truck_number'.$m];
						$containerData['deliver_by'] = $data['deliver_by'.$m];
						$containerData['container_type_id'] = $data['container_type_id'.$m];						
						//$containerData['diesel_amount'] = $data['diesal'.$m];
						$containerData['job_id'] = $job_id;
						$containerData['container_number'] = $data['container_number'.$m];
						
						$return_id = $this->getjob_containers($containerData['truck_number'],$job_id);
						if($return_id){
								//$this->db->where('id',$return_id);
								//$this->db->update('an_shipment_container',$containerData);
						}
						else{
							$this->db->insert('an_shipment_container',$containerData);
						}
						
						
						$diesal = 'diesal'.$m;
						$truck_number = 'truck_number'.$m;
						$container_number ='container_number'.$m;
						$deliver_by ='deliver_by'.$m;
						$container_type_id = 'container_type_id'.$m;
						
						unset($data[$truck_number]);
						unset($data[$diesal]);
						unset($data[$container_number]);
						unset($data[$deliver_by]);
						unset($data[$container_type_id]);
						
					}
					
						$sizen = 'size'.$m;
						unset($data[$sizen]);
						
				}
			}
		//	echo "<pre>";
		//	print_r($data['diesal1']);
		//	exit;
			
			unset($data['container_number0']);
			unset($data['count_container']);
			unset($data['size']);
			unset($data['truck_number']);
			unset($data['deliver_by']);
			unset($data['container_type_id']);
			$newdata = array();			
			//$newdata['customer_id'] = $data['customer_id'];
			//$newdata['job_type'] = $data['job_type'];
			$newdata['way_of_shipment'] = $data['way_of_shipment'];
			$meees = $this->get_job_detail($job_id);
			if($meees->mandatory_attachment!=''){
				$newdata['job_status'] = $data['job_status'];
			}
			$newdata['email_send'] = $data['email_send'];
			$newdata['review'] = $data['review'];
			if(isset($data['job_step_no'])&& $data['job_step_no']<=4){
				$newdata['job_step_no'] = $data['job_step_no'];
			}
			
			//$newdata['job_step_no'] = ($data['job_step_no']>4)?4:$data['job_step_no'];
			//$newdata['total_value'] = $data['total_value'];
	
			unset($data['customer_id'],$data['job_type'],$data['way_of_shipment'],$data['job_step_no']);
			unset($data['products']);
			unset($data['bank_id']);
			unset($data['job_status'],$data['total_value'],$data['email_send'],$data['review'],$data['id'],$data['charges_id'],$data['current_charges_id']);
			unset($data['charges_advance'],$data['sale_value'],$data['remaining'],$data['purchase_value'],$data['comment'],$data['notes'],$data['all_discount'],$data['received_amount'],$data['charges_id'],$data['current_charges_id'],$data['container_number1'],$data['size1'],$data['truck_number1'],$data['deliver_by1'],$data['container_type_id1'],$data['diesal1']);
			unset($data['supplier_value'],$data['container_category']);
			
			$data['job_id'] = $job_id;
			$this->db->where('job_id',$job_id);
			$this->db->update('an_shipmentinfo',$data);
			
		$this->getLocationForAutoSerach($data['pickup_location']);
		$this->getLocationForAutoSerach($data['delivery_location']);
		$this->getLocationForAutoSerach($data['port_of_loading']);
		$this->getLocationForAutoSerach($data['loading_place']);
		$this->getLocationForAutoSerach($data['offloading_place']);
		
		//print_r($newdata);exit;
		//echo '<pre>';
		//print_r($_POST);
		//exit;
		$this->db->where('id', $job_id);
		$this->db->update('an_jobs', $newdata);	
		
		
		/*// add here duty charges. only 
			$myjob_id = $job_id;
			$tablename = '';
			$ress = $this->customers->get_customer_detail($customer_id);
			$account_type_customer = $ress->account_type;
			if($account_type_customer == 'contract' /*&& $job_statuss != "waiting_for_customer"*){
				$sql = " select * from an_contract_job_charges where job_id = '$myjob_id' and charge_id = '7' ";
				$q = $this->db->query($sql);
				if($q->num_rows() == 0)
				{				
					$mydata = array();	
					$tablename = 'an_contract_job_charges';
					$mydata['job_id'] = $myjob_id;
					$mydata['customer_id'] = $customer_id;
					$mydata['charge_id'] =  '7';
					$mydata['charge_quaintity'] = '1' ;
					$mydata['per_charge_amount'] = '1' ;
					$mydata['total_charge'] = $duty_chares_for_job ;
					$mydata['created'] = date('Y-m-d H:i:s');
					$mydata['created_by'] = $this->session->userdata('userid');
					$this->db->insert('an_contract_job_charges', $mydata);
				}
			}else if($account_type_customer == 'cash'/* && $job_statuss != "waiting_for_customer"*){
				
				$sql = " select * from an_cash_job_charges where job_id = '$myjob_id' and charge_id = '7' ";
				$q = $this->db->query($sql);
				if($q->num_rows() == 0)
				{
					$mydata = array();
					$tablename = 'an_cash_job_charges';
					$mydata['job_id'] = $myjob_id;
					$mydata['customer_id'] = $customer_id;
					$mydata['charge_id'] =  '7';
					$mydata['per_charge_amount'] = '1';
					$mydata['charge_quaintity'] = '1';
					$mydata['total_charge'] = $duty_chares_for_job ;
					$mydata['created'] = date('Y-m-d H:i:s');
					$mydata['created_by'] = $this->session->userdata('userid');
					$this->db->insert('an_cash_job_charges', $mydata);
				}
			}else if($account_type_customer == 'fixed' /*&& $job_statuss != "waiting_for_customer"*){
				
				$sql = " select * from an_fixed_job_charges where job_id = '$myjob_id' and charge_id = '7' ";
				$q = $this->db->query($sql);
				if($q->num_rows() == 0)
				{
					$mydata = array();
					$tablename = 'an_fixed_job_charges';
					$mydata['job_id'] = $myjob_id;
					$mydata['customer_id'] = $customer_id;
					$mydata['charge_id'] =  '7';
					$mydata['per_charge_amount'] = '1' ;
					$mydata['charge_quaintity'] = '1' ;
					$mydata['total_charge'] = $duty_chares_for_job ;
					$mydata['created'] = date('Y-m-d H:i:s');
					$mydata['created_by'] = $this->session->userdata('userid');
					$this->db->insert('an_fixed_job_charges', $mydata);
				}
			}*/
			
			// add here duty charges. only end
			
			// adding payments for cash start
			/*$ressd = $this->getDutyChargeswithJobType($myjob_id);
			//print_r($ressd);
			$tablename = $ressd->tablename;
			
			if($payment_type=='Cash' && $job_statuss != "waiting_for_customer"){
	
				$rec = $this->getAllChargesForPayment($tablename,$myjob_id);
				//$total_cash=0;
				foreach($rec as $eachRec){
					//$total_cash += $eachRec->total_charge;
					$this->UpdateBranchCash($branches_id,$eachRec->total_charge);
					//update query for cash
					// update status to paid
					$this->UpdatePaidStatusOfCash($tablename,$eachRec->id);
					//insert into table compnaytaransctions
					$companyDate = array();
					$companyDate['job_id'] = $myjob_id;
					$companyDate['branch_id'] = $branches_id;
					$companyDate['customer_id'] = $customer_id;
					$companyDate['charge_id'] = $eachRec->id;
					$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
					$companyDate['transaction_type'] = 'credit';
					$companyDate['payment_type'] = 'deposite';
					$companyDate['created'] = date('Y-m-d H:i:s');
					$companyDate['emp_id'] = $this->session->userdata('userid');
					$this->db->insert('an_company_transaction',$companyDate);
				}
			}else if($payment_type=='Accounts' && $job_statuss != "waiting_for_customer"){
				$rec = $this->getAllChargesForPayment($tablename,$myjob_id);
				//$total_cash=0;
				foreach($rec as $eachRec){
					//$total_cash += $eachRec->total_charge;
					$this->UpdateAccountCash($account_id,$eachRec->total_charge);
					//update query for cash
					// update status to paid
					$this->UpdatePaidStatusOfCash($tablename,$eachRec->id);
					//insert into table compnaytaransctions
					$companyDate = array();
					$companyDate['job_id'] = $myjob_id;
					$companyDate['branch_id'] = $branches_id;
					$companyDate['customer_id'] = $customer_id;
					$companyDate['charge_id'] = $eachRec->id;
					$companyDate['account_id'] = $account_id;
					$companyDate['transaction_type'] = 'credit';
					$companyDate['payment_type'] = 'deposite';
					$companyDate['created'] = date('Y-m-d H:i:s');
					$companyDate['emp_id'] = $this->session->userdata('userid');
					$this->db->insert('an_company_transaction',$companyDate);
				}
			}*/
			
			// adding payments for cash end
		
		
		return TRUE;
	}
	
	
	function update_job_copy($data,$job_id)
	{
		$res = $this->get_job_detail($job_id);
		//echo "<pre>";
		//print_r($data);
		//exit;
		$customer_id = $res->customer_id;
		$job_statuss = isset($data['job_status'])?$data['job_status']:$res->job_status;
		$duty_chares_for_job = $res->duty_charges;
		$payment_type = isset($data['way_of_payment'])?$data['way_of_payment']:$res->way_of_payment;
		$branches_id = $this->getCustomerBranch($customer_id);//$res->branch_id;
		$account_id = isset($data['account_id'])?$data['account_id']:'';
		unset($data['account_id']);
		//exit;
		$data['customer_id'] = $res->customer_id;
		
		
			$new_id = $data['customer_id'];
			if(isset($data['count_txt']) && $data['count_txt'] > 0){
				$res = $this->customers->get_customer_detail($res->customer_id);
				$data['account_type'] = $res->account_type;
				
			for($i=0;$i<=$data['count_txt'];$i++){
				if($data['account_type'] == 'fixed'){
					$getdata = array();
					
					if(isset($data['txt_'.$i]) && isset($data['txtno_'.$i]) && isset($data['txttotal_'.$i]) && isset($data['txtpur_'.$i]) ){	
						
						//$new_ids = $this->getLastJobId();
						//$getdata['job_id'] = ($new_ids->Auto_increment);
						
						$getdata['job_id'] = $job_id;
						$getdata['customer_id'] = $new_id;
						$getdata['charge_id'] =  $i;
						//$getdata['charge_value'] = $data['txt_'.$i] ;
						$getdata['per_charge_amount'] = isset($data['txt_'.$i])?$data['txt_'.$i]:'' ;
						$getdata['charge_quaintity'] = isset($data['txtno_'.$i])?$data['txtno_'.$i]:'';
						$getdata['total_charge'] = isset($data['txttotal_'.$i])?$data['txttotal_'.$i]:'' ;
						$getdata['pur_charge'] = isset($data['txtpur_'.$i])?$data['txtpur_'.$i]:'' ;
						//$getdata['created'] = date('Y-m-d H:i:s');
						//$getdata['created_by'] = $this->session->userdata('userid');
						//$port_data['customer_id'] = $getdata['customer_id'];
						//$port_data['created'] = date('Y-m-d H:i:s');
						
						//$this->db->insert('an_customer_port_selection',$port_data);
						$sql = "select * from an_fixed_job_charges where job_id = '$job_id' and customer_id = '$new_id' and charge_id = '$i' ";
						
						$q = $this->db->query($sql);
						if($q->num_rows() > 0)
						{
							$this->db->where('job_id', $job_id);
							$this->db->where('customer_id', $new_id);
							$this->db->where('charge_id', $i);
							$this->db->update('an_fixed_job_charges', $getdata);
						}else{
							$this->db->insert('an_fixed_job_charges',$getdata);
						}
						
					//==================payment start branch cash=========================================
					
						
					if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Cash'){	
						$this->UpdateBranchCash($branches_id,$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_fixed_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_cash_management',$companyDate);
					}
    				
					//==================payment end branch cash===========================================
						
					//==================payment start Account Cash=========================================
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Accounts' && isset($data['account_id'.$i])){	
						//$total_cash += $eachRec->total_charge;
						$this->UpdateAccountCash($data['account_id'.$i],$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_fixed_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['account_id'] = $data['account_id'.$i];
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_company_transaction',$companyDate);
					}
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='direct' && isset($data['account_id'.$i])){	
						//$total_cash += $eachRec->total_charge;
						//$this->UpdateAccountCash($data['account_id'.$i],$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_fixed_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						/*$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['account_id'] = $data['account_id'.$i];
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_company_transaction',$companyDate);*/
					}
					
					//==================payment end Account cash===========================================
						
						
						unset($data['txt_'.$i],$data['txttotal_'.$i],$data['txtpur_'.$i],$data['txtno_'.$i]);
						unset($data['way_of_payment'.$i],$data['account_id'.$i]);
					}
				}else if($data['account_type'] == 'cash'){
					
				    if(isset($data['txt_'.$i]) && isset($data['txtno_'.$i]) && isset($data['txttotal_'.$i])  && isset($data['txtpur_'.$i])){
						$getdata = array();
						
						//$new_ids = $this->getLastJobId();
						//$getdata['job_id'] = ($new_ids->Auto_increment);
						
						$getdata['job_id'] = $job_id;
						$getdata['customer_id'] = $new_id;
						$getdata['charge_id'] =  $i;
						$getdata['per_charge_amount'] = $data['txt_'.$i] ;
						$getdata['charge_quaintity'] = $data['txtno_'.$i] ;
						$getdata['total_charge'] = $data['txttotal_'.$i] ;
						$getdata['pur_charge'] = $data['txtpur_'.$i] ;
						//$getdata['created'] = date('Y-m-d H:i:s');
						//$getdata['created_by'] = $this->session->userdata('userid');
						//$port_data['customer_id'] = $getdata['customer_id'];
						//$port_data['created'] = date('Y-m-d H:i:s');
						//print_r($getdata);
						//exit;
						//$this->db->insert('an_customer_port_selection',$port_data);
						$sql = "select * from an_cash_job_charges where job_id = '$job_id' and customer_id = '$new_id' and charge_id = '$i' ";
						//echo $sql;exit;
						$q = $this->db->query($sql);
						if($q->num_rows() > 0)
						{
							$this->db->where('job_id', $job_id);
							$this->db->where('customer_id', $new_id);
							$this->db->where('charge_id', $i);
							$this->db->update('an_cash_job_charges', $getdata);
						}else{
							$this->db->insert('an_cash_job_charges',$getdata);
						}
						
						
						//==================payment start branch cash=========================================
					
						
					if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Cash'){	
						$this->UpdateBranchCash($branches_id,$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_cash_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_cash_management',$companyDate);
					}
    				
					//==================payment end branch cash===========================================
						
					//==================payment start Account Cash=========================================
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Accounts' && isset($data['account_id'.$i])){	
						//$total_cash += $eachRec->total_charge;
						$this->UpdateAccountCash($data['account_id'.$i],$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_cash_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['account_id'] = $data['account_id'.$i];
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_company_transaction',$companyDate);
					}
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='direct'){	
						//$this->UpdateBranchCash($branches_id,$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_cash_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						/*$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_cash_management',$companyDate);*/
					}
					
					//==================payment end Account cash===========================================
						
						
						//$this->db->where('job_id', $job_id);
						//$this->db->where('customer_id', $new_id);
						//$this->db->where('charge_id', $i);
						//$this->db->update('an_cash_job_charges', $getdata);
						unset($data['txt_'.$i],$data['txtno_'.$i],$data['txttotal_'.$i],$data['txtpur_'.$i]);
						unset($data['way_of_payment'.$i],$data['account_id'.$i]);
					}
				}else if($data['account_type'] == 'contract'){
					
					if(isset($data['txt_'.$i]) && isset($data['txtpercharge_'.$i]) && isset($data['txttotal_'.$i])  && isset($data['txtpur_'.$i])){	
						$getdata = array();
						
						
						//$new_ids = $this->getLastJobId();
						//$getdata['job_id'] = ($new_ids->Auto_increment);
						
						$getdata['job_id'] = $job_id;
						$getdata['customer_id'] = $new_id;
						$getdata['charge_id'] =  $i;
						$getdata['charge_quaintity'] = $data['txt_'.$i] ;
						$getdata['per_charge_amount'] = $data['txtpercharge_'.$i] ;
						$getdata['total_charge'] = $data['txttotal_'.$i] ;
						$getdata['pur_charge'] = $data['txtpur_'.$i] ;
						
						//$getdata['created'] = date('Y-m-d H:i:s');
						//$getdata['created_by'] = $this->session->userdata('userid');
						//$port_data['customer_id'] = $getdata['customer_id'];
						//$port_data['created'] = date('Y-m-d H:i:s');
						//$this->db->insert('an_customer_port_selection',$port_data);
						$sql = "select * from an_contract_job_charges where job_id = '$job_id' and customer_id = '$new_id' and charge_id = '$i' ";
						//echo $sql;exit;
						$q = $this->db->query($sql);
						if($q->num_rows() > 0)
						{
							$this->db->where('job_id', $job_id);
							$this->db->where('customer_id', $new_id);
							$this->db->where('charge_id', $i);
							$this->db->update('an_contract_job_charges', $getdata);
						}else{
							$this->db->insert('an_contract_job_charges',$getdata);
						}
					
					//==================payment start branch cash=========================================
					
						
					if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Cash'){	
						$this->UpdateBranchCash($branches_id,$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_contract_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_cash_management',$companyDate);
					}
    				
					//==================payment end branch cash===========================================
						
					//==================payment start Account Cash=========================================
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Accounts' && isset($data['account_id'.$i])){	
						//$total_cash += $eachRec->total_charge;
						$this->UpdateAccountCash($data['account_id'.$i],$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_contract_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['account_id'] = $data['account_id'.$i];
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_company_transaction',$companyDate);
					}
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='direct'){	
						//$this->UpdateBranchCash($branches_id,$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_contract_job_charges';
						$this->UpdatePaidStatusOfCharges($tablename,$i,$job_id);
						//insert into table compnaytaransctions
						/*$companyDate = array();
						$companyDate['job_id'] = $job_id;
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						$this->db->insert('an_cash_management',$companyDate);*/
					}
					
					//==================payment end Account cash===========================================
						//$this->db->where('job_id', $job_id);
						//$this->db->where('customer_id', $new_id);
						//$this->db->where('charge_id', $i);
						//$this->db->update('an_contract_job_charges', $getdata);
						unset($data['txt_'.$i]);
						unset($data['txtpercharge_'.$i],$data['txttotal_'.$i],$data['txtpur_'.$i]);
						unset($data['way_of_payment'.$i],$data['account_id'.$i]);
					}
				}
				//unset($data['txt_'.$i]);
				//unset($data['txtpercharge_'.$i],$data['txttotal_'.$i],$data['txtno_'.$i]);
				
				
			}
			unset($data['count_txt']);
			unset($data['account_type'],$data['type_of_shipment']);
			
		} 
			for($j=0;$j<=$i;$j++){
				unset($data['txt_'.$j]);
				unset($data['txtpercharge_'.$j],$data['txttotal_'.$j],$data['txtpur_'.$j],$data['txtno_'.$j]);
				unset($data['way_of_payment'.$j],$data['account_id'.$j]);
			}
			
			if(isset($data['count_container'])){
				for($m=1;$m<=$data['count_container'];$m++){
					if(isset($data['container_number'.$m]) && $data['container_number'.$m]!=''){
						$containerData['size'] = $data['size'.$m];
						$containerData['truck_number'] = $data['truck_number'.$m];
						$containerData['deliver_by'] = $data['deliver_by'.$m];
						$containerData['container_type_id'] = $data['container_type_id'.$m];						
						$containerData['job_id'] = $job_id;
						$containerData['container_number'] = $data['container_number'.$m];
						$this->db->insert('an_shipment_container',$containerData);
						unset($data['container_number'.$m]);
						unset($data['size'.$m]);
						unset($data['truck_number'.$m]);
						unset($data['deliver_by'.$m]);
						unset($data['container_type_id'.$m]);
					}
				}
			}
			unset($data['container_number0']);
			unset($data['count_container']);
			unset($data['size']);
			
			unset($data['bank_id']);
			
						unset($data['truck_number']);
						unset($data['deliver_by']);
						unset($data['container_type_id']);
			$newdata = array();			
			//$newdata['customer_id'] = $data['customer_id'];
			//$newdata['job_type'] = $data['job_type'];
			$newdata['way_of_shipment'] = $data['way_of_shipment'];
			$meees = $this->get_job_detail($job_id);
			if($meees->mandatory_attachment!=''){
				$newdata['job_status'] = $data['job_status'];
			}
			$newdata['email_send'] = $data['email_send'];
			$newdata['review'] = $data['review'];
			$newdata['job_step_no'] = ($data['job_step_no']>4)?4:$data['job_step_no'];
			//$newdata['total_value'] = $data['total_value'];
			//print_r($data);exit;
			unset($data['customer_id'],$data['job_type'],$data['way_of_shipment'],$data['job_step_no']);
			unset($data['job_status'],$data['total_value'],$data['email_send'],$data['review'],$data['id']);
			unset($data['charges_advance'],$data['sale_value'],$data['remaining'],$data['purchase_value'],$data['comment'],$data['notes'],$data['all_discount'],$data['received_amount'],$data['charges_id'],$data['current_charges_id']);
			unset($data['supplier_value']);
			
			$data['job_id'] = $job_id;
			$this->db->where('job_id',$job_id);
			$this->db->update('an_shipmentinfo',$data);
			
		
		//print_r($newdata);exit;
		//echo '<pre>';
		//print_r($_POST);
		//exit;
		$this->db->where('id', $job_id);
		$this->db->update('an_jobs', $newdata);	
		
		
		/*// add here duty charges. only 
			$myjob_id = $job_id;
			$tablename = '';
			$ress = $this->customers->get_customer_detail($customer_id);
			$account_type_customer = $ress->account_type;
			if($account_type_customer == 'contract' /*&& $job_statuss != "waiting_for_customer"*){
				$sql = " select * from an_contract_job_charges where job_id = '$myjob_id' and charge_id = '7' ";
				$q = $this->db->query($sql);
				if($q->num_rows() == 0)
				{				
					$mydata = array();	
					$tablename = 'an_contract_job_charges';
					$mydata['job_id'] = $myjob_id;
					$mydata['customer_id'] = $customer_id;
					$mydata['charge_id'] =  '7';
					$mydata['charge_quaintity'] = '1' ;
					$mydata['per_charge_amount'] = '1' ;
					$mydata['total_charge'] = $duty_chares_for_job ;
					$mydata['created'] = date('Y-m-d H:i:s');
					$mydata['created_by'] = $this->session->userdata('userid');
					$this->db->insert('an_contract_job_charges', $mydata);
				}
			}else if($account_type_customer == 'cash'/* && $job_statuss != "waiting_for_customer"*){
				
				$sql = " select * from an_cash_job_charges where job_id = '$myjob_id' and charge_id = '7' ";
				$q = $this->db->query($sql);
				if($q->num_rows() == 0)
				{
					$mydata = array();
					$tablename = 'an_cash_job_charges';
					$mydata['job_id'] = $myjob_id;
					$mydata['customer_id'] = $customer_id;
					$mydata['charge_id'] =  '7';
					$mydata['per_charge_amount'] = '1';
					$mydata['charge_quaintity'] = '1';
					$mydata['total_charge'] = $duty_chares_for_job ;
					$mydata['created'] = date('Y-m-d H:i:s');
					$mydata['created_by'] = $this->session->userdata('userid');
					$this->db->insert('an_cash_job_charges', $mydata);
				}
			}else if($account_type_customer == 'fixed' /*&& $job_statuss != "waiting_for_customer"*){
				
				$sql = " select * from an_fixed_job_charges where job_id = '$myjob_id' and charge_id = '7' ";
				$q = $this->db->query($sql);
				if($q->num_rows() == 0)
				{
					$mydata = array();
					$tablename = 'an_fixed_job_charges';
					$mydata['job_id'] = $myjob_id;
					$mydata['customer_id'] = $customer_id;
					$mydata['charge_id'] =  '7';
					$mydata['per_charge_amount'] = '1' ;
					$mydata['charge_quaintity'] = '1' ;
					$mydata['total_charge'] = $duty_chares_for_job ;
					$mydata['created'] = date('Y-m-d H:i:s');
					$mydata['created_by'] = $this->session->userdata('userid');
					$this->db->insert('an_fixed_job_charges', $mydata);
				}
			}*/
			
			// add here duty charges. only end
			
			// adding payments for cash start
			/*$ressd = $this->getDutyChargeswithJobType($myjob_id);
			//print_r($ressd);
			$tablename = $ressd->tablename;
			
			if($payment_type=='Cash' && $job_statuss != "waiting_for_customer"){
	
				$rec = $this->getAllChargesForPayment($tablename,$myjob_id);
				//$total_cash=0;
				foreach($rec as $eachRec){
					//$total_cash += $eachRec->total_charge;
					$this->UpdateBranchCash($branches_id,$eachRec->total_charge);
					//update query for cash
					// update status to paid
					$this->UpdatePaidStatusOfCash($tablename,$eachRec->id);
					//insert into table compnaytaransctions
					$companyDate = array();
					$companyDate['job_id'] = $myjob_id;
					$companyDate['branch_id'] = $branches_id;
					$companyDate['customer_id'] = $customer_id;
					$companyDate['charge_id'] = $eachRec->id;
					$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
					$companyDate['transaction_type'] = 'credit';
					$companyDate['payment_type'] = 'deposite';
					$companyDate['created'] = date('Y-m-d H:i:s');
					$companyDate['emp_id'] = $this->session->userdata('userid');
					$this->db->insert('an_company_transaction',$companyDate);
				}
			}else if($payment_type=='Accounts' && $job_statuss != "waiting_for_customer"){
				$rec = $this->getAllChargesForPayment($tablename,$myjob_id);
				//$total_cash=0;
				foreach($rec as $eachRec){
					//$total_cash += $eachRec->total_charge;
					$this->UpdateAccountCash($account_id,$eachRec->total_charge);
					//update query for cash
					// update status to paid
					$this->UpdatePaidStatusOfCash($tablename,$eachRec->id);
					//insert into table compnaytaransctions
					$companyDate = array();
					$companyDate['job_id'] = $myjob_id;
					$companyDate['branch_id'] = $branches_id;
					$companyDate['customer_id'] = $customer_id;
					$companyDate['charge_id'] = $eachRec->id;
					$companyDate['account_id'] = $account_id;
					$companyDate['transaction_type'] = 'credit';
					$companyDate['payment_type'] = 'deposite';
					$companyDate['created'] = date('Y-m-d H:i:s');
					$companyDate['emp_id'] = $this->session->userdata('userid');
					$this->db->insert('an_company_transaction',$companyDate);
				}
			}*/
			
			// adding payments for cash end
		
		
		return TRUE;
	}
	
	public function add_new_job($job_id='')
	{
		$data = $this->input->post();
	
		unset($data['sub_mit'],$data['sub_reset'],$data['job_id']);
		        
		$newdata['shipment_standard'] = $data['shipment_standard'];
		$newdata['freight_charges'] = $data['freight_charges'];
		$newdata['total_cost_of_goods'] = $data['total_cost_of_goods'];
		$newdata['insurance'] = $data['insurance'];
		$newdata['duty_charges'] = $data['duty_charges']+$data['additional_charges'];
		$newdata['additional_charges'] = $data['additional_charges'];
		$newdata['way_of_payment'] = $data['way_of_payment'];
		
		unset($data['shipment_standard'],$data['freight_charges'],$data['total_cost_of_goods']);
		unset($data['insurance'],$data['duty_charges'],$data['additional_charges'],$data['way_of_payment'],$data['charges_id']);
		unset($data['bank_id'],$data['supplier_value']);
		
		$data['emp_id'] = $this->session->userdata('userid');
		$this->db->insert('an_jobs', $data);
		$new_job_id = $this->db->insert_id();
		$newdata['job_id'] = $new_job_id;
		$this->db->insert('an_shipmentinfo', $newdata);
		do_redirect('options?e=10');
	}
	
	
	function addCharges($job_charges){	
		return $this->db->insert('jobs_charges', $job_charges);	
	}
	
	
	/*
	 * 
	 * Enter description here ...
	 * Created by M.Ahmed
	 */
	public function add_job($job_id='')
	{
		$data = $this->input->post();
		//echo "<pre>";
		$mytablename = '';
		$customer_id = $data['customer_id'];
		
		$duty_chares_for_job = $data['duty_charges'];
		$payment_type = $data['way_of_payment'];
		$branches_id = $this->getCustomerBranch($customer_id);
		$job_statuss = $data['job_status'];
		$account_id = isset($data['account_id'])?$data['account_id']:'';
		unset($data['account_id']);
		//print_r($data);
		//exit;
		$job_id = $this->input->post('job_id');
		
		unset($data['sub_mit'],$data['sub_reset'],$data['job_id']);
					
		if($job_id!='')
		{
			$this->db->where('job_id', $job_id);
			$this->db->update('an_jobs', $data);
			do_redirect('options?e=10');
		}
		else
		{
			
			
			//-----------------------------------------------------
			//this logic is for saving charges start
			
			$new_id = $data['customer_id'];
			
			
			if(isset($data['count_txt']) && $data['count_txt'] > 0){
				$res = $this->customers->get_customer_detail($data['customer_id']);
				$data['account_type'] = $res->account_type;
				
			for($i=0;$i<=$data['count_txt'];$i++){
				if($data['account_type'] == 'fixed'){
					if(isset($data['txt_'.$i]) && isset($data['txtno_'.$i]) && isset($data['txttotal_'.$i]) && isset($data['txtpur_'.$i]) ){
						$getdata = array();
						
						$new_ids = $this->getLastJobId();
						$getdata['job_id'] = ($new_ids->Auto_increment);
						
										
						
						
						$getdata['customer_id'] = $new_id;
						$getdata['charge_id'] =  $i;
						//$getdata['charge_value'] = $data['txt_'.$i] ;
						$getdata['per_charge_amount'] = $data['txt_'.$i] ;
						$getdata['charge_quaintity'] = $data['txtno_'.$i] ;
						$getdata['total_charge'] = $data['txttotal_'.$i] ;
						$getdata['pur_charge'] = $data['txtpur_'.$i] ;
						$getdata['created'] = date('Y-m-d H:i:s');
						$getdata['created_by'] = $this->session->userdata('userid');
						//$port_data['customer_id'] = $getdata['customer_id'];
						//$port_data['created'] = date('Y-m-d H:i:s');
						
						//$this->db->insert('an_customer_port_selection',$port_data);
						//$this->db->insert('an_fixed_job_charges', $getdata);
						
					//==================payment start branch cash=========================================
						
					if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Cash'){
						
						//$this->UpdateBranchCash($branches_id,$getdata['total_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_fixed_job_charges';
						//$this->UpdatePaidStatusOfCharges($tablename,$i);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $getdata['job_id'];
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						//$this->db->insert('an_cash_management',$companyDate);
					}
    				
					//==================payment end branch cash===========================================
						
					//==================payment start Account Cash=========================================
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Accounts' && isset($data['account_id'.$i])){	
						//$total_cash += $eachRec->total_charge;
						//$this->UpdateAccountCash($data['account_id'.$i],$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_fixed_job_charges';
						//$this->UpdatePaidStatusOfCharges($tablename,$i);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $getdata['job_id'];
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['account_id'] = $data['account_id'.$i];
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						//$this->db->insert('an_company_transaction',$companyDate);
					}
					
					//==================payment end Account cash===========================================
					
						
						
						
						unset($data['txt_'.$i],$data['txttotal_'.$i],$data['txtpur_'.$i],$data['txtno_'.$i]);
					}
				}else if($data['account_type'] == 'cash'){
					if(isset($data['txt_'.$i]) && isset($data['txtno_'.$i]) && isset($data['txttotal_'.$i]) && isset($data['txtpur_'.$i]) ){
				
						$getdata = array();
						
						$new_ids = $this->getLastJobId();
						$getdata['job_id'] = ($new_ids->Auto_increment);
						
						$getdata['customer_id'] = $new_id;
						$getdata['charge_id'] =  $i;
						$getdata['per_charge_amount'] = $data['txt_'.$i] ;
						$getdata['charge_quaintity'] = $data['txtno_'.$i] ;
						$getdata['total_charge'] = $data['txttotal_'.$i] ;
						$getdata['pur_charge'] = $data['txtpur_'.$i] ;
						
						$getdata['created'] = date('Y-m-d H:i:s');
						$getdata['created_by'] = $this->session->userdata('userid');
						//$port_data['customer_id'] = $getdata['customer_id'];
						//$port_data['created'] = date('Y-m-d H:i:s');
						
						//$this->db->insert('an_customer_port_selection',$port_data);
						//$this->db->insert('an_cash_job_charges', $getdata);
						
						//==================payment start branch cash=========================================
						
					if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Cash'){
						
						//$this->UpdateBranchCash($branches_id,$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_cash_job_charges';
						//$this->UpdatePaidStatusOfCharges($tablename,$i);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $getdata['job_id'];
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						//$this->db->insert('an_cash_management',$companyDate);
					}
    				
					//==================payment end branch cash===========================================
						
					//==================payment start Account Cash=========================================
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Accounts' && isset($data['account_id'.$i])){	
						//$total_cash += $eachRec->total_charge;
						//$this->UpdateAccountCash($data['account_id'.$i],$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_cash_job_charges';
						//$this->UpdatePaidStatusOfCharges($tablename,$i);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $getdata['job_id'];
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['account_id'] = $data['account_id'.$i];
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						//$this->db->insert('an_company_transaction',$companyDate);
					}
					
					//==================payment end Account cash===========================================
						
						unset($data['txt_'.$i],$data['txtno_'.$i],$data['txttotal_'.$i],$data['txtpur_'.$i]);
					}
				}else if($data['account_type'] == 'contract'){
					if(isset($data['txt_'.$i]) && isset($data['txtpercharge_'.$i]) && isset($data['txttotal_'.$i]) && isset($data['txtpur_'.$i]) ){
						$getdata = array();
						
						$new_ids = $this->getLastJobId();
						$getdata['job_id'] = ($new_ids->Auto_increment);
						
						$getdata['customer_id'] = $new_id;
						$getdata['charge_id'] =  $i;
						$getdata['charge_quaintity'] = $data['txt_'.$i] ;
						$getdata['per_charge_amount'] = $data['txtpercharge_'.$i] ;
						$getdata['total_charge'] = $data['txttotal_'.$i] ;
						$getdata['pur_charge'] = $data['txtpur_'.$i] ;
						
						$getdata['created'] = date('Y-m-d H:i:s');
						$getdata['created_by'] = $this->session->userdata('userid');
						//$port_data['customer_id'] = $getdata['customer_id'];
						//$port_data['created'] = date('Y-m-d H:i:s');
						//$this->db->insert('an_customer_port_selection',$port_data);
						//$this->db->insert('an_contract_job_charges', $getdata);
						
					//=============payment start branch cash=========================================
						
					if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Cash'){
						
						//$this->UpdateBranchCash($branches_id,$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_contract_job_charges';
						//$this->UpdatePaidStatusOfCharges($tablename,$i);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $getdata['job_id'];
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						//$this->db->insert('an_cash_management',$companyDate);
					}
    				
					//==================payment end branch cash===========================================
						
					//==================payment start Account Cash=========================================
					
					else if(isset($data['way_of_payment'.$i]) && $data['way_of_payment'.$i]=='Accounts' && isset($data['account_id'.$i])){	
						//$total_cash += $eachRec->total_charge;
						//$this->UpdateAccountCash($data['account_id'.$i],$getdata['pur_charge']);
						//update query for cash
						// update status to paid
						$tablename = 'an_contract_job_charges';
						//$this->UpdatePaidStatusOfCharges($tablename,$i);
						//insert into table compnaytaransctions
						$companyDate = array();
						$companyDate['job_id'] = $getdata['job_id'];
						$companyDate['branch_id'] = $branches_id;
						$companyDate['customer_id'] = $customer_id;
						$companyDate['charge_id'] = $i;
						$companyDate['account_id'] = $data['account_id'.$i];
						$companyDate['transaction_type'] = 'credit';
						$companyDate['payment_type'] = 'deposite';
						$companyDate['value'] = $getdata['total_charge'];
						$companyDate['created'] = date('Y-m-d H:i:s');
						$companyDate['emp_id'] = $this->session->userdata('userid');
						//$this->db->insert('an_company_transaction',$companyDate);
					}
					
					//==================payment end Account cash===========================================
						
						unset($data['txt_'.$i]);
						unset($data['txtpercharge_'.$i],$data['txttotal_'.$i],$data['txtpur_'.$i]);
					}
				}
				unset($data['way_of_payment'.$i],$data['account_id'.$i]);
			}
			unset($data['count_txt']);
			unset($data['account_type'],$data['type_of_shipment']);
		} 
			if(isset($data['count_container'])){
				for($m=0;$m<=$data['count_container'];$m++){
					if(isset($data['container_number'.$m]) ){
						$containerData['job_id'] = $job_id;
						$containerData['container_number'] = $data['container_number'.$m];
						$this->db->insert('an_shipment_container',$containerData);
						
					}
					unset($data['container_number'.$m]);
				}
			}
			unset($data['count_container']);
			$newdata = array();			
			$newdata['customer_id'] = $data['customer_id'];
			$newdata['job_type'] = $data['job_type'];
			$newdata['way_of_shipment'] = $data['way_of_shipment'];
			$newdata['job_status'] = $data['job_status'];
			$newdata['total_value'] = $data['total_value'];
			$newdata['email_send'] = $data['email_send'];
			$newdata['review'] = $data['review'];
			$newdata['job_step_no'] = $data['job_step_no'];
			$newdata['container_category'] = $data['container_category'];
			
			unset($data['customer_id'],$data['job_type'],$data['way_of_shipment'],$data['job_step_no']);
			unset($data['job_status'],$data['total_value'],$data['email_send'],$data['review'],$data['charges_id'],$data['current_charges_id']);
			unset($data['charges_advance'],$data['sale_value'],$data['remaining'],$data['purchase_value'],$data['comment'],$data['notes'],$data['all_discount'],$data['received_amount']);
			unset($data['bank_id'],$data['supplier_value'],$data['container_category']);
			
			$new_ids = $this->getLastJobId();
			$data['created'] = date('Y-m-d H:i:s');
			$data['job_id'] = ($new_ids->Auto_increment);
			$this->db->insert('an_shipmentinfo',$data);
			
			
			$newdata['emp_id'] = $this->session->userdata('userid');
			$newdata['created'] = date('Y-m-d H:i:s');
			
			$this->db->insert('an_jobs', $newdata);
			$new_id = $this->db->insert_id();
			if($data['duty_charges'] !=""){
							
							//echo "<pre>";
							//print_r($data);
							$result_val = $data['duty_charges']+$data['additional_charges'];
							$job_charges['charges_advance'] = $result_val;
							$job_charges['sales_value'] = $result_val;
							$job_charges['purchase_value'] = 0;
							$job_charges['charges_id'] = 7;
							$job_charges['chareges_type'] = 'actual';
							$job_charges['job_id']   = $new_id;
							$this->addCharges($job_charges);
						}
			
			// this logic charges end
			
			// add here duty charges. only 
			$myjob_id = $new_id;
			$tablename = '';
			$ress = $this->customers->get_customer_detail($customer_id);
			$account_type_customer = $ress->account_type;
			if($account_type_customer == 'contract' /*&& $job_statuss != "waiting_for_customer"*/){
			
				$mydata = array();	
				$tablename = 'an_contract_job_charges';
				$mydata['job_id'] = $myjob_id;
				$mydata['customer_id'] = $customer_id;
				$mydata['charge_id'] =  '7';
				$mydata['charge_quaintity'] = '1' ;
				$mydata['per_charge_amount'] = '1' ;
				$mydata['total_charge'] = $duty_chares_for_job ;
				$mydata['created'] = date('Y-m-d H:i:s');
				$mydata['created_by'] = $this->session->userdata('userid');
				$this->db->insert('an_contract_job_charges', $mydata);
			}else if($account_type_customer == 'cash' /*&& $job_statuss != "waiting_for_customer"*/){
				
				$mydata = array();
				$tablename = 'an_cash_job_charges';
				$mydata['job_id'] = $myjob_id;
				$mydata['customer_id'] = $customer_id;
				$mydata['charge_id'] =  '7';
				$mydata['per_charge_amount'] = '1';
				$mydata['charge_quaintity'] = '1';
				$mydata['total_charge'] = $duty_chares_for_job ;
				$mydata['created'] = date('Y-m-d H:i:s');
				$mydata['created_by'] = $this->session->userdata('userid');
				$this->db->insert('an_cash_job_charges', $mydata);
			}else if($account_type_customer == 'fixed' /*&& $job_statuss != "waiting_for_customer"*/){
				
				$mydata = array();
				$tablename = 'an_fixed_job_charges';
				$mydata['job_id'] = $myjob_id;
				$mydata['customer_id'] = $customer_id;
				$mydata['charge_id'] =  '7';
				$mydata['per_charge_amount'] = '1' ;
				$mydata['charge_quaintity'] = '1' ;
				$mydata['total_charge'] = $duty_chares_for_job ;
				$mydata['created'] = date('Y-m-d H:i:s');
				$mydata['created_by'] = $this->session->userdata('userid');
				$this->db->insert('an_fixed_job_charges', $mydata);
			}
			
			// add here duty charges. only end
			
			// adding payments for cash start
			
			/*if($payment_type=='Cash'  && $job_statuss != "waiting_for_customer"){
	
				$rec = $this->getAllChargesForPayment($tablename,$myjob_id);
				//$total_cash=0;
				foreach($rec as $eachRec){
					//$total_cash += $eachRec->total_charge;
					$this->UpdateBranchCash($branches_id,$eachRec->total_charge);
					//update query for cash
					// update status to paid
					$this->UpdatePaidStatusOfCash($tablename,$eachRec->id);
					//insert into table compnaytaransctions
					$companyDate = array();
					$companyDate['job_id'] = $myjob_id;
					$companyDate['branch_id'] = $branches_id;
					$companyDate['customer_id'] = $customer_id;
					$companyDate['charge_id'] = $eachRec->id;
					$companyDate['branch_cash_id'] = $this->getBranchCashId($branches_id);
					$companyDate['transaction_type'] = 'credit';
					$companyDate['payment_type'] = 'deposite';
					$companyDate['created'] = date('Y-m-d H:i:s');
					$companyDate['emp_id'] = $this->session->userdata('userid');
					$this->db->insert('an_company_transaction',$companyDate);
				}
			}else if($payment_type=='Accounts'  && $job_statuss != "waiting_for_customer"){
				$rec = $this->getAllChargesForPayment($tablename,$myjob_id);
				//$total_cash=0;
				foreach($rec as $eachRec){
					//$total_cash += $eachRec->total_charge;
					$this->UpdateAccountCash($account_id,$eachRec->total_charge);
					//update query for cash
					// update status to paid
					$this->UpdatePaidStatusOfCash($tablename,$eachRec->id);
					//insert into table compnaytaransctions
					$companyDate = array();
					$companyDate['job_id'] = $myjob_id;
					$companyDate['branch_id'] = $branches_id;
					$companyDate['customer_id'] = $customer_id;
					$companyDate['charge_id'] = $eachRec->id;
					$companyDate['account_id'] = $account_id;
					$companyDate['transaction_type'] = 'credit';
					$companyDate['payment_type'] = 'deposite';
					$companyDate['created'] = date('Y-m-d H:i:s');
					$companyDate['emp_id'] = $this->session->userdata('userid');
					$this->db->insert('an_company_transaction',$companyDate);
				}
			}*/
			
			// adding payments for cash end
			
			//-----------------------------------------------------
			$config['upload_path'] = './uploads/jobs/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '100';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';
	
			$this->load->library('upload', $config);
	
			if ( ! $this->upload->do_upload('attachment'))
			{
				$error = array('error' => $this->upload->display_errors());
				//print_r($error);
				//$this->load->view('upload_form', $error);
				////do_redirect('options?e=10');
				do_redirect('add_jobs/'.$new_id.'?e=10');
			}
			else
			{
				$upload_data = $this->upload->data(); 
				
  				$file_name = $upload_data['file_name'];
				//$exts = split("[/\\.]", $file_name);
				//$n    = count($exts)-1;
				//$ext  = $exts[$n];
				
				$datas['attachment'] = $file_name;
				$datas['created'] = date('Y-m-d');
				$this->db->where('id', $new_id);
				$this->db->update('an_jobs', $datas);
				do_redirect('add_jobs/'.$new_id.'?e=10');
				//do_redirect('options?e=10');
			}
			
		}
	}
//----------------------------------------------------------------------
	function updateJobStatusToCancel($job_id=''){
		$sql = "UPDATE an_jobs SET job_status = 'cancel' WHERE id = ".$job_id;
			
		$q = $this->db->query($sql);
		do_redirect('options?e=13');
	}
	
	function updateJobStatusToClose($job_id=''){
		$sql = "UPDATE an_jobs SET job_status = 'close' WHERE id = ".$job_id;
			
		$q = $this->db->query($sql);
		do_redirect('options?e=13');
	}
	function updateImageIndb($datas='',$new_id=''){
		$this->db->where('id', $new_id);
		$this->db->update('an_jobs', $datas);
	}
	
	function getSupplierData(){
		
		$sql = "SELECT * FROM `an_suppliers`";
		$q = $this->db->query($sql);
		
		if($q->num_rows() > 0){
			
			$locs = $q->result();
			foreach($locs as $eachLoc){
				$supp[] = 
					array('sup'=>$eachLoc->supplier_name);
				
			}
		
				
		}
		else{
			return 0;
		}
		
		return $supp;
	}
	
	function updateImageIndbEmail($datas=''){
		
		$sql = "SELECT * FROM email_attachment WHERE created_by = ".$data['created_by'];
			
		$q = $this->db->query($sql);
		if($q->num_rows() > 0)
		{
			$this->db->where('created_by', $data['created_by']);
			$this->db->update('email_attachment', $datas);
		}else{
			$this->db->insert('email_attachment', $datas);
		}
	}
	function UpdatePaidStatusOfCharges($tablename='',$charge_id = '',$jobid=''){
		$sql = "UPDATE ".$tablename." SET is_company_paid = '1' WHERE total_charge != 0 and job_id = ".$jobid." and charge_id = ".$charge_id;
			
		$q = $this->db->query($sql);
		return true;
	}
	function UpdatePaidStatusOfCash($tablename='',$id = ''){
		$sql = "UPDATE ".$tablename." SET is_company_paid = '1' WHERE id = ".$id;
			
		$q = $this->db->query($sql);
		return true;
	}
	
	function UpdateBranchCash($branch_id='',$amount = '0'){
		$sql = "UPDATE an_branch_cash SET branch_cash = branch_cash - $amount WHERE branch_id = ".$branch_id;
			
		$q = $this->db->query($sql);
		return true;
	}
	
	function UpdateAccountCash($account_id='',$amount = '0'){
		$sql = "UPDATE an_bank_accounts SET account_cash = account_cash - $amount WHERE account_id = ".$account_id;
			
		$q = $this->db->query($sql);
		return true;
	}
	
	
	function getCustomerBranch($customer_id=''){
		$sql = "SELECT branch_id FROM an_customers WHERE id = ".$customer_id;
			
		$q = $this->db->query($sql);
		if($q->num_rows() > 0)
		{
			$res = $q->row();
			return $res->branch_id;
		}
	}
	function get_all_brancheswith($branch_id=''){
		$sql = "SELECT * FROM an_branch WHERE STATUS = 'A' and branch_id != ".$branch_id;
			
		$q = $this->db->query($sql);
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
	}
	
	function LoadAllBankAccount(){
		$sql = "SELECT * FROM an_bank_accounts WHERE STATUS = 'Active'";
			
		$q = $this->db->query($sql);
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
	}
	
	function getAllChargesForPayment($tablename='',$job_id=''){
		$sql = "SELECT * FROM ".$tablename." WHERE job_id = '".$job_id."' AND is_company_paid = '0' ";
			
		$q = $this->db->query($sql);
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
	}
	function getBranchCashId($branch_id=''){
		$sql = "SELECT id FROM `an_branch_cash` WHERE branch_id = ".$branch_id ;
			
		$q = $this->db->query($sql);
		if($q->num_rows() > 0)
		{
			$res = $q->row();
			return $res->id;
		}
	}
	// Created by M.Ahmed
	
	function get_customer_detail($customer_id=''){
		$sql = "SELECT 
				  * 
				FROM
				  an_customers 
				WHERE an_customers.`id` = '$customer_id' ";
		$q = $this->db->query($sql);
		return $q->row();
	}
	//created by M.Ahmed
	function get_charges_by_port($port='',$account_type='',$customer_id=''){
		$account_type='cash';
		if($account_type=='cash'){
			$sql = "SELECT 
					  * 
					FROM
					  `an_charges` 
					WHERE STATUS = 'A' ";
			
			if($port=='land')
			$sql .= " AND  land_port = '1' ";
			
			if($port=='sea')
			$sql .= " AND  sea_port = '1' ";
			
			if($port=='air')
			$sql .= " AND  air_port = '1' ";
			
			$sql.= " AND charge_id != '7' ";
		}else if($account_type=='fixed'){
			$sql = "SELECT 
					  * 
					FROM
					  `an_charges` AS c,
					  an_fixed_customer_charges AS f
					WHERE c.charge_id = f.`charge_id`
					AND
					c.STATUS = 'A' ";
				if($port=='land')
			$sql .= " AND  c.land_port = '1' ";
			
			if($port=='sea')
			$sql .= " AND  c.sea_port = '1' ";
			
			if($port=='air')
			$sql .= " AND  c.air_port = '1' ";
				
				
			$sql .= "
					  AND c.charge_id != '7' 
					  AND f.`customer_id` = ".$customer_id;
		}else if($account_type=='contract'){
			$sql = "SELECT 
					  * 
					FROM
					  `an_charges` AS c,
					  an_contract_customer_charges AS f
					WHERE c.charge_id = f.`charge_id`
					AND
					c.STATUS = 'A' ";
				if($port=='land')
			$sql .= " AND  c.land_port = '1' ";
			
			if($port=='sea')
			$sql .= " AND  c.sea_port = '1' ";
			
			if($port=='air')
			$sql .= " AND  c.air_port = '1' ";
				
				
			$sql .= "
					  AND c.charge_id != '7' 
					  AND f.`customer_id` = ".$customer_id;
		}
		
		$q = $this->db->query($sql);
		if($q->num_rows() > 0)
		{
			return $q->result_array();
		}
	}
	
	public function get_customer_charges_info_new($customerid='',$port='',$duty_charge='')
	{
		//$customerid=$this->input->post('customer_id');
		//$port=$this->input->post('port');
		//$duty_charge = $this->input->post('duty_charge');
			if($customerid	!=	'')
			{
				$customer = $this->_data['customer'] = $this->get_customer_detail($customerid);
				$charges_by_port = $this->get_charges_by_port($port,$customer->account_type);
				$i=1;
				
				if($customer->account_type == 'cash'){
					foreach($charges_by_port as $result){
						?>
                        
						<div style="height:89px;clear:left; border-top: 1px solid rgb(255, 255, 255); padding: 7px;" id="div_<?php echo $result['charge_id'];?>">
                        <label style="width:200px;float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter per charge."  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>"  onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  value="0" style="width:100px" /> &nbsp;&nbsp;
                        <label style="width:200px;float:left">No. of  <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter no." onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  class="formtxtfield" type="text" name="txtno_<?php echo $result['charge_id'];?>" id="txtno_<?php echo $result['charge_id'];?>" value="0" style="width:100px" /> &nbsp;&nbsp;
                        
                        <label style="width:200px;float:left">Total Purchase Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="total purchase"  class="totalpur formtxtfield" type="text" name="txtpur_<?php echo $result['charge_id'];?>" onchange="gettotalsof();" id="txtpur_<?php echo $result['charge_id'];?>" value="0" style="width:100px" /> 
                        <span style="margin-top:15px;">
                        <label style="width:200px;float:left">Total Sales Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="total value"  class="totalsale formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" value="0" style="width:100px"  /> 
                        </span>
                        <?php
						if($result['is_actual']=='1'){
							?>
                            
							<span style="margin-top:15px;">
						   <!-- <br /><br />-->
							<label style="width:200px;float:left">Select Payment Way &nbsp;&nbsp;</label>
							<div class="dropmenu" >
									<div class="styled-select">
							<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
									</div>
							</div>
							
							<span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
							<label style="width:200px;float:left">Select Account &nbsp;&nbsp;</label>
							<div class="dropmenu">
								<div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
								  
								</div>
							  </div>
							</span>
							</span>
							<?php
						}else{
							?>
                            <input type="hidden" name="<?php echo 'way_of_payment'.$result['charge_id'];?>" id="<?php echo 'way_of_payment'.$result['charge_id'];?>" value="Cash" />
                            <?php
						}
						?>
						<?php /*?><a class="del_txt" id="anc_<?php echo $result['charge_id'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php */?></div>
						<?php
						$i++;
					}
				}else if($customer->account_type == 'fixed'){
					foreach($charges_by_port as $result){
						 $valus1 = $this->get_fixed_customer_values($customer->id,$result['charge_id']);
						 $valus = isset($valus1->charge_value)?$valus1->charge_value:0;
						//print_r($result['is_actual']);
						?>
                        
						<div style="height:89px;clear:left; border-top: 1px solid rgb(255, 255, 255); padding: 7px;" id="div_<?php echo $result['charge_id'];?>">
                        <label style="width:200px;float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter per charge." class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" <?php echo ($valus>0)?'readonly="readonly"':'';?> value="<?php echo $valus;?>"   onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  style="width:100px"   />  
						<label style="width:200px;float:left">No. of <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter no." class="formtxtfield" type="text" name="txtno_<?php echo $result['charge_id'];?>" id="txtno_<?php echo $result['charge_id'];?>" value="0"   onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  style="width:100px"  />  
                        <label style="width:200px;float:left">Total Purchase Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="total purchase"  onchange="gettotalsof();" class="totalpur formtxtfield" type="text" name="txtpur_<?php echo $result['charge_id'];?>" id="txtpur_<?php echo $result['charge_id'];?>" style="width:100px"  value="0" />  
                        <span style="margin-top:15px;">
                        <label style="width:200px;float:left">Total Sales Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="total value." class="totalsale formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" style="width:100px"  value="0" /> 
                        </span> 
						<?php
						if($result['is_actual']=='1'){
							?>
                            
                        <span style="margin-top:15px;">
                       <!-- <br /><br />-->
						<label style="width:200px;float:left">Select Payment Way &nbsp;&nbsp;</label>
						<div class="dropmenu" >
                      	    	<div class="styled-select">
						<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
                        		</div>
                        </div>
                        
                        <span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
                        <label style="width:200px;float:left">Select Account &nbsp;&nbsp;</label>
                        <div class="dropmenu">
                            <div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
                              
                            </div>
                          </div>
                        </span>
                        </span>
                        <?php
                        }else{
							?>
                            <input type="hidden" name="<?php echo 'way_of_payment'.$result['charge_id'];?>" id="<?php echo 'way_of_payment'.$result['charge_id'];?>" value="Cash" />
                            <?php
						}
						?>
						<?php /*?><a class="del_txt" id="anc_<?php echo $result['charge_id'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php */?>
                        </div>
						<?php
						$i++;
					}
				}else if($customer->account_type == 'contract'){
					foreach($charges_by_port as $result){
						
						 $valus1 = $this->get_contract_customer_values($customer->id,$result['charge_id']);
						 $charge_quaintity = isset($valus1->charge_quaintity)?$valus1->charge_quaintity:0;
						 $per_charge_amount = isset($valus1->per_charge_amount)?$valus1->per_charge_amount:0;
						
						?>
						<div style="height:89px;border-top: 1px solid rgb(255, 255, 255); padding: 7px;" id="div_<?php echo $result['charge_id'];?>">
                        <label style="width:200px;float:left">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input type="text"  class="formtxtfield" placeholder="Enter per expense charges." name="txtpercharge_<?php echo $result['charge_id'];?>" id="txtpercharge_<?php echo $result['charge_id'];?>" value="<?php echo $per_charge_amount;?>"  onchange="calculate_total_cost_charges('<?php echo $result['charge_id'];?>');"  style="margin-left:10px; width:100px;" />&nbsp;&nbsp;
                        
                        <label style="width:200px;float:left">No. of <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input  onchange="calculate_total_cost_charges('<?php echo $result['charge_id'];?>');"  placeholder="Enter quantity"  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>" style="width:100px"  value="<?php echo $charge_quaintity;?>" />&nbsp;&nbsp; &nbsp;&nbsp;
                        
                        <label style="width:200px;float:left">Total Purchase Charges&nbsp;&nbsp;</label>
                        <input placeholder="Enter purchases" onchange="gettotalsof();"  class="totalpur formtxtfield" type="text" name="txtpur_<?php echo $result['charge_id'];?>" id="txtpur_<?php echo $result['charge_id'];?>" style="width:100px"  value="" />&nbsp;&nbsp; &nbsp;&nbsp;
                        <span style="margin-top:15px;">
                        <label style="width:200px;float:left">Total Sales Charges<?php //echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter quantity"  class="totalsale formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" style="width:100px"  value="<?php echo ($charge_quaintity*$per_charge_amount);?>" />&nbsp;&nbsp; &nbsp;&nbsp;
                        </span>
                        <?php
						if($result['is_actual']=='1'){
							?>
                            
                        <span style="margin-top:15px;">
                       <!-- <br /><br />-->
						<label style="width:200px;float:left">Select Payment Way &nbsp;&nbsp;</label>
						<div class="dropmenu" >
                      	    	<div class="styled-select">
						<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
                        		</div>
                        </div>
                        
                        <span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
                        <label style="width:200px;float:left">Select Account &nbsp;&nbsp;</label>
                        <div class="dropmenu">
                            <div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
                              
                            </div>
                          </div>
                        </span>
                        </span>
                        <?php
                        }else{
							?>
                            <input type="hidden" name="<?php echo 'way_of_payment'.$result['charge_id'];?>" id="<?php echo 'way_of_payment'.$result['charge_id'];?>" value="Cash" />
                            <?php
						}
						?>
						<?php /*?><a class="del_txt" id="anc_<?php echo $result['per_charge_amount'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php */?></div>
						<?php
						$i++;
					}
				}
				?>
                <?php
                //=======================================
                
					//$result['charge_id'] = 7;// 7 is for duty charges.
					//$result['charge_name'] =  "Duty Charges";
						?>
						<?php /*?><div style="height:89px;clear:left; border-top: 1px solid rgb(255, 255, 255); padding: 7px;" id="div_<?php echo $result['charge_id'];?>">
                        <label style="width:200px;float:left; visibility:hidden">Per <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter per charge."  class="formtxtfield" type="text" name="txt_<?php echo $result['charge_id'];?>" id="txt_<?php echo $result['charge_id'];?>"  onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  value="0" style="width:100px; visibility:hidden" /> &nbsp;&nbsp;
                        <label style="width:200px;float:left; visibility:hidden">No. of  <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="Enter no." onchange="calculate_total_cost('<?php echo $result['charge_id'];?>');"  class="formtxtfield" type="text" name="txtno_<?php echo $result['charge_id'];?>" id="txtno_<?php echo $result['charge_id'];?>" value="0" style="width:100px; visibility:hidden" /> &nbsp;&nbsp;
                        <label style="width:200px;float:left">Total <?php echo ucfirst($result['charge_name']);?> &nbsp;&nbsp;</label>
                        <input placeholder="total value"  class="formtxtfield" type="text" name="txttotal_<?php echo $result['charge_id'];?>" id="txttotal_<?php echo $result['charge_id'];?>" value="<?php echo $duty_charge;?>" style="width:100px" readonly="readonly" /> 
                        <span style="margin-top:15px;">
                       <!-- <br /><br />-->
						<label style="width:200px;float:left">Select Payment Way &nbsp;&nbsp;</label>
						<div class="dropmenu" >
                      	    	<div class="styled-select">
						<?php get_statusdropdown('','way_of_payment'.$result['charge_id'],'way_of_payment','',' onchange="getAllBankAccountsForCharges('.$result['charge_id'].');" '); ?>
                        		</div>
                        </div>
                        
                        <span id="<?php echo 'span'.$result['charge_id'];?>" style="display:none">
                        <label style="width:200px;float:left">Select Account &nbsp;&nbsp;</label>
                        <div class="dropmenu">
                            <div class="styled-select" id="div_accounts_responce<?php echo $result['charge_id'];?>">
                              
                            </div>
                          </div>
                        </span>
                        </span>
						<?php /*?><a class="del_txt" id="anc_<?php echo $result['charge_id'];?>" chgid="<?php echo $result['charge_id'];?>" href="#_">Remove</a><?php * /?></div><?php */?>
						<?php
						//$i++;
					
				
                //=======================================
                ?>
				<input type="hidden" name="count_txt" id="count_txt" value="<?php echo $i;?>" />
                <br />
                <label id="lbl_pur" style='float:left'>Purchase = 0 &nbsp;&nbsp;</label>  
                <label id="lbl_sales" style='float:left'>Sales = 0 &nbsp;&nbsp;</label> 
                <label id="lbl_profit" style='float:left'>Profit = 0 OMR &nbsp;&nbsp;</label>
                <?php
				
			}
	}
	function getDutyCharge($job_id=''){
		$sql = "SELECT duty_charges FROM an_shipmentinfo WHERE job_id= ".$job_id;
			
		$q = $this->db->query($sql);
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
	}
	
		//created by M.ahmed
	public function get_contract_customer_values($customer_id='',$charge_id='')
	{
		$sql = "SELECT 
				  * 
				FROM
				  an_contract_customer_charges
				WHERE customer_id='".$customer_id."' 
				AND charge_id = '".$charge_id."'";
		$q = $this->db->query($sql);
		
		if($q->num_rows() > 0)
		{
			return $q->row();
		}else{
			return '0';
		}
	}
	// Created By M.Ahmed
	
	function getJobTypePaidorUnpaid($job_id='',$job_status=''){
		//$addsql = " AND is_company_paid = '0' ";
		$addsql = "";
		if($job_id!=''){
			$sql = "SELECT * FROM an_contract_job_charges AS cj, an_charges AS c WHERE 
					cj.`charge_id` = c.`charge_id`
					AND job_id = '".$job_id."'  ".$addsql;
			$q = $this->db->query($sql);
			
			$q = $this->db->query($sql);
			if($q->num_rows() > 0)
			{
				return $q->result();
			}else{
				$sql = "SELECT * FROM an_fixed_job_charges AS cj, an_charges AS c WHERE 
						cj.`charge_id` = c.`charge_id`
						AND job_id = '".$job_id."' ".$addsql;
				$q = $this->db->query($sql);
				
				$q = $this->db->query($sql);
				if($q->num_rows() > 0)
				{
					return $q->result();
				}else{
					$sql = "SELECT * FROM an_cash_job_charges AS cj, an_charges AS c WHERE 
							cj.`charge_id` = c.`charge_id`
							AND job_id = '".$job_id."' ".$addsql;
					$q = $this->db->query($sql);
					
					$q = $this->db->query($sql);
					if($q->num_rows() > 0)
					{
						return $q->result();
					}else{
						return false;
					}
				}
			}
		}else{
			return false;
		}
		
	}
	
	function getTotalProfitAndLoss(){
		$sql = "SELECT 
 SUM(fxed.total_charge) AS sales,
 SUM(fxed.`pur_charge`) AS purchase,
 SUM(fxed.`total_charge`)-SUM(fxed.pur_charge) AS profit
FROM
  an_jobs AS j,
  an_customers AS c,
  an_shipmentinfo AS s,
  an_users AS u,
  an_fixed_job_charges AS fxed
WHERE j.`customer_id` = c.`id` 
  AND s.`job_id` = j.`id` 
  AND u.`userid` = j.`emp_id` 
  AND fxed.`job_id` = j.`id` 
  AND c.`status` = 'A' 
 
UNION 
SELECT 
SUM(cah.total_charge) AS sales,
 SUM(cah.`pur_charge`) AS purchase,
 SUM(cah.`total_charge`)-SUM(cah.pur_charge) AS profit
FROM
  an_jobs AS j,
  an_customers AS c,
  an_shipmentinfo AS s,
  an_users AS u,
  an_cash_job_charges AS cah
WHERE j.`customer_id` = c.`id` 
  AND s.`job_id` = j.`id` 
  AND u.`userid` = j.`emp_id` 
  AND cah.job_id = j.`id` 
  AND c.`status` = 'A' 
UNION
SELECT 
SUM(ctn.total_charge) AS sales,
 SUM(ctn.`pur_charge`) AS purchase,
 SUM(ctn.`total_charge`)-SUM(ctn.pur_charge) AS profit
FROM
  an_jobs AS j,
  an_customers AS c,
  an_shipmentinfo AS s,
  an_users AS u,
 an_contract_job_charges AS ctn
WHERE j.`customer_id` = c.`id` 
  AND s.`job_id` = j.`id` 
  AND u.`userid` = j.`emp_id` 
  AND ctn.`job_id` = j.`id` 
  AND c.`status` = 'A' 
";
$q = $this->db->query($sql);
			
			if($q->num_rows() > 0)
			{
				return $q->result();
			}
	}
	
	
	function getJobSalesAndProfit($job_id=''){
		$addsql = " ";
		
		if($job_id!=''){
			 $sql = "SELECT * FROM an_contract_job_charges AS cj, an_charges AS c WHERE 
					cj.`charge_id` = c.`charge_id`
					AND job_id = '".$job_id."'  ".$addsql;
			$q = $this->db->query($sql);
			
			if($q->num_rows() > 0)
			{
				$newsql = "SELECT SUM(total_charge) AS total_sales, SUM(pur_charge) AS total_purchase , SUM(total_charge)-SUM(pur_charge) AS profit FROM an_contract_job_charges AS cj, an_charges AS c WHERE 
					cj.`charge_id` = c.`charge_id`
					AND job_id = '".$job_id."'  ".$addsql;
					$q = $this->db->query($newsql);
				return $q->row();
			}else{
				$sql = "SELECT * FROM an_fixed_job_charges AS cj, an_charges AS c WHERE 
						cj.`charge_id` = c.`charge_id`
						AND job_id = '".$job_id."' ".$addsql;
				$q = $this->db->query($sql);
				
				if($q->num_rows() > 0)
				{
					$newsql = "SELECT SUM(total_charge) AS total_sales, SUM(pur_charge) AS total_purchase , SUM(total_charge)-SUM(pur_charge) AS profit FROM an_fixed_job_charges AS cj, an_charges AS c WHERE 
					cj.`charge_id` = c.`charge_id`
					AND job_id = '".$job_id."'  ".$addsql;
					$q = $this->db->query($newsql);
				
					return $q->row();
				}else{
					 $sql = "SELECT SUM(total_charge) AS total_sales, SUM(pur_charge) AS total_purchase , SUM(total_charge)-SUM(pur_charge) AS profit FROM an_cash_job_charges AS cj, an_charges AS c WHERE 
							cj.`charge_id` = c.`charge_id`
							AND job_id = '".$job_id."' ".$addsql;
					$q = $this->db->query($sql);
					
					if($q->num_rows() > 0)
					{
						$newsql = "SELECT SUM(total_charge) AS total_sales, SUM(pur_charge) AS total_purchase , SUM(total_charge)-SUM(pur_charge) AS profit FROM an_cash_job_charges AS cj, an_charges AS c WHERE 
					cj.`charge_id` = c.`charge_id`
					AND job_id = '".$job_id."'  ".$addsql;
					$q = $this->db->query($newsql);
						return $q->row();
					}else{
						return false;
					}
				}
			}
		}else{
			return false;
		}
		
	}
	
	
	function getJobType($job_id='',$job_status=''){
		$addsql = " AND is_company_paid = '0' ";
		
		if($job_id!=''){
			$sql = "SELECT * FROM an_contract_job_charges AS cj, an_charges AS c WHERE 
					cj.`charge_id` = c.`charge_id`
					AND job_id = '".$job_id."'  ".$addsql;
			$q = $this->db->query($sql);
			
			$q = $this->db->query($sql);
			if($q->num_rows() > 0)
			{
				return $q->result();
			}else{
				$sql = "SELECT * FROM an_fixed_job_charges AS cj, an_charges AS c WHERE 
						cj.`charge_id` = c.`charge_id`
						AND job_id = '".$job_id."' ".$addsql;
				$q = $this->db->query($sql);
				
				$q = $this->db->query($sql);
				if($q->num_rows() > 0)
				{
					return $q->result();
				}else{
					$sql = "SELECT * FROM an_cash_job_charges AS cj, an_charges AS c WHERE 
							cj.`charge_id` = c.`charge_id`
							AND job_id = '".$job_id."' ".$addsql;
					$q = $this->db->query($sql);
					
					$q = $this->db->query($sql);
					if($q->num_rows() > 0)
					{
						return $q->result();
					}else{
						return false;
					}
				}
			}
		}else{
			return false;
		}
		
	}
	
	function getAllLocations(){
		$sql = "select * from an_locations";
		$q = $this->db->query($sql);
		
		if($q->num_rows() > 0)
		{
			$locs = $q->result();
			foreach($locs as $eachLoc){
				$cities[] = 
					array('loc'=>$eachLoc->location);
				
			}
		}else{
			return '0';
		}
		
		
		return $cities;
	}
	
	//created by M.ahmed
	public function get_fixed_customer_values($customer_id='',$charge_id='')
	{
		$sql = "SELECT 
				  * 
				FROM
				  an_fixed_customer_charges
				WHERE customer_id='".$customer_id."' 
				AND charge_id = '".$charge_id."'";
		$q = $this->db->query($sql);
		
		if($q->num_rows() > 0)
		{
			return $q->row();
		}else{
			return '0';
		}
	}
	
	
	// Created By M.Ahmed
	
	function getDutyChargeswithJobTypeNotPaid($job_id='',$job_status=''){
		if($job_id!=''){
			$sql = "SELECT *,'an_contract_job_charges' as tablename FROM an_contract_job_charges AS cj, an_charges AS c WHERE 
					cj.`charge_id` = c.`charge_id` 
					AND cj.is_company_paid = '0'
					AND job_id = '".$job_id."'  LIMIT 0,1 ";
			$q = $this->db->query($sql);
			
			$q = $this->db->query($sql);
			if($q->num_rows() > 0)
			{
				return $q->row();
			}else{
				$sql = "SELECT *,'an_fixed_job_charges' as tablename FROM an_fixed_job_charges AS cj, an_charges AS c WHERE 
						cj.`charge_id` = c.`charge_id`
						AND cj.is_company_paid = '0'
						AND job_id = '".$job_id."'  LIMIT 0,1 ";
				$q = $this->db->query($sql);
				
				$q = $this->db->query($sql);
				if($q->num_rows() > 0)
				{
					return $q->row();
				}else{
					$sql = "SELECT *,'an_cash_job_charges' as tablename FROM an_cash_job_charges AS cj, an_charges AS c WHERE 
							cj.`charge_id` = c.`charge_id`
							AND cj.is_company_paid = '0'
							AND job_id = '".$job_id."'  LIMIT 0,1 ";
					$q = $this->db->query($sql);
					
					$q = $this->db->query($sql);
					if($q->num_rows() > 0)
					{
						return $q->row();
					}else{
						return false;
					}
				}
			}
		}else{
			return false;
		}
		
	}
	
	
	// Created By M.Ahmed
	
	function getDutyChargeswithJobType($job_id='',$job_status=''){
		if($job_id!=''){
			$sql = "SELECT *,'an_contract_job_charges' as tablename FROM an_contract_job_charges AS cj, an_charges AS c WHERE 
					cj.`charge_id` = c.`charge_id`
					AND job_id = '".$job_id."'  LIMIT 0,1 ";
			$q = $this->db->query($sql);
			
			$q = $this->db->query($sql);
			if($q->num_rows() > 0)
			{
				return $q->row();
			}else{
				$sql = "SELECT *,'an_fixed_job_charges' as tablename FROM an_fixed_job_charges AS cj, an_charges AS c WHERE 
						cj.`charge_id` = c.`charge_id`
						AND job_id = '".$job_id."'  LIMIT 0,1 ";
				$q = $this->db->query($sql);
				
				$q = $this->db->query($sql);
				if($q->num_rows() > 0)
				{
					return $q->row();
				}else{
					$sql = "SELECT *,'an_cash_job_charges' as tablename FROM an_cash_job_charges AS cj, an_charges AS c WHERE 
							cj.`charge_id` = c.`charge_id`
							AND job_id = '".$job_id."'  LIMIT 0,1 ";
					$q = $this->db->query($sql);
					
					$q = $this->db->query($sql);
					if($q->num_rows() > 0)
					{
						return $q->row();
					}else{
						return false;
					}
				}
			}
		}else{
			return false;
		}
		
	}
	// Created By M.Ahmed
	
	function getLastJobId(){
		$sql = "SHOW TABLE STATUS LIKE 'an_jobs'";
		$q = $this->db->query($sql);
		return $q->row();
	}
	
	
	// Created By M.Ahmed
	
	function get_customer_by_id($customer_id=''){
		$sql = "select * from an_customers where id = ".$customer_id;
		$q = $this->db->query($sql);
		return $q->row();
	}
	
	
	// Created By M.Ahmed
	
	function checkMandatoryFile($job_id=''){
		$return = false;
		$sql = "select * from an_jobs where id = ".$job_id." and mandatory_attachment != '' ";
		$q = $this->db->query($sql);
		if($q->num_rows() > 0){
			$return = 'present';
		}else{
			$return = false;
		}
		echo $return;
	}
	
	
	
	// Created By M.Ahmed
	
	function updatedoc_recipt_date($date='',$job_id=''){
		$return = false;
		$sql = "update an_shipmentinfo set doc_recipt_date = '$date' where job_id = '$job_id' ";
		$q = $this->db->query($sql);
		return true;
	}
	// Created By M.Ahmed
	// used in filter from and to date
	
	function getExpirejobsListsWithFilter($from='',$to=''){
		$sql = "SELECT 
					*,
					j.id AS job_id,
					u.`fullname` AS employee_name
				FROM
					an_jobs AS j,
					an_customers AS c,
					an_shipmentinfo AS s,
					an_users AS u
				WHERE j.`customer_id` = c.`id` 
				    AND j.created < DATE_SUB(CURDATE(), INTERVAL 2 DAY)
					AND s.`job_id` = j.`id` 
					AND j.job_status = 'cancel'
					AND u.`userid` = j.`emp_id`
					AND c.`status` = 'A' 
					AND j.`created`  BETWEEN '".$from."' AND '".$to."' 
				ORDER BY j.`id` DESC ;";
		$q = $this->db->query($sql);
		return $q->result();
	}
	// Created By M.Ahmed
	// used in filter from and to date
	
	function getCanceljobsListsWithFilter($from='',$to=''){
		$sql = "SELECT 
					*,
					j.id AS job_id,
					u.`fullname` AS employee_name
				FROM
					an_jobs AS j,
					an_customers AS c,
					an_shipmentinfo AS s,
					an_users AS u
				WHERE j.`customer_id` = c.`id` 
					AND s.`job_id` = j.`id` 
					AND j.job_status = 'cancel'
					AND u.`userid` = j.`emp_id`
					AND c.`status` = 'A' 
					AND j.`created`  BETWEEN '".$from."' AND '".$to."' 
				ORDER BY j.`id` DESC ;";
		$q = $this->db->query($sql);
		return $q->result();
	}
	// Created By M.Ahmed
	// used in filter from and to date
	
	function getjobsListsWithFilter($from='',$to=''){
		 /*$sql = "SELECT 
					*,
					j.id AS job_id,
					u.`fullname` AS employee_name
				FROM
					an_jobs AS j,
					an_customers AS c,
					an_shipmentinfo AS s,
					an_users AS u
				WHERE j.`customer_id` = c.`id` 
					AND j.job_status != 'cancel'
					AND s.`job_id` = j.`id` 
					AND u.`userid` = j.`emp_id`
					AND c.`status` = 'A' 
					AND j.`created`  BETWEEN '".$from."' AND '".$to."'
					AND job_id NOT IN (     
					SELECT job_id
					FROM an_jobs_invoice_relation) 
				ORDER BY j.`id` DESC ;";*/
				
				$sql = "SELECT j.*,
  j.id  AS job_id,
  u.`fullname` AS employee_name ,
  SUM(jc.`sales_value`) AS sales,
  SUM(jc.`purchase_value`) AS purchase,
  SUM(jc.`charges_advance`) AS advance  
FROM
 an_users AS u ,	
  an_customers AS c,
  an_shipmentinfo AS s,
  an_jobs AS j
 LEFT JOIN `jobs_charges` AS jc ON jc.`job_id` = j.`id`
WHERE j.`customer_id` = c.`id` 
  AND j.job_status != 'cancel' 
  AND s.`job_id` = j.`id` 
  AND j.`created`  BETWEEN '".$from."' AND '".$to."'
  AND u.`userid` = j.`emp_id` 
  AND c.`status` = 'A' 
  GROUP BY jc.`job_id`
ORDER BY j.`id` DESC ";
		$q = $this->db->query($sql);
		return $q->result();
	}
	
	
	// Created By M.Ahmed
	
	function getjobsLists($customer_id = ''){
	$sub_sq = '';
	if($customer_id){
		$sub_sq = ' AND j.`customer_id` = "'.$customer_id.'"';
	}
/*
SELECT 
  *,
  j.id AS job_id,
  u.`fullname` AS employee_name ,
    SUM(jc.`sales_value`) AS sales,
  SUM(jc.`purchase_value`) AS purchase,
  SUM(jc.`charges_advance`) AS advance,
  (SELECT 
    SUM(jp.`payment_amount`) 
  FROM
    `job_payment` AS jp 
  WHERE jp.`job_id` = j.id) AS payment
FROM
  an_customers AS c,
  an_shipmentinfo AS s,
  an_users AS u ,
    an_jobs AS j
   LEFT JOIN `jobs_charges` AS jc ON jc.`job_id` = j.`id`
   LEFT JOIN `job_payment` AS jp ON jp.`job_id` = j.`id`
WHERE j.`customer_id` = c.`id` 
  AND j.created BETWEEN DATE_SUB(CURDATE(), INTERVAL 2 DAY) 
  AND CURDATE() 
  AND j.job_status != 'cancel' 
  AND s.`job_id` = j.`id` 
  AND u.`userid` = j.`emp_id` 
  AND c.`status` = 'A' 
  ".$sub_sq."
    GROUP BY jc.`job_id`
ORDER BY j.`id` DESC
*/
	/*$sql="SELECT 
  *,
  j.id AS job_id,
  u.`fullname` AS employee_name ,
    SUM(jc.`sales_value`) AS sales,
  SUM(jc.`purchase_value`) AS purchase,
  SUM(jc.`charges_advance`) AS advance,
  SUM(jp.`payment_amount`) AS payment
FROM
  an_customers AS c,
  an_shipmentinfo AS s,
  an_users AS u ,
    an_jobs AS j
   LEFT JOIN `jobs_charges` AS jc ON jc.`job_id` = j.`id`
   LEFT JOIN `job_payment` AS jp ON jp.`job_id` = j.`id`
WHERE j.`customer_id` = c.`id` 
  AND j.created BETWEEN DATE_SUB(CURDATE(), INTERVAL 2 DAY) 
  AND CURDATE() 
  AND j.job_status != 'cancel' 
  AND s.`job_id` = j.`id` 
  AND u.`userid` = j.`emp_id` 
  AND c.`status` = 'A' 
  ".$sub_sq."
    GROUP BY jc.`job_id`
ORDER BY j.`id` DESC";*/
	$sql = "
SELECT 
  *,
  j.id AS job_id,
  u.`fullname` AS employee_name 
FROM
  an_customers AS c,
  an_shipmentinfo AS s,
  an_users AS u ,
    an_jobs AS j
WHERE j.`customer_id` = c.`id` 
  AND s.`job_id` = j.`id` 
  AND u.`userid` = j.`emp_id` 
  AND c.`status` = 'A' 
  ORDER BY j.`id` DESC";
		/*$sql = "SELECT 
  *,
  j.id AS job_id,
  u.`fullname` AS employee_name 
FROM
  an_jobs AS j,
  an_customers AS c,
  an_shipmentinfo AS s,
  an_users AS u 
WHERE j.`customer_id` = c.`id` 
  AND j.created BETWEEN DATE_SUB(CURDATE(), INTERVAL 2 DAY) 
  AND CURDATE() 
  AND j.job_status != 'cancel' 
  AND s.`job_id` = j.`id` 
  AND u.`userid` = j.`emp_id` 
  AND c.`status` = 'A'
  AND job_id NOT IN (     
SELECT job_id
FROM an_jobs_invoice_relation) 
ORDER BY j.`id` DESC  ;";*/
		$q = $this->db->query($sql);
		return $result = $q->result();
		//echo "<pre>";
		//print_r($result);
		//exit;
		
	}
	
	function getAlljobs(){
		$sql = "SELECT 
  *,
  j.id AS job_id 
FROM
  an_jobs AS j ORDER BY j.`id` DESC
";
		$q = $this->db->query($sql);
		return $q->result();
	}
	
	function getAllinvoices(){
			$sql = "SELECT * FROM `an_jobs_invoice_relation` AS i
GROUP BY i.`invoice_id` ORDER BY i.`invoice_id` DESC";
		$q = $this->db->query($sql);
		return $q->result();
	}
	
	
	// Created By M.Ahmed
	
	function getExpirejobsLists(){
		$sql = "SELECT 
					*,
					j.id AS job_id,
					u.`fullname` AS employee_name
				FROM
					an_jobs AS j,
					an_customers AS c,
					an_shipmentinfo AS s,
					an_users AS u
				WHERE j.`customer_id` = c.`id` 
					AND j.created < DATE_SUB(CURDATE(), INTERVAL 2 DAY)
					AND s.`job_id` = j.`id` 
					AND u.`userid` = j.`emp_id`
					AND c.`status` = 'A' 
				ORDER BY j.`id` DESC ;";
		$q = $this->db->query($sql);
		return $q->result();
	}
	
	// Created By M.Ahmed
	
	function getCanceljobsLists(){
		$sql = "SELECT 
					*,
					j.id AS job_id,
					u.`fullname` AS employee_name
				FROM
					an_jobs AS j,
					an_customers AS c,
					an_shipmentinfo AS s,
					an_users AS u
				WHERE j.`customer_id` = c.`id` 
					AND j.job_status = 'cancel'
					AND s.`job_id` = j.`id` 
					AND u.`userid` = j.`emp_id`
					AND c.`status` = 'A' 
				ORDER BY j.`id` DESC ;";
		$q = $this->db->query($sql);
		return $q->result();
	}
	// Created by M.Ahmed
	
	function loadjobsWithStatus($customer_id,$job_status){
		$sql = "SELECT * FROM an_jobs 
				WHERE customer_id = '".$customer_id."'
				AND job_status = '".$job_status."'";
		
		$q = $this->db->query($sql);
		return $q->result();
	}
	// Created by M.Ahmed
	
	function get_all_customers($branch_id=''){
		$sql = "SELECT 
				  * 
				FROM
				  an_customers 
				WHERE an_customers.`status` = 'A' ";
		if($branch_id!=''){
			$sql .= " and branch_id = $branch_id ";
		}
		$q = $this->db->query($sql);
		return $q->result();
	}
	// Created by M.Ahmed
	
	function get_job_detail($job_id=''){
		$sql = "SELECT 
				  * 
				FROM
				  an_jobs ,
				  an_shipmentinfo
				WHERE an_jobs.`id` = an_shipmentinfo.`job_id`
				AND an_jobs.`id` =   ".$job_id;
		$q = $this->db->query($sql);
		return $q->row();
	}
	
	function getjob_containers($job_id){
		$sql = "SELECT * FROM `an_shipment_container` AS sc WHERE sc.`job_id`= ".$job_id;
		$q = $this->db->query($sql);
		return $q->result();
	}
	
	function getChargesData($job_id){
		  /*$sql = "SELECT c.`charge_id`,c.`charge_name`, jc.*,s.`supplier_name` FROM `jobs_charges` AS jc
INNER JOIN `an_charges` AS c
ON c.`charge_id` = jc.`charges_id`
LEFT JOIN `an_suppliers` AS s ON s.`id` = jc.`purchase_id`
WHERE jc.`job_id` = ".$job_id." AND jc.chareges_type  IS NULL";*/	
		$sql = "SELECT 
				  c.`charge_id`,
				  c.`charge_name`,
				  jc.*,
				  s.`supplier_name`,
				  jt.`container_number`,
				  t.`truck_number`
				FROM
				  `jobs_charges` AS jc 
				  INNER JOIN `an_charges` AS c 
					ON c.`charge_id` = jc.`charges_id` 
				  LEFT JOIN `an_suppliers` AS s 
					ON s.`id` = jc.`purchase_id` 
				  LEFT JOIN `jobs_trucks` AS jt 
					ON jt.`job_charge_id`= jc.`jb_c_id` 
				  LEFT JOIN `an_trucks` AS t 
					ON t.`id` = jt.`job_truck_id` 
				WHERE jc.`job_id` = ".$job_id." 
				  AND jc.chareges_type IS NULL AND jc.charges_id!=9";
		$q = $this->db->query($sql);
		return $q->result();
	}
	
	
	
	
	function getPaymentsData($job_id){
		$sql = "SELECT * FROM job_payment WHERE `job_id` = ".$job_id."";	
		$q = $this->db->query($sql);
		return $q->result();
	}
	function getLocationForAutoSerach($locationName=''){
		$sql = "select * from an_locations where location = '".$locationName."' ";
		$q = $this->db->query($sql);
		if($q->num_rows() > 0)
		{
			
		}else{
			$getData = array();
			$getData['location'] = $locationName;
			$this->db->insert('an_locations', $getData);
		}
	}
	
	
	
}
?>