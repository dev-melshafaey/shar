<?php foreach ($holding_orders as $key => $order) { ?>
    <div class=" col-md-4 panel-container" >
        <div class="panel panel-danger" onclick="workOnThisOrder(this)">
            <div class="panel-heading">
                <h3 class="panel-title "> <i class="glyphicon glyphicon-info-sign"></i>  # <?= $order[0]['invoice_id'] ?>  <span class="pull-right inv_date" style="margin-right: 40px;"><?= $order[0]['invoice_date'] ?></span></h3>
                <ul class="list-inline panel-actions pull-right">
                    <li><a href="#" class="fullscreen" role="button" title="Toggle fullscreen"><i class="glyphicon glyphicon-zoom-in"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th style="width: 10%;">#</th><th style="width: 55%;">Item <br> الصنف</th><th style="width: 20%;">Size<br> الحجم</th><th style="width: 15%;">Qty <br> الكمية</th></tr>
                    </thead>
                    <tbody>
                        <?php foreach ($order as $key => $value) { ?>
                            <tr>
                                <td><?= ++$key ?></td>
                                <td><b><?= _s($value['itemname'], 'arabic') . "<br>" . _s($value['itemname'], 'english'); ?><br><u class='text-info'><?= $value['item_ar_name'] . "<br>" . $value['item_eng_name']; ?></u></b></td>
                                <td><?= ($value['invoice_item_price'] == $value['large_price']) ? "Large<br>كبير" : (($value['invoice_item_price'] == $value['medium_price']) ? 'Medium<br>وسط' : "Small<br>صغير"); ?></td>
                                <td><?= $value['invoice_item_quantity']; ?></td></tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer panel-footer-custom-red" >
                  <label class="pull-right"><?= $order[0]['fullname'] ?><br><?= $order[0]['sales_name'] ?></label>
                <!--<button class="btn btn-danger actionBtn" style="display: none">cancel&Tab;الغاء</button>-->
                <button class="btn btn-success btn-lg actionBtn" onclick="hideOrder(this,<?= $order[0]['order_id'] ?>)" style="visibility: hidden"><i class="glyphicon glyphicon-ok"></i></button>
            </div>
        </div>
    </div>
<?php } ?>

<script>
    var count = $('.badge').html();
    $('.badge').html(parseInt(count) +<?= count($holding_orders); ?>);
</script>

