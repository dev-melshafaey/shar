
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>POS System</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>pos_assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/loadercss.css" rel="stylesheet">
        <!-- Reset -->
        <link href="<?php echo base_url(); ?>pos_assets/css/normalize.css" rel="stylesheet">
        <!-- Custom -->
        <link href="<?php echo base_url(); ?>pos_assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>pos_assets/css/screens.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>newcss/style.css" rel="stylesheet">
        <!-- Select -->
        <link href="<?php echo base_url(); ?>pos_assets/css/bootstrap-select.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo base_url(); ?>pos_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Owl Carousel -->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <ul class="nav navbar-nav navbar-left" style="margin-top: 5px;">       
                        <li><img src="<?php echo base_url(); ?>img/logo.ico" alt="Business Solutions"  class="img-responsive img-circle"></li>
                        <li><h4>Business Solution by Durar Smart Solutions LLC &copy; 2018</h4> </li>
                    </ul>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right" >                                              
                        <li>
                            <button class="btn btn-danger" type="button">
                                Total Invoices <br> عددالفواتير  <span class="badge"><?= count($holding_orders); ?></span>
                            </button>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid" id="cont">
            <?php foreach ($holding_orders as $key => $order) { ?>
                <div class=" col-md-4 panel-container" >
                    <div class="panel panel-danger" >
                        <div class="panel-heading">
                            <h3 class="panel-title "> <i class="glyphicon glyphicon-info-sign"></i>  # <?= $order[0]['invoice_id'] ?>  <span class="pull-right inv_date" style="margin-right: 40px;"><?= $order[0]['invoice_date'] ?></span></h3>
                            <ul class="list-inline panel-actions pull-right">
                                <li><a href="#" class="fullscreen" role="button" title="Toggle fullscreen"><i class="glyphicon glyphicon-zoom-in"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body" onclick="workOnThisOrder(this)">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr><th style="width: 10%;">#</th><th style="width: 55%;">Item <br> الصنف</th><th style="width: 20%;">Size<br> الحجم</th><th style="width: 15%;">Qty <br> الكمية</th></tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($order as $key => $value) { ?>
                                        <tr>
                                            <td><?= ++$key ?></td>
                                            <td><b><?= _s($value['itemname'], 'arabic') . "<br>" . _s($value['itemname'], 'english'); ?><br><u class='text-info'><?= $value['item_ar_name'] . "<br>" . $value['item_eng_name']; ?></u></b></td>
                                            <td><?= ($value['invoice_item_price'] == $value['large_price']) ? "Large<br>كبير" : (($value['invoice_item_price'] == $value['medium_price']) ? 'Medium<br>وسط' : "Small<br>صغير"); ?></td>
                                            <td><?= $value['invoice_item_quantity']; ?></td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="panel-footer panel-footer-custom-red" >
                            <label class="pull-right"><?= $order[0]['fullname'] ?><br><?= $order[0]['sales_name'] ?></label>
                            <!--<button class="btn btn-danger actionBtn" style="display: none">cancel&Tab;الغاء</button>-->
                            <button class="btn btn-success btn-lg actionBtn" onclick="hideOrder(this,<?= $order[0]['order_id'] ?>)" style="visibility: hidden"><i class="glyphicon glyphicon-ok"></i></button>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>


        <script src="<?php echo base_url(); ?>pos_assets/js/jquery.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>pos_assets/js/bootstrap.min.js"></script>
        <!-- Select Picker -->
        <script src="<?php echo base_url(); ?>pos_assets/js/bootstrap-select.js"></script>
        <!-- Owl Carousel -->

    </body>
</html>

<script>
                            function hideOrder(btn, order_id) {
                                $.ajax({
                                    type: 'post',
                                    url: "<?= base_url() ?>kitchen/orderDone",
                                    dataType: 'json',
                                    data: {order: order_id},
                                    success: function (data) {
                                        if (data) {
                                            $(btn).parents('.panel-container').hide('slow', function () {
                                                $(btn).parents('.panel-container').remove();
                                            });
                                            var count = $('.badge').html();
                                            $('.badge').html(parseInt(count) - 1);
                                        }
                                    }
                                });
                            }
                            function workOnThisOrder(panel_body) {
                                $(panel_body).parent('.panel').removeClass('panel-danger').addClass('panel-success');
                                $(panel_body).next('.panel-footer').removeClass('panel-footer-custom-red').addClass('panel-footer-custom-green');
                                $(panel_body).next('.panel-footer').children('.actionBtn').css('visibility', 'visible');
                            }
                            function getNewOrders() {
                                $.ajax({
                                    type: 'get',
                                    url: "<?= base_url() ?>kitchen/addNew/<?= $type ?>/",
                                    dataType: 'html',
                                    success: function (html) {
                                        $('#cont').append(html);
                                    }
                                });
                            }

                            $(document).ready(function () {
                                setInterval(function () {
                                    getNewOrders();
                                }, 60000);
                               
                                $("#cont").on("click",'.fullscreen', function (e) {
                                    e.preventDefault();

                                    var $this = $(this);

                                    if ($this.children('i').hasClass('glyphicon-resize-full'))
                                    {
                                        $this.children('i').removeClass('glyphicon-resize-full');
                                        $this.children('i').addClass('glyphicon-resize-small');
                                    }
                                    else if ($this.children('i').hasClass('glyphicon-resize-small'))
                                    {
                                        $this.children('i').removeClass('glyphicon-resize-small');
                                        $this.children('i').addClass('glyphicon-resize-full');
                                    }
                                    $(this).closest('.panel').toggleClass('panel-fullscreen');
                                });                               
                            });

</script>

