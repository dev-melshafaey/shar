<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kitchen_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getholdingOrders($categ, $flag) {
        $query = "SELECT * FROM (`hold_orders_view`) WHERE `categ_type` = '$categ' ";
        $last_date = $this->getLastInvoiceDate();

        if (!$flag && $last_date != null) {
            $query .= " and `invoice_date` > '$last_date' ";
        }
        $query .= " ORDER BY `invoice_date` ";
        $query = $this->db->query($query);

        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            $this->setLastInvoiceDate($data);
            return $data;
        } else {
            return [];
        }
    }

    public function getLastInvoiceDate() {
        $last_date = $this->session->userdata('last_date');
        if ($last_date) {
            return $last_date;
        } else {
            return null;
        }
    }

    public function setLastInvoiceDate($data) {
        $max = max(array_map(function( $row ) {
                    return $row['invoice_date'];
                }, $data));
        $this->session->set_userdata(array(
            'last_date' => $max,
        ));
    }

    public function orderDone($order) {
        $this->db->where('order_id', $order);
        return $this->db->update('orders', array('order_status' => 'ready'));
    }

}
