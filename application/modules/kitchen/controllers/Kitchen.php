<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kitchen extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('bs_helper');
        $this->load->model('Kitchen_model', 'kitchen');
    }

    public function orders($type) {
        $reuslt = $this->kitchen->getholdingOrders($type,TRUE);
        foreach ($reuslt as $key => $value) {
            $return[$value['invoice_id']][] = $value;
        }

        $data['holding_orders'] = $return;
        $data['type'] = $type;
        $this->load->view('kitchen', $data);
    }

    public function addNew($type) {
        $reuslt = $this->kitchen->getholdingOrders($type,FALSE);
        foreach ($reuslt as $key => $value) {
            $return[$value['invoice_id']][] = $value;
        }
        $data['holding_orders'] = $return;
        $data['type'] = $type;
        print $this->load->view('panel', $data, true);
    }
    

    public function orderDone() {
        $order = $this->input->post('order');
        $reuslt = $this->kitchen->orderDone($order);
        echo json_encode($reuslt);
    }

}
