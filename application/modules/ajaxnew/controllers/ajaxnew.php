<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ajaxnew extends CI_Controller
{
	private $_data	=	array();

//----------------------------------------------------------------------
	/*
	* Constructor
	*/
   public function __construct()
   {
		parent::__construct();
		
		//load Home Model
	   $this->load->model('ajax/ajax_model','ajax');
   }
   
   
   	function removePartner(){
		$partner_id = $this->input->post('partner_id');
		
		$data['is_delete'] = 1;
		$condition = array('partner_id'=>$partner_id);
		echo $this->ajax->update_db('company_partners',$data,$condition);
	}
	
	function deleteFile(){

		$document_id = $this->input->post('document_id');
		$doc_type = $this->input->post('doc_type');
		
		$data['is_delete'] = 1;
		$condition = array('document_id'=>$document_id);
		echo $this->ajax->update_db('compnay_documents',$data,$condition);
	}
	
	function deleteCompany(){
			
		$company_id = $this->input->post('company_id');
		$data['is_delete'] = 1;
		$condition = array('companyid'=>$company_id);
		echo $this->ajax->update_db('bs_company',$data,$condition);
		
	
	}
    function addCompanyData(){
	   	 $this->load->model('company_management/company_management_model','company');
			$data = $this->input->post();
				if($this->input->post())
		{
			// Upload a selected file and get name
			$companyid = $this->input->post('company_id');
			
			

				
			$data['ownerid']	=	ownerid();
			$data['company_name']	=	json_encode(array('en' =>	$this->input->post('company_name_en'),'ar' =>	$this->input->post('company_name_ar')));
			unset($data['submit_company']);
			unset($data['company_name_en']);
			unset($data['company_name_ar']);
			
			// insert data into database
			//echo "<pre>";
			//print_r($data);
			//exit;	
				
			if($companyid!='')
			{
				if($data['formstep'] == 2){
				
					 $total = count($data['partnername']);
					for($a=0;$a<=$total;$a++) {	
						if($data['partnername'][$a] !=""){
							
							$partnrData['partnername'] = $data['partnername'][$a];
							$partnrData['partnerphone'] = $data['partnerphone'][$a];
							$partnrData['Partnershare'] = $data['Partnershare'][$a];
							$partnrData['partneremail'] = $data['partneremail'][$a];
							$partnrData['company_id'] = $data['company_id'] ;
							if(isset($data['partner_id'][$a]) && $data['partner_id'][$a]!=""){
								 $condition = array('partner_id'=>$data['partner_id'][$a]);
								 $this->ajax->update_db('company_partners',$partnrData,$condition);
							}
							else{
								
								 $this->ajax->insertDatabase('company_partners',$partnrData);
							}
							
						}
					}
					
												
				}
				elseif($data['formstep'] == 3){
						
						
						$total = count($data['document_title']);
						
						for($a=0;$a<=$total;$a++) {	
						if($data['document_date'][$a] !="" && !$data['document_id'][$a]){
							$documentData['document_title'] = $data['document_title'][$a];
							$documentData['document_date'] = date('Y-m-d',strtotime($data['document_date'][$a]));
							$documentData['document_image'] = $data['document_logo'][$a];
							$documentData['company_id'] = $data['company_id'];
							 $this->ajax->insertDatabase('compnay_documents',$documentData);
							}
							else{
								$documentData['document_title'] = $data['document_title'][$a];
								$documentData['document_date'] = date('Y-m-d',strtotime($data['document_date'][$a]));
								$documentData['document_image'] = $data['document_logo'][$a];
								
								$condition = array('document_id'=>$data['document_id'][$a]);
								//echo "<pre>";
								//print_r($documentData);
								//print_r($condition);
								$this->ajax->update_db('compnay_documents',$documentData,$condition);
							
							}
						}
						
						$total = count($data['company_logo']);
						
						for($a=0;$a<=$total;$a++) {	
						if($data['company_logo'][$a] !=""){
							if(!$data['logo_id'][$a]){
								$logoData['logo_name'] = $data['company_logo'][$a]  ;
								$logoData['company_id'] = $data['company_id'] ;
								 $this->ajax->insertDatabase('company_logos',$logoData);
							}
						}
					}
				
				
			//	$this->company->update_company($data,$companyid);
				}
				else{
						$company_id = $data['company_id'] ;
						unset($data['company_id']);	
						$this->company->update_company($data,$companyid);

				}

				
					$response = array('status'=>'success');
		}
		else{	
				$compnay_id = $this->company->add_company($data);
				$response = array('id'=>$compnay_id,'status'=>'success');
			} 
			
			echo  json_encode($response);
   	}
   }
   
}