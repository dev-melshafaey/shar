<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class ajax extends CI_Controller {

    /*

     * Properties

     */



    private $_data = array();



//----------------------------------------------------------------------

    /*

     * Constructor

     */

    public function __construct() {

        parent::__construct();



        //load Home Model

        $this->load->model('ajax_model', 'ajax');

        $this->lang->load('main', get_set_value('site_lang'));

    }



    public function get_permission() {

        $ownerid = $this->session->userdata('userid');

    }

    function getAmountLimitations(){

        $result  = $this->ajax->getPendingLimitCustomerAmount();

        ///echo "<pre>";

        //print_r($result);

        $arr = array();

        if(!empty($result)){

            foreach($result as $res){

                // echo "<pre>";

                //print_r($res);

                if($res->remaining>0){

                    if($res->sales>$res->totalpaid){

                        if($res->remaining>= $res->amount_limit){

                            $notification['userid'] = $res->userid;

                            $notification['fname'] = $res->fullname;

                            $notification['rem'] = $res->remaining;

                            $arr[] = json_encode($notification);



                        }

                    }





                }

            }

            echo json_encode($arr);

        }

        else{

            echo  false;

        }

    }

    function getDaysLimitNotifications(){

        $result  = $this->ajax->getPendingDaysInvoicesNotification();



        $arr = array();

        if(!empty($result)){

            foreach($result as $res){

               // echo "<pre>";

                //print_r($res);

                $arr[] = json_encode($res);

            }

            echo json_encode($arr);

        }

        else{

            echo  false;

        }

    }

    function addSalesData() {

        ///echo "<pre>";

        //print_r($_POST);

        //echo $_POST['sales'];

        parse_str($_POST['sales'], $salesData);

        //echo $values['input1']; //Outputs 'meeting'

        //$salesData =  unserialize(urldecode($_POST['sales']));

        //$salesData = unserialize($_POST['sales']);



        $emCode = $this->ajax->getLastEmployeeCode();

        $employeecode = $emCode->employeecode + 1;

        //print_r($salesData);

        //exit;

        $data['fullname'] = $salesData['fullname'];

        $data['username'] = $salesData['fullname'];

        $data['phone_number'] = $salesData['mobile'];

        $data['employeecode'] = $employeecode;

        $data['ownerid'] = $salesData['ownerId'];

        if ($salesData['companyid'] != "")

            $data['companyid'] = $salesData['companyid'];

        if ($salesData['branchid'] != "")

            $data['branchid'] = $salesData['branchid'];

        $data['office_number'] = $salesData['phone'];

        $this->ajax->insertDatabase('bs_users', $data);

        $userId = $this->ajax->InsertId();



        $data2['bs_sales_salary'] = $salesData['salary'];

        $data2['bs_sales_commession_type'] = $salesData['month1'];

        $data2['bs_sales_commession'] = $salesData['commession'];

        $data2['bs_user_id'] = $userId;

        $return = $this->ajax->insertDatabase('bs_sales', $data2);

        $salerId = $this->db->insert_id(); //$this->ajax->InsertId();

        //	echo $respnse = '<option value="ss">asas</option>';

        if ($return) {

            $response['saler_id'] = $salerId;

            $response['employee_code'] = $employeecode;

            echo json_encode($response);

            ///echo $respnse = '<option value="'+$salerId+'">'+$data['fullname']+'</option>';

        } else {

            echo false;

        }

    }



    function addCompanyData() {



        $this->load->model('company_management/company_management_model', 'company');

        if ($this->input->post()) {

            // Upload a selected file and get name

            $companyid = $this->input->post('company_id');













            $data['ownerid'] = ownerid();

            $data['company_name'] = json_encode(array('en' => $this->input->post('company_name_en'), 'ar' => $this->input->post('company_name_ar')));

            unset($data['submit_company']);

            unset($data['company_name_en']);

            unset($data['company_name_ar']);



            // insert data into database

            if ($companyid != '') {

                if ($data['formstep'] == 2) {



                    echo $total = count($data['partnername']);

                    for ($a = 0; $a <= $total; $a++) {

                        if ($data['partnername'][$a] != "") {

                            $partnrData['partnername'] = $data['partnername'][$a];

                            $partnrData['partnerphone'] = $data['partnerphone'][$a];

                            $partnrData['Partnershare'] = $data['Partnershare'][$a];

                            $partnrData['partneremail'] = $data['partneremail'][$a];

                            $partnrData['company_id'] = $data['company_id'];

                            echo $this->ajax->insertDatabase('company_partners', $partnrData);

                        }

                    }

                } elseif ($data['formstep'] == 3) {

                    $total = count($data['document_title']);

                    for ($a = 0; $a <= $total; $a++) {

                        if ($data['document_date'][$a] != "" && !$data['document_id'][$a]) {

                            $documentData['document_title'] = $data['document_title'][$a];

                            $documentData['document_date'] = $data['document_date'][$a];

                            $documentData['document_logo'] = $data['document_logo'][$a];

                            $documentData['company_id'] = $data['company_id'];

                            echo $this->ajax->insertDatabase('compnay_documents', $documentData);

                        }

                    }



                    $total = count($data['company_logos']);



                    for ($a = 0; $a <= $total; $a++) {

                        if ($data['logo_name'][$a] != "") {

                            if (!$data['logo_id'][$a]) {

                                $logoData['logo_name'] = $data['logo_name'][$a];

                                $logoData['company_id'] = $data['company_id'];

                                echo $this->ajax->insertDatabase('compnay_documents', $logoData);

                            }

                        }

                    }

                    //	$this->company->update_company($data,$companyid);

                    $response = array('status' => 'error');

                } else {

                    $compnay_id = $this->company->add_company($data);

                    $response = array('id' => $compnay_id, 'status' => 'success');

                }



                echo json_encode($response);

            }

        }

    }



    function getuserData() {

        

    }

function getCategoryProducts(){

			    $category_id = $this->input->post('category_id');

				$category_data = $this->ajax->getCategoryProducts($category_id);

				$htm = '';

				if($category_data){

						//echo "<pre>";

						//print_r($category_data);

						$count = 1;

						//$htm.=

						// $totalproducts = count($category_data);

						/*if($totalproducts>0){

							for($a=0;$a<$totalproducts;$a++){

										

								echo $a;	if($categ->product_picture == 'no-image.png'){

										$htm.='<div class="col-sm-4 col-md-2"><div class="btnPro"><img src="'.base_url().'pos_assets/images/beverage.jpg" class="img-responsive"><h2>'.$item['english'].'</h2></div></div>';

								}

								else{

									$htm.='<div class="col-sm-4 col-md-2"><div class="btnPro"><img src="'.base_url().'uploads/'.$item['product_picture'].'" class="img-responsive"><h2>'.$item['english'].'</h2></div></div>';

								}

							}

						}*/

						//exit;

						foreach($category_data as $i=>$categ){

							

								$item =  unserialize($categ->itemname);	

								//print_r($categ);

								//print_r($jsname);

								//echo $jsname;

								if($count == 1){

								//echo $count;

									$htm.='<div class="cslide-slide">';

								}

								//echo $count;

								//echo $i;

								if($categ->product_picture == 'no-image.png'){

										$htm.='<div class="col-sm-4 col-md-2" onclick="addproduct('.$categ->itemid.')"><div class="btnPro"><img src="'.base_url().'pos_assets/images/beverage.jpg" class="img-responsive"><h2>'.$item['english'].'</h2></div></div>';

								}

								else{

									$htm.='<div class="col-sm-4 col-md-2"><div class="btnPro"><img src="'.base_url().'uploads/'.$item['product_picture'].'" class="img-responsive"><h2>'.$item['english'].'</h2></div></div>';

								}

								if($count == 24){

								

									$htm.='</div>';

									$count = 0;

								}

								$htm.='<input type="hidden" id="item_id'.$categ->itemid.'" name="item_id'.$categ->itemid.'" value="'.$categ->itemid.'">';

								$htm.='<input type="hidden" id="item_name'.$categ->itemid.'" name="item_name'.$categ->itemid.'" value="'.$item['english'].'">';

								$htm.='<input type="hidden" id="min_val'.$categ->itemid.'" name="min_val'.$categ->itemid.'" value="'.$categ->min_sale_price.'">';

								$htm.='<input type="hidden" id="sale_price'.$categ->itemid.'" name="sale_price'.$categ->itemid.'" value="'.$categ->sale_price.'">';

								$count++;

								

								

								

						}

							

							echo $htm;

				}

	}

	

    function getAutoSearchProducts() {

        //echo "<pre>";

        //print_r($_POST);

        //print_r($_GET);

        //exit;



        if (is_numeric($_GET['category']) && $_GET['category']) {

            // if ($_GET['term'] == "")

            $term = trim(strip_tags($_GET['term']));

            if (isset($_GET['category']) && $_GET['category'] != "" && $_GET['category'] != "Select" && $_GET['category'] != "اختر") {

                $category = trim(strip_tags($_GET['category']));

                $storeid = $_GET['storeid'];

                $return = $this->ajax->getSearchProduct($term, $category,$storeid);

            } else {

                $return = $this->ajax->getSearchProduct($term);

            }



            //$return = $this->ajax->getSearchProduct($term);

            foreach ($return as $eachloc) {

                // Add the necessary "value" and "label" fields and append to result set

               $eachloc['value'] = $eachloc['id'];

                $eachloc['label'] = "{$eachloc['prod']}";

				$eachloc['saleprice'] = $eachloc['sale_price'];

				$eachloc['minsaleprice'] = $eachloc['min_sale_price'];

                $matches[] = $eachloc;

            }

            // Truncate, encode and return the results

            //$matches = array_slice($matches, 0, 5);

            print json_encode($matches);

        } else {



            if ($_GET['term'] != "") {

                $term = trim(strip_tags($_GET['term']));

                //if (isset($_GET['term']) && $_GET['category'] == "") {

                //$category = trim(strip_tags($_GET['category']));

                $return = $this->ajax->getSearchProduct($term, '', $_GET['storeid']);

                //}

                //echo 'text';



                foreach ($return as $eachloc) {

                    // Add the necessary "value" and "label" fields and append to result set

                     $eachloc['value'] = $eachloc['id'];

                    $eachloc['label'] = "{$eachloc['prod']}";

					$eachloc['saleprice'] = $eachloc['sale_price'];

					$eachloc['minsaleprice'] = $eachloc['min_sale_price'];

                    $matches[] = $eachloc;

                }

                // Truncate, encode and return the results

                //$matches = array_slice($matches, 0, 5);

                print json_encode($matches);

            }

        }

    }



    function getAutoSearchProducts2() {

        //echo "<pre>";

        //print_r($_POST);

        //print_r($_GET);

        //exit;

        $term = trim(strip_tags($_GET['term']));

        $store = $_GET['store'];

        if (is_numeric(trim($_GET['category'])) && $_GET['category']) {

            // if ($_GET['term'] == "")

          //  echo "iff";







            if (isset($_GET['category']) && $_GET['category'] != "" && $_GET['category'] != "Select" && $_GET['category'] != "اختر") {

                $category = trim(strip_tags($_GET['category']));

                $return = $this->ajax->getProductList2($term, $category, $store);

            } else {

                $return = $this->ajax->getProductList2($term);

            }



            //$return = $this->ajax->getSearchProduct($term);

            foreach ($return as $eachloc) {

                // Add the necessary "value" and "label" fields and append to result set

                $eachloc['value'] = $eachloc['id'];

                $eachloc['label'] = "{$eachloc['prod']}";

                $matches[] = $eachloc;

            }

            // Truncate, encode and return the results

            //$matches = array_slice($matches, 0, 5);







            print json_encode($matches);

        } else {



            if ($_GET['term'] != "") {

                $term = trim(strip_tags($_GET['term']));

                //if (isset($_GET['term']) && $_GET['category'] == "") {

                //$category = trim(strip_tags($_GET['category']));

                $return = $this->ajax->getProductList2($term, '', $_GET['storeid']);

                //}

                //echo 'text';



                foreach ($return as $eachloc) {

                    // Add the necessary "value" and "label" fields and append to result set

                    $eachloc['value'] = $eachloc['id'];

                    $eachloc['label'] = "{$eachloc['prod']}";

                    $eachloc['quant'] = "{$eachloc['quant']}";

                    $eachloc['bsstoreitems'] = "{$eachloc['bsstoreitems']}";

                    //$eachloc['store_item_id'] = "{$eachloc['store_item_id']}";

                    $matches[] = $eachloc;

                }

                // Truncate, encode and return the results

                //$matches = array_slice($matches, 0, 5);







                print json_encode($matches);

            }

        }

    }



    function getAutoSearchCustomer() {

        $term = trim(strip_tags($_GET['term']));

        $return = $this->ajax->getSearchCustomer($term);

        foreach ($return as $eachloc) {

            // Add the necessary "value" and "label" fields and append to result set

            $eachloc['value'] = $eachloc['id'];

            $eachloc['label'] = "{$eachloc['cus']}";

            $eachloc['buywithtotal'] = "{$eachloc['buywithtotal']}";

            $eachloc['blacklist'] = "{$eachloc['blacklist']}";

            $eachloc['reson_blacklist'] = "{$eachloc['reson_blacklist']}";

            $eachloc['storeid'] = "{$eachloc['storeid']}";

            $matches[] = $eachloc;

        }

        // Truncate, encode and return the results

        $matches = array_slice($matches, 0, 5);

        print json_encode($matches);

    }



    function getAutoSearchCustomer2() {

        $term = trim(strip_tags($_GET['term']));

        $return = $this->ajax->getSearchCustomer2($term);

        foreach ($return as $eachloc) {

            // Add the necessary "value" and "label" fields and append to result set

            $eachloc['value'] = $eachloc['id'];

            $eachloc['label'] = "{$eachloc['cus']}";

            $eachloc['buywithtotal'] = "{$eachloc['buywithtotal']}";

            $matches[] = $eachloc;

        }

        // Truncate, encode and return the results

        $matches = array_slice($matches, 0, 5);

        print json_encode($matches);

    }



    function getAutoSearchExpenses() {

        $term = trim(strip_tags($_GET['term']));

        $return = $this->ajax->getSearchExpense($term);

        foreach ($return as $eachloc) {

            // Add the necessary "value" and "label" fields and append to result set

            $eachloc['value'] = $eachloc['id'];

            $eachloc['label'] = "{$eachloc['cus']}";

            $matches[] = $eachloc;

        }

        // Truncate, encode and return the results

        $matches = array_slice($matches, 0, 5);

        print json_encode($matches);

    }



    function create_barcode() {





        $postData = $this->input->post();

        //echo $_SERVER['DOCUMENT_ROOT'];

        if (!empty($postData)) {

            if ($postData['itemname']) {

                include "Barcode39.php";



                // set Barcode39 object

                $bc = new Barcode39($postData['itemname']);



                // display new barcode

                $imagePath = $_SERVER['DOCUMENT_ROOT'] . $this->config->item('barcode_bath') . 'barcode_image';



                echo $imagePath;



                $imagePath = $imagePath . '/' . $postData['itemname'] . '.gif';



                $bc->barcode_text_size = 5;



                // set barcode bar thickness (thick bars)

                $bc->barcode_bar_thick = 4;



                // set barcode bar thickness (thin bars)

                $bc->barcode_bar_thin = 2;

                $bc->draw($imagePath);

                return true;

            }

        }

        //$this->load->library('Barcode39','bar');

        //$this->bar->Barcode39("Shay Anderson");

        //$bc = new Barcode39("Shay Anderson"); 

        //echo "asdasd";

        //echo $this->bar->draw();

    }



    public function branch_list() {

        $companyid = $this->input->post('companyid');

        $this->db->select('branchid,branchname');

        $this->db->from('bs_company_branch');

        $this->db->where('companyid', $companyid);

        $this->db->order_by("branchid", "ASC");

        $query = $this->db->get();

        $dropdown = '';

        $dropdownul = '';

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->branchid . '" ';



            if ($value == $row->branchid || $_POST['branch'] == $row->branchid) {

                $dropdownse .= 'selected="selected"';

            }

            //$com = json_decode($row->branchname, TRUE);

            $dropdown .= ' >' . _s($row->branchname, get_set_value('site_lang')) . '</option>';

            $dropdownul .= '<li>' . _s($row->branchname, get_set_value('site_lang')) . '</li>';

        }



        $query->free_result();

        $ar['dropdown'] = $dropdown;

        $ar['dropdownul'] = $dropdownul;



        //Store

        $this->db->select('storeid,storename');

        $this->db->from('bs_store');

        $this->db->where('companyid', $companyid);

        $this->db->order_by("storename", "ASC");

        $store_query = $this->db->get();

        $store = '<option value="">Select Store</option>';

        foreach ($store_query->result() as $st) {

            $store .= '<option value="' . $st->storeid . '" ';

            if ($value == $st->storeid) {

                $store .= 'selected="selected"';

            }

            //$store .= '>' . $st->storename . '</option>';

            $store .= '>' . _s($st->storename, get_set_value('site_lang')) . '</option>';

        }

        $store_query->free_result();

        $ar['store'] = $store;



        //Customers

        $this->db->select('userid,fullname');

        $this->db->from('bs_users');

        $this->db->where('companyid', $companyid);

        $this->db->where('member_type', '6');

        $this->db->where('status', 'A');

        $this->db->order_by("fullname", "ASC");

        $customer_query = $this->db->get();

        $customer = '<option value="">Select Customer</option>';

        foreach ($customer_query->result() as $cu) {

            $customer .= '<option value="' . $cu->userid . '" ';

            if ($value == $cu->userid) {

                $customer .= 'selected="selected"';

            }

            $customer .= '>' . $cu->fullname . '</option>';

        }

        $customer_query->free_result();



        $ar['customer'] = $customer;

        echo json_encode($ar);

    }



    public function product_list() {

        $storeid = $this->input->post('storeid');

        $this->db->select('itemid,itemname,serialnumber');

        $this->db->from('bs_item');

        $this->db->where('storeid', $storeid);

        $this->db->order_by("itemname", "ASC");

        $query = $this->db->get();

        $dropdown = '<option value="">Select Product</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option data-serial="' . $row->serialnumber . '" value="' . $row->itemid . '" ';

            if ($value == $row->itemid) {

                $dropdown .= 'selected="selected"';

            }

            $dropdown .= '>' . $row->itemname . '</option>';

        }



        $query->free_result();

        $ar['products'] = $dropdown;



        echo json_encode($ar);

    }



    public function customer_info() {

        $customerid = $this->input->post('customerid');

        $this->db->select('phone_number,office_number,fax_number');

        $this->db->from('bs_users');

        $this->db->where('userid', $customerid);

        $this->db->order_by("phone_number", "ASC");

        $query = $this->db->get();

        foreach ($query->result() as $row) {

            $ar['phone'] = $row->phone_number;

            $ar['office'] = $row->office_number;

            $ar['fax'] = $row->fax_number;

        }

        $query->free_result();

        echo json_encode($ar);

    }



    public function area_list() {

        $locationid = $this->input->post('locationid');

        $this->db->select('areaname,areaid');

        $this->db->from('bs_area');

        $this->db->where('locationid', $locationid);

        $this->db->where('areastatus', 'A');

        $this->db->order_by("areaname", "ASC");

        $query = $this->db->get();

        $dropdown = '<option value="">Select Area</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->areaid . '" ';

            if ($value == $row->areaid) {

                $dropdown .= 'selected="selected"';

            }

            $dropdown .= '>' . $row->areaname . '</option>';

        }



        echo($dropdown);

    }



    public function city_list() {

        $countryid = $this->input->post('countryid');



        $this->db->select('cityid,cityname,citycode');

        $this->db->from('bs_city');

        $this->db->where('countryid', $countryid);

        $this->db->where('citystatus', 'A');

        $this->db->order_by("cityname", "ASC");

        $query = $this->db->get();

        $dropdown = '<option value="">Select City</option>';



        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->cityid . '" citycode1="' . $row->citycode . '"';

            if ($value == $row->cityid) {

                $dropdown .= 'selected="selected"';

            }

            $dropdown .= '>' . $row->cityname . ' (' . $row->citycode . ')</option>';

        }



        echo($dropdown);

    }



    function getProductBySuplier() {

        $post = $this->input->post();

        //echo "<pre>";

        //print_r($post);



        $this->db->select('itemid,itemname,serialnumber');

        $this->db->from('bs_item');

        if ($post['supplier_id']) {

            $this->db->where('suppliderid', $post['supplier_id']);

        }



        $query = $this->db->get();

        $dropdown .= '<option data-serial="" value="" ';

        $dropdown .= '>Select</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option data-serial="' . $row->serialnumber . '" value="' . $row->itemid . '" ';

            $dropdown .= '>' . $row->itemname . '</option>';

        }



        echo($dropdown);

    }



    public function filter_product() {

        $comid = $this->input->post('comid');

        $categoryid = $this->input->post('categoryid');

        $itemtype = $this->input->post('itemtype');

        $storeid = $this->input->post('storeid');

        $supplierid = $this->input->post('supplierid');

        $this->db->select('itemid,itemname,serialnumber');

        $this->db->from('bs_item');

        $this->db->where('ownerid', ownerid());

        if ($comid != '') {

            $this->db->where('comid', $comid);

        }

        if ($categoryid != '') {

            $this->db->where('categoryid', $categoryid);

        }

        if ($itemtype != '') {

            $this->db->where('itemtype', $itemtype);

        }

        if ($storeid != '') {

            $this->db->where('storeid', $storeid);

        }

        if ($supplierid != '') {

            $this->db->where('supplierid', $supplierid);

        }



        $this->db->where('product_status', 'A');

        $this->db->where('(`quantity`<=`point_of_re_order`)');

        $this->db->order_by("itemname", "ASC");

        $query = $this->db->get();

        $dropdown = '<option value="">Select Product</option>';



        foreach ($query->result() as $row) {

            $dropdown .= '<option data-serial="' . $row->serialnumber . '" value="' . $row->itemid . '" ';

            if ($value == $row->itemid) {

                $dropdown .= 'selected="selected"';

            }

            $dropdown .= '>' . $row->itemname . '</option>';

        }



        echo($dropdown);

    }



    public function create_employ_code() {

        $companyid = $this->input->post('companyid');



        $branchid = $this->input->post('branchid');

        //$branch_code	=	$this->get_branch_code($companyid,$branchid);



        $qx = $this->db->query("SELECT COUNT(userid)+1 AS maxiumid FROM bs_users WHERE companyid='" . $companyid . "' AND branchid='" . $branchid . "' LIMIT 0,1");

        foreach ($qx->result() as $rx) {

            $employee_code = strtoupper($companyid . $branchid . $rx->maxiumid);

        }

        echo($employee_code);

    }



//-----------------------------------------------------------------------

    /**

     * 

     * Enter description here ...

     * @param unknown_type $companyid

     * @param unknown_type $branchid

     */

    public function get_branch_code($companyid, $branchid) {

        return $branch_code = $this->ajax->get_branch_code($companyid, $branchid);

    }



//-----------------------------------------------------------------------

    /**

     * 

     * Enter description here ...

     */

    public function account_exist() {

        $bankid = $this->input->post('bankid');

        $account_number = $this->input->post('account_number');



        $isAvailable = $this->ajax->account_num_exist($bankid, $account_number);



        echo json_encode(array('valid' => $isAvailable));

    }



    function getProductData() {

        $this->load->model('sales_management/sales_management_model', 'sales_management');

        //echo "";

        $pid = $_POST['pid'];

        if($_POST['storeid']!='p'){

            $storeid = $_POST['storeid'];

        }else{

            $storeid = $_POST['sid'];

        }

        //$pid=1;

        //$storeid=2;

        $data = $this->ajax->getProductList($pid);



        $this->_data['products'] = $this->ajax->getProductList($pid);

        $pData = $this->ajax->getavgSalesvalue($pid, $storeid);

        //echo "<pre>";

        //echo $pid."/".$storeid;

        //print_r($pData);

        //exit;

        $avgArr = array();

        $purchase_arr = array();

        if (!empty($pData)) {



            foreach ($pData as $productData) {

                //			echo "<pre>";

                //print_r($productData);



                $purchase_arr[] = $productData->purchase_id;

                $pprice = $productData->purchase_item_price;

                $ptype = $productData->profit_type;

                $pval = $productData->profit_value;

                if(isset($ptype)){

				if ($ptype == 'percentage') {

                    //		echo "percenth";

                    $ptemp = $pval * $pprice;

                    $percentageval = $ptemp / 100;

                	} else {

                    	$percentageval = $pval;

                	}

					$totalsaleval = $pprice + $percentageval;

                

				}

				else{

						$totalsaleval = $productData->sale_price;

				}

                //echo $percentageval;

                $avgArr[] = $totalsaleval;

            }

        }





        $toalsum = array_sum($avgArr);

        $count = count($avgArr);

        $netprofitval = $toalsum / $count;

        //echo $netprofitval;

        $purchase_arr = array_unique($purchase_arr);

        $expnens_val = $this->calculteExpense($purchase_arr);

        //echo "<pre>";

        $total_expense = 0;

        //print_r($expnens_val);

        ///total expense for sale invocie

        if (!empty($expnens_val)) {

            foreach ($expnens_val as $expinvoice) {

                foreach ($expinvoice as $exval) {



                    $total_expense+=$exval;

                }

            }

        }



        $totalval = $netprofitval + $total_expense;

        //echo ceil($totalval);

        if (ceil($totalval) != 0) {

            $data->sale_price = ceil($totalval);

        } else {

            $data->sale_price = $data->sale_price;

        }

        //exit;

        //echo $totalval;

        echo json_encode(array('product' => $data, 'itemname2' => _s($data->itemname, get_set_value('site_lang')), 'note' => str_replace('&nbsp;', ' ', strip_tags($data->notes)), 'unit' => _s($data->unit_title, get_set_value('site_lang')),'totalval'=>$totalval));

    }



    function getProductData2() {

        $this->load->model('sales_management/sales_management_model', 'sales_management');

        //echo "";

        $pid = $_POST['pid'];

       // $data = $this->ajax->getProductList2($pid);

		echo "test";
		exit;
        $this->_data['products'] = $this->ajax->getProductList2($pid);

        echo json_encode(array('product' => $this->_data['products'], 'itemname2' => _s($data->itemname, get_set_value('site_lang')), 'note' => str_replace('&nbsp;', ' ', strip_tags($data->notes)), 'unit' => _s($data->unit_title, get_set_value('site_lang'))));

    }



    function calculteExpense($puurchaseinv) {

        $expence_purchase = array();

        if (!empty($puurchaseinv)) {



            foreach ($puurchaseinv as $pinvoice) {

                $expensedata = $this->ajax->getAllExpensesByinvoiceId($pinvoice);

                //				echo "<pre>";

                //print_r($expensedata);

                //exit;

                foreach ($expensedata as $expense) {

                    //$expense->expense_id;

                    //				echo "<pre>";

                    //print_r($expense);

                    //echo $expense->expense_id;

                    //exit;

                    $expenseInvoicesdata = $this->ajax->getavgExpenseBYExpenseId($expense->expense_id);

                    foreach ($expenseInvoicesdata as $expinvData) {

                        if ($expinvData->purchase_id == $pinvoice) {



                            $purchaseItemsCount = $this->ajax->getPurchaseItemsCount($pinvoice);

                            if ($purchaseItemsCount > 1) {

                                $expence_purchase[$pinvoice][$expense->expense_id] = $expinvData->perpercentagevalue / $purchaseItemsCount;

                            } else {

                                $expence_purchase[$pinvoice][$expense->expense_id] = $expinvData->perpercentagevalue;

                            }

                            //$expence_purchase[$pinvoice][$expense->expense_id] = $expinvData->perpercentagevalue; 

                            break;

                        }

                    }

                }

            }

        }



        return $expence_purchase;

    }



    function getCustomerData() {

        $this->load->model('sales_management/sales_management_model', 'sales_management');

        //echo "";

        $pid = $_POST['uid'];

        $data = $this->ajax->getCustomerList($pid);



        $this->_data['products'] = $this->ajax->getCustomerList($pid);

        echo json_encode($this->_data['products']);

    }



    public function supplier_dropbox() {

        $ownerid = $this->input->post('ownerid');

        $this->db->select('supplierid,suppliername');

        $this->db->from('bs_suppliers');

        $this->db->where('ownerid', $ownerid);

        $this->db->order_by("suppliername", "ASC");

        $query = $this->db->get();

        $dropdown = '<option value="">Select Supplier</option>';

        foreach ($query->result() as $row) {

            $dropdown .= '<option value="' . $row->supplierid . '" ';

            if ($value == $row->supplierid) {

                $dropdown .= 'selected="selected"';

            }

            $dropdown .= '>' . $row->suppliername . '</option>';

        }

        echo($dropdown);

    }



    public function all_data_list() {

        $ownerid = ownerid();

        $companyid = $this->input->post('comid');

        $itemtype = $this->input->post('itemtype');

        //Category

        $this->db->select('catid,catname');

        $this->db->from('bs_category');

        $this->db->where('companyid', $companyid);

        $this->db->where('cattype', $itemtype);

        $this->db->order_by("catname", "ASC");

        $category_query = $this->db->get();

        $category = '<option value="">Select Category</option>';

        foreach ($category_query->result() as $cat) {

            $category .= '<option value="' . $cat->catid . '" ';

            if ($value == $cat->catid) {

                $category .= 'selected="selected"';

            }

            $category .= '>' . $cat->catname . '</option>';

        }

        $category_query->free_result();

        $ar['category'] = $category;



        //Store

        $this->db->select('storeid,storename');

        $this->db->from('bs_store');

        $this->db->where('companyid', $companyid);

        $this->db->order_by("storename", "ASC");

        $store_query = $this->db->get();

        $store = '<option value="">Select Store</option>';

        foreach ($store_query->result() as $st) {

            $store .= '<option value="' . $st->storeid . '" ';

            if ($value == $st->storeid) {

                $store .= 'selected="selected"';

            }

            $store .= '>' . $st->storename . '</option>';

        }

        $store_query->free_result();

        $ar['store'] = $store;

        echo json_encode($ar);

    }



    /*     * */



    function calculate_pending_jobs() {

        $post = $this->input->post();

        //echo "<pre>";

        //print_r($post);

        //$this->_data['jobs_data'] = $this->ajax->getJobsAmount($post['customer_id']);

        $this->_data['jobs_data'] = $this->ajax->getCusmer_invoices($post['customer_id']);

        echo $this->load->view('ajax_job_view', $this->_data, true);

    }



    function calculate_pending_jobs2() {

        $post = $this->input->post();

        //echo "<pre>";

        //print_r($post);

        //$this->_data['jobs_data'] = $this->ajax->getJobsAmount($post['customer_id']);

        $this->_data['jobs_data'] = $this->ajax->getCusmer_invoices2($post['customer_id']);

        echo $this->load->view('ajax_job_view_2', $this->_data, true);

    }



    function calculate_pending_hiddenjobs() {

        $post = $this->input->post();

        //echo "<pre>";

        //print_r($post);

        //$this->_data['jobs_data'] = $this->ajax->getJobsAmount($post['customer_id']);

        $this->_data['jobs_data'] = $this->ajax->getCusmer_invoices($post['customer_id']);

        echo $this->load->view('ajax_job_hidden', $this->_data, true);

    }

    

    

    /*Forms and list popup*/

        

    function getAddnewcustomer() {

        $this->load->view('Addnewcustomer', $this->_data);

    }

    function getAddnewcustomer2() {

        $this->load->view('Addnewcustomer2', $this->_data);

    }

    function save_new_customer() {

        $post = $this->input->post();

        //	echo "<pre>";

        //print_r($post);

        $items['companyid'] = $post['newcomid'];

        $items['branchid'] = $post['newbranchid'];

        

        //$items['itemname'] = serialize($post['newitemname']);

        $items['fullname'] = $post['newfullname'];

        $items['phone_number'] = $post['newphone_number'];

        $items['opening_balance'] = $post['opening_balance'];

        $items['credit_limit'] = $post['credit_limit'];

        $items['member_type'] = '6';

        if(post('supplier')!=""){$items['type'] = '2';}

        //$items['due_charges'] = $post['due_charges'];

        

        echo $this->db->insert('bs_users', $items);

        $customer_id = $this->db->insert_id();

        $this->ajax->addopening($customer_id);

        if(post('supplier')==""){$this->ajax->add_due_charges($customer_id);}else{$this->ajax->add_due_charges2($customer_id);}

    }

    function getallnewcustomer() {

        $this->load->view('getallnewcustomer', $this->_data);

    }

    function getallnewcustomer2() {

        $this->load->view('getallnewcustomer2', $this->_data);

    }

    

    /**/

    function getallitems($par = null, $storevalue = null) {





        //$this->_data['par'] = $par;

        if ($par == 'p') {

            $this->_data['par'] = $par;

        } else {

            $this->_data['par'] = '';

        }





        if ($par != '' && $storevalue == '') {

            $this->_data['storevalue'] = $par;

        } elseif ($par != '' && $storevalue != '') {

            $this->_data['storevalue'] = $storevalue;

        }



        //echo $this->_data['par'];

        //die();

        $this->load->view('getallitems', $this->_data);

    }

    function searchabout_permession(){

        

        if($this->session->userdata('bs_memtype')=='1' || $this->session->userdata('bs_memtype')=='5'){

            

            echo 'ADMIN';

        }else{

            echo 'NOADMIN';

        }

    }

    function getAddnew() {

        $this->load->view('add_newitem', $this->_data);

    }



	function getConvetData(){

	

		

			$gd = $this->ajax->getdharamval();

			//echo "<pre>";

			//print_r($gd);

			if($gd->is_dharam_static != 1)

				echo $res = getAedDhuram();

			else{

				echo $gd->dharm_value;

			}

			//echo "<pre>";

			//print_r($res);

			

	}

    function save_new_item() {

        $post = $this->input->post();

        //	echo "<pre>";

        //print_r($post);

        $items['comid'] = $post['newcomid'];

        $items['branchid'] = $post['newbranchid'];

        $items['itemname'] = serialize($post['newitemname']);

        $items['categoryid'] = $post['newcategoryid'];

        $items['itemtype'] = $post['newitemtype'];

        $items['serialnumber'] = $post['newserialnumber'];

        $items['barcodenumber'] = $post['newbarcodenumber'];

        $items['barcode_image'] = $post['newbarcode_image'];

        echo $this->db->insert('bs_item', $items);

    }

}



