<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customers extends CI_Controller {
    /*
     * Properties
     */

    private $_data = array();

//----------------------------------------------------------------------
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();
		//echo get_set_value('site_lang');
        //load Home Model
        $this->load->model('customers_model', 'customers');
        $this->load->model('users/users_model', 'users');
        $this->lang->load('main', get_set_value('site_lang'));
        $this->_data['udata'] = userinfo_permission();
        $this->db->query('SET SQL_BIG_SELECTS=1');
    }

//----------------------------------------------------------------------

    /*
     * Customer Listing  Page
     */
    /* public function index()
      {
      // Load Home View
      $this->load->view('customers',	$this->_data);
      } */

    public function options($t='') {

        //$this->_data['customerdetails'] = $this->customers->get_all_calculation();//mahmoud

       //$this->_data['customerData'] = $this->customers->getAllCustomersInvoice();//mahmoud

       // $this->_data['customerbalalnce'] = $this->customers->getAllCustomersbalance();//mahmoud

        //$this->_data['allstorelist'] =        $this->customers->getAllStores();//mahmoud

        $this->_data['tp'] = $t;
       // $this->_data['inside3'] = 'staticsall';//mahmoud
        $this->_data['inside2'] = 'customers';
        $this->_data['inside'] = 'add-customer-form';
        $this->load->view('common/tabs', $this->_data);
		//$this->load->view('customers', $this->_data);
    }

    public function statics($id = null) {
        // Load Home View
        $this->_data['id'] = $id;
        $this->_data['customerdetails'] = $this->customers->get_calculation($id);

        $this->_data['customerData'] = $this->customers->getCustomerInvoicesPayments($id);

        $this->_data['customerbalalnce'] = $this->customers->getCustomerBalance($id);
        //echo "<pre>";
        //print_r($this->_data['customerData ']);
        $this->_data['inside'] = 'statics';
        $this->load->view('common/main', $this->_data);
		
    }

    function removeString($serarchiid) {
        preg_match_all('!\d+!', $serarchiid, $matches);
        //print_r($matches);
        return $matches[0][0];
    }

    function search_data() {
        $data = $this->input->post();
        //print_r($data);
        //$data['search_id'];
        //echo strstr($data['search_id'],'inv');
        //exit;
        if (strstr($data['search_id'], 'inv')) {
            //echo "adasd";
            $this->_data['type'] = 'invoice';
            $searchid = $this->removeString($serarchiid);
            $this->_data['invoicedetails'] = $this->customers->getCustomersInvoiceStatus($searchid);
        } elseif (strstr($data['search_id'], 'p')) {
            //echo "else";
            $this->_data['type'] = 'payment';
            $this->_data['invoicedetails'] = $this->customers->getPaymentVoucher($searchid);
        }

        $this->load->view('voucher_view', $this->_data);
        // exit;
    }

    public function voucher($type, $id = null) {
        // Load Home View
        $this->_data['id'] = $id;
        $this->_data['type'] = $type;
        $this->_data['companydetails'] = $this->customers->getCompanyDetails();

        if ($type == 'invoice') {

            $this->_data['invoicedetails'] = $this->customers->getCustomersInvoiceStatus($id);
            //echo "<pre>";
            //print_r($this->_data['invoicedetails']);

            $this->_data['customerdetails'] = $this->customers->get_calculation($id);
            $this->_data['customerData'] = $this->customers->getCustomerInvoicesPayments($id);

            $this->_data['customerbalalnce'] = $this->customers->getCustomerBalance($id);
        } else {
            $this->_data['invoicedetails'] = $this->customers->getPaymentVoucher($id);
            //	echo "<pre>";
            //	print_r($this->_data['invoicedetails']);
        }
        //echo "<pre>";
        //print_r($this->_data['invoicedetails']);
      //  exit;
        //$this->_data['inside'] = 'statics';
        $this->load->view('voucher_view', $this->_data);
    }

    function staticsaa() {

        $this->_data['customerdetails'] = $this->customers->get_calculation($id);

        $this->_data['customerData'] = $this->customers->getCustomerInvoicesPayments($id);

        $this->_data['customerbalalnce'] = $this->customers->getCustomerBalance($id);
        //echo "<pre>";
        //print_r($this->_data['customerData ']);
        $this->_data['inside'] = 'statics';
        $this->load->view('common/main', $this->_data);
    }

    public function staticsdebitcredit($id = null) {
        // Load Home View

        $this->_data['customerdetails'] = $this->customers->get_calculation($id);

        $this->_data['customerData'] = $this->customers->getCustomerInvoicesPayments($id);

        $this->_data['customerbalalnce'] = $this->customers->getCustomerBalance($id);
        $this->_data['id'] = $id;
        //echo "<pre>";
        //print_r($this->_data['customerData ']);
        $this->_data['inside'] = 'staticscerdit_debit';
        $this->load->view('common/main', $this->_data);
    }

    function allstaticsdebitcredit() {
        $this->_data['customerdetails'] = $this->customers->get_all_calculation();

        $this->_data['customerData'] = $this->customers->getAllCustomersInvoice();

        $this->_data['customerbalalnce'] = $this->customers->getAllCustomersbalance();
        //echo "<pre>";
        //print_r($this->_data['customerData ']);
        $this->_data['inside'] = 'staticscerdit_debit_all';
        $this->load->view('common/main', $this->_data);
    }

    public function getCustomerData_ajax($customerid) {
        $customerData = $this->customers->users_list($customerid);
        $cus = $customerData[0];

        $html = '<div  style="text-align:right !important; direction:rtl;">';
        $html .= '<h3>' . $cus->fullname . '<br />';
        $html .= lang('Code') . ' : #' . $cus->userid . '</h3>';
        $html .= '<div class="g8"> <span class="pop_title">' . lang('Mobile-Number') . ' : </span>';
        $html .= '</div> ';

        $html .= '<div class="g8">' . $cus->phone_number . '</div>';

        $html .= '<!--line--> ';
        $html .= '<div class="g8"> <span>' . lang('Contact-Number') . ' : </span>';
        $html .= '</div> ';

        $html .= '<div class="g8">&nbsp;' . $cus->office_number . '</div>';

        $html .= '<!--line--> ';
        $html .= '<div class="g8"> <span>' . lang('Fax-Number') . ': </span>';
        $html .= '</div> ';

        $html .= '<div class="g8">&nbsp;' . $cus->fax_number . '</div>';

        $html .= '<!--line--> ';
        $html .= '<div class="g8"> <span>' . lang('Email-Address') . ' : </span>';
        $html .= '</div> ';
        $html .= '<div class="g8">&nbsp;' . $cus->email_address . '</div>';

        $html .= '<!--line--> ';
        $html .= '<div class="g8"> <span>' . lang('Address') . ' : </span>';
        $html .= '</div> ';
        $html .= '<div class="g8">&nbsp;' . $cus->address . '</div>';

        $html .= '<!--line--> ';
        $html .= '<div class="g8"> <span>' . lang('Debit') . ' : </span>';
        $html .= '</div> ';
        $html .= '<div class="g8">&nbsp;' . $cus->totalremaining . '</div>';

        $html .= '<!--line--> ';
        $html .= '<div class="g8"> <span>' . lang('Credit') . ' : </span>';
        $html .= '</div> ';
        $html .= '<div class="g8">&nbsp;' . $cus->amount . '</div>';



        $html .= '</div>';
        echo $html;
    }

    public function invoices($customerid = NULL) {
        // Load Home View
        $this->_data['sale_invoices'] = $this->customers->getSaleInvocies($customerid);
        $this->_data['inside'] = 'customer-invoices';
        $this->load->view('common/main', $this->_data);
    }

//----------------------------------------------------------------------

    /*
     * Add Customer
     */
    public function add($customerid = NULL) {


        if ($this->input->post('submit_customer')) {
            $data['ownerid'] = ownerid();
            $data = $this->input->post();

            //print_r($data);
            //exit;


            unset($data['submit_customer']);


            // insert data into database
            if ($customerid != '') {
                $this->customers->update_customer($data, $customerid);
            } else {
                $this->customers->add_customer($data);
            }

            //redirect(base_url().'customers/options');
            //exit();
        } else {
            if ($customerid != '') {
                $this->_data['customer'] = $this->customers->get_customer_detail($customerid);
            }

            $this->_data['company_id'] = $this->customers->get_company_id(ownerid());
            $this->_data['branch_id'] = $this->customers->get_branch_id($this->_data['company_id']);
            //$this->_data['emCode'];
            // Load Home View
            //$this->load->view('add-customer-form', $this->_data);
            $this->_data['inside'] = 'add-customer-form';
            $this->load->view('common/main', $this->_data);
        }
    }

    public function addnewcustomer($customersid) {
        if ($customersid != '') {
            $this->_data['user'] = $this->customers->get_customers_detail($customersid);
        }
        $this->_data['allstorelist'] =        $this->customers->getAllStores();
        //print_r($this->_data['allstorelist']);
        //$this->load->view('add-customer-form', $this->_data);
        $this->_data['inside'] = 'add-customer-form';
        $this->load->view('common/main', $this->_data);
    }

    public function addnewcustomer2($customersid) {
        if ($customersid != '') {
            $this->_data['user'] = $this->customers->get_customers_detail($customersid);
        }

        $this->load->view('add-customer-form', $this->_data);
        //$this->_data['inside']='add-customer-form';
        //$this->load->view('common/main', $this->_data);
    }

    //----------------------------------------------------------------------
    /**
     * 
     * Enter description here ...
     */
    public function add_new_customer() {
        $all_ids = $this->input->post();
        ///echo "<pre>";
        //print_r($all_ids);
        //exit;

        $emCode = $this->customers->getLastEmployeeCode();
        $employeecode = $emCode->employeecode + 1;

        $this->customers->add_new_customer($employeecode);
        //$this->db->insert($this->_table_customers, $data);
        //$customer_id
    }

    /*     * *
     * 
     */

    public function delete_customers() {
        $all_ids = $this->input->post('ids');


        if (!empty($all_ids)) {

            $this->customers->delete_customers($all_ids);

            $this->session->set_flashdata('success', 'Record has been deleted.');

            redirect(base_url() . 'customers/options');
            exit();
        } else {
            $this->session->set_flashdata('error', 'There is No record Selected.');

            redirect(base_url() . 'customers/options');
            exit();
        }
    }

    function getCustomeDues() {
        $post = $this->input->post();
        //	echo "<pre>";
        //	print_r($post);
        //$post['customer_id'];
        $dues = $this->customers->getCustomersDues($post['customer_id']);
        echo json_encode($dues);
    }

    function pay_reciep() {

        $this->_data['charges_all'] = $this->customers->getAllcharges();
        //$this->load->view('add_payment',$this->_data);
        $this->_data['inside'] = 'add_payment';
        $this->load->view('common/main', $this->_data);
    }

    function add_payment_post() {
        $post = $this->input->post();
        //echo "<pre>";
        //print_r($post);
        //echo $post['cash_to_branch_id'];
        //exit;

        $customerData = $this->customers->getCustomerPaymetns($post['customer_id']);
        //print_r($customerData);
        if ($customerData->totalpayment > $customerData->totalcharges) {
            $remaining = $customerData->totalpayment - $customerData->totalcharges;
        }

        if ($remaining > 0) {

            $last_payment = $this->customers->getLastpaymentIdByCustomerMultiple($post['customer_id'], '');
            //echo "<pre>";
            //print_r($last_payment);
            //echo $remaining;
            //echo $last_payment->ppp;
            if (trim($last_payment->ppp) == 0 || trim($last_payment->ppp) < $remaining) {

                if ($post['payment_amount'] > $last_payment->payment_amount || trim($last_payment->ppp) == 0) {
                    //echo $remaining;
                    //if()
                    $all_last_payment = $this->customers->getLastpaymentIdByCustomerMultiple($post['customer_id'], 1);
                    if (!empty($all_last_payment)) {
                        foreach ($all_last_payment as $in => $l_payment) {
                            //echo "<pre>";
                            //	print_r($l_payment);
                            $ppp = $l_payment->payment_amount - $l_payment->paidamount;
                            //echo  $ppp;
                            if ($ppp > 0) {
                                if ($ppp >= $remaining) {
                                    //echo "iff";
                                    $last_payment = $all_last_payment[$in];

                                    break;
                                }
                            }
                        }
                    }
                } else {
                    $last_payment->payment_amount;
                }
            }


        }


        $data['user_id'] = $this->session->userdata('userid');

        if (isset($post['invoice_payments']))
            $jobcharges = $post['invoice_payments'];



        $userid = $this->session->userdata('bs_userid');
        //$this->session->userdata('bs_userid');
        $user = $this->users->get_user_detail($userid);
        $data['refrence_id'] = $post['customer_id'];
        $data['payment_amount'] = $post['payment_amount'];
        $data['payment_type'] = 'receipt';
        ///print_r($data);
        //exit;


        $vid = addvouchers('receipt',$post);
        if ($post['refrence_by'] == 'against_reference') {

            $data['refernce_type'] = 'against';
        } elseif ($post['refrence_by'] == 'on_account') {

            $data['refernce_type'] = 'onaccount';
        } elseif ($post['refrence_by'] == 'on_account') {

            $data['refernce_type'] = 'onaccount';
        } else {
            $data['refernce_type'] = $post['refrence_by'];
        }


        //print_r($data);
        $data['payment_details'] = $post['notes'];
        if (trim($post['refrence_by']) != "clear_dues" && trim($post['deduct_from']) != "from_account" && trim($post['refrence_by']) != "return_on_account") {
            //echo "if";
            $payment_id = $this->customers->insert($data, 'payments');
        }

        if ($post['refrence_by'] == "clear_dues" && $post['deduct_from'] != "from_account" && trim($post['refrence_by']) != "return_on_account") {
            //echo "if2";
            //print_r($data);
            $payment_id = $this->customers->insert($data, 'payments');
        }
        //exit;

        if ($post['payment_method'] == 'bank') {
            $transaction_data['branch_id'] = $user->branchid;
            $transaction_data['transaction_by'] = 'invoice';
            $transaction_data['transaction_type'] = 'debit';
            $transaction_data['value'] = $post['payment_amount'];
            $transaction_data['amount_type'] = 'sales';
            $transaction_data['amount_type'] = $post['payment_type'];
            $transaction_data['account_id'] = $post['account_id'];
            $transaction_data['notes'] = $post['notes'];
            $transaction_data['voucher_id'] = $vid;
            $transaction_data['clearance_date'] = date('Y-m-d');
            $transaction_id = $this->customers->insert($transaction_data, 'an_company_transaction');
        }

        if($post['payment_method'] == 'cash' || isset($post['cash_to_branch_id'])){
            
            $payment_cash['branch_cash_id'] = $post['cash_to_branch_id'];
            
            $payment_cash['customer_id'] = $_POST['customerid'];
            $payment_cash['value'] = $post['payment_amount'];
            $payment_cash['notes'] = $post['notes'];
            $payment_cash['purpose_of_payment'] = $post['notes'];
            $payment_cash['created'] = $this->session->userdata('bs_userid');
            $payment_cash['transaction_type'] = 'debit';
            $payment_cash['payment_type'] = 'cash';
            $payment_cash['voucher_id'] = $vid;
            $payment_cash['refrence_type'] = 'payment_in';
            $payment_cash['date'] = date('Y-m-d');
            //$payment_cash['invoice_id'] = $invoice_id;
            //5
            $payment_id = $this->customers->insert($payment_cash,'an_cash_management');
        }


        if (!empty($jobcharges) && $post['refrence_by'] == 'against_reference') {
            //echo "asdasd";
            //exit;
            $this->on_against($post, $last_payment, $jobcharges, $payment_id);
        } elseif ($post['refrence_by'] == 'clear_dues') {
            $this->on_dues($post, $last_payment, $job_data, $payment_id);
        } elseif ($post['refrence_by'] == 'advance') {
			$this->updateInviceUp($post['bl_number'],$post['payment_amount']);
            $this->on_advance($post, $last_payment, $job_data, $payment_id);
            //	exit;
        } elseif ($post['refrence_by'] == 'return_on_account') {
            $this->returnOnAccount($post, $last_payment, $job_data, $payment_id);
            //	exit;
        }

            $this->session->set_userdata('bs_payment',$payment_id);
        //redirect(base_url() . 'customers/pay_reciep');
            $newurl  = 'customers/voucher/payment/'.$payment_id;
         //do_redirect('customers/pay_reciep?e=10');
         do_redirect('customers/voucher/payment/'.$payment_id);
        exit();
    }

    function returnOnAccount($post, $last_payment, $job_data, $payment_id) {

        //	echo "<pre>";
        //print_r($post);		
        //	print_r($last_payment->id);
        //	exit;
        $inv['invoice_total_amount'] = $post['payment_amount'];
        $inv['customer_id'] = $post['customer_id'];
        $inv['sales_amount'] = $post['payment_amount'];
        $inv['invoice_status'] = 1;
        $inv['invoice_type'] = 'reverse';
        $this->customers->insert($inv, 'an_invoice');
        $invid = $this->db->insert_id();

        $inv_pay['invoice_id'] = $invid;
        $inv_pay['payment_id'] = $last_payment->id;
        $inv_pay['payment_amount'] = $post['payment_amount'];
        $this->customers->insert($inv_pay, 'invoice_payments');
    }

    function on_advance($post, $last_payment, $job_data, $payment_id) {
        //echo "<pre>";
        //print_r($last_payment);
        //exit;
        //$post['deduct_from'] 
        if ($post['deduct_from'] == 'from_direct') {
            $bl_number = $post['bl_number'];
            $inv_pay['invoice_id'] = $bl_number;
            $inv_pay['payment_id'] = $payment_id;
            $inv_pay['payment_amount'] = $post['payment_amount'];
            $this->customers->insert($inv_pay, 'invoice_payments');
        } else {

            $bl_number = $post['bl_number'];
            $inv_pay['invoice_id'] = $bl_number;
            $inv_pay['payment_id'] = $last_payment->id;
            $inv_pay['payment_amount'] = $post['payment_amount'];
            $this->customers->insert($inv_pay, 'invoice_payments');
        }
    }

    function on_against($post, $last_payment, $jobcharges, $payment_id) {

        //echo "<pre>";
        //print_r($last_payment);
        //echo "<pre>";
        //print_r($post);
        //exit;

        if ($post['deduct_from'] == "from_account") {
            if (count($last_payment) > 1) {
                
            } else {
                $payment_amount = $post['payment_amount'];
                $payment_id = $last_payment->id;
            }
        } else {
            $payment_amount = $post['payment_amount'];
        }

        //		echo $payment_amount;
///			exit;
        //   echo "<pre>";
        //print_r($post);
        //       echo "<pre>";
        //     print_r($post);
        $jobcharges = $post['invoice_payments'];
        $payment_amount = $post['payment_amount'];
        $total = count($jobcharges['invoice_id']);
        $invoice_status = 0;
        for ($a = 0; $a < $total; $a++) {
            if ($payment_amount > 0) {

                $remaininginvamount = $post['invoice_payments']['total_amount'][$a] - $post['invoice_payments']['invoice_paid'][$a];
                if ($payment_amount >= $remaininginvamount) {
                    //echo "if";
                    $paid_invoice_amount = $remaininginvamount;
                    $payment_amount = $payment_amount - $remaininginvamount;
                    $invoice_status = 1;
                } elseif ($payment_amount < $remaininginvamount) {
                    //echo "else";
                    $paid_invoice_amount = $payment_amount;
                    $payment_amount = $payment_amount - $payment_amount;
                    $invoice_status = 0;
                    $prev_payment = $this->customers->getpaidInvoiceAmount($post['invoice_payments']['invoice_id'][$a]);
                    $total_payment = $paid_invoice_amount + $prev_payment;
                    //echo $post['invoice_payments']['total_amount'][$a];
                    if ($total_payment >= $post['invoice_payments']['total_amount'][$a]) {
                        $invoice_status = 1;
                    }
                }

                //echo "Paid to invoice".$paid_invoice_amount."<br>";
                //echo "remaingin".$payment_amount."<br>";
                //echo "status".$invoice_status."<br>";

                $inv_pay['invoice_id'] = $jobcharges['invoice_id'][$a];
                $inv_pay['payment_id'] = $payment_id;
                $inv_pay['payment_amount'] = $paid_invoice_amount;
                //echo "<pre>";
                //print_r($inv_pay);
                $this->customers->insert($inv_pay, 'invoice_payments');
                $job_data['invoice_id'] = $jobcharges['invoice_id'][$a];
                $job_data['payment_id'] = $payment_id;


                $chargesData['payment_amount'] = $post['payment_amount'];
                $job_data['invoice_paid'] = $jobcharges['invoice_paid'][$a];
                $job_data['total_amount'] = $jobcharges['total_amount'][$a];
                $paid_amount = $job_data['invoice_paid'] + $post['payment_amount'];

                if ($job_data['total_amount'] > $paid_amount) {
                    $invoice_main['status'] = 2;
                }
                $invoice_main['invoice_status'] = $invoice_status;

                if (isset($invoice_main)) {
                    $this->db->where('invoice_id', $job_data['invoice_id']);
                    $this->db->update('an_invoice', $invoice_main);
                }
            }

            //echo $payment_amount;	
            if ($paid_amount > 0) {

                //     $this->on_account($post,$paid_amount,'onaccount');
            }
        }
    }
	
	function updateInviceUp($invoice_id,$payment_amount){
		
					$prev_payment = $this->customers->getpaidInvoiceAmount($invoice_id);
                    $total_payment = $payment_amount + $prev_payment;
                    //echo $post['invoice_payments']['total_amount'][$a];
                    if ($total_payment >= $payment_amount) {
                        $invoice_status = 1;
                    }
					else{
						$invoice_status = 0;
					}
					$invoice_main['invoice_status'] = $invoice_status;
					 if (isset($invoice_main)) {
                    $this->db->where('invoice_id', $invoice_id);
                    $this->db->update('an_invoice', $invoice_main);
                }
	}

    function on_account($post, $amount, $type) {
        $data['refrence_id'] = $post['customer_id'];
        $data['payment_amount'] = $amount;
        $data['payment_type'] = 'receipt';
        ///print_r($data);
        //exit;

        if ($type == 'against_reference') {

            $data['refernce_type'] = 'against';
        } elseif ($type == 'on_account') {

            $data['refernce_type'] = 'onaccount';
        } elseif ($type == 'on_account') {

            $data['refernce_type'] = 'onaccount';
        }

        $payment_id = $this->customers->insert($data, 'payments');
    }

    function on_dues($post, $last_payment, $job_data, $payment_id) {


        //payment_amount
        $invData = $this->customers->getCustomersDues($post['customer_id']);
        //echo "<pre>";
        //print_r($invData);
        //print_r($last_payment);
        //exit;

        $job_data['invoice_id'] = $invData->invoice_id;
        if (isset($payment_id))
            $job_data['payment_id'] = $payment_id;

        if ($post['deduct_from'] != 'from_direct') {
            if (isset($last_payment) && !empty($last_payment)) {
                //	$last_payment['payment_amount'];

                if ($post['payment_amount'] != "") {

                    $job_data['payment_amount'] = $post['payment_amount'];
                    if ($last_payment->payment_amount != "" && $last_payment->payment_amount > 0) {
                        if ($last_payment->payment_amount != "" && $last_payment->payment_amount != 0 && $post['payment_amount'] <= $last_payment->payment_amount) {
                            //echo "lasttt";
                            $job_data['payment_amount'] = $post['payment_amount'];
                        }
                    } else {
                        $job_data['payment_amount'] = $post['payment_amount'];
                    }


                    $job_data['payment_id'] = $last_payment->id;
                }
            }
        } else {

            $job_data['payment_amount'] = $post['payment_amount'];
        }

        //print_r($job_data);
        // exit;
        $this->customers->insert($job_data, 'invoice_payments');


        if ($job_data['payment_amount'] == $invData->sales_amount)
            $invoice_main['invoice_status'] = 1;

        if (isset($invoice_main)) {
            $this->db->where('invoice_id', $invData->invoice_id);
            $this->db->update('an_invoice', $invoice_main);
        }
    }

    function getCustomePayments() {

        $post = $this->input->post();
        $this->_data['payment_data'] = $this->customers->getCustomerPaymetns($post['customer_id']);
        echo json_encode($this->_data['payment_data']);
    }

    function getInvoicePending() {
        $post = $this->input->post();
        $payment_data = $this->customers->getInvoicePaymentModule($post['invoiceid']);
        //echo "<pre>";
        //print_r($payment_data);
        echo $payment_data->remaining;
        //$payment_data
    }

    public function getAutoSearchJob() {
        // Data could be pulled from a DB or other source


        $term = trim(strip_tags($_GET['term']));
        $customers = $this->customers->getJobs($term);
        // Cleaning up the term
        // Rudimentary search
        foreach ($customers as $eachloc) {
            // Add the necessary "value" and "label" fields and append to result set
            $eachloc['value'] = $eachloc['id'];
            $eachloc['label'] = "{$eachloc['id']}";
            $matches[] = $eachloc;
        }
        // Truncate, encode and return the results
        $matches = array_slice($matches, 0, 5);
        print json_encode($matches);
    }

    function view_payments($customerid = "") {

        //$this->_data
        if ($customerid != "") {

            $this->_data['payment_data'] = $this->customers->getCustomerPaymetns($customerid);
            $this->_data['invoice_data'] = $this->customers->getCusmer_invoices($customerid);
            ///print_r($this->_data['invoice_data']);
        }

        //exit;
        $this->load->view('view_payments', $this->_data);
    }

//----------------------------------------------------------------------
}
