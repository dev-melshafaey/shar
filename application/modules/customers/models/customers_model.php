<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customers_model extends CI_Model {
    /*
     * Properties
     */

    private $_table_users;
    private $_table_banks;
    private $_table_modules;
    private $_table_customers;
    private $_table_permissions;
    private $_table_companies;
    private $_table_company_branches;
    private $_table_users_bank_transactions;

//----------------------------------------------------------------------

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();

        //Load Table Names from Config
        $this->_table_users = $this->config->item('table_users');
        $this->_table_banks = $this->config->item('table_banks');
        $this->_table_customers = $this->config->item('table_customers');
        $this->_table_modules = $this->config->item('table_modules');
        $this->_table_permissions = $this->config->item('table_permissions');
        $this->_table_companies = $this->config->item('table_companies');
        $this->_table_company_branches = $this->config->item('table_company_branches');
        $this->_table_users_bank_transactions = $this->config->item('table_users_bank_transactions');
    }

//----------------------------------------------------------------------


    /*
     * Add New Customer
     * @param $data array
     * return TRUE
     */
    function add_customer($data) {
        $this->db->insert($this->_table_customers, $data);

        return TRUE;
    }

//----------------------------------------------------------------------


    /*
     * Update Customer
     * @param $data array
     * @param $customerid
     * return TRUE
     */
    function update_customer($data, $customerid) {
        $this->db->where('customerid', $customerid);
        $this->db->update($this->_table_customers, $data);

        return TRUE;
    }

    function getJobs($search_name) {
        $sql = "SELECT * FROM `an_invoice`  AS i WHERE i.invoice_id ='" . $search_name . "' AND invoice_status=0";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            $locs = $q->result();
            foreach ($locs as $eachLoc) {
                $customers[] = array('id' => $eachLoc->invoice_id);
            }
        } else {
            //return '0';
        }




        return $customers;
    }

    function getpaidInvoiceAmount($inv_id) {
        $sql = "SELECT SUM(ip.`payment_amount`) AS paymentamount FROM `invoice_payments` AS ip
WHERE ip.`invoice_id` = " . $inv_id . "";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $locs = $q->row()->paymentamount;
        } else {
            return false;
        }
    }

    ///avaliable store items in the quantity
    function getStoreItemsAvailable($itemid, $storeid) {
        $sql = "SELECT 
					  st.`item_id`,
					  @t4 := IF(
						SUM(st.`quantity`) IS NULL,
						0,
						SUM(st.`quantity`)
					  ) - IF(
						SUM(ssi.`soled_quantity`) IS NULL,
						0,
						SUM(ssi.`soled_quantity`)
					  ) AS remaining 
					FROM
					  `bs_store_items` AS st 
					  LEFT JOIN `store_soled_items` AS ssi 
						ON ssi.`store_item_id` = st.`store_item_id` 
					WHERE st.`item_id` = '" . $itemid . "' AND st.`store_id` = '" . $storeid . "' 
					  GROUP BY st.`item_id`
					ORDER BY st.`store_item_id` ASC ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    ///storeitems to sole
    function getStoreItemsToSole($itemid, $storeid) {
        $sql = "SELECT 
		  st.`item_id`,
		  st.*,
		  @t4 := IF(
			st.`quantity` IS NULL,
			0,
			st.`quantity`
		  ) - IF(
			ssi.`soled_quantity` IS NULL,
			0,
			ssi.`soled_quantity`
		  ) AS remaining ,
		  bpi.`purchase_item_price`
		FROM
		  `bs_store_items` AS st 
		  LEFT JOIN `store_soled_items` AS ssi 
			ON ssi.`store_item_id` = st.`store_item_id` 
		  INNER JOIN `bs_purchase_items` AS bpi 
			ON bpi.`purchase_id` = st.`purchase_id` 
		WHERE st.`item_id` = '" . $itemid . "' 
		  AND IF(
			st.`quantity` IS NULL,
			0,
			st.`quantity`
		  ) - IF(
			ssi.`soled_quantity` IS NULL,
			0,
			ssi.`soled_quantity`
		  ) != 0 AND bpi.`confirm_status` = '1'
		  AND  st.`store_id` = '" . $storeid . "'
		ORDER BY st.`store_item_id` ASC ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    ////stote avali items list
    function getStoreItemsAvailableList($itemid, $storeid) {
        $sql = "SELECT 
					  st.`item_id`,
					  @t4 := IF(
						SUM(st.`quantity`) IS NULL,
						0,
						SUM(st.`quantity`)
					  ) - IF(
						SUM(ssi.`soled_quantity`) IS NULL,
						0,
						SUM(ssi.`soled_quantity`)
					  ) AS remaining 
					FROM
					  `bs_store_items` AS st 
					  LEFT JOIN `store_soled_items` AS ssi 
						ON ssi.`store_item_id` = st.`store_item_id` 
					WHERE st.`item_id` = '" . $itemid . "' AND st.`store_id` = '" . $storeid . "' 
					  GROUP BY st.`item_id`
					ORDER BY st.`store_item_id` ASC ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getInvoicePaymentModule($invoiceid) {
        $sql = "SELECT 
			  i.`invoice_id`,
			  i.`sales_amount`,
			  SUM(ip.`payment_amount`) AS totalpayment,
			  IF(
				SUM(ip.`payment_amount`) IS NULL,
				0,
				SUM(ip.`payment_amount`)
			  ) AS payment_amount ,
			  i.`sales_amount` - IF(
				SUM(ip.`payment_amount`) IS NULL,
				0,
				SUM(ip.`payment_amount`)
			  ) AS remaining 
			FROM
			  `an_invoice` AS i 
			  LEFT JOIN `invoice_payments` AS ip 
				ON ip.`invoice_id` = i.`invoice_id` 
			WHERE i.`invoice_id` = '" . $invoiceid . "' 
			  AND i.`invoice_status` = '0' 
			GROUP BY i.`invoice_id` ";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function getCompanyDetails(){
        $cid = $this->session->userdata('bs_companyid');
        $sql= "SELECT
            c.*,
            COUNT(cp.`company_id`) AS partners,
            cl.`logo_name`
          FROM
            `bs_company` AS c
            LEFT JOIN `company_partners` AS cp
              ON cp.`company_id` = c.`companyid`
            LEFT JOIN `company_logos` cl
              ON cl.`company_id` = c.`companyid`
              WHERE c.`is_delete` = 0 AND c.`companyid` = '".$cid."'";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    function getPaymentVoucher($payment_id) {
       // $sql = "SELECT * FROM `payments` AS p WHERE p.`id` = '" . $payment_id . "'";
        $sql = "SELECT
  p.*,u.`fullname`
FROM
  `payments` AS p
  INNER JOIN `bs_users` AS u
    ON u.`userid` = p.`refrence_id`
WHERE p.`id` = '". $payment_id ."' ";
       // echo $sql;
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function getCustomerInvoicesPayments($customer_id) {
        /* $sql= "SELECT 
          p.`id`,
          p.`payment_amount` AS paidamount,
          ip.`payment_amount` AS invoicepayment,
          ip.`invoice_id`,
          IF(p.`payment_amount`IS NULL, 0, p.`payment_amount`) AS payment_amount,
          IF(p.`payment_amount`IS NULL, 0, p.`payment_amount`)-ip.`payment_amount` AS remaining,
          i.`sales_amount`,
          p.`created`
          FROM
          `payments` AS p
          LEFT JOIN `invoice_payments` AS ip
          ON ip.`payment_id` = p.`id`
          LEFT JOIN `an_invoice` AS i
          ON i.`invoice_id` = ip.`invoice_id`
          WHERE p.`refrence_id` = ".$customer_id."
          ORDER BY p.`id` ASC "; */

        /* $sql = "(SELECT 
          i.invoice_id,
          i.`invoice_date`,
          p.id,
          ip.`payment_amount` AS invoicepayment,
          p.`refernce_type`,
          i.`sales_amount`,
          IF(
          p.`payment_amount` IS NULL,
          0,
          p.`payment_amount`
          ) AS payment_amount,
          IF(
          p.`payment_amount` IS NULL,
          0,
          p.`payment_amount`
          ) - ip.`payment_amount` AS balance,
          IF(
          i.`sales_amount` IS NULL,
          0,
          i.sales_amount
          ) - IF(
          ip.`payment_amount` IS NULL,
          0,
          ip.`payment_amount`
          ) AS reamining,
          i.`sales_amount`,
          p.`created`,
          i.`invoice_type`
          FROM
          `an_invoice` AS i
          LEFT JOIN `invoice_payments` AS ip
          ON ip.`invoice_id` = i.`invoice_id`
          LEFT JOIN `payments` AS p
          ON p.`id` = ip.`payment_id`
          WHERE i.`customer_id` = ".$customer_id.")
          UNION
          (SELECT
          i.invoice_id,
          i.`invoice_date`,
          p.id,
          ip.`payment_amount` AS invoicepayment,
          p.`refernce_type`,
          i.`sales_amount`,
          IF(
          p.`payment_amount` IS NULL,
          0,
          p.`payment_amount`
          ) AS payment_amount,
          IF(
          p.`payment_amount` IS NULL,
          0,
          p.`payment_amount`
          ) - ip.`payment_amount` AS balance,
          IF(
          i.`sales_amount` IS NULL,
          0,
          i.sales_amount
          ) - IF(
          ip.`payment_amount` IS NULL,
          0,
          ip.`payment_amount`
          ) AS reamining,
          i.`sales_amount`,
          p.`created`,
          i.`invoice_type`
          FROM
          `payments` AS p
          LEFT JOIN `invoice_payments` AS ip
          ON ip.`payment_id` = p.`id`
          LEFT JOIN `an_invoice` AS i
          ON i.`invoice_id` = ip.`invoice_id`
          WHERE p.`refrence_id` = ".$customer_id."
          AND p.`payment_amount` != '0')"; */
        $sql = "(SELECT 
  i.invoice_id,
  i.`invoice_date`,
  p.id,
  ip.`payment_amount` AS invoicepayment,
    p.`refernce_type`,    
  i.`sales_amount`,
  IF(
    p.`payment_amount` IS NULL,
    0,
    p.`payment_amount`
  ) AS payment_amount,
  IF(
    p.`payment_amount` IS NULL,
    0,
    p.`payment_amount`
  ) - ip.`payment_amount` AS balance,
  IF(
    i.`sales_amount` IS NULL,
    0,
    i.sales_amount
  ) - IF(
    ip.`payment_amount` IS NULL,
    0,
    ip.`payment_amount`
  ) AS reamining,
  i.`sales_amount`,
  p.`created`,
  i.`invoice_type` ,
    ip.`inv_payment_id`
FROM
  `payments` AS p 
  LEFT JOIN `invoice_payments` AS ip 
    ON ip.`payment_id` = p.`id` 
  LEFT JOIN `an_invoice` AS i 
    ON i.`invoice_id` = ip.`invoice_id` 
WHERE p.`refrence_id` = " . $customer_id . " 
  AND p.`payment_amount` != '0'  AND p.status='1' AND i.`cancel_sales`='0') 
UNION
(SELECT 
  i.invoice_id,
  i.`invoice_date`,
  p.id,
  ip.`payment_amount` AS invoicepayment,
    p.`refernce_type`,    
  i.`sales_amount`,
  IF(
    p.`payment_amount` IS NULL,
    0,
    p.`payment_amount`
  ) AS payment_amount,
  IF(
    p.`payment_amount` IS NULL,
    0,
    p.`payment_amount`
  ) - ip.`payment_amount` AS balance,
  IF(
    i.`sales_amount` IS NULL,
    0,
    i.sales_amount
  ) - IF(
    ip.`payment_amount` IS NULL,
    0,
    ip.`payment_amount`
  ) AS reamining,
  i.`sales_amount`,
  p.`created`,
  i.`invoice_type` ,
    ip.`inv_payment_id`
FROM
  `an_invoice` AS i 
  LEFT JOIN `invoice_payments` AS ip 
    ON ip.`invoice_id` = i.`invoice_id` 
  LEFT JOIN `payments` AS p 
    ON p.`id` = ip.`payment_id` AND p.status = '1'
WHERE i.`customer_id` = " . $customer_id . " AND i.`cancel_sales`='0' ) ORDER BY id,invoice_id,inv_payment_id  ASC";

        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function insert($data, $table) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }


    function getAllStores(){
        $this->db->select('storeid,storename');

        $this->db->from('bs_store');


            $this->db->where('storestatus', 'A');

        $this->db->order_by("storename", "ASC");

        $query = $this->db->get();
        if($query->num_rows() > 1){
                return $query->result();
        }
        else{
            return false;
        }
    }

//----------------------------------------------------------------------

    /*
     * Get all Customers
     * return Object
     */
    public function get_all_customers() {
        $membertype = get_member_type();

        if ($membertype != 5) {
            $this->db->where('ownerid', ownerid());
        }

        $query = $this->db->get($this->_table_customers);

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

//----------------------------------------------------------------------

    /**
     * 
     * Enter description here ...
     */
    public function add_new_customer($employeecode) {
        $data = $this->input->post();
        $data['companyid'] = null;
        $data['branchid'] = null;
        $userid = $this->input->post('userid');

        $data['employeecode'] = $employeecode;
        $data['buywithtotal'] = post('buywithtotal');

        $data['upassword'] = md5($this->input->post('upassword'));

        $foundbefore = $this->get_customers_detail('', 'username', post('fullname'));
        unset($data['sub_mit'], $data['sub_reset'], $data['userid'], $data['confpassword']);
        unset($data['due_charges']);

        if (!$foundbefore) {
            if ($userid != '') {
                $this->db->where('userid', $userid);
                $this->db->update($this->config->item('table_users'), $data);
                $this->add_due_charges($userid);
                do_redirect('options?e=10');
            } else {
                $this->add_due_charges();    
                $this->db->insert($this->config->item('table_users'), $data);
                $customer_id = $this->db->insert_id();
                
                if ($data['supplier'] == "") {
                    $this->add_due_charges($customer_id);
                } else {
                    $this->add_due_charges2($customer_id);
                }
                do_redirect('options?e=10');
            }
        } else {
            do_redirect('options?e=13');
        }
    }

    function getCustomersDues($customer_id) {
        $sql = "SELECT 
				  i.`customer_id`,
				    i.invoice_id,
				  SUM(sales_amount) AS sales_amount,
				  SUM(pamount) AS amount 
				FROM
				  `an_invoice` AS i 
				  INNER JOIN `bs_users` AS c 
					ON c.`userid` = i.`customer_id` 
				  LEFT JOIN 
					(SELECT 
					  SUM(ip.`payment_amount`) AS pamount,
					  ip.`invoice_id` 
					FROM
					  `invoice_payments` AS ip 
					  INNER JOIN `an_invoice` AS ii 
						ON ii.`invoice_id` = ip.`invoice_id` 
					  INNER JOIN `bs_users` AS c 
						ON c.`userid` = ii.`customer_id` 
					WHERE c.`userid` = " . $customer_id . " AND ii.`invoice_type` ='duecharges'
					GROUP BY ii.`customer_id`) d 
					ON d.invoice_id = i.`invoice_id` 
				WHERE i.customer_id = " . $customer_id . "  AND i.`invoice_status` = '0' AND i.`invoice_type` ='duecharges'
				GROUP BY i.`customer_id`";
        $q = $this->db->query($sql);
        return $q->row();
    }

    function get_all_calculation() {
        $sql = "SELECT 
  c.`userid`,
  c.fullname,
  IF(
    totalpayment IS NULL,
    0,
    totalpayment
  ) AS totalpayment,
  IF(
    totalinvpayment IS NULL,
    0,
    totalinvpayment
  ) AS totalinvpayment,
  IF(totalsales IS NULL, 0, totalsales) AS totalsales,
  IF(totalsales IS NULL, 0, totalsales) - IF(
    totalinvpayment IS NULL,
    0,
    totalinvpayment
  ) AS remainng,
  IF(
    totalpayment IS NULL,
    0,
    totalpayment
  ) - IF(
    totalinvpayment IS NULL,
    0,
    totalinvpayment
  ) AS balance 
FROM
  `bs_users` AS c 
  LEFT JOIN 
    (SELECT 
      SUM(p.`payment_amount`) AS totalpayment,
      p.`refrence_id` 
    FROM
      `payments` AS p 
    GROUP BY  p.`refrence_id`) AS pp 
    ON pp.refrence_id = c.userid 
  LEFT JOIN 
    (SELECT 
      SUM(ip.`payment_amount`) AS totalinvpayment,
      ii.`customer_id` 
    FROM
      `invoice_payments` AS ip 
      INNER JOIN `an_invoice` AS ii 
        ON ii.`invoice_id` = ip.`invoice_id` 
    GROUP BY ii.`customer_id`) AS ipp 
    ON ipp.customer_id = c.userid 
  LEFT JOIN 
    (SELECT 
      SUM(i.`sales_amount`) AS totalsales,
      i.`customer_id` 
    FROM
      `an_invoice` AS i 
    GROUP BY i.`customer_id`) AS inv 
    ON inv.customer_id = c.userid 
    GROUP BY c.userid";

        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getAllCustomersInvoice() {
        $sql = "(SELECT 
  u.`fullname`,	
  i.invoice_id,
  i.`invoice_date`,
  p.id,
  ip.`payment_amount` AS invoicepayment,
  p.`refernce_type`,
  
  i.`sales_amount`,
  IF(
    p.`payment_amount` IS NULL,
    0,
    p.`payment_amount`
  ) AS payment_amount,
  IF(
    p.`payment_amount` IS NULL,
    0,
    p.`payment_amount`
  ) - ip.`payment_amount` AS balance,
  IF(
    i.`sales_amount` IS NULL,
    0,
    i.sales_amount
  ) - IF(
    ip.`payment_amount` IS NULL,
    0,
    ip.`payment_amount`
  ) AS reamining,
  i.`sales_amount`,
  p.`created`,
  i.`invoice_type`,
  ip.`inv_payment_id` 
FROM
  `payments` AS p 
  LEFT JOIN `invoice_payments` AS ip 
    ON ip.`payment_id` = p.`id` 
  LEFT JOIN `an_invoice` AS i 
    ON i.`invoice_id` = ip.`invoice_id` 
  LEFT JOIN `bs_users` AS u 
    ON u.`userid` = p.`refrence_id` 
WHERE p.`payment_amount` != '0') 
UNION
(SELECT 
  u.`fullname`,
  i.invoice_id,
  i.`invoice_date`,
  p.id,
  ip.`payment_amount` AS invoicepayment,
  p.`refernce_type`,
  i.`sales_amount`,
  IF(
    p.`payment_amount` IS NULL,
    0,
    p.`payment_amount`
  ) AS payment_amount,
  IF(
    p.`payment_amount` IS NULL,
    0,
    p.`payment_amount`
  ) - ip.`payment_amount` AS balance,
  IF(
    i.`sales_amount` IS NULL,
    0,
    i.sales_amount
  ) - IF(
    ip.`payment_amount` IS NULL,
    0,
    ip.`payment_amount`
  ) AS reamining,
  i.`sales_amount`,
  p.`created`,
  i.`invoice_type`,
  ip.`inv_payment_id` 
FROM
  `an_invoice` AS i 
  LEFT JOIN `invoice_payments` AS ip 
    ON ip.`invoice_id` = i.`invoice_id` 
  LEFT JOIN `payments` AS p 
    ON p.`id` = ip.`payment_id`
 LEFT JOIN `bs_users` AS u
 ON u.`userid` = i.`customer_id`    
    ) 
ORDER BY id,
invoice_id,
inv_payment_id ASC ";

        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getAllCustomersbalance() {

        $sql = "SELECT 
  c.`userid`,
  SUM(i.`sales_amount`) AS sales_amount,
  i.`invoice_total_amount`,
  pamount,
  i.`invoice_date`,
  SUM(pamount) AS amount,
  totalinvoice,
  SUM(i.`sales_amount`) - pamount AS totalremaining,
  c.* 
FROM
  bs_users AS c 
  LEFT JOIN `an_invoice` AS i 
    ON i.`customer_id` = c.`userid` 
  LEFT JOIN 
    (SELECT 
      SUM(ip.`payment_amount`) AS pamount,
      ip.`invoice_id` 
    FROM
      `invoice_payments` AS ip 
      INNER JOIN `an_invoice` AS ii 
        ON ii.`invoice_id` = ip.`invoice_id` 
      INNER JOIN `bs_users` AS c 
        ON c.`userid` = ii.`customer_id` 
    GROUP BY ii.`customer_id`) d 
    ON d.invoice_id = i.`invoice_id` 
    AND i.`invoice_status` = '0' 
  LEFT JOIN 
    (SELECT 
      COUNT(ai.`invoice_id`) AS totalinvoice,
      ai.`customer_id`
      FROM
      `an_invoice` AS ai
	WHERE ai.invoice_type !='duecharges'
       GROUP BY ai.`customer_id`) dd 
    ON dd.customer_id = c.`userid` 
    AND i.`invoice_status` = '0' 
GROUP BY c.`userid`  
ORDER BY c.userid DESC";
        $q = $this->db->query($sql);
        return $q->result();
    }

    function getCustomersInvoiceStatus($id) {

        $sql = "SELECT a.*,
  a.`invoice_id`,
  a.`invoice_total_amount`,
  SUM(ip.`payment_amount`) AS totalpayment,
  c.`username`
FROM
  `an_invoice` AS a 
   LEFT JOIN `bs_users` AS c
    ON c.`userid` = a.`customer_id`
  LEFT JOIN `invoice_payments` AS ip 
    ON ip.`invoice_id` = a.`invoice_id` 
	WHERE a.`invoice_id` = '" . $id . "'";

        $q = $this->db->query($sql);
        return $q->row();
    }

    function get_calculation($id) {
        /*
          $sql="SELECT
          SUM(i.`sales_amount`) AS totalpayment,
          SUM(i.`sales_amount`) - pamount AS totalremaining
          FROM
          `an_invoice` AS i
          LEFT JOIN
          (SELECT
          (p.`payment_amount`) AS pamount,
          ip.`invoice_id`
          FROM
          `invoice_payments` AS ip
          INNER JOIN `an_invoice` AS ii
          ON ii.`invoice_id` = ip.`invoice_id`
          INNER JOIN `bs_users` AS c
          ON c.`userid` = '$id'
          INNER JOIN `payments` AS p
          ON p.`id` = ip.`payment_id`
          GROUP BY ii.`customer_id`,
          p.id) d
          ON d.invoice_id = i.`invoice_id`
          AND i.`invoice_status` = '0'
          WHERE i.`customer_id` = '$id' ";
         */
        /*
          $sql="SELECT
          sales,
          SUM(i.`sales_amount`) AS totalpayment,
          SUM(pamount) AS totalpaid

          FROM
          `an_invoice` AS i
          LEFT JOIN
          (SELECT
          (p.`payment_amount`) AS pamount,
          ip.`invoice_id`
          FROM
          `invoice_payments` AS ip
          INNER JOIN `an_invoice` AS ii
          ON ii.`invoice_id` = ip.`invoice_id`
          INNER JOIN `bs_users` AS c
          ON c.`userid` = '$id'
          INNER JOIN `payments` AS p
          ON p.`id` = ip.`payment_id`
          GROUP BY ii.`customer_id`,
          p.id) d
          ON d.invoice_id = i.`invoice_id`
          AND i.`invoice_status` = '0'
          LEFT JOIN
          (SELECT
          SUM(ii.`sales_amount`) AS sales,
          ii.`customer_id`
          FROM
          `an_invoice` AS ii
          GROUP BY ii.`customer_id`) AS id
          ON id.customer_id = i.`customer_id`
          WHERE i.`customer_id` = '$id' ";
         */
        $sql = "SELECT 
                c.`userid`,
				c.fullname,
                IF(
                  totalpayment IS NULL,
                  0,
                  totalpayment
                ) AS totalpayment,
                IF(
                  totalinvpayment IS NULL,
                  0,
                  totalinvpayment
                ) AS totalinvpayment,
                IF(
                  totalsales IS NULL,
                  0,
                  totalsales
                ) AS totalsales,
                IF(
                  totalsales IS NULL,
                  0,
                  totalsales
                )-IF(
                  totalinvpayment IS NULL,
                  0,
                  totalinvpayment
                ) AS remainng,
                IF(
                  totalpayment IS NULL,
                  0,
                  totalpayment
                ) -IF(
                  totalinvpayment IS NULL,
                  0,
                  totalinvpayment
                ) AS balance
              FROM
                `bs_users` AS c 
                LEFT JOIN 
                  (SELECT 
                    SUM(p.`payment_amount`) AS totalpayment,
                    p.`refrence_id` 
                  FROM
                    `payments` AS p 
                  WHERE p.`refrence_id` = '$id' AND p.status='1') AS pp 
                  ON pp.refrence_id = c.userid 
                LEFT JOIN 
                  (SELECT 
                    SUM(ip.`payment_amount`) AS totalinvpayment,
                    ii.`customer_id` 
                  FROM
                    `invoice_payments` AS ip 
                    INNER JOIN `an_invoice` AS ii 
                      ON ii.`invoice_id` = ip.`invoice_id` 
                  WHERE ii.`customer_id` = '$id' AND ii.`cancel_sales` ='0') AS ipp 
                  ON ipp.customer_id = c.userid 
                   LEFT JOIN 
                  (
              SELECT 
                SUM(i.`sales_amount`) AS totalsales,
                i.`customer_id` 
              FROM
                `an_invoice` AS i 
              WHERE i.`customer_id` = '$id' AND i.`cancel_sales` ='0') AS inv 
                  ON inv.customer_id = c.userid 
              WHERE c.`userid` = '$id' 
              ";
		//echo $sql;
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function add_due_charges2($customer_id) {
        //echo "asasd";
        $all_ids = $this->input->post();
        if ($all_ids['due_charges']) {
            $userid = $this->input->post('userid');
            $due['purchase_total_amount'] = $all_ids['due_charges'];
            $due['sales_amount'] = $all_ids['due_charges'];
            $due['purchase_type'] = 'duecharges';
            $due['customer_id'] = $customer_id;
            //echo "<pre>";
            //print_r($due);
            $this->db->insert('an_purchase', $due);
        }
    }

    function add_due_charges($customer_id=null) {
        //echo "asasd";

        $all_ids = $this->input->post();
        if ($all_ids['due_charges']) {
            $userid = $this->input->post('userid');
            $due['invoice_total_amount'] = post('due_charges');
            $due['sales_amount'] = post('due_charges');
            $due['invoice_type'] = 'duecharges';
            $due['customer_id'] = $customer_id;
            //echo "<pre>";
            //print_r($due);
            $this->db->insert('an_invoice', $due);
        }
    }

    function addopening($customer_id) {
        $all_ids = $this->input->post();
        if ($all_ids['opening_balance']) {
            $userid = $this->input->post('userid');
            $payment['payment_type'] = 'receipt';
            $payment['refernce_type'] = 'opening';
            $payment['payment_amount'] = $all_ids['opening_balance'];
            $payment['user_id'] = $userid;
            $payment['refrence_id'] = $customer_id;
            $this->db->insert('payments', $payment);
        }
    }

//----------------------------------------------------------------------

    /**
     * 
     * Enter description here ...
     */
    public function users_list($id = null,$tp ='') {
        /*       $permission_type_id = $this->input->post('permission_type_id');
          $member_type = get_member_type();
          $query = "SELECT
          a.userid as cuserid,
          a.`fullname` as cfullname,
          a.`username`,
          bs_users_bank_transaction.*,
          bs_sales_invoice.*,
          IF(a.`employeecode`='','-----',a.`employeecode`) AS employeecode,
          IF(a.`office_number`='','-----',a.`office_number`) AS office_number,
          IF(a.`fax_number`='','-----',a.`fax_number`) AS fax_number,
          IF(a.`phone_number`='','-----',a.`phone_number`) AS phone_number,
          IF(a.`status`='A','Active','Deactive') AS `status`,
          b.`permissionhead`,
          c.`fullname` AS ownername,
          SUM(bs_users_bank_transaction.`cheque_amount`) - SUM(bs_sales_invoice.`net_amount`) AS Debit,
          SUM(bs_users_bank_transaction.`cheque_amount`) AS Credit,
          SUM(bs_users_bank_transaction.`cheque_amount`) + SUM(bs_sales_invoice.`net_amount`) AS Total
          FROM
          bs_users AS a
          LEFT JOIN bs_users_bank_transaction ON bs_users_bank_transaction.userid=a.userid
          LEFT JOIN bs_sales_invoice ON bs_sales_invoice.customerid=a.userid
          ,
          bs_users As c,
          bs_permission_type AS b WHERE a.`member_type` = b.`permission_type_id` AND b.`permission_type_id`=6 AND a.ownerid=c.userid AND ";
          if ($member_type != '5' && $id == 0) {
          $query .= " (a.ownerid='" . $this->session->userdata('bs_ownerid') . "' OR a.ownerid='" . $this->session->userdata('bs_userid') . "') AND ";
          }



          $query .= " a.userid > 0 GROUP BY a.`fullname` Order by a.fullname ASC";
         */
        /* $sql="
          SELECT
          c.`userid` ,
          SUM(i.`sales_amount`),
          i.`invoice_total_amount`,
          pamount,
          i.`invoice_date`,
          SUM(pamount) AS amount ,
          COUNT(i.invoice_id) AS totalinvoice,
          SUM(i.`sales_amount`)-pamount AS totalremaining,
          c.*
          FROM
          bs_users AS c
          LEFT JOIN `an_invoice` AS i
          ON i.`customer_id`  = c.`userid`
          LEFT JOIN
          (SELECT
          SUM(ip.`payment_amount`) AS pamount,
          ip.`invoice_id`
          FROM
          `invoice_payments` AS ip
          INNER JOIN `an_invoice` AS ii
          ON ii.`invoice_id` = ip.`invoice_id`
          INNER JOIN `bs_users` AS c
          ON c.`userid` = ii.`customer_id`
          GROUP BY ii.`customer_id`) d
          ON d.invoice_id = i.`invoice_id` AND i.`invoice_status` = '0'
          GROUP BY c.`userid` ORDER BY c.userid DESC";
         */
         $this->db->query('SET SQL_BIG_SELECTS=1'); 
          if ($id != 0) {
            $query = " AND c.userid='" . $id . "' ";
          }else{
             $query = ""; 
          }

        if($tp =='0'){
            $query .= " AND (IF(sales IS NULL, 0, sales) - IF(paaaa IS NULL, 0, paaaa)) >0 ";
        }
        $sql = "SELECT 
                sales,
                ptamount,
                paaaa,
                c.`userid`,
                IF(sales IS NULL, 0, sales) AS sales_amount,
                i.`invoice_total_amount`,
                IF(ptamount IS NULL, 0, ptamount) - IF(paaaa IS NULL, 0, paaaa) AS amount,
                i.`invoice_date`,
                SUM(pamount) AS pamount,
                totalinvoice,
                IF(sales IS NULL, 0, sales) - pamount AS totalremaining,
                IF(sales IS NULL, 0, sales) - IF(paaaa IS NULL, 0, paaaa) AS totalremaining,
                c.* 
              FROM
                bs_users AS c 
                LEFT JOIN `an_invoice` AS i 
                  ON i.`customer_id` = c.`userid` 
                LEFT JOIN 
                  (SELECT 
                    (p.`payment_amount`) - (ip.`payment_amount`) AS pamount,
                    ip.`invoice_id` 
                  FROM
                    `invoice_payments` AS ip 
                    INNER JOIN `an_invoice` AS ii 
                      ON ii.`invoice_id` = ip.`invoice_id` 
                    INNER JOIN `bs_users` AS c 
                      ON c.`userid` = ii.`customer_id` 
                    INNER JOIN `payments` AS p 
                      ON p.`id` = ip.`payment_id` 
                  GROUP BY ii.`customer_id`,
                    p.id) d 
                  ON d.invoice_id = i.`invoice_id` 
                  AND i.`invoice_status` = '0' 
                LEFT JOIN 
                  (SELECT 
                    COUNT(ai.`invoice_id`) AS totalinvoice,
                    ai.`customer_id`
                  FROM
                    `an_invoice` AS ai 
                  WHERE ai.invoice_type != 'duecharges' 
                  GROUP BY ai.`customer_id`) dd 
                  ON dd.customer_id = c.`userid` 
                  AND i.`invoice_status` = '0' 
                LEFT JOIN 
                  (SELECT 
                    SUM(ipp.`payment_amount`) paaaa,
                    iio.`customer_id` 
                  FROM
                    `invoice_payments` AS ipp 
                    INNER JOIN `an_invoice` AS iio 
                      ON iio.`invoice_id` = ipp.invoice_id 
                  GROUP BY iio.customer_id) pp 
                  ON pp.customer_id = c.`userid` 
                LEFT JOIN 
                  (SELECT 
                    SUM(pt.`payment_amount`) ptamount,
                    pt.`refrence_id` 
                  FROM
                    `payments` AS pt 
                  GROUP BY pt.`refrence_id`) ppt 
                  ON ppt.`refrence_id` = c.`userid` 
                LEFT JOIN 
                  (SELECT 
                    SUM(ic.`sales_amount`) AS sales,
                    ic.`customer_id` 
                  FROM
                    `an_invoice` AS ic 
                  GROUP BY ic.`customer_id`) AS icc 
                  ON icc.customer_id = c.userid 

              WHERE c.type='1' and c.member_type='6' $query
              GROUP BY c.`userid` 
              ORDER BY c.userid DESC limit 10";
        $sql="SELECT * FROM bs_users AS c WHERE c.type='1' AND c.member_type='6' ";
        $q = $this->db->query($sql);
        return $q->result();
    }

    public function users_list2($id = 0) {
        /*       $permission_type_id = $this->input->post('permission_type_id');
          $member_type = get_member_type();
          $query = "SELECT
          a.userid as cuserid,
          a.`fullname` as cfullname,
          a.`username`,
          bs_users_bank_transaction.*,
          bs_sales_invoice.*,
          IF(a.`employeecode`='','-----',a.`employeecode`) AS employeecode,
          IF(a.`office_number`='','-----',a.`office_number`) AS office_number,
          IF(a.`fax_number`='','-----',a.`fax_number`) AS fax_number,
          IF(a.`phone_number`='','-----',a.`phone_number`) AS phone_number,
          IF(a.`status`='A','Active','Deactive') AS `status`,
          b.`permissionhead`,
          c.`fullname` AS ownername,
          SUM(bs_users_bank_transaction.`cheque_amount`) - SUM(bs_sales_invoice.`net_amount`) AS Debit,
          SUM(bs_users_bank_transaction.`cheque_amount`) AS Credit,
          SUM(bs_users_bank_transaction.`cheque_amount`) + SUM(bs_sales_invoice.`net_amount`) AS Total
          FROM
          bs_users AS a
          LEFT JOIN bs_users_bank_transaction ON bs_users_bank_transaction.userid=a.userid
          LEFT JOIN bs_sales_invoice ON bs_sales_invoice.customerid=a.userid
          ,
          bs_users As c,
          bs_permission_type AS b WHERE a.`member_type` = b.`permission_type_id` AND b.`permission_type_id`=6 AND a.ownerid=c.userid AND ";
          if ($member_type != '5' && $id == 0) {
          $query .= " (a.ownerid='" . $this->session->userdata('bs_ownerid') . "' OR a.ownerid='" . $this->session->userdata('bs_userid') . "') AND ";
          }

          if ($id != 0) {
          $query .= " a.userid='" . $id . "' AND ";
          }

          $query .= " a.userid > 0 GROUP BY a.`fullname` Order by a.fullname ASC";
         */
        /* $sql="
          SELECT
          c.`userid` ,
          SUM(i.`sales_amount`),
          i.`invoice_total_amount`,
          pamount,
          i.`invoice_date`,
          SUM(pamount) AS amount ,
          COUNT(i.invoice_id) AS totalinvoice,
          SUM(i.`sales_amount`)-pamount AS totalremaining,
          c.*
          FROM
          bs_users AS c
          LEFT JOIN `an_invoice` AS i
          ON i.`customer_id`  = c.`userid`
          LEFT JOIN
          (SELECT
          SUM(ip.`payment_amount`) AS pamount,
          ip.`invoice_id`
          FROM
          `invoice_payments` AS ip
          INNER JOIN `an_invoice` AS ii
          ON ii.`invoice_id` = ip.`invoice_id`
          INNER JOIN `bs_users` AS c
          ON c.`userid` = ii.`customer_id`
          GROUP BY ii.`customer_id`) d
          ON d.invoice_id = i.`invoice_id` AND i.`invoice_status` = '0'
          GROUP BY c.`userid` ORDER BY c.userid DESC";
         */

        $sql = "SELECT 
  sales,
  ptamount,
  paaaa,
  c.`userid`,
  IF(sales IS NULL, 0, sales) AS sales_amount,
  i.`invoice_total_amount`,
  IF(ptamount IS NULL, 0, ptamount) - IF(paaaa IS NULL, 0, paaaa) AS amount,
  i.`invoice_date`,
  SUM(pamount) AS pamount,
  totalinvoice,
  IF(sales IS NULL, 0, sales) - pamount AS totalremaining,
  IF(sales IS NULL, 0, sales) - IF(paaaa IS NULL, 0, paaaa) AS totalremaining,
  c.* 
FROM
  bs_users AS c 
  LEFT JOIN `an_invoice` AS i 
    ON i.`customer_id` = c.`userid` 
  LEFT JOIN 
    (SELECT 
      (p.`payment_amount`) - (ip.`payment_amount`) AS pamount,
      ip.`invoice_id` 
    FROM
      `invoice_payments` AS ip 
      INNER JOIN `an_invoice` AS ii 
        ON ii.`invoice_id` = ip.`invoice_id` 
      INNER JOIN `bs_users` AS c 
        ON c.`userid` = ii.`customer_id` 
      INNER JOIN `payments` AS p 
        ON p.`id` = ip.`payment_id` 
    GROUP BY ii.`customer_id`,
      p.id) d 
    ON d.invoice_id = i.`invoice_id` 
    AND i.`invoice_status` = '0' 
  LEFT JOIN 
    (SELECT 
      COUNT(ai.`invoice_id`) AS totalinvoice,
      ai.`customer_id`
    FROM
      `an_invoice` AS ai 
    WHERE ai.invoice_type != 'duecharges' 
    GROUP BY ai.`customer_id`) dd 
    ON dd.customer_id = c.`userid` 
    AND i.`invoice_status` = '0' 
  LEFT JOIN 
    (SELECT 
      SUM(ipp.`payment_amount`) paaaa,
      iio.`customer_id` 
    FROM
      `invoice_payments` AS ipp 
      INNER JOIN `an_invoice` AS iio 
        ON iio.`invoice_id` = ipp.invoice_id 
    GROUP BY iio.customer_id) pp 
    ON pp.customer_id = c.`userid` 
  LEFT JOIN 
    (SELECT 
      SUM(pt.`payment_amount`) ptamount,
      pt.`refrence_id` 
    FROM
      `payments` AS pt 
    GROUP BY pt.`refrence_id`) ppt 
    ON ppt.`refrence_id` = c.`userid` 
  LEFT JOIN 
    (SELECT 
      SUM(ic.`sales_amount`) AS sales,
      ic.`customer_id` 
    FROM
      `an_invoice` AS ic 
    GROUP BY ic.`customer_id`) AS icc 
    ON icc.customer_id = c.userid 
    
WHERE c.type='2'and c.member_type!='5'
GROUP BY c.`userid` 
ORDER BY c.userid DESC ";
        $q = $this->db->query($sql);
        return $q->result();
    }

    function getCustomerBalance($customer_id) {
        $sql = "SELECT 
  c.`userid`,
  SUM(i.`sales_amount`) AS sales_amount,
  i.`invoice_total_amount`,
  pamount,
  i.`invoice_date`,
  SUM(pamount) AS amount,
  totalinvoice,
  SUM(i.`sales_amount`) - pamount AS totalremaining,
  c.* 
FROM
  bs_users AS c 
  LEFT JOIN `an_invoice` AS i 
    ON i.`customer_id` = c.`userid` 
  LEFT JOIN 
    (SELECT 
      SUM(ip.`payment_amount`) AS pamount,
      ip.`invoice_id` 
    FROM
      `invoice_payments` AS ip 
      INNER JOIN `an_invoice` AS ii 
        ON ii.`invoice_id` = ip.`invoice_id` 
      INNER JOIN `bs_users` AS c 
        ON c.`userid` = ii.`customer_id` 
    GROUP BY ii.`customer_id`) d 
    ON d.invoice_id = i.`invoice_id` 
    AND i.`invoice_status` = '0' 
  LEFT JOIN 
    (SELECT 
      COUNT(ai.`invoice_id`) AS totalinvoice,
      ai.`customer_id`
      FROM
      `an_invoice` AS ai
	WHERE ai.invoice_type !='duecharges'
       GROUP BY ai.`customer_id`) dd 
    ON dd.customer_id = c.`userid` 
    AND i.`invoice_status` = '0' 
    WHERE c.`userid` = " . $customer_id . "
GROUP BY c.`userid`  
ORDER BY c.userid DESC";
        $q = $this->db->query($sql);
        return $q->row();
    }

    function getLastEmployeeCode() {
        $sql = "SELECT employeecode FROM `bs_users`  WHERE  employeecode!=''  ORDER BY `bs_users`.`userid` DESC LIMIT 1";
        $q = $this->db->query($sql);
        return $q->row();
    }

//--------------------------------------------------------------------

    /**
     * 
     * Enter description here ...
     * @param unknown_type $id
     */
    public function get_customers_detail($id, $par = null, $vpar = null) {

        if ($par) {
            $this->db->where($par, $vpar);
        } else {
            $this->db->where('userid', $id);
        }

        $query = $this->db->get($this->config->item('table_users'));

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

//--------------------------------------------------------------------

    /**
     * 
     * Get Company ID
     * @param INTEGER $id
     * return integer
     */
    public function get_company_id($id) {
        $this->db->where('ownerid', $id);
        $query = $this->db->get($this->_table_companies);

        if ($query->num_rows() > 0) {
            return $query->row()->companyid;
        }
    }

    //--------------------------------------------------------------------

    /**
     * 
     * Get Branch ID
     * @param INTEGER $id
     * return STRING
     */
    public function get_branch_id($companyid) {
        $this->db->where('companyid', $companyid);
        $query = $this->db->get($this->_table_company_branches);

        if ($query->num_rows() > 0) {
            return $query->row()->branchid;
        }
    }

//----------------------------------------------------------------------

    /*
     * Delete Bulk Record
     * @param $customer_ids array
     * return TRUE
     */
    function delete_customers($user_ids) {
        $this->db->where_in('userid', $user_ids);
        $this->db->delete($this->_table_users);

        return true;
    }

//----------------------------------------------------------------------
    function getSaleInvocies($id = '') {
        if ($id) {
            $subq = "WHERE u.`userid` = '" . $id . "'";
        }
        /*
          $sql = "SELECT
          u.`fullname` AS customername,
          u.`userid`,
          bsi.*
          FROM
          `an_invoice` AS bsi
          INNER JOIN `bs_users` AS u
          ON u.`userid` = bsi.`customer_id`
          " . $subq . ""; */
        $sql = "
            SELECT 
            i.`customer_id`,
            i.`invoice_id` AS invoice_id,
            i.`sales_amount`,
            i.`invoice_total_amount`,
            c.*,
            pamount,
            bii.invoice_item_discount_type as discount_type,
            i.`invoice_date` as i_date,
            SUM(pamount) AS amount ,
            SUM(invoice_item_discount) AS iidiscount,
            invoice_total_amount AS iiprice,
            SUM(invoice_item_price_purchase) AS iiprice_purchase,
            sales_amount-pamount AS remaining,
            GROUP_CONCAT(ds.bs_user_id) AS cols,
            SUM(bs_sales_commession) AS salescommession
          FROM
            `an_invoice` AS i 
            INNER JOIN `bs_users` AS c 
              ON c.`userid` = i.`customer_id` 
            LEFT JOIN 
              (SELECT 
                SUM(ip.`payment_amount`) AS pamount,
                ip.`invoice_id` 
              FROM
                `invoice_payments` AS ip 
                INNER JOIN `an_invoice` AS ii 
                  ON ii.`invoice_id` = ip.`invoice_id` 
                INNER JOIN `bs_users` AS c 
                  ON c.`userid` = ii.`customer_id` 
              WHERE c.`userid` = '$id' 
              GROUP BY ip.`invoice_id`) d 
              ON d.invoice_id = i.`invoice_id` 
          LEFT JOIN bs_invoice_items AS bii ON bii.invoice_id=i.invoice_id   
          LEFT JOIN bs_sales_invoice_persons AS spi 
            ON spi.invoice_id = i.invoice_id 
          LEFT JOIN bs_sales AS ds 
            ON ds.bs_sales_id = spi.sales_id 
          WHERE i.customer_id = '$id'  
          GROUP BY i.`invoice_id`";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function getAllcharges() {
        $sql = "SELECT * FROM `an_charges`";
        $q = $this->db->query($sql);
        return $q->result();
    }

    function test($customerid) {
        $sql = "SELECT 
  i.`customer_id`,
  i.`invoice_id` AS invoice_id,
  i.`sales_amount`,
  i.`invoice_total_amount`,
  pamount,
  i.`invoice_date`,
  SUM(pamount) AS amount 
FROM
  `an_invoice` AS i 
  INNER JOIN `bs_users` AS c 
    ON c.`userid` = i.`customer_id` 
  LEFT JOIN 
    (SELECT 
      SUM(ip.`payment_amount`) AS pamount,
      ip.`invoice_id` 
    FROM
      `invoice_payments` AS ip 
      INNER JOIN `an_invoice` AS ii 
        ON ii.`invoice_id` = ip.`invoice_id` 
      INNER JOIN `bs_users` AS c 
        ON c.`userid` = ii.`customer_id` 
    WHERE c.`userid` = " . $customerid . " 
    GROUP BY ip.`invoice_id`) d 
    ON d.invoice_id = i.`invoice_id` 
WHERE i.customer_id = " . $customerid . "  AND i.`invoice_status` = '0'
GROUP BY i.`invoice_id` ";
    }

    function getLastpaymentIdByCustomer($customer_id, $type) {
        /* $sql = "SELECT 
         * 
          FROM
          `payments` AS p
          WHERE p.`refrence_id` = ".$customer_id."
          AND p.`payment_type` = 'receipt' AND p.`refernce_type` = 'advance' OR p.`refernce_type` = 'opening'
          ORDER BY p.`id` DESC LIMIT 1"; */
        $sql = "SELECT 
  p.`id`,p.`payment_amount`,ip.`payment_amount` AS paidamount 
FROM
  `payments` AS p 
  LEFT JOIN `invoice_payments` AS ip
  ON ip.`payment_id` = p.`id`
WHERE p.`payment_type` = 'receipt'  
  AND p.`refernce_type` = '" . $type . "' 
  AND p.`refrence_id` =" . $customer_id . "     
  ORDER BY p.id DESC";

        //echo $sql;
        $q = $this->db->query($sql);
        return $q->row();
    }

    function getLastpaymentIdByCustomerMultiple($customer_id, $returntype) {
        /* $sql = "SELECT 
         * 
          FROM
          `payments` AS p
          WHERE p.`refrence_id` = ".$customer_id."
          AND p.`payment_type` = 'receipt' AND p.`refernce_type` = 'advance' OR p.`refernce_type` = 'opening'
          ORDER BY p.`id` DESC LIMIT 1"; */
        /* $sql = "SELECT 
          p.`id`,
          p.`payment_amount`,
          ip.`payment_amount` AS paidamount,
          IF(
          p.payment_amount IS NULL,
          0,
          p.payment_amount
          ) - IF(
          ip.`payment_amount` IS NULL,
          0,
          ip.`payment_amount`
          ) AS payment_amount
          FROM
          `payments` AS p
          LEFT JOIN `invoice_payments` AS ip
          ON ip.`payment_id` = p.`id`
          WHERE p.`payment_type` = 'receipt'
          AND p.`refrence_id` = ". $customer_id ."
          ORDER BY p.id DESC "; */
        $sql = "
SELECT 
  p.`id`,
  p.`payment_amount`,
  ip.`payment_amount` AS paidamount,
  @v2 := FORMAT(IF(
    p.payment_amount IS NULL,
    0,
    p.payment_amount
  ) - IF(
    ip.`payment_amount` IS NULL,
    0,
    ip.`payment_amount`
  ),0) AS ppp ,
   @v2
FROM
  `payments` AS p 
  LEFT JOIN `invoice_payments` AS ip 
    ON ip.`payment_id` = p.`id` 
WHERE p.`payment_type` = 'receipt' 
  AND p.`refrence_id` = " . $customer_id . "
  AND p.`refernce_type`!= 'advance'
    GROUP BY p.id
ORDER BY p.id DESC ";

        //echo $sql;
        $q = $this->db->query($sql);
        if ($returntype != "") {
            return $q->result();
        } else {
            return $q->row();
        }
    }

    /*     * **** */

    function getCustomerPaymetns($id) {
        /* $sql = "SELECT 
          (SELECT
          SUM(jc.`sales_value`)
          FROM
          `an_jobs` AS j
          INNER JOIN `jobs_charges` AS jc
          ON jc.`job_id` = j.`id`
          INNER JOIN `an_jobs_invoice_relation` AS ij
          ON ij.`job_id` = j.`id`
          WHERE j.`customer_id` = '" . $id . "'
          GROUP BY j.`customer_id`) AS totalcharges,
          (SELECT
          SUM(jp.`payment_amount`)
          FROM
          `job_payment` AS jp
          WHERE jp.`customer_id` = '" . $id . "') AS totaljobpayment ,
          (SELECT
          SUM(p.`payment_amount`)
          FROM
          `payments`AS p
          WHERE p.`refrence_id` = '" . $id . "') AS totalpayment
          FROM
          `an_customers` AS c
          WHERE c.`id` = '" . $id . "'  ";
         * 
         */
        /* $sql = "SELECT 
          (SELECT
          SUM(i.`sales_amount`)
          FROM
          `an_invoice` AS i
          WHERE i.`customer_id` = '" . $id . "'
          GROUP BY i.`customer_id`) AS totalcharges,
          (SELECT
          SUM(ip.`payment_amount`)
          FROM
          `invoice_payments` AS ip
          INNER JOIN `an_invoice` AS i
          WHERE i.`customer_id` = '" . $id . "') AS totalpayment,
          (SELECT
          SUM(p.`payment_amount`)
          FROM
          `payments` AS p
          WHERE p.`refrence_id` = '" . $id . "') AS totalpayment
          FROM
          `bs_users` AS u
          WHERE u.`userid`  ='" . $id . "'"; */

        $sql = "
				SELECT 
  IF(
    SUM(p.`payment_amount`) IS NULL,
    0,
    SUM(p.`payment_amount`)
  ) AS totalpayment,
  IF(
    SUM(totalcharges) IS NULL,
    0,
    SUM(totalcharges)
  ) 
   AS totalcharges 
FROM
  `payments` AS p 
  LEFT JOIN 
    (SELECT 
      ip.`payment_id`,
      SUM(ip.`payment_amount`) AS totalcharges 
    FROM
      `invoice_payments` AS ip 
      INNER JOIN `an_invoice` AS i 
        ON i.`invoice_id` = ip.`invoice_id` 
    WHERE i.`customer_id` = '" . $id . "') AS ip 
    ON ip.`payment_id` = p.`id` 
WHERE p.`refrence_id` = '" . $id . "' 
  AND p.`payment_type` = 'receipt'";


        //echo $sql;
        //$sql="select sale_payments.*,bs_user.userid,bs_user.openbalance from sale_payments ";
        //$this->db->select('sale_payments.payment_amount,sale_payments.customer_id');
        //$this->db->where('sale_payments.customer_id', $id);
        //$this->db->join('bs_users','bs_users.userid=sale_payments.customer_id','left');
        //$q = $this->db->get('sale_payments');
        $total = 0;
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $return = $q->row();
        } else {
            return false;
        }
    }

    function new_dc_customer($id = null) {
        /* $sql="SELECT 
          i.`customer_id`,
          i.`invoice_id` AS invoice_id,
          i.`sales_amount`,
          i.`invoice_total_amount`,
          pamount,
          i.`invoice_date`,
          SUM(pamount) AS amount
          FROM
          `an_invoice` AS i
          INNER JOIN `bs_users` AS c
          ON c.`userid` = i.`customer_id`
          LEFT JOIN
          (SELECT
          SUM(ip.`payment_amount`) AS pamount,
          ip.`invoice_id`
          FROM
          `invoice_payments` AS ip
          INNER JOIN `an_invoice` AS ii
          ON ii.`invoice_id` = ip.`invoice_id`
          INNER JOIN `bs_users` AS c
          ON c.`userid` = ii.`customer_id`
          WHERE c.`userid` = '$id'
          GROUP BY ip.`invoice_id`) d
          ON d.invoice_id = i.`invoice_id`
          WHERE i.customer_id = '$id'  AND i.`invoice_status` = '0'
          GROUP BY i.`invoice_id`";
         */
        $sql = "SELECT 
              i.`customer_id`,

              SUM(i.`sales_amount`),
              i.`invoice_total_amount`,
              pamount,
              i.`invoice_date`,
              SUM(pamount) AS amount ,
              SUM(i.`sales_amount`)-pamount AS totalremaining
            FROM
              `an_invoice` AS i 
              INNER JOIN `bs_users` AS c 
                ON c.`userid` = i.`customer_id` 
              LEFT JOIN 
                (SELECT 
                  SUM(ip.`payment_amount`) AS pamount,
                  ip.`invoice_id` 
                FROM
                  `invoice_payments` AS ip 
                  INNER JOIN `an_invoice` AS ii 
                    ON ii.`invoice_id` = ip.`invoice_id` 
                  INNER JOIN `bs_users` AS c 
                    ON c.`userid` = ii.`customer_id` 
                WHERE c.`userid` = '60' 
                GROUP BY ii.`customer_id`) d 
                ON d.invoice_id = i.`invoice_id` 
            WHERE i.customer_id = '60'  AND i.`invoice_status` = '0'
            GROUP BY i.`customer_id`";
        $q = $this->db->query($sql);
        if ($q->num_rows()) {

            return $q->row()->totalremaining;
        } else {

            return false;
        }
    }

    function get_balance($id) {

        $this->db->select('bs_users.userid,bs_users.openbalance');
        $this->db->where('bs_users.userid', $id);
        $q = $this->db->get('bs_users');
        return $q->row()->openbalance;
    }

}
