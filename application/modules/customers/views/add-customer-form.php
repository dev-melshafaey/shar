

<div class=" form-wrapper">
    <div class=" col-xs-12">
        <div id="main-content" class="main_content"> 
          <!--<div class="title title alert alert-info"> <span><?php echo lang('add-edit') ?></span> </div>-->
          <div class="notion title title alert alert-info"> * <?php echo lang('mess1') ?> </div>
            <?php error_hander($this->input->get('e')); ?>
            <form action="<?php echo base_url(); ?>customers/add_new_customer" method="post" id="form1"  class="sample-form" name="frm_customer" autocomplete="off">
                <?php //var_dump()?>
                <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
                <input type="hidden" name="userid" id="userid" value="<?php echo $user->userid; ?>" />
                <div class="form">
                    <div class="">
                        <input type="hidden" value="0" name="companyid">
                        <?php //if (get_member_type() == '1' OR get_member_type() == '5'): ?>
                            <!--<div class="g4 form-group">
                                <label class="col-lg-3"><?php echo lang('Company-Name') ?></label>
                                <div class="ui-select col-md-8" >
                                    <div class="">
                                        <?php company_dropbox('companyid', $user->companyid); ?>
                                        <span class="arrow arrowselectbox">&amp;</span>
                                    </div>
                                </div>
                            </div>
                            <div class="g4 form-group">
                                <label class="col-lg-3"><?php echo lang('Branch-Name') ?></label>
                                <br>
                                <div class="ui-select " >
                                    <div class="">
                                        <?php company_branch_dropbox('branchid', $user->branchid, $user->companyid); ?>
                                        <span class="arrow arrowselectbox">&amp;</span>
                                    </div>
                                </div>
                            </div>
                        
                        <div class="g3">
                            <label class=""><?php echo lang('Store-Name') ?></label>
                            <div class="">
                                <div class="ui-select" >
                                    <div class="">
                                        <?php store_dropbox('storeid', $user->storeid, $user->companyid);



                                        ?>

                                             </div>
                                </div>
                            </div>
                        </div>
                        
                            <div class="g2  form-group">
                                <label><?php echo lang('Add-Account') ?>
                                    
                                </label>
                                <input type="checkbox" value="1" id="ad_account" style="width:10%"/>
                            </div>
                            <br clear="all">-->
                            <!--Login Form-->
                            <div class="g4 form-group login_account">
                                <label class=""><?php echo lang('User-Name') ?></label>
                                <input name="username"  id="username" value="<?php echo ($user->username) ? $user->username : post('username'); ?>" type="text"  class="validate[required] form-control" style=""/>
                            </div>
                            <div class="g4 form-group login_account">
                                <label class=""><?php echo lang('Password') ?></label>
                                <input name="upassword" id="upassword" value="" type="password"  class="validate[required] form-control" style=""/>
                            </div>
                            <div class="g4 form-group login_account">
                                <label class=""><?php echo lang('RePassword') ?></label>
                                <input name="confpassword" id="confpassword" value="" type="password"   class="form-control" style=""/>
                            </div>
                            
                        <?php //endif; ?>
                        <br clear="all">
                        <div class="g4 form-group">
                            <label class=""><?php echo lang('Customer-Name') ?></label>
                            <input name="fullname" id="fullname" value="<?php echo ($user->fullname) ? $user->fullname : post('fullname'); ?>" type="text"  class="required  valid form-control"/>
                        </div>
                        <div class="g4 form-group">
                            <label class=""><?php echo lang('Mobile-Number') ?></label>
                            <input name="phone_number" id="phone_number" value="<?php echo $user->phone_number; ?>" type="text"  class="required  valid form-control"/>
                        </div>
                        <div class="g4 form-group">
                            <label class=""><?php echo lang('Contact-Number') ?></label>
                            <input name="office_number" id="office_number" value="<?php echo $user->office_number; ?>" type="text"  class="form-control"/>
                        </div>
                        <div class="g4 form-group">
                            <label class=""><?php echo lang('Fax-Number') ?></label>
                            <input name="fax_number" id="fax_number" value="<?php echo $user->fax_number; ?>" type="text"  class="form-control"/>
                        </div>
                        <br clear="all">
                        <div class="g4 form-group">
                            <label class=""><?php echo lang('Email-Address') ?></label>
                            <input name="email_address" id="email_address" value="<?php echo $user->email_address; ?>" type="text"  class="form-control"/>
                        </div>
                        <div class="g4 form-group">
                            <label class=""><?php echo lang('Address') ?></label>
                            <input name="address" id="address" value="<?php echo $user->address; ?>" type="text"  class="form-control"/>
                        </div>
                        <div class="g4 form-group">
                            <label class=""><?php echo lang('Status') ?></label>
                            <div class="ui-select " >
                                <div class="">
                                    <?php get_statusdropdown($user->status, 'status'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="g4 form-group">
                            <label class=""><?php echo lang('Notes') ?></label>
                            <input name="notes" id="notes" value="<?php echo $user->notes; ?>" type="text"  class="form-control"/>
                        </div>
                        <?php if (get_member_type() != '1' OR get_member_type() != '5'): ?>
                            <input type="hidden" name="companyid" id="companyid"  value="<?php echo $company_id; ?>" />
                            <input type="hidden" name="branchid"  id="branchid" value="<?php echo $branch_id; ?>" />
                        <?php endif; ?>

                        <br clear="all">        
                        <div class="g4 form-group">
                            <label class=""><?php echo lang('Opening-Balance') ?></label>

                            <input name="opening_balance" id="opening_balance" value="<?php //echo isset($customer->credit_limit)?$customer->credit_limit:'';  ?>" type="text"  class="form-control"/>

                        </div>

                        <div class="g4 form-group">
                            <label class=""><?php echo lang('Credit-Amount-Limit') ?></label>

                            <input name="credit_limit" id="credit_limit" value="<?php echo isset($user->credit_limit)?$user->credit_limit:'';  ?>" type="text"  class="form-control"/>

                        </div>


                        <div class="g4 form-group">
                            <label class=""><?php echo lang('due_charges') ?></label>

                            <input name="due_charges" id="due_charges" value="<?php //echo isset($customer->credit_limit)?$customer->credit_limit:'';  ?>" type="text"  class="form-control"/>

                        </div>
                        <div class="g4 form-group">
                            <label class="">Amount Limit<?php //echo lang('time') ?></label>
                            <div class="form_field">
                                <input name="amount_limit" id="amount_limit" value="<?php echo isset($user->amount_limit)?$user->amount_limit:'';  ?>" type="text"  class="form-control"/>
                            </div>
                        </div>
                        <div class="g4 form-group">
                            <label class=""><?php echo lang('time') ?></label>
                            <div class="form_field">
                                <input name="days_limit" id="days_limit" value="<?php echo isset($user->days_limit)?$user->days_limit:'';  ?>" type="text"  class="form-control"/>
                            </div>
                        </div>

                        <div class="g3 form-group">
                            <label class=""><?php echo lang('Account-Type') ?></label>

                            <div class="ui-select ">
                                <div class="">
                                    <?php get_statusdropdown(isset($user->account_type) ? $user->account_type : '', 'account_type', 'account_type', isset($user->id) ? '' : ""); ?>
                                </div>
                            </div>

                        </div>

                        <br clear="all"/>
                        <div class="g2 form-group">
                            <label class=""><?php echo lang('buywithtotal') ?> </label>

                            <input name="buywithtotal" id="buywithtotal" value="1" <?php echo ($user->buywithtotal==1)? 'checked="checked"':'';  ?> type="checkbox" style="width:10%" class=""/> 

                        </div>
                        <br clear="all"/>
                        <?php if($user->userid):?>
                        <div class="g4 form-group">
                            <label class=""><?php echo lang('blacklist')  ?></label>

                            <input name="blacklist" id="blacklist" onclick="blacklist(this.val)" value="1" <?php echo DoSelect($user->blacklist,"1",'checked')?> type="checkbox"  class=""/> 

                        </div>
                        <br clear="all"/>
                        <div class=" form-group" style="display: none">
                            <!--<label class=""><?php echo lang('reson-blacklist') ?></label>-->

                            <textarea class="ckeditor" name="reson_blacklist"><?php echo isset($user->reson_blacklist)?$user->reson_blacklist:'';  ?></textarea>

                        </div>
                        <?php endif;?>
                        <br clear="all"/>
                        

                        <div class=" from-group">
                            <input type="hidden" name="member_type" id="member_type" value="6">
                            <input name="sub_mit" id="sub_mit" type="submit" class="btn-flat primary green flt-r g3" value="<?php echo lang('Add') ?>" />
                            <input name="sub_reset" type="reset" class="btn-flat primary gray flt-r g3" value="<?php echo lang('Reset') ?>" />
                        </div>
                    </div>

                    <!--end of raw--> 
                </div>
            </form>
        </div>
        <!-- END PAGE --> 
    </div>
</div>
