
<div id="main-content" class="main_content">
    <div class="title title alert alert-info">
        <span><?php breadcramb(); ?> >>  <?php echo lang('View-All-Debit-Credit')   ?></span>
    </div>
    <br clear="all"/>
    <br clear="all"/>
    <a href="<?php echo base_url(); ?>customers/staticsdebitcredit/<?php echo $id; ?>">View Debit Credit</a>	
    
        
    <div class=" inp-btns pad-l g16">
        <?php ?>
        <div class="g4 form-group"  style="padding:1px;"><label class="col-md-2"><?php echo lang('Customer-Name'); ?>: </label><?php echo $customerdetails->fullname; ?></div>
        <br clear="all"/>
<!--        <table id="" style="text-align:center;" width="40%">
            <tr>
                <td><strong><?php echo lang('totalCredit'); ?></strong></td>
                <td><strong><?php echo lang('totalDebit'); ?></strong></td> 
                <td><strong><?php echo lang('totalremaining'); ?></strong></td>
                <td><strong><?php echo lang('totalbalance'); ?></strong></td>
            </tr>
            <tr>
                <td><?php echo $customerdetails->totalsales; ?></td>
                <td><?php echo $customerdetails->totalpayment; ?></td>
                <td><?php echo $customerdetails->remainng; ?></td>
                <td><?php echo $customerdetails->balance; ?></td>
            </tr>

        </table>	-->
        
<!--        <div class="green g3 tag">
            <div class="g6"><?php echo lang('totalCredit'); ?></div>
            <div class="g6"><?php echo $customerdetails->totalsales; ?></div>
            
        </div>
        <div class="red g3 tag">
            <div class="g6"><?php echo lang('totalDebit'); ?></div>
            <div class="g6"><?php echo $customerdetails->totalpayment; ?></div>
            
        </div>-->
        <div class="red g5 tag">
            <div class="g6"><?php echo lang('totalremaining'); ?></div>
            <div class="g6"><?php echo $customerdetails->remainng; ?></div>
            
        </div>
        <div class="green g3 tag">
            <div class="g6"><?php echo lang('totalbalance'); ?></div>
            <div class="g6"><?php echo $customerdetails->balance; ?></div>
            
        </div>
    </div>
        
        

    <div class="table-wrapper users-table">
        <form action="<?php //echo form_action_url('delete_customers');   ?>" id="listing" method="post" autocomplete="off">
            <div class="row head">
                <div class="col-md-12">

                    <?php error_hander($this->input->get('e')); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php /* action_buttons('addnewcustomer', $cnt); */ ?>
                    <table id="" class="table table-bordered invoice-table mb20">
                        <thead class="thead header">
                            <tr style="">
                                <th ><?php echo " Payment id "; ?></th>
                                <th  id="no_filter"><?php echo lang('Invoice-Number') ?></th>
                                <th ><?php echo lang('invoicedate') ?></th>
                                <th><?php echo lang('Credit'); ?></th>
                                <th><?php echo lang('Debit'); ?> </th> 
                                <th><?php echo lang('remaining'); ?></th>
                                <th><?php echo lang('totalremaining'); ?></th>
                                <th><?php echo lang('balance'); ?></th>
                            </tr>
                        </thead>
                            <?php
                            //echo "<pre>";
                            //print_r($customerData);
                            $invoice_arr = array();
                            $payment_arr = array();
                            $remaining_arr = array();
                            if (!empty($customerData)) {
                                $total_paid = 0;
                                $total_inv_paid = 0;
                                $total_inv = 0;
                                $total_remainng = 0;
                                $total_balance = 0;
                                foreach ($customerData as $customer) {
                                    //$invoice_arr[$customer->invoice_id]['sales'] =$customer->sales_amount;
                                    //$invoice_arr[$customer->invoice_id]['sales'] =$customer->invoice_id; 
                                    if ($customer->payment_amount != 0) {
                                        //print_r($payment_arr);

                                        if (!in_array($customer->id, $payment_arr) && ($customer->refernce_type == 'onaccount' || $customer->refernce_type == 'opening' || $customer->refernce_type == 'advance')) {
                                            //echo "eleccc";
                                            //echo "if";
                                            ?>
                                            <tr style="font-weight: bold">
                                            	
												<td><a href="<?php echo base_url(); ?>customers/voucher/payment/<?php echo $customer->id; ?>"><?php echo $customer->id; ?></a></td>
                                                <td><?php ?></td>
                                                <td><?php if ($customer->created != NULL) echo date('d/m/Y h:m:s', strtotime($customer->created));
                            else echo $customer->invoice_date; ?></td>
                                                <td><?php echo 0; ?></td>
                                                <td><?php
                                                    //echo $customer->invoice_date; 
                                                    echo $customer->payment_amount;
                                                    ///   echo "iiinnnnn";
                                                    $total_paid+=$customer->payment_amount;
                                                    ?></td>
                                                <td><?php
                                                    if ($customer->payment_amount != 0) {
                                                        // echo $customer->reamining;

                                                        if ($total_inv > $total_paid) {

                                                            if (!in_array($customer->invoice_id, $invoice_arr)) {
                                                                $total_remainng+=$customer->sales_amount;
                                                            } else {
                                                                echo $total_remainng+=$customer->sales_amount;
                                                            }

                                                            if ($customer->payment_amount < $total_remainng)
                                                                echo $total_remainng = $total_remainng - $customer->payment_amount;
                                                        }
                                                        else {
                                                            echo $total_remainng = "0";
                                                        }
                                                        $payment_price[$customer->id] = $customer->payment_amount;
                                                        //$total_remainng+=$customer->sales_amount;
                                                    }
                                                    ?></td>
                                                <td>0</td>
                                                <td><?php
                                                    // echo $customer->balance;
                                                    //     $total_balance+=$customer->balance; 


                                                    $total_balance += $customer->payment_amount;
                                                    echo $total_balance;
                                                    ?></td>
                                            </tr>         
                                            <?php
                                            $payment_arr[] = $customer->id;
                                        }
                                    }
                                    if ($customer->sales_amount != 0) {
                                        //echo "<pre>";
                                        //print_r($payment_arr);
                                        //echo "else";
                                        ?>

                                        <tr style="font-weight: bold">
											<td><?php //echo $customer->id; ?></td>											
                                            <td><a href="<?php echo base_url(); ?>customers/voucher/invoice/<?php echo $customer->invoice_id; ?>"> <?php if (!in_array($customer->invoice_id, $invoice_arr)) echo $customer->invoice_id; ?></a></td>
                                            <td><?php if ($customer->created != NULL) echo date('d/m/Y h:m:s', strtotime($customer->created));
                                        else echo $customer->invoice_date; ?></td>
                                            <td><?php if (!in_array($customer->invoice_id, $invoice_arr)) {
                                        echo $customer->sales_amount;
                                        $total_inv+=$customer->sales_amount;
                                    }
                                        ?></td>
                                            <td><?php
                                                //echo $customer->invoice_date; 
                                                 if($customer->invoice_type == 'reverse'){
												 		?>("Return Account")<?php
												 }
												 else{
													 ?>
                                                      (<?php echo $customer->refernce_type; ?>)
                                                     <?php
												  }
												 
                                                $total_inv_paid+=$customer->invoicepayment;
                                                ?> (<?php echo $customer->refernce_type; ?>)
                                                <?php
                                                if ($customer->refernce_type != 'onaccount') {
                                                    //  echo "test";
                                                    $total_paid+=$customer->invoicepayment;
                                                } else {
                                                    //$total_inv_paid+=$customer->invoicepayment;
                                                }
                                                ?>
                                            </td>
                                            <td><?php
                                                if (isset($invoice_price[$customer->invoice_id])) {
                                                    $v_payment = $invoice_price[$customer->invoice_id] + $customer->invoicepayment;
                                                } else {
                                                    $v_payment = $customer->invoicepayment;
                                                }

                                                echo $remaining = $customer->sales_amount - $v_payment;
                                                $remaining_arr[$customer->invoice_id] = $remaining;

                                                //$customer->invoicepayment; 
                                                ?></td>
                                            <td><?php echo array_sum($remaining_arr); ?></td>
                                            <td><?php
                                                //echo $payment_price[$customer->id]."<br/>";
                                                //     $total_balance+=$customer->balance; 
                                                if (isset($payment_price[$customer->id])) {
                                                    $payment_price[$customer->id] = $payment_price[$customer->id] - $customer->invoicepayment;

                                                    if ($total_balance > 0) {
                                                        $total_balance = $total_balance - $customer->invoicepayment;
                                                    }
                                                } {
                                                    
                                                }

                                                echo $total_balance;
                                                ?></td>
                                        </tr>

                                        <?php
                                        $invoice_arr[] = $customer->invoice_id;
                                        if (isset($invoice_price[$customer->invoice_id]))
                                            $invoice_price[$customer->invoice_id] = $invoice_price[$customer->invoice_id] + $customer->invoicepayment;
                                        else
                                            $invoice_price[$customer->invoice_id] = $customer->invoicepayment;
                                    }


                                    if (isset($payment_price[$customer->id]))
                                        $payment_price[$customer->id] = $payment_price[$customer->id] + $customer->payment_amount;
                                    else
                                        $payment_price[$customer->id] = $customer->payment_amount;
                                }
                            }
                            ?>
                            <tr  style="font-weight: bold">

                                <td colspan="2">Total</td>
                                <td><?php echo $total_inv;
                            //$total_paid+=$customer->paidamount; 
                            ?></td>
                                <td><?php echo $total_paid; //$customerbalalnce->pamount;
                            //$total_inv+=$customer->invoicepayment; 
                            ?></td>
                                <td><?php echo array_sum($remaining_arr); //if($total_inv>$total_paid) echo $total_remainng = $total_inv-$total_paid; //echo $customerbalalnce->totalremaining;
                            //$total_remainng+=$customer->remaining; 
                            ?></td>
                                <td><?php echo array_sum($remaining_arr); ?></td>                 
                                <td><?php echo $total_balance; //if ($customerbalalnce->totalremaining < 0) echo $customerbalalnce->totalremaining;  ?></td>

                            </tr>

                        

                    </table>
                </div>
            </div>
            <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>

<!--<input type="submit" class="send_icon" value=""/>-->
        </form>
    </div>



</div>    