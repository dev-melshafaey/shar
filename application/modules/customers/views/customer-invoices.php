


            <div class="table-wrapper users-table">
                <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">
                    <div class="row head">
                        <div class="col-md-12">
                            <h4>

                                <div class="title"> <span><?php echo lang('main') ?><?php //breadcramb();    ?></span> </div>


                            </h4>
                            <?php error_hander($this->input->get('e')); ?>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-12">

                            <table id="new_data_table" style="text-align: center">
                                <thead>
                                    <tr>
                                         <th id="no_filter"><label for="checkbox"></label></th>
                                         <th ><?php echo lang('Invoice-Number') ?></th>
                                         <th ><?php echo lang('offer-Number') ?></th>
                                         <th style="width:250px"><?php echo lang('Customer') ?></th>
                                         <th ><?php echo lang('Total-Price') ?></th>
                                         <th ><?php echo lang('discountamount') ?></th>
                                         <th ><?php echo lang('typepay') ?></th>
                                         <th ><?php echo lang('amountpay') ?></th>
                                         <th ><?php echo lang('remain') ?></th>
                                         <th ><?php echo lang('NetPrice') ?></th>
                                         <!--<th ><?php echo lang('saleprice') ?></th>-->
                                         <th ><?php echo lang('expences') ?></th>
                                         <!--<th ><?php echo lang('Statusinvoice') ?></th>-->
                                         <th ><?php echo lang('salesperson') ?></th>
                                         <th ><?php echo lang('salesperson_percent') ?></th>
                                         <th ><?php echo lang('Date') ?></th>
                                         <th id="no_filter" width="5%">&nbsp;</th>
                                    </tr>
                                </thead>
                                
                                <?php
                                $cnt = 0;
                                //print_r($sale_invoices[0]->fullname);
                                foreach ($sale_invoices as $sdata) {
                                    $cnt++;
                                    ?>
                                    <tr>
                                        <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $sdata->invoice_id ?>" value="<?php echo $sdata->invoice_id; ?>" /></td>
                                        <td>#<?php echo $sdata->invoice_id; ?></td>
                                        <td></td>
                                        <td style="width:250px"><a href="<?php echo base_url(); ?>customers/statics/<?php echo $sdata->userid; ?>"><?php echo $sdata->fullname; ?></a></td>
                                        <td>
                                            <?php echo $sdata->iiprice?>
                                            <?php if($sdata->discount_type=='1'):?>
                                                 <?php //echo $sdata->iiprice-(($sdata->iiprice*$sdata->iidiscount)/100)?>
                                            <?php else:?>
                                                  <?php //echo $sdata->iiprice-$sdata->iidiscount; ?>
                                            <?php endif;?>
                                        </td>
                                        <td>
                                            <?php if($sdata->discount_type=='1'):?>
                                             <?php echo $sdata->iidiscount; ?>%
                                            <?php else:?>
                                             <?php echo $sdata->iidiscount; ?> RO
                                             <?php endif;?>
                                        
                                        </td>
                                        
                                        <td><?php //echo $sdata->pti; ?></td>
                                        
                                        
                                        <td class="green"><?php echo $sdata->pamount; ?></td>
                                        
                                        <td class="red"><?php echo $sdata->remaining-$sdata->iidiscount; ?></td>
                                        
                                        <td>
                                            <?php if($sdata->discount_type=='2'):?>
                                                 <?php echo ((($sdata->iiprice)-($sdata->iiprice_purchase))-$sdata->salescommession)-$sdata->iidiscount; ?>
                                            <?php else:?>
                                             
                                             <?php echo ((($sdata->iiprice)-($sdata->iiprice_purchase))-$sdata->salescommession)-(($sdata->iiprice*$sdata->iidiscount)/100); ?>
                                             <?php endif;?>
                                        </td>
                                        <!--<td><?php echo $sdata->iiprice; ?></td>-->
                                        <td></td>
                                        
                                        <td><?php $salesvalue=explode(',',$sdata->cols); ?>
                                            <?php if($salesvalue):?>
                                            <?php $count=array_unique($salesvalue)?>
                                                <?php foreach($salesvalue as $skey=>$svalue):?>
                                                    <?php if($count[$skey]):?>
                                                        <a onclike="<?php echo $count[$skey]?>" data-toggle="modal" href="#myModal"><?php echo $count[$skey]?></a><br/>
                                                    <?php endif;?>    
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </td>
                                        <td><?php echo $sdata->salescommession; ?></td>
                                        <td><?php echo $sdata->i_date; ?></td>
                                        


                                        
                                        
                                    <td>
                                        <a onClick="" data-toggle="modal" href="#myModal"><i class="icon-search"></i></a> 
                                        <a onClick="" data-toggle="modal" href="#myModal"><i class="icon-print"></i></a> 
                                        
                                        <?php //edit_button('addnewcustomer/'.$userdata->userid);   ?>
                                        <!--<a id="<?php //echo $userdata->userid;   ?>" href='#' class='basic'><img src="<?php echo base_url(); ?>images/internal/i_info.png" width="28" height="18" border="0" /></a>--> 

                                            <!-- modal content -->

                                            <div id="basic-modal-content" class="dig<?php echo $userdata->userid; ?>">
                                                <h3><?php echo $userdata->fullname; ?><br />
                                                    Code : #<?php echo $userdata->employeecode; ?></h3>

                                                <code> <span class="pop_title"><?php echo lang('Mobile-Number'); ?> </span>
                                                    <div class="pop_txt"><?php echo $userdata->phone_number; ?></div>
                                                </code> 
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Contact-Number'); ?>  </span>
                                                    <div class="pop_txt"><?php echo $userdata->office_number; ?></div>
                                                </code> 
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Fax-Number'); ?> </span>
                                                    <div class="pop_txt"><?php echo $userdata->fax_number; ?></div>
                                                </code>
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Email-Address'); ?> </span>
                                                    <div class="pop_txt"><?php echo $userdata->email_address; ?></div>
                                                </code> 
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Address'); ?></span>
                                                    <div class="pop_txt"><?php echo $userdata->address; ?></div>
                                                </code> 
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Type'); ?></span>
                                                    <div class="pop_txt"><?php echo $userdata->permissionhead; ?></div>
                                                </code> 
                                                <code> <span class="pop_title"><?php echo lang('Responsable-Name'); ?></span>
                                                    <div class="pop_txt"><?php echo($userdata->responsable_name); ?></div>
                                                </code>
                                                <code> <span class="pop_title"><?php echo lang('Responsable-Phone'); ?></span>
                                                    <div class="pop_txt"><?php echo($userdata->responsable_phone); ?></div>
                                                </code>
                                                <code> <span class="pop_title"><?php echo lang('Notes'); ?></span>
                                                    <div class="pop_txt"><?php echo($userdata->notes); ?></div>
                                                </code>
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Status'); ?></span>
                                                    <div class="pop_txt"><?php echo($userdata->status); ?></div>
                                                </code> 
                    <?php } ?>
                            </table>
                        </div>
                    </div>
                    <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>

<?php //action_buttons('addnewcustomer',$cnt);   ?>
 <!--<input type="submit" class="send_icon" value=""/>-->
                </form>
            </div>
          