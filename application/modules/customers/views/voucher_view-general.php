<?php
$am = $invoicedetails->payment_amount;

//exit;
//<!onload="print()"
?>
<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Voucher</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/receipt.css">
</head>

<body >
<div class="wrapper" >
    <table>
        <thead>
        <tr>
            <td colspan="3">
                <table>
                    <tr>
                        <td class="tdata mkcenter"><?php $c = unserialize($companydetails->company_name); echo $c['en']; ?><br>
                            Fax : <?php echo $companydetails->company_fax; ?><br>
                            MOB : <?php echo $companydetails->phone_number; ?></td>
                        <td class="tdata mkcenter"><img src="<?php echo base_url() ?>uploads/company_logo/<?php echo $companydetails->logo_name; ?>" style="width: 100%;"></td>
                        <td class="tdata mkcenter"><?php echo $c['ar']; ?><br>
 <?php echo $companydetails->company_fax;?> فاكس : <br>هاتف : <?php echo $companydetails->phone_number; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td colspan="3">
                <table>
                    <tr>
                        <td class="tdata">No. <?php echo $invoicedetails->id; ?></td>
                        <td class="tdata mkcenter"><h3>RECEIPT VOUCHER</h3></td>
                        <td class="tdata" ><input type="text" class="roundbox" style="width:46%; float:right;margin-left:5px;" value="<?php echo $am; ?> OMR">  </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>Date <input type="text" value="<?php echo date('d-m-Y',strtotime($invoicedetails->created)); ?>" class="dotted"></td>
            <td></td>
            <td><input type="text" value="<?php echo date('d-m-Y',strtotime($invoicedetails->created)); ?>" class="dotted"> التاريخ</td>
        </tr>
        <tr>
            <td colspan="3">Received From Mr/Mrs. <input type="text" value="<?php echo strtoupper($invoicedetails->fullname); ?>" class="dotted" style="width:55%;text-align: center;">استلمنا من الفاضل \ الفاضلة</td>
        </tr>
        <tr style="display: none;">
            <td colspan="3">Amount<input type="text" value="<?php echo $am; ?> OMR" class="dotted" style="width:81%;text-align: center;"> مبلغ وقدره</td>
        </tr>
        <tr>
            <td colspan="3">In Words <input type="text" value="<?php echo amount_word_logic($am);?>" class="dotted" style="width:39%"><input type="text" value="<?php echo amount_words_logic_ar($am); ?>" class="dotted" style="width:39%">مبلغ وقدره</td>
        </tr>
        <tr>
            <td colspan="3">Details <input type="text" id="details_id" name="details_id"  value="<?php echo $invoicedetails->payment_details; ?>" onkeyup="update_invoice_details()" class="dotted" style="width:83%"> ملاحضات</td>
        </tr>
        <tr>
            <td colspan="3"><input type="text" class="dotted" style="width:100%"></td>
        </tr>
        <tr>
            <td colspan="3">
                <table><tr>
                        <td class="tdata mkcenter tpad"><span style="float:left">Manager</span><span style="float:right">Manager</span><input type="text" class="dotted" style="width:100%"></td>
                        <td class="tdata mkcenter tpad"><span style="float:left">Accountant</span><span style="float:right">Accountant</span><input type="text" class="dotted" style="width:100%"></td>
                        <td class="tdata mkcenter tpad"><span style="float:left">Receiver By</span><span style="float:right">Receiver By</span><input type="text" class="dotted" style="width:100%"></td>
                    </tr></table>
            </td>
        </tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <input type="hidden" id="paymentid" name="paymentid" value="<?php echo $invoicedetails->id; ?>">
        </tbody>
    </table>
</div>
</body>
</html>
<script src="<?php echo base_url(); ?>durarthem/ltr/js/jquery.js"></script>
<script type="text/javascript">
    function update_invoice_details(){
        arData = $("#details_id").val();
        arnewData = $.parseHTML(arData);
        //alert(arnewData);
        invId = $("#paymentid").val();
        $.ajax({
            url:"<?php echo base_url(); ?>ajax/change_print_option",
            type: 'post',

            data: {invId:invId,tdData:arData},
            cache: false,
            //dataType:"json",
            success: function (data)
            {
                //alert(data);
            }
        });

    }

</script>