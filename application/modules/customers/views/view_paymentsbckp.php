<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>
<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <?php
	 		if(isset($invoice_data) && !empty($invoice_data) && isset($invoice_data[0])){
						$total_payments = $invoice_data[0]->payments;
					}
					else{
						$total_payments = 0;
					}
	  ?>
      <div id="main-content" class="main_content">
      <div class="title"> <span><?php breadcramb(); echo "Total Paid ".$total_payments; ?></span> </div>
      <?php error_hander($this->input->get('e')); ?>
      <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">
        <div class="form">
          <div class="CSSTableGenerator " id="printdiv" >
<div style="float: left; margin-left: 12px; margin-top: 4px;">
                From Date 
                <input type="text" id="from_date_filter" value=""> 
                To Date 
                <input type="text" id="to_date_filter" value="" >
                <input type="button" onclick="updateTransactionsDataTableDiv();" value="Search">
            </div>
                <table width="100%" align="left" id="usertable">
                	<thead>
                    <tr>
                      <th width="1%" id="no_filter"><label for="checkbox"></label></th>
                      <th width="3%">#</th>
                      <th width="3%">Job Id.</th>
                        <th width="16%" >invoice_id</th>
                        <th width="11%" >Total Invoice</th>
                        <th width="11%" >Paid Amount</th>
                        <th width="3%">Total Remaining</th>
                        <th width="13%">Notes</th>
                        <th width="7%" id="no_filter">&nbsp;</th>
                    </tr>
                    </thead>
              		<tfoot>
                    <tr>
                      <td style="background-color:#afe2ee"></td>
                      <td  style="background-color:#afe2ee">
                      <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                      <td  style="background-color:#afe2ee">
                      <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td style="background-color:#afe2ee" >
                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td  style="background-color:#afe2ee">
                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td  style="background-color:#afe2ee">
                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td  style="background-color:#afe2ee">
                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td  style="background-color:#afe2ee">
                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td  style="background-color:#afe2ee"></td>
                 
                       <td  style="background-color:#afe2ee">&nbsp;</td>
                  </tr>
                  </tfoot>
                    
                    <?php
					$total_debit =0;
					$total_credit =0;
					$total = 0;
					$counter = 1;
					$remaining = 0;
					$total_payments;
					if(!empty($invoice_data)){
						foreach($invoice_data as $eachTrans){

					?>

                    <tr>

                      <td><input type="checkbox" name="checkbox2" id="checkbox2" /></td>

                      <td><?php echo $counter;?></td>

                      <td><?php echo $eachTrans->id;?></td>

                        <td><?php echo $eachTrans->invoice_id;?></td>

                        <td><?php echo number_format($eachTrans->charge_amount, 2, '.', ',');?></td>

                        <td><?php if($total_payments>=$eachTrans->charge_amount){  $total_payments = $total_payments-$eachTrans->charge_amount; $rem= $eachTrans->charge_amount-$eachTrans->charge_amount; echo  $eachTrans->charge_amount; } else{ echo $total_payments; $total_payments = $eachTrans->charge_amount-$total_payments;}?></td>
						
                          <td><?php echo $total_payments; //if($total_payments>=$eachTrans->charge_amount){ $rem =$eachTrans->charge_amount-$eachTrans->charge_amount; $remaining+= $rem;  }else{ echo $rem = $eachTrans->charge_amount-$total_payments; $remaining+= $rem; }   ?></td>

                        <td><?php //if($eachTrans->transaction_type == 'credit') { echo number_format($eachTrans->credit, 2, '.', ','); $total_credit = $total_credit+$eachTrans->credit; } ?></td>

                        <td><?php   //  if($eachTrans->transaction_type == 'debit') { echo $total = $total+$eachTrans->value; } else { echo $total = $total-$eachTrans->value; } ?></td>

                        <td><?php //echo substr($eachTrans->notes,0,10).'...';?></td>

                        

                        

                  

                      <td>  


                            

                  <!-- modal content -->

                  
                    </div></td>

                    </tr>

                    <?php

					}
					}
					?>       
                </table>
            </div>
        </div>
        <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>
       
        <?php //action_buttons('addnewcustomer',$cnt); ?>
        <!--<input type="submit" class="send_icon" value=""/>-->
      </form>
    </div>
      <!-- END PAGE -->  
   </div>
</section>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(function() {
$( "#from_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to_date_filter" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from_date_filter" ).datepicker( "option", "maxDate", selectedDate );
}
});
});
</script>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
