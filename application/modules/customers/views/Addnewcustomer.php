<form id="newitemform" class="sample-form" action="" method="post">
    <div class="g8">
        <label class=""><?php echo lang('Company-Name') ?></label>
        <div class="">
            <div class="ui-select" >
                <div class="">
                    <?php company_dropbox('newcomid', $hd->comid); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="g8">
        <label class=""><?php echo lang('Branch-Name') ?></label>
        <div class="">
            <div class="ui-select" >
                <div class="">

                    <?php //company_branch_dropbox('branchid', $user->branchid, $user->companyid); ?>
                    <?php company_branch_dropbox('newbranchid', $user->branchid, $user->companyid); ?>
                    <span class="arrow arrowselectbox">&amp;</span>
                </div>
            </div>
        </div>



    </div>   

    <br clear="all">
    <div class="g8 form-group">
        <label class=""><?php echo lang('Customer-Name') ?></label>
        <input name="newfullname" id="newfullname" value="" type="text"  class="form-control"/>
    </div>
    <div class="g8 form-group">
        <label class=""><?php echo lang('Mobile-Number') ?></label>
        <input name="newphone_number" id="newphone_number" value="" type="text"  class="form-control"/>
    </div>
    <br clear="all">        
    <div class="g8 form-group">
        <label class=""><?php echo lang('Opening-Balance') ?></label>

        <input name="opening_balance" id="opening_balance" value="<?php //echo isset($customer->credit_limit)?$customer->credit_limit:'';   ?>" type="text"  class="form-control"/>

    </div>

    <div class="g8 form-group">
        <label class=""><?php echo lang('Credit-Amount-Limit') ?></label>

        <input name="credit_limit" id="credit_limit" value="<?php echo isset($user->credit_limit) ? $user->credit_limit : ''; ?>" type="text"  class="form-control"/>

    </div>

    <br clear="all">
    <div class="g16 form-group">
        <label class=""><?php echo lang('due_charges') ?></label>

        <input name="due_charges" id="due_charges" value="<?php //echo isset($customer->credit_limit)?$customer->credit_limit:'';   ?>" type="text"  class="form-control"/>

    </div>
    <input name="" type="button" class="submit_btn btn-glow green" value="<?php echo lang('Add') ?>" onclick="save_newcustomer()"  />
</form>