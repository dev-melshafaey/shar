
<div class="table-wrapper users-table">
  <form action="<?php //echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">

    <?php error_hander($this->input->get('e')); ?>
    <div class="">
      <div class="">
        <?php /*action_buttons('addnewcustomer', $cnt);*/ ?>
          <input name="sub_mit" id="sub_mit" type="button" onclick="printDiv('new_data_table')" class="btn-flat primary green flt-r g3" value="Print" />
          <input name="btnExport" id="btnExport" type="button"   class="btn-flat primary green flt-r g3" value="Export Excel" onClick ="$('#new_data_table').tableExport({type:'excel',escape:'false'});"/>
          <input  name="btnExport" id="btnExport" type="button" class="btn-flat primary green flt-r g3" value="Export Pdf" onClick ="$('#new_data_table').tableExport({type:'pdf',escape:'false'});"/>

           <div class="g4"><label class="text-warning">نواختر :</label><br>
               <div class="ui-select" style="width:100%">
                   <select class="" name="records_type"  id="records_type" >
                       <option value="" selected="selected">اختر </option>
                       <option <?php if($tp == '0'){ ?> selected <?php } ?> value="0">المتبقية</option>
                       <option <?php if($tp == ''){ ?> selected <?php } ?>  value="">كل</option>
                   </select>
               </div>
           </div>
          <table id="new_data_table">
          <thead>
            <tr>
              <th  id="no_filter"></th>
              <th ><?php echo lang('Customer-Name') ?></th>
              <th ><?php echo lang('Number-of-Invoices') ?></th>
              <th  ><?php echo lang('Mobile-Number') ?></th>
              <th ><?php echo lang('Debit') ?></th>
              <th ><?php echo lang('Credit') ?></th>
              <!--<th ><?php echo lang('time') ?></th>-->
              <!--<th ><?php echo lang('Total') ?></th>-->
              <!--<th ><?php echo lang('Status') ?></th>-->
              <th  id="no_filter">&nbsp;</th>
            </tr>
          </thead>
          <?php
            $cnt = 0;
            foreach ($this->customers->users_list('',$tp) as $userdata) {
                $cnt++;
                ?>
          <tr>
            <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $userdata->userid; ?>" value="<?php echo $userdata->userid; ?>" /></td>
            <td ><a href="<?php echo base_url(); ?>customers/statics/<?php echo $userdata->userid; ?>"><?php echo $userdata->fullname; ?></a></td>
            <td><a href="<?php echo base_url(); ?>customers/invoices/<?php echo $userdata->userid; ?>"><?php if($userdata->totalinvoice !="") echo $userdata->totalinvoice; else echo 0; ?></a></td>
            <td><?php echo $userdata->phone_number; ?></td>   
            
            <td class="red"><?php echo abs($userdata->totalremaining)?></td>
            <td class="green"><?php echo $userdata->amount ?></td>
            
            <!--<td ><?php echo $userdata->days_limit?></td>-->
            <!--<td><?php echo ($userdata->Total == '') ? '00:00' : $userdata->Total; ?></td>-->
            <!--<td><?php echo($userdata->status=='Active')? 'مفعل': 'غير مفعل'; ?></td>-->
            <td><?php edit_button('addnewcustomer/' . $userdata->userid); ?>            
            <a onClick="show_dialog2('#demo-modal', '<?php echo base_url(); ?>customers/getCustomerData_ajax/<?php echo $userdata->userid?>')" href="javascript:void" data-toggle=""><i class="icon-eye-view"></i></a> 
            
              
              </td>
          </tr>
          <?php } ?>
        </table>
      </div>
    </div>
    <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>
    
    <!--<input type="submit" class="send_icon" value=""/>-->
  </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#records_type").change(function (){
            vvl = $(this).val();

            window.top.location = '<?php echo base_url() ?>customers/options/'+vvl
        });
    })
</script>
