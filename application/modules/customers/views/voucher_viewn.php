<?php
$am = $invoicedetails->payment_amount;

//exit;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Voucher</title>
    <script type="text/javascript">
        window.print();
        function print_page(){
            alert('asdasd');
        }
    </script>
</head>
<body onload="print_page()">
<table width="960" border="0" cellspacing="0" cellpadding="0"  dir="rtl" align="center">
    <tr>
        <td height="187" valign="top" style="font:Arial, Helvetica, sans-serif; font-size: 16px; font-weight:bold;"><table width="100%" border="0" cellspacing="0" cellpadding="0" dir="rtl">
                <tr>
                    <td width="35%" align="right" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="22"> سلطنة عُمــان</td>
                            </tr>
                            <tr>
                                <td height="22">س . ت : <?php echo $companydetails->company_pobox; ?></td>
                            </tr>
                            <tr>
                                <td height="22">هاتف : <?php echo $companydetails->phone_number; ?></td>
                            </tr>
                            <tr>
                                <td height="22">فاكس : <?php echo $companydetails->company_fax; ?> </td>
                            </tr>
                            <tr>
                                <td height="22">ص.ب : <?php echo $companydetails->company_pcode; ?>  </td>
                            </tr>
                            <tr>
                                <td height="22">الرمز البريدي : <?php echo $companydetails->company_email; ?>  </td>
                            </tr>
                        </table>  </td>
                    <td width="30%" align="center" valign="middle"><img src="<?php echo base_url() ?>uploads/company_logo/<?php echo $companydetails->logo_name; ?>" width="100%" /></td>
                    <td width="35%"  align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="22" align="left" valign="top">Sultanate of Oman</td>
                            </tr>
                            <tr>
                                <td height="22" align="left" valign="top">C.R : <?php ?></td>
                            </tr>
                            <tr>
                                <td height="22" align="left" valign="top">Tel : <?php echo $companydetails->phone_number; ?></td>
                            </tr>
                            <tr>
                                <td height="22" align="left">Fax : <?php echo $companydetails->company_fax; ?></td>
                            </tr>
                            <tr>
                                <td height="22" align="left">P.O.Box : <?php echo $companydetails->company_pobox; ?></td>
                            </tr>
                            <tr>
                                <td height="22" align="left">P.C : <?php echo $companydetails->company_pcode; ?></td>
                            </tr>
                        </table></td>
                </tr>
            </table>
        </td>

    </tr>
    <tr>
        <td height="800" valign="top" style="font-family:Arial, Helvetica, sans-serif; color:#000;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="80" valign="top"><hr /></td>
                </tr>
                <tr>
                    <td height="50" align="center" valign="middle" style="font-size:25"><strong><u>       <h3>سند صرف <?php if($type  == 'invoice') echo "Invoice Voucher"; else echo "Payment Voucher"; ?></u></strong></td>
                </tr>
                <tr>
                    <td height="80">&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="21%" height="40" align="left"><?php if($type  == 'invoice') { echo "inv#".$invoicedetails->invoice_id;}else{ echo $invoicedetails->id; } ?></td>
                                <td width="10%" align="left" dir="ltr">Voucher No : </td>
                                <td width="69%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="40" align="left" dir="ltr"><?php echo date('d-m-Y',strtotime($invoicedetails->created)); ?></td>
                                <td align="left">                Date</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="82%">&nbsp;</td>
                                <td width="18%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" valign="middle"><?=strtoupper($invoicedetails->fullname)?></td>
                                <td height="40" align="left" valign="middle">Paid By</td>
                            </tr>
                            <tr>
                                <td align="left" valign="middle" dir="ltr"><strong> <?php echo $am; //number_format($_val[0]) . ' ' . $_val[1]?> RO </strong></td>
                                <td height="40" align="left" valign="middle">Amount</td>
                            </tr>
                            <tr>
                                <td align="left" valign="middle"><?php echo amount_word_logic($am); ?></td>
                                <td height="40" align="left" valign="middle">Amount in Words</td>
                            </tr>

                        </table></td>
                </tr>
            </table></td>

    </tr>
    <tr>
        <td height="44" valign="top" style="font-family:Arial, Helvetica, sans-serif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20"><hr /></td>
                </tr>
                <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="6%" align="left" valign="top">&nbsp;</td>
                                <td width="20%" height="30" align="left" valign="top">Recieved By</td>
                                <td width="3%" height="30" align="left" valign="top">&nbsp;</td>
                                <td width="19%" height="30" align="left" valign="top">Approved By</td>
                                <td width="4%" height="30" align="left" valign="top">&nbsp;</td>
                                <td width="17%" height="30" align="left" valign="top">Verified By</td>
                                <td width="5%" height="30" align="left" valign="top">&nbsp;</td>
                                <td width="18%" height="30" align="left" valign="top">Prepared By </td>
                                <td width="8%" align="left" valign="top">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">&nbsp;</td>
                                <td height="30" align="left" valign="top">&nbsp;</td>
                                <td height="30" align="left" valign="top">&nbsp;</td>
                                <td height="30" align="left" valign="top">&nbsp;</td>
                                <td height="30" align="left" valign="top">&nbsp;</td>
                                <td height="30" align="left" valign="top">&nbsp;</td>
                                <td height="30" align="left" valign="top">&nbsp;</td>
                                <td height="30" align="left" valign="top">&nbsp;</td>
                                <td align="left" valign="top">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="10" align="left" valign="top">&nbsp;</td>
                                <td height="10" align="left" valign="top">&nbsp;</td>
                                <td height="10" align="left" valign="top">&nbsp;</td>
                                <td height="10" align="left" valign="top">&nbsp;</td>
                                <td height="10" align="left" valign="top">&nbsp;</td>
                                <td height="10" align="left" valign="top">&nbsp;</td>
                                <td height="10" align="left" valign="top">&nbsp;</td>
                                <td height="10" align="left" valign="top">&nbsp;</td>
                                <td height="10" align="left" valign="top">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
