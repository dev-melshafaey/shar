
<div id="main-content" class="main_content">
    <div class="title title alert alert-info">
        <span><?php breadcramb(); ?> >>  <?php echo lang('View-All-Debit-Credit') ?></span>
    </div>

    <div class="inp-btns pad-l g16 title">
        <?php ?>
        <input name="sub_mit" id="sub_mit" type="button" onclick="printDiv('tb1')" class="btn-flat primary green flt-r g3" value="Print" />
        <!--<div class="col-md-4 form-group"  style="padding:1px;"><label class="col-md-2"><?php echo lang('Customer-Name'); ?>: </label><?php echo $customerdetails->fullname; ?></div>-->
        <table id="tb1" class="table table-bordered invoice-table mb20" style="text-align:center;">
            <thead class="thead header">
                <tr>   
                    <td><?php echo lang('Customer-Name'); ?></td>
                    <td><?php echo lang('totalCredit'); ?></td>
                    <td><?php echo lang('totalDebit'); ?></td> 
                    <td><?php echo lang('totalremaining'); ?></td>
                    <td><?php echo lang('totalbalance'); ?></td>
                </tr>
            </thead>
            <?php
            //echo "<pre>";
            //print_r($customerdetails);
            if (!empty($customerdetails)) {
                foreach ($customerdetails as $customer) {
                    ?>
                    <tr>
                        <td><a href="<?php echo base_url() ?>customers/statics/<?php echo $customer->userid; ?>"><?php echo $customer->fullname; ?></a></td>
                        <td><?php echo $customer->totalsales; ?></td>
                        <td><?php echo $customer->totalpayment; ?></td>
                        <td><?php echo $customer->remainng; ?></td>
                        <td><?php echo $customer->balance; ?></td>
                    </tr>         
                    <?php
                }
            }
            ?>


        </table>	
    </div>

    <div class="table-wrapper users-table">
        <form action="<?php //echo form_action_url('delete_customers');      ?>" id="listing" method="post" autocomplete="off">
            <div class="row head">
                <div class="col-md-12">

                    <?php error_hander($this->input->get('e')); ?>
                </div>
            </div>
            <div class="row">
                <input name="sub_mit" id="sub_mit" type="button" onclick="printDiv('tb2')" class="btn-flat primary green flt-r g3" value="Print" />
                <div class="col-md-12">
                    <?php /* action_buttons('addnewcustomer', $cnt); */ ?>
                    <table id="tb2" class="table table-bordered invoice-table mb20">
                        <thead class="thead header">
                            <tr style="">
                                <th ><?php echo lang('invoicedate') ?></th>
                                <th ><?php echo lang('Customer-Name'); ?></th>
                                <th><?php echo "Credit"; //echo lang('Credit');    ?></th>
                                <th><?php echo "Debit"; //echo lang('Debit');    ?> </th> 
                                <th><?php echo "balance"; //echo lang('balance');    ?></th>
                            </tr>
                        </thead>
                        <?php
//echo "<pre>";
//print_r($customerData);
                        $invoice_arr = array();
                        $payment_arr = array();
                        $remaining_arr = array();
                        if (!empty($customerData)) {
                            $total_paid = 0;
                            $total_inv_paid = 0;
                            $total_inv = 0;
                            $total_remainng = 0;
                            $total_balance = 0;
                            foreach ($customerData as $customer) {
                                //$invoice_arr[$customer->invoice_id]['sales'] =$customer->sales_amount;
                                //$invoice_arr[$customer->invoice_id]['sales'] =$customer->invoice_id; 
                                if ($customer->payment_amount != 0) {
                                    //print_r($payment_arr);

                                    if (!in_array($customer->id, $payment_arr) && ($customer->refernce_type == 'onaccount' || $customer->refernce_type == 'opening')) {
                                        //echo "eleccc";
                                        //echo "if";
                                        ?>
                                        <tr style="font-weight: bold">
                                            <td><?php
                                                if ($customer->created != NULL)
                                                    echo date('d/m/Y h:m:s', strtotime($customer->created));
                                                else
                                                    echo $customer->invoice_date;
                                                ?></td>
                                            <td><?php echo $customer->fullname; ?></td>

                                            <td><?php echo 0; ?></td>
                                            <td><?php
                                                //echo $customer->invoice_date; 
                                                echo $customer->payment_amount;
                                                ///   echo "iiinnnnn";
                                                $total_paid+=$customer->payment_amount;
                                                ?></td>
                                            <td><?php
                                                // echo $customer->balance;
                                                //     $total_balance+=$customer->balance; 


                                                $total_balance += $customer->payment_amount;
                                                echo $total_balance;
                                                ?></td>
                                            
                                        </tr>         
                                        <?php
                                        $payment_arr[] = $customer->id;
                                    }
                                }
                                if ($customer->sales_amount != 0) {
                                    //echo "<pre>";
                                    //print_r($payment_arr);
                                    //echo "else";
                                    //if (!in_array($customer->invoice_id, $invoice_arr))
                                    ?>

                                    <tr style="font-weight: bold">
                                        <td><?php
                                            if ($customer->created != NULL)
                                                echo date('d/m/Y h:m:s', strtotime($customer->created));
                                            else
                                                echo $customer->invoice_date;
                                            ?></td>
                                        <td><?php echo $customer->fullname; ?></td>

                                        <td><?php
                                            if (!in_array($customer->invoice_id, $invoice_arr)) {
                                                echo $customer->sales_amount;
                                                $total_inv+=$customer->sales_amount;
                                            }
                                            ?></td>
                                        <td><?php
                                            //echo $customer->invoice_date; 
                                            // echo $customer->invoicepayment;

                                            $total_inv_paid+=$customer->invoicepayment;

                                            //echo "<pre>";
                                            //print_r($customer);
                                            //if(!in_array($customer->id,$payment_arr)){
                                            echo $customer->invoicepayment;

                                            //}
                                            ?>
                                        </td>
                                        <td><?php
                                            if (!in_array($customer->invoice_id, $invoice_arr)) {

                                                if (!in_array($customer->id, $payment_arr)) {
                                                    //		echo $customer->invoicepayment;
                                                    if ($customer->invoicepayment != "") {
                                                        $remainingsales = $customer->sales_amount - $customer->invoicepayment;
                                                        $total_balance = $total_balance - $remainingsales;
                                                    } else {

                                                        $total_balance = $total_balance - $customer->sales_amount;
                                                    }
                                                }
                                            }

                                            if (in_array($customer->id, $payment_arr)) {
                                                //	echo "ifff";
                                                //echo $customer->invoicepayment;
                                                $total_balance = $total_balance - $customer->invoicepayment;
                                            }

                                            echo $total_balance;
                                            ?></td>
                                        
                                    </tr>





                                    <?php
                                    $invoice_arr[] = $customer->invoice_id;
                                    if (isset($invoice_price[$customer->invoice_id]))
                                        $invoice_price[$customer->invoice_id] = $invoice_price[$customer->invoice_id] + $customer->invoicepayment;
                                    else
                                        $invoice_price[$customer->invoice_id] = $customer->invoicepayment;
                                }


                                if (isset($payment_price[$customer->id]))
                                    $payment_price[$customer->id] = $payment_price[$customer->id] + $customer->payment_amount;
                                else
                                    $payment_price[$customer->id] = $customer->payment_amount;
                            }
                        }
                        ?>
                        <tr  style="font-weight: bold">

                            <td colspan="3">Total</td>
                            <td><?php
                                echo $total_inv;
                                //$total_paid+=$customer->paidamount; 
                                ?></td>
                            <td><?php echo $total_balance; //if ($customerbalalnce->totalremaining < 0) echo $customerbalalnce->totalremaining;     ?></td>

                        </tr>



                    </table>
                </div>
            </div>
            <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>

<!--<input type="submit" class="send_icon" value=""/>-->
        </form>
    </div>



</div>    