<script type="text/javascript">

    function swapme() {
        //  alert('swapme');
        pid = $("#customer_id").val();
        pname = $("#customer_name").val();
        $("#customer_name").val(pid);
        $("#customer_id").val(pname);
        //$("#customerid-val").val(pname);
    }
    function showRemaining() {
        pay_amount = $("#payment_amount").val();
        refrence_val = $(".refrence_by:checked").val();

        if (pay_amount) {

            if (refrence_val == 'against_reference') {
                rem = $("#total_remaining").val();
                pay_remianing = pay_amount - rem;
                if (parseInt(pay_amount) >= parseInt(rem)) {
                    cl_status = $("#rem_payment").hasClass('rest_money_min')
                    if (cl_status) {
                        $("#rem_payment").removeClass('rest_money_min');
                    }
                    $("#rem_payment").addClass('rest_money_plus');
                    if (pay_remianing) {
                        p_rm = pay_remianing.toFixed(2);
                        $("#rem_payment").html('+' + p_rm + ' RO');
                    }
                }
                else {
                    //alert('else');
                    cl_status = $("#rem_payment").hasClass('rest_money_plus')
                    if (cl_status) {
                        $("#rem_payment").removeClass('rest_money_plus');
                    }
                    p_rm = pay_remianing.toFixed(2);
                    $("#rem_payment").addClass('rest_money_min');
                    $("#rem_payment").html(+p_rm + ' RO');
                }
            }
            if (refrence_val == 'on_account') {

                $("#bl_div").hide();
                pay_remianing = pay_amount - charges;
                if (parseInt(pay_amount) >= parseInt(charges)) {
                    cl_status = $("#rem_payment").hasClass('rest_money_min')
                    if (cl_status) {
                        $("#rem_payment").removeClass('rest_money_min');
                    }
                    $("#rem_payment").addClass('rest_money_plus');
                    if (pay_remianing) {
                        p_rm = pay_remianing.toFixed(2);
                        $("#rem_payment").html('+' + p_rm + ' RO');
                    }
                }
                else {
                    //alert('else');
                    cl_status = $("#rem_payment").hasClass('rest_money_plus')
                    if (cl_status) {
                        $("#rem_payment").removeClass('rest_money_plus');
                    }
                    p_rm = pay_remianing.toFixed(2);
                    $("#rem_payment").addClass('rest_money_min');
                    $("#rem_payment").html(+p_rm + ' RO');
                }


            }

        }
        $("#payment_rem").show();

    }

    function showInvocices() {

        refrence_val = $(".refrence_by:checked").val();
        customer_id = $("#customer_id").val();

        if (refrence_val == 'against_reference') {
			$('#fatoora').slideUp('slow');
			$('#russom').slideUp('slow');
			$('#inv_tbl').slideUp('slow');
            $.ajax({
                url: config.BASE_URL + "ajax/calculate_pending_jobs",
                type: 'post',
                data: {customer_id: customer_id},
                cache: false,
                success: function (data) {
                    console.log(data);
                    $("#inv_tbl").html(data);
                    $("#inv_tbl").show();
                    $("#bl_div").hide();
                    $("#charges_due").hide();
                }
            });
            $("#payment_rem").hide();
        }
        else if (refrence_val == 'advance') {
			$('#fatoora').slideDown('slow');
			$('#russom').slideUp('slow');
			$('#inv_tbl').slideUp('slow');
            $("#inv_tbl").hide();
            $("#bl_div").show();
            $("#charges_due").hide();
            $("#payment_rem").hide();
        }
        else if (refrence_val == 'clear_dues') {
			$('#fatoora').slideUp('slow');
			$('#russom').slideDown('slow');	
			
            $.ajax({
                url: config.BASE_URL + "customers/getCustomeDues/",
                type: 'post',
                data: {customer_id: customer_id},
                cache: false,
                success: function (data) {
                    res = $.parseJSON(data);
                    console.log(res);
                    charges = res.sales_amount;
                    amount = res.amount;

                    if (amount != null){
                        charges = parseInt(charges) - parseInt(amount)
                    }
					
					if(charges ==null)
					charges = 0;
					

                    $("#due_charges").css('color', 'red');
                    $("#due_charges").html(charges + ' OMR');
                    $("#charges_due").show();
					$("#account_charges").hide();
					$("#due_charges").show();
                    $("#div_deduct").show();
                }
            });
        }
        else if (refrence_val == 'on_account') {
			$('#fatoora').slideUp('slow');
			$('#russom').slideUp('slow');
            $("#bl_div").hide();
            $("#inv_tbl").hide();
			$('#inv_tbl').slideUp('slow');


            $.ajax({
                url: config.BASE_URL + "customers/getCustomePayments/",
                type: 'post',
                data: {customer_id: customer_id},
                cache: false,
                success: function (data) {
                    res = $.parseJSON(data);
                    console.log(res);
                    charges = res.totalcharges;
                    if (res.totalpayment) {
                        if (res.totalcharges > 0) {
                            //alert('if');
                            if (res.totalcharges > res.totalpayment) {
                                charges = res.totalcharges - res.totalpayment;
                            }
                            else {

                                charges = res.totalpayment - res.totalcharges;
                            }
                        }
                        else {
                          //  alert('else');	
                            charges = res.totalpayment;
                        }
                    }

					
                        $("#charges_due").show();
                        $("#account_charges").show();
						$("#due_charges").hide();
                    
					if (charges) {
						//	alert('if2');
						$("#account_charges").html(charges + ' OMR');


                    }
					else{
					    $("#account_charges").html('0 OMR');
					}


                    //	$("#inv_tbl").html(data);
                    //	$("#inv_tbl").show();
                    //	$("#bl_div").hide();
                }
            });
            //$("#inv_tbl").hide();
            //$("#bl_div").hide();
        }

        else {
            //$("#inv_tbl").hide();			
        }
    }
	
	function check_deduct(){
			ddfrm = $("#deduct_from").val();	
			if(ddfrm == 'from_account'){
				//alert('asd');
					//alert(jsArr.totalcharges);
					if (jsArr.totalpayment) {
                        if (jsArr.totalcharges > 0) {
                            alert('if');
                            if (jsArr.totalcharges > jsArr.totalpayment) {
                            	alert('if2');
							    charges = jsArr.totalcharges - jsArr.totalpayment;
                            }
                            else {
								alert('else2');
                                charges = jsArr.totalpayment - jsArr.totalcharges;
                            	alert(charges);
							}
                        }
                        else {
                            alert('else');	
                            charges = jsArr.totalpayment;
                        	alert(charges);	
						}
                    }
			}
	}
	jsArr = '';
	function searchinAccount(){
		
            $.ajax({
                url: config.BASE_URL + "customers/getCustomePayments/",
                type: 'post',
                data: {customer_id: customer_id},
                cache: false,
                success: function (data) {
    	                ress = $.parseJSON(data);
        	            console.log(ress);
						jsArr = ress;			
					
                    }
            });
            //$("#inv_tbl").hide();
            //$("#bl_div").hide();
        
	}
	
	
	

    $(document).ready(function () {
		//alert('asdasd');
		
		
		searchinAccount();
		function showing_black_magic()
		{
			var refrence_val = $(".refrence_by:checked").val();
        	var customer_id = $("#customer_id").val();
			$.ajax({
                url: config.BASE_URL + "customers/getCustomePayments/",
                type: 'post',
                data: {customer_id: customer_id},
                cache: false,
                success: function (data) {
                    res = $.parseJSON(data);
                    console.log(res);
                    charges = res.totalcharges;
                    if (res.totalpayment) {
                        if (res.totalcharges > 0) {
                            //alert('if');
                            if (res.totalcharges > res.totalpayment) {
                                charges = res.totalcharges - res.totalpayment;
                            }
                            else {

                                charges = res.totalpayment - res.totalcharges;
                            }
                        }
                        else {
                            //alert('else');	
                            charges = res.totalpayment;
                        }
                    }

                    if (charges) {
                        $("#charges_due").show();
                        $("#charges_due").show();
                        $("#due_charges").html(charges + ' OMR');


                    }


                    //	$("#inv_tbl").html(data);
                    //	$("#inv_tbl").show();
                    //	$("#bl_div").hide();
                }
            });
		}
		
		
		$('#customer_name').blur(function(){
			var customer_name = $('#customer_name').val();
			var payment_amount = $('#payment_amount').val();
			if(customer_name!='')
			{
				//$('.blackmagic').fadeIn('slow');
				$('#refrence_id1').click();
				showing_black_magic();
				searchinAccount();
			}
			else
			{
				//$('.blackmagic').fadeOut('slow');
			}
		});

		$('#payment_amount').blur(function(){
			var customer_name = $('#customer_name').val();
			var payment_amount = $('#payment_amount').val();
			if(customer_name!='' && payment_amount!='')
			{
				$('.blackmagic').fadeIn('slow');
				$('#refrence_id1').click();
				showing_black_magic();
			}
			else
			{
				$('.blackmagic').fadeOut('slow');
			}
		});

        $(".refrence_by").change(function () {

            pay_amount = $(this).val();
            refrence_val = $(".refrence_by:checked").val();
            customer_id = $("#customer_id").val();
            if (refrence_val == 'against_reference') {
                $.ajax({
                    url: config.BASE_URL + "ajax/calculate_pending_jobs",
                    type: 'post',
                    data: {pay_amount: pay_amount, customer_id: customer_id},
                    cache: false,
                    success: function (data) {
                        console.log(data);
                        $("#inv_tbl").html(data);
                        $("#inv_tbl").show();
                    }
                });
            }

        });

        $("#bank_id").change(function () {
            bank_val = $(this).val();
            if (bank_val != "") {
                $("#payment-type").show();

            }
            else {
            }
        });

        $("#charges_job").change(function () {

            c_id = $(this).val();
            jobId = $("#bl_number").val();

            $.ajax({
                url: config.BASE_URL + "ajax/check_charges",
                type: 'post',
                data: {c_id: c_id, job_id: jobId},
                cache: false,
                success: function (data) {

                    //console.log(data+'data');
                    res = $.parseJSON(data);
                    console.log(res + 'res');
                    if (res.jb_c_id) {
                        $("#job_charges_id").val(res.jb_c_id);
                    }

                    if (res.is_actual == '1') {
                        //alert(res.is_actual);

                        /*if(res.charges_advance){
                         $("#advance").val(res.charges_advance);	
                         }*/

                        if (res.charges_advance) {
                            $("#advance").val(0);
                        }

                        $("#advance_payment").show();
                    }
                    else {
                        $("#advance_payment").hide();
                    }

                    if (res.total_value) {
                        $("#payment_amount").val(res.total_value);
                    }
                    $("#payment_payment").show();


                    //$("#charges_job").html(data);
                    //$("#div_charges").fadeIn();
                }
            });

            $("#submit_cash").click(function () {
                $(".jobs_way").remove();
            });

        })

        $(".payment_by").click(function () {
            transaction_val = $(this).val();
            if (transaction_val == 'bank') {
                $("#div_banks").show();
                $("#payment-type").show();
            }
            else {
                $("#div_banks").hide();
                $("#payment-type").hide();
            }

        });



        var ac_config = {
            source: "<?php echo base_url(); ?>ajax/getAutoSearchCustomer",
            select: function (event, ui) {
                $("#customer_name").val(ui.item.id);
                $("#customer_id").val(ui.item.cus);
                console.log(ui);
                //swapme();
                setTimeout('swapme()', 500);
            },
            minLength: 1
        };

        $("#customer_name").autocomplete(ac_config);


        var ac_config = {
            source: "<?php echo base_url(); ?>customers/getAutoSearchJob",
            select: function (event, ui) {
                $("#bl_number").val(ui.item.cus);
                console.log(ui);
                //swapme();
                $("#div_charges").show();

            },
            minLength: 1
        };

        $("#bl_number").autocomplete(ac_config);

        $('#cheque_date').datepicker({dateFormat: 'yy-mm-dd'});

    })


</script>
</header>

<div class="table-wrapper users-table " style="margin-top:0px" >
<div class="row head">
  <div class="col-md-12">
    <h4>
      <div class="title"> <span>
        <?php breadcramb(); ?> 
        </span> </div>
    </h4>
    <?php error_hander($this->input->get('e')); ?>
  </div>
</div>
<form id="form_cashs" action="<?php echo base_url(); ?>customers/add_payment_post" method="post" enctype="multipart/form-data">
  <div class="form" id="form-refresh"></div>
  <input type="hidden" id="job_charges_id" name="job_charges_id" />
  <div class="col-md-12 form-group mycontent">
    <div class="col-md-4 form-group">
      <label><?php echo lang('Customer-Name'); ?> :</label>
      <input name="customer_name" id="customer_name" type="text"  class="form-control"  />
      <input type="hidden" name="customer_id" id="customer_id" />
    </div>
    <div class="col-md-4 form-group">
      <label><?php echo lang('Value'); ?> :</label>
      <input name="payment_amount" id="payment_amount" type="text"  class="form-control" onchange="showRemaining()"/>
    </div>
    <div class="col-md-4 form-group">
      <div  id="charges_due" style="display:none;">
        <label><?php echo lang('Total'); ?> : </label>
        <br>
        <span id="due_charges" class="col-md-5 green"></span>
        <br>
        <span id="account_charges" class="col-md-5" style="display:none;"></span></div>
    </div>
    
    </div>
    <br clear="all">
    <!-----------Radio Buttons---------------->
    <div class="col-md-2 form-group ">
    
      <label><?php echo lang('on-Account'); ?> :
        <input type="radio" name="refrence_by" class="refrence_by" value="on_account" id="refrence_id1" onclick="showInvocices()">
      </label>
    </div>
<!--    <div class="col-md-2 form-group blackmagic" style="display:none;">
      <label><?php //echo lang('Advance'); ?> :
        <input type="radio" name="refrence_by" class="refrence_by" value="advance" id="refrence_id2" onclick="showInvocices()">
      </label>
    </div>-->
    <div class="col-md-2 form-group ">
      <label > <?php echo lang('Against-Reference'); ?> :
        <input type="radio" name="refrence_by" class="refrence_by" value="against_reference" id="reference_id3" onclick="showInvocices()">
      </label>
    </div>
    <div class="col-md-2 form-group ">
      <label><?php echo lang('clear_due_charges'); ?> :
        <input type="radio" name="refrence_by" class="refrence_by" value="clear_dues" id="reference_id3" onclick="showInvocices()">
      </label>
    </div>
    <div class="col-md-4 form-group "></div>
    <br clear="all">
    <!-----------Hidden Fields----------------> 
  </div>
  <div class="col-md-12 form-group mycontent" style="padding-top:10px; display:none;" id="fatoora">
    <div class="col-md-3 form-group" id="bl_div" style="display:none;">
      <label><?php echo lang('Invoice-Number'); ?> :</label>
      <input name="bl_number" id="bl_number" type="text"  class="form-control"/>
    </div>
    <div class="col-md-2 form-group">
      <label><?php echo lang('Payment-Method'); ?> :</label>
      <div class="defaultP">
        <input type="radio" name="payment_method" class="payment_by" value="bank" id="payment_type1">
        <label for="payment_type1"><?php echo lang('Bank'); ?></label>
        <input type="radio" name="payment_method" class="payment_method" value="cash" id="payment_type2">
        <label for="payment_type2"><?php echo lang('Cash'); ?></label>
      </div>
    </div>
    <div class="col-md-3 form-group">
      <div  id="div_banks" style="display:none;">
        <label><?php echo lang('Bank'); ?> :</label>
        <div class="styled-select" style="width:100%">
          <?php company_bank_dropbox('bank_id', '', 'english', ' onchange="getBankAccounts();" '); ?>
        </div>
      </div>
    </div>
    <div class="col-md-2 form-group">
      <div  style="display:none" id="div_accounts">
        <label><?php echo lang('Accounts') ?> :</label>
        <div class="styled-select"  style="width:100%" id="div_accounts_responce"> </div>
      </div>
      <div id="div_checkque" style="display:none;">
        <label><?php echo lang('Deposite-Recipt') ?> :</label>
        <input type="file"  class="form-control" name="deposite_recipt" id="deposite_recipt" />
      </div>
    </div>
    <div class="col-md-2 form-group">
      <div  style="display:none;" id="payment-type">
        <label><?php echo lang('Payment-Type'); ?></label>
        <div class="styled-select"  style="width:100%">
          <select name="payment_type" id="payment_type" class="form-control" onchange="BankOrCashToggle();">
            <option value=""><?php echo lang('choose'); ?></option>
            <?php echo get_payment_method() ?>
          </select>
        </div>
      </div>
    </div>
    <br clear="all">
    <div class="col-md-6 form-group" style="display:none" id="cheque_div_date">
      <label><?php echo lang('Cheque-Date') ?> </label>
      <input name="cheque_date" id="cheque_date" type="text"  class="form-control"/>
    </div>
    <div class="col-md-6 form-group" style="display:none" id="cheque_div_number">
      <label><?php echo lang('Cheque-Number') ?> </label>
      <input name="cheque_number" id="cheque_nummber" type="text"  class="form-control"/>
    </div>
    
    <!-----------Hidden Fields----------------> 
    <br clear="all">
  </div>
  <!------------------------------>
  <div class="col-md-12 form-control mycontent " id="russom" style="display:none; margin:5px 0px;">
    <div class="col-md-4 form-group" id="div_deduct" style="display:none;">
      <label><?php echo lang('from'); ?> :</label>
      <div class="styled-select">
        <select id="deduct_from" name="deduct_from" onchange="check_deduct()">
          <option value="">Select</option>
          <option value="from_account"><?php echo lang('on-Account'); ?></option>
          <option value="from_direct">Direct</option>
        </select>
      </div>
    </div>
    <div class="col-md-4 form-group" id="payment_rem" style="display:none;">
      <label><?php echo lang('remaining'); ?> :</label>
      <div  id="rem_payment" class="col-md-5 red" ></div>
    </div>
    <br clear="all"/>
    <div class="col-md-4 form-group" style="display:none" id="div_tr_accounts">
      <label><?php echo lang('Accounts') ?> :</label>
      <div class="styled-select" id="div_tr_accounts_responce"> </div>
    </div>
    <br clear="all">
  </div>
  <!------------------------------->
  <div id="inv_tbl" class="mycontent "  style="display:none; padding: 10px; margin:10px 0px;"> </div>
  <!------------------------------>
  <div class="col-md-12 form-group mycontent "  style="padding: 10px;">
    <label><?php echo lang('Notes') ?> :</label>
    <textarea name="notes" id="notes" cols="" rows=""  style="resize:none;" class="form-control"></textarea>
  </div>
  <div class="col-md-12 form-group mycontent "  style="padding: 10px;">
    <input name="submit_cash" id="submit_cash" type="submit" class="btn-flat primary" value="<?php echo lang('Add'); ?>" />
  </div>
  <!------------------------------>
  
  </div>
</form>
<!-- END PAGE -->
</div>
