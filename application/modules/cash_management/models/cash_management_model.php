<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cash_management_model extends CI_Model {
	
	/*
	* Properties
	*/
//----------------------------------------------------------------------
    
	/*
	* Constructor
	*/
	
	function __construct()
    {
        parent::__construct();
		
		//Load Table Names from Config
    }
	
	function getBranchCashId($branch_id=''){
		$sql = "SELECT id from an_branch_cash where branch_id = ".$branch_id;	
		$q = $this->db->query($sql);
		$res = $q->row();	
		return $res->id;
	}
	function getAllcashBraches(){
			$sql = "SELECT * FROM `an_branch_cash` AS bc";
			$q = $this->db->query($sql);
	if($q->num_rows() > 0){
				return 	$res = $q->result();	
		}
		else{
			return false;
		}
	}
	
	function loadamount($branch_id=''){
		if($branch_id!=""){
		$sql = "SELECT 
				  act.`value`,
				  act.`transaction_type` 
				FROM
				  `an_cash_management` AS act 
				WHERE act.`branch_cash_id` = ".$branch_id." 
				ORDER BY act.`branch_cash_id` ASC 
				LIMIT 1  ";	
		$q = $this->db->query($sql);
		return $q->result();	
		}
		else{
			return false;
		}
	}
	function insertNewcash($data='',$emp_id='')
	{
		if($data['branch_id']){
			$data['branch_cash_id'] = $this->getBranchCashId($data['branch_id']);
		}
		if($emp_id==''){
		    $data['emp_id'] = $this->session->userdata('userid');
		}
		$this->db->insert('an_cash_management',$data);
		return $this->db->insert_id();
	}
	function LoadCashsWithFilter($branch_id='',$from='',$to='')
	{
		$sql = "SELECT 
				  t.id,
				  t.payment_type,
				  t.value,
				  t.created,
				  c.`customer_name`,
				  t.transaction_type,
				  IF(
					t.transaction_type = 'debit',
					t.value,
					'0'
				  ) AS 'debit',
				  IF(
					t.transaction_type = 'credit',
					t.value,
					'0'
				  ) AS 'credit',
				  t.value,
				  t.notes 
				FROM
				  `an_cash_management` AS t
				 left join  an_customers AS c 
				 
				on c.`id` = t.`customer_id` ";
				
				
		if($branch_id!=''){
			$sql .= "  where t.`branch_id` = '".$branch_id."'  ";
			$sql .= " and t.created  BETWEEN '".$from."' AND '".$to."'  ";
		}else{
			$sql .= " WHERE t.created  BETWEEN '".$from."' AND '".$to."'  ";
		}
		
		$sql .= " order by t.id asc ";
		
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function LoadCashs($branch_id='')
	{
		$sql = "SELECT 
				  t.id,
				  t.payment_type,
				  t.value,
				  t.created,
				  c.`customer_name`,
				  t.transaction_type,
				  IF(
					t.transaction_type = 'debit',
					t.value,
					'0'
				  ) AS 'debit',
				  IF(
					t.transaction_type = 'credit',
					t.value,
					'0'
				  ) AS 'credit',
				  t.value,
				  t.notes 
				FROM
				  `an_cash_management` AS t
				 left join  an_customers AS c 
				 
				on c.`id` = t.`customer_id` ";
				
				
		if($branch_id!=''){
			$sql .= "  where t.branch_cash_id = '".$branch_id."'  ";
		}
		
		$sql .= " order by t.id asc ";
		
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	
	function LoadTransactions($account_id='')
	{
		$sql = "SELECT 
				  t.id,
				  t.`job_id`,
				  t.payment_type,
				  t.value,
				  t.created,
				  c.`customer_name`,
				  IF(
					t.transaction_type = 'debit',
					t.value,
					'0'
				  ) AS 'debit',
				  IF(
					t.transaction_type = 'credit',
					t.value,
					'0'
				  ) AS 'credit',
				  t.value,
				  t.notes 
				FROM
				  an_company_transaction AS t, an_customers AS c
				  WHERE c.`id` = t.`customer_id` ";
		if($account_id!=''){
			$sql .= "  AND t.`account_id` = '".$account_id."'  ";
		}
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function LoadBankAccountByBankId($bank_id='')
	{
		$sql = "SELECT 
				  * 
				FROM
				  an_bank_accounts 
				WHERE bank_id = '".$bank_id."' 
				  AND STATUS = 'Active' ";
		$q = $this->db->query($sql);
		return $q->result();
		
	}
	
	
	function LoadBankAccountExceptAccountId($account_id='')
	{
		$sql = "SELECT 
				  * 
				FROM
				  an_bank_accounts 
				WHERE account_id != '".$account_id."' 
				  AND STATUS = 'Active' ";
		$q = $this->db->query($sql);
		return $q->result();
		
	}
	
	
	function updateBranchCash($branch_id='',$amount='0')
	{
		$sql = "UPDATE 
				  an_branch_cash 
				SET
				  branch_cash = branch_cash - $amount 
				WHERE branch_id = '".$branch_id."' ";
		$q = $this->db->query($sql);
		return true;
	}
	
	function OldLoadAllCashManagement()
	{
		$sql = "SELECT 
				  * 
				FROM
				  an_branch,
				  an_branch_cash 
				WHERE an_branch.`branch_id` = an_branch_cash.`branch_id` ";
		$q = $this->db->query($sql);
		return $q->result();		
	}
	
	function LoadAllCashManagementWithFilter($from='',$to='')
	{
		$sql = "SELECT 
				  ab.*,
				  atc.*,
				  atc2.*,
				  b.*,
				  ab.`created` AS account_created 
				FROM
				  `an_branch_cash` AS ab 
				  INNER JOIN `an_branch` AS b 
					ON b.`branch_id` = ab.`branch_id`
				  LEFT JOIN 
					(SELECT 
					  act.`branch_id`,
					  SUM(act.`value`) AS totaldebit 
					FROM
					  `an_cash_management` AS act 
					WHERE act.`transaction_type` = 'debit' 
					GROUP BY act.`branch_id`) AS atc 
					ON atc.branch_id = ab.`branch_id` 
				  LEFT JOIN 
					( SELECT 
					  act2.`branch_id`,
					  SUM(act2.`value`) AS totalcredit 
					FROM
					  `an_cash_management` act2 
					WHERE act2.`transaction_type` = 'credit' 
					GROUP BY act2.`branch_id`) AS atc2 
					ON atc2.branch_id = ab.branch_id 
					
					WHERE ab.created  BETWEEN '".$from."' AND '".$to."' 
					";
		$q = $this->db->query($sql);
		return $q->result();		
	}
	function LoadAllCashManagement()
	{
		/*$sql = "SELECT 
				  ab.*,
				  atc.*,
				  atc2.*,
				  b.*,
				  ab.`created` AS account_created 
				FROM
				  `an_branch_cash` AS ab 
				  INNER JOIN `an_branch` AS b 
					ON b.`branch_id` = ab.`branch_id`
				  LEFT JOIN 
					(SELECT 
					  act.`branch_id`,
					  SUM(act.`value`) AS totaldebit 
					FROM
					  `an_cash_management` AS act 
					WHERE act.`transaction_type` = 'debit' 
					GROUP BY act.`branch_id`) AS atc 
					ON atc.branch_id = ab.`branch_id` 
				  LEFT JOIN 
					( SELECT 
					  act2.`branch_id`,
					  SUM(act2.`value`) AS totalcredit 
					FROM
					  `an_cash_management` act2 
					WHERE act2.`transaction_type` = 'credit' 
					GROUP BY act2.`branch_id`) AS atc2 
					ON atc2.branch_id = ab.branch_id 
					";*/
		$sql = "SELECT 
    ab.*,
  totaldebit,
  totalcredit,
  ab.`id` AS branch_cash_id 
FROM
  `an_branch_cash` AS ab 
  LEFT JOIN 
    (SELECT 
      act.`branch_cash_id`,
      SUM(act.`value`) AS totaldebit 
    FROM
      `an_cash_management` act 
    WHERE act.`transaction_type` = 'debit' 
    GROUP BY act.`branch_cash_id`) AS atc 
    ON atc.branch_cash_id = ab.id
  LEFT JOIN 
    (SELECT 
      act2.`branch_cash_id`,
      SUM(act2.`value`) AS totalcredit 
    FROM
      `an_cash_management` act2 
    WHERE act2.`transaction_type` = 'credit' 
    GROUP BY act2.`branch_cash_id`) AS atc2 
    ON atc2.branch_cash_id = ab.id ";			
		$q = $this->db->query($sql);
		return $q->result();		
	}
	
	function LoadAllbanksAccounts()
	{
		$sql = "SELECT 
  ab.*,
  atc.*,
  atc2.*
FROM
  `an_branch_cash` AS ab 
  LEFT JOIN 
    (SELECT 
      act.`branch_cash_id`,
      SUM(act.`value`) AS totaldebit 
    FROM
      `an_cash_management` act 
    WHERE act.`transaction_type` = 'debit' 
    GROUP BY act.`branch_cash_id`) AS atc 
    ON atc.branch_cash_id = ab.id
  LEFT JOIN 
    (SELECT 
      act2.`branch_cash_id`,
      SUM(act2.`value`) AS totalcredit 
    FROM
      `an_cash_management` act2 
    WHERE act2.`transaction_type` = 'credit' 
    GROUP BY act2.`branch_cash_id`) AS atc2 
    ON atc2.branch_cash_id = ab.id ";
		$q = $this->db->query($sql);
		return $q->result();		
	}
	
	function updateBranchAccountCash($branch_id='',$amount='0')
	{
		$sql = "UPDATE 
				  an_branch_cash 
				SET
				  branch_cash = branch_cash - $amount 
				WHERE branch_id =  ".$branch_id;
		$q = $this->db->query($sql);
		return true;
	}
	
	function updateBranchAccountCashSender($branch_id='',$amount='0')
	{
		$sql = "UPDATE 
				  `an_branch_cash` 
				SET
				  branch_cash = branch_cash + $amount 
				WHERE branch_id =  ".$branch_id;
		$q = $this->db->query($sql);
		return true;
	}
	
	
	
	function udpateJobStatus($job_id='')
	{
		$data['job_status'] = 'close';
		$this->db->where('id', $job_id);
		$this->db->update('an_jobs',$data);
		return true;
	}
	
	function udpateJobStatusPaid($job_id='',$charge_id='')
	{
		$rec = $this->getJobType($job_id,$charge_id);
		
		$tablename = $rec->tablename;
		
		$data['is_company_paid'] = '1';
		$this->db->where('job_id', $job_id);
		$this->db->where('charge_id', $charge_id);
		$this->db->update($tablename,$data);
		return true;
	}
	
	// Created By M.Ahmed
	
	function getJobType($job_id='',$charge_id=''){
		if($job_id!=''){
			$sql = "SELECT *,'an_contract_job_charges' as tablename FROM an_contract_job_charges AS cj, an_charges 													AS c WHERE 
					cj.`charge_id` = c.`charge_id`
					AND job_id = '".$job_id."' AND c.`charge_id` = '".$charge_id."' ";
			$q = $this->db->query($sql);
			
			$q = $this->db->query($sql);
			if($q->num_rows() > 0)
			{
				return $q->row();
			}else{
				$sql = "SELECT *,'an_fixed_job_charges' as tablename FROM an_fixed_job_charges AS cj, an_charges AS c WHERE 
						cj.`charge_id` = c.`charge_id`
						AND job_id = '".$job_id."'  AND c.`charge_id` = '".$charge_id."' ";
				$q = $this->db->query($sql);
				
				$q = $this->db->query($sql);
				if($q->num_rows() > 0)
				{
					return $q->row();
				}else{
					$sql = "SELECT *,'an_cash_job_charges' as tablename FROM an_cash_job_charges AS cj, an_charges AS c WHERE 
							cj.`charge_id` = c.`charge_id`
							AND job_id = '".$job_id."'  AND c.`charge_id` = '".$charge_id."'  ";
					$q = $this->db->query($sql);
					
					$q = $this->db->query($sql);
					if($q->num_rows() > 0)
					{
						return $q->row();
					}else{
						return false;
					}
				}
			}
		}else{
			return false;
		}
		
	}
	
	
//----------------------------------------------------------------------	
}
?>