<br/>
<div class="table-wrapper users-table">
    <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">
 

 <?php error_hander($this->input->get('e')); ?>

        <div class="">
            <div class="">
                <?php /* action_buttons('addnewcustomer', $cnt); */ ?>
                <table id="new_data_table">
                    <thead class="thead">
                        <tr>
                            <th  id="no_filter"><label for="checkbox">
                                    <!--<input type="checkbox" class="" id="checkAll" data-set=""/>-->
                                </label></th>
                            <th ><?php echo lang('transaction_no') ?></th>
                            <th ><?php echo lang('amount') ?></th>
                            <th ><?php echo lang('Debit') ?></th>
                            <th ><?php echo lang('Credit') ?></th>
                            <th ><?php echo lang('Total') ?></th>
                            <th  id="no_filter">&nbsp;</th>
                        </tr>
                    </thead>
                    <?php
                    $cnt = 0;
                    $total_debit = 0;
                    $total = 0;
                    foreach ($cashs as $eachTrans) {
                        $cnt++;
                        ?>
                        <tr>
                            <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $eachTrans->id; ?>" value="<?php echo $eachTrans->id;
                    ; ?>" /></td>
                            <td><?php echo $eachTrans->id; ?></td>
                            <td><?php echo number_format($eachTrans->value, 2, '.', ',') ?></td>
                            <td class="green"><?php if ($eachTrans->transaction_type == 'debit') {
                        echo number_format($eachTrans->debit, 2, '.', ',');
                        $total_debit = $total_debit + $eachTrans->debit;
                    } ?></td>
                            <td class="red"><?php if ($eachTrans->transaction_type == 'credit') {
                        echo number_format($eachTrans->credit, 2, '.', ',');
                        $total_credit = $total_credit + $eachTrans->credit;
                    } ?></td>
                            <td><?php if ($eachTrans->transaction_type == 'debit') {
                        echo $total = $total + $eachTrans->value;
                    } else {
                        echo $total = $total - $eachTrans->value;
                    } ?></td>
                            <td>            
                                <a  href="#"><i class="icon-eye-view"></i></a>


                            </td>
                        </tr>
<?php } ?>
                    <tr>
                        <td></td>
                        <td><?php echo lang('Total') ?></td>
                        <td></td>
                        <td class="green"><?php echo number_format($total_debit, 2, '.', ','); ?></td>
                        <td class="red"><?php echo number_format($total_credit, 2, '.', ','); ?></td>
                        <td><?php echo number_format($total, 2, '.', ','); ?></td>
                        <td>            


                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>

<!--<input type="submit" class="send_icon" value=""/>-->
    </form>
</div>
