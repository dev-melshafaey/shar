
<div class="row form-wrapper">
  <div class="col-md-12 col-xs-12">
    <div id="main-content" class="main_content"> 
      <!--      <div class="title title alert alert-info"> <span><?php echo lang('add-edit') ?></span> </div>
      <div class="notion title title alert alert-info"> * <?php echo lang('mess1') ?> </div>-->
      <?php error_hander($this->input->get('e')); ?>
      <form action="<?php echo base_url(); ?>cash_management/create_new_branch_cash" method="post" id="form1" class="" name="frm_customer" autocomplete="off">
        <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
        <input type="hidden" name="userid" id="userid" value="<?php echo $user->userid; ?>" />
        <div class="form">
          <div class="g12">
            <div class="g4 form-group">
                                    <label class="text-warning"><?php echo lang('Company-Name') ?> </label>
                                    <div class="">
                                        <div class="ui-select" style="width:100%">
                                            <div class="">
                                             <?php company_dropbox('companyid', $user->companyid); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            
            <div class="g4 form-group">
              <label class="col-lg-12 text-warning"><?php echo "Branch Title";  //echo lang('accountTitle') ?></label>
              <input name="branch_cash_name" id="branch_cash_name" value="" type="text"   class="form-control" style=""/>
            </div>
            <div class="g4 form-group">
              <label class="col-lg-12 text-warning"><?php echo lang('openingBalance') ?></label>
              <input name="branch_cash" id="branch_cash" value="" type="text"   class="form-control" style=""/>
            </div>
 
            <div class="g4 form-group">
              <label class="col-lg-12 text-warning"><?php echo lang('clearanceDate') ?></label>
              <input name="clearance_date" id="clearance_date" value="" type="text"  class="datapic_input form-control" style=""/>
            </div>
                        <div class="g4 form-group">
              <label class="col-lg-12 text-warning"><?php echo lang('Notes') ?></label>
				<textarea class="form-control" name="notes" id="notes"><?php echo $user->notes; ?></textarea>
            </div>
                <br  clear="all"/> 
                <div class=" form-group">
                            <input type="hidden" name="member_type" id="member_type" value="6">
                            <input name="sub_mit" id="sub_mit" type="submit" class="btn-flat primary green flt-r g3" value="<?php echo lang('Add') ?>" />
                            <input name="sub_reset" type="reset" class="btn-flat primary gray flt-r g3" value="<?php echo lang('Reset') ?>" />

                        </div>
                
                     </div>
          
          
          <!--end of raw--> 
        </div>
        
 
      </form>
    </div>
    <!-- END PAGE --> 
  </div>
</div>
