<div style=" margin-left: 12px; margin-top: 4px;" class="fdirection">
    From Date 
    <input type="text" id="from_date_filter" value="<?php echo $from_date_filter; ?>"> 
    To Date 
    <input type="text" id="to_date_filter" value="<?php echo $to_date_filter; ?>" >
    <input type="button" onclick="updateBranchCashDataTableDiv();" value="Search">
</div>
<table width="100%" align="left" id="usertable">

    <thead>
        <tr>
            <th width="1%" id="no_filter"><label for="checkbox"></label></th>
            <th width="3%">Branch Name</th>
            <th width="16%" >Branch Code</th>
            <th width="11%" >Branch Cash</th>
            <th width="11%" >Branch Address</th>
            <th width="11%">Debit</th>
            <th width="11%">Credit</th>
            <th width="11%">Status</th>
            <th width="11%">Created</th>
            <th width="7%" id="no_filter">&nbsp;</th>
        </tr>
    </thead>


    <?php
    foreach ($CashManagement as $eachBranch) {
        ?>
        <tr>
            <td><input type="checkbox" name="checkbox2" id="checkbox2" /></td>
            <td><?php echo $eachBranch->branch_name; ?></td>
            <td><?php echo $eachBranch->branch_code; ?></td>
            <td><?php echo $total = $eachBranch->totaldebit - $eachBranch->totalcredit; ?></td>
            <td><?php echo $eachBranch->branch_address; ?></td>
            <td><?php echo ($eachBranch->totaldebit != '') ? $eachBranch->totaldebit : 0; ?></td>
            <td><?php echo ($eachBranch->totalcredit != '') ? $eachBranch->totalcredit : 0; ?></td>

            <td><?php echo ($eachBranch->status == "A") ? "Active" : "Deactive"; ?></td>
            <td><?php echo $eachBranch->created; ?></td>



            <td> <a id="<?php echo $eachBranch->branch_id; ?>" href='#' class='basic'><img src="<?php echo base_url(); ?>images/internal/i_info.png" width="28" height="18" border="0" /></a>

                <a id="" href='<?php echo base_url() . 'cash_management/index/' . $eachBranch->branch_id; ?>'>View Transaction</a> 

                <?php /* ?><a id="" href='<?php echo base_url().'transaction_management/index/'.$eachBranch->branch_id; ?>'>View Transaction</a><?php */ ?>          
                <!-- modal content -->

                <div id="basic-modal-content" class="dig<?php echo $eachBranch->branch_id; ?>">
                    <h3><?php echo $eachBranch->branch_name; ?><br />
                        Branch Code  : #<?php echo $eachBranch->branch_code ?></h3>

                    <code> <span class="pop_title">Cash : </span>
                        <div class="pop_txt"><?php echo $total = $eachBranch->totaldebit - $eachBranch->totalcredit; ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Address: </span>
                        <div class="pop_txt"><?php echo ($eachBranch->branch_address); ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Status : </span>
                        <div class="pop_txt"><?php echo ($eachBranch->status == "A") ? "Active" : "Deactive"; ?></div>
                    </code>
                    <!--line--> 
                    <code> <span class="pop_title">Contact Person : </span>
                        <div class="pop_txt"><?php echo $eachBranch->contact_person; ?></div>
                    </code>
                    <!--line--> 
                    <code> <span class="pop_title">Total Debit : </span>
                        <div class="pop_txt"><?php echo ($eachBranch->totaldebit != '') ? $eachBranch->totaldebit : 0; ?></div>
                    </code>
                    <!--line--> 
                    <code> <span class="pop_title">Total Credit : </span>
                        <div class="pop_txt"><?php echo ($eachBranch->totalcredit != '') ? $eachBranch->totalcredit : '0'; ?></div>
                    </code>  
                    <!--line--> 
                    <code> <span class="pop_title">Account Created : </span>
                        <div class="pop_txt"><?php echo $eachBranch->created; ?></div>
                    </code> 
                    <!--line--> 


                    <?php
                    //get_permission_text();
                    ?>
                    </ul>
                </div>
                </code>
                </div></td>
        </tr>
        <?php
    }
    ?>

</table>
<script>
    $(document).ready(function () {

        $("#from_date_filter").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            numberOfMonths: 1,
            onClose: function (selectedDate) {
                $("#to_date_filter").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#to_date_filter").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            numberOfMonths: 1,
            onClose: function (selectedDate) {
                $("#from_date_filter").datepicker("option", "maxDate", selectedDate);
            }
        });


        if ($('#usertable').length > 0)
        {
            $('#usertable tfoot td').each(function () {
                var title = $('#usertable thead th').eq($(this).index()).text();
                var attr_id = $('#usertable thead th').eq($(this).index()).attr('id');
                $(this).html('<input class="' + attr_id + ' searchfilter" type="text" placeholder="' + title + '" />');
            });



            var user_table = $('#usertable').DataTable();
            user_table.columns().eq(0).each(function (colIdx) {
                $('input', user_table.column(colIdx).footer()).on('keyup change', function () {
                    user_table
                            .column(colIdx)
                            .search(this.value)
                            .draw();
                });

            });
            $('.no_filter, #usertable_filter').hide();

            $('.searchfilter').keyup(function () {
                console.log($(this).val());
                $('#usertable td').removeHighlight().highlight($(this).val());
            });
        }


        $('.basic').click(function () {

            var id = $(this).attr('id');
            $('.dig' + id).modal();

            return false;
        });

    });

</script> 