<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cash_management extends CI_Controller {
    /*
     * Properties
     */

    private $_data = array();

//----------------------------------------------------------------------
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        //load Home Model
        $this->load->model('cash_management_model', 'cash_management');
        $this->load->model('transaction_management/transaction_management_model', 'transaction_management');
		$this->lang->load('main', get_set_value('site_lang'));
        $this->load->model('jobs/jobs_model', 'jobs');
        $this->load->model('customers/customers_model', 'customers');
        $this->load->model('users/users_model', 'users');
        $this->_data['udata'] = userinfo_permission();
    }

//----------------------------------------------------------------------
    /*
     * cash Home Page
     */
    public function index($branch_id = '') {

        //redirect(base_url().'cash_management/cash');
        //  Load Home View

        $this->_data['cashs'] = $this->cash_management->LoadCashs($branch_id);
		//print_r($this->_data['cashs']);
        $this->_data['debitAmont'] = $this->cash_management->loadamount($branch_id);

        //$this->load->view('cashs', $this->_data);
        
        $this->_data['inside'] = 'transactions';
		//$this->_data['inside2'] = 'cashs';
		//$this->_data['inside2'] = 'cash-list';
        //$this->_data['inside'] = 'add-newtransactionr-form';

        $this->load->view('common/main', $this->_data);
    }

    public function indexDataTable($branch_id = '') {

        $this->_data['from_date_filter'] = $this->input->post('from_date_filter');
        $this->_data['to_date_filter'] = $this->input->post('to_date_filter');

        $this->_data['cashs'] = $this->cash_management->LoadCashsWithFilter($branch_id, $this->_data['from_date_filter'], $this->_data['to_date_filter']);
        $this->_data['debitAmont'] = $this->cash_management->loadamount($branch_id);

        //$this->load->view('cashsDataTable', $this->_data);
                
        $this->_data['inside'] = 'cashsDataTable';
        $this->load->view('common/main', $this->_data);
    }

    function create_new_branch_cash() {
        $postData = $this->input->post();
        //echo "<pre>";
        //print_r($postData);
		//exit;
        $cash_data['branch_id'] = $postData['branch_id'];
		$cash_data['company_id'] = $postData['companyid'];
        $cash_data['branch_cash_name'] = $postData['branch_cash_name'];
        $cash_data['branch_cash'] = $postData['branch_cash'];
        $cash_data['created'] = date('Y-m-d');
        $this->db->insert('an_branch_cash', $cash_data);
        $cash_branch_id = $this->db->insert_id();

        $opening_transaction['branch_cash_id'] = $cash_branch_id;
        $opening_transaction['transaction_type'] = 'debit';
        $opening_transaction['payment_type'] = 'deposite';
        $opening_transaction['value'] = $postData['branch_cash'];
        $opening_transaction['created'] = date('Y-m-d');
        $this->db->insert('an_cash_management', $opening_transaction);

        do_redirect('cash_management?e=10');
    }

    function new_cash_branch() {
        $userid = $this->session->userdata('userid');
        $this->_data['user'] = $this->users->get_user_detail($userid);
        //$this->load->view('new_cash_branch', $this->_data);
        $this->_data['inside'] = 'new_cash_branch';
        $this->load->view('common/main', $this->_data);
        
    }

//----------------------------------------------------------------------
    public function banks() {
        // Load Home View
        $this->_data['bank_accounts'] = $this->cash_management->LoadAllbanksAccounts();
        //$this->load->view('banks', $this->_data);
                $this->_data['inside'] = 'banks';
        $this->load->view('common/main', $this->_data);
    }

    public function cash() {
        // Load Home View
        $this->_data['CashManagement'] = $this->cash_management->LoadAllCashManagement();
        //$this->load->view('cash-management', $this->_data);
        //$this->_data['inside'] = 'cash-management';
		 $this->_data['inside'] = 'cash-list';
        $this->load->view('common/main', $this->_data);
    }

    public function cashWithFilter() {
        $this->_data['from_date_filter'] = $this->input->post('from_date_filter');
        $this->_data['to_date_filter'] = $this->input->post('to_date_filter');
        $this->_data['CashManagement'] = $this->cash_management->LoadAllCashManagementWithFilter($this->_data['from_date_filter'], $this->_data['to_date_filter']);
        //$this->load->view('cash-managementDatatable', $this->_data);
        $this->_data['inside'] = 'cash-managementDatatable';
        $this->load->view('common/main', $this->_data);
    }

    /*
     * Add cash Form Page
     */

    public function add_cash() {

        $data = $this->input->post();
        //echo "<pre>";
        //print_r($data);
		//exit;
        unset($data['job_type']);
        unset($data['job_status'], $data['submit_cash']);
        $data['charge_id'] = isset($data['charges_selection']) ? $data['charges_selection'] : 0;
        $data['job_id'] = isset($data['job_id']) ? $data['job_id'] : 0;

        unset($data['charges_selection']);
        unset($data['jobs']);
        $data['created'] = date('Y-m-d');

        $userid = $this->session->userdata('userid');
        $user = $this->users->get_user_detail($userid);
		unset($data['ownerid']);
		unset($data['userid']);
		unset($data['member_type']);
		unset($data['sub_mit']);
		unset($data['job_id']);
        $data['branch_cash_id'] = $data['cash_brach_id'];
		unset($data['cash_brach_id']);
	
        //$return = $this->transaction_management->checkPaymentTransaction($data['job_id'], $data['charges_job'], $data['transaction_by']);
        //$cashData['customer_id'] = $this->transaction_management->getCustomerId($data['job_id']);
        /*if (!$return) {
            if ($data['transaction_type'] == 'debit' && $data['amount_type'] == 'advance') {

                $payments['payment_type'] = 'receipt';
                $payments['payment_amount'] = $data['value'];
                $payments['refrence_id'] = $cashData['customer_id'];
                $payments['refernce_type'] = 'against';
                $this->db->insert('payments', $payments);
                $payment_id = $this->db->insert_id();
                $cashData['payment_amount'] = $data['value'];
                $cashData['payment_purpose'] = $data['amount_type'];
                $cashData['payment_by'] = $data['transaction_by'];
                $cashData['account_pk'] = $data['account_id'];
                $cashData['account_id'] = $data['account_id'];
                $cashData['parent_id'] = $payment_id;
                $cashData['job_id'] = $data['jobs'];
                $cashData['charges_id'] = $data['charges_job'];
                $this->db->insert('job_payment', $cashData);

                $chargesData['payment_amount'] = $data['value'];
                $this->db->where('jb_c_id', $data['job_charges_id']);
                $this->db->update('jobs_charges', $chargesData);
            } elseif ($data['amount_type'] == 'sales') {
                $chargesData['payment_amount'] = $data['value'];
                $this->db->where('jb_c_id', $data['job_charges_id']);
                $this->db->update('jobs_charges', $chargesData);
            }
        }*/

        $cash_type = $data['cash_type'];

        

        unset($data['charges_job']);
        $id = $this->cash_management->insertNewcash($data, '');




        if ($data['payment_type'] == 'transfer') {
            $data2 = $data;
            if ($data['transaction_type'] == 'debit') {
                $data2['transaction_type'] = 'credit';
            } else {
                $data2['transaction_type'] = 'debit';
            }


            $data2['branch_id'] = $data['rec_branch_id'];
            unset($data2['rec_branch_id']);
            $id2 = $this->cash_management->insertNewcash($data2, $data['emp_id']);
        } else if ($data['payment_type'] == 'deposite' && $cash_type != 'Indirect') {
            $data2 = $data;
            if ($data['transaction_type'] == 'debit') {
                $data2['transaction_type'] = 'credit';
            } else {
                $data2['transaction_type'] = 'debit';
            }
            //$data2['transaction_type'] = 'credit';
            $data2['branch_id'] = $data['rec_branch_id'];
            unset($data2['rec_branch_id']);
            //$id2 = $this->cash_management->insertNewcash($data2,$data['emp_id']);
        }
        if ($id) {
            //update account - minus balance from compnay account.
            if ($data['rec_branch_id'] == '' || !isset($data['rec_branch_id'])) {

                // here we are deposite cash so it will + plus in Branch Cash accounts.
              //  $this->cash_management->updateBranchAccountCashSender($data['branch_id'], $data['value']);
                //logic for image upload start
                // this logic charges end
                //-----------------------------------------------------
                //$config['upload_path'] = './uploads/recipt/';
                //$config['allowed_types'] = 'gif|jpg|png';
//$config['max_size']	= '1000';
                //$config['max_width']  = '10240';
                //$config['max_height']  = '7680';
                //$this->load->library('upload', $config);
                //if ( ! $this->upload->do_upload('deposite_recipt'))
//
//				{
//
//					$error = array('error' => $this->upload->display_errors());
//
//					print_r($error);
//
//					exit;
//
//					//$this->load->view('upload_form', $error);
//
//					//do_redirect('options?e=10');
//
//				}
//
//				else
//
//				{
//
//					$upload_data = $this->upload->data(); 
//
//					
//
//					$file_name = $upload_data['file_name'];
//
//					//$exts = split("[/\\.]", $file_name);
//
//					//$n    = count($exts)-1;
//
//					//$ext  = $exts[$n];
//
//					
//
//					$datas['deposite_recipt'] = $file_name;
//
//					$this->db->where('id', $id);
//
//					$this->db->update('an_cash_management', $datas);
//
//					//do_redirect('options?e=10');
//
//				}
                //logic for image upload end
                //update branch cash - minus balance from branch cash.
                //$this->cash_management->updateBranchCash($data['branch_id'],$data['value']);
            } else {

                if ($data['transaction_type'] == 'credit') {
                    $this->cash_management->updateBranchAccountCash($data['branch_id'], $data['value']);

                    $this->cash_management->updateBranchAccountCashSender($data['rec_branch_id'], $data['value']);
                } else if ($data['transaction_type'] == 'debit') {
                    $this->cash_management->updateBranchAccountCashSender($data['branch_id'], $data['value']);

                    $this->cash_management->updateBranchAccountCash($data['rec_branch_id'], $data['value']);
                }
            }

            //update status of charge which is paid and it not further list in add transcation for this customer.
            //if($data['charge_id']!='7')
            if ($data['job_id'] != 0) {
                $this->cash_management->udpateJobStatusPaid($data['job_id'], $data['charge_id']);
                if ($data['charge_id'] == '7') {
                    
                }
            }
        }
        redirect(base_url() . 'cash_management');
    }

//----------------------------------------------------------------------
    /*
     * Add cash Form Page
     */
    public function new_cash() {
        // Load Home View
        $this->_data['customers'] = $this->jobs->get_all_customers();
        $this->_data['jobs'] = $this->jobs->getAlljobs();
        $this->_data['invoices'] = $this->jobs->getAllinvoices();
		$this->_data['cashbranches'] = $this->cash_management->getAllcashBraches();
		//echo "<pre>";
		//print_r($this->_data['cashbranches']);			

		

        $this->_data['customers'] = $this->jobs->get_all_customers();
        //$this->load->view('add-cash-form', $this->_data);
        //$this->_data['inside'] = 'add-cash-form';
       // $this->_data['inside2'] = 'cash-list';
        $this->_data['inside'] = 'add-newtransactionr-form';
		
		$this->load->view('common/main', $this->_data);
    }

    /*
     * Add loadtranserBankaccounts
     */

    public function loadtranserBankaccounts() {
        $account_id = $this->input->post('account_id');
        $res = $this->cash_management->LoadBankAccountExceptAccountId($account_id);
        ?>
        <select id="rec_account_id" name="rec_account_id" onchange="" >
            <option value="">Select Bank Account</option>
        <?php
        foreach ($res as $eachRes) {
            ?>
                <option value="<?php echo $eachRes->account_id; ?>"><?php echo $eachRes->account_name; ?></option>
            <?php
        }
        ?>
        </select>
            <?php
        }

        /*
         * Add getBankAccounts
         */

        public function getBankAccounts() {
            $bank_id = $this->input->post('bank_id');
            $res = $this->cash_management->LoadBankAccountByBankId($bank_id);
            ?>
        <select id="account_id" name="account_id" onchange="loadtranserBankaccounts();
                       " >
            <option value="">Select Bank Account</option>
        <?php
        foreach ($res as $eachRes) {
            ?>
                <option value="<?php echo $eachRes->account_id; ?>"><?php echo $eachRes->account_name; ?></option>
            <?php
        }
        ?>
        </select>
            <?php
        }

        /*
         * Add cash Form Page
         */

        public function cash_methods() {
            // Load Home View
            //$this->load->view('cash-methods', $this->_data);
            $this->_data['inside'] = 'cash-methods';
            $this->load->view('common/main', $this->_data);
        }

        //----------------------------------------------------------------------
        /*
         * Add New Invoice
         */
        public function add_invoice() {
            // Load Home View
            //$this->load->view('add-invoice', $this->_data);
            $this->_data['inside'] = 'add-invoice';
            $this->load->view('common/main', $this->_data);
        }

        //----------------------------------------------------------------------
        /*
         * Add New Invoice
         */
        public function invoice() {
            // Load Home View
            //$this->load->view('invoice', $this->_data);
            $this->_data['inside'] = 'invoice';
            $this->load->view('common/main', $this->_data);
        }

//----------------------------------------------------------------------
    }
    ?>