<?php $this->load->view('common/meta');?>

<!--body with bg-->

<div class="body">

<header>

	<?php $this->load->view('common/header');?>

	<?php $this->load->view('common/search');?>

	<nav>

		<?php $this->load->view('common/navigations');?>

	</nav>

</header>



<!--Section-->

<section>

<div id="container" class="row-fluid">

      <!-- BEGIN SIDEBAR -->

		<?php $this->load->view('common/left-navigations');?>

      <!-- END SIDEBAR -->

      <!-- BEGIN PAGE -->  

      <div id="main-content" class="main_content">

      <div class="title"> <span><?php breadcramb(); ?></span> </div>

      <?php error_hander($this->input->get('e')); ?>

      <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">

        <div class="form">

          <div class="CSSTableGenerator " id="printdiv" >

<div style="float: left; margin-left: 12px; margin-top: 4px;">
                From Date 
                <input type="text" id="from_date_filter" value=""> 
                To Date 
                <input type="text" id="to_date_filter" value="" >
                <input type="button" onclick="updateEachBranchCashDataTableDiv();" value="Search">
            </div>

                <table width="100%" align="left" id="usertable">

                

                	<thead>

                    <tr>

                      <th width="1%" id="no_filter"><label for="checkbox"></label></th>

                      <th width="3%">Transaction No.</th>

                      <th width="3%">Job Id.</th>

                        <th width="16%" >Payment Type</th>

                        <th width="11%" >Transaction Amount</th>

                        <th width="11%" >Date of Import</th>

                        <th width="11%">Customer</th>

                        <th width="11%">Debit</th>

                        <th width="11%">Credit</th>

                        <th width="3%">Total</th>

                        <th width="13%">Notes</th>

                        <th width="7%" id="no_filter">&nbsp;</th>

                    </tr>

                    </thead>

              		<tfoot>

                    <tr>

                      <td style="background-color:#afe2ee"></td>

                      <td  style="background-color:#afe2ee">

                      <input type="text" name="textfield" id="textfield"  class="flex_feild"/>

                      </td>

                      <td  style="background-color:#afe2ee">

                      <input type="text" name="textfield" id="textfield"  class="flex_feild"/>

                      </td>

                       <td style="background-color:#afe2ee" >

                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>

                      </td>

                       <td  style="background-color:#afe2ee">

                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>

                      </td>

                       <td  style="background-color:#afe2ee">

                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>

                      </td>

                       <td  style="background-color:#afe2ee">

                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>

                      </td>

                       <td  style="background-color:#afe2ee">

                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>

                      </td>

                       <td  style="background-color:#afe2ee">

                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>

                      </td>

                       <td  style="background-color:#afe2ee">

                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>

                      </td>

                       <td  style="background-color:#afe2ee"></td>

                 

                       <td  style="background-color:#afe2ee">&nbsp;</td>

                  </tr>

                  </tfoot>

                    

                    <?php
					$total_debit =0;
					$total_credit =0;
					$total = 0;

					foreach($cashs as $eachcashs){

					?>

                    <tr>

                      <td><input type="checkbox" name="checkbox2" id="checkbox2" /></td>

                      <td><?php echo $eachcashs->id;?></td>

                      <td><?php echo $eachcashs->job_id;?></td>

                        <td><?php echo $eachcashs->payment_type;?></td>

                        <td><?php echo $eachcashs->value;?></td>

                        <td><?php echo $eachcashs->created;?></td>

                        <td><?php echo $eachcashs->customer_name;?></td>

                        <td><?php if($eachcashs->transaction_type == 'debit')  { echo number_format($eachcashs->debit, 2, '.', ','); $total_debit = $total_debit+$eachcashs->debit;} ?></td>
                        <td><?php if($eachcashs->transaction_type == 'credit') { echo number_format($eachcashs->credit, 2, '.', ','); $total_credit = $total_credit+$eachcashs->credit; } ?></td>
                        <td><?php     if($eachcashs->transaction_type == 'debit') { echo $total = $total+$eachcashs->value; } else { echo $total = $total-$eachcashs->value; } ?></td>

                        <td><?php echo substr($eachcashs->notes,0,10).'...';?></td>

                        

                        

                  

                      <td> <a id="<?php echo $eachcashs->id; ?>" href='#' class='basic'><img src="<?php echo base_url(); ?>images/internal/i_info.png" width="28" height="18" border="0" /></a>          

                  <!-- modal content -->

                  

                  <div id="basic-modal-content" class="dig<?php echo $eachcashs->id; ?>">

                    <h3><?php echo $eachcashs->customer_name; ?><br />

                      Date  : #<?php echo $eachcashs->created ?></h3>

                    

                    <code> <span class="pop_title">JOb Id: </span>

                    <div class="pop_txt"><?php echo $eachcashs->job_id; ?></div>

                    </code>

                    

                    <code> <span class="pop_title">Payment Type : </span>

                    <div class="pop_txt"><?php echo $eachcashs->payment_type; ?></div>

                    </code> 

                    <!--line--> 

                    <code> <span class="pop_title">Transaction amount : </span>

                    <div class="pop_txt"><?php echo ($eachcashs->value); ?></div>

                    </code> 

                    <!--line--> 

                    <code> <span class="pop_title">Debit : </span>

                    <div class="pop_txt"><?php echo $eachcashs->debit; ?></div>

                    </code>

                    <!--line--> 

                    <code> <span class="pop_title">Credit : </span>

                    <div class="pop_txt"><?php echo $eachcashs->credit; ?></div>

                    </code> 

                   <!--line--> 

                    <code> <span class="pop_title">Notes : </span>

                    <div class="pop_txt"><?php echo $eachcashs->notes; ?></div>

                    </code> 

                     <!--line--> 

                   

                   

                    	<?php

							//get_permission_text();

						?>

                      </ul>

                    </div>

                    </code>

                    </div></td>

                    </tr>

                    <?php

					}

					?>

                            

                </table>
                <table style="padding-left: 46%;">
                
                     <tr>
                        <td  style="width: 42%;"><?php echo number_format($total_debit, 2, '.', ',');?></td>
                        <td  style="width: 36%;"><?php echo number_format($total_credit, 2, '.', ',');?></td>
                        <td><?php echo number_format($total, 2, '.', ',');?></td>
                        <td></td>
                     
                     </tr>
                </table>

            </div>

        </div>

        <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>

       

        <?php //action_buttons('addnewcustomer',$cnt); ?>

        <!--<input type="submit" class="send_icon" value=""/>-->

      </form>

    </div>

      <!-- END PAGE -->  

   </div>

</section>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(function() {
$( "#from_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to_date_filter" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from_date_filter" ).datepicker( "option", "maxDate", selectedDate );
}
});
});
</script>
<!-- End Section-->

<!--footer-->

<?php $this->load->view('common/footer');?>

