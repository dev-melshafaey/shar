<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/left-navigations');?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content" class="main_content">
    	<div class="title">
    		<span><a href="">Cash Management</a></span>
    		<span class="subtitle">New Cash</span>
    	</div>

		<form id="form_cashs" action="<?php echo base_url();?>cash_management/add_cash" method="post" enctype="multipart/form-data">
			<input type="hidden" id="job_charges_id" name="job_charges_id" />
			 <input  type="hidden" name="transaction_by" class="transaction_by" value="Job" id="transaction_type1">
            <div class="form" id="form-refresh">
          
          
	          <div class="raw form-group">
                    <div class="form_title">Cash Type:</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
                          <?php get_statusdropdown((isset($cash->cash_type) && $cash->cash_type !='' )?$cash->cash_type:'','cash_type','cash_type',false,' onchange="loadCashType()" ');?>
                        </div>
                      </div>
                    </div>
                </div>
          
                <!--<div class="raw form-group">
                    <div class="form_title">Branch Name</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
                          <?php //company_branch_dropbox('branch_id',isset($user->branchid)?$user->branchid:'',' onchange="loadCustomersWithEmployeesAndBranches()" '); ?>
                        </div>
                      </div>
                    </div>
                </div>-->
                  
                <!--<div class="raw form-group" id="div_customers" style="">
                    <div class="form_title">Customer</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select" id="div_customers_responce">
                            <select id="customer_id" name="customer_id">
                            
								<option value="">Select Customers</option>
                            </select>
                        </div>
                      </div>
                    </div>
                </div>-->
                <div class="raw form-group" id="div_jobs">
                    <div class="form_title">Jobs:</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select" id="div_jobs_responce">
						 <select name="jobs" id="jobs" onchange="getCharges(this)">
                            <option value="">Select</option>
                            <?php
								foreach($jobs as $job){
									?>
                                   <option value="<?php echo $job->id; ?>" ><?php echo $job->id; ?></option> 
                                    <?php
								}
							?>
                          </select>
                			           
                        </div>
                      </div>
                    </div>
                </div>
                <?php
						  if(!empty($jobs)){
								  foreach($jobs as $job){
								   ?>
								   <input type="hidden" name="job_type[]" id="<?php echo $job->id; ?>" class="jobs_way" value="<?php echo $job ->way_of_shipment;?>" />
								   <?php
								  }
						  }
						  
						   ?>
                <div class="raw form-group" style="display:none;" id="bl_div">
                        <div class="form_title">Bl Number: </div>
                        <div class="form_field">
                            <span id="bl_span"></span>
                        </div>
                    </div>
                <div class="raw form-group" id="div_charges" style="display:none;">
                    <div class="form_title">Charges</div>
                    <div class="form_field">
                        <div class="dropmenu">
                            <div class="styled-select" id="div_charges_responce">
                            <select name="charges_job" id="charges_job">
                          </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="raw form-group">
                    <div class="form_title">Transaction Type:</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
                          <select name="transaction_type" id="transaction_type">
                            <option value="debit">Debit</option>
                            <option value="credit">Credit</option>
                          </select>
                        </div>
                      </div>
                    </div>
                </div>
        
                <div class="raw form-group">
                    <div class="form_title">Amount </div>
                    <div class="form_field">
                        <input name="value" id="value" type="text"  class="formtxtfield_small"/>
                        <span>RO.</span>
                    </div>
                     <div class="form_title">Amount Type</div>
                    <div class="form_field">
                        <div class="dropmenu">
                            <div class="styled-select">
                            <select name="amount_type" id="amount_type">
                          	<option value="">Select</option>
                          	<option value="advance">Advance Received</option>
                            <option value="sales">Sales</option>
                            <option value="purchase">Purchase</option>
                          </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="raw form-group">
                <div class="form_title">Payment Type</div>
                    <div class="form_field">
                        <div class="dropmenu">
                            <div class="styled-select">
                                <select name="payment_type" id="payment_type" onchange="toggleBranches();">
                                    <option value="">Select Payment Way</option>
                                    <option value="transfer">Transfer</option>
                                    <option value="cash">Cash</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="raw form-group" id="div_branch" style="display:none">
                    <div class="form_title">Transfer To Branch Name</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select" id="div_branch_responce">
                          <?php company_branch_dropbox('rec_branch_id','',' onchange="loadEmployees()" '); ?>
                        </div>
                      </div>
                    </div>
                </div>
                
                <?php /*?><div class="raw form-group" id="div_banks" style="display:none">
                    <div class="form_title">Banks </div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
                          <?php company_bank_dropbox('bank_id','','english',' onchange="getBankAccounts();" ');?>
                        </div>
                      </div>
                    </div>
                </div><?php */?>
                
                <!--<div class="raw form-group" style="display:none" id="div_employee">
                    <div class="form_title">Employee Name </div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select" id="div_employee_responce">
                        	<?php //employee_dropbox('emp_id',(isset($cash->emp_id) && $cash->emp_id !='' )?$cash->emp_id:'',' onchange="addherefunstion()" ');?>
                        </div>
                      </div>
                    </div>
                </div>-->
                
               
                
<!--                <div class="raw form-group" id="div_checkque" style="display:none">
                    <div class="form_title">Deposite Recipt </div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
	                        <input type="file"  class="formtxtfield" name="deposite_recipt" id="deposite_recipt" />
                        </div>
                      </div>
                    </div>
                </div>-->
                
               
                
                
                <div class="raw form-group" style="display:none" id="div_tr_accounts">
                    <div class="form_title">Accounts </div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select" id="div_tr_accounts_responce">
                          
                        </div>
                      </div>
                    </div>
                </div>
                
                <div class="raw">
                    <div class="form_title">Notes </div>
                    <div class="form_field">
                        <textarea name="notes" id="notes" cols="" rows="" class="formareafield"></textarea>
                    </div>
                </div>
                
                <div class="raw" style="display:none" id="div_payment_purpose">
                    <div class="form_title">Purpose of payment </div>
                    <div class="form_field">
                        <textarea name="purpose_of_payment" id="purpose_of_payment" cols="" rows="" class="formareafield"></textarea>
                    </div>
                </div>
                
                <div class="raw" align="center">
                    <input name="submit_cash" id="submit_cash" type="submit" class="submit_btn" value="Add" />
                    <!-- <input name="id" type="hidden"  value="" /> -->
                    
                </div>
                <!--end of raw-->
			</div>
		</form>
		<!-- END PAGE -->  
	</div>
    <!-- END PAGE -->  
</div>
</section>
<script type="text/javascript">
$(function() {
	$("#jobs").change(function(){
			
				//alert('change');
				jb_val = $(this).val();
				
				jb_type= $("#"+jb_val).val();
				//alert(jb_type);
				$.ajax({
				url: config.BASE_URL + "ajax/getcharges_ajax",
				type: 'post',
				data:{jb_type:jb_type,jb_val:jb_val},
				cache: false,
				success: function(data){
					res = jQuery.parseJSON(data);
					console.log(res);
					//console.log(res);
					$("#value").val(res.sales);
					$("#bl_span").html(res.bl);
					$("#bl_div").show();
			
					$("#charges_job").html(res.html);
					$("#div_charges").fadeIn();
				}
				});
			
			});
			
			$("#charges_job").change(function(){
				
				c_id = $(this).val();
				jobId	= $("#jobs").val();
				
				$.ajax({
				url: config.BASE_URL + "ajax/check_charges",
				type: 'post',
				data:{c_id:c_id,job_id:jobId},
				cache: false,
				success: function(data){
					
						//console.log(data+'data');
						res = $.parseJSON(data);
						console.log(res+'res');
						if(res.jb_c_id){
							$("#job_charges_id").val(res.jb_c_id);
						}
						
						if(res.is_actual== '1'){
							//alert(res.is_actual);
							
							/*if(res.charges_advance){
								$("#advance").val(res.charges_advance);	
							}*/
							
							if(res.charges_advance){
								$("#advance").val(0);	
							}
							
							$("#advance_payment").show();
						}
						else{
							$("#advance_payment").hide();
						}
						
						if(res.total_value){
								//$("#value").val(res.total_value);	
						}
						
								//$("#paid_amount").html(res.payment_amount);
								$("#payment_payment").show();
						
							
					//$("#charges_job").html(data);
					//$("#div_charges").fadeIn();
				}
				});
				
				$("#submit_cash").click(function(){
						$(".jobs_way").remove();
				});
			
			})
	});
</script>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
