<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>
<!--Section-->
<section>
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/left-navigations');?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content" class="main_content">
    	<div class="title">
    		<span><a href="<?php echo base_url(); ?>cash_management">Cash</a></span>
    		<span class="subtitle">New Cash Branch</span>
    	</div>
		<form id="form_transactions" action="<?php echo base_url();?>cash_management/create_new_branch_cash" method="post" enctype="multipart/form-data">
			<input type="hidden" id="job_charges_id" name="job_charges_id" />
			<div class="form" id="form-refresh">
          		<input  type="hidden" name="branch_id" value="<?php  echo $user->branchid;   ?>" />
                
                <div class="raw form-group">
                    <div class="form_title">Branch Title: </div>
                    <div class="form_field">
                        <input name="branch_cash_name" id="branch_cash_name" type="text"  class="formtxtfield"/>
                    </div>
                </div>
                
			    <div class="raw form-group">
                    <div class="form_title">Opening Balance: </div>
                    <div class="form_field">
                        <input name="branch_cash" id="branch_cash" type="text"  class="formtxtfield_small"/>
                        <span>RO.</span>
                    </div>
                </div>
                <div class="raw">
                    <div class="form_title">Notes </div>
                    <div class="form_field">
                        <textarea name="notes" id="notes" cols="" rows="" class="formareafield"></textarea>
                    </div>
                </div>
                
                <div class="raw" align="center">
                    <input name="submit_account" id="submit_account" type="submit" class="submit_btn" value="Add" />
                    <!-- <input name="id" type="hidden"  value="" /> -->
                    
                </div>
                <!--end of raw-->
			</div>
		</form>
		<!-- END PAGE -->  
	</div>
    <!-- END PAGE -->  
</div>
</section><script  type="text/javascript">
$(document).ready(function (){
			
	});
/*$(function() {
			var ac_config = {
		source: "<?php echo base_url();?>jobs/getAutoSearch",
		select: function(event, ui){
			$("#account_name").val(ui.item.city);
		},
		minLength:1
	};
	$("#account_name").autocomplete(ac_config);
	
    });*/
</script>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
