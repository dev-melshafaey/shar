<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
      <div class="title"> <span><?php breadcramb(); ?></span> </div>
      <?php error_hander($this->input->get('e')); ?>
      <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">
        <div class="form">
          <div class="CSSTableGenerator " id="printdiv" >

                <table width="100%" align="left" id="usertable">
                
                	<thead>
                    <tr>
                      <th width="1%" id="no_filter"><label for="checkbox"></label></th>
                      <th width="3%">Account Name</th>
                        <th width="16%" >Bank Name</th>
                        <th width="11%" >Account Number</th>
                        <th width="11%" >Account Debit Amount</th>
                        <th width="11%" >Account Credit Amount</th>
                        <th width="11%" >Account Total</th>
                        <th width="11%">Status</th>
                        <th width="11%">Created</th>
                        <th width="7%" id="no_filter">&nbsp;</th>
                    </tr>
                    </thead>
              		<tfoot>
                    <tr>
                      <td style="background-color:#afe2ee"></td>
                      <td  style="background-color:#afe2ee">
                      <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td style="background-color:#afe2ee" >
                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td  style="background-color:#afe2ee">
                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td  style="background-color:#afe2ee">
                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td  style="background-color:#afe2ee">
                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                        
                       <td  style="background-color:#afe2ee"></td>
                 
                       <td  style="background-color:#afe2ee">&nbsp;</td>
                  </tr>
                  </tfoot>
                    
                    <?php
					foreach($bank_accounts as $eachAcc){
					?>
                    <tr>
                      <td><input type="checkbox" name="checkbox2" id="checkbox2" /></td>
                      <td><?php echo $eachAcc->account_name;?></td>
                        <td><?php echo $eachAcc->bank_name;?></td>
                        <td><?php echo $eachAcc->account_number;?></td>
                        <td><?php if($eachAcc->totaldebit != "") echo $eachAcc->totaldebit; else echo "0";?></td>
                        <td><?php if($eachAcc->totalcredit != "") echo $eachAcc->totalcredit; else echo "0";?></td>
                        <td><?php echo $total = $eachAcc->totaldebit-$eachAcc->totalcredit;?></td>
                        <td><?php echo $eachAcc->status;?></td>
                        <td><?php echo $eachAcc->account_created;?></td>
                        
                        
                  
                      <td> <a id="<?php echo $eachAcc->account_id; ?>" href='#' class='basic'><img src="<?php echo base_url(); ?>images/internal/i_info.png" width="28" height="18" border="0" /></a> 
                      
                      <a id="" href='<?php echo base_url().'transaction_management/index/'.$eachAcc->account_id; ?>'>View Transaction</a>          
                  <!-- modal content -->
                  
                  <div id="basic-modal-content" class="dig<?php echo $eachAcc->account_id; ?>">
                    <h3><?php echo $eachAcc->account_name; ?><br />
                      Date  : #<?php echo $eachAcc->account_created ?></h3>
                    
                    <code> <span class="pop_title">Bank Name : </span>
                    <div class="pop_txt"><?php echo $eachAcc->bank_name; ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Account Number : </span>
                    <div class="pop_txt"><?php echo ($eachAcc->account_number); ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Cash : </span>
                    <div class="pop_txt"><?php echo $eachAcc->account_cash; ?></div>
                    </code>
                    <!--line--> 
                    <code> <span class="pop_title">Status : </span>
                    <div class="pop_txt"><?php echo $eachAcc->status; ?></div>
                    </code> 
                   <!--line--> 
                    <code> <span class="pop_title">Account Created : </span>
                    <div class="pop_txt"><?php echo $eachAcc->account_created; ?></div>
                    </code> 
                     <!--line--> 
                   
                   
                    	<?php
							//get_permission_text();
						?>
                      </ul>
                    </div>
                    </code>
                    </div></td>
                    </tr>
                    <?php
					}
					?>
                            
                </table>
            </div>
        </div>
        <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>
       
        <?php //action_buttons('addnewcustomer',$cnt); ?>
        <!--<input type="submit" class="send_icon" value=""/>-->
      </form>
    </div>
      <!-- END PAGE -->  
   </div>
</section>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
