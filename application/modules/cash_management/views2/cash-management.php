<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
      <div class="title"> <span><?php breadcramb(); ?></span> </div>
      <?php error_hander($this->input->get('e')); ?>
      <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off" onsubmit="return false;">
        <div class="form">
          <div class="CSSTableGenerator " id="printdiv" >
                <div style="float: left; margin-left: 12px; margin-top: 4px;">
                    From Date 
                    <input type="text" id="from_date_filter" value=""> 
                    To Date 
                    <input type="text" id="to_date_filter" value="" >
                    <input type="button" onclick="updateBranchCashDataTableDiv();" value="Search">
                </div>
                <table width="100%" align="left" id="usertable">
                
                	<thead>
                    <tr>
                      <th width="1%" id="no_filter"><label for="checkbox"></label></th>
                      <th width="3%">Branch Name</th>
                        <th width="11%" >Branch Cash</th>
                        <th width="11%" >Branch</th>
                        <th width="11%">Debit</th>
                        <th width="11%">Credit</th>
                        <th width="11%">Created</th>
                        <th width="7%" id="no_filter">&nbsp;</th>
                    </tr>
                    </thead>
              		<tfoot>
                    <tr>
                      <td style="background-color:#afe2ee"></td>
                      <td  style="background-color:#afe2ee">
                      <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td  style="background-color:#afe2ee">
                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td  style="background-color:#afe2ee">
                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td  style="background-color:#afe2ee">
                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                       <td  style="background-color:#afe2ee">
                     <input type="text" name="textfield" id="textfield"  class="flex_feild"/>
                      </td>
                        
                       <td  style="background-color:#afe2ee"></td>
                 
                       <td  style="background-color:#afe2ee">&nbsp;</td>
                  </tr>
                  </tfoot>
                    
                    <?php
					foreach($CashManagement as $eachBranch){
						//echo "<pre>";
						//print_r($eachBranch);
					?>
                    <tr>
                      <td><input type="checkbox" name="checkbox2" id="checkbox2" /></td>
                      <td><?php echo $eachBranch->branch_cash_name;?></td>
                        <td><?php echo $total = $eachBranch->totaldebit-$eachBranch->totalcredit;?></td>
                        <td><?php echo $eachBranch->branch_cash_name;?></td>
                        <td><?php echo ($eachBranch->totaldebit!='')?$eachBranch->totaldebit:0;?></td>
                        <td><?php echo ($eachBranch->totalcredit!='')?$eachBranch->totalcredit:0;?></td>
                        
                        <td><?php echo $eachBranch->created;?></td>
                        
                        
                  
                      <td> <a id="<?php echo $eachBranch->branch_id; ?>" href='#' class='basic'><img src="<?php echo base_url(); ?>images/internal/i_info.png" width="28" height="18" border="0" /></a>
                      
                      <a id="" href='<?php echo base_url().'cash_management/index/'.$eachBranch->branchId; ?>'>View Transaction</a> 
                      
                      <?php /*?><a id="" href='<?php echo base_url().'transaction_management/index/'.$eachBranch->branch_id; ?>'>View Transaction</a><?php */?>          
                  <!-- modal content -->
                  
                  <div id="basic-modal-content" class="dig<?php echo $eachBranch->branch_id; ?>">
                    <h3><?php echo $eachBranch->branch_name; ?><br />
                      Branch Code  : #<?php echo $eachBranch->branch_code ?></h3>
                    
                    <code> <span class="pop_title">Cash : </span>
                    <div class="pop_txt"><?php echo $total = $eachBranch->totaldebit-$eachBranch->totalcredit;?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Address: </span>
                    <div class="pop_txt"><?php echo ($eachBranch->branch_address); ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Status : </span>
                    <div class="pop_txt"><?php echo ($eachBranch->status=="A")?"Active":"Deactive";?></div>
                    </code>
                    <!--line--> 
                    <code> <span class="pop_title">Contact Person : </span>
                    <div class="pop_txt"><?php echo $eachBranch->contact_person; ?></div>
                    </code>
                     <!--line--> 
                    <code> <span class="pop_title">Total Debit : </span>
                    <div class="pop_txt"><?php echo ($eachBranch->totaldebit!='')?$eachBranch->totaldebit:0;?></div>
                    </code>
                    <!--line--> 
                    <code> <span class="pop_title">Total Credit : </span>
                    <div class="pop_txt"><?php echo ($eachBranch->totalcredit!='')?$eachBranch->totalcredit:'0';?></div>
                    </code>  
                   <!--line--> 
                    <code> <span class="pop_title">Account Created : </span>
                    <div class="pop_txt"><?php echo $eachBranch->created; ?></div>
                    </code> 
                     <!--line--> 
                   
                   
                    	<?php
							//get_permission_text();
						?>
                      </ul>
                    </div>
                    </code>
                    </div></td>
                    </tr>
                    <?php
					}
					?>
                            
                </table>
            </div>
        </div>
        <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>
       
        <?php //action_buttons('addnewcustomer',$cnt); ?>
        <!--<input type="submit" class="send_icon" value=""/>-->
      </form>
    </div>
      <!-- END PAGE -->  
   </div>
</section>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(function() {
$( "#from_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to_date_filter" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from_date_filter" ).datepicker( "option", "maxDate", selectedDate );
}
});
});
</script>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
