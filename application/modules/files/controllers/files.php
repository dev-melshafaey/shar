<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Files extends CI_Controller {
    /*
     * Properties
     */

    private $_data = array();

//----------------------------------------------------------------------
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        //load Home Model
        $this->load->model('files_model', 'files');
        $this->load->model('users/users_model', 'users');
        $this->lang->load('main', get_set_value('site_lang'));
        $this->_data['udata'] = userinfo_permission();
    }

//----------------------------------------------------------------------

    /*
     * Expensis Listing Page
     */
    public function index() {
        // Load Home View
        //$this->load->view('expenses', $this->_data);
        $this->_data['inside2'] = 'files';
        $this->_data['inside'] = 'add-files';
        $this->load->view('common/tabs', $this->_data);
    }

    public function view_all() {

        $this->index();
    }

    function all_files($id = null) {


        $this->_data['id'] = $id;
        $this->_data['inside2'] = 'all_files';
        $this->_data['inside'] = 'add-files';
        $this->load->view('common/tabs', $this->_data);
    }

    function categories() {


        $this->_data['inside2'] = 'categories';
        $this->_data['inside'] = 'add_categories';
        $this->load->view('common/tabs', $this->_data);
    }

//----------------------------------------------------------------------

    /*
     * Add New Expensis Form
     */
    public function add_files($id = null,$fid=null) {
     if ($id) {
           
            if ($this->input->post()) {
                
                
                 
                $postData = $this->input->post();

                foreach($postData['files'] as $row){
                    
                        $data['document_title'] = serialize($postData['document_title']);
                        $data['company_id'] = $postData['companyid'];
                        $data['ccat'] = $postData['ccat'];
                        $data['document_from'] = $postData['document_from'];
                        $data['document_to'] = $postData['document_to'];
                        $data['renew'] = $postData['renew'];
                        $data['document_image'] = $row;
                        
                        //print_r($data);
                        $this->db->insert('compnay_documents', $data);
                        //$this->db->where('pid', $fid);
                        //$this->db->update('compnay_documents', $data);
                }



                //die();    

                //$this->db->where('pid', $id);
                //$this->db->update('pages', $data);
                //redirect('pages_managment/viewall/' . $id . '/?e=11');


                //print_r($postData);
            }
            //$this->load->view('add-expenses', $this->_data);


            $this->_data['exdata'] = $this->files->getfile($fid);

            //$this->load->view('add-expenses', $this->_data);
            $this->_data['inside2'] = 'all_files';
            $this->_data['inside'] = 'add-files';
            $this->_data['fid'] = $fid;
            $this->_data['id'] = $id;
            $this->load->view('common/tabs', $this->_data);
        } else {
            //echo '<pre>';
            $postData = $this->input->post();

                foreach($postData['files'] as $row){
                    
                        $data['document_title'] = serialize($postData['document_title']);
                        $data['company_id'] = $postData['companyid'];
                        $data['ccat'] = $postData['ccat'];
                        $data['document_from'] = $postData['document_from'];
                        $data['document_to'] = $postData['document_to'];
                        $data['renew'] = $postData['renew'];
                        $data['document_image'] = $row;
                        
                        //print_r($data);
                        $this->db->insert('compnay_documents', $data);
                }

            //die();    
            //$this->db->insert('pages', $data);
            redirect('files/view_all/?e=10');
        }
    }
    function deleteitem(){
        
        $this->db->where('document_id',$_POST['pid']);
        $this->db->delete('compnay_documents');
    }
    function add_cat($id = null) {

        $userData = $this->users->get_user_detail($userid);
        //print_r($userData);

        if ($id) {
            if ($this->input->post()) {
                $postData = $this->input->post();


                $data['ftitle'] = $postData['ftitle'];





                $this->db->where('fid', $id);
                $this->db->update('files_categories', $data);
                redirect('files/categories/' . $id . '/?e=11');


                //print_r($postData);
            }
            //$this->load->view('add-expenses', $this->_data);

            if ($id) {
                $this->_data['exdata'] = $this->files->get_cat($id);
                $this->_data['fid'] = $id;
            }
            //$this->load->view('add-expenses', $this->_data);
            $this->_data['inside2'] = 'categories';
            $this->_data['inside'] = 'add_categories';
            $this->load->view('common/tabs', $this->_data);
        } else {
            $postData = $this->input->post();


            $data['ftitle'] = $postData['ftitle'];






            $this->db->insert('files_categories', $data);
            redirect('files/categories/?e=10');
        }
    }

    function getAutoSearch() {
        // Data could be pulled from a DB or other source


        $term = trim(strip_tags($_GET['term']));
        $customers = $this->files->getExpenseByTitle($term);
        // Cleaning up the term
        // Rudimentary search
        foreach ($customers as $eachloc) {
            // Add the necessary "value" and "label" fields and append to result set
            $eachloc['value'] = $eachloc['id'];
            $eachloc['label'] = "{$eachloc['cus']}";
            $matches[] = $eachloc;
        }
        // Truncate, encode and return the results
        $matches = array_slice($matches, 0, 5);
        print json_encode($matches);
    }

//----------------------------------------------------------------------

    function uploadfile_files() {
        //$uploaddir = '../../img/'; 
        
        //$uploads = $this->uploading->uploading("uploadfile", "events");
        $path = APPPATH . '../uploads/compnay_documents/';
        $targetFolder = $this->config->item('bath'); // Relative to the root
        $file_name = upload_file('Filedata', $path, true, 50, 50);
        //$file2 = "/home/geniuscu/public_html/portfolio/images/thumbs/".$_FILES['uploadfile']['name']; 
        //$file2 = realpath(APPPATH."../uploads/".$folder.$_FILES['uploadfile']['name']); 
        //$file = "/home/geniuscu/public_html/portfolio/images/".$_FILES['uploadfile']['name']; 
        //$file = realpath(APPPATH."../uploads/aqars".$_FILES['uploadfile']['name']); 
        //if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file) ) { 
        //var_dump($file_name);
        if ($file_name) {
            //copy($file,$file2);
            // resize('200','97',$file2);
            $array=array('ex'=>$this->strafter($file_name,'.'),'filename'=>$file_name);
            $re=  json_encode($array);
            print_r($re);
            
        } else {
            echo "error";
        }
    }
    function strafter($string, $substring) {
      $pos = strpos($string, $substring);
      if ($pos === false)
       return $string;
      else 
       return(substr($string, $pos+strlen($substring)));
    }
    function uploadify2() {

        $targetFolder = $this->config->item('bath'); // Relative to the root

        $verifyToken = md5('unique_salt' . $_POST['timestamp']);

        if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
            $targetFile = rtrim($targetPath, '/') . '/' . $_FILES['Filedata']['name'];

            // Validate the file type
            $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // File extensions
            $fileParts = pathinfo($_FILES['Filedata']['name']);

            if (in_array($fileParts['extension'], $fileTypes)) {

                //$file=move_uploaded_file($tempFile,$targetFile);
                $path = APPPATH . '../uploads/compnay_documents/';
                $file_name = upload_file('Filedata', $path, true, 50, 50);
                /*
                  $sdata['document_date'] = post('document_date');
                  $sdata['document_title'] = post('document_title');
                  $sdata['company_id'] = post('company_id_up');
                  $sdata['document_image'] = $file_name;
                 */
                //var_dump($sdata);
                //$this->company->inserData('compnay_documents', $sdata);
                //print_r($file_name);
                //echo $tempFile."<br/>";
                //echo $targetFile;

                echo $file_name;
            } else {
                echo '<script>Invalid file type</script>';
            }
        }

        echo '<script>alert("Invalid file type")</script>';
    }

}
