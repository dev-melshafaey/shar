
<script src="<?php echo base_url() ?>js/uploading/jquery.uploadify.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>js/uploading/uploadify.css">

<div id="main-content" class="main_content">
    <!--    <div class="title title alert alert-info">
            <span><?php breadcramb(); ?> >> <?php echo lang('add-edit2') ?></span>
        </div>-->

    <div class="notion title title alert alert-info">*  <?php echo lang('mess1') ?></div>

    <?php error_hander($this->input->get('e')); ?>

    <form action="<?php echo base_url() ?>files/add_files/<?php echo $id;?>/<?php echo $fid?>"  method="post" class="sample-form">




        <div class="form mycontent">
            <div class="">



                <div class="g4 form-group">
                    <label class="col-lg-3"><?php echo lang('Company-Name') ?></label>
                    <div class="ui-select col-md-8" >
                        <div class="">
                            <?php company_dropbox('companyid', $exdata->company_id); ?>
                            <span class="arrow arrowselectbox">&amp;</span>
                        </div>
                    </div>
                </div>



                <div class="g3 form-group">
                    <label class=""><?php echo lang('title') ?></label>
                    <div class="">
                        <input name="document_title[ar]" id="title" type="text"  value="<?php echo (post('document_title')) ? post('document_title') : _s($exdata->document_title,'ar'); ?>" class="required  valid form-control"/>
                    </div>
                </div>



                <div class="g3 form-group" id="div_banks">
                    <label class="text-warning"><?php echo lang('cat') ?> </label>
                    <div class="">
                        <div class="ui-select" style="width:100%">
                            <div class="">
                                <?php files_categories('ccat', $exdata->ccat, 'english', ''); ?>
                                <span class="arrow arrowselectbox">&amp;</span>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="g4 form-group">
                    <label class=""><?php echo lang('expireddate') ?></label>
                    <div class="g5" style="margin-top: 0px;">
                        <input name="document_from"  id="" type="text"  value="<?php echo (post('document_from')) ? post('document_from') : $exdata->document_from; ?>" class="required  valid form-control datapic_input"/>

                    </div>
                    <div class="g5" style="margin-top: 0px;">
                        <input name="document_to"  id="" type="text"  value="<?php echo (post('document_to')) ? post('document_to') : $exdata->document_to; ?>" class="required  valid form-control datapic_input"/>
                    </div>
                </div>


                <div class="g2 form-group">
                    <label class=""><?php echo lang('renew') ?></label>
                    <div class="defaultP">

                        <input type="checkbox" id="" class="" value="1" name="renew" style="width: 10%;">

                    </div>
                </div>






                <br  clear="all"/>


                <div class="">
                    <style>
                        .photo-box {
                            margin: 0px 10px 10px 0px;
                            border: 1px solid #CCC;
                            padding: 5px;
                            background: #fff;
                        }

                    </style>


                                <?php if($exdata->document_image):?>
                                    <div style="float:right;margin-left:20px" class="photo-box">
                                        <!--<input type="hidden" name="files[]" class="imgfiles<?= $exdata->document_id ?>" value="<?=$exdata->document_image ?>" id="imgfiles" />-->
                                        <?php $getex=strafter($exdata->document_image,'.')?>
                                        <?php if($getex == "jpg" || $getex=="png" || $getex=="jpeg" || $getex=="gif"):?>
                                            <img src="<?= base_url() ?>uploads/compnay_documents/<?= $exdata->document_image ?>" alt="" class="image_mult<?= $exdata->document_id ?>" style="width: 150px;height: 130px;" />
                                        <?php else:?>
                                            <div class="image_mult<?= $exdata->document_id ?>"><span class="icon-createfile  "></span><br /><a href="<?= base_url() ?>uploads/compnay_documents/<?php echo $exdata->document_image?>"><?php echo $exdata->document_image?></a></div>
                                        <?php endif;?>    
                                        <div ><a href="#X" class="2remove_image<?= $exdata->document_id ?>" style="color:red">X</a></div>

                                    </div>

                            
                                <script type="text/javascript" >
                                    $('.2remove_image<?= $exdata->document_id ?>').click(function () {

                                        $('.image_mult<?= $exdata->document_id ?>').remove();
                                        $('.remove_image<?= $exdata->document_id ?>').remove();
                                        $('.imgfiles<?= $exdata->document_id ?>').remove();
                                        
                                        $.ajax({
                                            url: "<?php echo base_url() ?>files/deleteitem",
                                            dataType: 'json',
                                            type: 'post',
                                            data: {pid: <?=$exdata->document_id ?>},
                                            cache: false,
                                            success: function (data)
                                            {

                                            }
                                        });

                                    });
                                </script>
                            <?php endif;?>



                    <br clear="all"/>

                    <br clear="all"/>			
                    <script type="text/javascript" src="<?php echo base_url(); ?>durarthem/ofiles/js_up/ajaxupload.3.5.js" ></script>
                    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>durarthem/ofiles/js_up/styles.css" />
                    <script type="text/javascript" >
                                $(function () {
                                    var btnUpload = $('#upload');
                                    var status = $('#status');
                                    new AjaxUpload(btnUpload, {
                                        //action: 'cms-content/controllers/upload-file.php',
                                        action: '<?= base_url() ?>files/uploadfile_files',
                                        name: 'Filedata',
                                        onSubmit: function (file, ext) {
                                            /*if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                                                // extension is not allowed 
                                                status.text('Only JPG, PNG or GIF files are allowed');
                                                return false;
                                            }*/
                                            status.text('جاري التحميل...');
                                        },
                                        onComplete: function (file, response, ext) {
                                            //On completion clear the status
                                            status.text('');
                                            var redata=$.parseJSON(response);
                                            //Add uploaded file to list
                                            //alert(ext);
                                            if (redata.ex === "jpg" || redata.ex ==="png" || redata.ex==="jpeg" || redata.ex==="gif") {

                                                $('<div style="width:110px;float:right;margin-left:20px;word-break: break-all;" class="photo-box"></div>').appendTo('#files').html('<i class="icon-remove-sign" style="color:red;font-size:20px;position: absolute;"></i>\n\
                                                    <img src="<?= base_url() ?>uploads/compnay_documents/' + redata.filename + '" alt="" style="width: 100px;height: 100px;"  />\n\
                                                <br /><input type="hidden" value="' + redata.filename + '" name="files[]"/>').addClass('success');
                                            } else {
                                                $('<div style="width:110px;float:right;margin-left:20px;word-break: break-all;" class="photo-box"></div>').appendTo('#files').html('\n\
                                                <span class="icon-createfile"></span><br /><a href="<?= base_url() ?>uploads/compnay_documents/' + redata.filename + '">' + redata.filename + '</a>\n\
                                                     <input type="hidden" value="' + redata.filename + '" name="files[]"/>').addClass('success');

                                            }
                                        }
                                    });

                                });
                    </script>

                    <div id="upload" ><span>تحميل الملفات المرفقة<span></div><span id="status" ></span>


                                <div id="files" ></div>

                                <br clear="all"/>

                                </div>


                                <!--                <div class="g16">
                                                    <form>
                                                        <div id="queue"></div>
                                                        <input id="file_upload" name="file_upload" type="file" multiple="true">
                                                    </form>
                                
                                                    <div id="getfiles_logo"> ngRepeat: file in queue </div> 
                                                </div> -->


                                <br  clear="all"/>
                                <div class="g16">
                                    <label class=""><?php echo lang('Notes') ?></label>
                                    <div class="">
                                        <textarea name="Notes" cols="" rows="" class="form-control"><?php echo (post('Notes')) ? post('Notes') : $exdata->Notes; ?></textarea>

                                    </div>
                                </div>
                                <br  clear="all"/>
                                <div class="" align="">
                                    <input type="hidden" name="expense_id" id="expense_id" />
                                    <input name="" type="submit" class="btn-glow primary  submit_btn g3 green" value="<?php echo lang('Add') ?>" />
                                    <input name="" type="reset" class="btn-glow primary  g3 gray" value="<?php echo lang('Reset') ?>" />
                                </div>
                                </div>  
                                <!--end of raw field-box-->
                                </div>
                                </form>
                                <!-- END PAGE -->  
                                </div>
                                <!-- END PAGE -->  

                                <script type="text/javascript">

<?php $timestamp = time(); ?>
                                    $(function () {
                                        var cid = $("#company_idu2").text();

                                        $('#file_upload').uploadify({
                                            'formData': {
                                                'mustafa': '1',
                                                'timestamp': '<?php echo $timestamp; ?>',
                                                'token': '<?php echo md5('unique_salt' . $timestamp); ?>'
                                            },
                                            'swf': '<?php echo base_url() ?>js/uploading/uploadify.swf',
                                            'uploader': '<?php echo base_url() ?>files/uploadify2',
                                            'itemTemplate': '<div id="${fileID}" class="uploadify-queue-item">\
                                                                <div class="cancel">\
                                                                    <a href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')">X</a>\
                                                                </div>\
                                                                <span class="fileName">${fileName} (${fileSize})</span><span class="data"></span>\
                                                            </div>',
                                            'onUploadComplete': function (file) {
                                                //alert('The file ' + file.name + ' finished processing.');

                                                $("#getfiles_logo").append("<input type='text' name='file' value='" + file.name + "' />");
                                                //$("#getfiles_logo").load(config.BASE_URL + "company_management/getfiles/"+$("#company_id").val());

                                            },
                                            'onUploadError': function (file, errorCode, errorMsg, errorString) {
                                                alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
                                            }
                                        });
                                    });

                                </script>        