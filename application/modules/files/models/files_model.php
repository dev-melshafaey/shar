<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Files_model extends CI_Model {
    /*
     * Properties
     */

//----------------------------------------------------------------------

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();

        //Load Table Names from Config
    }

    public function get_list() {
        //$this->db->select('module_name,moduleid');
        $this->db->select('compnay_documents.*,bs_company.companyid,bs_company.company_name,
                COUNT(compnay_documents.document_id) AS files_count');

        $this->db->join('compnay_documents', 'compnay_documents.company_id=bs_company.companyid', 'left');
        //$this->db->join('compnay_documents_categories','ie.expense_id= an_expenses.id','left');

        $this->db->group_by('bs_company.companyid');
        $this->db->order_by("bs_company.companyid", "ASC");

        $query = $this->db->get('bs_company');
        return $query->result();
    }

    public function get_list_files($id) {
        //$this->db->select('module_name,moduleid');
        /*
          $this->db->select('compnay_documents.*,bs_company.*
          ');

          $this->db->join('bs_company',"bs_company.companyid='". $id."'  ",'left');
          //$this->db->join('compnay_documents_categories','ie.expense_id= an_expenses.id','left');

          //$this->db->group_by('an_expenses.id');


          $this->db->where("compnay_documents.company_id", $id);
          //$this->db->group_by('compnay_documents.company_id');
          $this->db->order_by("compnay_documents.document_id", "ASC");

          $query = $this->db->get('compnay_documents');
         */


        $sql = "SELECT compnay_documents.*,bs_company.*
                 FROM compnay_documents  LEFT JOIN bs_company ON bs_company.companyid='" . $id . "'   WHERE compnay_documents.company_id='" . $id . "';";
        $q = $this->db->query($sql);


        return $q->result();
    }

    public function get_list_cat() {
        //$this->db->select('module_name,moduleid');
        $this->db->from('files_categories');
        $this->db->order_by("fid", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_ex($id = null) {
        //$this->db->select('module_name,moduleid');
        $this->db->from('an_expenses');
        $this->db->where('id', $id);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    public function get_cat($id = null) {
        //$this->db->select('module_name,moduleid');
        $this->db->from('files_categories');
        $this->db->where('fid', $id);
        $this->db->order_by("fid", "ASC");
        $query = $this->db->get();
        return $query->row();
    }
    public function getfile($id = null) {
        //$this->db->select('module_name,moduleid');
        $this->db->from('compnay_documents');
        $this->db->where('document_id', $id);
        $this->db->order_by("document_id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function searchexpenseByTitle($search_name) {
        $sql = "SELECT * FROM an_expenses_charges AS ec WHERE ec.expense_title = '" . $search_name . "';";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function getExpenseByTitle($search_name) {
        $sql = "SELECT * FROM an_expenses_charges AS ec WHERE ec.expense_title LIKE '%" . $search_name . "%';";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            $locs = $q->result();
            foreach ($locs as $eachLoc) {
                $customers[] = array('cus' => $eachLoc->expense_title, 'id' => $eachLoc->id);
            }
        } else {
            return '0';
        }


        return $customers;
    }

    //shahzaib function 
    //9/11/2015
    function getPurchase() {

        $this->db->from('an_purchase');
        $this->db->order_by("purchase_id", "DESC");
        $query = $this->db->get();
        return $query->result();
    }

//----------------------------------------------------------------------	
}

?>