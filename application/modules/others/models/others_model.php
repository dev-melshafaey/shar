<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class others_model extends CI_Model {
    /*
     * Properties
     */

//----------------------------------------------------------------------

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();
        
        //Load Table Names from Config
    }

    function add_asssets() {
        
    }

    function getAllCapitals() {
        $sql = "SELECT c.*,u.`username` FROM `capitals` AS c INNER JOIN `bs_users` AS u ON u.`userid` = c.`user_id`";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $q->result();
        } else {
            return '0';
        }
    }

    function getAllLiabilties() {
        $sql = "SELECT 
  ll.*,
  SUM(pl.`payment_amount`) AS total_payments_amount,
  COUNT(pl.`liabilty_id`) total_payments,
  llp.`party_name` 
FROM
  `loan_liabalities` AS ll 
  INNER JOIN `loan_liabilities_parties` AS llp 
    ON llp.`party_id` = ll.`party_id` 
  LEFT JOIN `payments_liablities` AS pl 
    ON pl.`liabilty_id` = ll.`libality_id` 
GROUP BY pl.`liabilty_id`";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $q->result();
        } else {
            return '0';
        }
    }

    function getAllpaymentsBylibId($id, $type) {

        if ($type != "") {
            $sql = "SELECT 
					  ll.*,
					SUM(pl.`payment_amount`)  AS total_payments,
					  llp.`party_name` 
					FROM
					  `loan_liabalities` AS ll 
					  INNER JOIN `loan_liabilities_parties` AS llp 
						ON llp.`party_id` = ll.`party_id` 
					  LEFT JOIN `payments_liablities` AS pl 
						ON pl.`liabilty_id` = ll.`libality_id` 
					WHERE ll.`libality_id` = '" . $id . "'
					GROUP BY ll.`libality_id`";
            $q = $this->db->query($sql);

            if ($q->num_rows() > 0) {

                return $q->row();
            } else {
                return '0';
            }
        } else {
            $sql = "SELECT 
			  ll.*,
			pl.`payment_amount`,  
			  llp.`party_name` 
			FROM
			  `loan_liabalities` AS ll 
			  INNER JOIN `loan_liabilities_parties` AS llp 
				ON llp.`party_id` = ll.`party_id` 
			  LEFT JOIN `payments_liablities` AS pl 
				ON pl.`liabilty_id` = ll.`libality_id` 
			WHERE pl.`liabilty_id` = '" . $id . "'";

            $q = $this->db->query($sql);

            if ($q->num_rows() > 0) {

                return $q->result();
            } else {
                return '0';
            }
        }
    }

    function getAllpaymentsByloanId($id, $type) {

        if ($type != "") {
            $sql = "  SELECT 
							ll.*,
							SUM(pl.`payment_amount`)  AS total_payments,
							u.`username`
						  FROM
							`loan_advance` AS ll 
							INNER JOIN `an_users` AS u 
							  ON u.`userid` = ll.`user_id` 
							LEFT JOIN `loan_advance_received` AS pl 
							  ON pl.`loan_advance_id` 
						  WHERE ll.`loan_advance_id` = '" . $id . "'
						  GROUP BY ll.`loan_advance_id`";
            $q = $this->db->query($sql);

            if ($q->num_rows() > 0) {

                return $q->row();
            } else {
                return '0';
            }
        } else {
            $sql = "SELECT 
				ll.*,
				pl.`payment_amount`,
				u.`username`
			  FROM
				`loan_advance` AS ll 
				INNER JOIN `an_users` AS u 
				  ON u.`userid` = ll.`user_id` 
				LEFT JOIN `loan_advance_received` AS pl 
				  ON pl.`loan_advance_id` 
			  WHERE ll.`loan_advance_id` = '" . $id . "'";

            $q = $this->db->query($sql);

            if ($q->num_rows() > 0) {

                return $q->result();
            } else {
                return '0';
            }
        }
    }

    function getAllloans() {
        $sql = "SELECT 
			  ll.*,
			  SUM(pl.`payment_amount`) AS total_received_amount,
			  COUNT(pl.`loan_advance_id`) total_received,
			  u.`username`
			FROM
			  `loan_advance` AS ll 
			  INNER JOIN `bs_users` AS u ON u.`userid`  = ll.`user_id`
			  LEFT JOIN `loan_advance_received` AS pl 
				ON pl.`loan_advance_id` = ll.`loan_advance_id` 
			GROUP BY pl.`loan_advance_id`";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $q->result();
        } else {
            return '0';
        }
    }

    function getUserByTitle($search_name) {
        $sql = "SELECT * FROM `an_users` AS u WHERE  u.`fullname` LIKE  '%" . $search_name . "%';";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            $locs = $q->result();
            foreach ($locs as $eachLoc) {
                $customers[] = array('cus' => $eachLoc->fullname, 'id' => $eachLoc->userid);
            }
        } else {
            return '0';
        }


        return $customers;
    }

    function getPartyByTitle($search_name) {
        $sql = "SELECT * FROM `loan_liabilities_parties` AS llp WHERE llp.`party_name` LIKE '%" . $search_name . "%';";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            $locs = $q->result();
            foreach ($locs as $eachLoc) {
                $customers[] = array('cus' => $eachLoc->party_name, 'id' => $eachLoc->party_id);
            }
        } else {
            return '0';
        }


        return $customers;
    }

//----------------------------------------------------------------------	
}

?>