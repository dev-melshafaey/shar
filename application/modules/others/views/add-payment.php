<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
      <div class="title">Add Payment</div>

<form action=""  method="post">
<div class="form">
<div class="raw">
<div class="form_title">Party Name</div>
<div class="form_field">
<input name="party_name" id="party_name" type="text"  class="formtxtfield" value="<?php echo $payment_data->party_name; ?>" disabled="disabled"/>
<input name="party_id" id="party_id"  type="hidden"  class="formtxtfield" value="<?php echo $payment_data->party_id; ?>"/>
<input name="libality_id" id="libality_id"  type="hidden"  class="formtxtfield" value="<?php echo $payment_data->libality_id; ?>"/>
  </div>
  </div>
<div class="raw">
<div class="form_title">Total Amount</div>
<div class="form_field">
    <input type="text"  class="formtxtfield_small"  value="<?php echo $payment_data->libality_amount; ?>" disabled="disabled"/>
    <span>RO.</span>
</div>
</div>
<div class="raw">
<div class="form_title">Total Remaining</div>
<div class="form_field">
    <input type="text"  class="formtxtfield_small"  value="<?php echo $payment_data->libality_amount-$payment_data->total_payments; ?>" disabled="disabled"/>
    <span>RO.</span>
</div>
</div>  
<div class="raw">
<div class="form_title">Amount</div>
<div class="form_field">
    <input name="amount_value" id="amount_value" type="text"  class="formtxtfield_small" />
    <span>RO.</span>
</div>
</div>
<div class="raw">
<div class="form_title">Date</div>
<div class="form_field">
    <input name="liablity_date" id="liablity_date" type="text"  class="formtxtfield_small"/>
</div>
</div>
 
<div class="raw">
<div class="form_title">Notes</div>
<div class="form_field">
   <textarea name="" cols="" rows="" class="formareafield"></textarea>

</div>
<div class="raw form-group">
                    <div class="form_title">Payment Method:</div>
                    <div class="form_field">
					<div class="defaultP">
                       <input type="radio" name="payment_method" class="payment_by" value="bank" id="payment_type1"> <label for="payment_type1">Bank</label>
					<input type="radio" name="payment_method" class="payment_by" value="cash" id="payment_type2"> <label for="payment_type2">Cash</label></div>
                    </div>
                    
                </div>
                
<div class="raw form-group bank_options" id="div_banks" style="display:none;">
                    <div class="form_title">Banks </div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
                          <?php company_bank_dropbox('bank_id','','english',' onchange="getBankAccounts();" ');?>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="raw form-group bank_options" style="display:none" id="div_accounts">
                    <div class="form_title">Accounts </div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select" id="div_accounts_responce">
                          
                        </div>
                      </div>
                    </div>
                </div>
                
                <div class="raw form-group bank_options">
                <div class="form_title">Payment Type</div>
                    <div class="form_field">
                        <div class="dropmenu">
                            <div class="styled-select">
                                <select name="payment_type" id="payment_type" onchange="BankOrCashToggle();">
                                    <option value="0">Select Payment Way</option>
                                    <option value="withdraw">Withdraw</option>
                                    <option value="cheque">Cheque</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="raw form-group bank_options" style="display:none" id="cheque_div_date">
                    <div class="form_title">Cheque Date </div>
                    <div class="form_field">
                        <input name="cheque_date" id="cheque_date" type="text"  class="formtxtfield dp"/>
                        
                    </div>
                </div>
                <div class="raw form-group bank_options" style="display:none" id="cheque_div_number">
                    <div class="form_title">Cheque Number </div>
                    <div class="form_field">
                        <input name="cheque_number" id="cheque_nummber" type="text"  class="formtxtfield"/>
                        
                    </div>
                </div>
</div>
<div class="raw" align="center">
		  <input name="" type="submit" class="submit_btn" value="Add" />
           <input name="" type="reset" class="reset_btn" value="Reset" />
                    </div>
<!--end of raw-->
</div>
</form>
<!-- END PAGE -->  
</div>
      <!-- END PAGE -->  
   </div>
</section>
<script type="text/javascript">
function swapval(){
	uid = $("#party_name").val();
	uname = $("#party_id").val();
	$("#party_id").val(uid);
	$("#party_name").val(uname);		
}
$(function() {
        $('#liablity_date').datepicker({dateFormat: 'yy-mm-dd'});
		$('#cheque_date').datepicker({dateFormat: 'yy-mm-dd'});
	    });
$(document).ready(function(){
	//alert('ready');
			var ac_config = {
		source: "<?php echo base_url();?>others/getAutoLiablitiesSearch",
		select: function(event, ui){
			$("#party_name").val(ui.item.cus);
			$("#party_id").val(ui.item.cus);
			console.log(ui);
			//swapme();
			setTimeout('swapval()',500);
		},
		minLength:1
	};
	
	$("#party_name").autocomplete(ac_config);
	$(".payment_by").click(function(){
			p_id = $(this).val();
			if(p_id == 'cash'){
				$(".bank_options").hide();
			}
			else{
				$(".bank_options").show();
			}	
	})
});
</script>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
