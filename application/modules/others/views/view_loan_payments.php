<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>
<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
      <div class="title"> <span><?php breadcramb(); ?></span><input type="submit" class="print_icon_up" value=""/>
<input type="submit" class="delete_icon_up" value=""/>
<input type="submit" class="edit_icon_up" value=""/>
<input type="submit" class="add_icon_up" value=""/>
<input type="button" value="Add Payment" class="" style="font-weight: bold; font-size: 13px; border-top-width: 0px; border-bottom-width: 0px; border-left-width: 0px; height: 57px; cursor: pointer; float: right; border-radius: 5px;" id="payment_btn">
<input type="hidden" value="<?php echo $id; ?>" id="selected_id" name="selected_id" /> 
</div>
		<div><?php
			$total_amount = '';
				if(!empty($payment_data)){
					$total_amount = $payment_data[0]->loan_amount;
				}
			echo "<h3>Total Amount ".$total_amount."</h3>";	
		?>
        </div>
      <?php error_hander($this->input->get('e')); ?>
      <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">
        <div class="form">
          <div class="CSSTableGenerator " id="printdiv" >
          	<table width="100%" align="left" id="usertable">
              <thead>
              <tr>
                <th width="12%" >Receipt Amount</th>
                <th width="12%" >Remaining Amount</th>
                <th width="12%">Date</th>
                <th width="7%" id="no_filter">&nbsp;</th>
              </tr>
              </thead>
              <tfoot>
              <tr>
              <td  style="background-color:#afe2ee"><input type="checkbox" name="checkbox" id="checkboxall" /></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"></td>
				<td style="background-color:#afe2ee"></td>
              </tr>
              </tfoot>
              <?php
			  	$cnt = 0;
				
				
              	foreach($payment_data as $userdata) {
					//echo "<pre>";
					//print_r($userdata);
					$cnt++;
			  ?>
              <tr>
                <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $userdata->loan_advance_id; ?>" value="<?php echo $userdata->loan_advance_id; ?>" /></td>
                <td><?php echo $userdata->payment_amount; ?></td>
                <td><?php   $total_amount = $total_amount-$userdata->payment_amount; echo $total_amount;   ?></td>
                <td><?php echo $userdata->loan_date;   ?></td>
                <td>           
                  <!-- modal content -->
                  </td>
              </tr>
              <?php } ?>
            </table>
          </div>
        </div>
        <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>
     	<input type="submit" class="print_icon_bottom" value=""/>
<input type="submit" class="delete_icon_bottom" value=""/>
<input type="submit" class="edit_icon_bottom" value=""/>
<input type="submit" class="add_icon_bottom" value=""/>  
        <?php //action_buttons('addnewcustomer',$cnt); ?>
        <!--<input type="submit" class="send_icon" value=""/>-->
      </form>
    </div>
      <!-- END PAGE -->  
   </div>
</section>
<!-- End Section-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(function() {
$( "#from_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to_date_filter" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from_date_filter" ).datepicker( "option", "maxDate", selectedDate );
}
});

$("#payment_btn").click(function(){
			//	alert('click');
				s_id = $("#selected_id").val();
					if(s_id !=""){
							
						window.top.location = '<?php echo base_url(); ?>index.php/others/add_payment_loan/'+s_id;										
					}
			});


});
</script>
<!--footer-->
<?php $this->load->view('common/footer');?>
