<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Others extends CI_Controller {
    /*
     * Properties
     */

    private $_data = array();

//----------------------------------------------------------------------
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        //load Home Model
        $this->load->model('others_model', 'others');
        $this->load->model('users/users_model', 'users');
        $this->lang->load('main', get_set_value('main_lang'));
        $this->_data['udata'] = userinfo_permission();
    }

//----------------------------------------------------------------------

    /*
     * Home Page
     */
    public function index() {
        // Load Home View
        $this->load->view('quotation');
    }

    function add_libalities() {

        if ($this->input->post()) {
            //	  		echo "<pre>";
            $assets_data = $this->input->post();
            //			print_r($assets_data);
            $this->db->insert('assets', $assets_data);
            //			exit;
        }
        $this->load->view('add-li', $this->_data);
    }

    function add_capitals() {
        if ($this->input->post()) {

            $postData = $this->input->post();
            $method = $postData['payment_method'];
            $transaction_id = $this->add_bank_cash_transaction($method, $postData, 'receipt');

            //echo "<pre>";
            //print_r($postData);

            $capital_data['user_id'] = $postData['user_id'];
            $capital_data['capital_amount'] = $postData['amount_value'];
            $capital_data['capital_date'] = $postData['capital_date'];
            if (isset($postData['capital_id']) && $postData['capital_id'] != "") {
                $this->db->where('capital_id', $postData['capital_id']);
                $this->db->update('capitals', $capital_data);
            } else
                $this->db->insert('capitals', $capital_data);
        }

        //$this->load->view('add-capitals', $this->_data);
        $this->_data['inside'] = 'add-capitals';
        $this->load->view('common/main', $this->_data);
    }

    function add_liablitiy() {
        if ($this->input->post()) {
            $postData = $this->input->post();

            $userid = $this->session->userdata('userid');
            $user = $this->users->get_user_detail($userid);

            if ($postData['payment_method'] == 'bank') {
                $method = $postData['payment_method'];
                $postData['clearance_date'] = date('Y-m-d');
                $transaction_id = $this->add_bank_cash_transaction($method, $postData, 'receipt');
            } else {
                $cash_data['branch_id'] = $user->branchid;
                $cash_data['transaction_type'] = 'debit';
                $cash_data['value'] = $post['payment_amount'];
                $cash_data['created'] = date('Y-m-d');
                $this->db->insert('an_company_transaction', $cash_data);
                $transaction_id = $this->db->insert_id();
            }

            if ($postData['party_id'] == "") {
                $part_data['party_name'] = $postData['party_name'];
                $this->db->insert('loan_liabilities_parties', $part_data);
                $postData['party_id'] = $this->db->insert_id();
            }

            $lib_data['party_id'] = $postData['party_id'];
            $lib_data['libality_amount'] = $postData['amount_value'];
            $lib_data['liablity_date'] = $postData['liablity_date'];

            if (isset($postData['liability_id']) && $postData['liability_id'] != "") {
                $this->db->where('party_id', $postData['party_id']);
                $this->db->update('loan_liabalities', $lib_data);
            } else {
                $this->db->insert('loan_liabalities', $lib_data);
            }
        }

        //$this->load->view('add-liablity', $this->_data);
        $this->_data['inside'] = 'add-liablity';
        $this->load->view('common/main', $this->_data);
    }

    function add_loan_advance() {
        if ($this->input->post()) {
            $postData = $this->input->post();
            //echo "<pre>";
            //print_r($postData);
            //exit;
            $method = $postData['payment_method'];
            $postData['amount_value'] = $postData['loan_amount'];


            $userid = $this->session->userdata('userid');
            $user = $this->users->get_user_detail($userid);

            if ($postData['payment_method'] == 'bank') {
                $postData['clearance_date'] = date('Y-m-d');
                $transaction_id = $this->add_bank_cash_transaction($method, $postData, 'credit');
            } else {
                $cash_data['branch_id'] = $user->branchid;
                $cash_data['transaction_type'] = 'credit';
                $cash_data['value'] = $post['payment_amount'];
                $cash_data['created'] = date('Y-m-d');
                $this->db->insert('an_company_transaction', $cash_data);
                $transaction_id = $this->db->insert_id();
            }
            unset($postData['user_name']);
            $loanData['loan_amount'] = $postData['loan_amount'];
            $loanData['loan_date'] = $postData['loan_date'];
            $loanData['loan_reazon'] = $postData['loan_reazon'];
            $loanData['user_id'] = $postData['user_id'];
            //$postData['user_id'];
            //$postData['loan_amount'];
            $this->db->insert('loan_advance', $loanData);
        }

        //$this->load->view('add-advance-loan', $this->_data);
        $this->_data['inside'] = 'add-advance-loan';
        $this->load->view('common/main', $this->_data);
    }

    function view_liablitiespayments($liablity_id) {
        $this->_data['payment_data'] = $this->others->getAllpaymentsBylibId($liablity_id, '');
        $this->_data['id'] = $liablity_id;
        //$this->load->view('view_payments', $this->_data);
        $this->_data['inside'] = 'view_payments';
        $this->load->view('common/main', $this->_data);
    }

    function view_receivedloans($liablity_id) {

        $this->_data['payment_data'] = $this->others->getAllpaymentsByloanId($liablity_id, '');
        $this->_data['id'] = $liablity_id;
        $this->load->view('view_loan_payments', $this->_data);
    }

    function add_payment_liablities($id) {
        $q_strring = '';
        if ($this->input->post()) {
            $postData = $this->input->post();
            //print_r($postData);
            $method = $postData['payment_method'];

            $userid = $this->session->userdata('userid');
            $user = $this->users->get_user_detail($userid);

            if ($postData['payment_method'] == 'bank') {
                $postData['clearance_date'] = date('Y-m-d');
                $transaction_id = $this->add_bank_cash_transaction($method, $postData, 'credit');
            } else {
                $cash_data['branch_id'] = $user->branchid;
                $cash_data['transaction_type'] = 'credit';
                $cash_data['value'] = $post['payment_amount'];
                $cash_data['created'] = date('Y-m-d');
                $this->db->insert('an_company_transaction', $cash_data);
                $transaction_id = $this->db->insert_id();
            }
            $lib_payment['liabilty_id'] = $postData['libality_id'];
            $lib_payment['payment_amount'] = $postData['amount_value'];
            $lib_payment['payment_date'] = $postData['liablity_date'];
            $this->db->insert('payments_liablities', $lib_payment);
            redirect(base_url() . 'others/view_liablitiespayments/' . $id . '?e=11');
        }
        $this->_data['payment_data'] = $this->others->getAllpaymentsBylibId($id, 'group');
        $this->load->view('add-payment', $this->_data);
    }

    function view_capitals() {
        $this->_data['capital_data'] = $this->others->getAllCapitals();
        //$this->load->view('view_capitals', $this->_data);
        $this->_data['inside'] = 'view_capitals';
        $this->load->view('common/main', $this->_data);
    }

    function view_liablities() {
        $this->_data['liablity_data'] = $this->others->getAllLiabilties();
        //$this->load->view('view_liablities', $this->_data);
        $this->_data['inside'] = 'view_liablities';
        $this->load->view('common/main', $this->_data);
    }

    function add_bank_cash_transaction($method, $postData, $type) {

        if ($method == 'cash') {
            $method = $postData['payment_method'];

            $userid = $this->session->userdata('userid');
            $user = $this->users->get_user_detail($userid);
            $transaction['branch_id'] = $user->branchid;
            $transaction['transaction_by'] = 'loan_advance';
            if ($type == 'receipt')
                $transaction['transaction_type'] = 'debit';
            else
                $transaction['transaction_type'] = 'credit';

            $transaction['value'] = $postData['amount_value'];
            $transaction['payment_type'] = $postData['payment_type'];

            $this->db->insert('an_cash_management', $transaction);
            return $this->db->insert_id();
        }
        else {

            $method = $postData['payment_method'];
            $transaction['account_id'] = $postData['account_id'];
            $transaction['transaction_by'] = 'loan_advance';
            if ($type == 'receipt')
                $transaction['transaction_type'] = 'debit';
            else
                $transaction['transaction_type'] = 'credit';

            $transaction['value'] = $postData['amount_value'];
            $transaction['payment_type'] = $postData['payment_type'];

            if ($postData['payment_type'] == 'cheque') {
                $transaction['cheque_date'] = $postData['cheque_date'];
                $transaction['cheque_number'] = $postData['cheque_number'];
            }

            $this->db->insert('an_company_transaction', $transaction);
            return $this->db->insert_id();
        }
    }

    //for paymetn table
    function addgeneralpayment($data) {
        $this->db->insert('payments', $data);
        return $this->db->insert_id();
    }

    function add_payment_loan($id) {
        if ($this->input->post()) {
            $postData = $this->input->post();

            $userid = $this->session->userdata('userid');
            $user = $this->users->get_user_detail($userid);


            $method = $postData['payment_method'];
            if ($postData['payment_method'] == 'bank') {
                $postData['clearance_date'] = date('Y-m-d');
                $transaction_id = $this->add_bank_cash_transaction($method, $postData, 'receipt');
            } else {
                $cash_data['branch_id'] = $user->branchid;
                $cash_data['transaction_type'] = 'debit';
                $cash_data['value'] = $post['payment_amount'];
                $cash_data['created'] = date('Y-m-d');
                $this->db->insert('an_company_transaction', $cash_data);
                $transaction_id = $this->db->insert_id();
            }


            $lib_payment['loan_advance_id'] = $postData['loan_advance_id'];
            $lib_payment['payment_amount'] = $postData['amount_value'];
            $lib_payment['payment_date'] = $postData['loan_date'];
            $lib_payment['refrence_id'] = $transaction_id;
            $lib_payment['refrence_id'] = $this->session->userdata('userid');
            $lib_payment['payment_method'] = $method;

            $this->db->insert('loan_advance_received', $lib_payment);
            redirect(base_url() . 'others/view_receivedloans/' . $id . '?e=11');
        }
        $this->_data['payment_data'] = $this->others->getAllpaymentsByloanId($id, 'group');
        //echo "<pre>";
        //print_r($this->_data['payment_data']);	
        $this->load->view('add-loan-payment', $this->_data);
    }

    function view_loans() {
        $this->_data['loans_data'] = $this->others->getAllloans();
        //$this->load->view('view_loans', $this->_data);
        $this->_data['inside'] = 'view_loans';
        $this->load->view('common/main', $this->_data);
    }

    function getAutoSearch() {
        // Data could be pulled from a DB or other source


        $term = trim(strip_tags($_GET['term']));
        $customers = $this->others->getUserByTitle($term);
        // Cleaning up the term
        // Rudimentary search
        foreach ($customers as $eachloc) {
            // Add the necessary "value" and "label" fields and append to result set
            $eachloc['value'] = $eachloc['id'];
            $eachloc['label'] = "{$eachloc['cus']}";
            $matches[] = $eachloc;
        }
        // Truncate, encode and return the results
        $matches = array_slice($matches, 0, 5);
        print json_encode($matches);
    }

    function getAutoLiablitiesSearch() {
        // Data could be pulled from a DB or other source


        $term = trim(strip_tags($_GET['term']));
        $customers = $this->others->getPartyByTitle($term);
        // Cleaning up the term
        // Rudimentary search
        foreach ($customers as $eachloc) {
            // Add the necessary "value" and "label" fields and append to result set
            $eachloc['value'] = $eachloc['id'];
            $eachloc['label'] = "{$eachloc['cus']}";
            $matches[] = $eachloc;
        }
        // Truncate, encode and return the results
        $matches = array_slice($matches, 0, 5);
        print json_encode($matches);
    }

//----------------------------------------------------------------------

    /*
     * Add Customer
     */
    public function add() {
        // Load Home View
        $this->load->view('add-quotation-form', $this->_data);
    }

//----------------------------------------------------------------------
}

?>