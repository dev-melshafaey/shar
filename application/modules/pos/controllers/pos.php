<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pos extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('bs_helper');
        $this->lang->load('main', get_set_value('site_lang'));

        $this->load->model('pos_model', 'pos_mangement');
        $this->load->model('ajax/ajax_model', 'ajax');
        // $this->_data['udata'] = userinfo_permission();
    }

    function index() {

        $this->_data['categlist'] = $this->pos_mangement->getcategory();
        $this->load->view('pos_view', $this->_data);
    }

    function update_start_time() {
        $post = $this->input->post();
        //  echo "<pre>";
        //print_r($post);
        //order_id
        //$post['start_time'];
        if ($post['status'] == 'start') {
            $st_time = $post['start_time'];
            $pData['order_start_time'] = date('Y-m-d H:i:s', strtotime($st_time));
            $this->db->where('order_id', $post['ordnumbr']);
            $this->db->update('orders', $pData);
        } else {
            //$st_time = $post['start_time'];
            $pData['order_ready_time'] = date('Y-m-d H:i:s');
            $pData['order_status'] = 'ready';
            $this->db->where('order_id', $post['ordnumbr']);
            echo $this->db->update('orders', $pData);
        }
    }

    function dateDiff($date) {
        $mydate = date("Y-m-d H:i:s");
        $theDiff = "";
        //echo $mydate;//2014-06-06 21:35:55
        $datetime1 = date_create($date);
        $datetime2 = date_create($mupdaydate);
        $interval = date_diff($datetime1, $datetime2);
        //echo $interval->format('%s Seconds %i Minutes %h Hours %d days %m Months %y Year    Ago')."<br>";
        $min = $interval->format('%i');
        $sec = $interval->format('%s');
        $hour = $interval->format('%h');
        $mon = $interval->format('%m');
        $day = $interval->format('%d');
        $year = $interval->format('%y');
        $return = array();
        if ($interval->format('%i%h%d%m%y') == "00000") {
            //echo $interval->format('%i%h%d%m%y')."<br>";
            $return['sec'] = $sec;
        } else if ($interval->format('%h%d%m%y') == "0000") {
            //return $min." Minutes";
            $return['min'] = $min;
        } else if ($interval->format('%d%m%y') == "000") {
            //return $hour." Hours";
            $return['hour'] = $hour;
        } else if ($interval->format('%m%y') == "00") {
            //return $day." Days";
            $return['day'] = $day;
        } else if ($interval->format('%y') == "0") {
            //return $mon." Months";
            $return['mon'] = $mon;
        } else {
            //return $year." Years";
            $return['year'] = $year;
        }

        return $return;
    }

    function dd() {
        $mydate = '2016-10-06 12:10:19';
        //echo $mydate= date("Y-m-d H:i:s");
        echo "The Difference between the server's date and $mydate is:<br> ";
        echo $this->dateDiff($mydate);
    }

    function screen_view() {

        $this->_data['holding_orders_total'] = $this->pos_mangement->getholdingOrdersByGroup('cold');
        $this->_data['holding_orders'] = $this->pos_mangement->getholdingOrders('cold');
        //echo "<pre>";
        //print_r($this->_data['holding_orders']);

        $this->load->view('screen_view', $this->_data);
    }

    function screen_view2() {

        $this->_data['holding_orders'] = $this->pos_mangement->getholdingOrders('hot');
        //echo "<pre>";
        //print_r($this->_data['holding_orders']);

        $this->load->view('screen_view2', $this->_data);
    }

    function search_invoice_sales_new() {
        $postData = $this->input->post();
        // echo "<pre>";
        // print_r($postData);
        //exit;
        $sin = $postData['sin'];
        if ($postData['sin']) {

            $invoicelist = $this->pos_mangement->getPosSalesInvoiceItem($sin);
            //     print_r($invoicelist);
            //   exit;
            $Sno = 1;
            $hiddenhtml = '';
            $html = '';
            foreach ($invoicelist as $inv) {
                $itData = unserialize($inv->itemname);
                $name = $itData['english'];
                $hiddenhtml.= '<input type="hidden" class="item_name" value="' . $name . '"><input type="hidden" class="item_quantity" value="' . $inv->invoice_item_quantity . '"> <input type="hidden" class="itemid" value="' . $inv->invoice_item_id . '">';
                $html.='<tr><td data-title="No.">' . $Sno . '</td><td data-title="Image" class="itemname">' . $itData['english'] . '</td><td data-title="Name" class="itemquantity">' . $inv->invoice_item_quantity . '</td><td data-title="Price" class="numeric itemprice">' . $inv->invoice_item_price . '</td><input type="hidden" class="itemid" value="' . $inv->invoice_item_id . '"></tr>';
                $Sno++;
            }
        }

        echo $html;
        exit;
    }

    function getRandomArbitrary() {
        return Math . floor((Math . random() * 1000) + 1);
    }

    function getLinkItems() {
        $postData = $this->input->post();

        $item_id = $postData['item_id'];

        $posAdd = $this->pos_mangement->getPosAdditionalItems($item_id);

        $li = '';
        //echo "<pre>";
        //print_r($posAdd);
        if ($posAdd) {
            foreach ($posAdd as $i => $posA) {
                $iprice = getAmountFormat($posA->price);

                preg_match("/iPhone|Android|iPad|iPod|webOS/", $_SERVER['HTTP_USER_AGENT'], $matches);
                $os = current($matches);


                if ($os != "") {
                    $li.='<tr style="margin-top: 20px;"><td style="padding: 15px;"><input style="transform: scale(2.5);width: 30px;height: 30px;" type="checkbox" id="more" name="itemmore' . $posA->link_id . '" value="' . $posA->link_id . '" onclick="assignMoreItem(' . $posA->link_id . ')"> </td><td style="padding:8px;"><div style="font-size:20px;padding-left:23px;">' . $posA->item_eng_name . '|' . $posA->item_ar_name . ' </div></td><td style="padding: 8px;"><input type="hidden" id="moreitemname' . $posA->link_id . '"  name="moreitemname[]" value="' . $posA->item_eng_name . '|' . $posA->item_ar_name . '"><input type="hidden" id="moreitemid' . $posA->link_id . '"  name="moreitemprice[]" value="' . $iprice . '"> ' . $iprice . '</td></tr>';
                } else {
                    $li.='<tr><td style="padding: 8px;"><input style="padding: 56px;transform: scale(2.5);position: absolute;" type="checkbox" id="more" name="itemmore' . $posA->link_id . '" value="' . $posA->link_id . '" onclick="assignMoreItem(' . $posA->link_id . ')"> <div style="font-size:20px;padding-left:23px;">' . $posA->item_eng_name . '|' . $posA->item_ar_name . ' </div></td><td style="padding: 8px;"><input type="hidden" id="moreitemname' . $posA->link_id . '"  name="moreitemname[]" value="' . $posA->item_eng_name . '|' . $posA->item_ar_name . '"><input type="hidden" id="moreitemid' . $posA->link_id . '"  name="moreitemprice[]" value="' . $iprice . '"> ' . $iprice . '</td></tr>';
                }
            }
        }
        echo $li;
        exit;
    }

    function get_posinvoice_byidnew() {
        $postData = $this->input->post();

        $invoicelist = $this->pos_mangement->getSalesInvoiceById($postData['inv_id']);
        $csname = $this->pos_mangement->getCustomerIdByInvId($postData['inv_id']);

        $cname = $csname->csname;
        $cuid = $csname->userid;
        $cop_value = $csname->coupon_value;
        if ($invoicelist) {
            $html = '';
            $hiddenhtml = '';
            $Sno = 1;
            $total = 0;
            $totalitems = 0;
            foreach ($invoicelist as $inv) {
                $itData = unserialize($inv->itemname);
                $cat = $inv->categ_type;
                $name = $itData['english'];
                $namear = $itData['arabic'];
                //$inv->invoice_item_price;
                $type = '';
                if ($inv->invoice_item_price == $inv->large_price) {
                    $type = 'L';
                }

                if ($inv->invoice_item_price == $inv->medium_price) {
                    $type = 'M';
                }

                if ($inv->invoice_item_price == $inv->small_price) {
                    $type = 'S';
                }

                $invprice = getAmountFormat($inv->invoice_item_price);
                $hiddenhtml.= '<input type="hidden" class="item_name" value="' . $name . '"><input type="hidden" class="item_quantity" value="' . $inv->invoice_item_quantity . '"> <input type="hidden" class="itemid" value="' . $inv->invoice_item_id . '">';
                $html.='<tr><td data-title="No.">' . $Sno . '</td><td data-title="Image" class="itemname"><span class="pname">' . $itData['english'] . '</span><span>' . $itData['arabic'] . '</span></td><td data-title="Name" class="itemquantity">' . $inv->invoice_item_quantity . '</td><td data-title="Price" class="numeric itemprice">' . $invprice . '</td><input type="hidden" class="itemid" value="' . $inv->invoice_item_id . '"><input type="hidden" class="iteminvid" value="' . $inv->inovice_product_id . '"><input type="hidden" class="itemcat" value="' . $cat . '"><input type="hidden" class="itemtar" value="' . $namear . '"><input type="hidden" class="itemteng" value="' . $name . '"><input type="hidden" class="type" value="' . $type . '"></tr>';
                $totalitems+=$inv->invoice_item_quantity;
                $total+=$invprice;
                $Sno++;
            }
            //$html.='<tr><td data-title="No."></td><td data-title="Image"></td><td data-title="Name">('.$totalitems.')</td><td data-title="Price" class="numeric">'.getAmountFormat($total).'</td></tr>';
        }

        $arr = array('html' => $html, 'csname' => $cname, 'csid' => $cuid, 'cop_value' => $cop_value);
        echo json_encode($arr);
    }

    function get_posinvoice_byid() {
        $postData = $this->input->post();
        //echo "<pre>";
        // print_r($postData);
        //$postData['type'];

        $invoicelist = $this->pos_mangement->getSalesInvoiceById($postData['inv_id']);
        $csname = $this->pos_mangement->getCustomerIdByInvId($postData['inv_id']);
//        print_r($invoicelist);

        $cname = $csname->csname;
        $cuid = $csname->userid;
        if ($invoicelist) {
            $html = '';
            $hiddenhtml = '';
            $Sno = 1;
            $total = 0;
            $totalitems = 0;
            foreach ($invoicelist as $inv) {
                $itData = unserialize($inv->itemname);
                $cat = $inv->categ_type;
                $name = $itData['english'];
                $namear = $itData['arabic'];
                //$inv->invoice_item_price;
                $type = '';
                if ($inv->invoice_item_price == $inv->large_price) {
                    $type = 'L';
                }

                if ($inv->invoice_item_price == $inv->medium_price) {
                    $type = 'M';
                }

                if ($inv->invoice_item_price == $inv->small_price) {
                    $type = 'S';
                }
                $innerhtml = " <br>";

                //$inv->inovice_product_id;
                $mItems = $this->pos_mangement->getMoreItemsByInvItemid($inv->inovice_product_id);

                if (!empty($mItems)) {
                    foreach ($mItems as $mitem) {
                        $innerhtml.= " <span>$mitem->item_eng_name | $mitem->item_ar_name</span> ";
                    }
                }

                $invprice = getAmountFormat($inv->invoice_item_quantity * $inv->invoice_item_price);
                $hiddenhtml.= '<input type="hidden" class="item_name" value="' . $name . '"><input type="hidden" class="item_quantity" value="' . $inv->invoice_item_quantity . '"> <input type="hidden" class="itemid" value="' . $inv->invoice_item_id . '">';
                $html.='<tr><td data-title="No.">' . $Sno . '</td><td data-title="Image" class="itemname"><span class="pname">' . $itData['english'] . '</span><span>' . $itData['arabic'] . '</span>  ' . $innerhtml . '</td><td data-title="Name" class="itemquantity">' . $inv->invoice_item_quantity . '</td><td data-title="Price" class="numeric itemprice">' . $invprice . '</td><input type="hidden" class="itemid" value="' . $inv->invoice_item_id . '"><input type="hidden" class="iteminvid" value="' . $inv->inovice_product_id . '"><input type="hidden" class="itemcat" value="' . $cat . '"><input type="hidden" class="itemtar" value="' . $namear . '"><input type="hidden" class="itemteng" value="' . $name . '"><input type="hidden" class="type" value="' . $type . '"></tr>';
                $totalitems+=$inv->invoice_item_quantity;
                $total+=$invprice;
                $Sno++;
            }
            //$html.='<tr><td data-title="No."></td><td data-title="Image"></td><td data-title="Name">('.$totalitems.')</td><td data-title="Price" class="numeric">'.getAmountFormat($total).'</td></tr>';
        }
        // echo $html;
        $arr = array('html' => $html, 'csname' => $cname, 'csid' => $cuid);
        echo json_encode($arr);
    }

    function get_posinvoice_byidd() {
        $postData = $this->input->post();
        //echo "<pre>";
        // print_r($postData);
        //$postData['type'];

        $invoicelist = $this->pos_mangement->getSalesInvoiceById($postData['inv_id']);
//        print_r($invoicelist);

        if ($invoicelist) {
            $html = '';
            $hiddenhtml = '';
            $Sno = 1;
            $total = 0;
            $totalitems = 0;
            foreach ($invoicelist as $inv) {
                $invoice_item_id = $inv->invoice_item_id;
                $type = $inv->categ_type;
                $itData = unserialize($inv->itemname);
                $name = $itData['english'];
                $namear = $itData['arabic'];
                $randid = getRandomArbitrary();
                //		htm+='<input type="hidden" id="itemtype'+randid+'_'+pname+'"  name="itemtype[]" value="'+type+'">';
                //htm+='<input type="hidden" id="itemtar'+randid+'_'+pname+'"  name="itemtar[]" value="'+par+'">';
                //htm+='<input type="hidden" id="itemtpcat'+randid+'_'+pname+'"  name="itemtpcat[]" value="'+itemtp+'">';
                $hiddenhtml.= '<input type="hidden" id="itemtpcat' . $randid . '_' . $invoice_item_id . '"  name="itemtar[]" value="' . $namear . '"><input type="hidden" id="itemtar' . $randid . '_' . $invoice_item_id . '"  name="itemtar[]" value="' . $namear . '"><input type="hidden" id="itemtype' . $randid . '_' . $invoice_item_id . '"  name="itemtype[]" value="' . $type . '"><input type="hidden" class="item_name" value="' . $name . '"><input type="hidden" class="item_quantity" value="' . $inv->invoice_item_quantity . '"> <input type="hidden" id="' . $inv->invoice_item_id . '" name="invoice_item_id[]" class="itemid" value="' . $inv->invoice_item_id . '">';
                $html.='<tr><td data-title="No.">' . $Sno . '</td><td data-title="Image" class="itemname"><span class="pname">' . $itData['english'] . '</span><span>' . $itData['arabic'] . '</span></td><td data-title="Name" class="itemquantity">' . $inv->invoice_item_quantity . '</td><td data-title="Price" class="numeric itemprice">' . $inv->invoice_item_price . '</td><input type="hidden" class="itemid" value="' . $inv->invoice_item_id . '"></tr>';
                $totalitems+=$inv->invoice_item_quantity;
                $total+=$inv->invoice_item_price;
                $Sno++;
            }
            $html.='<tr><td data-title="No."></td><td data-title="Image"></td><td data-title="Name">(' . $totalitems . ')</td><td data-title="Price" class="numeric">' . getAmountFormat($total) . '</td></tr>';
        }
        echo $html;
    }

    function update_cancel_invoice() {


        $postData = $this->input->post();
        //echo "<pre>";
        // print_r($postData);
        //   exit;
        //$postData['inv_id'] ;

        $post = post();
        $post['invocie_id'] = $postData['inv_id'];


        $pData['cancel_sales'] = 1;
        $pid = post('purchaseInvocie');
        $this->db->where('invoice_id', $post['invocie_id']);
        $this->db->update('an_invoice', $pData);

        $pData = array();

        $pData['sales_item_is_cancel'] = 1;
        $pid = post('purchaseInvocie');
        $this->db->where('invoice_id', $post['invocie_id']);
        $this->db->update('bs_invoice_items', $pData);

        $pData = array();
        $pData['cancel_status'] = 1;
        $pid = post('purchaseInvocie');
        $this->db->where('invoice_id', $post['invocie_id']);
        $this->db->update('orders', $pData);


        $orderid = $this->sales_management->getOrderIdByInId($post['invocie_id']);
        $pData = array();
        $pData['cancel_status'] = 1;
        $this->db->where('order_id', $orderid);
        $this->db->update('clear_order_payment', $pData);



        $pData = array();
        $pData['sales_payment_is_cancel'] = 1;
        $pid = post('purchaseInvocie');
        $this->db->where('invoice_id', $post['invocie_id']);
        $this->db->update('invoice_payments', $pData);

        $pData = array();
        $pData['is_cancel'] = 1;
        $pid = post('purchaseInvocie');
        $this->db->where('recipt_id', $post['invocie_id']);
        $this->db->update('store_soled_items', $pData);


        $payments = $this->sales_management->getPaymentIdsForCancel($post['invocie_id']);
        if ($payments) {
            foreach ($payments as $pm) {

                $pm->payment_id;
                $pmData['status'] = 0;
                $this->db->where('id', $pm->payment_id);
                $this->db->update('payments', $pmData);
            }
        }






        $invData = $this->sales_management->getInvoiceDataById($post['invocie_id']);

        unset($invData['invoice_id']);
        $newinvId = $this->sales_management->inserData('an_invoice', $invData);
        $invItemData = $this->sales_management->getInvoiceItemsDataById($post['invocie_id']);
        if ($invItemData) {
            foreach ($invItemData as $invItems) {

                $invItems['invoice_id'] = $newinvId;
                unset($invItems['inovice_product_id']);
                $this->sales_management->inserData('bs_invoice_items', $invItems);
            }
        }


        $invItemPaymentData = $this->sales_management->getInvoicePaymentsById($post['invocie_id']);
        if ($invItemPaymentData) {
            foreach ($invItemPaymentData as $invpItems) {
                $invpItems['invoice_id'] = $newinvId;
                //	$allpayments[] = $invpItems['payment_id'];
                $pmData = $this->sales_management->getPaymentDataById($invpItems['payment_id']);
                unset($pmData['id']);
                $invpItems['payment_id'] = $this->sales_management->inserData('payments', $pmData);
                unset($invpItems['inv_payment_id']);
                $this->sales_management->inserData('invoice_payments', $invpItems);
            }
        }



        $invSoledData = $this->sales_management->getSoldItemsByInvId($post['invocie_id']);
        if ($invSoledData) {
            foreach ($invSoledData as $soled) {
                $soled['recipt_id'] = $newinvId;
                unset($soled['soled_id']);
                $this->sales_management->inserData('store_soled_items', $soled);
            }
        }



        //	exit;



        if ($post) {
            
        }

        // return true;
    }

    function get_posinvoice_all() {
        $postData = $this->input->post();
        //$postData['type'];
        $invoicelist = $this->pos_mangement->getAllinvoiceList();
        //echo "<pre>";
        //print_r($invoicelist);

        if ($invoicelist) {
            $html = '';
            $total = 0;
            $totalitems = 0;
            $Sno = 1;
            // echo "<pre>";
            // print_r($invoicelist);

            foreach ($invoicelist as $inv) {

                $url1 = base_url() . "phpprinter/example/pos_receipt_inv.php?invid=" . $inv->invoice_id;
                $url2 = base_url() . "sales_management/edit_print_order/" . $inv->invoice_id;
                $html.='<tr><td data-title="No.">' . $Sno . '</td><td data-title="Image"><a href="javascript:void(0)" onclick="assignInvoiceItemListById(' . $inv->invoice_id . ')" >' . $inv->invoice_id . '</a></td><td>' . $inv->empname . '</td><td>' . $inv->csname . '</td><td data-title="Name"><a href="javascript:void(0)" onclick="viewInvoiceItemListById(' . $inv->invoice_id . ')">(' . $inv->totalitems . ')</a></td><td data-title="Price" class="numeric">' . $inv->invoice_total_amount . '<input type="hidden" id="customername' . $inv->invoice_id . '" value="' . $inv->csname . '"><input type="hidden" id="customerid' . $inv->invoice_id . '" value="' . $inv->userid . '"></td><td><a id="' . $inv->invoice_id . '" class ="asd";  onclic="PrintInvOrder(' . $inv->invoice_id . ')" href="javascript:void(0)" >Print Invoice</a></td><td></td></tr>';
                $total+=$inv->invoice_total_amount;
                $totalitems+=$inv->totalitems;
                $Sno++;
            }
            //  $html.='<tr><td data-title="No."></td><td data-title="Image"></td><td data-title="Name">('.$totalitems.')</td><td data-title="Price" class="numeric">'.getAmountFormat($total).'</td></tr>';
        }
        echo $html;
    }

    function get_posinvoice_bytype() {
        $postData = $this->input->post();
        //$postData['type'];
        $invoicelist = $this->pos_mangement->getSalesInvoiceByType($postData['type']);
        if ($invoicelist) {
            $html = '';
            $total = 0;
            $totalitems = 0;
            $Sno = 1;
            // echo "<pre>";
            // print_r($invoicelist);

            foreach ($invoicelist as $inv) {

                $html.='<tr><td data-title="No.">' . $Sno . '</td><td data-title="Image"><a href="javascript:void(0)" onclick="assignInvoiceItemListById(' . $inv->invoice_id . ')" >' . $inv->invoice_id . '</a></td><td>' . $inv->csname . '</td><td data-title="Name"><a href="javascript:void(0)" onclick="viewInvoiceItemListById(' . $inv->invoice_id . ')">(' . $inv->totalitems . ')</a></td><td data-title="Price" class="numeric">' . $inv->invoice_total_amount . '<input type="hidden" id="customername' . $inv->invoice_id . '" value="' . $inv->csname . '"><input type="hidden" id="customerid' . $inv->invoice_id . '" value="' . $inv->userid . '"></td></tr>';
                $total+=$inv->invoice_total_amount;
                $totalitems+=$inv->totalitems;
                $Sno++;
            }
            //  $html.='<tr><td data-title="No."></td><td data-title="Image"></td><td data-title="Name">('.$totalitems.')</td><td data-title="Price" class="numeric">'.getAmountFormat($total).'</td></tr>';
        }
        echo $html;
    }

    function testprint() {
        $printer = printer_open("\\\\DESKTOP-E1FASQL\\HP LaserJet Professional P1102");
        printer_start_doc($printer, "Doc");
        printer_start_page($printer);
        $barcode = printer_create_font("Free 3 of 9 Extended", 400, 200, PRINTER_FW_NORMAL, false, false, false, 0);
        $arial = printer_create_font("Arial", 50, 50, PRINTER_FW_MEDIUM, false, false, false, 0);
        printer_select_font($printer, $barcode);
        //printer_draw_text($printer, "*123456*", 50, 50);
        printer_select_font($printer, $arial);
        printer_draw_text($printer, "123456", 500, 500);
        printer_delete_font($barcode);
        printer_delete_font($arial);
        printer_end_page($printer);
        printer_end_doc($printer);
        printer_close($printer);
    }

    function printract() {
        $printer = ("\\\\DESKTOP-E1FASQL\\HP LaserJet Professional P1102");
        $handle = printer_open($printer);
        printer_start_doc($handle);
        printer_start_page($handle);

        printer_create_dc($handle);
        /* do some stuff with the dc */
        printer_set_option($handle, PRINTER_TEXT_COLOR, "333333");
        printer_draw_text($handle, 1, 1, "text");
        printer_delete_dc($handle);

        /* create another dc */
        printer_create_dc($handle);
        printer_set_option($handle, PRINTER_TEXT_COLOR, "000000");
        printer_draw_text($handle, 1, 1, "text");
        /* do some stuff with the dc */

        printer_delete_dc($handle);

        printer_end_page($handle);
        printer_end_doc($handle);
        printer_close($handle);
    }

    function view_printers() {
        //$printerList = printer_list(PRINTER_ENUM_LOCAL);
        //var_dump(printer_list(PRINTER_ENUM_LOCAL));
        //PRINTER_ENUM_LOCAL |
        //  print_r($printerList);
        // var_dump($printerList);
        //  echo "<pre>";
        //print_r($_SERVER['DOCUMENT_ROOT']);
        $fname = $_SERVER['DOCUMENT_ROOT'] . '/juice/assets/test.txt';
        //exit;
        $printer = ("\\\\DESKTOP-E1FASQL\\HP LaserJet Professional P1102");
        if ($ph = printer_open($printer)) {
            printer_start_doc($printer, "Doc");
            printer_start_page($printer);
            $fh = fopen($fname, "rb");
            $content = fread($fh, filesize($fname));
            //$fh = fopen("filename.ext", "rb");
            //$content = fread($fh, filesize("filename.ext"));

            printer_set_option($ph, PRINTER_MODE, "RAW");
            printer_write($ph, $content);

            printer_end_page($printer);
            printer_end_doc($printer);
            printer_close($printer);
            // Get file contents
            //$fh = fopen("filename.ext", "rb");
            //$content = fread($fh, filesize("filename.ext"));
            //fclose($fh);
            $content = "hello";
            // Set print mode to RAW and send PDF to printer
            //printer_end_doc($ph);
            //printer_close($ph);
        } else
            "Couldn't connect...";
    }

    function add() {
        $branchid = $this->session->userdata('bs_branchid');
        $store = $this->ajax->getStoreByBranchId($branchid);

        $bs_userid = $this->session->userdata('bs_userid');
        $this->_data['userData'] = $this->pos_mangement->get_user_detail($bs_userid);
        $this->_data['addData'] = $this->pos_mangement->getAdditionItems();

        $this->_data['readycount'] = $this->pos_mangement->getReadyOrdersCount($bs_userid);
        $this->_data['storeid'] = $store->storeid;
        $this->_data['categlist'] = $this->pos_mangement->getcategory();
        $this->load->view('pos_view', $this->_data);
    }

    function add2() {
        $branchid = $this->session->userdata('bs_branchid');
        $store = $this->ajax->getStoreByBranchId($branchid);

        $bs_userid = $this->session->userdata('bs_userid');
        $this->_data['userData'] = $this->pos_mangement->get_user_detail($bs_userid);
        //echo "<pre>";
        //print_r($userData);

        $this->_data['readycount'] = $this->pos_mangement->getReadyOrdersCount($bs_userid);
        $this->_data['storeid'] = $store->storeid;
        $this->_data['categlist'] = $this->pos_mangement->getcategory();
        $this->load->view('pos_view_ar', $this->_data);
    }

    function update_payment_cardinvoice() {
        $postData = $this->input->post();
        $invid = $postData['invid'];
        $am = $postData['am'];


        //echo "<pre>";
        //print_r($postData);
        //exit;

        if (isset($postData['discval']) && $postData['discval'] != "") {
            $am = $postData['am'] - $postData['discval'];
        }

        $pData['payment_type'] = 'receipt';
        $pData['refernce_type'] = 'against';
        $pData['payment_amount'] = $am;
        $pData['refrence_id'] = $postData['customer_id'];
        $pData['payment_date'] = date('Y-m-d');
        $paym_id = $this->pos_mangement->inserData('payments', $pData);

        $pinvData['payment_id'] = $paym_id;
        $pinvData['payment_amount'] = $am;
        $pinvData['invoice_id'] = $invid;
        $pinvData['payment_type'] = 1;
        $pinvData['payment_date'] = date('Y-m-d');
        $pinvData['datetopay'] = date('Y-m-d');
        $inv_p_id = $this->pos_mangement->inserData('invoice_payments', $pinvData);

        $invData['invoice_payment_status'] = 'payment';
        $invData['invoice_deal_type'] = 'card';
        $invData['card_number'] = $postData['add_card_no'];

        if (isset($postData['discval']) && $postData['discval'] != "") {
            $invData['invoice_total_amount'] = $am;
            $invData['sales_amount'] = $am;
            $invData['invoice_totalDiscount'] = $postData['discval'];
            //$invData['invoice_totalDiscount'] = $postData['discval'];
        }
        $this->db->where('invoice_id', $invid);
        $this->db->update('an_invoice', $invData);


        $orderData['order_status'] = 'paid';
        $orderData['order_amount'] = 'paid';
        $this->db->where('invoice_id', $invid);
        echo $this->db->update('orders', $orderData);
    }

    function update_payment_invoice() {
        $postData = $this->input->post();
        
        $invid = $postData['invid'];
        $am = $postData['am'];

        if (isset($postData['discval']) && $postData['discval'] != "") {
            $am = $postData['am'] - $postData['discval'];
        }
        $pData['payment_type'] = 'receipt';
        $pData['refernce_type'] = 'against';
        $pData['payment_amount'] = $am;
        $pData['refrence_id'] = $postData['customer_id'];
        $pData['payment_date'] = date('Y-m-d');
        $paym_id = $this->pos_mangement->inserData('payments', $pData);

        $pinvData['payment_id'] = $paym_id;
        $pinvData['payment_amount'] = $am;
        $pinvData['invoice_id'] = $invid;
        $pinvData['payment_type'] = 1;
        $pinvData['payment_date'] = date('Y-m-d');
        $pinvData['datetopay'] = date('Y-m-d');
        $inv_p_id = $this->pos_mangement->inserData('invoice_payments', $pinvData);

        $invData['invoice_payment_status'] = 'payment';

        if (isset($postData['discval']) && $postData['discval'] != "") {
            $invData['invoice_total_amount'] = $am;
            $invData['sales_amount'] = $am;
            $invData['invoice_totalDiscount'] = $postData['discval'];
            //$invData['invoice_totalDiscount'] = $postData['discval'];
        }
        $this->db->where('invoice_id', $invid);
        $this->db->update('an_invoice', $invData);


        $orderData['order_status'] = 'paid';
        $orderData['order_amount'] = $am;
        $this->db->where('invoice_id', $invid);
        echo $this->db->update('orders', $orderData);
    }

    function create_barcode($id) {


        //$postData = $this->input->post();
        //echo $_SERVER['DOCUMENT_ROOT'];
        if ($id) {
            include "Barcode39.php";

            // set Barcode39 object
            $bc = new Barcode39($id);

            // display new barcode
            $imagePath = $_SERVER['DOCUMENT_ROOT'] . $this->config->item('barcode_bath') . 'barcode_image/orders';

            //echo $imagePath;

            $imagePath = $imagePath . '/' . $id . '.gif';

            $bc->barcode_text_size = 5;

            // set barcode bar thickness (thick bars)
            $bc->barcode_bar_thick = 4;

            //echo $imagePath;
            // set barcode bar thickness (thin bars)
            $bc->barcode_bar_thin = 2;
            $bc->draw($imagePath);
            return true;
        }

        //$this->load->library('Barcode39','bar');
        //$this->bar->Barcode39("Shay Anderson");
        //$bc = new Barcode39("Shay Anderson");
        //echo "asdasd";
        //echo $this->bar->draw();
    }

    function get_ready_Orders() {
        $bs_userid = $this->session->userdata('bs_userid');
        $invoicelist = $this->pos_mangement->getReadyOrdersList($bs_userid);

        $addpermsall = $this->session->userdata('bs_addperms');

        $addperms = $addpermsall->addmodules;
        if ($addperms) {
            $mPermission = explode(',', $addperms);
        }


        if ($invoicelist) {
            $html = '';
            $total = 0;
            $totalitems = 0;
            $Sno = 1;
            foreach ($invoicelist as $inv) {


                $fifty = $inv->order_amount / 2;
                $ten = $inv->order_amount * (10 / 100);
                $qrtr = $inv->order_amount * (25 / 100);
                $sevenFive = $inv->order_amount * (75 / 100);
                if (!empty($mPermission)) {
                    if (in_array('2', $mPermission)) {
                        $html.='<tr id="tr_' . $inv->invoice_id . '"><td data-title="No."><a href="javascript:void(0)" onclick="assignInvoiceItemListById(' . $inv->invoice_id . ')">' . $inv->invoice_id . '</a></td><td data-title="Name"><a href="javascript:void(0)" onclick="viewInvoiceItemListById(' . $inv->invoice_id . ')">(' . $inv->fullname . ')</a></td><td data-title="Price" class="numeric">' . $inv->order_amount . '</td><td data-title="Price" class="numeric">' . $inv->order_amount . '</td><td><input type="radio" name="payment_disc" class="discount_percent" id="discount_percent' . $inv->invoice_id . '" value="' . $inv->order_amount . '" onclick="discount_percent_payment(' . $inv->invoice_id . ',this.value)">100%&#8195;<input type="radio" class="discount_percent" name="payment_disc" id="discount_percent' . $inv->invoice_id . '" value="' . $sevenFive . '" onclick="discount_percent_payment(' . $inv->invoice_id . ',this.value)">75%&#8195;<input type="radio" class="discount_percent" name="payment_disc" id="discount_percent' . $inv->invoice_id . '" value="' . $fifty . '" onclick="discount_percent_payment(' . $inv->invoice_id . ',this.value)">50% &#8195;<input type="radio" class="discount_percent" name="payment_disc" id="discount_percent' . $inv->invoice_id . '" value="' . $qrtr . '" onclick="discount_percent_payment(' . $inv->invoice_id . ',this.value)">25% &#8195;<input type="radio" class="discount_percent" name="payment_disc" id="discount_percent' . $inv->invoice_id . '" value="' . $ten . '" onclick="discount_percent_payment(' . $inv->invoice_id . ',this.value)">10%<br><input type="text" class="form-control" id="discount_val' . $inv->invoice_id . '" value="0" style="width:60%"><td><button  onclick="add_payment(' . $inv->invoice_id . ',' . $inv->userid . ',' . $inv->order_amount . ')" type = "button" class = "btn btn-success btn-block tbox">Payment</button> <br> <button onclick="showCardPop(' . $inv->invoice_id . ',' . $inv->userid . ',' . $inv->order_amount . ')" type = "button" class = "btn btn-danger btn-block tbox">PayByCard</button></td></tr>';
                    } else {
                        $html.='<tr id="tr_' . $inv->invoice_id . '"><td data-title="No."><a href="javascript:void(0)" onclick="assignInvoiceItemListById(' . $inv->invoice_id . ')">' . $inv->invoice_id . '</a></td><td data-title="Name"><a href="javascript:void(0)" onclick="viewInvoiceItemListById(' . $inv->invoice_id . ')">(' . $inv->fullname . ')</a></td><td data-title="Price" class="numeric">' . $inv->order_amount . '</td><td data-title="Price" class="numeric">' . $inv->order_amount . '</td><td style="display:none;"><input type="checkbox" class="discount_percent" id="discount_percent' . $inv->invoice_id . '" value="' . $inv->order_amount . '">100%<br><input type="text" class="form-control" id="discount_val' . $inv->invoice_id . '" value="0" style="width:60%" ><td><button  onclick="add_payment(' . $inv->invoice_id . ',' . $inv->userid . ',' . $inv->order_amount . ')" type = "button" class = "btn btn-success btn-block tbox">Payment</button> <br> <button onclick="showCardPop(' . $inv->invoice_id . ',' . $inv->userid . ',' . $inv->order_amount . ')" type = "button" class = "btn btn-danger btn-block tbox">PayByCard</button></td></tr>';
                    }
                } else {
                    $html.='<tr id="tr_' . $inv->invoice_id . '"><td data-title="No."><a href="javascript:void(0)" onclick="assignInvoiceItemListById(' . $inv->invoice_id . ')">' . $inv->invoice_id . '</a></td><td data-title="Name"><a href="javascript:void(0)" onclick="viewInvoiceItemListById(' . $inv->invoice_id . ')">(' . $inv->fullname . ')</a></td><td data-title="Price" class="numeric">' . $inv->order_amount . '</td><td data-title="Price" class="numeric">' . $inv->order_amount . '</td><td style="display:none;"><input type="checkbox" class="discount_percent" id="discount_percent' . $inv->invoice_id . '" value="' . $inv->order_amount . '">100%<br><input type="text" class="form-control" id="discount_val' . $inv->invoice_id . '" value="0" style="width:60%"><td><button  onclick="add_payment(' . $inv->invoice_id . ',' . $inv->userid . ',' . $inv->order_amount . ')" type = "button" class = "btn btn-success btn-block tbox">Payment</button> <br> <button onclick="showCardPop(' . $inv->invoice_id . ',' . $inv->userid . ',' . $inv->order_amount . ')" type = "button" class = "btn btn-danger btn-block tbox">PayByCard</button></td></tr>';
                }

                $total+=$inv->order_amount;
                $totalitems+=$inv->totalitems;
                $Sno++;
            }
            // $html.='<tr><td data-title="No.">'.count($invoicelist).'</td><td data-title="Name"></td><td data-title="Price" class="numeric">'.getAmountFormat($total).'</td><td></td></tr>';
        }
        echo $html;
    }

    function listpos() {

        // Load Home View
        // Load Home View
        $mem = $this->session->userdata('bs_memtype');
        if ($mem != 5 && $mem != 1) {
            //echo "dsasd";
            $id = $this->session->userdata('bs_userid');
        } else {
            $id = "";
        }

        //echo $id;

        $this->_data['sale_invoices'] = $this->pos_mangement->getSaleInvocies_new($id);

        //$this->load->view('sales-invoices', $this->_data);

        $this->_data['inside'] = 'sales-invoices';

        $this->load->view('common/main', $this->_data);
    }

    //function
    function PosPaymentEntry($payment_id, $invoice_id, $customerid, $invoicetotal) {


        $post = $this->input->post();

        $branchid = $this->session->userdata('bs_branchid');
        $bs_userid = $this->session->userdata('bs_userid');

        if ($post['invoice_paymemt_type'] == 'card') {



            $bankinfo = $this->pos_mangement->getPosBankAccount($branchid);

            $payment_bank['invoice'] = $invoice_id;

            $payment_bank['customer_id'] = $_POST['customerid'];

            $payment_bank['payment_id'] = $payment_id;

            $payment_bank['amount_type'] = 'sales';

            $payment_bank['refrence_type'] = 'sales';

            $payment_bank['transaction_type'] = 'debit';

            $payment_bank['value'] = $invoicetotal;


            $payment_bank['clearance_date'] = date('Y-m-d');

            $payment_bank['bank_id'] = $bankinfo->bank_id;

            $payment_bank['account_id'] = $bankinfo->account_id;





            //6

            $payment_id = $this->pos_mangement->inserData('an_company_transaction', $payment_bank);
        } else {

            $cashinfo = $this->pos_mangement->getPosCashAccount($branchid);
            $payment_cash['payment_id'] = $payment_id;

            $payment_cash['customer_id'] = $customerid;

            $payment_cash['value'] = $invoicetotal;


            $payment_cash['transaction_type'] = 'debit';

            $payment_cash['payment_type'] = 'cash';

            $payment_cash['refrence_type'] = 'payment_in';

            $payment_cash['date'] = date('Y-m-d');
            $payment_cash['branch_cash_id'] = $cashinfo->id;
            $payment_cash['invoice_id'] = $invoice_id;

            //5

            $payment_id = $this->pos_mangement->inserData('an_cash_management', $payment_cash);
        }
    }

    function printed($id) {


        $this->_data['invoice'] = $this->pos_mangement->p_invoice($id);

        $this->_data['p_invoices'] = $this->pos_mangement->p_invoices($id);

        //$this->_data['inside'] = 'printed';
        //echo "<pre>"; print_r($this->_data['p_invoices']); //exit;
        $this->load->view('printed', $this->_data);
    }

    function test() {
        echo "<pre>";
        //	print_r($_SESSION);
        //echo "test";
        echo $this->session->userdata('bs_branchid');
        $sData = $this->session->all_userdata();
        print_r($sData);
    }

    function cancel_pos_inv() {


        $this->load->model('sales_management/sales_management_model', 'sales_management');

        $postData = $this->input->post();
        //echo "<pre>";
        //print_r($postData);
        //exit;
        $invid = $postData['inv'];
        //exit;

        if ($postData) {

            $pData['cancel_sales'] = 1;
            $pid = post('purchaseInvocie');
            $this->db->where('invoice_id', $invid);
            $this->db->update('an_invoice', $pData);

            $pData = array();

            $pData['sales_item_is_cancel'] = 1;
            $pid = post('purchaseInvocie');
            $this->db->where('invoice_id', $invid);
            $this->db->update('bs_invoice_items', $pData);

            $pData = array();
            $pData['sales_payment_is_cancel'] = 1;
            $pid = post('purchaseInvocie');
            $this->db->where('invoice_id', $invid);
            $this->db->update('invoice_payments', $pData);

            $pData = array();
            $pData['is_cancel'] = 1;
            $pid = post('purchaseInvocie');
            $this->db->where('recipt_id', $invid);
            $this->db->update('store_soled_items', $pData);

            $pData = array();
            $pData['cancel_status'] = 1;
            $pid = post('purchaseInvocie');
            $this->db->where('invoice_id', $invid);
            $this->db->update('orders', $pData);


            $orderid = $this->sales_management->getOrderIdByInId($invid);
            $pData = array();
            $pData['cancel_status'] = 1;
            $this->db->where('order_id', $orderid);
            $this->db->update('clear_order_payment', $pData);


            $payments = $this->sales_management->getPaymentIdsForCancel($invid);
            if ($payments) {
                foreach ($payments as $pm) {

                    $pm->payment_id;
                    $pmData['status'] = 0;
                    $this->db->where('id', $pm->payment_id);
                    $this->db->update('payments', $pmData);
                }
            }
            echo '1';
        } else {
            echo false;
        }
    }

    function search_invoice_sales() {
        $postData = $this->input->post();
        //  echo "<pre>";
        // print_r($postData);
        //exit;
        $bs_userid = $this->session->userdata('bs_userid');
        $invwhole = $postData['sin'];
        $inv = substr($invwhole, 3);
        ///exit;
        $result = $this->pos_mangement->getOrderDetailsByInId($inv);
        //  exit;
        if ($result) {
            if ($result->order_status == 'hold') {
                $data['order_status'] = 'ready';
                $data['update_status_id'] = $bs_userid;
                $this->db->where('invoice_id', $inv);
                echo $this->db->update('orders', $data);
            } elseif ($result->order_status == 'ready') {
                echo "2";
            } else {
                echo "3";
            }
        } else {
            echo "0";
        }
    }

    function save_payment_amount() {
        $this->load->model('users/users_model', 'users');
        $postData = $this->input->post();

        $uid = $postData['uid'];
        $am = $postData['am'];
        $order_amount = $postData['order_amount'];
        ///$uData = $this->users->getUserOrderById($uid);

        $clearData['user_id'] = $uid;
        $clearData['clear_amount'] = $am;
        $clearData['order_amount'] = $order_amount;
        $clearData['clear_by'] = $this->session->userdata('bs_userid');
        echo $clear_id = $this->pos_mangement->inserData('clear_users_orders', $clearData);
    }

    function save_payment_amountN() {

        $this->load->model('users/users_model', 'users');

        $postData = $this->input->post();



        //   echo "<pre>";
        //   print_r($postData);



        $uid = $postData['uid'];

        $am = $postData['am'];

        $clearby = $this->session->userdata('bs_username');



        $ud = $this->pos_mangement->get_user_detail($uid);

        $notes = getAmountFormat($am) . ' Amount  Receive By  ' . $clearby . ' for User ' . $ud->fullname;

        //save_user_log('receive',$notes);



        $order_amount = $postData['order_amount'];

        $clearData['user_id'] = $uid;

        $clearData['clear_amount'] = $am;

        $clearData['order_amount'] = $order_amount;

        $clearData['clear_by'] = $this->session->userdata('bs_userid');

        $clear_id = $this->pos_mangement->inserData('clear_users_orders', $clearData);

        $clearPaymentData['clear_id'] = $clear_id;
        $clearPaymentData['user_id'] = $uid;
        $clearPaymentData['clear_amount'] = $am;
        //print_r($clearPaymentData);
        echo $this->pos_mangement->inserData('clear_order_payment', $clearPaymentData);
    }

    function save_payment_amount_march() {
        $this->load->model('users/users_model', 'users');
        $postData = $this->input->post();

        //   echo "<pre>";
        //   print_r($postData);


        $uid = $postData['uid'];
        $am = $postData['am'];
        $order_amount = $postData['order_amount'];
        $uData = $this->users->getUserOrderById($uid);

        //echo "<pre>";
        // print_r($uData);
        //  exit;
        if ($am != $uData->totalpendingamount) {

            //echo "iff";
            //exit;
            $clearData['user_id'] = $uid;
            $clearData['clear_amount'] = $am;
            $clearData['order_amount'] = $order_amount;
            $clearData['clear_by'] = $this->session->userdata('bs_userid');
            $clear_id = $this->pos_mangement->inserData('clear_users_orders', $clearData);
            $orderslist = $this->pos_mangement->getOrderListById($uid);
            //print_r($orderslist);
            //exit;

            if ($orderslist) {
                foreach ($orderslist as $orders) {

                    //am
                    if ($am != "" && $am != "0.000") {



                        $paidOrder = $orders->totalpaid + $am;

                        if ($orders->order_amount <= $am) {
                            $orderData['clear_order'] = '1';
                            $this->db->where('order_id', $orders->order_id);
                            $this->db->update('orders', $orderData);
                            $clear_amount = $orders->order_amount - $orders->totalpaid;
                            $am = $am - $clear_amount;
                        } elseif ($orders->order_amount == $paidOrder) {
                            $orderData['clear_order'] = '1';
                            $this->db->where('order_id', $orders->order_id);
                            $this->db->update('orders', $orderData);
                            $clear_amount = $orders->order_amount - $orders->totalpaid;
                            $am = $am - $clear_amount;
                        } elseif ($orders->order_amount < $paidOrder) {
                            $orderData['clear_order'] = '1';
                            $this->db->where('order_id', $orders->order_id);
                            $this->db->update('orders', $orderData);
                            $clear_amount = $orders->order_amount - $orders->totalpaid;
                            $am = $am - $clear_amount;
                        } else {

                            $clear_amount = $am;
                            $am = $am - $clear_amount;
                        }
                        //echo $am;
                        $clearPaymentData['order_id'] = $orders->order_id;
                        $clearPaymentData['clear_id'] = $clear_id;
                        $clearPaymentData['user_id'] = $uid;
                        $clearPaymentData['clear_amount'] = $clear_amount;
                        //print_r($clearPaymentData);
                        $this->pos_mangement->inserData('clear_order_payment', $clearPaymentData);
                    }
                }
            }
        } else {
            //echo "else";
            //exit;
            $clearData['user_id'] = $uid;
            $clearData['clear_amount'] = $am;
            $clearData['order_amount'] = $order_amount;
            $clearData['clear_by'] = $this->session->userdata('bs_userid');
            $clear_id = $this->pos_mangement->inserData('clear_users_orders', $clearData);
            $orderslist = $this->pos_mangement->getOrderListById($uid);
            //print_r($orderslist);
            //exit;
            if ($orderslist) {
                foreach ($orderslist as $orders) {
                    $orderData['clear_order'] = '1';
                    $this->db->where('order_id', $orders->order_id);
                    $this->db->update('orders', $orderData);

                    $clear_amount = $orders->order_amount - $orders->totalpaid;
                    $am = $am - $clear_amount;

                    $clearPaymentData['order_id'] = $orders->order_id;
                    $clearPaymentData['clear_id'] = $clear_id;
                    $clearPaymentData['user_id'] = $uid;
                    $clearPaymentData['clear_amount'] = $clear_amount;
                    //print_r($clearPaymentData);

                    $this->pos_mangement->inserData('clear_order_payment', $clearPaymentData);
                }
            }
        }
    }

    function save_payment_amount_old() {
        $this->load->model('users/users_model', 'users');
        $postData = $this->input->post();

        //echo "<pre>";
        //print_r($postData);
        $uid = $postData['uid'];
        $am = $postData['am'];
        $order_amount = $postData['order_amount'];
        $uData = $this->users->getUserOrderById($uid);
        if ($am != $uData->totalpendingamount) {

            //echo "iff";
            //exit;
            $clearData['user_id'] = $uid;
            $clearData['clear_amount'] = $am;
            $clearData['order_amount'] = $order_amount;
            $clearData['clear_by'] = $this->session->userdata('bs_userid');
            $clear_id = $this->pos_mangement->inserData('clear_users_orders', $clearData);
            $orderslist = $this->pos_mangement->getOrderListById($uid);
            //print_r($orderslist);
            //exit;

            if ($orderslist) {
                foreach ($orderslist as $orders) {

                    //am
                    if ($am != "" && $am != "0.000") {
                        if ($orders->order_amount <= $am) {
                            $orderData['clear_order'] = '1';
                            $this->db->where('order_id', $orders->order_id);
                            $this->db->update('orders', $orderData);
                            $clear_amount = $orders->order_amount - $orders->totalpaid;
                            $am = $am - $clear_amount;
                        } else {

                            $clear_amount = $am;
                            $am = $am - $clear_amount;
                        }
                        //echo $am;
                        $clearPaymentData['order_id'] = $orders->order_id;
                        $clearPaymentData['clear_id'] = $clear_id;
                        $clearPaymentData['user_id'] = $uid;
                        $clearPaymentData['clear_amount'] = $clear_amount;
                        //print_r($clearPaymentData);
                        $this->pos_mangement->inserData('clear_order_payment', $clearPaymentData);
                    }
                }
            }
        } else {
            //echo "else";
            //exit;
            $clearData['user_id'] = $uid;
            $clearData['clear_amount'] = $am;
            $clearData['order_amount'] = $order_amount;
            $clearData['clear_by'] = $this->session->userdata('bs_userid');
            $clear_id = $this->pos_mangement->inserData('clear_users_orders', $clearData);
            $orderslist = $this->pos_mangement->getOrderListById($uid);
            //print_r($orderslist);
            //exit;
            if ($orderslist) {
                foreach ($orderslist as $orders) {
                    $orderData['clear_order'] = '1';
                    $this->db->where('order_id', $orders->order_id);
                    $this->db->update('orders', $orderData);

                    $clear_amount = $orders->order_amount - $orders->totalpaid;
                    $am = $am - $clear_amount;

                    $clearPaymentData['order_id'] = $orders->order_id;
                    $clearPaymentData['clear_id'] = $clear_id;
                    $clearPaymentData['user_id'] = $uid;
                    $clearPaymentData['clear_amount'] = $clear_amount;
                    //print_r($clearPaymentData);

                    $this->pos_mangement->inserData('clear_order_payment', $clearPaymentData);
                }
            }
        }
    }

    function getReadyCount() {
        $bs_userid = $this->session->userdata('bs_userid');
        echo $this->pos_mangement->getReadyOrdersCount($bs_userid);
    }

    function update_invoice_status() {

        $this->load->view('pos_view_update', $this->_data);
    }

    function get_pending_orders() {
        
    }

    function add_customer() {
        $postData = $this->input->post();
        $customerData['fullname'] = $postData['name'];
        $customerData['phone_number'] = $postData['phone'];
        $customerData['address'] = $postData['address'];
        $customerData['notes'] = $postData['work'];
        $customerData['member_type'] = 6;
        $customerData['status'] = 'A';
        $customer_id = $this->pos_mangement->inserData('bs_users', $customerData);
        echo json_encode($customer_id);
    }

    function save_newpost_data() {
        $postData = $this->input->post();
        /* echo '<pre>';
          print_r($postData['store_id']);
          exit(); */
        $quantity = $postData['quantity'];
        $total = count($quantity);

        $totalprice = 0;
        $bs_branchid = $this->session->userdata('bs_branchid');
        $bs_userid = $this->session->userdata('bs_userid');

        if ($postData['customer_id'] == "") {

            $uid = $this->pos_mangement->getByUserid($postData['customer_name']);
            if ($uid) {
                $postData['customer_id'] = $uid;
            } else {
                $customerData['fullname'] = $postData['customer_name'];
                $customerData['phone_number'] = $postData['phone_number'];
                $customerData['member_type'] = 6;
                $customerData['status'] = 'A';
                $postData['customer_id'] = $this->pos_mangement->inserData('bs_users', $customerData);
            }
        }


        if ($postData['inv_id'] != "") {

            $insertid = $postData['inv_id'];
            $invData = $this->pos_mangement->getInvoiceById($insertid);
            $copData = $this->pos_mangement->getCoupon($insertid);
            $totalprice = 0;
            $coupon_value = $invData->coupon_value;

            $ordered_ids = [];
            $ordered_prcs = [];
            $ordered_qtys = [];
            $this->db->where('invoice_id', $insertid);
            $this->db->delete('bs_invoice_items');
            $this->db->where('invoice_id', $insertid);
            $this->db->delete('pos_additional_items');
            $this->db->where('invoice_id', $insertid);
            $this->db->delete('more_items');
            $this->db->where('recipt_id', $insertid);
            $this->db->delete('store_soled_items');
            for ($a = 0; $a < $total; $a++) {
                $postImageInvoice = array();
                // $itemnv = $postData['iteminv'][$a];
                // if ($itemnv == "") {
                array_push($ordered_ids, $postData['itemid'][$a]);
                array_push($ordered_prcs, $postData['itemperprice'][$a]);
                array_push($ordered_qtys, $postData['quantity'][$a]);
                $postImageInvoice['invoice_id'] = $insertid;
                $postImageInvoice['invoice_item_quantity'] = $postData['quantity'][$a];
                $postImageInvoice['invoice_item_id'] = $postData['itemid'][$a];

                $postImageInvoice['invoice_item_store_id'] = $postData['store_id'][$a];
                $postImageInvoice['invoice_item_price'] = $postData['itemperprice'][$a];
                $postImageInvoice['confirm_status'] = 1;
                $totalprice = $totalprice + $postData['itemtotalprice'][$a];

                $invitemid = $this->pos_mangement->inserData('bs_invoice_items', $postImageInvoice);

                $additionalItems = $postData['additionalItems'];

                if (isset($postData['additionalItems'][$postData['itemid'][$a]])) {
                    $addtionalitemsDetails = $postData['additionalItems'][$postData['itemid'][$a]];
                    if ($addtionalitemsDetails) {
                        foreach ($addtionalitemsDetails as $itemDetails) {
                            $additemData['pos_item_id'] = $postData['itemid'][$a];
                            $additemData['additional_item_price'] = $itemDetails['item_price'];
                            $additemData['additional_item_val'] = $itemDetails['item_val'];
                            $additemData['invoice_id'] = $insertid;
                            $additemData['invoice_itempk_id'] = $invitemid;
                            $additemData['additional_item_id'] = $itemDetails['item_id'];
                            $this->pos_mangement->inserData('pos_additional_items', $additemData);
                        }
                    }
                }

                $moreItems = $postData['moreItems'];
                $randid = $postData['itemrand'][$a];
                if (isset($postData['moreItems'][$randid])) {
                    $addtionalMoreitemsDetails = $postData['moreItems'][$randid];
                    if ($addtionalMoreitemsDetails) {
                        foreach ($addtionalMoreitemsDetails as $itemDetails) {
                            // $moreitemData['more_item_id'] = $postData['itemid'][$a];
                            $moreitemData['link_price'] = $itemDetails['item_price'];
                            $moreitemData['invoice_id'] = $insertid;
                            $moreitemData['invoice_item_id'] = $invitemid;
                            $moreitemData['invoice_itempk_id'] = $postData['itemid'][$a];
                            $moreitemData['link_id'] = $itemDetails['item_id'];
                            $this->pos_mangement->inserData('more_items', $moreitemData);
                        }
                    }
                }

                $postImageInvoice = array();
                // $postImageInvoice['invoice_id'] = $insertid;
                $postImageInvoice['soled_quantity'] = $postData['quantity'][$a];
                $postImageInvoice['item_id'] = $postData['itemid'][$a];
                $postImageInvoice['store_id'] = $postData['store_id'][$a];
                $postImageInvoice['userid'] = $bs_userid;
                $postImageInvoice['recipt_id'] = $insertid;
                //   $totalprice = $totalprice+$postData['itemtotalprice'][$a];
                $invitemid = $this->pos_mangement->inserData('store_soled_items', $postImageInvoice);
                //}
                //moreItems
            }

            $arr = pocess_coupon($copData, $ordered_ids, $ordered_prcs, $ordered_qtys);

            if ($copData['dis_type'] == 'percent') {
                $totalprice -= $arr[1];
                $data['coupon_value'] = $coupon_value + $arr[1];
            }
            $data['invoice_total_amount'] = $totalprice;
            $data['sales_amount'] = $totalprice;
            $data['pos_payable_val'] = $totalprice;
            $this->db->where('invoice_id', $insertid);
            $this->db->update('an_invoice', $data);


            $pData['invoice_id'] = $insertid;
            $pData['user_id'] = $bs_userid;
            $pData['order_amount'] = $totalprice;
            $pData['order_date'] = date('Y-m-d H:i:s');
            $this->db->where('invoice_id', $insertid);
            $this->db->update('orders', $pData);

            //minus from store
            for ($a = 0; $a < $total; $a++) {
                $itemid = $postData['itemid'][$a];
                $qty = $postData['quantity'][$a];
                $itemtype = $postData['itemtype'][$a] ? $postData['itemtype'][$a] : 'large';
                $query = $this->db->get_where('dish_content', array('item_id' => $itemid, 'size' => strtolower($itemtype)));

                if ($query->num_rows() > 0) {
                    insertRawData($query->result(), $itemid, $bs_userid, $insertid, $postData['store_id'][$a], $qty);
                }
            }
        } else {
            // coupon process 
            $arr = pocess_coupon($postData['coupon'], $postData['itemid'], $postData['itemperprice'], $postData['quantity']);

            $posInvoice['branchid'] = $bs_branchid;
            $posInvoice['user_id'] = $bs_userid;
            $posInvoice['invoice_totalDiscount'] = $postData['discount_final_val'];
            $posInvoice['pos_paid'] = $postData['total_paid'];
            $posInvoice['invoice_type'] = 'pos';

            $posInvoice['card_number'] = $postData['card_no'] || 0;

            $posInvoice['pos_discount_type'] = $postData['discount'];
            $posInvoice['invoice_payment_status'] = $postData['pos_invoice_status'];
            $posInvoice['customer_id'] = $postData['customer_id'];
            $posInvoice['companyid'] = $this->session->userdata('bs_companyid');

            $posInvoice['coupon_value'] = $arr[1]; // coupon process

            $insertid = $this->pos_mangement->inserData('an_invoice', $posInvoice);

            // coupon process
            if ($arr[2]) {// if there is a coupon dicount
                $cop['invoice_id'] = $insertid;
                $cop['coupon_id'] = $arr[2];
                $cop['customer_id'] = $postData['customer_id'];
                $this->pos_mangement->inserData('invoice_coupon', $cop);
            }


            for ($a = 0; $a < $total; $a++) {
                $postImageInvoice = array();
                $postImageInvoice['invoice_id'] = $insertid;
                $postImageInvoice['invoice_item_quantity'] = $postData['quantity'][$a];
                $postImageInvoice['invoice_item_id'] = $postData['itemid'][$a];


                $postImageInvoice['invoice_item_store_id'] = $postData['store_id'][$a];
                $postImageInvoice['invoice_item_price'] = $postData['itemperprice'][$a];
                $postImageInvoice['confirm_status'] = 1;
                $totalprice = $totalprice + $postData['itemtotalprice'][$a];
                $invitemid = $this->pos_mangement->inserData('bs_invoice_items', $postImageInvoice);

                $additionalItems = $postData['additionalItems'];

                if (isset($postData['additionalItems'][$postData['itemid'][$a]])) {
                    $addtionalitemsDetails = $postData['additionalItems'][$postData['itemid'][$a]];
                    if ($addtionalitemsDetails) {
                        foreach ($addtionalitemsDetails as $itemDetails) {
                            $additemData['pos_item_id'] = $postData['itemid'][$a];
                            $additemData['additional_item_price'] = $itemDetails['item_price'];
                            $additemData['additional_item_val'] = $itemDetails['item_val'];
                            $additemData['invoice_id'] = $insertid;
                            $additemData['invoice_itempk_id'] = $invitemid;
                            $additemData['additional_item_id'] = $itemDetails['item_id'];
                            $this->pos_mangement->inserData('pos_additional_items', $additemData);
                        }
                    }
                }


                $moreItems = $postData['moreItems'];
                $randid = $postData['itemrand'][$a];
                if (isset($postData['moreItems'][$randid])) {
                    $addtionalMoreitemsDetails = $postData['moreItems'][$randid];
                    if ($addtionalMoreitemsDetails) {
                        foreach ($addtionalMoreitemsDetails as $itemDetails) {
                            // $moreitemData['more_item_id'] = $postData['itemid'][$a];
                            $moreitemData['link_price'] = $itemDetails['item_price'];
                            $moreitemData['invoice_id'] = $insertid;
                            $moreitemData['invoice_item_id'] = $invitemid;
                            $moreitemData['invoice_itempk_id'] = $postData['itemid'][$a];
                            $moreitemData['link_id'] = $itemDetails['item_id'];
                            $this->pos_mangement->inserData('more_items', $moreitemData);
                        }
                    }
                }

                //moreItems

                $postImageInvoice = array();
                // $postImageInvoice['invoice_id'] = $insertid;
                $postImageInvoice['soled_quantity'] = $postData['quantity'][$a];
                $postImageInvoice['item_id'] = $postData['itemid'][$a];
                $postImageInvoice['store_id'] = $postData['store_id'][$a];
                $postImageInvoice['userid'] = $bs_userid;
                $postImageInvoice['recipt_id'] = $insertid;
                //   $totalprice = $totalprice+$postData['itemtotalprice'][$a];
                $invitemid = $this->pos_mangement->inserData('store_soled_items', $postImageInvoice);
            }

            if ($postData['discount_final_val'] > 0 && $postData['discount_final_val'] != "") {

                $totalprice = $totalprice - $postData['discount_final_val'];
            }

            $totalprice -= $arr[1]; // coupon process

            $data['invoice_total_amount'] = $totalprice;
            $data['sales_amount'] = $totalprice;
            $data['pos_payable_val'] = $postData['total_paid'];
            $this->db->where('invoice_id', $insertid);
            $this->db->update('an_invoice', $data);

            $pData['invoice_id'] = $insertid;
            $pData['user_id'] = $bs_userid;
            $pData['order_amount'] = $totalprice;
            // $pData['order_status'] = 'ready';
            $pData['order_date'] = date('Y-m-d H:i:s');
            $paym_id = $this->pos_mangement->inserData('orders', $pData);
            $this->create_barcode($insertid);


            //minus from store
            for ($a = 0; $a < $total; $a++) {
                $itemid = $postData['itemid'][$a];
                $itemtype = $postData['itemtype'][$a] ? $postData['itemtype'][$a] : 'large';
                $qty = $postData['quantity'][$a];
                //$type = $postData['itemtpcat'][$a];
                $query = $this->db->get_where('dish_content', array('item_id' => $itemid, 'size' => strtolower($itemtype)));

                if ($query->num_rows() > 0) {
                    insertRawData($query->result(), $itemid, $bs_userid, $insertid, $postData['store_id'][$a], $qty);
                }
            }
            //
            echo $insertid;
        }
    }

    function continueChecking($res) {
        $days = array('', 'All', 'Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri');
        $t = date('d-m-Y');
        $today = date("D", strtotime($t));
        $myArray = explode(',', $res->active_days);
        $dayCheck = FALSE;
        foreach ($myArray as $value) {
            if ($days[$value] == $today || $value == 1) {
                $dayCheck = TRUE;
                break;
            }
        }
        if ($dayCheck) {
            if ($res->hide == 0) {
                $return = ['الكوبون صالح للإستخدام', 5, $res];
                return $return;
            } else {
                $return = ['الكوبون معطل', 4, $res];
                return $return;
            }
        } else {
            $return = ['غير صالح في هذا اليوم', 3, $res];
            return $return;
        }
    }

    function checkCoupon() {
        $return = [];
        $postData = $this->input->post();
        $coupon = $postData['coupon'];

        $query = "SELECT `coupon`.*,fullname AS cust_name,COUNT(`invoice_coupon`.`coupon_id`) AS used FROM coupon"
                . " LEFT JOIN bs_users ON bs_users.userid = customer_id LEFT JOIN `invoice_coupon` ON `invoice_coupon`.`coupon_id` = `coupon`.`coupon_id`"
                . " WHERE coupon_string = '$coupon'";
        $sql = $this->db->query($query);
        $res = $sql->row();
        if ($res->coupon_id) {
            if ($res->is_used == 0) {
                if ($res->start_date <= date("Y-m-d") && $res->end_date >= date("Y-m-d")) {
                    echo json_encode($this->continueChecking($res));
                } else {
                    $return = ['الكوبون منتهي الصلاحية', 2, $res];
                    echo json_encode($return);
                }
            } else {
                $return = ['الكوبون استخدم من قبل', 1, $res];
                echo json_encode($return);
            }
        } else {
            $return = ['الكوبون غير موجود'];
            echo json_encode($return);
        }
    }

    function get_update_orders() {

        $postData = $this->input->post();
        $ord = $postData['ordnumbr'];

        //$this->_data['holding_orders'] = $this->pos_mangement->getHoldOrdersList($ord);
        // $this->_data['holding_orders_group'] = $this->pos_mangement->getholdingOrdersAjaxGroup($ord,'cold');
        $this->_data['holding_orders_total'] = $this->pos_mangement->getholdingOrdersByGroup('cold');
        //$this->_data['holding_orders'] = $this->pos_mangement->getholdingOrdersAjax($ord,'cold');
        $this->_data['holding_orders'] = $this->pos_mangement->getholdingOrders('cold');
        //ordnumbr
        if (!empty($this->_data['holding_orders'])) {

            $screenview = $this->load->view('screen_view_ajax_new', $this->_data, true);
            $response = array('html' => $screenview, 'ordlist' => $this->_data['holding_orders'], 'ordergroup' => $this->_data['holding_orders_total']);
            echo json_encode($response);
        } else {
            echo false;
        }
    }

    function get_update_orders2() {

        $postData = $this->input->post();
        $ord = $postData['ordnumbr'];

        //$this->_data['holding_orders'] = $this->pos_mangement->getHoldOrdersList($ord);
        // $this->_data['holding_orders_group'] = $this->pos_mangement->getholdingOrdersAjaxGroup($ord,'cold');
        $this->_data['holding_orders_total'] = $this->pos_mangement->getholdingOrdersByGroup('hot');
        //$this->_data['holding_orders'] = $this->pos_mangement->getholdingOrdersAjax($ord,'cold');
        $this->_data['holding_orders'] = $this->pos_mangement->getholdingOrders('hot');
        //ordnumbr
        if (!empty($this->_data['holding_orders'])) {

            $screenview = $this->load->view('screen_view_ajax_new', $this->_data, true);
            $response = array('html' => $screenview, 'ordlist' => $this->_data['holding_orders'], 'ordergroup' => $this->_data['holding_orders_total']);
            echo json_encode($response);
        } else {
            echo false;
        }
    }

    function get_update_orders2_old() {

        $postData = $this->input->post();
        $ord = $postData['ordnumbr'];

        //$this->_data['holding_orders'] = $this->pos_mangement->getHoldOrdersList($ord);
        $this->_data['holding_orders_group'] = $this->pos_mangement->getholdingOrdersAjaxGroup($ord, 'cold');
        $this->_data['holding_orders'] = $this->pos_mangement->getholdingOrdersAjax($ord, 'cold');
        //ordnumbr
        if (!empty($this->_data['holding_orders'])) {

            $screenview = $this->load->view('screen_view_ajax', $this->_data, true);
            $response = array('html' => $screenview, 'ordlist' => $this->_data['holding_orders'], 'ordergroup' => $this->_data['holding_orders_group']);
            echo json_encode($response);
        } else {
            echo false;
        }
    }

    function save_pos_data() {
        $postData = $this->input->post();
        //	        echo "<pre>";
        //			print_r($postData);
        //			exit;
        $quantity = $postData['quantity'];
        $total = count($quantity);

        $bs_branchid = $this->session->userdata('bs_branchid');
        $bs_userid = $this->session->userdata('bs_userid');
        //pos invoce data
        //if($postData['customer_id']  == '')
        //$posInvoice['invoice_deal_type'] = 'cash';
        //if($postData['customer_id']  == '')
        //$posInvoice['invoice_deal_type'] = 'cash';
        //else
        //$posInvoice['invoice_deal_type'] = 'customer';

        $posInvoice['invoice_deal_type'] = $postData['invoice_paymemt_type'];



        if ($postData['inv_id'] != "" && $postData['invoice_type'] == 'hold') {
            //echo "if";
            //exit;

            $posInvoice['branchid'] = $bs_branchid;
            $posInvoice['user_id'] = $bs_userid;
            $posInvoice['invoice_totalDiscount'] = $postData['discount_val'];
            $posInvoice['pos_paid'] = $postData['total_paid'];
            $posInvoice['invoice_type'] = 'pos';
            $posInvoice['pos_discount_type'] = $postData['discount'];
            $posInvoice['invoice_payment_status'] = $postData['pos_invoice_status'];
            $posInvoice['card_number'] = $postData['card_no'];
            //$posInvoice['customer_id'] = $postData['customer_id'];
            $posInvoice['companyid'] = $this->session->userdata('bs_companyid');
            //$posInvoice['invoice_status'] = $postData['pos_invoice_status'];
            //echo "";
            //print_r($posInvoice);
            //$insertid = $this->pos_mangement->inserData('an_invoice', $posInvoice);
            $where = array('invoice_id' => $postData['inv_id']);
            $insertid = $this->pos_mangement->updateData('an_invoice', $posInvoice, $where);
            //  exit;
        } elseif ($postData['inv_id'] == "" && $postData['invoice_type'] != 'hold' && $postData['invoice_type'] != 'my') {
            //echo "elseif";
            //exit;
            $posInvoice['branchid'] = $bs_branchid;
            $posInvoice['user_id'] = $bs_userid;
            $posInvoice['invoice_totalDiscount'] = $postData['discount_val'];
            $posInvoice['pos_paid'] = $postData['total_paid'];
            $posInvoice['invoice_type'] = 'pos';
            $posInvoice['card_number'] = $postData['card_no'];
            $posInvoice['pos_discount_type'] = $postData['discount'];
            $posInvoice['invoice_payment_status'] = $postData['pos_invoice_status'];
            $posInvoice['customer_id'] = $postData['customer_id'];
            $posInvoice['companyid'] = $this->session->userdata('bs_companyid');
            //$posInvoice['invoice_status'] = $postData['pos_invoice_status'];
            //echo "";
            //print_r($posInvoice);
            $insertid = $this->pos_mangement->inserData('an_invoice', $posInvoice);
        } elseif ($postData['invoice_type'] == 'my') {
            //echo "elsemy";
            //exit;

            $pData = $this->input->post();
            //echo "<pre>";
            //  print_r($pData);
            // exit;
            $this->update_cancel_invoice();
        }
        //exit;


        $postImageInvoice = array();
        $totalprice = 0;

        for ($a = 0; $a < $total; $a++) {
            $postImageInvoice = array();

            if ($postData['inv_id'] != "" && $postData['invoice_type'] == 'hold') {
                $postImageInvoice['invoice_id'] = $postData['inv_id'];
                $postImageInvoice['invoice_item_quantity'] = $postData['quantity'][$a];
                $postImageInvoice['invoice_item_id'] = $postData['itemid'][$a];
                if ($postData['storeid'] == '') {
                    $postData['storeid'] = 1;
                }
                $postImageInvoice['invoice_item_store_id'] = $postData['storeid'];
                $postImageInvoice['invoice_item_price'] = $postData['itemperprice'][$a];
                $postImageInvoice['confirm_status'] = 1;
                $totalprice = $totalprice + $postData['itemtotalprice'][$a];
                //$invitemid = $this->pos_mangement->inserData('bs_invoice_items', $postImageInvoice);
                $where = array('invoice_id' => $postData['inv_id'], 'invoice_item_id' => $postImageInvoice['item_id']);
                $insertid = $this->pos_mangement->updateData('bs_invoice_items', $postImageInvoice, $where);
                $postImageInvoice = array();
                // $postImageInvoice['invoice_id'] = $insertid;
                $postImageInvoice['soled_quantity'] = $postData['quantity'][$a];
                $postImageInvoice['item_id'] = $postData['itemid'][$a];
                $postImageInvoice['store_id'] = $postData['storeid'];
                $postImageInvoice['userid'] = $bs_userid;
                $postImageInvoice['recipt_id'] = $postData['inv_id'];
                //   $totalprice = $totalprice+$postData['itemtotalprice'][$a];
                //$invitemid = $this->pos_mangement->inserData('store_soled_items', $postImageInvoice);

                $where = array('recipt_id' => $postData['inv_id'], 'item_id' => $postImageInvoice['item_id']);
                $insertid = $this->pos_mangement->updateData('store_soled_items', $postImageInvoice, $where);
                $insertid = $postData['inv_id'];
            } elseif ($postData['inv_id'] == "" && $postData['invoice_type'] != 'hold' && $postData['invoice_type'] != 'my') {

                $postImageInvoice['invoice_id'] = $insertid;
                $postImageInvoice['invoice_item_quantity'] = $postData['quantity'][$a];
                $postImageInvoice['invoice_item_id'] = $postData['itemid'][$a];
                if ($postData['storeid'] == '') {
                    $postData['storeid'] = 1;
                }
                $postImageInvoice['invoice_item_store_id'] = $postData['storeid'];
                $postImageInvoice['invoice_item_price'] = $postData['itemperprice'][$a];
                $postImageInvoice['confirm_status'] = 1;
                $totalprice = $totalprice + $postData['itemtotalprice'][$a];
                $invitemid = $this->pos_mangement->inserData('bs_invoice_items', $postImageInvoice);
                $postImageInvoice = array();
                // $postImageInvoice['invoice_id'] = $insertid;
                $postImageInvoice['soled_quantity'] = $postData['quantity'][$a];
                $postImageInvoice['item_id'] = $postData['itemid'][$a];
                $postImageInvoice['store_id'] = $postData['storeid'];
                $postImageInvoice['userid'] = $bs_userid;
                $postImageInvoice['recipt_id'] = $insertid;
                //   $totalprice = $totalprice+$postData['itemtotalprice'][$a];
                $invitemid = $this->pos_mangement->inserData('store_soled_items', $postImageInvoice);
            }
        }

        //$discount = $postData['discount'];	
        if ($postData['discount'] == 'percent') {
            //$postUpInv['pos_payable_val'];
            $final = $totalprice * $postData['discount_val'];
            $final = $final / 100;
            $finish = $totalprice - $final;
        } else {
            $finish = $totalval - $disc;
        }

        if ($postData['invoice_type'] != 'my') {

            $data['invoice_total_amount'] = $totalprice;
            $data['sales_amount'] = $totalprice;
            $data['pos_payable_val'] = $finish;
            $this->db->where('invoice_id', $insertid);
            $this->db->update('an_invoice', $data);
        }


        if (trim($postData['pos_invoice_status']) == 'payment' && $postData['invoice_type'] != 'my') {

            $pData['payment_type'] = 'receipt';
            $pData['refernce_type'] = 'against';
            $pData['payment_amount'] = $totalprice;
            $pData['refrence_id'] = $postData['customer_id'];
            $pData['payment_date'] = date('Y-m-d');
            $paym_id = $this->pos_mangement->inserData('payments', $pData);

            $pinvData['payment_id'] = $paym_id;
            $pinvData['payment_amount'] = $totalprice;
            $pinvData['invoice_id'] = $insertid;
            $pinvData['payment_type'] = 1;
            $pinvData['payment_date'] = date('Y-m-d');
            $pinvData['datetopay'] = date('Y-m-d');
            $inv_p_id = $this->pos_mangement->inserData('invoice_payments', $pinvData);
        }

        $pData = array();
        $pData['invoice_id'] = $insertid;
        $pData['user_id'] = $bs_userid;
        $pData['order_amount'] = $totalprice;
        $pData['order_date'] = date('Y-m-d H:i:s');
        $paym_id = $this->pos_mangement->inserData('orders', $pData);

        echo $insertid;
    }

}
