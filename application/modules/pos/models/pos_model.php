<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pos_model extends CI_Model {
    /*

     * Properties

     */

//----------------------------------------------------------------------



    /*

     * Constructor

     */



    function __construct() {

        parent::__construct();



        //Load Table Names from Config
    }

//----------------------------------------------------------------------	

    function inserData($table, $data) {

        $this->db->insert($table, $data);

        return $this->db->insert_id();
    }

    function updateData($table, $data, $where) {

        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    function update_getpending($id, $data) {

        $this->db->where('quotation_id', $id);

        $this->db->update('an_quotation', $data);

        //return $this->db->insert_id();

        return TRUE;
    }

    function update_items($id, $data) {

        $this->db->where('quotation_product_id', $id);

        $this->db->update('bs_quotation_items', $data);

        //return $this->db->insert_id();

        return TRUE;
    }

    public function get_user_detail($id) {

        $this->db->where('userid', $id);

        $query = $this->db->get('bs_users');



        if ($query->num_rows() > 0) {

            return $query->row();
        }
    }

    function getCoupon($id) {
        $sql = "SELECT * FROM invoice_coupon INNER JOIN `coupon` ON `invoice_coupon`.`coupon_id` = `coupon`.`coupon_id` WHERE invoice_id = $id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    function getInvoiceById($id) {
        $sql = "SELECT * FROM `an_invoice`  AS i WHERE i.`invoice_id` = '" . $id . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->row();
        } else {
            return false;
        }
    }

    function getOrderIdByInId($id) {
        $sql = "SELECT * FROM `orders` AS o WHERE o.`invoice_id` = '" . $id . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->row()->order_id;
        } else {
            return false;
        }
    }

    function getOrderListById($id) {
        $sql = "SELECT
  od.*,
totalpaid
FROM
  `bs_users` AS u
  LEFT JOIN `orders` AS od
    ON od.`user_id` = u.`userid`
  LEFT JOIN
    (SELECT
      crp.`order_id`,
      IFNULL(SUM(crp.`clear_amount`), 0) AS totalpaid
    FROM
      `clear_order_payment` AS crp
      WHERE crp.`user_id` = '" . $id . "'
    GROUP BY crp.`order_id`)  AS ccrp
    ON ccrp.order_id = od.`order_id`
  WHERE u.`member_type` != '6'
  AND od.`cancel_status` = '0'
  AND u.`userid` = '" . $id . "'
  AND od.`clear_order` = '0'
GROUP BY od.`order_id` ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getOrderListById2($id) {
        $sql = "SELECT
 od.*,IFNULL(SUM(crp.`clear_amount`),0) AS totalpaid
FROM
  `bs_users` AS u
  LEFT JOIN `orders` AS od
    ON od.`user_id` = u.`userid`
	LEFT JOIN `clear_order_payment` AS crp 
    ON crp.order_id = od.`order_id` 
WHERE u.`member_type` != '6' AND u.`userid` = '" . $id . "'  AND od.`clear_order` = '0' GROUP BY od.`order_id`";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getUserOrderById($userid) {
        $sql = "SELECT
  u.*,
  COUNT(od.`user_id`) AS totalorders,
  IFNULL(totalpendingamount, 0) AS totalpendingamount,
  IFNULL(totalpendingorders, 0) AS totalpendingorders
FROM
  `bs_users` AS u
  LEFT JOIN `orders` AS od
    ON od.`user_id` = u.`userid`
  LEFT JOIN
    (SELECT
      od2.`user_id`,
      SUM(od2.`order_amount`) AS totalpendingamount,
      COUNT(od2.`user_id`) AS totalpendingorders
    FROM
      `orders` AS od2
    WHERE od2.`clear_order` = '0'
    GROUP BY od2.`user_id`) AS odd2
    ON odd2.user_id = u.`userid`
WHERE u.`member_type` != '6' AND u.`userid` = '" . $userid . "'
ORDER BY COUNT(od.`user_id`) DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function getUserOrderById2($userid) {
        $sql = "SELECT
  u.*,
  COUNT(od.`user_id`) AS totalorders,
  IFNULL(totalpendingamount, 0) AS totalpendingamount,
  IFNULL(totalpendingorders, 0) AS totalpendingorders
FROM
  `bs_users` AS u
  LEFT JOIN `orders` AS od
    ON od.`user_id` = u.`userid`
  LEFT JOIN
    (SELECT
      od2.`user_id`,
      SUM(od2.`order_amount`) AS totalpendingamount,
      COUNT(od2.`user_id`) AS totalpendingorders
    FROM
      `orders` AS od2
    WHERE od2.`clear_order` = '0'
    GROUP BY od2.`user_id`) AS odd2
    ON odd2.user_id = u.`userid`
WHERE u.`member_type` != '6' AND u.`userid` = '" . $userid . "'
ORDER BY COUNT(od.`user_id`) DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function getUsersOrders() {
        $sql = "SELECT
  u.*,
  COUNT(od.`user_id`) AS totalorders,
  IFNULL(totalpendingamount, 0) AS totalpendingamount,
  IFNULL(totalpendingorders, 0) AS totalpendingorders
FROM
  `bs_users` AS u
  LEFT JOIN `orders` AS od
    ON od.`user_id` = u.`userid`
  LEFT JOIN
    (SELECT
      od2.`user_id`,
      SUM(od2.`order_amount`) AS totalpendingamount,
      COUNT(od2.`user_id`) AS totalpendingorders
    FROM
      `orders` AS od2
    WHERE od2.`clear_order` = '0'
    GROUP BY od2.`user_id`) AS odd2
    ON odd2.user_id = u.`userid`
WHERE u.`member_type` != '6'
GROUP BY u.`userid`
ORDER BY COUNT(od.`user_id`) DESC";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getposlist() {

        $getData = $this->input->get();
        //echo "<pre>";
        //print_r($getData);

        $subsql = '';
        if (isset($getData['frm']) && $getData['frm'] != '') {
            $frm = $getData['frm'];
            $subsql.= 'Where Date(created)>= "' . $frm . '"';
        }
        if (isset($getData['to']) && $getData['to'] != '') {
            if ($subsql != '')
                $subsql .= ' AND ';
            $to = $getData['to'];
            $subsql.= ' Date(created)<= "' . $to . '"';
        }

        if (isset($getData['bid']) && $getData['bid'] != '') {
            if ($subsql != '')
                $subsql .= ' AND ';


            $subsql.= ' pos_branch_id = ' . $getData['bid'];
        }

        $sql = "SELECT 
					  pos.*,
					  u.* ,cb.`branchname`
					FROM
					  `pos_invoices` pos 
					  LEFT JOIN `bs_users` AS u 
						ON u.`userid` = pos.`pos_customer_id` 
					  LEFT JOIN `bs_company_branch` AS cb 
						ON cb.`branchid` = pos.`pos_branch_id` 
					" . $subsql . "
					ORDER BY pos.`pos_id` DESC ";
        $query = $this->db->query($sql);

        return $query->result();
    }

    function getCustomerIdByInvId($invid) {
        $sql = "SELECT
u.`userid`,
u.`fullname` AS csname,coupon_value
FROM
  `an_invoice` AS i
  INNER JOIN `bs_users` AS u
    ON u.`userid` = i.`customer_id`
WHERE i.`invoice_id` = '" . $invid . "' ";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function getSalesInvoiceById($id) {

        $sql = "SELECT
  i.*,bi.*
FROM
  `bs_invoice_items` AS bi
  INNER JOIN `bs_item` AS i
    ON i.`itemid` = bi.`invoice_item_id`
WHERE bi.`invoice_id` = '" . $id . "' ";

//        echo $sql;
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getOrderDetailsByInId($id) {
        $sql = "SELECT * FROM `orders`  AS o WHERE o.`invoice_id` = '" . $id . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function getReadyOrdersList($userid) {
        $sql = "
SELECT
  od.*,u.`fullname`,u.userid
FROM
  `orders` AS od
  INNER JOIN `an_invoice` AS i
    ON i.`invoice_id` = od.`invoice_id`
   LEFT JOIN `bs_users` AS u ON u.`userid` = i.`customer_id`
WHERE od.`user_id` = '" . $userid . "'
  AND order_status = 'ready' or order_status = 'hold' Order by od.order_id DESC";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getMoreItemsByInvItemid($invproid) {
        $sql = "SELECT
  ai.*
FROM
  `more_items` AS mi
  LEFT JOIN `link_items` AS li
    ON li.`link_id` = mi.`link_id`
  LEFT JOIN `additional_items` AS ai
    ON ai.`ad_item_id` = li.`additional_item_id`
WHERE mi.`invoice_item_id` = '" . $invproid . "'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getReadyOrdersCount($userid) {
        $sql = "SELECT
  COUNT(od.`user_id`) AS totalready
FROM
  `orders` AS od
WHERE od.`user_id` = '" . $userid . "'
  AND order_status = 'ready' ";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->row()->totalready;
        } else {
            return false;
        }
    }

    function getByUserid($uname) {
        $sql = "SELECT * FROM `bs_users` AS u WHERE u.`fullname` = '" . $uname . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row()->userid;
        } else {
            return false;
        }
    }

    function getAllinvoiceList() {
        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        $bs_userid = $this->session->userdata('bs_userid');
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;
        $sql = "SELECT
  i.*,
  u.userid,
  u.`fullname`,
  COUNT(it.`invoice_id`) AS totalitems,
  u.`fullname` AS csname,
  u2.`fullname` AS empname
FROM
  `an_invoice` AS i
  LEFT JOIN `bs_users` AS u2
    ON u2.`userid` = i.`user_id`
  LEFT JOIN `bs_invoice_items` AS it
    ON it.`invoice_id` = i.`invoice_id`
  LEFT JOIN `bs_users` AS u
    ON u.`userid` = i.`customer_id`
  INNER JOIN `orders` AS od
    ON od.`invoice_id` = i.`invoice_id`
   WHERE i.`invoice_date` >= '" . $from . "'
  AND i.`invoice_date` <= '" . $to . "' AND i.user_id ='" . $bs_userid . "'
GROUP BY i.`invoice_id`
ORDER BY i.`invoice_id` DESC ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getMoreItems($invitemid) {

        $sql = "SELECT
  ai.*
FROM
  `more_items` AS mi
  LEFT JOIN `link_items` AS li
    ON li.`link_id` = mi.`link_id`
  LEFT JOIN `additional_items` AS ai
    ON ai.`ad_item_id` = li.`additional_item_id`
WHERE mi.`invoice_item_id` = '" . $invitemid . "'";


        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            return $Q->result();
        } else {
            return false;
        }
    }

    function getPosAdditionalItems($id) {

        $sql = "SELECT
  *
FROM
  `additional_items` AS adi
  INNER JOIN `link_items` AS li
    ON li.`additional_item_id` = adi.`ad_item_id`
    WHERE li.`main_item_id` = '" . $id . "' order by `link_id` desc ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getSalesInvoiceByType($type) {
        $bs_userid = $this->session->userdata('bs_userid');
        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;

        if ($type == "hold") {
            $sql = "SELECT
  i.*,u.userid,
  u.`fullname`, COUNT(it.`invoice_id`) AS totalitems,u.`fullname` AS csname
FROM
  `an_invoice` AS i
  LEFT JOIN `bs_invoice_items` AS it
    ON it.`invoice_id` = i.`invoice_id`
  LEFT JOIN `bs_users` AS u
    ON u.`userid` = i.`customer_id`
  INNER JOIN `orders` AS od
    ON od.`invoice_id` = i.`invoice_id`
WHERE od.`order_status` = 'hold'  AND od.`user_id` = '" . $bs_userid . "' AND i.`invoice_date` >= '" . $from . "' AND i.`invoice_date` <= '" . $to . "'
GROUP BY i.`invoice_id`
ORDER BY i.`invoice_id` DESC ";
        } else {
            $id = $this->session->userdata('bs_userid');
            $sql = "SELECT
  i.*,u.`fullname`,
  COUNT(it.`invoice_id`) AS totalitems,u.`fullname` AS csname
FROM
  `an_invoice` AS i
  LEFT JOIN `bs_invoice_items` AS it
    ON it.`invoice_id` = i.`invoice_id`
      LEFT JOIN `bs_users` AS u
    ON u.`userid` = i.`customer_id`
WHERE i.`user_id` = '" . $id . "'
  AND DATE(i.`invoice_date`) = CURRENT_DATE()
    AND i.`invoice_payment_status` = 'payment'  AND i.`invoice_date` >= '" . $from . "' AND i.`invoice_date` <= '" . $to . "'
GROUP BY i.`invoice_id`
ORDER BY i.`invoice_id` DESC ;
";
        }

     

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getPosSalesInvoiceItem($inv) {
        $sql = "SELECT
  i.*,
  it.`invoice_item_quantity`,
  it.`invoice_item_price`
FROM
  `bs_invoice_items` AS it
  INNER JOIN `bs_item` AS i
    ON i.`itemid` = it.`invoice_item_id`
WHERE it.`invoice_id` = '" . $inv . "'";

        //echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getholdingOrders($categ) {

        $bs_userid = $this->session->userdata('bs_userid');
        /* $sql = "SELECT
          o.*,item.*,it.*
          FROM
          `orders` AS o
          INNER JOIN `an_invoice` AS i
          ON i.`invoice_id` = o.`invoice_id`
          INNER JOIN `bs_invoice_items` AS it
          ON it.`invoice_id` = i.`invoice_id`
          INNER JOIN `bs_item` AS item
          ON item.`itemid` = it.`invoice_item_id`
          WHERE o.`order_status` = 'hold' and item.categ_type = '$categ'  ORDER BY o.`order_id` ASC "; */
        $sql = "SELECT
  adval,
  aditemsnames,
   aditemsnamesar,
    u.`fullname` AS refno,
  it.`inovice_product_id`,
  o.*,
  item.*,
  it.*
FROM
  `orders` AS o
  INNER JOIN `an_invoice` AS i
    ON i.`invoice_id` = o.`invoice_id`
  INNER JOIN `bs_invoice_items` AS it
    ON it.`invoice_id` = i.`invoice_id`
  LEFT JOIN
    (SELECT
      pai.pos_item_id,
      pai.`invoice_itempk_id`,
      GROUP_CONCAT(pai.`additional_item_val`) AS adval,
      GROUP_CONCAT(ai.`item_eng_name`) AS aditemsnames,
      GROUP_CONCAT(ai.`item_ar_name`) AS aditemsnamesar
    FROM
      `pos_additional_items` AS pai
      INNER JOIN `additional_items` AS ai
        ON ai.`ad_item_id` = pai.`additional_item_id`
    GROUP BY pai.`invoice_itempk_id`) AS poi
    ON poi.invoice_itempk_id = it.`inovice_product_id`
  INNER JOIN `bs_item` AS item
    ON item.`itemid` = it.`invoice_item_id`
      INNER JOIN `bs_users` AS u
    ON u.`userid` = i.`customer_id`
WHERE o.`order_status` = 'hold'
  AND item.categ_type = '$categ' AND i.`user_id` = '$bs_userid'
ORDER BY o.`order_id` ASC ";
        //  echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getholdingOrdersByGroup($categ) {
        $sql = "SELECT
      o.*,item.*,it.*
    FROM
      `orders` AS o
      INNER JOIN `an_invoice` AS i
        ON i.`invoice_id` = o.`invoice_id`
      INNER JOIN `bs_invoice_items` AS it
        ON it.`invoice_id` = i.`invoice_id`
      INNER JOIN `bs_item` AS item
        ON item.`itemid` = it.`invoice_item_id`
        WHERE o.`order_status` = 'hold' and item.categ_type = '$categ' GROUP BY o.`order_id`  ORDER BY o.`order_id` ASC ";
        //  echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getHoldOrdersList($id) {
        $sql = "SELECT
                  o.*
                FROM
                  `orders` AS o
                  INNER JOIN `an_invoice` AS i
                    ON i.`invoice_id` = o.`invoice_id`
                WHERE o.`order_status` = 'hold'
                  AND o.`order_id` > $id
                  GROUP BY o.`order_id`
                ORDER BY o.`order_id` DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getholdingOrdersAjaxGroup($id, $categ) {
        //AND o.`order_id`>$id
        if ($id != "") {
            //$sb = " AND o.`order_id` > $id";
            $sb = " AND o.`order_id` > $id";
            $sql = "SELECT
      o.invoice_id
    FROM
      `orders` AS o
      INNER JOIN `an_invoice` AS i
        ON i.`invoice_id` = o.`invoice_id`
      INNER JOIN `bs_invoice_items` AS it
        ON it.`invoice_id` = i.`invoice_id`
      INNER JOIN `bs_item` AS item
        ON item.`itemid` = it.`invoice_item_id`
        WHERE o.`order_status` = 'hold' and item.categ_type = '$categ' " . $sb . " GROUP BY o.`order_id`  ORDER BY o.`order_id` ASC ";

            //  echo $sql;
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function getholdingOrdersAjax($id, $categ) {
        //AND o.`order_id`>$id
        if ($id != "") {
            $sb = " AND o.`order_id` > $id";

            $sql = "SELECT
      o.*,item.*,it.*
    FROM
      `orders` AS o
      INNER JOIN `an_invoice` AS i
        ON i.`invoice_id` = o.`invoice_id`
      INNER JOIN `bs_invoice_items` AS it
        ON it.`invoice_id` = i.`invoice_id`
      INNER JOIN `bs_item` AS item
        ON item.`itemid` = it.`invoice_item_id`
        WHERE o.`order_status` = 'hold' and item.categ_type = '$categ' " . $sb . "  ORDER BY o.`order_id` DESC ";

            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function getAdditionItems() {
        $sql = "SELECT * FROM `additional_items` AS ai";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getSaleInvocies_new($id = '') {
        $subq2 = '';
        if ($id) {

            $subq2 = " AND i.`user_id` = '" . $id . "'";
        }
        $get = $this->input->get();
        //echo "<pre>";
        //print_r($get);

        if ($get['frm'] != null && $get['to'] != null) {
            $subq2 .= " AND DATE(i.`invoice_date`) >= DATE('" . $get['frm'] . "') AND DATE(i.`invoice_date`) <= DATE('" . $get['to'] . "') ";
        }

        if (isset($get['rec']) && $get['rec'] != "") {
//        echo "iffff";
            //      exit;
            $subq2.=' AND (sum_invoiceprice- IFNULL(invoice_totalDiscount,0)) - IFNULL(pamount,0)>0';
        }

        if (isset($get['bid']) && $get['bid'] != "")
            $subq2.= ' AND i.branchid = ' . $get['bid'];

        $this->db->query('SET SQL_BIG_SELECTS=1');

        $sql = "SELECT

                i.`customer_id`,

                i.`invoice_id` AS in_id,

                i.`sales_amount`,
				i.refrence_id,

                i.`invoice_total_amount`,

                i.`qid`,

                c.*,

                ex.invoice_id,

                ex.value,

                pamount,

                i.`invoice_date` AS i_date,

                SUM(pamount) AS amount,



                GROUP_CONCAT(ds.bs_user_id) AS cols,

                SUM(amount) AS salescommession,

                SUM(ex.value) AS sumexvalue,

                suminvoicepurchase AS iipp,

                sum_invoiceprice AS sum_invoiceprice,

                sum_invoicediscount,

                invoice_totalDiscount AS alldiscount,

                sumipurchase,
				cancel_sales,
		(sum_invoiceprice-invoice_totalDiscount) - pamount AS remaining,

                d.payment_type

              FROM

                `an_invoice` AS i

                LEFT JOIN `bs_users` AS c

                  ON c.`userid` = i.`customer_id`



                LEFT JOIN

                  (SELECT

                    SUM(ip.`payment_amount`) AS pamount,

                    ip.`invoice_id`,

                    ip.payment_type as payment_type

                  FROM

                    `invoice_payments` AS ip

                    INNER JOIN `an_invoice` AS ii

                      ON ii.`invoice_id` = ip.`invoice_id`

                    INNER JOIN `bs_users` AS c

                      ON c.`userid` = ii.`customer_id`

                  GROUP BY ip.`invoice_id`) d

                  ON d.invoice_id = i.`invoice_id`



                LEFT JOIN

                  (SELECT

                    bii.*,

                    SUM(

                      bii.invoice_item_price_purchase

                    ) AS suminvoicepurchase,

                    SUM(

                      bii.invoice_item_price * bii.invoice_item_quantity

                    ) AS sum_invoiceprice,

                    SUM(

                      bii.invoice_item_discount * bii.invoice_item_quantity

                    ) AS sum_invoicediscount,

                  SUM(bs_item.purchase_price*bii.invoice_item_quantity) AS sumipurchase

                  FROM bs_invoice_items AS bii

                    LEFT JOIN bs_item ON bs_item.itemid = bii.invoice_item_id

                  GROUP BY bii.`invoice_id`) iii

                  ON iii.invoice_id = i.invoice_id



                LEFT JOIN bs_sales_invoice_persons AS spi

                  ON spi.invoice_id = i.invoice_id

                LEFT JOIN bs_sales AS ds

                  ON ds.bs_sales_id = spi.sales_id



                LEFT JOIN an_expenses AS ex

                  ON ex.sinvoice_id = i.invoice_id



              WHERE  ( i.invoice_type = 'pos') AND DATE(i.`invoice_date`) = CURRENT_DATE()  " . $subq2 . "

              GROUP BY i.`invoice_id`

              ORDER BY i.`invoice_id` DESC  ";
        ///echo $sql ;
        $query = $this->db->query($sql);

        return $query->result();
        ///ANd i.cancel_sales = 0
    }

    function getSaleInvocies_new2($id = '') {

        if ($id) {

            $subq = "WHERE u.`userid` = '" . $id . "'";
        }

        /*

          $sql = "SELECT

          u.`fullname` AS customername,

          u.`userid`,

          bsi.*

          FROM

          `an_invoice` AS bsi

          INNER JOIN `bs_users` AS u

          ON u.`userid` = bsi.`customer_id`

          " . $subq . ""; */

        $sql = "SELECT 

               i.`customer_id`,

               i.`quotation_id` AS quotation_id,

               i.`sales_amount`,

               i.`quotation_total_amount`,

               c.*,

               pamount,

               bii.quotation_item_discount_type AS discount_type,

               i.`quotation_date` AS i_date,

               SUM(pamount) AS amount,

               SUM(quotation_item_discount) AS iidiscount,

               SUM(quotation_item_price) AS iiprice,

               SUM(quotation_item_price_purchase) AS iiprice_purchase,

               sales_amount - pamount AS remaining,

               GROUP_CONCAT(ds.bs_user_id) AS cols,

               SUM(bs_sales_commession) AS salescommession,

               c2.fullname as ofullname

             FROM

               `an_quotation` AS i 

               

               LEFT JOIN `bs_users` AS c 

                 ON c.`userid` = i.`customer_id` 

                 

               LEFT JOIN `bs_users` AS c2 

                 ON c2.`userid` = i.`userid` 

                 

                 

               LEFT JOIN 

                 (SELECT 

                   SUM(ip.`payment_amount`) AS pamount,

                   ip.`invoice_id` 

                 FROM

                   `invoice_payments` AS ip 

                   INNER JOIN `an_quotation` AS ii 

                     ON ii.`quotation_id` = ip.`invoice_id` 

                   INNER JOIN `bs_users` AS c 

                     ON c.`userid` = ii.`customer_id` 

                 GROUP BY ip.`invoice_id`) d 

                 ON d.invoice_id = i.`quotation_id` 

               LEFT JOIN bs_quotation_items AS bii 

                 ON bii.quotation_id = i.quotation_id 

               LEFT JOIN bs_sales_quotation_persons AS spi 

                 ON spi.quotation_id = i.quotation_id 

               LEFT JOIN bs_sales AS ds 

                 ON ds.bs_sales_id = spi.sales_id 

                where i.quotation_status='1'

                

             GROUP BY i.`quotation_id` ";



        $query = $this->db->query($sql);

        return $query->result();
    }

    function getpending($id = '') {

        if ($id) {

            $subq = "WHERE u.`userid` = '" . $id . "'";
        }

        /*

          $sql = "SELECT

          u.`fullname` AS customername,

          u.`userid`,

          bsi.*

          FROM

          `an_invoice` AS bsi

          INNER JOIN `bs_users` AS u

          ON u.`userid` = bsi.`customer_id`

          " . $subq . ""; */

        $sql = "SELECT 

               i.`customer_id`,

               i.`quotation_id` AS quotation_id,

               i.`sales_amount`,

               i.`quotation_total_amount`,

               c.*,

               pamount,

               i.quotation_totalDiscount_type AS discount_type,

               i.`quotation_date` AS i_date,

               SUM(pamount) AS amount,

               quotation_totalDiscount AS iidiscount,

               SUM(quotation_item_price) AS iiprice,

               SUM(quotation_item_price_purchase) AS iiprice_purchase,

               sales_amount - pamount AS remaining,

               GROUP_CONCAT(ds.bs_user_id) AS cols,

               SUM(bs_sales_commession) AS salescommession 

             FROM

               `an_quotation` AS i 

               INNER JOIN `bs_users` AS c 

                 ON c.`userid` = i.`customer_id` 

               LEFT JOIN 

                 (SELECT 

                   SUM(ip.`payment_amount`) AS pamount,

                   ip.`invoice_id` 

                 FROM

                   `invoice_payments` AS ip 

                   INNER JOIN `an_quotation` AS ii 

                     ON ii.`quotation_id` = ip.`invoice_id` 

                   INNER JOIN `bs_users` AS c 

                     ON c.`userid` = ii.`customer_id` 

                 GROUP BY ip.`invoice_id`) d 

                 ON d.invoice_id = i.`quotation_id` 

               LEFT JOIN bs_quotation_items AS bii 

                 ON bii.quotation_id = i.quotation_id 

               LEFT JOIN bs_sales_quotation_persons AS spi 

                 ON spi.quotation_id = i.quotation_id 

               LEFT JOIN bs_sales AS ds 

                 ON ds.bs_sales_id = spi.sales_id 

                 

                 where i.quotation_status='0' and i.contracts='0'

             GROUP BY i.`quotation_id` ";



        $query = $this->db->query($sql);

        return $query->result();
    }

    function contracts_data($id = '') {

        if ($id) {

            $subq = "WHERE u.`userid` = '" . $id . "'";
        }

        /*

          $sql = "SELECT

          u.`fullname` AS customername,

          u.`userid`,

          bsi.*

          FROM

          `an_invoice` AS bsi

          INNER JOIN `bs_users` AS u

          ON u.`userid` = bsi.`customer_id`

          " . $subq . ""; */

        $sql = "SELECT 

               i.`customer_id`,

               i.`quotation_id` AS quotation_id,

               i.`sales_amount`,

               i.`quotation_total_amount`,

               c.*,

               pamount,

               i.quotation_totalDiscount_type AS discount_type,

               i.`quotation_date` AS i_date,

               SUM(pamount) AS amount,

               quotation_totalDiscount AS iidiscount,

               SUM(quotation_item_price) AS iiprice,

               SUM(quotation_item_price_purchase) AS iiprice_purchase,

               sales_amount - pamount AS remaining,

               GROUP_CONCAT(ds.bs_user_id) AS cols,

               SUM(bs_sales_commession) AS salescommession 

             FROM

               `an_quotation` AS i 

               INNER JOIN `bs_users` AS c 

                 ON c.`userid` = i.`customer_id` 

               LEFT JOIN 

                 (SELECT 

                   SUM(ip.`payment_amount`) AS pamount,

                   ip.`invoice_id` 

                 FROM

                   `invoice_payments` AS ip 

                   INNER JOIN `an_quotation` AS ii 

                     ON ii.`quotation_id` = ip.`invoice_id` 

                   INNER JOIN `bs_users` AS c 

                     ON c.`userid` = ii.`customer_id` 

                 GROUP BY ip.`invoice_id`) d 

                 ON d.invoice_id = i.`quotation_id` 

               LEFT JOIN bs_quotation_items AS bii 

                 ON bii.quotation_id = i.quotation_id 

               LEFT JOIN bs_sales_quotation_persons AS spi 

                 ON spi.quotation_id = i.quotation_id 

               LEFT JOIN bs_sales AS ds 

                 ON ds.bs_sales_id = spi.sales_id 

                 

                 where  i.contracts='1'

             GROUP BY i.`quotation_id` ";



        $query = $this->db->query($sql);

        return $query->result();
    }

    public function product_list($product_id) {

        if ($product_id != "") {

            $subq = " WHERE bs_item.`itemid` =" . $product_id . "";
        } else {

            $subq = "";
        }

        //$ownerid = ownerid();

        $query = $this->db->query("SELECT 

			  `bs_category`.`catname`,

			  `bs_item`.`storeid`,

			  `bs_suppliers`.`suppliername`,

			  `bs_store`.`storename`,

			  `bs_item`.`quotationid`,

			  `bs_item`.`quotationname`,

			  `bs_item`.`quotationname`,

			  `bs_item`.`quotationtype`,

			  `bs_item`.`notes`,

			  IF(

				bs_item.quotationtype = 'P',

				`bs_item`.`serialnumber`,

				'-----'

			  ) AS `serialnumber`,

			  IF(

				bs_item.itemtype = 'P',

				CONCAT(`bs_item`.`purchase_price`),

				'-----'

			  ) AS `purchase_price`,

			  IF(

				bs_item.itemtype = 'P',

				CONCAT(`bs_item`.`sale_price`),

				'-----'

			  ) AS `sale_price`,

			  IF(

				bs_item.itemtype = 'P',

				`bs_item`.`min_sale_price`,

				'-----'

			  ) AS `min_sale_price`,

			  IF(

				bs_item.itemtype = 'P',

				`bs_item`.`point_of_re_order`,

				'-----'

			  ) AS `point_of_re_order`,

			  IF(

				bs_item.itemtype = 'P',

				`bs_item`.`quantity`,

				'-----'

			  ) AS `quantity`,

			  `bs_item`.`product_picture` 

			FROM

			  (`bs_item`) 

			  JOIN `bs_category` 

				ON `bs_item`.`categoryid` = `bs_category`.`catid` 

			  JOIN `bs_store` 

				ON `bs_item`.`storeid` = `bs_store`.`storeid` 

			  JOIN `bs_suppliers` 

				ON `bs_item`.`suppliderid` = `bs_suppliers`.`supplierid` 

				" . $subq . " 

			ORDER BY `bs_item`.`itemname` ASC ");

        //$this->db->last_query();

        if ($product_id != "") {

            return $query->row();
        } else {

            return $query->result();
        }
    }

    function getSalesPersonsData() {

        $sql = "SELECT bsales.`bs_sales_id`,bu.`fullname`,bu.`userid`,bu.`office_number`,bu.`phone_number`,bu.`employeecode`,bsales.`bs_sales_commession`,bsales.`bs_sales_commession_type` FROM `bs_sales` bsales

                INNER JOIN `bs_users` AS bu ON bu.`userid` = bsales.`bs_user_id`";

        $query = $this->db->query($sql);

        return $query->result();
    }

    function getBankAccounts() {

        $sql = "SELECT c.`account_number` FROM `bs_company` AS c";

        $query = $this->db->query($sql);

        return $query->result();
    }

    function getcategory() {

        $sql = "SELECT * FROM `bs_category` AS c Where catstatus = 'A' order by cat_order ASC ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function getSaleInvocies($id = '', $re = null) {

        if ($id) {

            $subq = "WHERE u.`userid` = '" . $id . "'";
        }

        if ($re) {

            $subq = "WHERE bsi.`return_sales` = '" . $re . "'";
        }



        $sql = "SELECT 

			  u.`fullname` AS customername,

			  u.*,

			  bsi.* 

			FROM

			  `bs_sales_invoice_quotation` AS bsi 

			  INNER JOIN `bs_users` AS u 

				ON u.`userid` = bsi.`customerid`

				" . $subq . "";



        $query = $this->db->query($sql);

        return $query->result();
    }

    public function users_list() {

        $permission_type_id = $this->input->post('permission_type_id');

        $member_type = get_member_type();

        $query = "SELECT 

				  a.userid,

				  a.`fullname`,

				  a.`username`,

				  IF(a.`employeecode`='','-----',a.`employeecode`) AS employeecode,

				  IF(a.`office_number`='','-----',a.`office_number`) AS office_number,

				  IF(a.`fax_number`='','-----',a.`fax_number`) AS fax_number,

				  IF(a.`phone_number`='','-----',a.`phone_number`) AS phone_number,

				  IF(a.`status`='A','Active','Deactive') AS `status`,  

				  b.`permissionhead`,

				  c.`fullname` AS ownername 

				FROM

				  bs_users AS a,

				  bs_users As c,

				  bs_permission_type AS b WHERE a.`member_type` = b.`permission_type_id` AND b.`permission_type_id`=6 AND a.ownerid=c.userid AND ";

        if ($member_type != '5') {

            $query .= " (a.ownerid='" . $this->session->userdata('bs_ownerid') . "' OR a.ownerid='" . $this->session->userdata('bs_userid') . "') AND ";
        }



        $query .= " a.userid > 0 Order by a.fullname ASC";



        $q = $this->db->query($query);

        return $q->result();
    }

    /*

      function p_invoices($id = null) {



      $this->db->select('bs_invoice_quotations.*,bs_quotation.*');

      $this->db->where('bs_invoice_quotations.invocie_id', $id);

      $this->db->join('bs_quotation', 'bs_quotation.quotationid=bs_invoice_quotations.invoice_quotation_id', 'LEFT');

      //$this->db->order_by();

      $Q = $this->db->get('bs_invoice_quotations');

      return $Q->result();

      }

     */

    public function add_new_customer($employeecode = null) {

        $data = $this->input->post();

        $userid = $this->input->post('userid');



        $data['employeecode'] = $employeecode;



        $data['upassword'] = md5($this->input->post('upassword'));

        unset($data['sub_mit'], $data['sub_reset'], $data['userid'], $data['confpassword']);



        if ($userid != '') {

            $this->db->where('userid', $userid);

            $this->db->update($this->config->quotation('table_users'), $data);

            do_redirect('add_invoice?e=10');
        } else {

            $this->db->insert($this->config->quotation('table_users'), $data);

            do_redirect('add_invoice?e=10');
        }
    }

    function getCategories() {

        $sql = "SELECT * FROM `bs_category`";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

//----------------------------------------------------------------------



    function invoice_data($id) {



        $sql = "SELECT 

                i.*,

                qi.*,

                qii.*,

                i.`quotation_id` AS quotation_id,

                c.fullname,

                c.email_address,

                c.phone_number,

                c.userid,

                c.buywithtotal

              FROM

                `an_quotation` AS i 

            INNER JOIN `bs_users` AS c 

                      ON c.`userid` = i.`customer_id` 

            LEFT JOIN `bs_quotation_items` AS qi 

                      ON qi.`quotation_id` = i.`quotation_id` 

            INNER JOIN `bs_item` AS qii 

                      ON qii.`itemid` = qi.`quotation_item_id` 

              WHERE i.quotation_id='$id'    

              ";

        $query = $this->db->query($sql);

        return $query->result();
    }

    function invoice_data2($id) {



        $sql = "SELECT 

                i.*,

                qi.*,

                qii.*,

                i.`quotation_id` AS quotation_id,

                c.fullname,

                c.buywithtotal

              FROM

                `an_quotation` AS i 

            INNER JOIN `bs_users` AS c 

                      ON c.`userid` = i.`customer_id` 

            LEFT JOIN `bs_quotation_items` AS qi 

                      ON qi.`quotation_id` = i.`quotation_id` 

            INNER JOIN `bs_item` AS qii 

                      ON qii.`itemid` = qi.`quotation_item_id` 

              WHERE i.quotation_id='$id'    

              ";

        $query = $this->db->query($sql);

        return $query->result();
    }

    function p_invoice($id = null) {



        $this->db->select('pos_invoices.*');

        $this->db->select('bs_users.*');

        //$this->db->select('bs_company.*');
        // $this->db->select('company_logos.*');

        $this->db->where('pos_invoices.pos_id', $id);

        //$this->db->join('bs_company','bs_company.companyid=an_quotation.companyid', 'LEFT');

        $this->db->join('bs_users', 'bs_users.userid=pos_invoices.pos_customer_id', 'LEFT');

        //$this->db->join('company_logos','company_logos.company_id=bs_company.companyid', 'LEFT');
        //$this->db->order_by();

        $Q = $this->db->get('pos_invoices');

        return $Q->row();
    }

    function p_invoices($id = null) {



        $this->db->select('pos_invoice_items.*,bs_item.*');

        $this->db->where('pos_invoice_items.pos_inv_id', $id);

        $this->db->join('bs_item', 'bs_item.itemid=pos_invoice_items.pos_item_id', 'LEFT');

        //$this->db->order_by();

        $Q = $this->db->get('pos_invoice_items');

        return $Q->result();
    }

}
