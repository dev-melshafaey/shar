<!DOCTYPE html>
<html>
<head>
    <title>Point of Sale</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>screen_css/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>screen_css/css/pos.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>screen_css/css/bootstrap.min.css">
    <!--<script src="<?php echo base_url(); ?>screen_css/src/js/boot.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/js/core.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/js/chrome.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/js/data.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/js/dates.js"></script>-->
    <script src="<?php echo base_url(); ?>screen_css/lib/jquery-1.6.2.min.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/lib/jquery-ui.min.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/lib/underscore-1.1.7.min.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/lib/json2.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/lib/backbone-0.5.1.min.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/js/bootstrap.min.js"></script>
    <style>
        #receipt{
            min-height:20em;
            background: #bcbcbc;
        }

        .start_timer{
            right: 54% !important;
        }
        .working{
            background-color: orange;
            color: #fff;
        }
        .done{
            background-color: #b93028;
            color: #fff;
         /*   text-indent: 1em; */
            padding: 5px 0;
        }
        .donebtn{
            background-color: #b93028;
            color: #fff;
            }
        .btn-block {margin: 8px 0;}
        .order_num {font-size: 2rem; text-align: center; }
        .timer {padding: 5px; text-align: center; }
        .makemecenter {text-align: center;}
        #receipt td {text-align: center !important;}
        .bgcolns {background: #dfdfdf;}
        tbody {background: #fff;}
       /* thead {border-left: 1px solid #333; border-right: 1px solid #333;}
        tbody {border-left: 1px solid #333; border-right: 1px solid #333;} */
        .footstyle {position: absolute;bottom:0;}
        .btn-warning {background-color: #f8981d; border-radius: 0;font-weight: bold; text-transform: uppercase;}
        .blink_me {
          animation: blinker 1s linear infinite;
          color: #b82f27;
        }

        @keyframes blinker {  
          50% { opacity: 0; }
        }
        .start_timer {padding-top: 14px;text-indent: 7px;font-size: 18px;}
    </style>
</head>
<body style="background: #bcbcbc;" >
<?php
//echo "<pre>";
//print_r($holding_orders);
$orders_main = array();
?>

<div class="containerx" id="main_body">
<div class="row">
<div class="col-sm-12">
<div id="main_div" class="row" style="padding:.5em 21px;">
<?php
  if(!empty($holding_orders)){
    //echo "<pre>";
      // print_r($holding_orders);
     //   exit;
     $total = count($holding_orders);
      foreach($holding_orders as $i=>$orders){
 //       print_r($orders);
$ordid = $orders->order_id;
if($i == $total-1){
$ord_m = $ordid;
//echo "exit";
?>
    <script>
        //alert('ready');
        ordd = '<?php echo $ord_m; ?>';
    </script>
    <?php
    }

          if(!in_array($ordid,$orders_main)){
                $orders_main[] = $ordid;

                $moreItems = $this->pos_mangement->getMoreItems($ordid,$orders->itemid);
                echo "<pre>";
                print_r($moreItems);

    if($i !=0){
                  ?></tbody></table></div></div></div></div></div><?php
                }
            ?>
<div class="col-sm-3" id="order_main_div<?php echo $ordid ?>">

<div class="row">
<div id='leftpane' class="leftpane">
<div id="getting-started "></div>

    <div>

<div id='receipt'>
    <div class="donebtn  col-md-12"><button id="ordbtn<?php echo $ordid; ?>" onclick="change_order_status(this,'<?php echo $ordid; ?>')" type = "button" class = "col-md-2 btn btn-success btn-block" style="width: 48%; left: 148px;">Finish</button><div id="startclockorder<?php echo $ordid; ?>" class="start_timer col-sm-3"></div></div>
    <table>
<thead>
<th colspan="3" class="order_num">Order No #<?php echo $ordid; ?> <span class="blink_me" style="display: none;">Order is Late</span></th>
</thead>
<tr>
    <th colspan="3" class="timer"><div class="orderclock" id="clockorder<?php echo $ordid; ?>"></div> <?php  echo $orders->order_date; $ret = dateDiff($orders->order_date);  $ordersArr[$ordid] = $ret;  ?><br><div>Refrence No:<?php echo $orders->refno; ?></div></th>
</tr>
<tr class="bgcolns">
    <th class="makemecenter">Item</th>
    <th class="makemecenter">Qty</th>
</tr>
<?php
        ?>
<tr>
    <td class="makemecenter"><?php echo _s($orders->itemname,'english'); if($orders->adval !=""){ $adDataVales = explode(',',$orders->adval); $adDataNames = explode(',',$orders->aditemsnames); if(!empty($adDataNames)){ echo '<br>';  foreach($adDataNames as $i=>$adname){  echo ' <span style="font-size: 11px;">'.$adname.' '.$adDataVales[$i].'</span><br>'; } }   } //if() //echo $orders->itemname ?></td>
    <td class="makemecenter"><?php echo $orders->invoice_item_quantity; ?></td>
</tr>
<?php
          }
           else{
               ?>
               <tr>
                   <td><?php echo _s($orders->itemname,'english'); if($orders->adval !=""){
                           $adDataVales = explode(',',$orders->adval); $adDataNames = explode(',',$orders->aditemsnames); if(!empty($adDataNames)){ echo '<br>'; foreach($adDataNames as $i=>$adname){  echo ' <span style="font-size: 11px;">'.$adname.' '.$adDataVales[$i].'</span><br>';
                           } }   } //echo $orders->itemname ?></td>
                   <td><?php echo $orders->invoice_item_quantity; ?></td>
               </tr>
                <?php
           }
      }
  }
?>
    </tbody><tfoot class="footstyle"></tfoot></table></div></div></div></div>
</div>

</div>


</div>
</div>




<script type="text/template" id="category-template">
    <header>
        <ol class="breadcrum">
            <li>
                <a href="#"><img src="/point_of_sale2/static/img/home.png" class="homeimg"></a>
            </li>
            <% _.each(breadcrumb, function(category) { %>
                <li>
                    <img src="static/img/bc-arrow.png" class="bc-arrow">
                    <a href="#category/<%= category.id %>"><%= category.name %></a>
                </li>
            <% }); %>
        </ol>
        <div class="searchbox">
            <input placeholder="Search Products">
            <img class="search-clear" src="static/img/search_reset.gif">
        </div>
    </header>

    <div id="categories">
        <h4>Categories:</h4>
        <ol>
        <ol>
            <% _.each(categories, function(category) { %>
                <li><a href="#category/<%= category.id %>" class="button"><%= category.name %></a></li>
            <% }); %>
        </ol>
    </div>
</script>

<script type="text/template" id="product-template">
    <a href="#">
        <div class="product-img">
            <img src="data:image/gif;base64,<%= img %>">
            <span class="price-tag"><%= list_price %> &euro;</span>
        </div>
        <div class="product-name"><%= name %></div>
    </a>
</script>

<script type="text/template" id="orderline-template">
    <td><%= name %></td>
    <td><%= list_price.toFixed(2) %> &euro;</td>
    <td><%= discount.toFixed(2) %></td>
    <td><%= quantity.toFixed(0) %></td>
    <td><%= (list_price * (1 - discount/100) * quantity).toFixed(2) %> &euro;</td>
</script>

</body>

<script src="<?php echo base_url(); ?>screen_css/js/jquery.countdown.min.js"></script>
<script type='text/javascript'>
    <?php
    $js_array = json_encode($orders_main);
    //echo " ord_array = ". $js_array . ";\n";
    ?>
    jsarr = '<?php echo $js_array; ?>';
</script>
<script type="text/javascript">

    function change_order_status(obj,id){
        $.ajax({
            url: "<?php echo base_url(); ?>pos/update_start_time",
            type: 'post',
            data: {'ordnumbr':id,status:'ready'},
            cache: false,
            success: function (data) {

                //alert(data);
                //  $("#tb_data").html(data);
                if(data){
                    $("#order_main_div"+id).hide()
                }
            }
        });
    }
    function change_order_status_old(obj,id){
        ordh = $("#ordbtn"+id).html();

        if(ordh == 'Start'){
            //$(obj).css(left:50%);

            $(obj).css({left:148});
            $("#ordbtn"+id).html('Finish');
            $("#ordbtn"+id).removeClass('btn-warning');
            $("#ordbtn"+id).addClass('btn-success');
            var readySeconds = new Date().getTime();
            sttime = new Date().toLocaleString();
          // alert(sttime);
            $('#startclockorder'+id).countdown(readySeconds, {elapse: true})
                .on('update.countdown', function(event) {
                    var $this = $(this);
                    //
                    //evenc = event;
                    if (event.elapsed) {
                        $this.html(event.strftime('Timer: <span>%H:%M:%S</span>'));
                    } else {
                        $this.html(event.strftime('Timer: <span>%H:%M:%S</span>'));
                    }
                });


            $.ajax({
                url: "<?php echo base_url(); ?>pos/update_start_time",
                type: 'post',
                data: {'ordnumbr':id,'start_time':sttime,status:'start'},
                cache: false,
                success: function (data) {

                    //alert(data);
                    //  $("#tb_data").html(data);
                    if(data){

                    }
                }
            });

        }
        else{

            $.ajax({
                url: "<?php echo base_url(); ?>pos/update_start_time",
                type: 'post',
                data: {'ordnumbr':id,'start_time':sttime,status:'ready'},
                cache: false,
                success: function (data) {

                    //alert(data);
                    //  $("#tb_data").html(data);
                    if(data){
                        $("#order_main_div"+id).hide()
                    }
                }
            });
        }


    }
    /*$("#getting-started")
        .countdown("2017/01/01", function(event) {
            $(this).text(
                event.strftime('%D days %H:%M:%S')
            );
        });*/

    $(document).ready(function(){


        setInterval('checkorders()',5000);

        clockordersAr = $.parseJSON(jsarr);
        fiveSeconds = new Date().getTime() + 1000;

        audioElement = document.createElement('audio');
        //alert(audioElement);
        audioElement.setAttribute('src', '<?php echo base_url() ?>durarthem/sms-alert-5-daniel_simon.mp3');

        audioElement.addEventListener('ended', function() {
            // this.currentTime = 0;
            // this.play();
        }, false);

        $('#play').click(function() {
            audioElement.play();
        });

        $('#pause').click(function() {
            audioElement.pause();
        });


        $("#show_tables").click(function(){
            //alert('aa');
            $('.tb').toggle();
        });

        clockordersAr.forEach(function(entry) {
           // alert(entry);
            $('#clockorder'+entry).countdown(fiveSeconds, {elapse: true})
                .on('update.countdown', function(event) {
                    var $this = $(this);
                    evenc = event;
                    console.log(evenc.offset.totalMinutes);
                    if(evenc.offset.totalMinutes == 15){
                        audioElement.play();
                        blind = $('.orderclock').index(this);
                        $('.blink_me').eq(blind).show();
                    }
                    //alert( $('.orderclock').index(this) );
                    //var listItem = $( "#bar" );
                    //alert( "Index: " + listItem.index( "blink_me" ) );

                    //orderclock
                    tt = $(this);
                    //evenc.offset.totalMinutes
                    if (event.elapsed) {
                        $this.html(event.strftime('Order Start: <span>%H:%M:%S</span>'));
                    } else {
                        $this.html(event.strftime('Order Start: <span>%H:%M:%S</span>'));
                    }
                });

            $('#startclockorder'+entry).countdown(fiveSeconds, {elapse: true})
                .on('update.countdown', function(event) {
                    var $this = $(this);
                    //
                    //evenc = event;
                    if (event.elapsed) {
                        $this.html(event.strftime('Timer: <span>%H:%M:%S</span>'));
                    } else {
                        $this.html(event.strftime('Timer: <span>%H:%M:%S</span>'));
                    }
                });
        });

    })

    function convertMinutesintoSeconds(minutes){
      //      return =minutes*60;
    }

    function checkorders(){
        //main_div
        $.ajax({
            url: "<?php echo base_url(); ?>pos/get_update_orders",
            type: 'post',
            data: {'ordnumbr':ordd},
            cache: false,
            success: function (data) {

                //alert(data);
                //console.log(data);
                if(data){
                   // clockordersAr = new Array();
                    newdata = $.parseJSON(data);
                    ordli = newdata.ordlist;
                    ordlen = ordli.length;
                   // alert(ordli);
                    //console.log(ordli);
                    clockordersAr2 = newdata.ordergroup;
                    //alert(ordlen);
                    $.each(ordli, function(key,val) {
                      //    alert(key+val);

                        if(key ==ordlen-1){
                            ordd =   ordli[key].order_id;

                        }
                    });


                    newhtml = newdata.html;
                    $("#main_body").html(newhtml);


                    clockordersAr2.forEach(function(ent) {
                        //alert(entry);
                        entry = ent.invoice_id;
                        $('#clockorder'+entry).countdown(fiveSeconds, {elapse: true})
                            .on('update.countdown', function(event) {
                                var $this = $(this);
                                evenc = event;
                                console.log(evenc.offset.totalMinutes);
                                if(evenc.offset.totalMinutes == 15){
                                    audioElement.play();
                                    blind = $('.orderclock').index(this);
                                    $('.blink_me').eq(blind).show();
                                }
                                //alert( $('.orderclock').index(this) );
                                //var listItem = $( "#bar" );
                                //alert( "Index: " + listItem.index( "blink_me" ) );

                                //orderclock
                                tt = $(this);
                                //evenc.offset.totalMinutes
                                if (event.elapsed) {
                                    $this.html(event.strftime('Order Start: <span>%H:%M:%S</span>'));
                                } else {
                                    $this.html(event.strftime('Order Start: <span>%H:%M:%S</span>'));
                                }
                            });

                        $('#startclockorder'+entry).countdown(fiveSeconds, {elapse: true})
                            .on('update.countdown', function(event) {
                                var $this = $(this);
                                //
                                //evenc = event;
                                if (event.elapsed) {
                                    $this.html(event.strftime('Timer: <span>%H:%M:%S</span>'));
                                } else {
                                    $this.html(event.strftime('Timer: <span>%H:%M:%S</span>'));
                                }
                            });
                    });

                }
                //  $("#tb_data").html(data);
                if(data){
                   // $("#order_main_div"+id).hide()
                }
            }
        });
    }
</script>
</html>
