<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>POS System</title>



    <!-- Bootstrap -->

    <link href="<?php echo base_url(); ?>pos_assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Reset -->

    <link href="<?php echo base_url(); ?>pos_assets/css/normalize.css" rel="stylesheet">

    <!-- Custom -->

    <link href="<?php echo base_url(); ?>pos_assets/css/style.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>newcss/style.css" rel="stylesheet">

    <!-- Select -->

    <link href="<?php echo base_url(); ?>pos_assets/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- Font Awesome -->

    <link href="<?php echo base_url(); ?>pos_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Owl Carousel -->

    <link href="<?php echo base_url(); ?>pos_assets/css/owl.carousel.css" rel="stylesheet">

       <link href="<?php echo base_url(); ?>css/jqcode.css" rel="stylesheet">

	

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <style>

    .categ_active{

			color: #1d465e !important;

    		text-decoration: none;	

		}

    </style>

  </head>

  <body>

  <header>

  

  <div class="container">

  	<div class="row" style="display: none;">

    	<div class="col-sm-6"><img src="<?php echo base_url(); ?>pos_assets/images/logo.png" alt="Business Solutions" class="img-responsive bslogo"></div>
        <div class="col-sm-6"><div class="col-sm-3" style="font-size: 43px;">Total</div><div  class="col-sm-3" id="totalvalup" style="float: right; font-size: 41px;">0.000</div></div>
        <div class="col-sm-6"></div><div class="col-sm-6"><div class="col-sm-3" style="font-size: 34px;">Return</div><div class="col-sm-3" id="total_returnedup" style="float: right; font-size: 41px;">0.000</div></div>
    </div>
      <div class="row">
          <div class="col-sm-12">
              <div class="col-sm-2"><img src="<?php echo base_url(); ?>pos_assets/images/logo.png" alt="Business Solutions" class="img-responsive bslogo"></div>
              <div class="col-sm-8">
                  <ul class="list-inline" style="margin-top:1.5em">
                      <li><a href="" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Calculator</a></li>
                      <a href="javascript:void(0)"  id="item_detail_click" data-toggle="modal" data-target="#items_details" style="display: none;"></a>
                      <li><a href="" class="btn btn-primary" data-toggle="modal" data-target="#hold_items" onclick="viewInvoiceList('cancel')"><span class="glyphicon glyphicon-tag"></span> Hold Items</a></li>
                      <li><a href="" class="btn btn-info"><span class="glyphicon glyphicon-list-alt"></span> My Invoice</a></li>
                      <li><a href="" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel Invoice</a></li>
                      <li><a href="" class="btn btn-warning"><span class="glyphicon glyphicon-shopping-cart"></span> Order</a></li>
                  </ul>
              </div>
              <div class="col-sm-2"><div class="col-sm-3" style="font-size: 43px;">Total</div><div  class="col-sm-3" id="totalvalup" style="float: right; font-size: 38px;">0.000</div> <br clear="all"> <div class="col-sm-3" style="font-size: 43px;">Return</div><div class="col-sm-3" id="total_returnedup" style="float: right; font-size: 41px;">0.000</div></div>
          </div> <!--./col-sm-12-->
      </div>

  </div>

  

  </header>

    

                  

    <div class="container">

    

   

		<div class="row">

    		<div class="col-sm-5">

            	<div class="rt-box">

                

                <ul class="list-inline" style="display:none;">

                		<li class="pull-right">Add New Customer <a href="#"><i class="fa fa-plus"></i></a></li>

                        <li><br></li>

                </ul>



                	

                	<div class="form-group pull-left">

                     <form id="pos_form_data" >

                      <div class="col-sm-6"><label for="sel1">Customer</label></div>
                         <div class="col-sm-6"><label for="sel1">Quantity</label></div>

                      <!--<select class="selectpicker" style="display: none;">

                          <option>Cash</option>

                          <option>Ali</option>

                          <option>Bob</option>

                        </select>-->

                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="customer_name" name="customer_name" placeholder="Customer Name / Cash"><input type="hidden" name="customer_id" id="customer_id" />

                        </div>
                         <div class="col-sm-6">
                             <input type="text" class="form-control" id="q" name="q" placeholder="Quantity" value="1">
                         </div>

                    </form>

                    </div>

                	

                    <div class="form-group">

						<input type="text" class="form-control" id="item_name" name="item_name" onchange="checkBarcodeMatch(this.value)" placeholder="Barcode / Items name"><input type="hidden" id="item_id" name="item_id">

                    </div> <form id="pos_itemform_data" >

                    

                    <div class="scroll">

                   <table id="rtBox" class = "table">

                       <thead>

                          <tr>

                             <th><i class="fa fa-close"></i></th>

                             <th>Product</th>

                             <th>Qty</th>

                             <th>Price</th>

                          </tr>

                       </thead>

                    </table>

                    

                    

                    </div>

                    <!-- another table -->

                    



                    <table class="table">

                		<tbody>

							<tr class="info">

                

                                <td>Total Items</td>

                

                                <td id="totalitems">0</td>

                

                                <td>Total</td>

                

                                <td id="totalval">0</td>

                

                            </tr>

                

                            <tr class="warning">

                

                                <td>Discount </td>

                

                                <td><input type="radio" name="discount" id="discount" value="omr" onClick="assignDiscount()" checked> <span class="smaller">OMR</span> <input type="radio" name="discount" id="discount" value="percent" onClick="assignDiscount()"> <span class="smaller">%</span></td>

                

                                <td></td>

                

                                <td><div id='eVal4'><input type="text" class="form-control" id="discount_val" name="discount_val" placeholder="Discount" value="0"  style="width:80px;" onChange="assignDiscount()"></div></td>

                

                            </tr>

                

                            <tr class="success">

                

                                <td>Total Payable</td>

                

                                <td></td>

                

                                <td></td>

                

                                <td id="total_pay_val">0</td>

                

                            </tr>
							<tr>
                              <td>Total Paid</td>

                

                                <td></td>

                

                                <td></td>

                                <td id="total_paid_html"><input type="text" class="form-control" id="total_paid" name="total_paid" value="0" style="width:80px;" ></td>

                            </tr>
                            <tr>
                              <td>Returned</td>

                

                                <td></td>

                

                                <td></td>

                                <td id="total_returned"></td>

                            </tr>
                

                        </tbody>

                

                    </table>



					

					<!-- Table for buttons -->

    	



                    <table class="table">

                

                        <tbody>

                

                            <tr>



                                <td ><button onclick="add_payment_date('credit')" type = "button" class = "btn btn-warning btn-block">Credit</button></td>

                                <td><button onclick="window.top.location = location.href" type = "button" class = "btn btn-danger btn-block">Cancel</button></td>

                

                                <td><button onclick="add_payment_date('hold')" type = "button" class = "btn btn-warning btn-block">Hold</button></td>


                                <td ><button onclick="add_payment_date('payment')" type = "button" class = "btn btn-success btn-block">Payment</button></td>

                

                            </tr>

                            

                		</tbody>

                

                    </table>

					<input type="hidden" id="pos_invoice_status" name="pos_invoice_status">
                    <input type="hidden" id="storeid" name="storeid" value="<?php echo $storeid; ?>">

				</form>

					

                </div> <!--./ rt box --> 

            </div> <!--./ sm 4 -->

            

            <div class="col-sm-7">

            <!-- Category slider-->

            <div class="wrapper-with-margin">

                <div id="owl-demo" class="owl-carousel">

                	<?php 

						if($categlist){

								foreach($categlist as $categ){

									?>

 	                    			<div><a href="javascript:void(0)" onClick="getProducts(this,'<?php echo $categ->catid; ?>')"  class="categ_c"><?php $cname = unserialize($categ->catname); echo $cname['english']; ?></a></div>	               

									<?php

								}

						}

					?>		

                </div>

            </div>

            

            <!-- Content slider -->

                       

            <div id="cslide-slides" class="cslide-slides-master clearfix">

    

    <div class="cslide-slides-container clearfix">

        <!--<div class="cslide-slide">

            		<div class="col-sm-4 col-md-2"><div class="btnPro"><img src="<?php echo base_url(); ?>pos_assets/images/beverage.jpg" class="img-responsive"><h2>Apple</h2></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"><img src="<?php echo base_url(); ?>pos_assets/images/beverage.jpg" class="img-responsive"><h2>Apple</h2></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"><img src="<?php echo base_url(); ?>pos_assets/images/beverage.jpg" class="img-responsive"><h2>Apple</h2></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"><img src="<?php echo base_url(); ?>pos_assets/images/beverage.jpg" class="img-responsive"><h2>Apple</h2></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"><img src="<?php echo base_url(); ?>pos_assets/images/beverage.jpg" class="img-responsive"><h2>Apple</h2></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"><img src="<?php echo base_url(); ?>pos_assets/images/beverage.jpg" class="img-responsive"><h2>Apple</h2></div></div>

                    

                    <div class="col-sm-4 col-md-2"><div class="btnPro"><img src="<?php echo base_url(); ?>pos_assets/images/beverage.jpg" class="img-responsive"><h2>Apple</h2></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"><img src="<?php echo base_url(); ?>pos_assets/images/beverage.jpg" class="img-responsive"><h2>Apple</h2></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"><img src="<?php echo base_url(); ?>pos_assets/images/beverage.jpg" class="img-responsive"><h2>Apple</h2></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

         </div>-->

        <!--<div class="cslide-slide">

            		<div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

        </div>-->

        <!--<div class="cslide-slide">

            		<div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

                    <div class="col-sm-4 col-md-2"><div class="btnPro"></div></div>

        </div>-->

       

    </div>

    <!-- controls -->

    <div class="cslide-prev-next clearfix">

        <span class="cslide-prev">prev slide</span>

        <span class="cslide-next">next slide</span>

    </div>

</div><!-- /sliding content section -->

            

           </div> <!--./ sm 8 -->

        </div>

    

   

    </div>
  <!-- hold items modal start-->
  <div class="modal fade" id="items_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog">
          <div class="modal-container">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
              </div> <!-- /.modal-header -->
              <div class="panel panel-login">
                  <div class="panel-heading">
                      <div class="row">
                          <div class="col-xs-12">
                                   <div id="inv_details" style="display: none;"></div>
                                <div><a href="javascript:void(0)" onclick="getItemDetails()"><span>Invoice No</span><span id="list_inv_html"></span></a></div>
                                 <input type="hidden" id="list_inv_no" name="list_inv_no">
                              <div id="no-more-tables">
                                  <table class="col-md-12 table-bordered table-striped table-condensed cf">
                                      <thead class="cf">
                                      <tr>
                                          <th>Item Name.</th>
                                          <th>Item Picture</th>
                                          <th>Total Items</th>
                                          <th class="numeric">Price</th>
                                      </tr>
                                      </thead>
                                      <tbody id="tb_viewdata">
                                      </tbody>
                                  </table>
                              </div>

                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="modal fade" id="hold_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog">
          <div class="modal-container">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
              </div> <!-- /.modal-header -->
              <div class="panel panel-login">
                  <div class="panel-heading">
                      <div class="row">
                          <div class="col-xs-12">

                              <div id="no-more-tables">
                                  <table class="col-md-12 table-bordered table-striped table-condensed cf">
                                      <thead class="cf">
                                      <tr>
                                          <th>S.No.</th>
                                          <th>Invoice No</th>
                                          <th>Total Items</th>
                                          <th class="numeric">Price</th>
                                      </tr>
                                      </thead>
                                      <tbody id="tb_data">
                                      </tbody>
                                  </table>
                              </div>

                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="<?php echo base_url(); ?>pos_assets/js/jquery.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <script src="<?php echo base_url(); ?>pos_assets/js/bootstrap.min.js"></script>

    <!-- Select Picker -->

    <script src="<?php echo base_url(); ?>pos_assets/js/bootstrap-select.js"></script>

    <!-- Owl Carousel -->

    <script src="<?php echo base_url(); ?>pos_assets/js/owl.carousel.min.js"></script>

      <script src="<?php echo base_url(); ?>pos_assets/js/jqui.js"></script>

   <script type="text/javascript">
   function add_payment_date(inv_status){

	

	

			cus_id = $("#customer_id").val();

			item_name = $("#item_name").val();

            inv_id =$("#list_inv_no").val();

			$("#pos_invoice_status").val(inv_status);	

		

			//pserializedata.push({name: 'customer_id', value: cus_id});

			//pserializedata.push({name: 'item_name', value: item_name});

			

			pserializedata = $("#pos_itemform_data").serialize()+'&'+$.param({ 'customer_id': cus_id,'item_name': item_name,'inv_id': inv_id});

			//var pserializedata = $("#pos_form_data").serialize();

				 $.ajax({

            url: "<?php echo base_url(); ?>pos/save_pos_data",

            type: 'post',



           data: pserializedata,



            cache: false,



            //dataType:"json",



            success: function (data) {

					//window.top.location = '<?php echo base_url(); ?>pos/listpos';
                    if(data){
                        window.top.location = '<?php echo base_url(); ?>sales_management/printed/'+data;
                    }
            }



        });

	}

    function getItemDetails(){

    }
    function viewInvoiceList(tp){

        $.ajax({
            url: "<?php echo base_url(); ?>pos/get_posinvoice_bytype",
            type: 'post',
            data: {'type':tp},
            cache: false,
            success: function (data) {

                $("#tb_data").html(data);
            }
        });
    }

   function viewInvoiceItemListById(inv){
       $('.close').trigger('click');
       $("#item_detail_click").trigger('click');

      $("#list_inv_no").val(inv);
       $("#list_inv_html").html(inv);

       $.ajax({
           url: "<?php echo base_url(); ?>pos/get_posinvoice_byid",
           type: 'post',
           data: {'inv_id':inv},
           cache: false,
           success: function (data) {
                //alert(data);
              $("#tb_viewdata").html(data);
           }
       });
   }
	function addproduct(id){

			//alert(id);

			pname = id;

			$("#item_id"+id).val();

			itemval = $("#sale_price"+id).val();

			pid = $("#item_name"+id).val();

			minitemval = $("#min_val"+id).val();

			//min_val
			//alert(itemval);

			randid = getRandomArbitrary();
			
			ht = '<input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'>';
			//alert(ht);
			//htm = '<tr id="row'+randid+'" class="rows">'+ht+'<td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+pid+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="1"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+' OMR <input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'></td></tr>';
		//	htm = '<tr id="row'+randid+'" class="rows"><td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+pid+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="1"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+' OMR</td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"></tr>';
			///<input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'>
		
			htm = '<tr id="row'+randid+'" class="rows"><td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+pid+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="1"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+' OMR</td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'"></tr>';

		//$("#itemtotalprice"+randid+"_"+pname).val(itemval);	

		$("#rtBox").prepend(htm);

			calculatetotalval(); assignDiscount();

	}
	function getProducts(obj,id){

		//alert('asd');

		//	oo = obj;

		$('.categ_c').removeClass('categ_active');

		$(obj).addClass('categ_active');

		

		  $.ajax({



            url: "<?php echo base_url(); ?>ajax/getCategoryProducts",



            type: 'post',



           data: {'category_id':id},



            cache: false,



            //dataType:"json",



            success: function (data) {



                //response = $.parseJSON(data);



                //var comid = $.parseJSON(formData);

				//alert(data);

                //console.log(data);

				$(".cslide-slides-container").html(data);	

                //



                //pattern = formData;



                

				$("#cslide-slides").cslide();

                



                



                



                



            }



        });

	

	}


    function checkBarcodeMatch(search_data){
        //item_name

    }
	function swapme2p() {



        pid = $("#item_id").val();



        pname = $("#item_name").val();



        $("#item_name").val('');



        //$("#item_id").val(pname);

		//alert(pname);

		itemval = ut.item.sale_price;

		minitemval = ut.item.min_sale_price;

        q = $("#q").val();

		randid = getRandomArbitrary();

         itemval = itemval*q;
        itemval = itemval.toFixed(3);
        htm = '<tr id="row'+randid+'" class="rows"><td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+pid+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="'+q+'"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+' OMR</td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'"></tr>';

		

		//alert(randid);

		//alert(pname);

		//alert(itemval);

		

		//$("#itemtotalprice"+randid+"_"+pname).val(itemval);	

		$("#rtBox").prepend(htm);

			calculatetotalval(); assignDiscount();			  

	//<td style="width:22%;"><input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity" style="width:100%"></td>

    }

   function swapmematch(obj) {



       pid = obj.prod ;



       pname = obj.id;



       $("#item_name").val('');



       //$("#item_id").val(pname);

       //alert(pname);

       itemval = obj.sale_price;

       minitemval = obj.min_sale_price;

       q = $("#q").val();

       randid = getRandomArbitrary();

       itemval = itemval*q;
       itemval = itemval.toFixed(3);
       htm = '<tr id="row'+randid+'" class="rows"><td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+pid+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="'+q+'"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+' OMR</td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'"></tr>';



       //alert(randid);

       //alert(pname);

       //alert(itemval);



       //$("#itemtotalprice"+randid+"_"+pname).val(itemval);

       $("#rtBox").prepend(htm);

       calculatetotalval(); assignDiscount();

        $("#item_name").val('');
        $("#q").val(1);
       //$(':focus').blur();
       $('#item_name').focusTextToEnd();
       //<td style="width:22%;"><input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity" style="width:100%"></td>

   }
	

	function getRandomArbitrary() {

   			 num =Math.floor(Math.random()*10000);

			return  num;

	}

   function assignInvoiceItemListById(inv){
       $('.close').trigger('click');
       $("#list_inv_no").val(inv);
       $("#list_inv_html").html(inv);

       $.ajax({
           url: "<?php echo base_url(); ?>pos/get_posinvoice_byid",
           type: 'post',
           data: {'inv_id':inv},
           cache: false,
           success: function (data) {
               //alert(data);
               $("#tb_viewdata").html(data);
               additemList();

           }
       });
   }

    function additemList(){
        $(".itemname").each(function (index,value) {
            //console.log(index + ": " + value);
            console.log(index + ": " + $(this).text());
            itemname = $(this).text();
            itemprice = $(".itemprice").eq(index).html();
            console.log(itemprice);

            itemid = $(".itemid").eq(index).val();
            console.log(itemid);

            itemq = $(".itemquantity").eq(index).html();
            console.log(itemq);

            randid = getRandomArbitrary();

            itemval = Number(itemprice)*itemq;
            itemval = itemval.toFixed(3);
            htm = '<tr id="row'+randid+'" class="rows"><td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+itemid+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+itemname+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+itemid+')"  class="q'+randid+','+itemid+' form-control" id="quantity'+randid+'_'+itemid+'" name="quantity[]" placeholder="Quantity" value="'+itemq+'"  style="width:100%"></div></td><td id="p'+randid+'_'+itemid+'">'+itemval+' OMR</td><input type="hidden" id="itemid'+randid+'_'+itemid+'"  name="itemid[]" value="'+itemid+'"><input type="hidden" id="itemperprice'+randid+'_'+itemid+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+itemid+'" name="minitemperprice[]" value="'+itemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+itemid+'" name="itemtotalprice[]" value="'+itemval+'"></tr>';

            $("#rtBox").prepend(htm);

            calculatetotalval(); assignDiscount();

                //alert(index);
                    //itemname = $('.item_name').eq(index).html();
                //alert(itemname);
        });

    }
	function assignDiscount(){

			disc = $("#discount_val").val();

			disctype = $("#discount:checked").val();

			//alert(disc);

			if(disc && disctype){

			

				if(disctype == 'omr'){

						//alert('iff');	

						 //final = Number(totalval) * Number(disc);

						 finish = totalval- disc;

				}

				else{

						//alert('else');	

						  final = Number(totalval) * Number(disc);

				          final = final / 100;

				          finish = totalval - final;

							

				}

				

					$('#total_pay_val').html(finish.toFixed(3));

			}

;	}

	function removefromlist(rid,pkid){

		//alert('remove');

		var r = confirm("Are you Sure You Want to Delete");

			if (r == true) {

				//txt = "You pressed OK!";

				$("#row"+rid).remove();

				calculatetotalval(); assignDiscount();

				

			} else {

				txt = "You pressed Cancel!";

			}

	}
	function changeDatalist(pkid,proid){

		//alert('change');

		//alert(pkid);

		itemq = $("#quantity"+pkid+"_"+proid).val();

		itemprice = $("#itemperprice"+pkid+"_"+proid).val();

		totalprice = parseFloat(itemq*itemprice);

        totalprice = totalprice.toFixed(3);
		

			totalp = totalprice+' OMR';

			$("#p"+pkid+"_"+proid).html(totalp);

			$("#itemtotalprice"+pkid+"_"+proid).val(totalprice);	

			calculatetotalval();

	}

	totalval = 0;

	function calculatetotalval(){

		totalval = 0;

			totalnumitems = $('.rows').length;

		$('.p').each(function(i, obj) {

			//test

			//alert(i);

			  //alert($('.p').eq(i).val());

			//alert(obj.value);

			//ob = obj;

			totalval = parseFloat(totalval)+parseFloat(obj.value);

			

					

});

		$("#totalitems").html(totalnumitems);


		$("#totalval").html(totalval.toFixed(3));
       // $("#totalvalup").html(totalval.toFixed(3));

	}
function swapmeu() {



        pid = $("#customer_id").val();

        pname = $("#customer_name").val();

		//alert(pname);

        $("#customer_name").val(pid);

        $("#customer_id").val(pname);

    }
	
	$(document).ready(function () {

			

			//alert('ready');

			var carousel = $("#owl-demo");

		  	carousel.owlCarousel({

			navigation:true,

			navigationText: [

			  "<i class='icon-chevron-left icon-white'><</i>",

			  "<i class='icon-chevron-right icon-white'>></i>"

			  ],

		  });

		    var ac_config = {

            source: "<?php echo base_url(); ?>ajax/getAutoSearchCustomer",

            select: function (event, ui) {

                $("#customer_name").val(ui.item.cus);

				//$("#customername2").val(ui.item.cus);

                $("#customer_id").val(ui.item.cus);

                //$("#").html(ui.item.cus);
                console.log(ui);

			//	uu = ui;

                //swapme();

                //setTimeout('swapme()', 500);

				setTimeout('swapmeu()', 500);

            },

            minLength: 1

        };



        $("#customer_name").autocomplete(ac_config);



		  

		  $("#item_name").autocomplete({



            source: function (request, response) {



                $.getJSON("<?php echo base_url(); ?>ajax/getAutoSearchProductsPos", {category: '', term: $('#item_name').val(), storeid:''},



                response);



            },

              autoFocus: true,
            minLength: 0,


              response: function( event, ui ) {
                      //      alert('response');
                        //    alert(event);
                            itemn = $("#item_name").val();
                            utest = ui;
                            if(utest.content[0].brnumber  == itemn){
                               // ut.item  = utest.content[0];
                               // swapme2p();
                                swapmematch(utest.content[0]);
                            }
              },
            select: function (event, ui) {



                //action



                $("#item_name").val(ui.item.id);



               $("#item_id").val(ui.item.prod);



                




                //alert('m');



                //getProductData(ui.item.id, '', $('#store_id').val());



                //alert($('#store_id').val());



                console.log(ui);

				ut  = ui;

                //swapme();



                setTimeout('swapme2p()', 500);

                $("#q").val(1);
                $(':focus').blur();
                $("#item_name").focus();

            }



        });

		  
			$("#total_paid").keyup(function(){
				
				paid = $(this).val();
				ret = paid - finish;
                $("#totalvalup").html(paid);
				$("#total_returned").html(ret.toFixed(3));
                $("#total_returnedup").html(ret.toFixed(3));
			});
			
		});

   </script>
    <script>

		var oriVal;

		$("#eVal1").on('dblclick', 'span', function () {

			oriVal = $(this).text();

			$(this).text("");

			$("<input type='text' class='thVal'>").appendTo(this).focus();

		});

		$("#eVal1").on('focusout', 'span > input', function () {

			var $this = $(this);

			$this.parent().text($this.val() || oriVal);

			$this.remove(); // Don't just hide, remove the element.

		});

		

		var oriVal;

		$("#eVal2").on('dblclick', 'span', function () {

			oriVal = $(this).text();

			$(this).text("");

			$("<input type='text' class='thVal'>").appendTo(this).focus();

		});

		$("#eVal2").on('focusout', 'span > input', function () {

			var $this = $(this);

			$this.parent().text($this.val() || oriVal);

			$this.remove(); // Don't just hide, remove the element.

		});

		

		var oriVal;

		$("#eVal3").on('dblclick', 'span', function () {

			oriVal = $(this).text();

			$(this).text("");

			$("<input type='text' class='thVal'>").appendTo(this).focus();

		});

		$("#eVal3").on('focusout', 'span > input', function () {

			var $this = $(this);

			$this.parent().text($this.val() || oriVal);

			$this.remove(); // Don't just hide, remove the element.

		});

		

		var oriVal;

		$("#eVal4").on('dblclick', 'span', function () {

			oriVal = $(this).text();

			$(this).text("");

			$("<input type='text' class='thVal'>").appendTo(this).focus();

		});

		$("#eVal4").on('focusout', 'span > input', function () {

			var $this = $(this);

			$this.parent().text($this.val() || oriVal);

			$this.remove(); // Don't just hide, remove the element.

		});

	</script>
	<script src="<?php echo base_url(); ?>pos_assets/js/jquery.cslide.js"></script>

    <script>

		$(document).ready(function(){

			$("#cslide-slides").cslide();
            $.fn.focusTextToEnd = function(){
                this.focus();
                var $thisVal = this.val();
                this.val('').val($thisVal);
                return this;
            }

		});

	</script>

  </body>

</html>