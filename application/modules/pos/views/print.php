<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Quotation</title>
        <link href="<?php echo base_url() ?>durarthem/qprint/css/style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>

        <div id="cont">

            <div id="row-2">


                <div id="logo">
                    <div class="logo-img">

                        <?php if ($quotation->logo_name): ?>  
                            <img src="<?php echo base_url() ?>uploads/company_logo/<?php echo $quotation->logo_name ?>" width="160" height="100" />
                        <?php else: ?>
                            <img src="<?php echo base_url() ?>durarthem/bills/images/logo.jpg" width="160" height="100" />
                        <?php endif; ?>


                    </div>
                </div>


                <div id="bg">
                    <div id="name-com">
                        <h4><?php echo _s($quotation->company_name, 'en') ?></h4>
                    </div>

                    <div class="det">

                        <div class="row-det-1">

                            <h3>Adress :</h3>
                            <p><?php echo $quotation->company_address ?></p>
                        </div>


                        <div class="row-det-1">

                            <h3>Telphone :</h3>
                            <p>+ <?php echo $quotation->company_phone ?></p>
                        </div>

                        <div class="row-det-1">

                            <h3>Fax :</h3>
                            <p>+<?php echo $quotation->company_fax ?></p>
                        </div>

                        <div class="row-det-1">

                            <h3>E-mail :</h3>
                            <p><?php echo $quotation->company_email ?> </p>
                        </div>



                    </div>


                </div>


            </div>

            <div class="Quota"><h3 class="table-1"><?php echo($type=='') ? 'Quotation' : 'Contracts' ;?></h3></div>
            <div class="table-1">
                <h4>Dear Sir Or Madam</h4>
                <p><?php echo $quotation->qnotes ?>
                </p>
            </div>

            <div id="footer"></div>
            <div class="table-2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title-table"><h4>Quotation Summary</h4> </td>
                    </tr>
                </table>
                <table class="tab" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="txt-table">
                            <div class="">
                                <div class="tit-name">
                                    <p> No.  :</p>
                                </div>
                                <div class="tit-name">
                                    <p class="p-out"><?php echo $quotation->quotation_id ?></p>
                                </div>
                            </div>

                        </td>
                    </tr>
                </table>
                <table class="tab" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="txt-table1"><div class="">
                                <div class="tit-name">
                                    <p> Date  :</p>
                                </div>
                                <div class="tit-name">
                                    <p class="p-out"><?php echo date('Y/m/d', strtotime($quotation->quotation_date)); ?></p>
                                </div>
                            </div></td>
                    </tr>
                </table>
                <table class="tab" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="txt-table">
                            <div class="">
                                <div class="tit-name">
                                    <p>Sup Total</p>
                                </div>
                                <div class="tit-name">
                                    <p class="p-out"><?php echo $quotation->quotation_total_amount; ?> OMR</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="tab" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="txt-table1"><div class="">
                                <div class="tit-name">
                                    <p > Discound :</p>
                                </div>
                                <div class="tit-name">
                                    <p class="p-out"><?php echo $quotation->quotation_totalDiscount ?></p>
                                </div>

                            </div>


                        </td>


                    </tr>


                    <tr>
                        <td class="txt-table1"><div class="">
                                <div class="tit-name">
                                    <p > Total :</p>
                                </div>
                                <div class="tit-name">
                                    <p class="p-out"><?php echo $quotation->quotation_total_amount; ?></p>
                                </div>

                            </div>


                        </td>


                    </tr>
                </table>
                <table class="tab" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="end-table"></td>
                    </tr>
                </table>
            </div>
            <div id="footer"></div>



            <table class="wed-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="title-table"><h4>Quotation Details</h4></td>
                </tr>
                <?php foreach ($p_quotations as $p_quotation): ?>
                <tr>
                    <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
                            <tr>
                                <td class="coler-000"><span class="tab21">Serial</span></td>
                                <td class="coler-000"><span class="tab22">Item </span></td>
                                <td class="coler-000"><span class="tab23">Bcrcbde</span></td>
                                <!--<td class="coler-000"><span class="tab24">Discrimination</span></td>-->
                                <td class="coler-000"><span class="tab25">Price</span></td>
                                <td class="coler-000"><span class="tab26">QTY</span></td>
                                <td class="coler-000"><span class="tab27">Total</span></td>
                            </tr>
                            <tr>
                                <td class="coler-001"><span class="tab21"><?php echo $p_quotation->serialnumber ?></span></td>
                                <td class="coler-001"><span class="tab22"><?php echo _s($p_quotation->itemname, 'english') ?> || <?php echo _s($p_quotation->itemname, 'arabic') ?></span></td>
                                <td class="coler-001"><span class="tab23"><?php echo $p_quotation->barcodenumber ?></span></td>
                                <!--<td class="coler-001"><span class="tab24">Here you will Put  words</span></td>-->
                                <td class="coler-001"><span class="tab25"><?php echo $p_quotation->quotation_item_price ?> OMR</span></td>
                                <td class="coler-001"><span class="tab26"><?php echo $p_quotation->quotation_item_quantity ?></span></td>
                                <td class="coler-001"><span class="tab27"><?php echo ($p_quotation->quotation_item_price*(int)$p_quotation->quotation_item_quantity) ?></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><div class="photo">
                            <img src="<?php echo base_url()."uploads/item/".$p_quotation->storeid."/".$p_quotation->product_picture ?>?>" width="264" height="192" />
                        </div></td>
                </tr>
 
                    
           
                    

                <?php endforeach; ?>











                <tr>
                    <td  class="end-table"></td>
                </tr>
            </table>
            <div id="footer"></div>

            <?php if($quotation->Payment && $quotation->Design && $quotation->Completion && $quotation->Validity && $quotation->Note):?>
            <div class="table-3">
                
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title-table"><h4>Payment Teams & Conditions </h4> </td>
                    </tr>
                </table>

                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                    <?php if($quotation->Payment):?>
                    <tr>
                        <td class="coler-0005"><p class="quat">Payment :</p></td>
                        <td class="coler-0015"><p class="quat"><?php echo $quotation->Payment; ?> in Advance</p></td>
                    </tr>
                    <?php endif;?>
                    
                    <?php if($quotation->Design):?>
                    <tr>
                        <td class="coler-0005"><p class="quat">Design :</p></td>
                        <td class="coler-0015"><p class="quat">Balance <?php echo $quotation->Design; ?> at site</p></td>
                    </tr>
                    <?php endif;?>
                    
                    <?php if($quotation->Completion):?>
                    <tr>
                        <td class="coler-0005"><p class="quat">Completion :</p></td>
                        <td class="coler-0015"><p class="quat"><?php echo $quotation->Completion; ?> As per Approved Artwork & Sample</p></td>
                    </tr>
                    <?php endif;?>
                    
                    <?php if($quotation->Validity):?>
                    <tr>
                        <td class="coler-0005"><p class="quat">Validity :</p></td>
                        <td class="coler-0015"><p class="quat"><?php echo $quotation->Validity; ?>  Days For the date of order confirmation and receipts of all relevant approvals, Samples, Drawings, Templates etc.</p></td>
                    </tr>
                    <?php endif;?>
                    
                    <?php if($quotation->Note):?>
                    <tr>
                        <td class="coler-0005"><p class="quat">Note :</p></td>
                        <td class="coler-0015"><p class="quat"><?php echo $quotation->Note; ?> Days From the above date</p></td>
                    </tr>
                    <?php endif;?>
                    
                </table>


               

                <table class="tab" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="end-table"></td>
                    </tr>
                </table>
            </div>
             <?php endif;?>


            <div id="footer-b"></div>
            <div class="last-word"><p>Yours faithfully,<br><span></span></p></div>
            
            <br clear="all"/>
            <br clear="all"/>
            <div id="footer-b"></div>
        </div>

    </body>
</html>
