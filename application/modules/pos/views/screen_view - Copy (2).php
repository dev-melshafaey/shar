<!DOCTYPE html>
<html>
<head>
    <title>Point of Sale</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>screen_css/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>screen_css/css/pos.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>screen_css/css/bootstrap.min.css">
    <!--<script src="<?php echo base_url(); ?>screen_css/src/js/boot.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/js/core.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/js/chrome.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/js/data.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/js/dates.js"></script>-->
    <script src="<?php echo base_url(); ?>screen_css/lib/jquery-1.6.2.min.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/lib/jquery-ui.min.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/lib/underscore-1.1.7.min.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/lib/json2.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/lib/backbone-0.5.1.min.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/js/bootstrap.min.js"></script>
    <style>
        #receipt{
            height:18em;
            overflow-y: scroll;
            position: relative;
        }
        .working{
            background-color: orange;
            color: #fff;
        }
        .done{
            background-color: red;
            color: #fff;
        }
    </style>
</head>
<body>
<?php
//echo "<pre>";
//print_r($holding_orders);
$orders_main = array();
?>

<div class="containerx">
<div class="row">
<div class="col-sm-12">
<div class="row">
<?php
  if(!empty($holding_orders)){
      //  echo "<pre>";
        //print_r($holding_orders);
     //   exit;
      foreach($holding_orders as $i=>$orders){
 //       print_r($orders);

$ordid = $orders->order_id;
          if(!in_array($ordid,$orders_main)){
                $orders_main[] = $ordid;

                if($i !=0){
                  ?><div class="col-md-3 done"><div class="col-md-4"><button id="ordbtn<?php echo $ordid; ?>" onclick="change_order_status('<?php echo $ordid; ?>')" type = "button" class = " col-md-4 btn btn-warning btn-block">Start</button></div> <div id="startclockorder<?php echo $ordid; ?>"></div></div></div></div></div></div><?php
                }
            ?>
<div class="col-sm-3" id="order_main_div<?php echo $ordid ?>">

<div class="row">
<div id='leftpane' class="leftpane">
<div id="getting-started"></div>
    <div>
<div id='receipt'>
    <table>
<thead>
<th colspan="3">Order No #<?php echo $ordid; ?></th>
</thead>
<tr>
    <th colspan="3"><div id="clockorder<?php echo $ordid; ?>"></div> <?php  echo $orders->order_date; $ret = dateDiff($orders->order_date);  $ordersArr[$ordid] = $ret;  ?></th>
</tr>
<tr>
    <th>Item</th>
    <th>Qty</th>
    <th>Size</th>
</tr>
<?php
        ?>
<tr>
    <td><?php echo _s($orders->itemname,'arabic') //echo $orders->itemname ?></td>
    <td><?php echo $orders->invoice_item_quantity; ?></td>
    <td>Size</td>
</tr>
<?php
          }
           else{
               ?>
               <tr>
                   <td><?php echo _s($orders->itemname,'arabic') //echo $orders->itemname ?></td>
                   <td><?php echo $orders->invoice_item_quantity; ?></td>
                   <td>Size</td>
               </tr>
                <?php
           }
      }
  }
?>
    </tbody><tfoot><th class="done" colspan="3"> Timer : 00:00</th></tfoot></table></div></div></div></div>
</div>

</div>


</div>
</div>




<script type="text/template" id="category-template">
    <header>
        <ol class="breadcrum">
            <li>
                <a href="#"><img src="/point_of_sale2/static/img/home.png" class="homeimg"></a>
            </li>
            <% _.each(breadcrumb, function(category) { %>
                <li>
                    <img src="static/img/bc-arrow.png" class="bc-arrow">
                    <a href="#category/<%= category.id %>"><%= category.name %></a>
                </li>
            <% }); %>
        </ol>
        <div class="searchbox">
            <input placeholder="Search Products">
            <img class="search-clear" src="static/img/search_reset.gif">
        </div>
    </header>

    <div id="categories">
        <h4>Categories:</h4>
        <ol>
        <ol>
            <% _.each(categories, function(category) { %>
                <li><a href="#category/<%= category.id %>" class="button"><%= category.name %></a></li>
            <% }); %>
        </ol>
    </div>
</script>

<script type="text/template" id="product-template">
    <a href="#">
        <div class="product-img">
            <img src="data:image/gif;base64,<%= img %>">
            <span class="price-tag"><%= list_price %> &euro;</span>
        </div>
        <div class="product-name"><%= name %></div>
    </a>
</script>

<script type="text/template" id="orderline-template">
    <td><%= name %></td>
    <td><%= list_price.toFixed(2) %> &euro;</td>
    <td><%= discount.toFixed(2) %></td>
    <td><%= quantity.toFixed(0) %></td>
    <td><%= (list_price * (1 - discount/100) * quantity).toFixed(2) %> &euro;</td>
</script>

</body>
<script src="<?php echo base_url(); ?>screen_css/js/jquery.countdown.min.js"></script>
<script type='text/javascript'>
    <?php
    $js_array = json_encode($orders_main);
    //echo " ord_array = ". $js_array . ";\n";
    ?>
    jsarr = '<?php echo $js_array; ?>';
</script>
<script type="text/javascript">
    function change_order_status(id){
        ordh = $("#ordbtn"+id).html();

        if(ordh == 'Start'){
            $("#ordbtn"+id).html('Ready');
            $("#ordbtn"+id).removeClass('btn-warning');
            $("#ordbtn"+id).addClass('btn-success');
            var readySeconds = new Date().getTime();
            sttime = new Date().toLocaleString();
          // alert(sttime);
            $('#startclockorder'+id).countdown(readySeconds, {elapse: true})
                .on('update.countdown', function(event) {
                    var $this = $(this);
                    if (event.elapsed) {
                        $this.html(event.strftime('Timer: <span>%H:%M:%S</span>'));
                    } else {
                        $this.html(event.strftime('Timer: <span>%H:%M:%S</span>'));
                    }
                });


            $.ajax({
                url: "<?php echo base_url(); ?>pos/update_start_time",
                type: 'post',
                data: {'ordnumbr':id,'start_time':sttime,status:'start'},
                cache: false,
                success: function (data) {

                    //alert(data);
                    //  $("#tb_data").html(data);
                    if(data){

                    }
                }
            });

        }
        else{

            $.ajax({
                url: "<?php echo base_url(); ?>pos/update_start_time",
                type: 'post',
                data: {'ordnumbr':id,'start_time':sttime,status:'ready'},
                cache: false,
                success: function (data) {

                    //alert(data);
                    //  $("#tb_data").html(data);
                    if(data){
                        $("#order_main_div"+id).hide();
                    }
                }
            });
        }


    }
    /*$("#getting-started")
        .countdown("2017/01/01", function(event) {
            $(this).text(
                event.strftime('%D days %H:%M:%S')
            );
        });*/

    $(document).ready(function(){
        clockordersAr = $.parseJSON(jsarr);
        var fiveSeconds = new Date().getTime() + 5000;

        clockordersAr.forEach(function(entry) {
           // alert(entry);
            $('#clockorder'+entry).countdown(fiveSeconds, {elapse: true})
                .on('update.countdown', function(event) {
                    var $this = $(this);
                    if (event.elapsed) {
                        $this.html(event.strftime('Order Start: <span>%H:%M:%S</span>'));
                    } else {
                        $this.html(event.strftime('Order Start: <span>%H:%M:%S</span>'));
                    }
                });
        });

    })

    function convertMinutesintoSeconds(minutes){
      //      return =minutes*60;
    }
</script>
</html>
