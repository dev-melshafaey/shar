<!DOCTYPE html>
<html>
<head>
    <title>Point of Sale</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>screen_css/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>screen_css/css/pos.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>screen_css/css/bootstrap.min.css">
    <script src="<?php echo base_url(); ?>screen_css/src/js/boot.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/js/core.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/js/chrome.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/js/data.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/js/dates.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/lib/jquery-1.6.2.min.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/lib/jquery-ui.min.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/lib/underscore-1.1.7.min.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/lib/json2.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/src/lib/backbone-0.5.1.min.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/js/pos.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>screen_css/js/bootstrap.min.js"></script>
    <style>
        .working{
            background-color: orange;
        }
        .done{
            background-color: red;
        }
    </style>
</head>
<body>


<div class="containerx">
<div class="row">
<div class="col-sm-12">
<div class="row">
    <?php
    if(!empty($holding_orders)){
        foreach($holding_orders as $i=>$orders){
            $ordid = $orders->order_id;
            if(!in_array($ordid,$orders_main)) {
                $orders_main[] = $ordid;

                if($i>0){
                    ?>
                    </tbody>
                    <tfoot><th class="done" colspan="3">Timer : 00:00</th></tfoot>
                    </table>
                    </div>
                    <?php
                }
                ?>
                <div class="col-sm-3">

                    <div class="row">
                        <div id='leftpane' class="leftpane">
                            <div id="getting-started"></div>
                            <div id='receipt'>
                                <table>
                                    <thead>
                                    <th colspan="3">Order No #<?php echo $ordid; ?></th>
                                    <tr>

                                        <th colspan="3">Order Time : <span id="clockorder<?php echo $ordid; ?>"></span><?php echo $orders->order_date; echo $ret = dateDiff($orders->order_date); print_r($ret); ?> </th>
                                    </tr>

                                    </thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Qty</th>
                                        <th>Size</th>
                                    </tr>
                                    <tbody>

                                    <tr>
                                        <td><?php echo _s($orders->itemname,'arabic') //echo $orders->itemname ?></td>
                                        <td><?php echo $orders->invoice_item_quantity; ?></td>
                                        <td>Size</td>
                                    </tr>



                        </div>
                    </div>
                </div>  <!--./col-sm-3 -->

            <?php
            }
            else{
                ?>
                <tr>
                    <td><?php echo _s($orders->itemname,'arabic') //echo $orders->itemname ?></td>
                    <td><?php echo $orders->invoice_item_quantity; ?></td>
                    <td>Size</td>
                </tr>

            <?php
            }
            ?>

        <?php
        }
    }
    ?>


</div>

</div>


</div>
</div>




<script type="text/template" id="category-template">
    <header>
        <ol class="breadcrum">
            <li>
                <a href="#"><img src="/point_of_sale2/static/img/home.png" class="homeimg"></a>
            </li>
            <% _.each(breadcrumb, function(category) { %>
                <li>
                    <img src="static/img/bc-arrow.png" class="bc-arrow">
                    <a href="#category/<%= category.id %>"><%= category.name %></a>
                </li>
            <% }); %>
        </ol>
        <div class="searchbox">
            <input placeholder="Search Products">
            <img class="search-clear" src="static/img/search_reset.gif">
        </div>
    </header>

    <div id="categories">
        <h4>Categories:</h4>
        <ol>
            <% _.each(categories, function(category) { %>
                <li><a href="#category/<%= category.id %>" class="button"><%= category.name %></a></li>
            <% }); %>
        </ol>
    </div>
</script>

<script type="text/template" id="product-template">
    <a href="#">
        <div class="product-img">
            <img src="data:image/gif;base64,<%= img %>">
            <span class="price-tag"><%= list_price %> &euro;</span>
        </div>
        <div class="product-name"><%= name %></div>
    </a>
</script>

<script type="text/template" id="orderline-template">
    <td><%= name %></td>
    <td><%= list_price.toFixed(2) %> &euro;</td>
    <td><%= discount.toFixed(2) %></td>
    <td><%= quantity.toFixed(0) %></td>
    <td><%= (list_price * (1 - discount/100) * quantity).toFixed(2) %> &euro;</td>
</script>

</body>
<script src="js/jquery.countdown.min.js"></script>
<script type="text/javascript">
    $("#getting-started")
        .countdown("2017/01/01", function(event) {
            $(this).text(
                event.strftime('%D days %H:%M:%S')
            );
        });
</script>
</html>
