<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>POS System</title>



    <!-- Bootstrap -->

    <link href="<?php echo base_url(); ?>pos_assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/loadercss.css" rel="stylesheet">
    <!-- Reset -->

    <link href="<?php echo base_url(); ?>pos_assets/css/normalize.css" rel="stylesheet">

    <!-- Custom -->

    <link href="<?php echo base_url(); ?>pos_assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>newcss/style.css" rel="stylesheet">

    <!-- Select -->

    <link href="<?php echo base_url(); ?>pos_assets/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- Font Awesome -->

    <link href="<?php echo base_url(); ?>pos_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Owl Carousel -->

    <link href="<?php echo base_url(); ?>pos_assets/css/owl.carousel.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>css/jqcode.css" rel="stylesheet">


    <link rel="stylesheet" href="<?php echo base_url(); ?>css/SimpleCalculadorajQuery.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <style>

        .triggern{
            float: right;
            font-size: 25px;
        }
        .fa-close{
            font-size: 40px !important;
        }
        .categ_active{

            color: #1d465e !important;

            text-decoration: none;

        }
        .btn{
            font-size: 13px;
        }

        .header-fixed {
            position: fixed;
            top: 0px; display:none;
            background-color:white;
        }

        .rows{
            font-size: 10px;
        }

        .ppanel {
            position: absolute;
            top: 106px; right: 0px;
            display: none;
            left: 598px;
            background: #F9F9FF;
            border:1px solid #111111;
            -moz-border-radius-topleft: 20px;
            -webkit-border-top-left-radius: 20px;
            -moz-border-radius-bottomleft: 20px;
            -webkit-border-bottom-left-radius: 20px;
            width: 60%;
            padding: 11px 7px 59px 26px;
            filter: alpha(opacity=85);
            opacity: .85;
            z-index:2;
        }


        .ppanel p{
            margin: 0 0 15px 0;
            padding: 0;
            color: #cccccc;
        }

        .ppanel a, .ppanel a:visited{
            margin: 0;
            padding: 0;
            color: #ff0000;
            text-decoration: none;
            border-bottom: 1px solid #ff0000;
        }

        .ppanel a:hover, .ppanel a:visited:hover{
            margin: 0;
            padding: 0;
            color: #ffffff;
            text-decoration: none;
            border-bottom: 1px solid #ffffff;
        }

        a.trigger{
            position:absolute;
            text-decoration: none;
            top: 66px; right: 0px;
            font-size: 32px;
            letter-spacing:-1px;
            font-family: verdana, helvetica, arial, sans-serif;
            color:#fff;
            font-weight: 700;
            display: block;
            z-index:3;
            color: #000;
        }

        /*.additional, td, th {
            border: 1px solid black;
        }

        .additional table {
            border-collapse: collapse;
            width: 100%;
        }

        .additional th {
            height: 50px;
        }*/


    </style>

</head>

<body>

<header>

    <div class="demo">
        <svg class="loader">
            <filter id="blur">
                <fegaussianblur in="SourceGraphic" stddeviation="2"></fegaussianblur>
            </filter>
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="#F4F519" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-2">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="#DE2FFF" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-3">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="#FF5932" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-4">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="#E97E42" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-5">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="white" stroke-width="6" stroke-linecap="round" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-6">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="#00DCA3" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-7">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="purple" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-8">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="#AAEA33" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
    </div>

    <div class="container" style="padding-left:10px;">

        <div class="row">
            <div class="col-sm-12">
         <!--       <div class="col-sm-2 col-xs-12"><img src="<?php // echo base_url(); ?>pos_assets/images/logo.png" alt="Business Solutions" class="img-responsive bslogo"></div> -->
                <div class="col-sm-7 col-xs-12" style="padding:0px;">
                    <ul class="list-inline" style="margin-top:.5em">
                        <li><a href="javascript:void(0)"  onclick="cancelinvoice()" class="btn btn-success"><span class="glyphicon glyphicon-home" ></span>Home</a></li>
                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#hold_items" onclick="viewInvoiceList('my')" class="btn btn-info"><span class="glyphicon glyphicon-list-alt" ></span> My Invoice</a></li>
                        <li style="display: none;"><a id="view_prices_btn" href="" class="btn btn-default" data-toggle="modal" data-target="#check_price" ><span class="glyphicon glyphicon-plus"></span>prices</a></li>
                    <!--    <li><a href="" class="btn btn-default" data-toggle="modal" data-target="#view_cal" ><span class="glyphicon glyphicon-plus"></span> Calculator</a></li> -->
                        <a href="javascript:void(0)"  id="item_detail_click" data-toggle="modal" data-target="#items_details" style="display: none;"></a>
                        <a href="javascript:void(0)"  id="item_adddetail_click" data-toggle="modal" data-target="#addition_price" style="display: none;"></a>
                        <li><a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#hold_items" onclick="viewInvoiceList('hold')"><span class="glyphicon glyphicon-tag"></span> Hold Orders</a></li>
                        
                        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#cancel_items" onclick="searchCancelInvoice()" class="btn btn-danger"><span class="glyphicon glyphicon-remove" ></span> Cancel Order</a></li>
                        <li><a href="javascript:void(0)" class="btn btn-warning" data-toggle="modal" data-target="#order_items" onclick="getReadyOrders();"><span class="glyphicon glyphicon-shopping-cart"></span> Order <span id="ordercount" class="badge badge-xs badge-danger"><?php echo $readycount; ?></span></a></li>
                    </ul>
                </div>
                <div class="col-sm-2 col-xs-12"><img src="<?php  echo base_url(); ?>img/logo-login.png" alt="Business Solutions" class="img-responsive "></div>
				<div class="col-sm-2" style="float:right;"><a href="<?php echo base_url() ?>logout">Logout </a></div>
                <div class="col-sm-2" style="float:right;"><?php echo $this->session->userdata('bs_username'); ?></div>
                <div class="form-group qpull-left">

    <form id="pos_form_data" >
        <ul class="listbox">
            <li><label for="sel1" class="reflabel">Reference No.</label></li>
            <li><input type="text" class="form-control refbox" id="customer_name" name="customer_name" placeholder="Reference No"><input type="hidden" name="customer_id" id="customer_id" /><input type="hidden" name="usname" id="usname" value="<?php echo $userData->fullname; ?>" /><input type="hidden" name="prev_count" id="prev_count" value="<?php echo $readycount; ?>" /><input type="hidden" class="form-control" id="q" name="q" placeholder="Quantity" value="1"></li>
         <!--   <li><label for="sel1">Quantity</label></li>
            <li><input type="text" class="form-control" id="q" name="q" placeholder="Quantity" value="1"></li> -->
        </ul>
     <!--   <div class="col-sm-6 npadleft"><label for="sel1">Reference No.</label></div>
        <div class="col-sm-6 npadleft"><label for="sel1">Quantity</label></div>

        <!--<select class="selectpicker" style="display: none;">

            <option>Cash</option>

            <option>Ali</option>

            <option>Bob</option>

          </select>

        <div class="col-sm-6 npadleft">
            <input type="text" class="form-control" id="customer_name" name="customer_name" placeholder="Customer Name / Cash"><input type="hidden" name="customer_id" id="customer_id" />
        </div>
        <div class="col-sm-6 npadleft">
            <input type="text" class="form-control" id="q" name="q" placeholder="Quantity" value="1">
        </div> -->

    </form>

</div>

                <div class="col-sm-2 col-xs-12 rttxtnums" style="display: none;"><div class="col-sm-3 col-xs-3 rtxt_head">Total</div><div  class="col-sm-3 col-xs-3 rtxt_nums" id="totalvalup">0.000</div> <br clear="all"> <div class="col-sm-3 col-xs-3 rtxt_head">Return</div><div class="col-sm-3 col-xs-3 rtxt_nums" id="total_returnedup">0.000</div></div>
            </div> <!--./col-sm-12-->
        </div>

    </div>



</header>





<div class="container">





<div class="row">

<div class="col-sm-12">

<!-- Category slider-->

<div class="wrapper-with-margin">

    <div id="owl-demo" class="owl-carousel">

        <?php

        if($categlist){

            foreach($categlist as $categ){

                ?>

                <div><a href="javascript:void(0)" onClick="getProducts(this,'<?php echo $categ->catid; ?>')"  class="categ_c"><?php $cname = unserialize($categ->catname); echo $cname['arabic']; ?> <br> <?php echo $cname['english']; ?></a></div>

            <?php

            }

        }

        ?>

    </div>

</div>



<!-- Content slider -->







</div> <!--./ sm 8 -->
</div>
<div class="row">
<div class="col-sm-7">
    <div id="cslide-slides" class="cslide-slides-master clearfix">



        <div class="cslide-slides-container clearfix">




        </div>

        <!-- controls -->

        <div class="cslide-prev-next clearfix">

            <span class="cslide-prev">prev slide</span>

            <span class="cslide-next">next slide</span>

        </div>

    </div><!-- /sliding content section -->
</div>
<div class="col-sm-5" style="padding-left:5px;padding-right: 5px;">

<div class="rt-box">



<ul class="list-inline" style="display:none;">

    <li class="pull-right">Add New Customer <a href="#"><i class="fa fa-plus"></i></a></li>

    <li><br></li>

</ul>


<!--
<div class="col-sm-12">
            <div class="col-sm-6">1</div>
            <div class="col-sm-6">2</div>
        </div>
-->




<div class="form-group">

    <input type="text" class="form-control tbox barcodebox" id="item_name" name="item_name" onchange="checkBarcodeMatch(this.value)" placeholder="Barcode / Items name"><input type="hidden" id="item_id" name="item_id">

</div> <form id="pos_itemform_data" >

    <input type="hidden" id="invoice_type" name="invoice_type" value="new">
    <input type="hidden" id="invoice_paymemt_type" name="invoice_paymemt_type" value="cash">
    <div class="scroll">

        <table id="rtBox" class="table" style="text-align: center;">

            <thead>

            <tr>

                <th><i class="fa fa-close"></i></th>

                <th  style="text-align: center;">Product</th>

                <th style="text-align: center;">Qty</th>

                <th style="text-align: center;">Price</th>
                <th style="text-align: center;">Size</th>

            </tr>

            </thead>

        </table>





    </div>

    <!-- another table -->





    <table class="table">

        <tbody>

        <tr class="info">



            <td>Total Items</td>



            <td id="totalitems">0</td>



            <td>Total</td>



            <td id="totalval">0</td>

            <input type="hidden" name="discount" id="discount" value="omr" >
        </tr>



        <tr class="warning" style="">



            <td>Discount </td>



            <td><input type="radio" name="discount" id="discount" value="omr" onClick="assignDiscount()" checked> <span class="smaller">OMR</span> <input type="radio" name="discount" id="discount" value="percent" onClick="assignDiscount()"> <span class="smaller">%</span></td>



            <td></td>



            <td><div id='eVal4'><input type="text" class="form-control" id="discount_val" name="discount_val" placeholder="Discount" value="0"  style="width:80px;" onChange="assignDiscount()"></div></td>



        </tr>



        <tr class="success">



            <td>Total Payable</td>



            <td></td>



            <td></td>



            <td id="total_pay_val">0</td>



        </tr>
        <tr style="display: none;">
            <td>Total Paid</td>



            <td></td>



            <td></td>

            <td id="total_paid_html" ><input type="number" class="form-control tbox" id="total_paid" name="total_paid" value="0" style="width:80px;" ></td>

        </tr>
        <tr>
            <td>Returned</td>



            <td></td>



            <td></td>

            <td id="total_returned"></td>

        </tr>


        </tbody>



    </table>





    <!-- Table for buttons -->





    <table class="table">



        <tbody>



        <tr>



            <td style="display: none;"><button onclick="add_payment_date('credit')" type = "button" class = "btn btn-warning btn-block" style="display: none;">Credit</button></td>

            <td colspan="2"><button onclick="cancelinvoice()"  data-toggle="modal"  type = "button" class = "ordbtn btn btn-danger btn-block">Cancel</button></td>
            <td colspan="2"><button onclick="add_payment_date('hold')" type = "button" class = "ordbtn btn btn-warning btn-block">Order</button></td>

            <td  style="display: none;"><button onclick="add_payment_date('payment')" type = "button" class = "btn btn-success btn-block tbox">Payment</button></td>



        </tr>



        </tbody>



    </table>

    <input type="hidden" id="pos_invoice_status" name="pos_invoice_status">
    <input type="hidden" id="storeid" name="storeid" value="<?php echo $storeid; ?>">

</form>



</div> <!--./ rt box -->

</div> <!--./ sm 4 -->





</div>





</div>
<!-- hold items modal start-->
<div class="modal fade" id="items_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">

                    <div class="row">
                        <div class="col-xs-12">
                            <div id="inv_details" style="display: none;"></div>
                            <div><a href="javascript:void(0)" onclick="getItemDetails()"><span>Invoice No</span><span id="list_inv_html"></span></a></div>
                            <input type="hidden" id="list_inv_no" name="list_inv_no">
                            <div id="no-more-tables">
                                <div class="scroll">
                                    <table class="col-md-12 table-bordered table-striped table-condensed cf">
                                        <thead class="cf">
                                        <tr>
                                            <th>Item Name.</th>
                                            <th>Item Picture</th>
                                            <th>Total Items</th>
                                            <th class="numeric">Price</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tb_viewdata">
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="view_cal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-8" style="left: 18%;">

                            <div id="no-more-tables">
                                <div id="idCalculadora"> </div>
                                <div id="micalc"> </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="card_number" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">

                            <div class="g4 form-group search_div">

                                <label class="text-warning">Card Number: <img src="<?php echo base_url(); ?>images/visa-large.gif"></label>

                                <input id="add_card_no" name="add_card_no" type="text" maxlength="8" class="form-control" style="width:35%; text-align: center; position: relative; left: 34%;"/>
                                <br clear="all">
                            </div>
                            <br clear="all">
                            <div class="col-xs-12 form-group search_table" style="margin-top: 20px;">
                                <a class="btn btn-success"  href="javascript:void(0)" onclick="payByCard()">Pay</a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addition_price" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div>Additional Charges <span id="add_charges">0.000</span></div>
                    </div>

                </div>

            </div>

            <div class="pppanel" style="margin-left: 132px; width: 57%;padding:10px;">
                <table class="additional" border="5" style="text-align: center;">
                    <tr><th style="padding: 3px;">Item</th>
                        <th>Price</th>
                        <th style="padding: 2px;">More</th>
                        <th style="padding: 2px;">Less</th>
                        <th style="padding: 2px;">Remove</th>
                    </tr>
                    <?php
                    if(!empty($addData)){
                        foreach($addData as $i=>$ad){
                            ?>
                            <tr>
                                <td  style="padding: 3px;"><input type="hidden" name="itemid<?php echo $ad->ad_item_id; ?>" id="itemid<?php echo $ad->ad_item_id; ?>" value="<?php echo $ad->ad_item_id; ?>"><input type="hidden" name="itemname<?php echo $ad->ad_item_id; ?>" id="itemname<?php echo $ad->ad_item_id; ?>" value="<?php echo $ad->item_eng_name; ?>"><input type="hidden" name="itemprice<?php echo $ad->ad_item_id; ?>" id="itemprice<?php echo $ad->ad_item_id; ?>" value="<?php echo $ad->more_price; ?>"><?php echo $ad->item_eng_name; ?> - <?php echo $ad->item_ar_name; ?></td>
                                <td style="padding: 3px;"><?php if($ad->more_price !="") echo getAmountFormat($ad->more_price); else echo "0.00";   ?></td>
                                <td style="padding: 2px;"><input type="radio" class="additems" id="item<?php echo $ad->ad_item_id; ?>" name="item<?php echo $ad->ad_item_id; ?>" onclick="assignAdditionItem('<?php echo $ad->ad_item_id; ?>')" value="more"></td>
                                <td style="padding: 2px;"><input type="radio" class="additems" id="item<?php echo $ad->ad_item_id; ?>" name="item<?php echo $ad->ad_item_id; ?>" onclick="assignAdditionItem('<?php echo $ad->ad_item_id; ?>')" value="less"></td>
                                <td style="padding: 2px;"><input type="radio" class="additems" id="item<?php echo $ad->ad_item_id; ?>" name="item<?php echo $ad->ad_item_id; ?>" onclick="assignAdditionItem('<?php echo $ad->ad_item_id; ?>')" value="remove"></td>
                            </tr>
                        <?php
                        }
                    }
                    ?>
                </table>
                <div class="col-xs-12 form-group search_table" style="margin-top: 20px;">
                    <input type="hidden" id="rand_additem" name="rand_additem">
                    <input type="hidden" id="item_additem" name="item_additem">
                    <a class="btn btn-success"  href="javascript:void(0)" onclick="add_additional_items()"> Add</a>
                </div>

                <div style="clear:both;"></div>

                         </div>
        </div>


    </div>
</div>


<div class="modal fade" id="check_price" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <!--<div class="row">
                        <div>Additional Charges <span id="add_charges">0.000</span></div>
                    </div>-->
                    <div class="row">
                        <div class="col-xs-12" id="largebtn">
                            <div class="col-md-12"><a style="width: 100%;" href="javascript:void(0)" class="btn btn-primary" onclick="swapme2p('Large')">Large Price <span id="large_sp">(12)</span></a></div>
                        </div>
                    </div>

                    <br clear="all">
                    <div class="row" id="mediumbtn">
                        <div class="col-xs-12">
                            <div class="col-md-12"><a style="width: 100%;" href="javascript:void(0)" class="btn btn-success" onclick="swapme2p('Medium')">Medium Price <span id="medium_sp">(12)</span></a></div>
                        </div>
                    </div>
                    <br clear="all">
                    <div class="row" id="smallbtn">
                        <div class="col-xs-12">
                            <div class="col-md-12"><a style="width: 100%;" href="javascript:void(0)" class="btn btn-warning" onclick="swapme2p('Small')">Small Price <span id="small_sp">(12)</span></a></div>
                        </div>
                    </div>
                    <br  clear="all">
                    <div class="row">
                        <div class="col-xs-12" id="normalbtn">
                            <div class="col-md-12"><a style="width: 100%;" href="javascript:void(0)" class="btn btn-primary" onclick="swapme2p2()">Price <span id="normal_sp">(12)</span></a></div>
                        </div>
                    </div>
                </div>
                <!--<a class="trigger" href="#">+</a>-->

            </div>

        </div>
        <!--<div class="ppanel" style="display: none;">
            <table class="additional" border="5" style="text-align: center;">
                <tr><th style="padding: 3px;">Item</th>
                    <th>Price</th>
                    <th style="padding: 2px;">More</th>
                    <th style="padding: 2px;">Less</th>
                    <th style="padding: 2px;">Remove</th>
                </tr>
                <?php
                /*if(!empty($addData)){
                    foreach($addData as $i=>$ad){
                        ?>
                        <tr>
                            <td  style="padding: 3px;"><input type="hidden" name="itemid<?php echo $ad->ad_item_id; ?>" id="itemid<?php echo $ad->ad_item_id; ?>" value="<?php echo $ad->ad_item_id; ?>"><input type="hidden" name="itemname<?php echo $ad->ad_item_id; ?>" id="itemname<?php echo $ad->ad_item_id; ?>" value="<?php echo $ad->item_eng_name; ?>"><input type="hidden" name="itemprice<?php echo $ad->ad_item_id; ?>" id="itemprice<?php echo $ad->ad_item_id; ?>" value="<?php echo $ad->more_price; ?>"><?php echo $ad->item_eng_name; ?> - <?php echo $ad->item_ar_name; ?></td>
                            <td style="padding: 3px;"><?php if($ad->more_price !="") echo getAmountFormat($ad->more_price); else echo "0.00";   ?></td>
                            <td style="padding: 2px;"><input type="radio" class="additems" id="item<?php echo $ad->ad_item_id; ?>" name="item<?php echo $ad->ad_item_id; ?>" onclick="assignAdditionItem('<?php echo $ad->ad_item_id; ?>')" value="more"></td>
                            <td style="padding: 2px;"><input type="radio" class="additems" id="item<?php echo $ad->ad_item_id; ?>" name="item<?php echo $ad->ad_item_id; ?>" onclick="assignAdditionItem('<?php echo $ad->ad_item_id; ?>')" value="less"></td>
                            <td style="padding: 2px;"><input type="radio" class="additems" id="item<?php echo $ad->ad_item_id; ?>" name="item<?php echo $ad->ad_item_id; ?>" onclick="assignAdditionItem('<?php echo $ad->ad_item_id; ?>')" value="remove"></td>
                        </tr>
                    <?php
                    }
                }*/
                ?>
            </table>
            <div style="clear:both;"></div>
        </div>-->

    </div>
</div>
<div class="modal fade" id="cancel_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">

                            <div class="g4 form-group search_div">

                                <label class="text-warning">Order No:</label>

                                <input id="search_invoice_no" name="search_invoice_no" type="text" class="form-control"/>
                                <br clear="all">
                                <input type="button" value="بحث" class="btn btn-success   green" onclick="searchInvoice()">
                            </div>


                            <div class="search_table" id="no-more-tables" style="display: none;">
                                <h1 id="search_inv_id"></h1>
                                <div class="scroll">
                                <table id="searchtable" class="col-md-12 table-bordered table-striped table-condensed cf">
                                    <thead class="cf">
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Item no</th>
                                        <th>Quantity</th>
                                        <th class="numeric">Price</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tb_search_data">
                                    </tbody>
                                </table>
                                    <table class="header-fixed"></table>
                                    </div>
                                </div>
                            <br clear="all">
                            <div class="col-xs-12 form-group search_table" style="display: none;margin-top: 20px;">
                                <a class="btn btn-danger"  href="javascript:void(0)" onclick="confirmCancel()"> Cancel Invoice</a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="order_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <h1 id="show_u_name"><?php  echo $this->session->userdata('bs_username'); ?></h1>
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="no-more-tables">
                                <div class="scroll">
                                    <table id="hold_table" class="col-md-12 table-bordered table-striped table-condensed cf">
                                        <thead class="cf">
                                        <tr>
                                            <th>Order.No.</th>
                                            <th>Customer</th>
                                            <th class="numeric">Price</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tb_order_data">
                                        </tbody>
                                    </table>
                                    <table class="header-fixed"></table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="hold_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <h1 id="show_u_name" style="display: none;"><?php  echo $this->session->userdata('bs_username'); ?></h1>

                    <div class="row">
                        <div class="col-xs-12">
                            <div id="no-more-tables">
                                <div class="scroll">
                                <table id="hold_table" class="col-md-12 table-bordered table-striped table-condensed cf">
                                    <thead class="cf">
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Order No</th>
                                        <th>Total Items</th>
                                        <th class="numeric">Price</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tb_data">
                                    </tbody>
                                </table>
                                    <table class="header-fixed"></table>
                                </div>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="<?php echo base_url(); ?>pos_assets/js/jquery.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="<?php echo base_url(); ?>pos_assets/js/bootstrap.min.js"></script>

<!-- Select Picker -->

<script src="<?php echo base_url(); ?>pos_assets/js/bootstrap-select.js"></script>

<!-- Owl Carousel -->

<script src="<?php echo base_url(); ?>pos_assets/js/owl.carousel.min.js"></script>

<script src="<?php echo base_url(); ?>pos_assets/js/jqui.js"></script>
<script type="text/javascript">

memtype = '<?php echo $this->session->userdata('bs_memtype'); ?>';
var item_addtional_items = new Array();
//item_addtional_items

function confirmCancel() {
    var r = confirm("Are you Sure you want to Cancel?");
    if (r == true) {
        sinv = $("#search_invoice_no").val();
        $.ajax({
            url: "<?php echo base_url(); ?>pos/cancel_pos_inv",
            type: 'post',
            data: {'inv':sinv},
            cache: false,
            success: function (data) {

              //  $("#tb_data").html(data);
                if(data){
                    alert('Invocie Canceled Successfully');
                    $('.close').trigger('click');

                }
            }
        });
    }
}

function payByCard(){

    cus_id = $("#customer_id").val();

    item_name = $("#item_name").val();
	usname = $("#usname").val();
    inv_id =$("#list_inv_no").val();

    card_no =$("#add_card_no").val();
    //add_payment_date
    cusname = $("#customer_name").val();
    $("#invoice_paymemt_type").val('card');

    $("#pos_invoice_status").val('payment');
    //add_payment_date



    //pserializedata.push({name: 'customer_id', value: cus_id});

    //pserializedata.push({name: 'item_name', value: item_name});



    if(card_no !=""){
        pserializedata = $("#pos_itemform_data").serialize()+'&'+$.param({ 'customer_id': cus_id,'item_name': item_name,'inv_id': inv_id,'card_no': card_no,'usname':usname});

        //var pserializedata = $("#pos_form_data").serialize();

        $.ajax({

            url: "<?php echo base_url(); ?>pos/save_newpost_data",

            type: 'post',



            data: pserializedata,



            cache: false,



            //dataType:"json",



            success: function (data) {

                console.log(data);
                if(data){

                    /*window.open(
                        '<?php echo base_url(); ?>sales_management/printed/'+data,
                        '_blank'
                    );

                    window.top.location = '<?php echo base_url(); ?>pos/add';
                    */
                    window.top.location = '<?php echo base_url(); ?>pos/add';

                }
            }



        });
    }
    else{
        alert('Please Enter Card Number');
    }

}

function cancelinvoice(){
    window.top.location = '<?php echo base_url(); ?>pos/add';
}
function add_payment_date(inv_status){


    cus_id = $("#customer_id").val();

    item_name = $("#item_name").val();
   /// item_name = $("#item_name").val();
    inv_id =$("#list_inv_no").val();

    card_no =$("#add_card_no").val();

    //add_payment_date
    cusname = $("#customer_name").val();
    if(cusname !=""){
            $("#invoice_paymemt_type").val('customer');
    }
    else{
        $("#invoice_paymemt_type").val('cash');
    }

    $("#pos_invoice_status").val(inv_status);


	
    //pserializedata.push({name: 'customer_id', value: cus_id});

    //pserializedata.push({name: 'item_name', value: item_name});


    if(cusname !=""){
		$('.ordbtn').prop('disabled',true);
        pserializedata = $("#pos_itemform_data").serialize()+'&'+$.param({ 'customer_id': cus_id,'item_name': item_name,'inv_id': inv_id,card_no:card_no,customer_name:cusname});

        //var pserializedata = $("#pos_form_data").serialize();

        $.ajax({

            url: "<?php echo base_url(); ?>pos/save_newpost_data",

            type: 'post',



            data: pserializedata,



            cache: false,



            //dataType:"json",



            success: function (data) {

                console.log(data);
                if(data){
					ordernumb = data;
/* window.open(
                        '<?php echo base_url(); ?>sales_management/printed/'+data,
                        '_blank'
                    );*/


                   
							printPosReceipt();
                            printPosReceipt2();
	
                }
            }



        });

    }
    else{
        alert('Please Enter RefrenceNumber');
        $("#customer_name").focus();
    }

}

function printPosReceipt(){
	   inv_status = 'hold';
	cus_id = $("#customer_id").val();

    item_name = $("#item_name").val();
   /// item_name = $("#item_name").val();
    inv_id =$("#list_inv_no").val();
	usname = $("#usname").val();	
    card_no =$("#add_card_no").val();
	//ordernumb = '707';	
    //add_payment_date
    cusname = $("#customer_name").val();
    if(cusname !=""){
            $("#invoice_paymemt_type").val('customer');
    }
    else{
        $("#invoice_paymemt_type").val('cash');
    }

    $("#pos_invoice_status").val(inv_status);



	pserializedata = $("#pos_itemform_data").serialize()+'&'+$.param({ 'customer_id': cus_id,'item_name': item_name,'inv_id': inv_id,card_no:card_no,customer_name:cusname,order_id:ordernumb,'usname':usname});

        //var pserializedata = $("#pos_form_data").serialize();

        $.ajax({

            url: "<?php echo base_url(); ?>phpprinter/example/pos_receipt.php",

            type: 'post',



            data: pserializedata,



            cache: false,



            //dataType:"json",



            success: function (data) {

                //console.log(data);
              //   window.top.location = '<?php echo base_url(); ?>pos/add';
            }



        });
}

function printPosReceipt2(){
    inv_status = 'hold';
    cus_id = $("#customer_id").val();

    item_name = $("#item_name").val();
    /// item_name = $("#item_name").val();
    inv_id =$("#list_inv_no").val();
    usname = $("#usname").val();
    card_no =$("#add_card_no").val();
    //ordernumb = '707';
    //add_payment_date
    cusname = $("#customer_name").val();
    if(cusname !=""){
        $("#invoice_paymemt_type").val('customer');
    }
    else{
        $("#invoice_paymemt_type").val('cash');
    }

    $("#pos_invoice_status").val(inv_status);



    pserializedata = $("#pos_itemform_data").serialize()+'&'+$.param({ 'customer_id': cus_id,'item_name': item_name,'inv_id': inv_id,card_no:card_no,customer_name:cusname,order_id:ordernumb,'usname':usname});

    //var pserializedata = $("#pos_form_data").serialize();

    $.ajax({

        url: "<?php echo base_url(); ?>phpprinter/example/pos_receipt_cold.php",

        type: 'post',



        data: pserializedata,



        cache: false,



        //dataType:"json",



        success: function (data) {

            //console.log(data);
            if(memtype == '5'){
                window.top.location = '<?php echo base_url(); ?>sales_management/printed_iframe/'+ordernumb;

            }
        }



    });
}

function getReadyOrdersCount(){
    $.ajax({
        url: "<?php echo base_url(); ?>pos/getReadyCount",
        type: 'get',
        cache: false,
        success: function (data) {
          //  alert(data);
		  // audioElement.play();
		  prevCount = $("#prev_count").val();
		  prevCount = parseInt(prevCount);
		  newnumb = parseInt(data);
		  if(newnumb>prevCount){
		  		audioElement.play();
		  }
		  	$("#prev_count").val(newnumb);
            $("#ordercount").html(data);
        }
    });
}
function getReadyOrders(){

    $("#tb_order_data").html('');
    $('#invoice_type').val(tp);
    $(".demo").show();
    $.ajax({
        url: "<?php echo base_url(); ?>pos/get_ready_Orders",
        type: 'post',
        data: {'type':tp},
        cache: false,
        success: function (data) {
            $(".demo").hide();
            $("#tb_order_data").html(data);
        }
    });
}

function add_payment(invid,cusid,amount){
    var r = confirm("Do you want to Print Receipt");

    if (r == true) {
        p_receipt = 1;
        payMoney(invid,cusid,amount,p_receipt);
    }
    else{
        p_receipt = 0;
        payMoney(invid,cusid,amount,p_receipt);
    }

}

function payMoney(invid,cusid,amount,p_receipt){
    $.ajax({
        url: "<?php echo base_url(); ?>pos/update_payment_invoice",
        type: 'post',
        data: {'invid':invid,am:amount,customer_id:cusid,p_recp:p_receipt},
        cache: false,
        success: function (data) {

            $("#tr_"+invid).hide();
            //$(".demo").hide();
            //$("#tb_order_data").html(data);
            /*window.open(
                '<?php echo base_url(); ?>sales_management/printed/'+invid,
                '_blank'
            );*/
        }
    });
}
function viewInvoiceList(tp){

    if(tp == 'my'){
        $('#show_u_name').show();
    }
    else{
        $('#show_u_name').hide();
    }

    $("#tb_data").html('');
    $('#invoice_type').val(tp);
    $(".demo").show();
    $.ajax({
        url: "<?php echo base_url(); ?>pos/get_posinvoice_bytype",
        type: 'post',
        data: {'type':tp},
        cache: false,
        success: function (data) {
            $(".demo").hide();
            $("#tb_data").html(data);
        }
    });
}

function searchInvoice(){

    search_invoice_no = $("#search_invoice_no").val();
    $(".demo").show();
    sinv = 'Order #'+search_invoice_no;
    $("#search_inv_id").html(sinv);
    $.ajax({
        url: "<?php echo base_url(); ?>pos/search_invoice_sales_new",
        type: 'post',
        data: {'sin':search_invoice_no},
        cache: false,
        success: function (data) {

            $(".search_div").hide();

            $("#tb_search_data").html(data);
            $(".search_table").show();
            $(".demo").hide();

        }
    });
}

function viewInvoiceItemListById(inv){
    $('.close').trigger('click');
    $("#item_detail_click").trigger('click');

    $("#list_inv_no").val(inv);
    $("#list_inv_html").html(inv);
    $(".demo").show();
    $.ajax({
        url: "<?php echo base_url(); ?>pos/get_posinvoice_byid",
        type: 'post',
        data: {'inv_id':inv},
        cache: false,
        success: function (data) {
            //alert(data);
            $("#tb_viewdata").html(data);
            $(".demo").hide();
        }
    });
}

function addproduct(id){

    //alert(id);

    pname = id;

    $("#item_id"+id).val();

    itemval = $("#sale_price"+id).val();

    pid = $("#item_name"+id).val();

    minitemval = $("#min_val"+id).val();

    //min_val
    //alert(itemval);

    randid = getRandomArbitrary();

    ht = '<input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'>';
    //alert(ht);
    //htm = '<tr id="row'+randid+'" class="rows">'+ht+'<td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+pid+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="1"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+' OMR <input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'></td></tr>';
    //	htm = '<tr id="row'+randid+'" class="rows"><td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+pid+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="1"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+' OMR</td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"></tr>';
    ///<input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'>

    htm = '<tr id="row'+randid+'" class="rows"><td style="width:5%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:54%;"><span class="ProName"><a href="#">'+pid+'</a></span></td><td style="width:26%;"><div id="eVal1" ><input type="text" onchange="changeDatalist('+randid+','+pname+')" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control form-control-sm" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="1"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+' OMR</td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'"></tr>';

    //$("#itemtotalprice"+randid+"_"+pname).val(itemval);

    $("#rtBox").prepend(htm);

    calculatetotalval(); assignDiscount();

}
function getProducts(obj,id){

    //alert('asd');

    //	oo = obj;

    $('.categ_c').removeClass('categ_active');

    $(obj).addClass('categ_active');


    $(".demo").show();
    $.ajax({



        url: "<?php echo base_url(); ?>ajax/getCategoryProducts",



        type: 'post',



        data: {'category_id':id},



        cache: false,

        success: function (data) {
            $(".cslide-slides-container").html(data);

            $("#cslide-slides").cslide();
            $(".demo").hide();
        }



    });



}


function checkBarcodeMatch(search_data){
    //item_name

}

tp = '';
function viewCategPrices(itemid){
    small_price = $("#small_val"+itemid).val();
    if(small_price !="0.000"){
        $("#small_sp").html(small_price);
        //$("#small_sp").html(utitem.large_price);
        $("#smallbtn").show();
    }
    else{
        $("#smallbtn").hide();
    }

    medium_price = $("#medium_val"+itemid).val();

    if(medium_price !="0.000"){
        $("#medium_sp").html(medium_price);
        $("#mediumbtn").show();
    }else{
        $("#mediumbtn").hide();
    }
    large_price = $("#large_val"+itemid).val();
    if(large_price !="0.000"){
        $("#large_sp").html(large_price);
        $("#largebtn").show();
    }else{
        $("#largebtn").hide();
    }

    sale_price = $("#sale_price"+itemid).val();
    if(sale_price !="0.000"){
        $("#normal_sp").html(sale_price);
        $("#normalbtn").show();
    }else{
        $("#normalbtn").hide();
    }

    tp = 'category';
    itemGlobalval = itemid;
    $("#view_prices_btn").trigger('click');
}
function showPrices(){
//    testob  = obj;

    pid = $("#item_id").val();



    pname = $("#item_name").val();



    $("#item_name").val(pid);
//    $("#item_id").val(pname);

    if(utitem.small_price !="0.000"){
        $("#small_sp").html(utitem.small_price);
        //$("#small_sp").html(utitem.large_price);
        $("#smallbtn").show();
    }
    else{
        $("#smallbtn").hide();
    }

    if(utitem.large_price !="0.000"){
        $("#large_sp").html(utitem.large_price);
        $("#largebtn").show();
    }else{
        $("#largebtn").hide();
    }

    if(utitem.medium_price !="0.000"){
        $("#medium_sp").html(utitem.medium_price);
        $("#mediumbtn").show();
    }else{
        $("#mediumbtn").hide();
    }
    tp = '';
    $("#view_prices_btn").trigger('click');
}

function swapme2p2(itemid) {


    item_has_one = $("#item_has_one"+itemid).val();
    itemGlobalval =  itemid;
    $('.additems').attr('checked', false);
    chargesval = $("#add_charges").html();
    type='';
    tp = 'category';
    if(tp){
        pname = itemGlobalval;
        total = MultiArray.length;
        innerhtmlData = '';
        innerhiddenData = '';
        if(total>0){
            for(a=0;a<total;a++){
                additemId = MultiArray[a][0];
                additemData = MultiArray[a][3];
                additemval = MultiArray[a][1];
                additemprice = MultiArray[a][2];
                //alert(additemData);
                innerhtmlData+='<br><span style="font-size: 10px;">'+additemData+':'+additemval+',</span>';
                // innerhiddenData+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_id]" value="'+additemId+'>';
                // innerhiddenData+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_val]" value="'+additemval+'>';
                //innerhiddenData+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_price]" value="'+additemprice+'>';
            }
        }

//        alert('call');

        itemid = $("#item_id"+itemGlobalval).val();

        itemtp = $("#tp_cat"+itemGlobalval).val();
        //alert(itemtp+'itemGlobalval');
        //alert(itemid);
        itemval = $("#sale_price"+itemGlobalval).val();

        //itemval = ut.item.sale_price;

        pid = $("#item_name"+itemGlobalval).val();
        par =   $("#item_name_ar"+itemGlobalval).val();
        minitemval = $("#min_val"+itemGlobalval).val();

        itemval = parseFloat(itemval)+parseFloat(chargesval);
        itemval = itemval.toFixed(3);
        //min_val
        //alert(itemval);

        randid = getRandomArbitrary();

        ht = '<input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'>';

        htm = '<tr id="row'+randid+'" class="rows"><td style="width:5%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:54%;"><a href="#"><span class="pname">'+pid+'</span><span class="pname">'+par+' '+innerhtmlData+'</span></a></td><td style="width:26%;"><div id="eVal1" ><input type="number" onchange="changeDatalist('+randid+','+pname+')" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control form-control-sm" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="1"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+'</td><td>'+type+'<a class="triggern" onclick="triggetPlus('+randid+','+pname+')" href="javascript:void(0)">+</a></td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'">';
        htm+='<input type="hidden" id="itemtype'+randid+'_'+pname+'"  name="itemtype[]" value="'+type+'">';
        htm+='<input type="hidden" id="itemtar'+randid+'_'+pname+'"  name="itemtar[]" value="'+par+'">';
        htm+='<input type="hidden" id="itemtpcat'+randid+'_'+pname+'"  name="itemtpcat[]" value="'+itemtp+'"></tr>';
        if(total>0){
            for(a=0;a<total;a++){
                additemId = MultiArray[a][0];
                additemData = MultiArray[a][3];
                additemval = MultiArray[a][1];
                additemprice = MultiArray[a][2];
                //alert(additemData);
                htm+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_id]" value="'+additemId+'">';
                //  additemData = MultiArray[a][3]
                htm+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_val]" value="'+additemval+'">';
                htm+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_name]" value="'+additemData+'">';
                htm+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_price]" value="'+additemprice+'">';
            }
        }
        //htm+=innerhiddenData;
        htm+='<input type="hidden" id="itemteng'+randid+'_'+pname+'"  name="itemteng[]" value="'+pid+'"></tr>';

    } else{
        pid = $("#item_id").val();

        pname = $("#item_name").val();


        $("#item_name").val('');

        itemval = ut.item.sale_price;

        minitemval = ut.item.min_sale_price;
        par = ut.item.prod_ar;
        pname = ut.item.id;
        itemval = parseFloat(itemval)+parseFloat(chargesval);
        itemval = itemval.toFixed(3);

        q = $("#q").val();

        randid = getRandomArbitrary();

        itemval = itemval*q;
        itemval = itemval.toFixed(3);

        total = MultiArray.length;
        innerhtmlData = '';
        for(a=0;a<total;a++){
            additemData = MultiArray[a][3];
            //alert(additemData);
            innerhtmlData+='<span>'+additemData+'</span>';
        }
        htm = '<tr id="row'+randid+'" class="rows"><td style="width:5%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#"><span class="pname">'+pid+'</span><span class="pname">'+par+' '+innerhtmlData+'</span></a></span></td><td style="width:22%;"><div id="eVal1" ><input type="number" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="'+q+'"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+'</td><td>'+type+'</td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'">';
        htm+='<input type="hidden" id="itemtype'+randid+'_'+pname+'"  name="itemtype[]" value="'+type+'">';
        htm+='<input type="hidden" id="itemtar'+randid+'_'+pname+'"  name="itemtar[]" value="'+par+'">';
        htm+='<input type="hidden" id="itemteng'+randid+'_'+pname+'"  name="itemteng[]" value="'+pid+'"></tr>';
    }

    $('.close').trigger('click');


    $("#rtBox").prepend(htm);

    calculatetotalval(); assignDiscount();
    MultiArray = new Array();

    updatePrices();

    //<td style="width:22%;"><input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity" style="width:100%"></td>

}


function add_additional_items(){

    pname = $("#item_additem").val();
    rand = $("#rand_additem").val();
    total = MultiArray.length;
    //alert(MultiArray.length);
    //alert(additemData);
    htmadd = '';
    innerhtmlData = '';
    //totaladdprice = 0;
    totaladdprice = $("#itemperprice"+rand+"_"+pname).val();
    if(total>0){
        for(a=0;a<total;a++){
            additemId = MultiArray[a][0];
            additemData = MultiArray[a][3];
            additemval = MultiArray[a][1];
            additemprice = MultiArray[a][2];
            if(additemval == 'more'){
                //alert(additemval);
                //alert(totaladdprice);
                if(additemprice !=""){
                    //alert(additemprice);
                    totaladdprice = parseFloat(totaladdprice)+parseFloat(additemprice);
                    //totaladdprice.toFixed(3);
                  //  alert(totaladdprice);
                }


            }
            qt = $("#quantity"+rand+"_"+pname).val();
            //alert(additemData);
            innerhtmlData+='<br><span style="font-size: 10px;">'+additemData+':'+additemval+',</span>';
            // innerhiddenData+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_id]" value="'+additemId+'>';
            // innerhiddenData+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_val]" value="'+additemval+'>';
            //innerhiddenData+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_price]" value="'+additemprice+'>';
            htmadd+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_id]" value="'+additemId+'">';
            //  additemData = MultiArray[a][3]
            htmadd+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_val]" value="'+additemval+'">';
            htmadd+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_name]" value="'+additemData+'">';
            htmadd+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_price]" value="'+additemprice+'">';

        }

        totaladdprice = totaladdprice*qt;
        //alert(totaladdprice);

    }

    //
    if(totaladdprice>0){
               itemprice = $("#itemperprice"+rand+"_"+pname).val();
                itemval= parseFloat(totaladdprice);
                //itemval.toFixed(3);
                //alert(itemval);
                $("#itemperprice"+rand+"_"+pname).val(itemval.toFixed(3));
                $("#p"+rand+"_"+pname).html(itemval.toFixed(3));
     }

    $("#itemAdddata"+rand).append(innerhtmlData);

    $("#row"+rand).append(htmadd);
    $("#item_additem").val('');
    $("#rand_additem").val('');
    calculatetotalval(); assignDiscount();
    $('.close').trigger('click');
}
function swapme2p(type) {


	//alert(type);
    item_has_one = $("#item_has_one"+itemGlobalval).val();
    $('.additems').attr('checked', false);
    chargesval = $("#add_charges").html();
    if(item_has_one){
        tp = 'category';
    }

    if(tp){
        pname = itemGlobalval;
        //alert(total);

        total = MultiArray.length;
        innerhtmlData = '';
        innerhiddenData = '';
        if(total>0){
            for(a=0;a<total;a++){
                additemId = MultiArray[a][0];
                additemData = MultiArray[a][3];
                additemval = MultiArray[a][1];
                additemprice = MultiArray[a][2];
                //alert(additemData);
                innerhtmlData+='<br><span style="font-size: 10px;">'+additemData+':'+additemval+',</span>';
               // innerhiddenData+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_id]" value="'+additemId+'>';
               // innerhiddenData+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_val]" value="'+additemval+'>';
                //innerhiddenData+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_price]" value="'+additemprice+'>';
            }
        }

//        alert('call');

        itemid = $("#item_id"+itemGlobalval).val();

        itemtp = $("#tp_cat"+itemGlobalval).val();
        //alert(itemtp+'itemGlobalval');
        //alert(itemid);
        itemval = $("#sale_price"+itemGlobalval).val();

        if(type  == 'Large'){
            itemval = $("#large_val"+itemGlobalval).val();
        }
        else if(type  == 'Medium'){
            itemval = $("#medium_val"+itemGlobalval).val();
        }
        else{
            itemval = $("#small_val"+itemGlobalval).val();
        }

        pid = $("#item_name"+itemGlobalval).val();
        par =   $("#item_name_ar"+itemGlobalval).val();
        minitemval = $("#min_val"+itemGlobalval).val();

        itemval = parseFloat(itemval)+parseFloat(chargesval);
        itemval = itemval.toFixed(3);
        //min_val
        //alert(itemval);

        randid = getRandomArbitrary();
//        alert('test');
        ht = '<input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'>';

        htm = '<tr id="row'+randid+'" class="rows"><td style="width:5%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:54%;"><a href="#"><span class="pname">'+pid+'</span><span class="pname" id="itemAdddata'+randid+'">'+par+' '+innerhtmlData+'</span></a></td><td style="width:26%;"><div id="eVal1" ><input type="number" onchange="changeDatalist('+randid+','+pname+')" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control form-control-sm" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="1"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+'</td><td>'+type+' <a class="triggern" onclick="triggetPlus('+randid+','+pname+')" href="javascript:void(0)">+</a></td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'">';
		htm+='<input type="hidden" id="itemtype'+randid+'_'+pname+'"  name="itemtype[]" value="'+type+'">';
		htm+='<input type="hidden" id="itemtar'+randid+'_'+pname+'"  name="itemtar[]" value="'+par+'">';
        htm+='<input type="hidden" id="itemtpcat'+randid+'_'+pname+'"  name="itemtpcat[]" value="'+itemtp+'">';
        if(total>0){
            for(a=0;a<total;a++){
                additemId = MultiArray[a][0];
                additemData = MultiArray[a][3];
                additemval = MultiArray[a][1];
                additemprice = MultiArray[a][2];
                //alert(additemData);
                htm+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_id]" value="'+additemId+'">';
                //  additemData = MultiArray[a][3]
                htm+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_val]" value="'+additemval+'">';
                htm+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_name]" value="'+additemData+'">';
                htm+='<input type="hidden"  name="additionalItems['+pname+']['+a+'][item_price]" value="'+additemprice+'">';
            }
        }
        //htm+=innerhiddenData;
		htm+='<input type="hidden" id="itemteng'+randid+'_'+pname+'"  name="itemteng[]" value="'+pid+'"></tr>';
		
    } else{
        pid = $("#item_id").val();

        pname = $("#item_name").val();


        $("#item_name").val('');

        if(type  == 'Large'){
            itemval = ut.item.large_price;
        }
        else if(type  == 'Medium'){
            itemval = ut.item.medium_price;
        }
        else{
            if(ut.item.small_price !="")
            itemval = ut.item.small_price;
            else
             itemval = ut.item.sale_price
        }

        minitemval = ut.item.min_sale_price;
        par = ut.item.prod_ar;
        pname = ut.item.id;
        itemval = parseFloat(itemval)+parseFloat(chargesval);
        itemval = itemval.toFixed(3);

        q = $("#q").val();

        randid = getRandomArbitrary();

        itemval = itemval*q;
        itemval = itemval.toFixed(3);

        total = MultiArray.length;
        innerhtmlData = '';
        for(a=0;a<total;a++){
            additemData = MultiArray[a][3];
            //alert(additemData);
            innerhtmlData+='<span>'+additemData+'</span>';
        }
        htm = '<tr id="row'+randid+'" class="rows"><td style="width:5%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#"><span class="pname">'+pid+'</span><span class="pname">'+par+' '+innerhtmlData+'</span></a></span></td><td style="width:22%;"><div id="eVal1" ><input type="number" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="'+q+'"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+'</td><td>'+type+'<a class="triggern" onclick="triggetPlus('+randid+','+pname+')" href="javascript:void(0)">+</a></td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'">';
		htm+='<input type="hidden" id="itemtype'+randid+'_'+pname+'"  name="itemtype[]" value="'+type+'">';
		htm+='<input type="hidden" id="itemtar'+randid+'_'+pname+'"  name="itemtar[]" value="'+par+'">';
		htm+='<input type="hidden" id="itemteng'+randid+'_'+pname+'"  name="itemteng[]" value="'+pid+'"></tr>';
    }

    $('.close').trigger('click');


    $("#rtBox").prepend(htm);

    calculatetotalval(); assignDiscount();
    MultiArray = new Array();

    updatePrices();

    //<td style="width:22%;"><input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity" style="width:100%"></td>

}

function swapmematch(obj) {



    pid = obj.prod ;



    pname = obj.id;



    $("#item_name").val('');



    //$("#item_id").val(pname);

    //alert(pname);

    itemval = obj.sale_price;

    minitemval = obj.min_sale_price;

    q = $("#q").val();

    randid = getRandomArbitrary();

    itemval = itemval*q;
    itemval = itemval.toFixed(3);
    htm = '<tr id="row'+randid+'" class="rows"><td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+pid+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="'+q+'"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+' OMR</td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'"></tr>';



    //alert(randid);

    //alert(pname);

    //alert(itemval);



    //$("#itemtotalprice"+randid+"_"+pname).val(itemval);

    $("#rtBox").prepend(htm);

    calculatetotalval(); assignDiscount();

    $("#item_name").val('');
    $("#q").val(1);
    //$(':focus').blur();
    $('#item_name').focusTextToEnd();
    //<td style="width:22%;"><input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity" style="width:100%"></td>

}


function getRandomArbitrary() {

    num =Math.floor(Math.random()*10000);

    return  num;

}

function assignInvoiceItemListById(inv){
    $("#rtBox").html('');
    $('#tb_search_data').html('');
    $('.close').trigger('click');
    $("#list_inv_no").val(inv);
    $("#list_inv_html").html(inv);
    $(".demo").show();
    $.ajax({
        url: "<?php echo base_url(); ?>pos/get_posinvoice_byid",
        type: 'post',
        data: {'inv_id':inv},
        cache: false,
        success: function (data) {
            //alert(data);
            $("#tb_viewdata").html(data);
            additemList();

            $(".demo").hide();
        }
    });
}

//item_addtional_items = {};
//addfoodArray = {};
MultiArray = new Array();

function check_upadte_ar(itemid,itemval){
    mlen = MultiArray.length;
    for(a=0;a<mlen;a++){
        if(MultiArray[a][0] == itemid){
            //mprice= MultiArray[a][2];
            //totalprice = parseFloat(totalprice)+parseFloat(mprice);
            //totalprice = totalprice.toFixed(3);
            MultiArray[a][1] = itemval;
            return true;
        }
    }
        return false;

}
function updatePrices(){
    //largep = $("#large_sp").html();
    //smallp = $("#medium_sp").html();
    //smallp = $("#small_sp").html();
    //for()
    totalprice = 0;
    mlen = MultiArray.length;
    for(a=0;a<mlen;a++){
        if(MultiArray[a][1] == 'more'){
            mprice= MultiArray[a][2];
            totalprice = parseFloat(totalprice)+parseFloat(mprice);
            totalprice = totalprice.toFixed(3);
        }
    }
    if(mlen>0){
        $("#add_charges").html(totalprice);
    }
    else{
        $("#add_charges").html(0.00);
    }

}

function assignAdditionItem(itemadid){
   // alert(itemid);
    itemlen = MultiArray.length;
    ind = 0;
    //itemid = $("#itemid"+itemadid).val();
    itemval = $('input[name="item'+itemadid+'"]:checked').val();
    itemprice = $("#itemprice"+itemadid).val();
    itemname = $("#itemname"+itemadid).val();

   // $("#item"+itemid).val();
    if(itemlen>0){
        //alert('iff');

        resp = check_upadte_ar(itemadid,itemval);
        if(!resp){
            ind = itemlen;
            arr = new Array();

            //item_addtional_items[ind] = [];
            arr[0] = itemadid;
            arr[1] = itemval;
            arr[2] = itemprice;
            arr[3] = itemname;
            MultiArray[ind] = arr;

        }
        //addfoodArray["item_id"] = itemadid;
        //addfoodArray["item_value"] = itemval;
        //addfoodArray["item_price"] = itemprice;
        //item_addtional_items.[[ind] = addfoodArray;
        //item_addtional_items = {[ind]: addfoodArray};
    }
    else{
        arr = new Array();
            //MultiArray[0] = new Array();
        //MultiArray.push(new Array());
       // alert('else');
        //item_addtional_items[0] = [];
        arr[0] = itemadid;
        arr[1] = itemval;
        arr[2] = itemprice;
        arr[3] = itemname;
        //result[i].push(itemadid);
        //addfoodArray["item_id"] = itemadid;
        //addfoodArray["item_value"] = itemval;
       // addfoodArray["item_price"] = itemprice;
        //item_addtional_items.[0] = addfoodArray;
     //   item_addtional_items = {0: addfoodArray};
        htmn = '<input type="hidden" class="p" id="aditem" name="aditemid[]" value="'+itemadid+'>';
        htmn = '<input type="hidden" class="p" id="aditemval" name="aditemval[]" value="'+itemval+'>';
        htmn = '<input type="hidden" class="p" id="aditemval" name="aditemval[]" value="'+itemval+'>';
        MultiArray[0] = arr;
    }
    updatePrices();
}
function additemList(){
    $(".itemname").each(function (index,value) {
        //console.log(index + ": " + value);
        console.log(index + ": " + $(this).text());
        itemname = $(this).text();
        itemprice = $(".itemprice").eq(index).html();
        console.log(itemprice);

        itemid = $(".itemid").eq(index).val();
        console.log(itemid);

        itemq = $(".itemquantity").eq(index).html();
        console.log(itemq);

        randid = getRandomArbitrary();

        itemval = Number(itemprice)*itemq;
        itemval = itemval.toFixed(3);
        htm = '<tr id="row'+randid+'" class="rows"><td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+itemid+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+itemname+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onchange="changeDatalist('+randid+','+itemid+')" onkeyup="changeDatalist('+randid+','+itemid+')"  class="q'+randid+','+itemid+' form-control" id="quantity'+randid+'_'+itemid+'" name="quantity[]" placeholder="Quantity" value="'+itemq+'"  style="width:100%"></div></td><td id="p'+randid+'_'+itemid+'">'+itemval+' OMR</td><input type="hidden" id="itemid'+randid+'_'+itemid+'"  name="itemid[]" value="'+itemid+'"><input type="hidden" id="itemperprice'+randid+'_'+itemid+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+itemid+'" name="minitemperprice[]" value="'+itemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+itemid+'" name="itemtotalprice[]" value="'+itemval+'"></tr>';

        $("#rtBox").prepend(htm);

        calculatetotalval(); assignDiscount();

        //alert(index);
        //itemname = $('.item_name').eq(index).html();
        //alert(itemname);
    });

}
function assignDiscount(){

    disc = $("#discount_val").val();

    disctype = $("#discount:checked").val();

//    alert(disc);
  //  alert(disctype);
    if(disc && disctype){



        if(disctype == 'omr'){

            //alert('iff');

            //final = Number(totalval) * Number(disc);

            finish = totalval- disc;

        }

        else{

            //alert('else');

            final = Number(totalval) * Number(disc);

            final = final / 100;

            finish = totalval - final;



        }



        $('#total_pay_val').html(finish.toFixed(3));

    }

    	}

function removefromlist(rid,pkid){

    //alert('remove');

    var r = confirm("Are you Sure You Want to Delete");

    if (r == true) {

        //txt = "You pressed OK!";

        $("#row"+rid).remove();

        calculatetotalval(); assignDiscount();



    } else {

        txt = "You pressed Cancel!";

    }

}

function searchCancelInvoice(){
    $(".search_div").show();

    tblid = 'searchtable';
    fixedHead(tblid);

    $("#tb_search_data").html('');
    $("#search_invoice_no").val('');
    $(".search_table").hide();
}

function payByCardInvoice(){
    $(".search_div").show();

    tblid = 'searchtable';
    fixedHead(tblid);

    $("#tb_search_data").html('');
    $("#search_invoice_no").val('');
    $(".search_table").hide();
}
function changeDatalist(pkid,proid){

    //alert('change');

    //alert(pkid);

    itemq = $("#quantity"+pkid+"_"+proid).val();

    itemprice = $("#itemperprice"+pkid+"_"+proid).val();

    totalprice = parseFloat(itemq*itemprice);

    totalprice = totalprice.toFixed(3);


    totalp = totalprice;

    $("#p"+pkid+"_"+proid).html(totalp);

    $("#itemtotalprice"+pkid+"_"+proid).val(totalprice);

    calculatetotalval();
    assignDiscount();
}

totalval = 0;

function calculatetotalval(){

    totalval = 0;

    totalnumitems = $('.rows').length;

    $('.p').each(function(i, obj) {

        //test

        //alert(i);

        //alert($('.p').eq(i).val());

        //alert(obj.value);

        //ob = obj;

        totalval = parseFloat(totalval)+parseFloat(obj.value);





    });

    $("#totalitems").html(totalnumitems);


    $("#totalval").html(totalval.toFixed(3));
    //alert('asdas');
    $("#totalvalup").html(totalval.toFixed(3));

}
function swapmeu() {



    pid = $("#customer_id").val();

    pname = $("#customer_name").val();

    //alert(pname);

    $("#customer_name").val(pid);

    $("#customer_id").val(pname);

    $("#invoice_paymemt_type").val('customer');
}

function fixedHead(tblid){
    var tableOffset = $("#"+tblid).offset().top;
    var $header = $("#"+tblid+" > thead").clone();
    var $fixedHeader = $("#header-fixed").append($header);

    $(window).bind("scroll", function() {
        var offset = $(this).scrollTop();

        if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
            $fixedHeader.show();
        }
        else if (offset < tableOffset) {
            $fixedHeader.hide();
        }
    });
}
$(document).ready(function () {

	  audioElement = document.createElement('audio');
       //alert(audioElement);
        audioElement.setAttribute('src', '<?php echo base_url() ?>durarthem/sounds-937-job-done.mp3');

        audioElement.addEventListener('ended', function() {
           // this.currentTime = 0;
           // this.play();
        }, false);

        $('#play').click(function() {
            audioElement.play();
        });

        $('#pause').click(function() {
            audioElement.pause();
        });

    $(".trigger").click(function(){
        $(".ppanel").toggle("fast");
        $(this).toggleClass("active");
        return false;
    });



    tblid = 'hold_table';
    fixedHead(tblid);
    //alert('ready');
    //  $("#idCalculadora").Calculadora();
    //$("#micalc").Calculadora({'EtiquetaBorrar':'Clear'});

    setInterval('getReadyOrdersCount()',2000);
    var carousel = $("#owl-demo");

    carousel.owlCarousel({

        navigation:true,

        navigationText: [

            "<i class='icon-chevron-left icon-white'><</i>",

            "<i class='icon-chevron-right icon-white'>></i>"

        ],

    });

    var ac_config = {

        source: "<?php echo base_url(); ?>ajax/getAutoSearchCustomer",

        select: function (event, ui) {

            $("#customer_name").val(ui.item.cus);

            //$("#customername2").val(ui.item.cus);

            $("#customer_id").val(ui.item.cus);
            //$("#").html(ui.item.cus);
            console.log(ui);

            //	uu = ui;

            //swapme();

            //setTimeout('swapme()', 500);

            setTimeout('swapmeu()', 500);

        },

        minLength: 1

    };



   // $("#customer_name").autocomplete(ac_config);





    $("#item_name").autocomplete({



        source: function (request, response) {



            $.getJSON("<?php echo base_url(); ?>ajax/getAutoSearchProductsPos", {category: '', term: $('#item_name').val(), storeid:''},



                response);



        },
        autoFocus: true,
        minLength: 0,
        response: function( event, ui ) {
            itemn = $("#item_name").val();
            utest = ui;
            if(utest.content[0].brnumber  == itemn){
                // ut.item  = utest.content[0];
                // swapme2p();
               // swapmematch(utest.content[0]);
            }
        },
        select: function (event, ui) {

            $("#item_name").val(ui.item.id);

            $("#item_id").val(ui.item.prod);

            console.log(ui);

            utitem = ui.item;
            ut  = ui;

            //swapme();



            setTimeout('showPrices()', 500);

            //$("#q").val(1);
           // $(':focus').blur();
            //$("#item_name").focus();

        }



    });


    $("#total_paid").keyup(function(){

        paid = $(this).val();
        ret = paid - finish;
        $("#totalvalup").html(paid);
        $("#total_returned").html(ret.toFixed(3));
        $("#total_returnedup").html(ret.toFixed(3));
    });

});


function triggetPlus(rand,itemid){
   // alert('click');
    //$(".ppanel").toggle("fast");
    //$(this).toggleClass("active");
    //return false;
    $("#item_additem").val(itemid);
    $("#rand_additem").val(rand);
    $("#item_adddetail_click").trigger('click');
    ///$('.close').trigger('click');
}

</script>
<script src="<?php echo base_url(); ?>js/SimpleCalculadorajQuery.js"></script>

<script type="text/javascript">
    //$("#idCalculadora").Calculadora();
    $("#micalc").Calculadora({'EtiquetaBorrar':'Clear'});
</script>
<script>

    var oriVal;

    $("#eVal1").on('dblclick', 'span', function () {

        oriVal = $(this).text();

        $(this).text("");

        $("<input type='text' class='thVal'>").appendTo(this).focus();

    });

    $("#eVal1").on('focusout', 'span > input', function () {

        var $this = $(this);

        $this.parent().text($this.val() || oriVal);

        $this.remove(); // Don't just hide, remove the element.

    });



    var oriVal;

    $("#eVal2").on('dblclick', 'span', function () {

        oriVal = $(this).text();

        $(this).text("");

        $("<input type='text' class='thVal'>").appendTo(this).focus();

    });

    $("#eVal2").on('focusout', 'span > input', function () {

        var $this = $(this);

        $this.parent().text($this.val() || oriVal);

        $this.remove(); // Don't just hide, remove the element.

    });



    var oriVal;

    $("#eVal3").on('dblclick', 'span', function () {

        oriVal = $(this).text();

        $(this).text("");

        $("<input type='text' class='thVal'>").appendTo(this).focus();

    });

    $("#eVal3").on('focusout', 'span > input', function () {

        var $this = $(this);

        $this.parent().text($this.val() || oriVal);

        $this.remove(); // Don't just hide, remove the element.

    });



    var oriVal;

    $("#eVal4").on('dblclick', 'span', function () {

        oriVal = $(this).text();

        $(this).text("");

        $("<input type='text' class='thVal'>").appendTo(this).focus();

    });

    $("#eVal4").on('focusout', 'span > input', function () {

        var $this = $(this);

        $this.parent().text($this.val() || oriVal);

        $this.remove(); // Don't just hide, remove the element.

    });

</script>
<script src="<?php echo base_url(); ?>pos_assets/js/jquery.cslide.js"></script>

<script>

    $(document).ready(function(){

        $("#cslide-slides").cslide();
        $.fn.focusTextToEnd = function(){
            this.focus();
            var $thisVal = this.val();
            this.val('').val($thisVal);
            return this;
        }

        var currentBoxNumber = 0;
        $(".tbox").keyup(function (event) {
            if (event.keyCode == 13) {
                textboxes = $(".tbox");
                currentBoxNumber = textboxes.index(this);
                console.log(textboxes.index(this));
                if(textboxes.index(this) == 0){
                    itmval  = $("#item_name").val();
                    if(itmval == ''){
                        if (textboxes[currentBoxNumber + 1] != null) {
                            nextBox = textboxes[currentBoxNumber + 1];
                            nextBox.focus();
                            nextBox.select();
                            event.preventDefault();
                            return false;
                        }
                    }
                }
                else{
                    if (textboxes[currentBoxNumber + 1] != null) {
                        nextBox = textboxes[currentBoxNumber + 1];
                        nextBox.focus();
                        nextBox.select();
                        event.preventDefault();
                        return false;
                    }
                }
            }
        });

        $('#item_name').keydown(function (e){
            if(e.keyCode == 13){
                itemvl = $("#item_name").val();
                if(itemvl == ""){
                    //alert('you pressed enter ^_^');
                }

            }
        })

    });

</script>

</body>

</html>