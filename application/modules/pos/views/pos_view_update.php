<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>POS System</title>



    <!-- Bootstrap -->

    <link href="<?php echo base_url(); ?>pos_assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/loadercss.css" rel="stylesheet">
    <!-- Reset -->

    <link href="<?php echo base_url(); ?>pos_assets/css/normalize.css" rel="stylesheet">

    <!-- Custom -->

    <link href="<?php echo base_url(); ?>pos_assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>newcss/style.css" rel="stylesheet">

    <!-- Select -->

    <link href="<?php echo base_url(); ?>pos_assets/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- Font Awesome -->

    <link href="<?php echo base_url(); ?>pos_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Owl Carousel -->

    <link href="<?php echo base_url(); ?>pos_assets/css/owl.carousel.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>css/jqcode.css" rel="stylesheet">


    <link rel="stylesheet" href="<?php echo base_url(); ?>css/SimpleCalculadorajQuery.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <style>

        .categ_active{

            color: #1d465e !important;

            text-decoration: none;

        }
        .btn{
            font-size: 13px;
        }

        .header-fixed {
            position: fixed;
            top: 0px; display:none;
            background-color:white;
        }

        .rows{
            font-size: 10px;
        }


    </style>

</head>

<body>

<header>

    <div class="demo">
        <svg class="loader">
            <filter id="blur">
                <fegaussianblur in="SourceGraphic" stddeviation="2"></fegaussianblur>
            </filter>
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="#F4F519" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-2">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="#DE2FFF" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-3">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="#FF5932" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-4">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="#E97E42" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-5">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="white" stroke-width="6" stroke-linecap="round" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-6">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="#00DCA3" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-7">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="purple" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
        <svg class="loader loader-8">
            <circle cx="75" cy="75" r="60" fill="transparent" stroke="#AAEA33" stroke-width="6" stroke-linecap="round" stroke-dasharray="385" stroke-dashoffset="385" filter="url(#blur)"></circle>
        </svg>
    </div>

    <div class="container" style="padding-left:10px;">

        <div class="row">
            <div class="col-sm-12">
         <!--       <div class="col-sm-2 col-xs-12"><img src="<?php // echo base_url(); ?>pos_assets/images/logo.png" alt="Business Solutions" class="img-responsive bslogo"></div> -->
                <div class="col-sm-12 col-xs-12" style="padding:0px;text-align: center">
                    <ul class="list-inline" style="margin-top:.5em">
                    <!--    <li><a href="" class="btn btn-default" data-toggle="modal" data-target="#view_cal" ><span class="glyphicon glyphicon-plus"></span> Calculator</a></li> -->

                       <li><a id="confirm_click" href="javascript:void(0)" data-toggle="modal" data-target="#cancel_items" onclick="searchCancelInvoice()" class="btn btn-success"><span class="glyphicon glyphicon-list-alt" ></span> Confirm Order</a></li>

                    </ul>
                </div>

                <div class="form-group qpull-left">

    <form id="pos_form_data" >

     <!--   <div class="col-sm-6 npadleft"><label for="sel1">Reference No.</label></div>
        <div class="col-sm-6 npadleft"><label for="sel1">Quantity</label></div>

        <!--<select class="selectpicker" style="display: none;">

            <option>Cash</option>

            <option>Ali</option>

            <option>Bob</option>

          </select>

        <div class="col-sm-6 npadleft">
            <input type="text" class="form-control" id="customer_name" name="customer_name" placeholder="Customer Name / Cash"><input type="hidden" name="customer_id" id="customer_id" />
        </div>
        <div class="col-sm-6 npadleft">
            <input type="text" class="form-control" id="q" name="q" placeholder="Quantity" value="1">
        </div> -->

    </form>

</div>

                <div class="col-sm-2 col-xs-12 rttxtnums" style="display: none;"><div class="col-sm-3 col-xs-3 rtxt_head">Total</div><div  class="col-sm-3 col-xs-3 rtxt_nums" id="totalvalup">0.000</div> <br clear="all"> <div class="col-sm-3 col-xs-3 rtxt_head">Return</div><div class="col-sm-3 col-xs-3 rtxt_nums" id="total_returnedup">0.000</div></div>
            </div> <!--./col-sm-12-->
        </div>

    </div>



</header>


<!-- hold items modal start-->
<div class="modal fade" id="items_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="inv_details" style="display: none;"></div>
                            <div><a href="javascript:void(0)" onclick="getItemDetails()"><span>Invoice No</span><span id="list_inv_html"></span></a></div>
                            <input type="hidden" id="list_inv_no" name="list_inv_no">
                            <div id="no-more-tables">
                                <div class="scroll">
                                    <table class="col-md-12 table-bordered table-striped table-condensed cf">
                                        <thead class="cf">
                                        <tr>
                                            <th>Item Name.</th>
                                            <th>Item Picture</th>
                                            <th>Total Items</th>
                                            <th class="numeric">Price</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tb_viewdata">
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="view_cal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-8" style="left: 18%;">

                            <div id="no-more-tables">
                                <div id="idCalculadora"> </div>
                                <div id="micalc"> </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="card_number" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">

                            <div class="g4 form-group search_div">

                                <label class="text-warning">Card Number: <img src="<?php echo base_url(); ?>images/visa-large.gif"></label>

                                <input id="add_card_no" name="add_card_no" type="text" maxlength="8" class="form-control" style="width:35%; text-align: center; position: relative; left: 34%;"/>
                                <br clear="all">
                            </div>
                            <br clear="all">
                            <div class="col-xs-12 form-group search_table" style="margin-top: 20px;">
                                <a class="btn btn-success"  href="javascript:void(0)" onclick="payByCard()">Pay</a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="check_price" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12" id="largebtn">
                            <div class="col-md-12"><a style="width: 80%;" href="javascript:void(0)" class="btn btn-primary" onclick="swapme2p('Large')">Large Price <span id="large_sp">(12)</span></a></div>
                        </div>
                    </div>
                    <br clear="all">
                    <div class="row" id="mediumbtn">
                        <div class="col-xs-12">
                            <div class="col-md-12"><a style="width: 80%;" href="javascript:void(0)" class="btn btn-success" onclick="swapme2p('Medium')">Medium Price <span id="medium_sp">(12)</span></a></div>
                        </div>
                    </div>
                    <br clear="all">
                    <div class="row" id="smallbtn">
                        <div class="col-xs-12">
                            <div class="col-md-12"><a style="width: 80%;" href="javascript:void(0)" class="btn btn-warning" onclick="swapme2p('Small')">Small Price <span id="small_sp">(12)</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="cancel_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">

                            <div class="g4 form-group search_div">

                                <label class="text-warning">Order No رقم الطلبية</label>

                                <input id="search_invoice_no" name="search_invoice_no" type="text" class="form-control" style="width:35%; text-align: center; position: relative; left: 34%;"/>
                                <br clear="all">
                                <input type="button" value="Confirm" class="btn btn-success   green" onclick="searchInvoice()">
                            </div>


                            <div class="search_table" id="no-more-tables" style="display: none;">
                                <h1 id="search_inv_id"></h1>
                                <div class="scroll">
                                <table id="searchtable" class="col-md-12 table-bordered table-striped table-condensed cf">
                                    <thead class="cf">
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Item no</th>
                                        <th>Quantity</th>
                                        <th class="numeric">Price</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tb_search_data">
                                    </tbody>
                                </table>
                                    <table class="header-fixed"></table>
                                    </div>
                                </div>
                            <br clear="all">
                            <div class="col-xs-12 form-group search_table" style="display: none;margin-top: 20px;">
                                <a  class="btn btn-danger"  href="javascript:void(0)" onclick="confirmCancel()"> Cancel Invoice</a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="hold_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <h1 id="show_u_name" style="display: none;"><?php  echo $this->session->userdata('bs_username'); ?></h1>

                    <div class="row">
                        <div class="col-xs-12">
                            <div id="no-more-tables">
                                <div class="scroll">
                                <table id="hold_table" class="col-md-12 table-bordered table-striped table-condensed cf">
                                    <thead class="cf">
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Invoice No</th>
                                        <th>Total Items</th>
                                        <th class="numeric">Price</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tb_data">
                                    </tbody>
                                </table>
                                    <table class="header-fixed"></table>
                                </div>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="<?php echo base_url(); ?>pos_assets/js/jquery.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="<?php echo base_url(); ?>pos_assets/js/bootstrap.min.js"></script>

<!-- Select Picker -->

<script src="<?php echo base_url(); ?>pos_assets/js/bootstrap-select.js"></script>

<!-- Owl Carousel -->

<script src="<?php echo base_url(); ?>pos_assets/js/owl.carousel.min.js"></script>

<script src="<?php echo base_url(); ?>pos_assets/js/jqui.js"></script>
<script type="text/javascript">
function confirmCancel() {
    var r = confirm("Are you Sure you want to Cancel?");
    if (r == true) {
        sinv = $("#search_invoice_no").val();
        $.ajax({
            url: "<?php echo base_url(); ?>pos/cancel_pos_inv",
            type: 'post',
            data: {'inv':sinv},
            cache: false,
            success: function (data) {

              //  $("#tb_data").html(data);
                if(data){
                    alert('Invocie Canceled Successfully');
                    $('.close').trigger('click');

                }
            }
        });
    }
}

function payByCard(){

    cus_id = $("#customer_id").val();

    item_name = $("#item_name").val();

    inv_id =$("#list_inv_no").val();

    card_no =$("#add_card_no").val();
    //add_payment_date
    cusname = $("#customer_name").val();
    $("#invoice_paymemt_type").val('card');

    $("#pos_invoice_status").val('payment');
    //add_payment_date



    //pserializedata.push({name: 'customer_id', value: cus_id});

    //pserializedata.push({name: 'item_name', value: item_name});



    if(card_no !=""){
        pserializedata = $("#pos_itemform_data").serialize()+'&'+$.param({ 'customer_id': cus_id,'item_name': item_name,'inv_id': inv_id,'card_no': card_no});

        //var pserializedata = $("#pos_form_data").serialize();

        $.ajax({

            url: "<?php echo base_url(); ?>pos/save_newpost_data",

            type: 'post',



            data: pserializedata,



            cache: false,



            //dataType:"json",



            success: function (data) {

                console.log(data);
                if(data){

                    /*window.open(
                        '<?php echo base_url(); ?>sales_management/printed/'+data,
                        '_blank'
                    );

                    window.top.location = '<?php echo base_url(); ?>pos/add';
                    */


                }
            }



        });
    }
    else{
        alert('Please Enter Card Number الرجاء إدخال رقم الطلبية');
    }

}

function cancelinvoice(){
    window.top.location = '<?php echo base_url(); ?>pos/add';
}
function add_payment_date(inv_status){


    cus_id = $("#customer_id").val();

    item_name = $("#item_name").val();
   /// item_name = $("#item_name").val();
    inv_id =$("#list_inv_no").val();

    card_no =$("#add_card_no").val();

    //add_payment_date
    cusname = $("#customer_name").val();
    if(cusname !=""){
            $("#invoice_paymemt_type").val('customer');
    }
    else{
        $("#invoice_paymemt_type").val('cash');
    }

    $("#pos_invoice_status").val(inv_status);



    //pserializedata.push({name: 'customer_id', value: cus_id});

    //pserializedata.push({name: 'item_name', value: item_name});


    if(cusname !=""){
        pserializedata = $("#pos_itemform_data").serialize()+'&'+$.param({ 'customer_id': cus_id,'item_name': item_name,'inv_id': inv_id,card_no:card_no,customer_name:cusname});

        //var pserializedata = $("#pos_form_data").serialize();

        $.ajax({

            url: "<?php echo base_url(); ?>pos/save_newpost_data",

            type: 'post',



            data: pserializedata,



            cache: false,



            //dataType:"json",



            success: function (data) {

                console.log(data);
                if(data){
                    window.open(
                        '<?php echo base_url(); ?>sales_management/printed/'+data,
                        '_blank'
                    );


                    window.top.location = '<?php echo base_url(); ?>pos/add';


                }
            }



        });

    }
    else{
        alert('Please Enter RefrenceNumber');
    }

}

function getItemDetails(){

}
function viewInvoiceList(tp){

    if(tp == 'my'){
        $('#show_u_name').show();
    }
    else{
        $('#show_u_name').hide();
    }

    $("#tb_data").html('');
    $('#invoice_type').val(tp);
    $(".demo").show();
    $.ajax({
        url: "<?php echo base_url(); ?>pos/get_posinvoice_bytype",
        type: 'post',
        data: {'type':tp},
        cache: false,
        success: function (data) {
            $(".demo").hide();
            $("#tb_data").html(data);
        }
    });
}

function searchInvoice(){

    search_invoice_no = $("#search_invoice_no").val();
    $(".demo").show();
    sinv = 'Invoice #'+search_invoice_no;
    $("#search_inv_id").html(sinv);
    if(search_invoice_no !=""){
        $.ajax({
            url: "<?php echo base_url(); ?>pos/search_invoice_sales",
            type: 'post',
            data: {'sin':search_invoice_no},
            cache: false,
            success: function (data) {
                $(".demo").hide();
                if(data =='1'){
                    $("#search_invoice_no").val('');
                    alert('Order Confirmed Successfully تم تأكيد الطلبية بنجاح');
                }
                else if(data == '3'){
                    alert('Order number already Paid رقم الطلبية تم دفعها');
                }
                else if(data == '2'){
                    alert('Order number already Ready رقم الطلبية المدخل جاهزة مسبقا');
                }
                else{
                    alert('Order number does not exist رقم الطلبية المدخل غير موجود');
                }

            }
        });
    }
    else{
        alert('Please Enter Invoice Number الرجاء إدخال رقم الطلبية');
    }

}

function viewInvoiceItemListById(inv){
    $('.close').trigger('click');
    $("#item_detail_click").trigger('click');

    $("#list_inv_no").val(inv);
    $("#list_inv_html").html(inv);
    $(".demo").show();
    $.ajax({
        url: "<?php echo base_url(); ?>pos/get_posinvoice_byid",
        type: 'post',
        data: {'inv_id':inv},
        cache: false,
        success: function (data) {
            //alert(data);
            $("#tb_viewdata").html(data);
            $(".demo").hide();
        }
    });
}

function addproduct(id){

    //alert(id);

    pname = id;

    $("#item_id"+id).val();

    itemval = $("#sale_price"+id).val();

    pid = $("#item_name"+id).val();

    minitemval = $("#min_val"+id).val();

    //min_val
    //alert(itemval);

    randid = getRandomArbitrary();

    ht = '<input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'>';
    //alert(ht);
    //htm = '<tr id="row'+randid+'" class="rows">'+ht+'<td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+pid+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="1"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+' OMR <input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'></td></tr>';
    //	htm = '<tr id="row'+randid+'" class="rows"><td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+pid+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="1"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+' OMR</td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"></tr>';
    ///<input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'>

    htm = '<tr id="row'+randid+'" class="rows"><td style="width:5%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:54%;"><span class="ProName"><a href="#">'+pid+'</a></span></td><td style="width:15%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control form-control-sm" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="1"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+' OMR</td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'"></tr>';

    //$("#itemtotalprice"+randid+"_"+pname).val(itemval);

    $("#rtBox").prepend(htm);

    calculatetotalval(); assignDiscount();

}
function getProducts(obj,id){

    //alert('asd');

    //	oo = obj;

    $('.categ_c').removeClass('categ_active');

    $(obj).addClass('categ_active');


    $(".demo").show();
    $.ajax({



        url: "<?php echo base_url(); ?>ajax/getCategoryProducts",



        type: 'post',



        data: {'category_id':id},



        cache: false,

        success: function (data) {
            $(".cslide-slides-container").html(data);

            $("#cslide-slides").cslide();
            $(".demo").hide();
        }



    });



}


function checkBarcodeMatch(search_data){
    //item_name

}

tp = '';
function viewCategPrices(itemid){
    small_price = $("#small_val"+itemid).val();
    if(small_price !="0.000"){
        $("#small_sp").html(small_price);
        //$("#small_sp").html(utitem.large_price);

    }
    else{
        $("#smallbtn").hide();
    }

    medium_price = $("#medium_val"+itemid).val();

    if(medium_price !="0.000"){
        $("#medium_sp").html(medium_price);
    }else{
        $("#mediumbtn").hide();
    }
    large_price = $("#large_val"+itemid).val();
    if(large_price !="0.000"){
        $("#large_sp").html(large_price);
    }else{
        $("#largebtn").hide();
    }

    tp = 'category';
    itemGlobalval = itemid;
    $("#view_prices_btn").trigger('click');
}
function showPrices(){
//    testob  = obj;

    pid = $("#item_id").val();



    pname = $("#item_name").val();



    $("#item_name").val(pid);
//    $("#item_id").val(pname);

    if(utitem.small_price !="0.000"){
        $("#small_sp").html(utitem.small_price);
        //$("#small_sp").html(utitem.large_price);
        $("#smallbtn").show();
    }
    else{
        $("#smallbtn").hide();
    }

    if(utitem.large_price !="0.000"){
        $("#large_sp").html(utitem.large_price);
        $("#largebtn").show();
    }else{
        $("#largebtn").hide();
    }

    if(utitem.medium_price !="0.000"){
        $("#medium_sp").html(utitem.medium_price);
        $("#mediumbtn").show();
    }else{
        $("#mediumbtn").hide();
    }
    tp = '';
    $("#view_prices_btn").trigger('click');
}
function swapme2p(type) {



    if(tp){
        pname = itemGlobalval;

        $("#item_id"+itemGlobalval).val();

        itemval = $("#sale_price"+itemGlobalval).val();

        if(type  == 'Large'){
            itemval = $("#large_val"+itemGlobalval).val();
        }
        else if(type  == 'Medium'){
            itemval = $("#medium_val"+itemGlobalval).val();
        }
        else{
            itemval = $("#small_val"+itemGlobalval).val();
        }

        pid = $("#item_name"+itemGlobalval).val();
        par =   $("#item_name_ar"+itemGlobalval).val();
        minitemval = $("#min_val"+itemGlobalval).val();

        //min_val
        //alert(itemval);

        randid = getRandomArbitrary();

        ht = '<input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'>';

        htm = '<tr id="row'+randid+'" class="rows"><td style="width:5%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:54%;"><a href="#"><span class="pname">'+pid+'</span><span class="pname">'+par+' </span></a></td><td style="width:15%;"><div id="eVal1" ><input type="number" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control form-control-sm" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="1"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+'</td><td>'+type+'</td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'"></tr>';

    } else{
        pid = $("#item_id").val();

        pname = $("#item_name").val();


        $("#item_name").val('');

        if(type  == 'Large'){
            itemval = ut.item.large_price;
        }
        else if(type  == 'Medium'){
            itemval = ut.item.medium_price;
        }
        else{
            itemval = ut.item.small_price;
        }
        minitemval = ut.item.min_sale_price;
        par = ut.item.prod_ar;
        pname = ut.item.id;

        q = $("#q").val();

        randid = getRandomArbitrary();

        itemval = itemval*q;
        itemval = itemval.toFixed(3);
        htm = '<tr id="row'+randid+'" class="rows"><td style="width:5%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#"><span class="pname">'+pid+'</span><span class="pname">'+par+' </span></a></span></td><td style="width:22%;"><div id="eVal1" ><input type="number" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="'+q+'"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+'</td><td>'+type+'</td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'"></tr>';

    }

    $('.close').trigger('click');


    $("#rtBox").prepend(htm);

    calculatetotalval(); assignDiscount();

    //<td style="width:22%;"><input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity" style="width:100%"></td>

}

function swapmematch(obj) {



    pid = obj.prod ;



    pname = obj.id;



    $("#item_name").val('');



    //$("#item_id").val(pname);

    //alert(pname);

    itemval = obj.sale_price;

    minitemval = obj.min_sale_price;

    q = $("#q").val();

    randid = getRandomArbitrary();

    itemval = itemval*q;
    itemval = itemval.toFixed(3);
    htm = '<tr id="row'+randid+'" class="rows"><td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+pname+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+pid+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+pname+')"  class="q'+randid+','+pname+' form-control" id="quantity'+randid+'_'+pname+'" name="quantity[]" placeholder="Quantity" value="'+q+'"  style="width:100%"></div></td><td id="p'+randid+'_'+pname+'">'+itemval+' OMR</td><input type="hidden" id="itemid'+randid+'_'+pname+'"  name="itemid[]" value="'+pname+'"><input type="hidden" id="itemperprice'+randid+'_'+pname+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+pname+'" name="minitemperprice[]" value="'+minitemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+pname+'" name="itemtotalprice[]" value="'+itemval+'"></tr>';



    //alert(randid);

    //alert(pname);

    //alert(itemval);



    //$("#itemtotalprice"+randid+"_"+pname).val(itemval);

    $("#rtBox").prepend(htm);

    calculatetotalval(); assignDiscount();

    $("#item_name").val('');
    $("#q").val(1);
    //$(':focus').blur();
    $('#item_name').focusTextToEnd();
    //<td style="width:22%;"><input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity" style="width:100%"></td>

}


function getRandomArbitrary() {

    num =Math.floor(Math.random()*10000);

    return  num;

}

function assignInvoiceItemListById(inv){
    $("#rtBox").html('');
    $('#tb_search_data').html('');
    $('.close').trigger('click');
    $("#list_inv_no").val(inv);
    $("#list_inv_html").html(inv);
    $(".demo").show();
    $.ajax({
        url: "<?php echo base_url(); ?>pos/get_posinvoice_byid",
        type: 'post',
        data: {'inv_id':inv},
        cache: false,
        success: function (data) {
            //alert(data);
            $("#tb_viewdata").html(data);
            additemList();

            $(".demo").hide();
        }
    });
}

function additemList(){
    $(".itemname").each(function (index,value) {
        //console.log(index + ": " + value);
        console.log(index + ": " + $(this).text());
        itemname = $(this).text();
        itemprice = $(".itemprice").eq(index).html();
        console.log(itemprice);

        itemid = $(".itemid").eq(index).val();
        console.log(itemid);

        itemq = $(".itemquantity").eq(index).html();
        console.log(itemq);

        randid = getRandomArbitrary();

        itemval = Number(itemprice)*itemq;
        itemval = itemval.toFixed(3);
        htm = '<tr id="row'+randid+'" class="rows"><td style="width:15%;"><a href="javascript:void(0)" onclick="removefromlist('+randid+','+itemid+')"><i class="fa fa-close"></i></a></td><td style="width:37%;"><span class="ProName"><a href="#">'+itemname+'</a></span></td><td style="width:22%;"><div id="eVal1" ><input type="text" onkeyup="changeDatalist('+randid+','+itemid+')"  class="q'+randid+','+itemid+' form-control" id="quantity'+randid+'_'+itemid+'" name="quantity[]" placeholder="Quantity" value="'+itemq+'"  style="width:100%"></div></td><td id="p'+randid+'_'+itemid+'">'+itemval+' OMR</td><input type="hidden" id="itemid'+randid+'_'+itemid+'"  name="itemid[]" value="'+itemid+'"><input type="hidden" id="itemperprice'+randid+'_'+itemid+'" name="itemperprice[]" value="'+itemval+'"><input type="hidden" id="minitemperprice'+randid+'_'+itemid+'" name="minitemperprice[]" value="'+itemval+'"><input type="hidden" class="p" id="itemtotalprice'+randid+'_'+itemid+'" name="itemtotalprice[]" value="'+itemval+'"></tr>';

        $("#rtBox").prepend(htm);

        calculatetotalval(); assignDiscount();

        //alert(index);
        //itemname = $('.item_name').eq(index).html();
        //alert(itemname);
    });

}
function assignDiscount(){

    disc = $("#discount_val").val();

    disctype = $("#discount:checked").val();

    //alert(disc);

    if(disc && disctype){



        if(disctype == 'omr'){

            //alert('iff');

            //final = Number(totalval) * Number(disc);

            finish = totalval- disc;

        }

        else{

            //alert('else');

            final = Number(totalval) * Number(disc);

            final = final / 100;

            finish = totalval - final;



        }



        $('#total_pay_val').html(finish.toFixed(3));

    }

    ;	}

function removefromlist(rid,pkid){

    //alert('remove');

    var r = confirm("Are you Sure You Want to Delete");

    if (r == true) {

        //txt = "You pressed OK!";

        $("#row"+rid).remove();

        calculatetotalval(); assignDiscount();



    } else {

        txt = "You pressed Cancel!";

    }

}

function searchCancelInvoice(){
    $(".search_div").show();

    tblid = 'searchtable';
    fixedHead(tblid);

    $("#tb_search_data").html('');
    $("#search_invoice_no").val('');
    $(".search_table").hide();
}

function payByCardInvoice(){
    $(".search_div").show();

    tblid = 'searchtable';
    fixedHead(tblid);

    $("#tb_search_data").html('');
    $("#search_invoice_no").val('');
    $(".search_table").hide();
}
function changeDatalist(pkid,proid){

    //alert('change');

    //alert(pkid);

    itemq = $("#quantity"+pkid+"_"+proid).val();

    itemprice = $("#itemperprice"+pkid+"_"+proid).val();

    totalprice = parseFloat(itemq*itemprice);

    totalprice = totalprice.toFixed(3);


    totalp = totalprice;

    $("#p"+pkid+"_"+proid).html(totalp);

    $("#itemtotalprice"+pkid+"_"+proid).val(totalprice);

    calculatetotalval();
    assignDiscount();
}

totalval = 0;

function calculatetotalval(){

    totalval = 0;

    totalnumitems = $('.rows').length;

    $('.p').each(function(i, obj) {

        //test

        //alert(i);

        //alert($('.p').eq(i).val());

        //alert(obj.value);

        //ob = obj;

        totalval = parseFloat(totalval)+parseFloat(obj.value);





    });

    $("#totalitems").html(totalnumitems);


    $("#totalval").html(totalval.toFixed(3));
    //alert('asdas');
    $("#totalvalup").html(totalval.toFixed(3));

}
function swapmeu() {



    pid = $("#customer_id").val();

    pname = $("#customer_name").val();

    //alert(pname);

    $("#customer_name").val(pid);

    $("#customer_id").val(pname);

    $("#invoice_paymemt_type").val('customer');
}

function fixedHead(tblid){
    var tableOffset = $("#"+tblid).offset().top;
    var $header = $("#"+tblid+" > thead").clone();
    var $fixedHeader = $("#header-fixed").append($header);

    $(window).bind("scroll", function() {
        var offset = $(this).scrollTop();

        if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
            $fixedHeader.show();
        }
        else if (offset < tableOffset) {
            $fixedHeader.hide();
        }
    });
}
$(document).ready(function () {

    $("#confirm_click").trigger('click');
    tblid = 'hold_table';
    fixedHead(tblid);
    //alert('ready');
    //  $("#idCalculadora").Calculadora();
    //$("#micalc").Calculadora({'EtiquetaBorrar':'Clear'});

    var carousel = $("#owl-demo");

    carousel.owlCarousel({

        navigation:true,

        navigationText: [

            "<i class='icon-chevron-left icon-white'><</i>",

            "<i class='icon-chevron-right icon-white'>></i>"

        ],

    });

    var ac_config = {

        source: "<?php echo base_url(); ?>ajax/getAutoSearchCustomer",

        select: function (event, ui) {

            $("#customer_name").val(ui.item.cus);

            //$("#customername2").val(ui.item.cus);

            $("#customer_id").val(ui.item.cus);
            //$("#").html(ui.item.cus);
            console.log(ui);

            //	uu = ui;

            //swapme();

            //setTimeout('swapme()', 500);

            setTimeout('swapmeu()', 500);

        },

        minLength: 1

    };



    $("#customer_name").autocomplete(ac_config);





    $("#item_name").autocomplete({



        source: function (request, response) {



            $.getJSON("<?php echo base_url(); ?>ajax/getAutoSearchProductsPos", {category: '', term: $('#item_name').val(), storeid:''},



                response);



        },
        autoFocus: true,
        minLength: 0,
        response: function( event, ui ) {
            itemn = $("#item_name").val();
            utest = ui;
            if(utest.content[0].brnumber  == itemn){
                // ut.item  = utest.content[0];
                // swapme2p();
               // swapmematch(utest.content[0]);
            }
        },
        select: function (event, ui) {

            $("#item_name").val(ui.item.id);

            $("#item_id").val(ui.item.prod);

            console.log(ui);

            utitem = ui.item;
            ut  = ui;

            //swapme();



            setTimeout('showPrices()', 500);

            //$("#q").val(1);
           // $(':focus').blur();
            //$("#item_name").focus();

        }



    });


    $("#total_paid").keyup(function(){

        paid = $(this).val();
        ret = paid - finish;
        $("#totalvalup").html(paid);
        $("#total_returned").html(ret.toFixed(3));
        $("#total_returnedup").html(ret.toFixed(3));
    });

});

</script>
<script src="<?php echo base_url(); ?>js/SimpleCalculadorajQuery.js"></script>

<script type="text/javascript">
    //$("#idCalculadora").Calculadora();
    $("#micalc").Calculadora({'EtiquetaBorrar':'Clear'});
</script>
<script>

    var oriVal;

    $("#eVal1").on('dblclick', 'span', function () {

        oriVal = $(this).text();

        $(this).text("");

        $("<input type='text' class='thVal'>").appendTo(this).focus();

    });

    $("#eVal1").on('focusout', 'span > input', function () {

        var $this = $(this);

        $this.parent().text($this.val() || oriVal);

        $this.remove(); // Don't just hide, remove the element.

    });



    var oriVal;

    $("#eVal2").on('dblclick', 'span', function () {

        oriVal = $(this).text();

        $(this).text("");

        $("<input type='text' class='thVal'>").appendTo(this).focus();

    });

    $("#eVal2").on('focusout', 'span > input', function () {

        var $this = $(this);

        $this.parent().text($this.val() || oriVal);

        $this.remove(); // Don't just hide, remove the element.

    });



    var oriVal;

    $("#eVal3").on('dblclick', 'span', function () {

        oriVal = $(this).text();

        $(this).text("");

        $("<input type='text' class='thVal'>").appendTo(this).focus();

    });

    $("#eVal3").on('focusout', 'span > input', function () {

        var $this = $(this);

        $this.parent().text($this.val() || oriVal);

        $this.remove(); // Don't just hide, remove the element.

    });



    var oriVal;

    $("#eVal4").on('dblclick', 'span', function () {

        oriVal = $(this).text();

        $(this).text("");

        $("<input type='text' class='thVal'>").appendTo(this).focus();

    });

    $("#eVal4").on('focusout', 'span > input', function () {

        var $this = $(this);

        $this.parent().text($this.val() || oriVal);

        $this.remove(); // Don't just hide, remove the element.

    });

</script>
<script src="<?php echo base_url(); ?>pos_assets/js/jquery.cslide.js"></script>

<script>

    $(document).ready(function(){

        $("#cslide-slides").cslide();
        $.fn.focusTextToEnd = function(){
            this.focus();
            var $thisVal = this.val();
            this.val('').val($thisVal);
            return this;
        }

        var currentBoxNumber = 0;
        $(".tbox").keyup(function (event) {
            if (event.keyCode == 13) {
                textboxes = $(".tbox");
                currentBoxNumber = textboxes.index(this);
                console.log(textboxes.index(this));
                if(textboxes.index(this) == 0){
                    itmval  = $("#item_name").val();
                    if(itmval == ''){
                        if (textboxes[currentBoxNumber + 1] != null) {
                            nextBox = textboxes[currentBoxNumber + 1];
                            nextBox.focus();
                            nextBox.select();
                            event.preventDefault();
                            return false;
                        }
                    }
                }
                else{
                    if (textboxes[currentBoxNumber + 1] != null) {
                        nextBox = textboxes[currentBoxNumber + 1];
                        nextBox.focus();
                        nextBox.select();
                        event.preventDefault();
                        return false;
                    }
                }
            }
        });

        $('#search_invoice_no').keydown(function (e){
            if(e.keyCode == 13){
                itemvl = $("#item_name").val();
                if(itemvl == ""){
                    alert('ENter Invoice No');
                }
                else{
                    searchInvoice();
                }

            }
        })

    });

</script>

</body>

</html>