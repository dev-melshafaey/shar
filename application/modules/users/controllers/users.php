<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Users extends CI_Controller {

    /*

     * Properties

     */



    private $_data = array();



//----------------------------------------------------------------------

    /*

     * Constructor

     */

    public function __construct() {

        parent::__construct();



        //load Home Model

        $this->load->model('users_model', 'users');

        $this->lang->load('main', get_set_value('site_lang'));

        $this->_data['udata'] = userinfo_permission();

    }



//----------------------------------------------------------------------



    /*

     * Home Page

     */

    public function index() {



        //$this->_data['inside'] = 'users';

        //$this->load->view('common/main', $this->_data);

        

        $this->_data['inside2'] = 'users';

        $this->_data['inside'] = 'addusers';

        $this->load->view('common/tabs', $this->_data);

    }


    
    function users_orders(){
        $this->_data['usersorders'] =  $this->users->getUsersOrders();
        $this->load->view('users_order_list', $this->_data);
    }

    function payment_history($id){
        $this->_data['usersorders'] =  $this->users->getEmpPaymentHistory($id);
		$this->_data['user_detail'] =  $this->users->get_user_detail($id);
        $this->_data['inside'] = 'users_payment_list';
        $this->load->view('common/main', $this->_data);

    }

    public function voucher($id) {
        // Load Home View
        $this->_data['id'] = $id;
        // $this->_data['companydetails'] = $this->customers->getCompanyDetails();

        $this->_data['invoicedetails'] = $this->users->getPaymentVoucher($id);

        //echo "<pre>";
        //print_r($this->_data['invoicedetails']);
        //  exit;
        //$this->_data['inside'] = 'statics';
        $this->load->view('voucher_view', $this->_data);
    }


    public function update_add_permission() {



        //echo $_POST['val'];

        $modulesid = get_modules_add_id($_POST['val']);
        //echo "<pre>";
        //print_r($modulesid);




        $this->_data['perdata']=$modulesid;

        $this->_data['gid']=$_POST['val'];



        //$this->_data['inside'] = 'permissions';

        //$this->load->view('common/main', $this->_data);

        echo $this->load->view('update_additionl', $this->_data);

    }

    function additionalpermissions(){
        $this->_data['inside'] = 'additionalpermissions';

        $this->load->view('common/main', $this->_data);
    }

    function user_graphs($type='',$date=''){

        $data = array();

        //$user_graph =$this->users->getGraphData();


        if($type !=""){
            $user_graph = $this->users->getSearchGraphData($type,$date);

        }
        else{
            $type = 'Today';
            $date = date('Y-m-d');
            $user_graph =$this->users->getGraphData();
        }

        foreach($user_graph as $key => $itemg) {
            $data[] = (object)array('name'=>_s($itemg->itemname,get_set_value('site_lang')), 'y'=>(int)$itemg->totalsales);
        }

        // echo "<pre>";
        //print_r($data);
        //exit;
        //echo json_encode($this->_data['user_graph']);
        //echo $json;
        //  $json = $this->getGraphData();
        //echo $json = json_encode($json);
        $this->_data['user_graph'] = $data;
        $this->_data['type'] = $type;
        $this->_data['date'] = $date;
        //$this->_data['user_gdata'] = $json;
        $this->load->view('users_items_graphs', $this->_data);
    }

    function getItemsSearchData(){
        $this->users->getGraphData();
    }
    function search_data_graph(){
        $postData = $this->input->post();
        //echo "<pre>";
        //print_r($postData);
        $data = array();
        if($postData['search_month'] !=""){
            $user_graph = $this->users->getSearchGraphData('month',$postData['search_month']);
        }
        else{
            $user_graph = $this->users->getSearchGraphData('range','');
        }

        //     echo "<pre>";
        //       print_r($user_graph);

        foreach($user_graph as $key => $itemg) {
            $data[] = (object)array('name'=>_s($itemg->itemname,get_set_value('site_lang')), 'y'=>(int)$itemg->totalsales);
        }
        echo json_encode($data);
    }
    function getGraphData(){

        $user_graph =$this->users->getGraphData();
        // echo "<pre>";
        //print_r($this->_data['user_graph']);
        $json = '[';
        //$json = '';
        foreach($user_graph as $key => $itemg) {
            $json .= '{name: "' ._s($itemg->itemname,get_set_value('site_lang')).'",y:'.$itemg->totalsales.'}';

            if ($itemg !== end($user_graph)) {
                $json .= ',';
            }
        }

        $json .= ']';
        //json_decode($json, true);
        echo $json;
        $manage = (array) json_decode($json);
        //echo "<pre>";
        //print_r($manage);
        //header('Content-Type: application/json');
        return $manage;

    }
    public function permissions() {



        $this->_data['inside'] = 'permissions';

        $this->load->view('common/main', $this->_data);

        //$this->load->view('', $this->_data);

    }



    public function update_permission() {

        

        //echo $_POST['val'];

        $modulesid = get_modules_id($_POST['val']);

        

        

        $this->_data['perdata']=$modulesid;

        $this->_data['gid']=$_POST['val'];

        

        //$this->_data['inside'] = 'permissions';

        //$this->load->view('common/main', $this->_data);

        echo $this->load->view('update_permission', $this->_data);

    }



    public function all_users() {

        //$this->load->view('users',$this->_data);

        $this->index();

    }



    public function addnewuser($userid) {

        if ($userid != '') {

            $this->_data['user'] = $this->users->get_user_detail($userid);

        }

        //$this->load->view('addusers', $this->_data);

        $this->_data['inside'] = 'addusers';

        $this->load->view('common/main', $this->_data);

    }




    function saveaddpermission(){

        //echo '<pre>'; print_r($this->input->post());

        //exit;

        $postData = $this->input->post();
        // print_r($postData);
        if(isset($postData['p']) && !empty($postData['p'])){
            $p = $postData['p'];

            $result = $this->users->getPermissionByutypeid($postData['permission_type_id']);
            if($result) {

                $id = $postData['permission_type_id'];
                $this->db->where_in('user_type_id',$id);
                $this->db->delete('additional_permissions');
            }

            foreach($p as $iparent=>$perm){
                //echo $iparent;
                $pmData['add_mod_id'] = $perm;
                $pmData['user_type_id'] = $postData['permission_type_id'];
                $this->db->insert('additional_permissions',$pmData);


            }

            $result = $this->users->getPermissionByutypeid($postData['permission_type_id']);
            //echo "<pre>";
            //print_r($result);
            //exit;
            $this->session->set_userdata('bs_addperms',$result);

        }
        else{
            $id = $postData['permission_type_id'];
            $this->db->where_in('user_type_id',$id);
            $this->db->delete('additional_permissions');
            $this->session->set_userdata('bs_addperms',false);
        }


        // exit();
        //$this->users->save_permission();

        do_redirect('additionalpermissions?e=10');
    }



    public function savepermission() {

        //echo '<pre>'; print_r($this->input->post());

        //exit();

        $this->users->save_permission();

        do_redirect('permissions?e=10');

    }



    public function get_permission() {

        $this->users->get_permission();

    }



    public function bulk_delete() {

        $this->users->delete_users();

    }



    public function add_new_user() {

        $this->users->add_new_user();

    }



    public function logout() {

        //$this->session->sess_destroy();



        redirect(base_url() . 'login');

    }



//----------------------------------------------------------------------

}



?>