<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<html lang="ar">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Email Robots</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>emailcss/css/style.css">

    <style>
        /* CSS doc by Asif */






        /*================ Fonts ==================*/
        @font-face {
            font-family: 'helvatica';
            src:url('arfonts/helvatica.ttf') format('truetype');
            font-weight: 300;
            font-style: normal;
        }

        @font-face {
            font-family: 'helvatica';
            src:url('arfonts/helvatica.ttf') format('truetype');
            font-weight: 400;
            font-style: normal;

        }

        @font-face {
            font-family: 'helvatica';
            src:url('arfonts/helvatice.ttf') format('truetype');
            font-weight: 700;
            font-style: normal;

        }

        /*================ General ==================*/

        body {background: #dfdfdf;margin: 0; padding: 0;font-family: helvatica; font-size: 14px;}

        h1 {font-size: 24px; text-indent: 1em; margin: 0; padding:0;}
        p {font-size: 14px;}
        table {width: 100%;}
        table tr td { margin: 0; padding:0; }

        /*================ Specific ==================*/
        .gaptd {padding: .5em 1em; width: 33%; text-align: center;}
        .ar_txt {text-align: center;}
        .heading {background: none; color: #fff; padding: 5px; border: 1px solid #000;}

        /* Multi Colors */
        .greenclr {background: #fff; color: #000;}
        .greenltclr {background: #fff; color: #000;}
        .redclr {background: #fff; color: #000;}
        .greyclr {background: #fff; color: #000;}

        .greyborder {border-bottom: 1px solid #000;}

        .wrapper {width: 100%; background: #fff; margin: 0 auto; padding: 0;}
        .logo {margin: .5em auto; display: table;}

        .bslogo {width: 120px; margin-top: 1em; float: left; padding-left: 3em;}
        .footer {text-align: center;}

    </style>
</head>
<body style="background: #dfdfdf;margin: 0; padding: 0;font-family: helvatica; font-size: 14px;">
<div class="wrapper" background: #fff; margin: 0 auto; padding: 0;">
<table style="width: 100%;" class="beforeprint">
<tbody>
<tr>
    <td><img src="<?php echo base_url() ?>durarthem/rtl/img/logo-loginprint.png" class="logo" style="margin: .5em auto; display: table; width:40%;  "></td>
</tr>
<tr>
    <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center; color: #000; padding: 5px;">RECEIPT VOUCHER  ايصال استلام</h1></td>

</tr>
<tr>
    <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;color: #000; padding: 5px;"><?php echo $invoicedetails->fullname; ?></h1><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center; color: #000; padding: 5px;"><?php echo date('d/m/Y h:i:s'); ?> </h1></td>

</tr>
<tr>
    <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;"> Received Amount - النقود المستلمة </h1></td>
</tr>
<tr>
    <td>
        <div class="greyclr" style="color: #000;">
            <table>
                <tr>
                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><strong>Total Payment</strong></td>
                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><strong><?php echo getAmountFormat($invoicedetails->clear_amount); ?> (OMR)</strong></td>
                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><strong>لمبلغ الإجمالي</strong></td>
                </tr>
            </table>
        </div>
    </td>
</tr>
<tr style="visibility: hidden">
    <td></td>
</tr>
<tr >
    <td>
        <div class="greyclr" style="color: #000;">
            <table>
                <tr>
                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><strong>Manager</strong></td>
                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"></td>
                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><strong>Received By</strong></td>
                </tr>
            </table>
        </div>
    </td>
</tr>
<tr >
    <td>
        <div class="greyclr" style="color: #000;">
            <table>
                <tr>
                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><strong></strong></td>
                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"></td>
                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><strong></strong></td>
                </tr>
            </table>
        </div>
    </td>
</tr>


</tbody>
</table>

<script type="text/javascript">
    url = '<?php echo base_url(); ?>';
    window.print();

    setTimeout(redirectPage,'2000');

    function redirectPage(){
        window.top.location = url;
    }
</script>
</div>
</body>
</html>
