


<div id="main-content" class="main_content">

<!--    <div class="title title alert alert-info">

        <span><?php breadcramb();   ?> </span>

    </div>-->

    <div class="notion title title alert alert-info"> * <?php echo lang('mess1') ?> </div>

    

    <?php error_hander($this->input->get('e')); ?>

    <form action="<?php echo base_url(); ?>users/add_new_user" method="post" id="frm_users" name="frm_users" autocomplete="off">

        <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />

        <input type="hidden" name="userid" id="userid" value="<?php echo $user->userid; ?>" />

        <div class="form mycontent">

            <div class="g4" style="display: none;">

                <label class=""><?php echo lang('Company-Name')?></label>

                <div class="">

                    <div class="ui-select" >

                        <div class="">

                            <?php company_dropbox('companyid', $user->companyid); ?>

                            <span class="arrow arrowselectbox">&amp;</span>

                        </div>

                    </div>

                </div>

            </div>

            <div class="g4" style="display: none;">

                <label class=""><?php echo lang('Branch-Name')?></label>

                <div class="">

                    <div class="ui-select" >

                        <div class="">

                            <?php company_branch_dropbox('branchid', $user->branchid, $user->companyid); ?>

                            <span class="arrow arrowselectbox">&amp;</span>

                        </div>

                    </div>

                </div>

            </div>



            <div class="g4">

                <label class=""><?php echo lang('Full-Name')?></label>

                <div class="">

                    <input name="fullname" id="fullname" value="<?php echo $user->fullname; ?>" type="text"  class="form-control"/>

                </div>

            </div>

            

            <div class="g4">

                <label class=""><?php echo lang('User')?></label>

                <div class="">

                    <input name="username" id="username" value="<?php echo $user->username; ?>" type="text"  class="form-control"/>

                </div>

            </div>

            <br clear="all"/>

            <div class="g4">

                <label class=""><?php echo lang('Password')?></label>

                <div class="">

                    <input name="upassword" id="upassword" value="" type="password"  class="form-control"/>

                </div>

            </div>

            

            <div class="g4">

                <label class=""><?php echo lang('RePassword')?></label>

                <div class="">

                    <input name="confpassword" id="confpassword" value="" type="password"  class="form-control"/>

                </div>

            </div>

     


            <div class="g4">

                <label class=""><?php echo lang('Mobile-Number')?></label>

                <div class="">

                    <input name="phone_number" id="phone_number" value="<?php echo $user->phone_number; ?>" type="text"  class="form-control"/>

                </div>

            </div>

            <div class="g4">

                <label class=""><?php echo lang('Office-Number')?></label>

                <div class="">

                    <input name="office_number" id="office_number" value="<?php echo $user->office_number; ?>" type="text"  class="form-control"/>

                </div>

            </div>

            <br clear="all"/>

            <div class="g4">

                <label class=""><?php echo lang('Fax-Number')?></label>

                <div class="">

                    <input name="fax_number" id="fax_number" value="<?php echo $user->fax_number; ?>" type="text"  class="form-control"/>

                </div>

            </div>

            <div class="g4">

                <label class=""><?php echo lang('Type')?></label>

                <div class="">

                    <div class="ui-select" >

                        <div class="">

                            <?php global_permission_dropbox('member_type', $user->member_type); ?>

                            <span class="arrow arrowselectbox">&amp;</span>

                        </div>

                    </div>

                </div>

            </div>

            <div class="g4">

                <label class=""><?php echo lang('Status')?></label>

                <div class="">

                    <div class="ui-select" >

                        <div class="">

                            <?php get_statusdropdown($user->status, 'status'); ?>

                            

                        </div>

                    </div>

                </div>

            </div>

            

            <div class="g4">

                <label class=""><?php echo lang('Notes')?></label>

                <div class="">

                    <textarea name="notes" id="notes" cols="" rows="" class="form-control"><?php echo $user->notes; ?></textarea>

                </div>

            </div>

            <br clear="all"/>

            <div class="raw field-box" align="">

                <input name="sub_mit" id="sub_mit" type="submit" class="submit_btn green flt-r g3" value="<?php echo lang('Add')?>" />

                <input name="sub_reset" type="reset" class="reset_btn btn-glow primary submit_btn gray flt-r g3" value="<?php echo lang('Reset')?>" />

            </div>

            <!--end of raw field-box--> 

        </div>

    </form>

</div>

