<div class="panel-body" style="padding:0">



    <div class="qrow qcol-sm-12 qcol-md-offset-2 custyle">
        <div class="table-responsive">
            <table id="new_data_table" class="table table-striped custab">
                <thead>

                <tr>
                    <th></th>
                    <th><label style="padding-right:0; padding-left:0;" class="control-label"> Username </label>
                        <input type="text" class="form-control" placeholder="Username" /></th>
                    <th><label style="padding-right:0; padding-left:0;" class="control-label"> Total Orders </label>
                        <input type="text" class="form-control" placeholder='Total Orders' /></th>
                    <th><label style="padding-right:0; padding-left:0;" class="control-label"> Total Unclear Amount </label>
                        <input type="text" class="form-control" placeholder="Total Unclear Amount" /></th>
                    <th><label style="padding-right:0; padding-left:0;" class="control-label"> Action </label>
                        <input type="text" class="form-control" placeholder="Action" /></th>
                </tr>
                </thead>
                <tbody>
                <?php
                if($usersorders){
                    foreach($usersorders as $uorders){
                        ?>
                        <tr>
                            <td><input type="checkbox"></td>
                            <td><?php echo $uorders->fullname; ?></td>
                            <td><?php echo $uorders->totalorders; ?></td>
                            <td><?php echo $uorders->totalpendingamount; ?></td>
                            <td><a href="javascript:void(0)" onclick="showPayPrice('<?php echo $uorders->userid; ?>','<?php  echo $uorders->totalpendingamount; ?>')" data-toggle="modal" data-target="#actions" class="btn btn-success">Pay</a> <a href="<?php echo base_url(); ?>users/payment_history" class="btn btn-warning">View History</a></td>

                        </tr>
                    <?php
                    }
                }
                ?>

                </tbody>




            </table></div>
    </div>

</div> <!-- card -->