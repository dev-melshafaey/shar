<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>POS System</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>newdesign/css/bootstrap.min.css" rel="stylesheet">
    <!-- Reset -->
    <link href="<?php echo base_url(); ?>newdesign/css/normalize.css" rel="stylesheet">
    <!-- Custom -->
    <link href="<?php echo base_url(); ?>newdesign/css/style.css" rel="stylesheet">
    <!-- Arabic -->
    <link href="<?php echo base_url(); ?>newdesign/css/ar.css" rel="stylesheet">
    <!-- Select -->
    <link href="<?php echo base_url(); ?>newdesign/css/bootstrap-select.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>newdesign/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="<?php echo base_url(); ?>newdesign/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>newdesign/css/DateTimePicker.css" rel="stylesheet" type="text/css" />




    <style>
        .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9 {float: right;}
        .panel-heading {text-align: right;}
        .bootstrap-select:not([class*="span"]):not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {width: 100%;}
        .bootstrap-select.btn-group .btn .filter-option {text-align: right;}
        .bootstrap-select.btn-group .btn .caret {left: 12px; right: 0;}
        .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9 {float: right;}
        .dropdown-menu {text-align: right !important;}
        .green_gate {background: #5cb85c; border: none; color: #fff; margin-bottom: 1em;}
        .red_gate {background: #d9534f; border: none; color: #fff; margin-bottom: 1em;}
        .orange_gate {background: #f4a034; border: none; color: #fff; margin-bottom: 1em;}
        .yellow_gate {background: #c4c641; border: none; color: #fff; margin-bottom: 1em;}
        .form-control::-moz-placeholder {color: #777; text-align: center;}
        .table > thead > tr > th {text-align: center;}
        .form-horizontal .control-label {float: right;}
        .pagination {margin: 0 auto; display: table;}
        .loginmodal-container {background: #fff;text-align: center;}
        .panel-btns {padding: 1em 0 0 1em;}
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <![endif]-->

</head>
<body>
<header>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="col-sm-3 col-xs-12">
                    <ul class="list-inline">
                        <li><div class="dateplace"><h4 style="text-align:center"><img src="<?php echo base_url(); ?>newdesign/images/calendar-icon.png" width="33"> April 27</h4>
                                <h5>00 : 00 : 00</h5></div></li>
                        <li><img src="<?php echo base_url(); ?>newdesign/images/logo.png" alt="Business Solutions" class="img-responsive bslogo"></li>
                    </ul>

                </div>
                <div class="col-sm-7 col-xs-12">
                  </div>
                <div class="col-sm-2 col-xs-12"><img src="<?php echo base_url(); ?>newdesign/images/logo.png" alt="Business Solutions" class="img-responsive bslogo"></div>
            </div> <!--./col-sm-12-->
        </div>
    </div>

</header>

<section>
    <div class="container">
        <div class="panel panel-default">
        <!--
            <div class="panel-btns">
                <div class="row">
                    <ul class="list-inline pull-left nofluid" style="padding-left:1em;padding-bottom:0;">
                        <li><button type="submit" class="btn btn-default"><img src="<?php echo base_url(); ?>newdesign/images/acrobat-flat.png" width="24" height="24"> PDF</button></li>
                        <li><button type="submit" class="btn btn-default"><img src="<?php echo base_url(); ?>newdesign/images/csv-icon.png" width="24" height="24"> CSV</button></li>
                        <li><button type="submit" class="btn btn-default"><img src="<?php echo base_url(); ?>newdesign/images/xl-icon.png" width="24" height="24"> Excel</button></li>
                        <li><button type="submit" class="btn btn-default"><img src="<?php echo base_url(); ?>newdesign/images/print-flat.png" width="24" height="24"> Print</button></li>
                    </ul>
                </div>
            </div>
            -->
            <div class="panel-body" style="padding:0">



                <div class="qrow qcol-sm-12 qcol-md-offset-2 custyle">
                    <div class="col-sm-12">
                        <form id="searchDataForm">
                        <div class="ppanel-body">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Select Chart</label>
                                <div class="col-md-4">
                                    <select id="chart_type" onchange="changeChat(this.value)" name="chart_type" class="selectpicker" style="display: none;">
                                        <option value="straighr">Straight</option>
                                        <option value="pie">Pie</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Search By</label>
                                <div class="col-md-4">
                                    <select  id="search_by" name="search_by" onchange="getSearchBy(this.value)" class="selectpicker" style="display: none;">
                                        <option value="">Select</option>
                                        <option value="month">Month</option>
                                        <option value="Date">Date</option>
                                        <option value="today" selected>Today</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 search_by_date" style="display: none;">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label style="padding-right:0; padding-left:0;" class="control-label"> From </label>
                                        <input type="text" id="search_date_from" name="search_date_from"  class="form-control" data-field="date" readonly /><div id="dtBox"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 search_by_date" style="display: none;">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label style="padding-right:0; padding-left:0;" class="control-label"> To </label>
                                        <input type="text" id="search_date_to" name="search_date_to" class="form-control" data-field="date" readonly /><div id="dtBox"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 search_by_month" style="display:none;">
                                <div class="form-group">
                                    <div class="separator-1">
                                        <label style="padding-right:0; padding-left:0;" class="control-label"> Month </label>
                                        <input type="text" id="search_month" name="search_month" class="form-control" data-field="date" data-format="MM-yyyy" readonly><div id="dtBox-1"></div>
                                    </div>

                                </div>
                            </div>


                        </div> <!-- card -->
                        <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="col-sm-1" style="float:left;">
                                        <button type="button" value="right" class="btn btn-success" onclick="searchData()">Search <span class="glyphicon glyphicon-search"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        </form>
                        <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                    </div>


        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p>تصميم و برمجة شركة <a target="_blank" href="http://durar-it.com/en/"><img alt="Durar Smart Solutions" src="images/durar-smart-solutions.png"></a>  درر للحلول الذكية ش.م.م </p>
            </div><!--./grid -->
        </div><!--./row -->
    </div><!--./container-->
</footer>
<!--menu-->
<div id="wrapper" class="enlarged forced">
    <button class="button-menu-mobile open-left">
        <i class="fa fa-bars"></i>
    </button>
    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <div class="user-details hiddex-xs">
                <div class="pull-left">
                    <img src="images/avatar-1.jpg" alt="" class="thumb-md img-circle img-responsive">
                </div>
                <div class="user-info">
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">عبد الله <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)"><i class="fa fa-face-unlock"></i> الملف الشخصي<div class="ripple-wrapper"></div></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-settings"></i> إعدادات</a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-settings-power"></i> خروج</a></li>
                        </ul>
                    </div>
                    <p class="text-muted m-0">مدير</p>
                </div>
            </div>
            <!--- Divider -->
            <div class="clearfix"></div>
        </div>
    </div>	
    <!-- Left Sidebar End -->

</div>
<!-- END wrapper -->
<!--.menu-->

<!-- login modal start-->
<div class="modal fade" id="actions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>newdesign/images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="g4 form-group search_div" style="text-align: center;">
                                <label id="pay_am_html" class="text-warning"></label>
                                <input type="hidden" id="uid" name="uid"><input type="hidden" id="order_pending_amount" name="order_pending_amount">
                                <input id="amount_val" name="amount_val" type="text" class="form-control" style="width:35%; text-align: center; position: relative; left: 34%;"/>
                                <br clear="all">
                                <input type="button" value="Pay" class="btn btn-success ordbtn   green" onclick="addPayment()">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label style="padding-right:0; padding-left:0;" class="control-label"> Seelct Date</label>
                                    <input type="text" class="form-control" data-field="date" readonly /><div id="dtBox"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="actions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>newdesign/images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-xs-12">
                                <div class="g4 form-group search_div">

                                    <label id="pay_am_html" class="text-warning"></label>
                                    <input type="hidden" id="uid" name="uid">
                                    <input id="amount_val" name="amount_val" type="text" class="form-control" style="width:35%; text-align: center; position: relative; left: 34%;"/>
                                    <br clear="all">
                                    <input type="button" value="Pay" class="btn btn-success   green" onclick="addPayment()">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <div class="modal fade" id="pay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>newdesign/images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="g4 form-group search_div">

                                <label id="pay_am_html" class="text-warning"></label>
                                <input type="hidden" id="uid" name="uid">
                                <input id="amount_val" name="amount_val" type="text" class="form-control" style="width:35%; text-align: center; position: relative; left: 34%;"/>
                                <br clear="all">
                                <input type="button" value="Pay" class="btn btn-success   green" onclick="addPayment()">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- login modal end -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url(); ?>newdesign/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>newdesign/js/bootstrap.min.js"></script>
    <!-- Select Picker -->
    <script src="<?php echo base_url(); ?>newdesign/js/bootstrap-select.js"></script>
    <!-- datepicker -->
    <script type="text/javascript" src="<?php echo base_url(); ?>newdesign/js/DateTimePicker.js"></script>
    <script type="text/javascript">

        function searchData(){
                searchval  = $("#search_by").val();
                search_q = '';

                if(searchval == 'month'){
                    searchmonth = $("#search_month").val();
                    search_q = search_q+'/month/'+searchmonth;
                }
            else{
                    searchdatefrm = $("#search_date_from").val();
                    searchdateto = $("#search_date_to").val();
                    sdata = searchdatefrm+':'+searchdateto;
                    search_q = search_q+'/daily/'+sdata;
                }


            url = '<?php echo base_url(); ?>users/user_graphs/'+search_q;
           // alert(url);
            window.top.location = url;
        }
        function searchData2(){
            sData = $("#searchDataForm").serialize();
            $.ajax({
                url: "<?php echo base_url(); ?>users/search_data_graph",
                type: 'post',
                data:sData,
                cache: false,
                success: function (data) {

                    response = data;

                    serchby = $("#search_by").val();

                    if(serchby == 'month'){
                        mval = $("#search_month").val();
                        type = 'Month '+mval;
                    }
                    else{
                        dfrm = $("#search_date_from").val();
                        dto = $("#search_date_to").val();
                        type = 'Date From '+dfrm+' To'+dto;
                    }

                    chart = $("#chart_type").val();
                  //  alert(chart);
                    if(chart == 'pie'){
                        //loadCharts(type,response);
                        //loadCharts(type,response);
                        loadCharts(type);
                    }
                    else{
                        loadStraights(type);
                    }

                    //  $("#tb_data").html(data);
                    //if(data){
                      //  alert('Invocie Canceled Successfully');
                        //$('.close').trigger('click');

                 //   }
                }
            });
        }
        function getSearchBy(searchbyval){
               //alert(searchbyval);
            if(searchbyval == 'month'){
                $(".search_by_month").show();
                $(".search_by_date").hide();
            }
            else if(searchbyval == 'Date'){
                $(".search_by_month").hide();
                $(".search_by_date").show();
            }
            else{
                $(".search_by_month").hide();
                $(".search_by_date").hide();
            }
        }

       </script>
    <!-- script for sidebar menu -->
    <script>
        var resizefunc = [];
    </script>


    <script src="<?php echo base_url(); ?>chartsjs/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>chartsjs/modules/exporting.js"></script>
    <script src="<?php echo base_url(); ?>chartsjs/modules/drilldown.js"></script>
    <script type="text/javascript">
        type = '<?php echo $type.' '.$date; ?>';
        response = '<?php echo json_encode($user_graph); ?>';

        function loadStraightsDef(type){
            $('#container').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Total Sales. '+type
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Total Sales'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },

                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data:<?php echo json_encode($user_graph); ?>
                }],
                drilldown: {
                    series: []
                }
            });
        }

        function loadStraights(type){
            resp = response;
            alert(resp);
            $('#container').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Total Sales. '+type
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Total Sales'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    turboThreshold:0,
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },

                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data:resp
                }],
                drilldown: {
                    series: []
                }
            });
        }

        function loadChartsDef(type){
            $('#container').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: type
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data:response
                }]
            });
        }
        function loadCharts(type){
            resp = response;
            //
            $('#container').highcharts({
                chart: {

                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: type
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        turboThreshold: 0,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data:<?php echo json_encode($user_graph); ?>
                }]
            });
        }

        $(function () {
            $("#dtBox-1").DateTimePicker(
                {
                    parentElement: ".separator-1",
                    monthYearSeparator: "-"
                });
            $("#dtBox").DateTimePicker();
          //  loadCharts(type);
            loadStraightsDef(type);
            //alert('ready');

        });

        function changeChat(chart_type){
            //alert(chart_type);
            if(chart_type == 'pie'){

                loadCharts(type,response);
            }
            else{
                loadStraights(type,response);
            }
        }
    </script>

</body>
</html>