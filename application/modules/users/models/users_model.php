<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_model extends CI_Model {
    /*

     * Properties

     */

//----------------------------------------------------------------------



    /*

     * Constructor

     */



    function __construct() {

        parent::__construct();



        //Load Table Names from Config
    }

    public function permission_list() {

        $this->db->select('module_name,moduleid,module_name_ar');

        $this->db->from('mh_modules');

        $this->db->where('module_status', 'A');

        $this->db->where('module_parent', 0);

        $this->db->where('moduleid !=', 1);

        $this->db->order_by("module_order", "ASC");

        $query = $this->db->get();

        return $query->result_array();
    }

    function getEmpPaymentHistory($id) {
        $sql = "SELECT
  co.*,u.`fullname`
FROM
  `clear_users_orders` AS co
  INNER JOIN `bs_users` AS u
    ON u.`userid` = co.`clear_by`
WHERE co.user_id = '" . $id . "'
ORDER BY co.`clear_id` DESC ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getOrderListById($id) {
        $sql = "SELECT
 od.*
FROM
  `bs_users` AS u
  LEFT JOIN `orders` AS od
    ON od.`user_id` = u.`userid`
WHERE u.`member_type` != '6' AND u.`userid` = '" . $id . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getUserOrderById($userid) {
        $sql = "SELECT
  u.*,
  COUNT(od.`user_id`) AS totalorders,
  IFNULL(totalpendingamount, 0) AS totalpendingamount,
  IFNULL(totalpendingorders, 0) AS totalpendingorders
FROM
  `bs_users` AS u
  LEFT JOIN `orders` AS od
    ON od.`user_id` = u.`userid`
  LEFT JOIN
    (SELECT 
      od2.`user_id`,
      IFNULL(SUM(od2.`order_amount`),0) -  IFNULL(SUM(crp.`clear_amount`),0)  AS totalpendingamount,
     COUNT(od2.`user_id`) AS totalpendingorders 
    FROM
      `orders` AS od2 
      LEFT JOIN `clear_order_payment` AS crp 
    ON crp.`order_id` = od2.`order_id` 
    WHERE od2.`clear_order` = '0'
      AND od2.cancel_status = '0'
    GROUP BY od2.`user_id`) AS odd2
    ON odd2.user_id = u.`userid`   AND od.`cancel_status` = '0'
WHERE u.`member_type` != '6' AND u.`userid` = '" . $userid . "'
ORDER BY COUNT(od.`user_id`) DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function getSearchGraphData($type, $date) {
        if ($type == 'month') {
            $date1 = '01-' . $date;
            $lastdate = date('Y-m-t', strtotime($date1));
            $from = getSingelDate('from', $date1);
            $to = getSingelDate('to', $lastdate);
        } else {
            //$postData = $this->input->post();
            // $date;
            $dateAr = explode(":", $date);
            $from1 = date('Y-m-d', strtotime($dateAr[0]));
            $to1 = date('Y-m-d', strtotime($dateAr[1]));
            $from = getSingelDate('from', $from1);
            $to = getSingelDate('to', $to1);
        }

        $sql = "SELECT
  i.`itemname`,COUNT(bi.`invoice_item_id`) AS totalsales
FROM
  `an_invoice` AS inv
  INNER JOIN `bs_invoice_items` AS bi
    ON bi.`invoice_id` = inv.`invoice_id`
  INNER JOIN `bs_item` AS i
    ON i.`itemid` = bi.`invoice_item_id`
WHERE inv.`invoice_date` >= '" . $from . "'
  AND inv.`invoice_date` <= '" . $to . "'
  GROUP BY i.`itemid`
  ORDER BY COUNT(bi.`invoice_item_id`) DESC";
        //echo $sql;
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getGraphData() {
        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;

        $sql = "SELECT
  i.`itemname`,COUNT(bi.`invoice_item_id`) AS totalsales
FROM
  `an_invoice` AS inv
  INNER JOIN `bs_invoice_items` AS bi
    ON bi.`invoice_id` = inv.`invoice_id`
  INNER JOIN `bs_item` AS i
    ON i.`itemid` = bi.`invoice_item_id`
WHERE inv.`invoice_date` >= '" . $from . "'
  AND inv.`invoice_date` <= '" . $to . "'
  GROUP BY i.`itemid`
  ORDER BY COUNT(bi.`invoice_item_id`) DESC";
        //echo $sql;
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getUsersOrders1() {
        /* $sql = "SELECT
          u.*,
          COUNT(od.`user_id`) AS totalorders,
          IFNULL(totalpendingamount, 0) AS totalpendingamount,
          IFNULL(totalpendingorders, 0) AS totalpendingorders
          FROM
          `bs_users` AS u
          LEFT JOIN `orders` AS od
          ON od.`user_id` = u.`userid`
          LEFT JOIN
          (SELECT
          od2.`user_id`,
          IFNULL(SUM(od2.`order_amount`),0) -  IFNULL(SUM(crp.`clear_amount`),0)  AS totalpendingamount,
          COUNT(od2.`user_id`) AS totalpendingorders
          FROM
          `orders` AS od2
          LEFT JOIN `clear_order_payment` AS crp
          ON crp.`order_id` = od2.`order_id` AND od2.`cancel_status` = '0'
          WHERE od2.`clear_order` = '0'   AND od2.cancel_status = '0'
          GROUP BY od2.`user_id`) AS odd2
          ON odd2.user_id = u.`userid`
          WHERE u.`member_type` != '6' AND od.`cancel_status` = '0'
          GROUP BY u.`userid`
          ORDER BY COUNT(od.`user_id`) DESC"; */

        $sql = "SELECT
  u.*,
  COUNT(od.`user_id`) AS totalorders,
  IFNULL(totalpendingamount, 0) AS totalpendingamount,

  IFNULL(totalpendingorders, 0) AS totalpendingorders
FROM
  `bs_users` AS u
  LEFT JOIN `orders` AS od
    ON od.`user_id` = u.`userid`
  LEFT JOIN
    (SELECT
      od2.`user_id`,
      IFNULL(SUM(od2.`order_amount`), 0) - IFNULL(SUM(crp.`clear_amount`), 0) AS totalpendingamount,
      COUNT(od2.`user_id`) AS totalpendingorders
    FROM
      `orders` AS od2
      LEFT JOIN `clear_order_payment` AS crp
        ON crp.`order_id` = od2.`order_id`
        AND od2.`cancel_status` = '0'
    WHERE od2.`clear_order` = '0'
      AND od2.cancel_status = '0'
    GROUP BY od2.`user_id`) AS odd2
    ON odd2.user_id = u.`userid`

WHERE u.`member_type` != '6'  AND u.status='A'
  AND od.`cancel_status` = '0'
GROUP BY u.`userid`
ORDER BY COUNT(od.`user_id`) DESC";

//       echo $sql;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /*function getUsersOrders() {

        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);

        $from = $dateArr->from;
        $to = $dateArr->to;
        $sql = "SELECT
  u.*,
  COUNT(od.`user_id`) AS totalorders,
  IFNULL(totalpendingamount, 0) AS totalpendingamount,
   IF(IFNULL(totalpendingamount, 0) - IFNULL(totalclearamount, 0) < 0, 0,totalpendingamount-totalclearamount) AS totalpendingamount,
  IFNULL(totalpendingorders, 0) AS totalpendingorders
FROM
  `bs_users` AS u
  LEFT JOIN `orders` AS od
    ON od.`user_id` = u.`userid`
  LEFT JOIN
    (SELECT
      od2.`user_id`,
      IFNULL(SUM(od2.`order_amount`), 0) AS totalpendingamount,
      COUNT(od2.`user_id`) AS totalpendingorders
    FROM
      `orders` AS od2
    WHERE od2.`clear_order` = '0'
      AND od2.cancel_status = '0'
    GROUP BY od2.`user_id`) AS odd2
    ON odd2.user_id = u.`userid`
    LEFT JOIN
    (SELECT
      cur.`user_id`,
      IFNULL(SUM(cur.`clear_amount`), 0) AS totalclearamount
    FROM
      `clear_users_orders` AS cur
    GROUP BY cur.`user_id`) AS odd4
    ON odd4.user_id = u.`userid`
WHERE u.`member_type` != '6'
  AND u.status = 'A'
  AND od.`cancel_status` = '0' AND  create >= '" . $from . "' AND create <= '" . $to . "' 
GROUP BY u.`userid`
ORDER BY COUNT(od.`user_id`) DESC ";
        //echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }*/

    function getUsersOrders() {
        $subq2 = '';
        $subq3 = '';
        $subq4 = '';
        $subq5 = '';
        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);

        $from = $dateArr->from;
        $to = $dateArr->to;
        $subq2.= " AND od.`create` >= '" . $from . "' AND od.`create` <= '" . $to . "' ";

        $subq3.= " AND od2.`create` >= '" . $from . "' AND od2.`create` <= '" . $to . "' ";

        $subq4.= " AND od2.`create` >= '" . $from . "' AND od2.`create` <= '" . $to . "' ";

        $subq5.= " WHERE cur.`created` >= '" . $from . "' AND cur.`created` <= '" . $to . "' ";
        $sql = "SELECT
  u.*,
  COUNT(od.`user_id`) AS totalorders,
  IFNULL(totalpendingamount, 0) - IFNULL(totalclearamount,0) AS totalpendingamount,
  totalcancelamount,
  IFNULL(totalpendingorders, 0) AS totalpendingorders
FROM
  `bs_users` AS u
  LEFT JOIN `orders` AS od
    ON od.`user_id` = u.`userid`
  LEFT JOIN
    (SELECT
      od2.`user_id`,
      IFNULL(SUM(od2.`order_amount`), 0)  AS totalpendingamount,
      COUNT(od2.`user_id`) AS totalpendingorders
    FROM
      `orders` AS od2
    WHERE od2.`clear_order` = '0'
      AND od2.cancel_status = '0' $subq3
    GROUP BY od2.`user_id`) AS odd2
    ON odd2.user_id = u.`userid`

     LEFT JOIN
    (SELECT
  cur.`user_id`,
  IFNULL(SUM(cur.`clear_amount`),0) AS totalclearamount
FROM
  `clear_users_orders` AS cur
      $subq5
    GROUP BY cur.`user_id`) AS odd4
    ON odd4.user_id = u.`userid`


  LEFT JOIN
    (SELECT
 od2.order_id,
      od2.`user_id`,
      IFNULL(SUM(od2.`order_amount`), 0) - IFNULL(SUM(crp.`clear_amount`), 0) AS totalcancelamount,
      COUNT(od2.`user_id`) AS totalcancelpendingorders
    FROM
      `orders` AS od2
      LEFT JOIN `clear_order_payment` AS crp
        ON crp.`order_id` = od2.`order_id`
        AND od2.`cancel_status` = '0'
    WHERE od2.cancel_status = '0'
      AND od2.clear_order = '1' $subq4
    GROUP BY od2.`user_id`) AS odd23
    ON odd23.user_id = u.`userid`
WHERE u.`member_type` != '6'
  AND od.`cancel_status` = '0' $subq2
GROUP BY u.`userid`
ORDER BY COUNT(od.`user_id`) DESC";
        // echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function permission_parent_list() {

        $this->db->select('mh_modules.module_name,mh_modules.moduleid,mh_modules.module_name_ar');

        $this->db->from('mh_modules');

        $this->db->join("addition_modules", "addition_modules.module_parent_id=mh_modules.moduleid", 'inner');

        $this->db->where('module_status', 'A');

        $this->db->where('module_parent', 0);

        $this->db->where('moduleid !=', 1);

        $this->db->group_by("moduleid");
        $this->db->order_by("module_order", "ASC");

        $query = $this->db->get();

        return $query->result_array();
    }

    function getPermissionByutypeid($id) {
        $sql = "SELECT
  GROUP_CONCAT(ap.`add_mod_id`) AS addmodules
FROM
  `additional_permissions` AS ap
WHERE ap.`user_type_id` = '$id' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function getPaymentVoucher($payment_id) {
        // $sql = "SELECT * FROM `payments` AS p WHERE p.`id` = '" . $payment_id . "'";
        $sql = "SELECT
  cuo.*,
  u.`fullname`
FROM
  `clear_users_orders` AS cuo
  INNER JOIN `bs_users` AS u
    ON u.`userid` = cuo.`user_id`
WHERE cuo.`clear_id` = '" . $payment_id . "' ";
        // echo $sql;
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    public function permission_list_additionl($id) {

        $this->db->select('addition_modules.*');

        $this->db->from('addition_modules');

        $this->db->where('module_parent_id', $id);

        $this->db->order_by("add_module_order", "ASC");

        $query = $this->db->get();

        return $query->result_array();
    }

    function getUsersOrdersMarch() {
        /* $sql = "SELECT
          u.*,
          COUNT(od.`user_id`) AS totalorders,
          IFNULL(totalpendingamount, 0) AS totalpendingamount,
          IFNULL(totalpendingorders, 0) AS totalpendingorders
          FROM
          `bs_users` AS u
          LEFT JOIN `orders` AS od
          ON od.`user_id` = u.`userid`
          LEFT JOIN
          (SELECT
          od2.`user_id`,
          IFNULL(SUM(od2.`order_amount`),0) -  IFNULL(SUM(crp.`clear_amount`),0)  AS totalpendingamount,
          COUNT(od2.`user_id`) AS totalpendingorders
          FROM
          `orders` AS od2
          LEFT JOIN `clear_order_payment` AS crp
          ON crp.`order_id` = od2.`order_id` AND od2.`cancel_status` = '0'
          WHERE od2.`clear_order` = '0'   AND od2.cancel_status = '0'
          GROUP BY od2.`user_id`) AS odd2
          ON odd2.user_id = u.`userid`
          WHERE u.`member_type` != '6' AND od.`cancel_status` = '0'
          GROUP BY u.`userid`
          ORDER BY COUNT(od.`user_id`) DESC"; */

        $sql = "SELECT
  u.*,
  COUNT(od.`user_id`) AS totalorders,
  IFNULL(totalpendingamount, 0) AS totalpendingamount,
 
  IFNULL(totalpendingorders, 0) AS totalpendingorders
FROM
  `bs_users` AS u
  LEFT JOIN `orders` AS od
    ON od.`user_id` = u.`userid`
  LEFT JOIN
    (SELECT
      od2.`user_id`,
      IFNULL(SUM(od2.`order_amount`), 0) - IFNULL(SUM(crp.`clear_amount`), 0) AS totalpendingamount,
      COUNT(od2.`user_id`) AS totalpendingorders
    FROM
      `orders` AS od2
      LEFT JOIN `clear_order_payment` AS crp
        ON crp.`order_id` = od2.`order_id`
        AND od2.`cancel_status` = '0'
    WHERE od2.`clear_order` = '0'
      AND od2.cancel_status = '0'
    GROUP BY od2.`user_id`) AS odd2
    ON odd2.user_id = u.`userid`
  
WHERE u.`member_type` != '6'
  AND od.`cancel_status` = '0'
GROUP BY u.`userid`
ORDER BY COUNT(od.`user_id`) DESC";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function permission_list_child($id) {

        $this->db->select('module_name,moduleid,module_name_ar');

        $this->db->from('mh_modules');

        $this->db->where('module_status', 'A');

        $this->db->where('module_parent', $id);

        $this->db->where('moduleid !=', 1);

        $this->db->order_by("module_order", "ASC");

        $query = $this->db->get();

        return $query->result_array();
    }

    public function users_list() {

        $permission_type_id = $this->input->post('permission_type_id');

        $member_type = get_member_type();

        $query = "SELECT 

				  a.userid,

				  a.`fullname`,

				  a.`username`,

                                  a.member_type,

				  IF (a.`employeecode`='','-----',a.`employeecode`) AS employeecode,

				  IF (a.`office_number`='','-----',a.`office_number`) AS office_number,

				  IF (a.`fax_number`='','-----',a.`fax_number`) AS fax_number,

				  IF (a.`phone_number`='','-----',a.`phone_number`) AS phone_number,

				  IF (a.`status`='A','Active','Deactive') AS `status`,

                                  bs_permission_type.permission_type_id,

                                  bs_permission_type.permissionhead

				FROM

				  bs_users AS a

                                  LEFT JOIN bs_permission_type ON bs_permission_type.permission_type_id=a.member_type

				  WHERE a.`member_type` !=6 ";

        /*

          if ($member_type != '5') {

          $query .= " (a.ownerid='" . $this->session->userdata('bs_ownerid') . "' OR a.ownerid='" . $this->session->userdata('bs_userid') . "') AND ";

          }

         */



        $query .= "AND  a.userid > 0 Order by a.fullname ASC";



        $q = $this->db->query($query);

        return $q->result();
    }

    public function get_permission() {

        $permission_type_id = $this->input->post('permission_type_id');

        $owner_id = ownerid();

        $this->db->select('permissionid,permission_text');

        $this->db->from($this->config->item('table_permissions'));

        $this->db->where('permission_type_id', $permission_type_id);

        $this->db->where('owner_id', $owner_id);

        $query = $this->db->get();

        $res = $query->result_array();

        $arr['p_id'] = $res[0]['permissionid'];

        $arr['p_text'] = $res[0]['permission_text'];

        echo json_encode($arr);
    }

    public function get_user_detail($id) {

        $this->db->where('userid', $id);

        $query = $this->db->get('bs_users');



        if ($query->num_rows() > 0) {

            return $query->row();
        }
    }

    public function delete_users() {



        $u = $this->input->post('u');

        if ($u != '') {

            $this->db->where_in('userid', $u);

            $this->db->delete($this->config->item('table_users'));
        }

        do_redirect('all_users?e=12');
    }

    public function save_permission() {

        $permission_type_id = $this->input->post('permission_type_id');

        $owner_id = $this->session->userdata('bs_userid');

        $permissionid = $this->input->post('permissionid');



        if (post('p')) {



            foreach ($this->input->post('p') as $p) {

                foreach ($this->input->post('subp') as $child) {

                    $perm = str_replace($p . '_', '', $child);





                    //echo $perm;

                    if ($perm != '') {

                        $int = filter_var($perm, FILTER_SANITIZE_NUMBER_INT);

                        if (!is_numeric($int)) {

                            $arr[$p][] = $perm;
                        }
                    }
                }

                if (empty($arr[$p])) {

                    $arr[$p][] = '';
                }
            }

            $permission_text = json_encode($arr);

            $data = array(
                'permission_type_id' => $permission_type_id,
                'permission_text' => $permission_text,
                'owner_id' => $owner_id
            );









            if ($permissionid != '') {

                $this->db->where('permissionid', $permissionid);

                $this->db->update($this->config->item('table_permissions'), $data);
            } else {

                $this->db->select('permissionid');

                $this->db->from($this->config->item('table_permissions'));

                $this->db->where('permission_type_id', $permission_type_id);

                $this->db->where('owner_id', $owner_id);

                $query = $this->db->get();

                if ($query->num_rows() > 0) {

                    $res = $query->result_array();

                    $this->db->where('permissionid', $res[0]['permissionid']);

                    $this->db->update($this->config->item('table_permissions'), $data);
                } else {

                    $this->db->insert($this->config->item('table_permissions'), $data);
                }
            }



            do_redirect('permissions?e=10');
        } else {

            do_redirect('users/permissions/?e=17');
        }

        //print_r(json_encode($p));
    }

    public function add_new_user() {

        $data = $this->input->post();

        $userid = $this->input->post('userid');

        //echo "<pre>";
        //print_r($data);
        //exit;
        $userid = $this->input->post('userid');

        if ($this->input->post('upassword') != "") {
            $data['upassword'] = md5($this->input->post('upassword'));
        } else {
            unset($data['upassword']);
        }




        //echo "<pre>";
        //print_r($data);
        //exit;

        unset($data['sub_mit'], $data['sub_reset'], $data['userid'], $data['confpassword']);



        if ($userid != '') {



            $this->db->where('userid', $userid);

            $this->db->update($this->config->item('table_users'), $data);

            do_redirect('all_users?e=10');
        } else {

            if ($this->searchlike($this->input->post('username')) == false) {

                $this->db->insert($this->config->item('table_users'), $data);

                do_redirect('all_users?e=10');
            } else {

                do_redirect('all_users?e=13');
            }
        }
    }

    function searchlike($par) {



        $this->db->where('member_type !=', '6');

        $this->db->where('username', $par);

        $qu = $this->db->get('bs_users');

        $get = $qu->result();

        if ($get) {

            return true;
        } else {

            return false;
        }
    }

//----------------------------------------------------------------------	
}
