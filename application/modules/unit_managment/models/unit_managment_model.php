<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Unit_managment_model extends CI_Model {
    /*
     * Properties
     */

//----------------------------------------------------------------------

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();

        //Load Table Names from Config
    }

    public function get_list() {
        //$this->db->select('module_name,moduleid');
        $this->db->select('an_expenses.*,an_expenses.id as eid,an_expenses_charges.id,an_expenses_charges.expense_title as expenses_title_cat,COUNT(ie.expense_id) AS totalinvoices');
        
        $this->db->join('an_expenses_charges','an_expenses_charges.id=an_expenses.expense_charges_id','left');
		$this->db->join('expense_purchase ie','ie.expense_id= an_expenses.id','left');
        $this->db->group_by('an_expenses.id'); 
		$this->db->order_by("an_expenses.id", "ASC");
        $query = $this->db->get('an_expenses');
        return $query->result();
    }

    public function get_unit() {
        //$this->db->select('module_name,moduleid');
        $this->db->from('units');
        $this->db->order_by("unit_id", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    public function getunit($id=null) {
        //$this->db->select('module_name,moduleid');
        $this->db->from('units');
        $this->db->where('unit_id',$id);
        $this->db->order_by("unit_id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    public function get_ex_cat($id=null) {
        //$this->db->select('module_name,moduleid');
        $this->db->from('an_expenses_charges');
        $this->db->where('id',$id);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }
	
	
	function searchexpenseByTitle($search_name){
			$sql = "SELECT * FROM an_expenses_charges AS ec WHERE ec.expense_title = '" . $search_name . "';";
	        $q = $this->db->query($sql);
			 if ($q->num_rows() > 0) {
			 	return true;
			 }
			 else{
				 return false;
			 }

	}
    function getExpenseByTitle($search_name) {
        $sql = "SELECT * FROM an_expenses_charges AS ec WHERE ec.expense_title LIKE '%" . $search_name . "%';";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            $locs = $q->result();
            foreach ($locs as $eachLoc) {
                $customers[] = array('cus' => $eachLoc->expense_title, 'id' => $eachLoc->id);
            }
        } else {
            return '0';
        }


        return $customers;
    }
	
	//shahzaib function 
	//9/11/2015
	function getPurchase(){
		
		$this->db->from('an_purchase');
        $this->db->order_by("purchase_id", "DESC");
        $query = $this->db->get();
        return $query->result();
	
	}

//----------------------------------------------------------------------	
}

?>