<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inventory extends CI_Controller {
    /*
     * Properties
     */

    private $_data = array();
    private $remin = 0;

//----------------------------------------------------------------------
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        //load Home Model
        //echo get_set_value('site_lang');
        $this->load->model('inventory_model', 'inventory');
        $this->load->model('inventory2_model', 'inventory2');
        $this->lang->load('main', get_set_value('site_lang'));
        //$this->_data['udata'] = userinfo_permission();
    }

    /*

     * Properties

     */







    /*

     * Home Page

     */

    public function index() {

        // Load Home View

        $this->load->view('inventory', $this->_data);
    }

    function finishgoods($id = null) {

        $this->_data['inside2'] = 'itemsfinish';

        $this->_data['inside'] = 'add-finish-item';
        $data = $this->inventory->get_raw_material($id);
        $this->_data['hd'] = $data;

        $this->load->view('common/tabs', $this->_data);
    }


    public function categories($act = '', $catid = '') {

        // Load Home View

        if ($act == 'addnew') {

            $this->_data['cat'] = $this->inventory->cat_detail($catid);

            //$this->load->view('categories_new', $this->_data);



            $this->_data['inside'] = 'categories_new';

            $this->_data['inside2'] = 'categories';

            //$this->load->view('common/main', $this->_data);
        } else {

            if ($this->input->post('sub_mit')) {

                $this->inventory->add_update_category();
            } else {

//                $this->load->view('categories', $this->_data);

                $this->_data['inside'] = 'categories_new';

                $this->_data['inside2'] = 'categories';
            }
        }

        $this->load->view('common/tabs', $this->_data);
    }

    public function bulk_delete() {

        $this->inventory->delete_categories();
    }

    public function bulk_delete_stores() {

        $this->inventory->delete_stores();
    }

    public function bulk_point_or_reorder() {

        $this->inventory->bulk_point_or_reorders();
    }

//----------------------------------------------------------------------



    /*

     * Add Purchase Form

     */

    public function add_purchase() {

        // Load Home View

        $this->load->view('add-purchase', $this->_data);
    }

    function test() {
        echo "dsasdasd";
        exit;
    }

    function add_sales_confirmation_items() {

        $data = $this->input->post();

        //echo "<pre>";
        //print_r($data);

        if (!empty($data['receipt'])) {



            $purchase_receipt['invoice_id'] = $data['invoiceid'];

            $this->db->insert('sales_receipt', $purchase_receipt);

            $receipt_id = $this->db->insert_id();



            foreach ($data['receipt'] as $i => $receipt) {

                if ($receipt != 0) {

                    $receipt_item_data['invoice_id'] = $data['invoiceid'];

                    $receipt_item_data['item_id'] = $data['itemid'][$i];

                    $receipt_item_data['sale_item_id'] = $data['pivid'][$i];

                    $receipt_item_data['store_id'] = $data['store_id'][$i];

                    $receipt_item_data['is_complete'] = 1;

                    $receipt_item_data['receipt_id'] = $receipt_id;

                    $this->db->insert('sales_confirm_items', $receipt_item_data);





                    $store_data['store_id'] = $data['store_id'][$i];

                    $store_data['item_id'] = $data['itemid'][$i];

                    $store_data['soled_quantity'] = $data['quantity'][$i];

                    $this->db->insert('store_soled_items', $store_data);
                }
            }





            $invoice['confirm_status'] = 1;

            $this->db->where('invoiceid', $data['invoiceid']);

            $this->db->update('bs_sales_invoice', $invoice);



            redirect(base_url() . 'inventory/stores?e=10');
        }
    }

   

    function add_confirmation_items() {
        $data = $this->input->post();

        if (!empty($data['receipt'])) {



            $purchase_receipt['invoice_id'] = $data['invoiceid'];

            $this->db->insert('purchase_receipt', $purchase_receipt);

            $receipt_id = $this->db->insert_id();



            foreach ($data['receipt'] as $i => $receipt) {

                if ($receipt != 0) {





                    //$receipt_item_data['quantity'] = $receipt_item_data['quantity'][$i];

                    $receipt_item_data['invoice_id'] = $data['invoiceid'];

                    $receipt_item_data['item_id'] = $data['itemid'][$i];

                    $receipt_item_data['purchase_item_id'] = $data['pivid'][$i];

                    $receipt_item_data['store_id'] = $data['store_id'][$i];

                    $receipt_item_data['is_complete'] = 1;

                    $receipt_item_data['receipt_id'] = $receipt_id;



                    //is_complete

                    $this->db->insert('purchase_confirm_items', $receipt_item_data);



                    $store_data['store_id'] = $data['store_id'][$i];

                    $store_data['item_id'] = $data['itemid'][$i];

                    $store_data['quantity'] = $data['quantity'][$i];

                    $store_data['purchase_id'] = $data['invoiceid'];

                    $this->db->insert('bs_store_items', $store_data);
                } else {



                    $receipt_item_data['invoice_id'] = $data['invoiceid'];

                    $receipt_item_data['item_id'] = $data['itemid'][$i];

                    $receipt_item_data['purchase_item_id'] = $data['pivid'][$i];

                    $receipt_item_data['store_id'] = $data['store_id'][$i];

                    $receipt_item_data['other_quantity'] = $data['quantiy_avail'][$i];

                    $receipt_item_data['is_complete'] = 0;

                    $receipt_item_data['receipt_id'] = $receipt_id;

                    $receipt_item_data['reason'] = $data['reason'][$a];



                    $this->db->insert('purchase_confirm_items', $receipt_item_data);

                    if ($data['quantiy_avail'][$i] != '0') {

                        $store_data['store_id'] = $data['store_id'][$i];

                        $store_data['item_id'] = $data['itemid'][$i];

                        $store_data['quantity'] = $data['quantiy_avail'][$i];

                        $store_data['purchase_id'] = $data['invoiceid'];

                        $this->db->insert('bs_store_items', $store_data);
                    }
                }
            }



            $invoice['confirm_status'] = 1;

            $this->db->where('invoiceid', $data['invoiceid']);

            $this->db->update('bs_purchase_invoice', $invoice);
        }



        //inventory/stores

        redirect(base_url() . 'inventory/stores?e=10');
    }

//----------------------------------------------------------------------



    /*

     * Item Listing Page

     */

    function ttt() {
        echo "items";
        exit;
    }

    public function juiceitems($id = "") {


        /* $this->_data['products']	=	$this->inventory->product_list();



          echo '<pre>'; print_r($this->_data['products']); */

        // Load Home View
        //$this->_data['storeid'] = $id;
        //$this->load->view('items', $this->_data);

        if ($id != '') {

            $this->_data['hd'] = $this->inventory->get_juiceitem_detail($id);
            //echo "<pre>";
            //print_r($this->_data['hd']);
            $this->_data['link_items'] = $this->inventory->linkitems($id);
            //echo "<pre>";
            //print_r($this->_data['link_items']);
        }

        $this->_data['additional_items'] = $this->inventory->getAdditionalItems();

        $this->_data['inside2'] = 'juiceitems';

        $this->_data['inside'] = 'add-itemjuice';

        $this->load->view('common/tabs', $this->_data);
    }

    function additionalitems() {
        if ($id != '') {

            $this->_data['hd'] = $this->inventory->get_juiceitem_detail($id);
            // echo "<pre>";
            //print_r($this->_data['hd']);
        }

        $this->_data['inside2'] = 'list_extra_items';

        $this->_data['inside'] = 'add-item_extra';

        $this->load->view('common/tabs', $this->_data);
    }

    function editAdditem($id) {


        $this->_data['hd'] = $this->inventory->getAdditionItemById($id);
        //echo "<pre>";
        //print_r($this->_data['hd']);
        // exit;
        if (post()) {
            
        }

        $this->_data['inside'] = 'add-item_extra';

        $this->load->view('common/main', $this->_data);
    }

    function add_invent_items() {

        if ($id != '') {

            $this->_data['hd'] = $this->inventory->get_juiceitem_detail($id);
            // echo "<pre>";
            //print_r($this->_data['hd']);
        }

        $this->_data['inside2'] = 'list_invent_items';

        $this->_data['inside'] = 'add-item_invent';

        $this->load->view('common/tabs', $this->_data);
    }

    function add_inventstore_items() {

        if ($id != '') {

            $this->_data['hd'] = $this->inventory->get_juiceitem_detail($id);
            // echo "<pre>";
            //print_r($this->_data['hd']);
        }

        $this->_data['inside2'] = 'list_invent_storeitems';

        $this->_data['inside'] = 'add-item_inventstore';

        $this->load->view('common/tabs', $this->_data);
    }

    public function items($id = "") {


        /* $this->_data['products']	=	$this->inventory->product_list();



          echo '<pre>'; print_r($this->_data['products']); */

        // Load Home View

        $this->_data['storeid'] = $id;

        //$this->load->view('items', $this->_data);



        $this->_data['inside2'] = 'items';

        $this->_data['inside'] = 'add-item';





        $this->load->view('common/tabs', $this->_data);
    }

//----------------------------------------------------------------------



    /*

     * Add New Item

     */

    public function add_item($itemid) {

        // Load Home View

        if (post()) {

            $this->inventory->add_new_item();

            //var_dump($_POST);
            //die();
        }



        if ($itemid != '') {

            $this->_data['hd'] = $this->inventory->get_item_detail($itemid);
        }

        //$this->load->view('add-item', $this->_data);

        $this->_data['inside'] = 'add-item';

        $this->load->view('common/main', $this->_data);
    }

    public function add_inven_item_additional($itemid) {

        // Load Home View

        if (post()) {

            /// $this->inventory->add_new_item();
            $postData = post();

            echo "<pre>";
            print_r($postData);

            exit;


            $itemid = post('itemid');
            if ($itemid != '') {
                $data['inven_item_name_ar'] = post('item_ar_name');
                $data['inven_item_name_eng'] = post('item_eng_name');
                $this->db->where('inven_item_id', $itemid);
                $this->db->update('inventory_items', $data);
            } else {
                $data['inven_item_name_ar'] = post('item_ar_name');
                $data['inven_item_name_eng'] = post('item_eng_name');
                $itemid = $this->inventory->inserData('inventory_items', $data);
            }
            //var_dump($_POST);
            //die();
        }



        if ($itemid != '') {

            $this->_data['hd'] = $this->inventory->get_additionalitem_detail($itemid);
        }

        redirect(base_url() . 'inventory/add_invent_items?e=16');
        //$this->_data['inside'] = 'add-itemjuice';
        //$this->load->view('common/main', $this->_data);
    }

    public function add_item_additional($itemid) {

        // Load Home View

        if (post()) {

            /// $this->inventory->add_new_item();
            $postData = post();

            //echo "<pre>";
            //  print_r($postData);
            //exit;
            $itemid = post('itemid');
            if ($itemid != '') {
                $data['item_ar_name'] = post('item_ar_name');
                $data['item_eng_name'] = post('item_eng_name');
                $data['more_price'] = post('more_price');

                // $itemid = $this->inventory->inserData('bs_item', $data);
                $this->db->where('ad_item_id', $itemid);
                $this->db->update('additional_items', $data);
                //exit;
            } else {
                $data['item_ar_name'] = post('item_ar_name');
                $data['item_eng_name'] = post('item_eng_name');
                $data['more_price'] = post('more_price');
                $itemid = $this->inventory->inserData('additional_items', $data);
            }
            //var_dump($_POST);
            //die();
        }



        if ($itemid != '') {

            $this->_data['hd'] = $this->inventory->get_additionalitem_detail($itemid);
        }

        redirect(base_url() . 'inventory/additionalitems?e=16');
        //$this->_data['inside'] = 'add-itemjuice';
        //$this->load->view('common/main', $this->_data);
    }

    public function add_item_juice($itemid) {

        // Load Home View

        if (post()) {

            /// $this->inventory->add_new_item();
            $postData = post();
            //echo "<pre>";
            //print_r($postData);
            // exit;

            if ($postData['removeLinked'] != "") {
                $remvelinked = $postData['removeLinked'];

                //  echo strlen($remvelinked);
                // echo $newstr =  substr(0,1,$remvelinked);
                // echo substr(0,1,$newstr);
                $newstr = substr($remvelinked, 1);
                $strlen = strlen($newstr);

                $newstring = substr($newstr, 0, $strlen - 1);
                $removeArr = explode(',', $newstring);
                if (!empty($removeArr)) {
                    foreach ($removeArr as $i => $remv) {
                        if ($remv == "") {
                            //unset($remv);
                            unset($removeArr[$i]);
                        } elseif ($remv == "null") {
                            //unset($remv);
                            unset($removeArr[$i]);
                        } else {
                            //$rm = intval($remv);
                            //echo intval(strval($remv));
                            //echo $rm = $remv+0;
                            //echo $remv+0;
                            //var_dump($remv); // string '13' (length=2)
                            $myVar = intval($remv);
                            //echo $b = (int)$remv;
                            //echo (int)$myVar;
                            $linkid = $this->inventory->getLinkid($remv);
                            //exit;
                            //echo (int)str_replace(array(' ', ','), '', $remv);
                            //exit;
                            //$rm = (int)$remv;
                            $this->db->where_in('link_id', $linkid);
                            $this->db->delete('link_items');
                        }
                    }
                }
                
            }

         
            $itemid = post('itemid');
            $has_one_size = post('has_one_size');

            if ($has_one_size != "") {
                $data['sale_price'] = post('large_price');
                $data['large_price'] = post('large_price');
                $data['medium_price'] = 0;
                $data['small_price'] = 0;
            } else {
                $data['medium_price'] = post('medium_price');
                $data['large_price'] = post('large_price');
                $data['small_price'] = post('small_price');
            }

            $data['has_one_size'] = $has_one_size;

            if ($itemid != '') {


                $data['itemname'] = serialize(post('itemname'));
                $data['categoryid'] = post('categoryid');


                $data['itemtype'] = 'J';
                $data['barcodenumber'] = post('barcodenumber');
                $data['categ_type'] = post('categ_type');
                $data['notes_ar'] = post('notes_ar');
                $data['notes'] = post('notes');
                $data['product_status'] = post('product_status');
                // $itemid = $this->inventory->inserData('bs_item', $data);
                $this->db->where('itemid', $itemid);
                $this->db->update('bs_item', $data);

                $addi_items = post('addi_items');
                $price = post('price');
                $link_id = post('link_id');

                if (!empty($addi_items)) {
                    foreach ($addi_items as $i => $ad) {

                        if (isset($link_id[$i]) && $link_id[$i] != "") {
                            //echo "iff";
                            $adData = array();
                            $adData['main_item_id'] = $itemid;
                            $adData['additional_item_id'] = $ad;
                            $adData['price'] = $price[$i];
                            $adData['link_id'] = $link_id[$i];
                            // $this->inventory->inserData('link_items', $adData);
                            //  $invoice2['confirm_status'] = 1;
                            //  print_r($adData);

                            $this->db->where('link_id', $link_id[$i]);
                            $this->db->update('link_items', $adData);
                        } else {
                            //   echo "else";
                            $adData = array();
                            $adData['main_item_id'] = $itemid;
                            $adData['additional_item_id'] = $ad;
                            $adData['price'] = $price[$i];
                            //  $adData['link_id'] = $price[$i];
                            //  print_r($adData);
                            $this->inventory->inserData('link_items', $adData);
                        }
                    }
                    //   exit;
                }
            } else {
                $data['itemname'] = serialize(post('itemname'));
                $data['categoryid'] = post('categoryid');
                $data['large_price'] = post('large_price');
                $data['medium_price'] = post('medium_price');
                $data['itemtype'] = 'J';
                $data['categ_type'] = post('categ_type');
                $data['barcodenumber'] = post('barcodenumber');
                $data['small_price'] = post('small_price');
                $data['notes_ar'] = post('notes_ar');
                $data['notes'] = post('notes');
                $data['type_unit'] = 0;
                $data['product_status'] = post('product_status');
                $itemid = $this->inventory->inserData('bs_item', $data);

                //price
                $addi_items = post('addi_items');
                $price = post('price');
                $link_id = post('link_id');
                if (!empty($addi_items)) {
                    foreach ($addi_items as $i => $ad) {

                        $adData['main_item_id'] = $itemid;
                        $adData['additional_item_id'] = $ad;
                        $adData['price'] = $price[$i];
                        $this->inventory->inserData('link_items', $adData);
                    }
                }
                //$addItem['']
            }
            //var_dump($_POST);
            //die();
        }



        if ($itemid != '') {

            $this->_data['hd'] = $this->inventory->get_juiceitem_detail($itemid);
        }

        redirect(base_url() . 'inventory/juiceitems?e=16');
        //$this->_data['inside'] = 'add-itemjuice';
        //$this->load->view('common/main', $this->_data);
    }

    function history($id = null, $ty = null) {
        $this->_data['pid'] = $id;
        $this->_data['ty'] = $ty;
        $this->_data['inside'] = 'history';
        $this->load->view('common/main', $this->_data);
    }

    public function add_point_of_order($poid) {

        // Load Home View

        if ($this->input->post('submit_reorder') == 'Submit') {

            $this->inventory->add_new_point_of_order();
        }



        if ($poid != '') {

            $this->_data['hd'] = $this->inventory->get_pod_detail($poid);
        }

        $this->load->view('add_purchase', $this->_data);
    }

//----------------------------------------------------------------------



    /*

     * Stores Listing Page

     */

    public function stores() {

        // Load Home View
        //$this->load->view('stores', $this->_data);

        $this->_data['inside2'] = 'stores';

        $this->_data['inside'] = 'add-store';

        $this->load->view('common/tabs', $this->_data);
    }

    function storeItems($id) {
        $this->_data['id'] = $id;
        $this->_data['inside'] = 'store_items';
        $this->load->view('common/main', $this->_data);
    }

    function purchase_receipt() {

        $this->_data['recept'] = $this->inventory->purchase_receipt();
        $this->_data['inside'] = 'purchase_receipt';
        $this->load->view('common/main', $this->_data);
        // $this->load->view('store_items', $this->_data);
    }

    function sales_recept() {
        $this->_data['recept'] = $this->inventory->sales_recept();
        $this->_data['inside'] = 'sales_receipt';
        $this->load->view('common/main', $this->_data);
    }

    function viewItems($invoice_id) {

        $this->_data['invoice_items'] = $this->inventory->getitems($invoice_id);

        $this->_data['inside'] = 'invoice_items_receipt';

        $this->load->view('common/main', $this->_data);
    }

    function viewSalesItems($invoice_id) {
        $this->_data['invoice_items'] = $this->inventory->getsalesitems($invoice_id);

        $this->_data['inside'] = 'invoice_salesitems_receipt';

        $this->load->view('common/main', $this->_data);
    }

    /*

     * Add New Item

     */

    public function add_store($storeid = '') {

        // Load Home View

        if (post()) {



            $this->inventory->addStore();
        }

        if ($storeid != '') {

            $this->_data['cat'] = $this->inventory->get_store_detail($storeid);
        }





        //$this->load->view('add-store', $this->_data);

        $this->_data['inside2'] = 'stores';

        $this->_data['inside'] = 'add-store';

        $this->load->view('common/tabs', $this->_data);
    }

    public function add_suppliers() {

        // Load Home View

        $this->inventory->add_supplier();
    }

//----------------------------------------------------------------------



    /*

     * Stores Listing Page

     */

    public function point_reorder() {

        // Load Home View

        $this->load->view('point-of-reorder', $this->_data);
    }

//----------------------------------------------------------------------







    /*     * ********************************************************* */

    ///pending_sales_list

    function pending_sales_list($id = '') {

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);

        $userid = $this->session->userdata('bs_userid');

        $this->_data['sales_invoice'] = $this->inventory->getPendingSales($userid);

        $this->_data['inside'] = 'pending_sales_invoice';

        //pending_sales_list

        $this->load->view('common/main', $this->_data);



        //  $this->_data['inside2']='pending_purchase_invoice';
        // $this->_data['inside']='add-customer-form';
        //   $this->load->view('common/tabs', $this->_data);
    }

    function getSalesItems($id = null) {

        $this->_data['id'] = $id;

        ///print_r($this->_data['sales_items']);

        $this->_data['inside'] = 'pending_sales_items';

        $this->load->view('common/main', $this->_data);
    }

    function sold_onebyone($it = null, $st = null, $sales_quantity = null, $userid = null, $inv = null) {



        //$count=1;
        //echo "<pre>";
        //echo $it.','.$st;

        $get_store_its0 = $this->inventory->getStoreItemsToSole0($it, $st);



        //print_r($get_store_its0);
        //die();
        //$sales_quantity = 23;

        foreach ($get_store_its0 as $get_store_its00) {

            echo $sales_quantity;

            //	echo "<br>";



            if ($sales_quantity > 0) {

                if ($get_store_its00->remaining > 0) {

                    if ($get_store_its00->remaining >= $sales_quantity) {

                        //echo "if";
                        //echo "<br>";
                        //insertinsolditems();

                        $this->insertinsolditems($get_store_its00, $sales_quantity, $userid, $inv);



                        //var_dump($get_store_its00)."<br/>";
                        //echo $sales_quantity."<br/>";



                        $sales_quantity = 0;



                        break;
                    } else {

                        //echo "else";
                        //echo "<br>";

                        $sales_quantity = $sales_quantity - $get_store_its00->remaining;

                        //var_dump($get_store_its00)."<br/>";
                        //echo $get_store_its00->remaining."<br/>";



                        $this->insertinsolditems($get_store_its00, $get_store_its00->remaining, $userid, $inv);



                        //insertinsolditems();
                    }
                }
            }
        }



        //die('trs');
    }

    function insertinsolditems($data, $quantity, $userid = null, $inv = null) {

        $insertsoled['store_item_id'] = $data->store_item_id;

        $insertsoled['store_id'] = $data->store_id;

        $insertsoled['item_id'] = $data->item_id;

        $insertsoled['supplier_id'] = $data->supplier_id;

        $insertsoled['soled_quantity'] = $quantity;

        $insertsoled['recipt_id'] = $inv;

        $insertsoled['userid'] = $userid;

        return $this->db->insert('store_soled_items', $insertsoled);
    }

    function dogetsaleItems($id = null) {



        if (post('product')['ids']) {

            $count = 0;





            foreach (post('product')['ids'] as $key => $sale) {



                //echo post('product')['itemid'][$key].','. post('product')['store_id'][$key].','.post('product')['quantity'][$key].','.post('userid');
                //$q=post('product')['quantity'][$key];
                //$q = 9;

                $get_store_its = $this->inventory->get_store_it(post('product')['store_id'][$key], post('product')['itemid'][$key]);



                $this->sold_onebyone(post('product')['itemid'][$key], post('product')['store_id'][$key], post('product')['quantity'][$key], post('userid'), $id);

                /*

                  $quantity=$get_store_its->squan-$get_store_its->soldquant;

                  if($quantity-$q > 0){

                  $fquantity=$q;

                  }else{

                  $fquantity=$quantity-$q;



                  }

                  $this->db->insert('store_soled_items',array('store_id'=>post('product')['store_id'][$key],'soled_quantity'=>$fquantity,'item_id'=>post('product')['itemid'][$key]));





                 */

                $sitem['store_id'] = post('product')['store_id'][$key];

                $sitem['item_id'] = post('product')['ids'][$key];

                $sitem['receipt_id'] = post('product')['invoice_id'][$key];

                $sitem['note'] = post('product')['note'][$key];

                $sitem['quantity'] = post('product')['quantity'][$key];









                $invoice2['confirm_status'] = 1;

                $this->db->where('inovice_product_id', post('product')['ids'][$key]);

                $this->db->update('bs_invoice_items', $invoice2);



                $count++;
            }









            //die();



            if ($count < post('countitem')) {

                $invoice['confirm_status'] = 0;

                $invoice['confirm_complete'] = (post('countitem') - $count);
            } else {

                $invoice['confirm_status'] = 1;

                $invoice['confirm_complete'] = (post('countitem') - $count);
            }





            $this->db->where('invoice_id', post('invoiceid'));

            $this->db->update('an_invoice', $invoice);



            redirect(base_url() . 'inventory/pending_sales_list?e=16');
        } else {

            //redirect(base_url() . 'inventory/pending_sales_list');

            redirect(base_url() . 'inventory/getSalesItems/' . $id . '/?e=17');
        }
    }

    //----------------------------------------------------------------------
    ///pending_Purcahse_list



    function pending_purchase_list($id = '') {

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);

        $this->_data['inside'] = 'pending_purchase_invoice';

        $this->load->view('common/main', $this->_data);



        //  $this->_data['inside2']='pending_purchase_invoice';
        // $this->_data['inside']='add-customer-form';
        //   $this->load->view('common/tabs', $this->_data);
    }

    function getPurcahseItems($id = null) {

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);

        $this->_data['inside'] = 'getPurcahseItems';

        $this->load->view('common/main', $this->_data);
    }

    function dogetPurcahseItems($id = null) {



        if (post('product')['ids']) {

            $count = 0;

            foreach (post('product')['ids'] as $key => $sale) {



                $sitem['store_id'] = post('product')['store_id'][$key];

                $sitem['item_id'] = post('product')['itemid'][$key];

                $sitem['purchase_id'] = post('product')['purchase_id'][$key];

                $sitem['note'] = post('product')['note'][$key];

                $sitem['quantity'] = post('product')['quantity'][$key];

                $sitem['supplier_id'] = post('product')['supplier_id'][$key];





                //print_r($sitem);

                $sitems[] = $this->inventory->inserData('bs_store_items', $sitem);



                //$pitems['confirm_status'] = 1;
                //$this->inventory->inserData('bs_store_items', $pitems);







                $invoice2['confirm_status'] = 1;

                $this->db->where('inovice_product_id', post('product')['ids'][$key]);

                $this->db->update('bs_purchase_items', $invoice2);



                $count++;
            }



            //echo '<hr/>purchaseid:'. post('purchaseid');
            //$invoice['status'] = 1;
            //$invoice['purchases_status'] = 1;
            //$invoice['purchase_status'] = 1;
            //echo 'c'.$count."<br/>";
            //echo post('countitem');

            if ($count < post('countitem')) {

                $invoice['confirm_status'] = 0;

                $invoice['confirm_complete'] = (post('countitem') - $count);
            } else {

                $invoice['confirm_status'] = 1;

                $invoice['confirm_complete'] = (post('countitem') - $count);
            }



            //var_dump($invoice);

            $this->db->where('purchase_id', post('purchaseid'));

            $this->db->update('an_purchase', $invoice);

            redirect(base_url() . 'inventory/pending_purchase_list?e=15');
        } else {

            redirect(base_url() . 'inventory/getPurcahseItems/' . $id . '/?e=17');
        }
    }

    //----------------------------------------------------------------------
    ///pending_Purcahse_list



    function done_transfer_items($t_id) {


        $this->_data['trasnfer_itemids'] = $this->inventory->getTransferListItemsIds($t_id);
        $this->_data['inside'] = 'done_transfer_items';

        //pending_sales_list

        $this->load->view('common/main', $this->_data);
    }

    function done_transfer() {



        $this->_data['inside'] = 'done_transfer';

        //pending_sales_list

        $this->load->view('common/main', $this->_data);
    }

    function pending_transfer($id = null) {
        $this->_data['inside'] = 'pending_transfer';
        $this->_data['inside2'] = 'transfer_list';
        if ($id) {
            $this->_data['transfer_data'] = $this->inventory->get_transfer_list($id);
        }
        $this->load->view('common/tabs', $this->_data);
    }

    function save_transfer() {
        if (post()['transfer_id']) {//update
            foreach (post('product')['ids'] as $key => $sale) {
                if (post('product')['lost'][$key]) {
                    $deduct = (floatval(post('product')['lost'][$key]) / 100) * floatval(post('product')['quantity'][$key]);
                    $olddeduct = (floatval(post('product')['oldlost'][$key]) / 100) * floatval(post('product')['oldquantity'][$key]);
                    $store_id = post('from_store_id');
                    $item_id = post('product')['ids'][$key];
                    $data = array(
                        'percent' => post('product')['lost'][$key],
                        'quantity' => $deduct,
                    );

                    $this->db->where(array('transfer_id' => post()['transfer_id'], 'raw_id' => $item_id));
                    $this->db->update('lossed_items', $data);
                    $query = "update store_value set totalq = totalq+$olddeduct-$deduct where store_id = $store_id and item_id = $item_id";
                    $this->db->query($query);
                }
                $tData['transfer_id'] = post()['transfer_id'];
                $tData['transfer_qty'] = post('product')['quantity'][$key];
                $this->db->insert('transfer_items', $tData);
                $this->inventory->updateStoreValue(array('store_id' => post('to_store_id'), 'quantity' => post('product')['quantity'][$key], 'item_id' => post('product')['ids'][$key], 'transfer' => '1', 'fromstore' => post('from_store_id'), 'oldquantity' => post('product')['oldquantity'][$key]));
            }
        } else {//add new
            $this->db->query('SET SQL_BIG_SELECTS=1');
            $userid = $this->session->userdata('bs_userid');

            $transferData['transfer_from'] = post('fstore_id');
            $transferData['transfer_to'] = post('tstore_id');
            $transferData['user_id'] = $userid;
            $this->db->insert('transfer', $transferData);
            $transferid = $this->db->insert_id();

            if (post('product')['ids']) {
                $count = 0;
                foreach (post('product')['ids'] as $key => $sale) {
                    $sitem['store_id'] = post('tstore_id');
                    $sitem['item_id'] = post('product')['ids'][$key];
                    $sitem['note'] = post('product')['note'][$key];
                    $sitem['quantity'] = post('product')['quantity'][$key];
                    $sitem['transfer'] = '1';
                    $sitem['fromstore'] = post('fstore_id');
                    $q = post('product')['quantity'][$key];

                    //$get_store_its = $this->inventory->get_store_it(post('fstore_id'), post('product')['ids'][$key]);
                    $get_store_its = $this->inventory->get_store_it(post('product')['store_id'][$key], post('product')['itemid'][$key]);
                    $this->sold_onebyone(post('product')['ids'][$key], post('fstore_id'), post('product')['quantity'][$key], $userid);
                    $quantity = $get_store_its->squan - $get_store_its->soldquant;
                    $fquantity = $q;

                    //$this->db->insert('store_soled_items', array('store_id' => post('fstore_id'), 'soled_quantity' => $fquantity, 'item_id' => post('product')['ids'][$key]));
                    if (post('product')['lost'][$key]) {
                        $deduct = (floatval(post('product')['lost'][$key]) / 100) * floatval($fquantity);
                        $store_id = post('fstore_id');
                        $item_id = post('product')['ids'][$key];
                        $data = array(
                            'raw_id' => $item_id,
                            'store_id' => $store_id,
                            'percent' => post('product')['lost'][$key],
                            'quantity' => $deduct,
                            'transfer_id' => $transferid
                        );
                        $this->db->insert('lossed_items', $data);
                        $query = "update store_value set totalq = totalq-$deduct where store_id = $store_id and item_id = $item_id";
                        $this->db->query($query);
                    }
                    $this->inventory->updateStoreValue(array('store_id' => post('tstore_id'), 'quantity' => $fquantity, 'item_id' => post('product')['ids'][$key], 'transfer' => '1', 'fromstore' => post('fstore_id'), 'oldquantity' => 0));
                    $this->db->insert('raw_store_items', array('store_id' => post('tstore_id'), 'quantity' => $fquantity, 'item_id' => post('product')['ids'][$key], 'transfer' => '1', 'fromstore' => post('fstore_id')));
                    $storeitemid = $this->db->insert_id();

                    $tData['transfer_id'] = $transferid;
                    $tData['store_item_id'] = $storeitemid;
                    $tData['transfer_qty'] = $fquantity;
                    $this->db->insert('transfer_items', $tData);

                    /*
                      $invoice2['confirm_status'] = 1;
                      $this->db->where('inovice_product_id', post('product')['inovice_product_id'][$key]);
                      $this->db->update('bs_purchase_items', $invoice2);
                      $count++;
                     */
                }
                /* if($count < post('countitem')){
                  $invoice['confirm_status'] = 0;
                  $invoice['confirm_complete'] = (post('countitem')-$count);
                  }else{
                  $invoice['confirm_status'] = 1;
                  $invoice['confirm_complete'] = (post('countitem')-$count);
                  } */

                //$this->db->where('purchase_id', post('purchaseid'));
                //$this->db->update('an_purchase', $invoice);
                redirect(base_url() . 'inventory/print_invoice/' . $transferid . '');
            } else {
                redirect(base_url() . 'inventory/pending_transfer?e=17');
            }
        }
    }

    function print_invoice($id) {
        $trasnfer_itemids = $this->inventory->getTransferListItemsIds($id);
        $this->_data['t_data'] = $this->inventory->getTransferByDataId($id);
        $this->_data['transfer_data'] = $this->inventory->getTransferPrint($trasnfer_itemids->totalitems);
        $this->load->view('printed-transfer', $this->_data);
    }

    function getallitems($par = null, $storevalue = null) {





        //$this->_data['par'] = $par;

        if ($par == 'p') {

            $this->_data['par'] = $par;
        } else {

            $this->_data['par'] = '';
        }





        if ($par != '' && $storevalue == '') {

            $this->_data['storevalue'] = $par;
        } elseif ($par != '' && $storevalue != '') {

            $this->_data['storevalue'] = $storevalue;
        }



        //echo $this->_data['par'];
        //die();

        $this->load->view('getallitems', $this->_data);
    }

    function additem_wi() {
        
    }

    function views($id) {

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);

        $this->_data['inside'] = 'sviews';

        $this->load->view('common/main', $this->_data);
    }

    function printed($id) {

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);
        //$this->_data['inside'] = 'sviews';

        $this->load->view('printed', $this->_data);
    }

    function views2($id) {

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);

        $this->_data['inside'] = 'sviews2';

        $this->load->view('common/main', $this->_data);
    }

    function printed2($id) {

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);
        //$this->_data['inside'] = 'sviews';

        $this->load->view('printed2', $this->_data);
    }

    /*     * *mahmoud* */

    public function add_finisheditem() {
        $id = $this->input->post('itemid');
        $path = APPPATH . '../uploads/product_images';

        $file_name = upload_file('product_picture', $path, true, 50, 50);

        if ($file_name != '') {

            $data['product_picture'] = $file_name;
        }


        $this->inventory->add_raw_material($file_name, $id);
        redirect('inventory/finishgoods');
        //echo '<script>$("#tab2").tab()</script>';
    }

    public function assign_dishes($id = null) {

        $this->_data['inside'] = 'assign_dishes';
        $this->_data['inside2'] = 'assign_dishes_grid';
        $this->_data['items'] = $this->inventory->juiceproduct_list();
        $this->_data['raw_material'] = $this->inventory->raw_material_list();
        $this->_data['item_data'] = $this->inventory->get_item_data($id);
        $this->_data['item_id'] = $id;


        /* $this->_data['raw_material'] = array_filter($this->_data['raw_material'], function ($e) {
          foreach ($this->_data['item_data'] as $key => $value) {
          $id = $value->raw_id;
          if($e->id == $id){
          echo '1';
          }
          }
          }); */



        //print_r($neededObjects);
        //$this->_data['additional_items'] = $this->inventory->getAdditionalItems();


        $this->load->view('common/tabs', $this->_data);
    }

    public function add_assign_dishes($id = null) {

        //$addi_items = post('addi_items');
        $price = post('price');
        $arr = $price;
        $this->inventory->add_assign_dishes($arr, $id);
        redirect("inventory/assign_dishes?e=10");
    }

}
