<script>
    function setDefault(radio, type) {
        if (radio.checked) {
            $.ajax({
                url: config.BASE_URL + "ajax/setDefaultStore",
                dataType: 'json',
                type: 'post',
                data: {store_id: radio.value, type: type},
                cache: false,
                success: function (data) {
                    if (data) {
                        alert('تم الحفظ');
                    }
                }
            });
        }
    }
</script>
<div class="table-wrapper users-table">
    <form action="<?php //echo form_action_url('delete_customers');      ?>" id="listing" method="post" autocomplete="off">
        <!--            <div class=" head">
                        <div class="">
                            <h4>
        
                                <div class="title title alert blue"> <span><?php breadcramb(); ?></span> <span class="icon icon-infographic left"></span></div>
        
        
                            </h4>
                            
                        </div>
                    </div>-->
        <?php error_hander($this->input->get('e')); ?>
        <style>
            .href{


                height: 30px;
                padding: 6px 10px;
                margin: 5px 0 5px 0;
                font-size: 12px;
                line-height: 16px;
                border: 1px solid #979797;
                border-radius: 3px;
                background-color: #f5f5f5;
                text-shadow: 0 1px 0 #fff;
                box-shadow: inset 0 1px 1px #fff;
                -webkit-transition: border .3s linear, box-shadow .25s ease;
                -moz-transition: border .3s linear, box-shadow .25s ease;
                -o-transition: border .3s linear, box-shadow .25s ease;
                transition: border .3s linear, box-shadow .25s ease;


            }
        </style>

        <div class="">
            <div class="">
                <?php //action_buttons('add_store',$cnt); ?>  
                <a href="<?php echo base_url() ?>inventory/additem_wi" class="href gray flt-r g2"> &nbsp;&nbsp; Add item &nbsp;&nbsp;  <i class="icon-addtags"></i></a>
                <table id="new_data_table">
                    <form action="<?php echo form_action_url('bulk_delete_stores'); ?>" id="listing" method="post" autocomplete="off">
                        <thead class="thead">
                            <tr>
                                <th  id="no_filter"><label for="checkbox"></label><?php echo lang('All') ?> </th>
                                <th ><?php echo lang('Store') ?></th>
                                <th ><?php echo lang('Company-Name') ?></th>
                                <th ><?php echo lang('branch-Name') ?></th>
                                <th ><?php echo lang('Store-Manager') ?></th>
                                <th ><?php echo lang('Manager-Phone') ?></th>
                                <th ><?php echo lang('Email-Address') ?></th>
                                <th ><?php echo "Total Purchase"; ?></th>
                                <th ><?php echo "Total Sales"; ?></th>
                                <th ><?php echo lang('Total-Qunatity-Available') ?></th>
                                <th ><?php echo lang('default_hot') ?></th>
                                <th ><?php echo lang('default_cold') ?></th>
                                <th  id="no_filter">&nbsp;</th>
                            </tr>
                        </thead>

                        <?php
                        $cnt = 0;
                        foreach ($this->inventory->storelist() as $st) {

                            $cnt++;
                            $com = json_decode($st->company_name, TRUE);
                            $en = $com['en'];
                            $ar = $com['ar'];
                            ?>
                            <tr>
                                <td ><input type="checkbox" class="allcb input-resize" name="u[]" id="u_<?php echo $st->storeid; ?>" value="<?php echo $st->storeid; ?>"/></td>
                                <td ><a href="<?php echo base_url(); ?>inventory/storeItems/<?php echo $st->storeid; ?>"><?php echo _s($st->storename, get_set_value('site_lang')); ?></a></td>
                                <td><?php echo $en; ?>|<?php echo $ar; ?></td>
                                <td><?php //echo $st->branchname;      ?><?php echo _s($st->branchname, get_set_value('site_lang')); ?></td>
                                <td><?php echo $st->storemanager; ?></td>
                                <td><?php echo $st->managerphone; ?></td>

                                <td><a href="mailto:<?php echo $st->email; ?>"><?php echo $st->email; ?></a></td>
                                <td><?php echo $st->totalstpurchase; ?></td>

                                <td><?php echo $st->totalstsales; ?></td>
                                <td>
                                    <?php
                                    if ($st->remainingquantity) {
                                        echo ($st->remainingquantity);
                                        //echo $st->sumquan;
                                    } else {
                                        echo "0";
                                    }
                                    ?> 
                                </td>
                                <td><input type="radio" name="default_hot" <?= ($st->default_hot == 1) ? 'checked' : '' ?> onchange="setDefault(this, 'hot')" value="<?php echo $st->storeid; ?>"></td>
                                <td><input type="radio" name="default_cold" <?= ($st->default_cold == 1) ? 'checked' : '' ?> onchange="setDefault(this, 'cold')" value="<?php echo $st->storeid; ?>"></td>
                                <td ><?php edit_button('add_store/' . $st->storeid); ?> 
                                    <!--<a href='javascript:void' id="<?php echo $st->storeid; ?>" class='basic'><img src="<?php echo base_url(); ?>images/internal/more_info.png" width="24" height="16" border="0" /></a>-->

                                    <!-- modal content -->
                                    <!--
                                                                            <div id="basic-modal-content" class="dig<?php echo $st->storeid; ?>">
                                                                                <h3><?php echo $st->storename; ?></h3>
                                                                                line 
                                                                                <code> <span class="pop_title"><?php echo lang('Location-Address') ?> : </span>
                                                                                    <div class="pop_txt"><?php echo $st->location_name; ?><br /><?php echo $st->address; ?></div>
                                                                                </code> 
                                                                                line 
                                                                                <code> <span class="pop_title"><?php echo lang('Finacial-Type') ?> : </span>
                                                                                    <div class="pop_txt"><?php echo $st->financial; ?></div>
                                                                                </code> 
                                                                                line 
                                                                                <code> <span class="pop_title"><?php echo lang('Rent-Value') ?> : </span>
                                                                                    <div class="pop_txt"><?php echo $st->rent; ?></div>
                                                                                </code> 
                                                                                line 
                                                                                <code> <span class="pop_title"><?php echo lang('Per') ?> : </span>
                                                                                    <div class="pop_txt"><?php echo $st->rent_per; ?> <?php per_value('', '', $st->rent_week); ?>.</div>
                                                                                </code> 
                                                                                line 
                                                                                <code> <span class="pop_title"><?php echo lang('Date-Of-Paid-Rent') ?> : </span>
                                                                                    <div class="pop_txt"><?php echo date('d F Y', strtotime($st->dateofpaid)); ?></div>
                                                                                </code> 
                                                                                line 
                                                                                <code> <span class="pop_title"><?php echo lang('Owner-Phone') ?>  : </span>
                                                                                    <div class="pop_txt"><?php echo $st->ownerphone; ?></div>
                                                                                </code> 
                                                                                line 
                                                                                <code> <span class="pop_title"><?php echo lang('Store-Manager') ?> : </span>
                                                                                    <div class="pop_txt"><?php echo $st->storemanager; ?></div>
                                                                                </code> 
                                                                                line 
                                                                                <code> <span class="pop_title"><?php echo lang('Manager-Phone') ?> :</span>
                                                                                    <div class="pop_txt"><?php echo $st->managerphone; ?></div>
                                                                                </code> <code> <span class="pop_title"><?php echo lang('Store-Phone-Number') ?> :</span>
                                                                                    <div class="pop_txt"><?php echo $st->storenumber; ?></div>
                                                                                </code> 
                                                                                line 
                                                                                <code> <span class="pop_title"><?php echo lang('Fax') ?> :</span>
                                                                                    <div class="pop_txt"><?php echo $st->fax; ?></div>
                                                                                </code> 
                                                                                line 
                                                                                <code> <span class="pop_title"><?php echo lang('Email-Address') ?> :</span>
                                                                                    <div class="pop_txt"><?php echo $st->email; ?></div>
                                                                                </code> 
                                                                                line 
                                                                                <code> <span class="pop_title"><?php echo lang('Capcity') ?> :</span>
                                                                                    <div class="pop_txt"><?php echo $st->capcity; ?> <?php per_unit('', '', $st->capcity_unit); ?></div>
                                                                                </code> 
                                                                                line 
                                                                                <code> <span class="pop_title"><?php echo lang('Note') ?> :</span>
                                                                                    <div class="pop_txt"><?php echo $st->notes; ?></div>
                                                                                </code> 
                                                                                line 
                                    
                                                                            </div>-->

                                </td>
                            </tr>
                        <?php } ?>
                    </form>
                </table>
            </div>
        </div>
        <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>

        <!-- END PAGE --> 
</div>

