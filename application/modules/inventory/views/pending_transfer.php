<script type="text/javascript">
    function swapme2() {
        pid = $("#productid").val();
        pname = $("#productname").val();
        $("#productname").val(pname);
        $("#productid").val(pid);
    }

    $(document).ready(function () {
        $("#category_id").change(function () {
            val = $(this).val();
            $("#productname").trigger('keydown');
        });
        setInterval('checkData()', 1000);
        storeId = $("#store_id").val();

        $(function () {
            $("#productname").autocomplete({
                source: function (request, response) {
                    var sUrl = "<?php echo base_url(); ?>ajax/ajaxAutocomplete/" + $('#fstore_id').val();
                    $.getJSON(sUrl, request, function (result) {
                        response(result);
                    });
                },
                select: function (event, ui) {
                    $("#productid").val(ui.item.id);
                    $("#totalq").val(ui.item.totalq);
                    tt = ui.item;
                    getProductData(ui.item.id, ui.item.totalq, $('#fstore_id').val());
                    setTimeout('swapme2()', 500);
                }
            });
        });

        $("#productname").focus(function () {
            $("#productname").trigger('keydown');
        });



    });
    function checkData() {
        plen = allProductsData.length;
        if (plen > 0) {
            $('#customername').attr("disabled", true);
        }
        else {
            $('#customername').attr("disabled", false);
        }
    }

    var allProductsData = new Array();
    var allSalesData = new Array();
    totalAmountexComision = '';
    totalnetAmountexComision = '';
    $(document).keypress(function (e) {
        if (e.which == 13) {
            //   alert('You pressed enter!');
            if ($(".currentProduct").is(":focus")) {
                //  alert('You pressed enter!');
                if ($("#productid").val() != "")
                {
                    addItem();
                }
            }
            else {
                //alert('not');
            }
        }
    });


    netTotals = new Array(); /// for keep rocrds net total for each produc
    Totalsmin = new Array(); /// for keep rocrds net total for each produc
    rowCounter = 0; /// counter for table products

    netCouner = 0; // counter for net totals
    tbHtml = ''; // for adding new row in the table 

    editArray = new Array(); // edit any product fromm tab;e 



    total_value = 0; // total cacluate value



    productData = {}; // array for current working product



    counter = 0; // counter for products in the hiddden

</script> 



<div id="pad-wrapper">
    <div class="row form-wrapper">
        <div class=" col-xs-12">

            <div class="title title alert blue">
                <span><?php echo lang('mess_inv_4') ?> </span><span class="icon icon-infographic left" ></span>
            </div>
            <?php error_hander($this->input->get('e')); ?>
            <?php //$get_product = $this->inventory->getSalesInvoiceItems($id); ?>
            <?php $get_product = array(); ?>
            <div id="main-content" class="main_content ">
                <form name="" action="<?php echo base_url() ?>inventory/save_transfer" method="post">
                    <!--/<?php echo $id ?>-->

                    <div class="g3 form-group">
                        <label class=""><?php echo lang('fStore'); ?>   :  </label>

                        <?php store_dropbox2('fstore_id', $transfer_data[0]->transfer_from, '', $transfer_data[0]->transfer_id); ?>

                    </div>
                    <div class="g3 form-group">
                        <label class=""><?php echo lang('tStore') ?>   : </label>
                        <?php store_dropbox2('tstore_id', $transfer_data[0]->transfer_to, '', $transfer_data[0]->transfer_id); ?>

                    </div>


                    <div class="g3 form-group">
                        <label class="text-warning"><?php echo lang('Product-Name') ?> :</label>
                        <br>
                        <input  type="hidden" name="productid"  id="productid" class="formControl"/>
                        <input type="text" name="productname" <?= ($transfer_data[0]->transfer_id) ? "disabled" : '' ?> id="productname" class="form-control "/>
                        <input type="hidden" name="totalq"  id="totalq" class="form-control "/>
                        <input type="hidden" name="transfer_id" value='<?= $transfer_data[0]->transfer_id ?>' id="transfer_id" class="form-control "/>
                        <input type="hidden" name="from_store_id" value='<?= $transfer_data[0]->transfer_from ?>' class="form-control "/>
                        <input type="hidden" name="to_store_id" value='<?= $transfer_data[0]->transfer_to ?>' class="form-control "/>
                    </div>

                    <!--                    <div class="g2 form-group">
                                            <label class="text-warning"><?php $this->session->userdata('site_lang'); //echo get_set_value('site_lang');                   ?>  اضافة المنتج</label>
                                            <a href="#" onclick="addItemT()" style="float: right;margin-top: 10px;"><i class="icon-addtocart left" style="font-size:20px;"></i></a>
                                        </div>-->




                    <input type="hidden" name="invoiceid" value="<?php echo $get_product[0]->invoice_id ?>"/>
                    <br clear="all"/>
                    <br clear="all"/>
                    <br clear="all"/>

                    <style>
                        #table-pend{background-color: white !important;}
                        #table-pend tr {background-color: white !important;}
                    </style>


                    <table class="table table-bordered invoice-table mb20" id="table-pend" style="text-align:center !important">
                        <thead style="text-align:center !important">

                        <th><?php echo lang('code') ?></th>
                        <th><?php echo lang('Product-Name') ?></th>
                        <th><?php echo lang('Image') ?></th>
                        <th><?php echo lang('store_quantity') ?></th>
                        <th><?php echo lang('Unit') ?></th>
                        <th><?php echo lang('Quantity') ?></th>
                        <th><?php echo lang('lost') ?></th>
                        <th><?php echo lang('Quantity') ?></th>

<!--                        <th><?php echo lang('Discription') ?></th>-->
                        <th><?php echo lang('pprice') ?></th>
<!--                        <th><?php echo lang('notes') ?></th>-->
                        <th><?php //echo lang('notes')                   ?></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($transfer_data) {
                                foreach ($transfer_data as $value) {
                                    ?>
                                    <tr id="rti<?= $value->raw_id ?>">
                                        <td><input type="hidden" class="" name="product[ids][]"  value="<?= $value->raw_id ?>" />  <?= $value->raw_id ?> </td>
                                        <td><input type="text" class="" name="product[itemname][]"  value="<?= _s($value->itemname, get_set_value('site_lang')); ?>" /></td>
                                        <td></td>
                                        <td><?= $value->totalq1 ?></td>
                                        <td><?=  _s($value->unit_title, get_set_value('site_lang')); ?></td>
                                        <td><input type="text" class="trtr" name="product[quantity][]" value="<?= $value->transfer_qty ?>" onkeyup="changeQuantity(this,<?= $value->totalq1 ?>)" id="u_quantity_<?= $value->raw_id ?>"  />
                                        <td style="display: none"><input type="hidden" name="product[oldquantity][]" value="<?= $value->transfer_qty ?>"   />
                                        <td><input type="text" style="width:90%" value="<?= $value->percent ?>" name="product[lost][]" onkeyup="calcLost(this, <?= $value->raw_id ?>)" placeholder="الخسائر بالنسبة" > %
                                        <td style="display: none"><input type="hidden" value="<?= $value->percent ?>" name="product[oldlost][]"  > </td>
                                        <td><span id="sp_<?= $value->raw_id ?>"><?= $value->losed_qty ?></span></td>
                                        <td><input type="hidden" class="purchase_price" name="product[purchase_price][]" value="<?= $value->price ?>" /><?= $value->price ?></td>
                                        <td></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            <tr  class="grand-total"><td colspan="11"></td></tr>
                        </tbody>
                    </table>
                    <div class="g16" style="margin-right: 1%;border: 2px dotted white;height: 4px;">

                    </div>
                    <div class="g10" style="">

                    </div>
                    <div class="g2" style="">
                        <?php echo lang('Total-quantity') ?> : <span id="all_quant"></span>
                    </div>
                    <div class="g2" style="">
                        <?php echo lang('count-type') ?>  : <span id="count"></span>
                    </div>
                    <div class="g2" style="">
                        <?php echo lang('Total-Price') ?> :   <span id="all_price"></span>
                    </div>
                    <div class="field-box raw" align="">

                        <input name="sub_mit" id="sub_mit" type="submit" class="green flt-r g2" value="<?php echo lang('save') ?>" />

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>    

<script>
    $(document).ready(function () {

        $("#get_all_quant").html($("#all_quant").text());
        $("#get_all_price").html($("#all_price").text());

        $(".pod").select();

    });
</script> 