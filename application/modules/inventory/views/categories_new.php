
<div id="pad-wrapper">
    <div class="row form-wrapper">
        <div class="col-md-12 col-xs-12">



            <div id="main-content" class="main_content">
                <div class="title title alert alert-info">
                    <span><?php echo lang('add-edit') ?> <span class="icon icon-fire" style="float: left;"></span></span>
                </div>

                <div class="notion title title alert alert-info">*<?php echo lang('mess1') ?> <span class="icon icon-fire" style="float: left;"></span></div>


                <form action="<?php echo form_action_url('categories'); ?>" method="post" id="frm_category" name="frm_category" autocomplete="off">
                    <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
                    <input type="hidden" name="catid" id="catid" value="<?php echo $cat->catid; ?>" />
                    <div class="form mycontent"> 
                        <div class="g4">
                            <label class=""><?php echo lang('Category-Name-ar') ?></label>
                            <div class="">
                                <input name="catname[arabic]" class="form-control" id="catname" value="<?php echo _s($cat->catname,"arabic"); ?>" data-msg="Enter Category" type="text" />
                            </div>
                        </div>
                        <div class="g4">
                            <label class=""><?php echo lang('Category-Name-en') ?></label>
                            <div class="">
                                <input name="catname[english]" class="form-control" id="catname" value="<?php echo _s($cat->catname,"english"); ?>" data-msg="Enter Category" type="text" />
                            </div>
                        </div>
                        <!--<div class="g4">
                            <label class=""><?php echo lang('Status') ?></label>
                            <div class="">
                                <div class="ui-select">
                                    <div class="">
                                        <?php //get_statusdropdown($cat->catstatus, 'catstatus'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                        <div class="g4">

                            <?php
                                $act[] = 'A';
                                $act[] = 'D';
                            ?>
                            <select class="pod has-search" name="catstatus" id="catstatus" style="">
                                <option value="">Select</option>
                                <?php
                                    foreach($act as $ac){
                                        if($ac == 'A'){
                                            $selected = '';
                                            if($cat->catstatus == 'A'){
                                                $selected = "selected";
                                            }

                                            ?>
                                            <option value="A" <?php echo $selected; ?>>فعال</option>
                                            <?php
                                        }
                                        else{
                                            $selected = '';
                                            if($cat->catstatus == 'D'){
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="D" <?php echo $selected; ?>>غير فعال</option>
                                        <?php
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <br clear="all"/>
                        <div class="col-lg-12">
                            <label class=""><?php echo lang('Description') ?></label>                        <br clear="all"/>
                            <div class="">
                                <textarea name="catdescription" id="catdescription" class="formareafield ckeditor  form-control"><?php echo $cat->catdescription; ?></textarea>
                            </div>
                        </div>
                        
                        <br clear="all"/>
                        <div class="raw field-box" align="">
                            <input name="sub_mit" id="sub_mitx" type="submit" class="green flt-r g2 submit_btn" value="<?php echo lang('Add') ?>" />
                            <input name="sub_reset" type="reset" class="green flt-r g2 reset_btn" value="<?php echo lang('Reset') ?>" />
                        </div>
                        <!--end of raw field-box--> 
                    </div>
                </form>
            </div>
            <!-- END PAGE --> 
        </div>
    </div>
</div>
