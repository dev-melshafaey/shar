
<style>
    td,th{
        font-size: 12px;
    }
</style>
<script>
    function deletedishes(item_id) {
        //window.location.href = config.BASE_URL + "inventory/assign_dishes?e=10";
        var flag = confirm('هل تريد الحذف');
        if (flag) {
            $.ajax({
                url: config.BASE_URL + "ajax/deletedishes/",
                dataType: 'json',
                type: 'post',
                data: {id: item_id},
                cache: false,
                success: function (data) {
                    //alert(data);
                    if(data){
                    $('#row_'+item_id).remove();
                }}
            });
            
        }
    }
    $(function () {
        $("#dialog").dialog({
            autoOpen: false
        });

        $(".opener").on("click", function () {
            $("#dialog").dialog("open");
        });
        $('.ui-button-text').text('');
    });
    function getContents(id) {
        $('#tbody').html('');
        $.ajax({
            url: config.BASE_URL + "ajax/get_dish_content",
            dataType: 'json',
            type: 'post',
            data: {id: id},
            cache: false,
            success: function (data) {               
                for (var i = 0; i < data.length; i++) {
                    $('#tbody').append('<tr><td>' + data[i].qty + '</td><td>' + data[i].unit_title + '</td><td>' + data[i].auto_name + '</td></tr>');
                }

                $("#dialog").dialog("open");
            }
        });

    }
</script>
<div id="dialog" title="">
    <table>
        <thead>
            <tr>
                <th>الكمية</th>
                <th>الوحدة</th>
                <th>الصنف</th>
            </tr>
        </thead>
        <tbody id="tbody">

        </tbody>
    </table>
</div>

<div id="pad-wrapper">

    <div class="table-wrapper users-table ">
        <form action="<?php //echo form_action_url('delete_customers');                 ?>" id="listing" method="post" autocomplete="off">
            <div class=" head">
                <div class="">
                    <h4>

                        <div class="title"> <span><?php //breadcramb();               ?></span> </div>


                    </h4>

                </div>
            </div>



            <div class="">
                <div class="">
                    <?php //action_buttons('add_item', $cnt); ?>   
                    <table id="new_data_table">
                        <form action="<?php //echo form_action_url('bulk_delete');                 ?>" id="listing" method="post" autocomplete="off">
                            <thead class="thead">
                                <tr>
                                    <th  id="no_filter"><label for="checkbox"></label>
                                        <?php echo lang('All') ?></th>
                                    <th ><?php echo lang('Name') ?></th>
                                    <th >English<?php ?></th>
                                    <th>Type</th>
<!--                                    <th  ><?php echo lang('Category-Name') ?></th>-->
                                    <?php
                                    if ($this->session->userdata('bs_memtype') != 8) {
                                        ?>

                                        <th ><?php echo lang('size') ?> </th>
                                        <th ><?php echo lang('contents') ?> </th>
    <!--                                        <th ><?php echo lang('Large-price') ?> </th>
                                        <th ><?php echo lang('Medium-price') ?> </th>
                                        <th ><?php echo lang('Small-price') ?> </th>-->
                                        <?php
                                    }
                                    ?>
                                    <th ><?php echo lang('Date') ?></th>
                                    <!--<th width="6%">Quantity</th>-->
                                    <th  id="no_filter">&nbsp;</th>
                                </tr>
                            </thead>

                            <?php
                            $cnt = 0;
                            $data = $this->inventory->get_dish_content();
                            if ($data) {
                                foreach ($data as $inv) {
                                    $cnt++;
                                    ?>
                            <tr id="row_<?php echo $inv->item_id ?>">
                                        <td ><input type="checkbox" class="allcb" name="u[]" id="u_<?php echo $inv->itemid; ?>" value="<?php echo $inv->itemid; ?>" /></td>
                                        <td ><a href="<?php echo base_url() ?>inventory/history/<?php echo $inv->itemid; ?>">
                                                <?php //echo $inv->itemname; ?>
                                                <?php echo _s($inv->itemname, 'arabic'); ?>
                                            </a></td>
                                        <td><?php echo _s($inv->itemname, 'english'); ?></td>
                                        <td><?php echo $inv->categ_type; ?></td>
        <!--                                    <td><?php echo _s($inv->catname, get_set_value('site_lang')) ?></td>-->
                                        <?php
                                        if ($this->session->userdata('bs_memtype') != 8) {
                                            ?>
                                            <td><?php echo $inv->size; ?></td>
                                            <td> <a href="javascript:void" data-toggle="" onclick="getContents('<?php echo $inv->item_id; ?>')"><i class="icon-th-list left" style="font-size:20px;color:black"></i></a>

                                                <?php /* foreach($inv->contents as $item){
                                                  echo $item->qty." ".$item->unit_title." "._s($item->item_name,'arabic')."<br>";
                                                  } */ ?>
                                            </td>


                                            <?php
                                        }
                                        ?>
                                        <td ><?php echo date('Y-m-d', strtotime($inv->created_at)); ?></td>

                                        <td>
                                            <?php
                                            if ($udata['owner_id'] == 0) {
                                                edit_button('assign_dishes/' . $inv->item_id);
                                            }
                                            ?>
                                            <a href="javascript:void(0)"  onclick="deletedishes('<?php echo $inv->item_id ?>')"><img src="<?php echo base_url(); ?>images/1456404526_document_text_cancel.png"  style="width:22px;"/></a>

                                                                        <!--<a href='javascript:void'  id='<?php echo $inv->itemid; ?>' class='basic'><img src="<?php echo base_url(); ?>images/internal/more_info.png" width="24" height="16" border="0" /></a>-->

                                            <!-- modal content -->

                                            <div id="basic-modal-content" class="dig<?php echo $inv->itemid; ?>" style="display: none">
                                                <h3><?php echo $inv->itemname; ?><br />
                                                    <?php echo lang('Serial-Number') ?> : #<?php echo $inv->serialnumber; ?></h3>
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Category-Name') ?> : </span>
                                                    <div class="pop_txt"><?php echo $inv->catname; ?></div>
                                                </code> 
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Type') ?> : </span>
                                                    <div class="pop_txt"><?php product_type('', '', $inv->itemtype); ?></div>
                                                </code> 
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Store') ?> : </span>
                                                    <div class="pop_txt"><?php echo $inv->storename; ?></div>
                                                </code> 
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Purchase-Price') ?> : </span>
                                                    <div class="pop_txt"><?php echo $inv->purchase_price; ?> RO.</div>
                                                </code> 
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Sale-Price') ?>  : </span>
                                                    <div class="pop_txt"><?php echo $inv->sale_price; ?> RO.</div>
                                                </code> 
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Min-Sale-Price') ?> : </span>
                                                    <div class="pop_txt"><?php echo $inv->min_sale_price; ?> %</div>
                                                </code> 
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Point-of-Re-order') ?> : </span>
                                                    <div class="pop_txt"><?php echo $inv->point_of_re_order; ?></div>
                                                </code> 
                                                <!--line--> 
                                                <code> <span class="pop_title"><?php echo lang('Quantity') ?> :</span>
                                                    <div class="pop_txt"><?php echo $inv->quantity; ?></div>
                                                </code> 
                                                <!-- <code> <span class="pop_title">Note :</span>
                                                <div class="pop_txt">We can't Reply The Supplier Because He Is Cheet  Us</div>
                                                </code> -->
                                                <!--line--> 

                                            </div></td>
                                    </tr>
                                </form>
                                <?php
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
            <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>

    </div>
    <!-- END PAGE --> 
</div>
