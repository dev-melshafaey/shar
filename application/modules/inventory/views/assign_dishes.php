
<script>


    removeArray = new Array();

    function check_status(obj, ind, linked, linkprice, unit_title) {
        checkstatus = $(obj).is(':checked');
        if (checkstatus) {
            $("#price" + ind).show();
            $("#link_id" + ind).show();
            htm = '<input type="hidden" id="link_id' + ind + '" name="link_id[]" value="' + linked + '">';
            htm += '<input type="text" class="price_c" id="price' + ind + '" name="price[]" value="' + linkprice + '" placeholder="الكمية"  style="width: 50%;">';
            htm += "<span>" + unit_title + "</span>";
            childhtm = $("#child" + ind).html();
            if (childhtm != "") {
                $("#child" + ind).html(htm);
            }
            else {
                $("#child" + ind).html(htm);
            }
            if (linked != "") {
                removeArray.splice(linked, 1);

            }

        }
        else {
            $("#child" + ind).html('');
            if (linked != "") {
                removeArray[linked] = linked;

                $('#removeLinked').val(JSON.stringify(removeArray)); //store array

            }
            $("#price" + ind).hide();
            $("#link_id" + ind).hide();
        }
    }



    function searchForItem(inp) {
        $('.mydiv').hide();
        var txt = inp.value;
        $('.mydiv').each(function () {
            if ($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1) {
                $(this).show();
            }
        });
    }

</script>
<div id="main-content" class="main_content ">


    <?php error_hander($this->input->get('e')); ?>


    <form action="<?php echo form_action_url('add_assign_dishes/' . $item_id); ?>" method="post" id="frm_item" name="frm_item" autocomplete="off" enctype="multipart/form-data" class="sample-form">

        <div class="form mycontent">



            <div class="g3">

                <select <?= ($item_id) ? 'disabled' : '' ?> class="js-example-basic-single region_class required  valid" id="itemid" name="itemid">
                    <?php foreach ($items as $item): ?>
                        <option <?= ($item->itemid == $item_id) ? 'selected' : '' ?> value="<?php echo $item->itemid ?>"><?php echo _s($item->itemname, 'arabic'); ?></option>
                    <?php endforeach; ?>
                </select>

            </div>

            <div class="g3">
                <label class="">صغير  </label>
                <input name="dish-size" value="small" <?= ($item_data[0]->size == 'small' ? 'checked' : '') ?> type="radio"  class="form-control" style="width:2%"/>
            </div>
            <div class="g3">
                <label class="">وسط  </label>
                <input name="dish-size" value="medium" type="radio"  <?= ($item_data[0]->size == 'medium' ? 'checked' : '') ?> class="form-control" style="width:2%"/>
            </div>
            <div class="g3">
                <label class="">كبير  </label>
                <input name="dish-size" value="large" type="radio" <?= ($item_data[0]->size == 'large' ? 'checked' : '') ?> class="form-control" style="width:2%"/>
            </div>


            <br clear="all"/>
            <div class="g3">
                <input type="text" class="form-control" placeholder="ابدأ بالكتابة للبحث" onkeyup="searchForItem(this)">
            </div>
            <br clear="all"/>
            <div style="overflow: scroll; height: 171px;background-color: #FFF;">
                <?php
                if (!empty($raw_material)) {


                    foreach ($raw_material as $i => $aditem) {
                        ?>

                        <?php
                        $arr = [];
                        foreach ($item_data as $value) {
                            $arr[$value->qty] = $value->raw_id;
                        }
                        $check = "";
                        $key = "";
                        if (in_array($aditem->id, $arr)) {
                            $check = "checked";
                            $key = array_search($aditem->id, $arr);
                        }

                        $linkid = '';
                        $linkprice = '';
                        ?>


                        <div class="mydiv" style="display: inline-block;width: 200px;" id="parent<?php echo $i; ?>">
                            <input type="checkbox" <?php echo $check; ?> class="additem form-control" id="additem_<?php echo $aditem->id; ?>" name="addi_items[]" value="<?php echo $aditem->id; ?>"> 
                            <!--<input onclick="check_status(this, '<?php echo $i; ?>', '<?php echo $linkid; ?>', '<?php echo $linkprice; ?>', '<?php echo $aditem->unit_title ?>')" type="checkbox" <?php echo $check; ?> class="additem form-control" id="additem_<?php echo $aditem->id; ?>" name="addi_items[]" value="<?php echo $aditem->id; ?>">--> 
                            <?php echo _s($aditem->itemname, get_set_value('site_lang')); ?>
                            <div id="child<?php echo $i; ?>">
                                <input type="text" class="price_c" id="price<?php echo $i; ?>" name="price[<?php echo $aditem->id; ?>]" value="<?php echo $key; ?>" placeholder="الكمية"  style="width: 50%;">
                                <span><?php echo _s($aditem->unit_title,get_set_value('site_lang')) ?></span>
                            </div> 
                        </div>
                        <?php
                    }
                }
                ?>

            </div>         


            <br clear="all"/>
            <div class="g3">
                <span id="text">

                </span>
            </div>

            <div class="box  g16" align="">

                <input name="sub_mit" id="sub_mit" type="submit" class="green flt-r g2" value="<?php echo lang('Add') ?>" />

                <input name="sub_reset" type="reset" class="gray flt-r g2" value="<?php echo lang('Reset') ?>" />

            </div>



        </div>
    </form>
</div>

