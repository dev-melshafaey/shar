<style>
    #new_data_table tbody tr td,#new_data_table thead tr th{
        border: 1px solid #fff;
    }
</style>

<div id="pad-wrapper">

    <div class="table-wrapper users-table ">
        <form action="<?php //echo form_action_url('delete_customers');                      ?>" id="listing" method="post" autocomplete="off">
            <div class=" head">
                <div class="">
                    <h4>

                        <div class="title"> <span><?php //breadcramb();                    ?></span> </div>


                    </h4>

                </div>
            </div>



            <div class="">
                <div class="">
                    <?php //action_buttons('add_item', $cnt); ?>   
                    <table id="new_data_table">

                        <thead class="thead">
                            <tr>
                                <th  id="no_filter"><label for="checkbox"></label><?php echo lang('All') ?></th>
                                <th ><?php echo lang('transfer') ?></th>
                                <th ><?php echo lang('Name') ?></th>
                                <th ><?php echo lang('Quantity-transfer') ?></th>
                                <th ><?php echo lang('Unit') ?></th>
                                <th style="background: #7D9EBD"><?php echo lang('fStore') ?></th>
                                <th style="background: #7D9EBD"><?php echo lang('store_quantity') ?></th>
                                <th style="background: #94B874"><?php echo lang('tStore') ?></th>
                                <th style="background: #94B874"><?php echo lang('store_quantity') ?></th>

                                <th ><?php echo lang('lost') ?> % </th>
                                <th ><?php echo lang('Date') ?> % </th>
                                <th  id="no_filter">&nbsp;</th>
                            </tr>
                        </thead>

                        <?php
                        $data = $this->inventory->get_transfer_list();
                        if ($data) {
                            foreach ($data as $inv) {
                                ?>
                                <tr>
                                    <td></td>
                                    <td><?= $inv->transfer_id ?></td>
                                    <td><?php echo _s($inv->itemname, get_set_value('site_lang')); ?></td>
                                    <td><?php echo $inv->transfer_qty ?></td>
                                    <td><?php echo _s($inv->unit_title,get_set_value('site_lang')) ?></td>
                                    <td style="background: #7D9EBD"><?php echo _s($inv->st1n, get_set_value('site_lang')); ?></td>
                                    <td style="background: #7D9EBD"><?php echo $inv->totalq1 ?></td>
                                    <td style="background: #94B874"><?php echo _s($inv->st2n, get_set_value('site_lang')); ?></td>
                                    <td style="background: #94B874"><?php echo $inv->totalq2 ?></td>

                                    <td><?php echo $inv->percent ?>  </td>
                                    <td><?php echo date('Y-m-d', strtotime($inv->created)); ?>  </td>
                                    <td><?php
                                        /*if ($udata['owner_id'] == 0) {
                                            edit_button('pending_transfer/' . $inv->transfer_id);
                                        }*/
                                        ?>
                                    </td>
                                </tr>

                                <?php
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
            <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>

    </div>
    <!-- END PAGE --> 
</div>
