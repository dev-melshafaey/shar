



<div id="main-content" class="main_content">
<!--    <div class="title title alert blue">
        <span><?php breadcramb(); ?> </span><span class="icon icon-infographic left" ></span>
    </div>-->

    <div class="notion title title alert alert-info">* <?php echo lang('mess1') ?><span class="icon icon-fire" style="float: left;"></span></div>
    <?php error_hander($this->input->get('e')); ?>

    <form action="<?php echo form_action_url('add_store'); ?>" method="post" autocomplete="off" id="frm_store" name="frm_store" class="sample-form">
        <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
        <input type="hidden" name="storeid" id="storeid" value="<?php echo $cat->storeid; ?>" />
        <div class="form mycontent">
            <div class="">
                <div class="g3 form-group">
                    <label class=""><?php echo lang('Company-Name') ?></label>
                    <div class="ui-select " >
                        <div class="">
                            <?php company_dropbox('companyid', $cat->companyid); ?>
                        </div>
                    </div>
                </div>

                <div class="g3 form-group">
                    <label class=""><?php echo lang('branch-Name') ?></label>
                    <div class="ui-select  " >
                        <div class="">

                            <?php company_branch_dropbox('branchid', $cat->branchid, $cat->companyid); ?>
                        </div>
                    </div>



                </div>

                <div class="g3 form-group">
                    <label class=""><?php echo lang('Store-Name-ar') ?></label>
                    <input name="storename[arabic]" id="storename" value="<?php echo _s($cat->storename, "arabic"); ?>" type="text"  class="required  valid  form-control"/>                 
                </div>

                <div class="g3 form-group">
                    <label class=""><?php echo lang('Store-Name-en') ?></label>
                    <input name="storename[english]" id="storename" value="<?php echo _s($cat->storename, "english"); ?>" type="text"  class="required  valid  form-control"/>                 
                </div>

                <div class="g3 form-group">
                    <label class=""><?php echo lang('Storem') ?></label>
                    <?php all_users('storemanager', $cat->storemanager); ?>
                    <!--<input name="storemanager" id="" value="" type="text"  class="required  valid  form-control"/>-->                 
                </div>

                <br clear="all"/>
                <div style="display: none">    


                </div>

                <div class="g3 form-group">
                    <label class=""><?php echo lang('Status') ?></label>
                    <div class="ui-select " >
                       
                        <select id="storestatus" name="storestatus">
                            <option value="A">Active</option>
                            <option value="D">Not Active</option>
                        </select>
                    </div>
                </div>

                <br clear="all"/>
                <div class="raw field-box" align="">
                    <input name="sub_mit" id="sub_mit" type="submit" class="green flt-r g2" value="<?php echo lang('Add') ?>" />
                    <input name="sub_reset" type="reset" class="gray flt-r g2" value="<?php echo lang('Reset') ?>" />
                </div>
                <!--end of raw field-box--> 
            </div>
        </div>
    </form>
</div>
<!-- END PAGE --> 

<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>-->

<script>
    $(function () {

        $("#from_date_filter").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            numberOfMonths: 1,
            onClose: function (selectedDate) {
//$( "#from_date_filter" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $("#to_date_filter").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            numberOfMonths: 1,
            onClose: function (selectedDate) {
//$( "#to_date_filter" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });


</script>
<!-- End Section--> 
<!--footer-->
