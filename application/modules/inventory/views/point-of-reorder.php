<?php $this->load->view('common/meta');?>
<!--body with bg-->

	
<div class="body">
<header>
  <?php $this->load->view('common/header');?>
  
    <link href="<?php echo base_url(); ?>css/lib/jquery.dataTables.css" type="text/css" rel="stylesheet" />
    
    <!-- this page specific styles -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/compiled/datatables.css" type="text/css" media="screen" />
</header>
<?php $this->load->view('common/left-navigations'); ?>
    <div class="content">
        
        

        
        
        <!-- settings changer -->
        <div class="skins-nav">
            <a href="#" class="skin first_nav selected">
                <span class="icon"></span><span class="text">Default</span>
            </a>
            <a href="#" class="skin second_nav" data-file="css/compiled/skins/dark.css">
                <span class="icon"></span><span class="text">Dark skin</span>
            </a>
         </div>   
        <div id="pad-wrapper">
             
		<div class="table-wrapper users-table section">
                    <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">
                <div class="row head">
                    <div class="col-md-12">
                        <h4>
                            
                            <div class="title"> <span><?php breadcramb(); ?></span> </div>
  
                            
                        </h4>
                        <?php error_hander($this->input->get('e')); ?>
                    </div>
                </div>



                            <div class="row">
                <div class="col-md-12">

                    <table id="example">
            <form action="<?php echo form_action_url('bulk_point_or_reorder'); ?>" id="listing" method="post" autocomplete="off">
                <thead>
                <tr>
                <td width="1%"><label for="checkbox"><?php echo lang('All')?></label></td>
                <td width="7%"><?php echo lang('Serial-Number')?></td>
                <td width="24%" ><?php echo lang('Name')?> </td>
                <td width="20%" ><?php echo lang('Date-Of-Paid')?>  </td>
                <td width="10%"><?php echo lang('Instock')?></td>
                <td width="13%"><?php echo lang('Point-of-Sale')?> </td>
                <td width="11%"><?php echo lang('Net-Cost')?> /1</td>
                <td width="14%"><?php echo lang('Net-Profit')?> /1</td>
              </tr>
                </thead>    
              <?php 
                        $cnt = 0;
                foreach($this->inventory->re_order_list() as $cat) { 
				$cnt++;
				
			?>
              <tr>
                <td ><input type="checkbox" class="allcb" name="u[]" id="u_<?php echo $cat->poid; ?>" value="<?php echo $cat->poid; ?>" /></td>
                <td ><?php echo $cat->serialnumber; ?></td>
                <td><?php echo $cat->itemname; ?></td>
                <td><?php echo date('d F Y',strtotime($cat->paid_date)); ?></td>
                <td><?php echo $cat->quanitity; ?> Unit</td>
                <td><?php echo $cat->point_of_re_order; ?> Unit</td>
                <td><?php echo $cat->unit_price; ?> RO.</td>
                <td><?php echo $cat->point_of_re_order; ?> RO.</td>
              </tr>
              <?php } ?>
            </form>
          </table>
        </div>
      </div>
      <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>
      <?php action_buttons('inventory/add_purchase',$cnt); ?>
    </div>
    <!-- END PAGE --> 
  </div>
  </div>
  </div>

<!-- End Section--> 
<!--footer-->
<?php $this->load->view('common/footer');?>
