<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inventory_model extends CI_Model {

    function __construct() {

        parent::__construct();
    }

    public function re_order_list() {

        $ownerid = ownerid();

        $this->db->select('bs_point_of_re_order.poid,

		bs_point_of_re_order.quanitity,

		bs_point_of_re_order.unit_price,

		bs_point_of_re_order.paid_date,

		bs_item.serialnumber,

		bs_item.itemname,

		bs_item.point_of_re_order');

        $this->db->from('bs_point_of_re_order');

        $this->db->join('bs_item', 'bs_item.itemid = bs_point_of_re_order.productid');

        $this->db->where('bs_point_of_re_order.ownerid', $ownerid);

        $this->db->order_by("bs_point_of_re_order.paid_date", "DESC");

        $query = $this->db->get();

        return $query->result();
    }

    public function categorylist() {

        $ownerid = ownerid();

        $this->db->select('bs_category.catid,bs_category.catname,bs_category.catdescription,bs_category.catstatus,bs_company.company_name,bs_category.cattype');

        $this->db->from($this->config->item('table_cat'));

        $this->db->join('bs_company', 'bs_company.companyid = bs_category.companyid', 'LEFT');

        //$this->db->where('bs_category.ownerid', $ownerid);

        $this->db->order_by("bs_category.catname", "ASC");

        $query = $this->db->get();

        return $query->result();
    }

    function getPendingSales($userid) {

        /*

          $sql = 'SELECT

          s.`ownerid` AS owneri,

          ss.`invoiceid` AS invoice,

          sii.*,

          ss.*,

          u.*

          FROM

          `sales_invoice_items` AS sii

          INNER JOIN `bs_sales_invoice` AS ss

          ON ss.`invoiceid` = sii.`invoice_id`

          INNER JOIN `bs_store` AS s

          ON s.`storeid` = sii.`store_id`

          INNER JOIN `bs_users` AS u

          ON u.`userid` = ss.`customerid`

          WHERE s.`ownerid` = ' . $userid . '

          AND ss.`confirm_status` = "0" ';

          $query = $this->db->query($sql);

          if ($query->num_rows() > 0) {

          return $query->result();

          } else {

          return false;

          }

         */

        $sql = "SELECT 

                    bpi.*,

                    u.userid,u.fullname



                  FROM

                    `an_invoice` AS bpi 







                  LEFT JOIN `bs_users` AS u ON u.`userid` = bpi.`customer_id` 

                  

                  INNER JOIN `bs_invoice_items` AS ii ON ii.`invoice_id` = bpi.`invoice_id` and ii.invoice_itype='P'



                  





                  WHERE bpi.`confirm_status` = '0' AND bpi.invoice_type='itemscharges' and bpi.cancel_sales='0'



                  GROUP BY bpi.invoice_id



                  ORDER BY bpi.`invoice_id` DESC ";

        $query = $this->db->query($sql);



        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 



        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    function getSalesInvoiceItems($id) {

        /* $sql= 'SELECT i.*,piv.*, piv.`id` AS piv_id FROM `purchase_invoice_items` AS piv

          INNER JOIN `bs_item`  AS i

          ON i.`itemid` = piv.`product_id`

          WHERE piv.`invoice_id`='.$inv_id.''; */

        /* $sql ="SELECT piv.`store_id` AS store, i.*,piv.*, piv.`id` AS piv_id, s.`storename` FROM `sales_invoice_items` AS piv

          INNER JOIN `bs_item`  AS i

          ON i.`itemid` = piv.`product_id`

          INNER JOIN `bs_store` AS s ON s.`storeid`=piv.`store_id`

          WHERE piv.`invoice_id`=".$inv_id."" */;

        /* $sql = 'SELECT 

          storeitem,

          s.`ownerid` AS owneri,

          sii.`quantity` AS qt,

          sii.*,

          ss.* ,

          u.*,

          i.*

          FROM

          `sales_invoice_items` AS sii

          INNER JOIN `bs_sales_invoice` AS ss

          ON ss.`invoiceid` = sii.`invoice_id`

          INNER JOIN `bs_store` AS s

          ON s.`storeid` = sii.`store_id`

          INNER JOIN `bs_users` AS u ON u.`userid` = ss.`invoiceid`

          LEFT JOIN

          (SELECT

          bs_store_items.`store_id`,

          SUM(bs_store_items.`item_id`) AS storeitem,

          bs_store_items.`item_id`

          FROM

          `bs_store_items`

          INNER JOIN `bs_store` AS ss

          ON ss.`storeid` = bs_store_items.`store_id`

          GROUP BY bs_store_items.`item_id`,

          ss.`storeid`) AS bsi

          ON bsi.item_id = sii.`product_id`

          INNER JOIN `bs_item` AS i ON i.`itemid` = sii.`product_id`

          WHERE s.`ownerid` = ' . $inv_id . ' AND  ss.`confirm_status` = "0"

          GROUP BY s.`storeid`';

          $query = $this->db->query($sql);

          if ($query->num_rows() > 0) {

          return $query->result();

          } else {

          return false;

          }

         */



        $sql = "SELECT 

                bpi.*,

                bpi.invoice_item_store_id as sid,

                u.userid,u.fullname,

                s.storeid,

                s.storename,

                p.*,

                i.*

              FROM

                `bs_invoice_items` AS bpi 



              LEFT JOIN `an_invoice` AS p ON p.`invoice_id` = bpi.`invoice_id` 



              LEFT JOIN `bs_users` AS u ON u.`userid` = p.`customer_id` 



              LEFT JOIN `bs_store` AS s ON s.`storeid` = bpi.`invoice_item_store_id`



              

              

              LEFT JOIN `bs_item` AS i ON i.`itemid` = bpi.`invoice_item_id` 



              WHERE bpi.`invoice_id` = '$id' 

              

              and bpi.`confirm_status` = '0' 



              GROUP BY bpi.inovice_product_id

              



              ORDER BY bpi.`inovice_product_id` DESC ";

        $query = $this->db->query($sql);



        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 



        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    public function product_list($id) {

        $subq = "";

        /* if($id !=""){

          $subq = " AND bs_store.storeid =  '".$id."'";

          } */

        /*

          SELECT

          `bs_category`.`catname`,

          `bs_item`.`storeid`,

          `bs_suppliers`.`suppliername`,

          `bs_store`.`storename`,

          `bs_item`.`itemid`,

          `bs_item`.`itemname`,

          `bs_item`.`itemtype`,

          IF(

          bs_item.itemtype = 'P',

          `bs_item`.`serialnumber`,

          '-----'

          ) AS `serialnumber`,

          IF(

          bs_item.itemtype = 'P',

          CONCAT(`bs_item`.`purchase_price`,' RO.'),

          '-----'

          ) AS `purchase_price`,

          IF(

          bs_item.itemtype = 'P',

          CONCAT(`bs_item`.`sale_price`,' RO.'),

          '-----'

          ) AS `sale_price`,

          IF(

          bs_item.itemtype = 'P',

          `bs_item`.`min_sale_price`,

          '-----'

          ) AS `min_sale_price`,

          IF(

          bs_item.itemtype = 'P',

          `bs_item`.`point_of_re_order`,

          '-----'

          ) AS `point_of_re_order`,

          IF(

          bs_item.itemtype = 'P',

          `bs_item`.`quantity`,

          '-----'

          ) AS `quantity`,

          `bs_item`.`product_picture`

          FROM

          (`bs_item`)

          JOIN `bs_category`

          ON `bs_item`.`categoryid` = `bs_category`.`catid`

          JOIN `bs_store`

          ON `bs_item`.`storeid` = `bs_store`.`storeid`

          JOIN `bs_suppliers`

          ON `bs_item`.`suppliderid` = `bs_suppliers`.`supplierid`

          WHERE `bs_item`.`ownerid` = '".$ownerid."' ".$subq." ORDER BY `bs_item`.`itemname` ASC "

         */

        $sql = "SELECT
  `bs_category`.`catname`,u.`unit_title`,
  `bs_item`.`itemid`,
  `bs_item`.shelf,
  `bs_item`.`suppliderid`,
  `bs_item`.`itemname`,
  `bs_item`.`itemtype`,
  `bs_item`.`storeid`,
  `bs_item`.`submissiondate`,
  `bs_item`.`re_order`,
  bs_item.barcodenumber,
  IF(
    bs_item.serialnumber != '',
    `bs_item`.`serialnumber`,
    '-----'
  ) AS `serialnumber`,
  IF(
    bs_item.purchase_price != '',
    CONCAT(`bs_item`.`purchase_price`, ''),
    '-----'
  ) AS `purchase_price`,
  IF(
    bs_item.sale_price != '',
    CONCAT(`bs_item`.`sale_price`, ''),
    '-----'
  ) AS `sale_price`,
  IF(
    bs_item.min_sale_price != '',
    `bs_item`.`min_sale_price`,
    '-----'
  ) AS `min_sale_price`,
  IF(
    bs_item.point_of_re_order != '',
    `bs_item`.`point_of_re_order`,
    '-----'
  ) AS `point_of_re_order`,
  IF(
    bs_item.quantity != '',
    `bs_item`.`quantity`,
    '-----'
  ) AS `quantity`,
  `bs_item`.`product_picture`
FROM
  (`bs_item`)
  LEFT JOIN `bs_category`
    ON `bs_item`.`categoryid` = `bs_category`.`catid`
   LEFT JOIN `units` AS u ON u.`unit_id` =  bs_item.`type_unit`
ORDER BY `bs_item`.`itemid` DESC ";

        $ownerid = ownerid();

        $query = $this->db->query($sql);

        //$this->db->last_query();

        return $query->result();
    }

    function extraproduct_list() {
        $sql = "SELECT
  *
FROM
 `additional_items` ";
        $query = $this->db->query($sql);

        //$this->db->last_query();

        return $query->result();
    }

    function getAdditionItemById($id) {
        $sql = "SELECT * FROM `additional_items` AS ad WHERE ad.`ad_item_id` = '" . $id . "'";
        $query = $this->db->query($sql);

        //$this->db->last_query();

        return $query->row();
    }

    public function extrinventaproduct_list($id) {



        $sql = "SELECT
  *
FROM
 `inventory_items` ";
        $query = $this->db->query($sql);

        //$this->db->last_query();

        return $query->result();
    }

    public function raw_material_list() {
        //$query = "select units.unit_title,dish_content.qty, raw_material.* from raw_material inner join units on units.unit_id = raw_material.unit_id left join dish_content on dish_content.raw_id = raw_material.id group by id";
        $this->db->join('units', 'units.unit_id =raw_material.unit_id');
        $query = $this->db->get('raw_material');
        //$query = $this->db->query($query);
        return $query->result();
    }

    public function juiceproduct_list($id) {



        $sql = "SELECT
  *
FROM
  `bs_item`
  LEFT JOIN `bs_category`
    ON `bs_item`.`categoryid` = `bs_category`.`catid`
  WHERE bs_item.`itemtype` = 'J'
ORDER BY `itemid` DESC  ";
        $query = $this->db->query($sql);

        //$this->db->last_query();

        return $query->result();
    }

    public function get_dish_content() {
        $this->db->join('bs_item', 'bs_item.itemid = dish_content.item_id');
        //$this->db->group_by('bs_item.itemid');
        $this->db->order_by('created_at', 'desc');
        $query = $this->db->get('dish_content');
        $arr = array();
        foreach ($query->result() as $key => $value) {
            $obj = new stdClass();
            $sql = "SELECT qty,itemname AS item_name, unit_title FROM dish_content LEFT JOIN raw_material ON dish_content.`raw_id` = raw_material.id LEFT JOIN units ON raw_material.`unit_id` = units.`unit_id`where item_id = $value->item_id";
            $sql = $this->db->query($sql);
            $obj->item_id = $value->item_id;
            $obj->itemname = $value->itemname;
            $obj->categ_type = $value->categ_type;
            $obj->size = $value->size;
            $obj->created_at = $value->created_at;
            $obj->contents = $sql->result();
            array_push($arr, $obj);
        }
        return $arr;
    }

    function get_item_data($id) {
        if ($id != null) {
            $this->db->join('bs_item', 'bs_item.itemid = dish_content.item_id');
            $this->db->join('raw_material', 'dish_content.raw_id = raw_material.id');
            $query = $this->db->get_where('dish_content', array('item_id' => $id));
            return $query->result();
        }
    }

    public function cat_detail($catid) {

        $this->db->where('catid', $catid);

        $query = $this->db->get($this->config->item('table_cat'));

        if ($query->num_rows() > 0) {

            return $query->row();
        }
    }

    public function get_item_detail($itemid) {

        $this->db->where('itemid', $itemid);

        $query = $this->db->get($this->config->item('table_item'));

        if ($query->num_rows() > 0) {

            return $query->row();
        }
    }

    public function get_juiceitem_detail($itemid) {

        $this->db->where('itemid', $itemid);

        $query = $this->db->get('bs_item');

        if ($query->num_rows() > 0) {

            return $query->row();
        }
    }

    public function get_additionalitem_detail($itemid) {

        $this->db->where('ad_item_id', $itemid);

        $query = $this->db->get('additional_items');

        if ($query->num_rows() > 0) {

            return $query->row();
        }
    }

    public function delete_categories() {



        $u = $this->input->post('u');

        if ($u != '') {

            $this->db->where_in('catid', $u);

            $this->db->delete($this->config->item('table_cat'));
        }

        do_redirect('categories?e=12');
    }

    function getLinkItem($id) {

        $this->db->where_in('link_id', $id);

        $this->db->delete('link_items');
    }

    public function bulk_point_or_reorders() {



        $u = $this->input->post('u');

        if ($u != '') {

            $this->db->where_in('poid', $u);

            $this->db->delete('bs_point_of_re_order');
        }

        do_redirect('point_reorder?e=12');
    }

    public function delete_stores() {



        $u = $this->input->post('u');

        if ($u != '') {

            $this->db->where_in('storeid', $u);

            $this->db->delete($this->config->item('table_store'));
        }

        do_redirect('stores?e=12');
    }

    public function add_update_category() {

        $data = $this->input->post();

        $catid = $this->input->post('catid');

        $data['catname'] = serialize(post('catname'));

        unset($data['sub_mit'], $data['sub_reset'], $data['catid']);

        if ($catid != '') {

            $this->db->where('catid', $catid);

            $this->db->update($this->config->item('table_cat'), $data);

            do_redirect('categories?e=10');
        } else {

            $this->db->insert($this->config->item('table_cat'), $data);

            do_redirect('categories?e=10');
        }
    }

    public function addStore() {

        $data = $this->input->post();

        $storeid = $this->input->post('storeid');

        $data['storename'] = serialize(post('storename'));

        unset($data['store_submit'], $data['sub_reset'], $data['storeid'], $data['sub_mit']);









        if ($storeid != '') {

            $this->db->where('storeid', $storeid);

            $this->db->update($this->config->item('table_store'), $data);



            $udata = array('storemanager' => $storeid);

            $this->db->where('userid', post('storemanager'));

            $this->db->update('bs_users', $udata);



            //var_dump($udata);
            //die(post('storemanager'));

            do_redirect('stores?e=10');
        } else {

            $this->db->insert($this->config->item('table_store'), $data);

            $storeid = $this->db->insert_id();





            $udata = array('storemanager' => $storeid);

            $this->db->where('userid', post('storemanager'));

            $this->db->update('bs_users', $udata);



            //die();

            do_redirect('stores?e=10');
        }
    }

    function getLinkid($id) {
        $sql = "SELECT li.`link_id` FROM `link_items` AS li WHERE li.`link_id`=$id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row()->link_id;
        } else {
            return false;
        }
    }

    function getAdditionalItems() {
        $sql = "SELECT * FROM `additional_items` AS ai";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
            ;
        } else {
            return false;
        }
    }

    function linkitems($id) {
        $sql = "SELECT * FROM `link_items` AS li WHERE li.`main_item_id` = '" . $id . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getItemByBracode($itembarcode) {

        $sql = "SELECT
  i.`itemid`
FROM
  `bs_item` AS i
WHERE i.`barcodenumber` = '" . $itembarcode . "' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function add_new_item() {

        $data = $this->input->post();




        $itemid = $this->input->post('itemid');

        $barcodenumber = $this->input->post('barcodenumber');

        $path = APPPATH . '../uploads/item/' . $this->input->post('storeid') . '/';

        $file_name = upload_file('product_picture', $path, true, 50, 50);

        if ($file_name != '') {

            $data['product_picture'] = $file_name;
        }

        $data['itemname'] = serialize(post('itemname'));





        if ($itemid == "") {
            $data['purchase_price_dharam'] = post('purchase_price_dharam');

            if (post('get_option_p') == "0") {



                $data['sale_price'] = post('cal_sale_price');

                $data['salingbytotal'] = post('cal_salingbytotal');

                $data['min_sale_price'] = post('cal_min_sale_price');
            }
        } else {



            if (post('get_option_p') == "0" && post('sale_price') == null) {



                $data['sale_price'] = post('cal_sale_price');

                $data['salingbytotal'] = post('cal_salingbytotal');

                $data['min_sale_price'] = post('cal_min_sale_price');
            } else {

                $data['sale_price'] = post('sale_price');

                $data['salingbytotal'] = post('salingbytotal');

                $data['min_sale_price'] = post('min_sale_price');
            }
        }



        unset($data['submit_item'], $data['sub_mit'], $data['sub_reset'], $data['itemid'], $data['cal_purchase_price'], $data['get_option_p'], $data['offer_dicounte']);

        unset($data['cal_sale_price'], $data['cal_salingbytotal'], $data['cal_min_sale_price']);



        if ($itemid != '') {

            $this->db->where('itemid', $itemid);

            $this->db->update($this->config->item('table_item'), $data);

            do_redirect('items?e=10');
        } else {

            if (!$this->getItemByBracode($barcodenumber)) {
                $this->db->insert($this->config->item('table_item'), $data);
                do_redirect('items?e=10');
            } else {
                // do_redirect('items?e=10');
                do_redirect('items?e=13');
            }
        }
    }

    public function add_new_point_of_order() {

        $data = $this->input->post();

        $poid = $this->input->post('poid');

        $path = APPPATH . '../uploads/point_of_re_order/' . $this->input->post('storeid') . '/';

        $attachment = upload_file('attachment', $path, true, 50, 50);

        if ($attachment != '') {

            $data['attachment'] = $attachment;
        }

        unset($data['submit_reorder'], $data['sub_reset'], $data['poid'], $data['serialnumber']);

        if ($itemid != '') {

            $this->db->where('poid', $poid);

            $this->db->update('bs_point_of_re_order', $data);

            do_redirect('point_reorder?e=10');
        } else {

            ;

            $this->db->insert('bs_point_of_re_order', $data);

            do_redirect('point_reorder?e=10');
        }
    }

    public function add_supplier() {

        $data = $this->input->post();

        $supplierid = $this->input->post('supplierid');

        unset($data['submit_supplier'], $data['sub_reset']);

        if ($supplierid != '') {

            $this->db->where('supplierid', $supplierid);

            $this->db->update($this->config->item('table_suppliers'), $data);

            echo('1');
        } else {

            $this->db->insert($this->config->item('table_suppliers'), $data);

            echo('1');
        }
    }

    public function get_store_detail($storeid) {

        $this->db->where('storeid', $storeid);

        $query = $this->db->get($this->config->item('table_store'));

        if ($query->num_rows() > 0) {

            return $query->row();
        }
    }

    function purchase_receipt() {

        /*    $sql = "SELECT 

          bpi.*,

          u.userid,u.fullname,

          s.storeid,

          s.storename,

          sum(pi.purchase_item_quantity) as quantity,

          sum(pp.payment_amount) as paid



          FROM

          `an_purchase` AS bpi







          LEFT JOIN `bs_users` AS u ON u.`userid` = bpi.`customer_id`



          LEFT JOIN `bs_store` AS s ON s.`storeid` = bpi.`purchase_item_store_id`



          LEFT JOIN `bs_purchase_items` AS pi ON pi.`purchase_id` = bpi.`purchase_id`



          LEFT JOIN `purchase_payments` AS pp ON pp.`purchase_id` = bpi.`purchase_id`





          WHERE bpi.`confirm_status` = '1'



          GROUP BY bpi.purchase_id



          ORDER BY bpi.`purchase_id` DESC ";

         */

        $sql = "SELECT 

  si.*,

  u.userid,

  u.fullname,

  s.storeid,

  s.storename,

  SUM(si.quantity) AS qq,

  SUM(pp.payment_amount) AS paid,

  pii.itemid,

  pii.itemname,

  pii.product_picture,

  ii.purchase_id,

  ii.purchase_item_price 

FROM

  `bs_store_items` AS si 

  LEFT JOIN `bs_users` AS u 

    ON u.`userid` = si.`supplier_id` 

  LEFT JOIN `bs_store` AS s 

    ON s.`storeid` = si.`store_id` 

  LEFT JOIN `bs_item` AS pii

    ON pii.`itemid` = si.`item_id` 

  LEFT JOIN `purchase_payments` AS pp 

    ON pp.`purchase_id` = si.`purchase_id` 

  LEFT JOIN `bs_purchase_items` AS ii 

    ON ii.`purchase_id` = si.`purchase_id` 

WHERE si.`transfer` = '0' 

GROUP BY si.item_id 

ORDER BY si.`item_id` ASC  ";

        $query = $this->db->query($sql);



        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 



        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    function sales_recept() {

        /* $sql = "SELECT 

          pr.*

          FROM

          `sales_receipt` AS pr

          INNER JOIN `sales_confirm_items` AS pci

          ON pci.`receipt_id` = pr.`purchase_receipt_id`

          GROUP BY pci.`invoice_id` ";

          $q = $this->db->query($sql);

          return $q->result();

         */

        /*

          $sql = "SELECT

          si.*,

          u.userid,u.fullname,

          s.storeid,

          s.storename,

          sum(si.soled_quantity) as quantity,

          sum(pp.payment_amount) as paid,

          pi.itemid,pi.itemname,pi.product_picture,

          ii.invoice_id,ii.invoice_item_price

          FROM

          `store_soled_items` AS si







          LEFT JOIN `bs_users` AS u ON u.`userid` = si.`userid`



          LEFT JOIN `bs_store` AS s ON s.`storeid` = si.`store_id`



          LEFT JOIN `bs_item` AS pi ON pi.`itemid` = si.`item_id`



          LEFT JOIN `bs_invoice_items` AS ii ON ii.`invoice_id` = si.`recipt_id`



          LEFT JOIN `invoice_payments` AS pp ON pp.`invoice_id` = si.`recipt_id`







          GROUP BY si.item_id



          ORDER BY si.`store_item_id` DESC  ";

         */



        $sql = "SELECT 

                    si.*,

                    u.userid,u.fullname,

                    s.storeid,

                    s.storename,

                    sum(si.soled_quantity) as quantity,

                    pi.itemid,pi.itemname,pi.product_picture,

                    ii.invoice_id,ii.invoice_item_price

                  FROM

                    `store_soled_items` AS si 







                  LEFT JOIN `bs_users` AS u ON u.`userid` = si.`userid` 



                  LEFT JOIN `bs_store` AS s ON s.`storeid` = si.`store_id`

                  

                  LEFT JOIN `bs_item` AS pi ON pi.`itemid` = si.`item_id`

                  

                  LEFT JOIN `bs_invoice_items` AS ii ON ii.`invoice_id` = si.`recipt_id`

                  









                  GROUP BY si.recipt_id



                  ORDER BY si.`recipt_id` DESC  ";



        $query = $this->db->query($sql);



        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 



        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    function getitems($invoiceid) {

        $sql = "SELECT pii.`purchase_id`, s.`storename`, i.`itemname`,pii.* FROM `bs_purchase_items` AS pii

                            INNER JOIN `bs_item` AS i ON i.`itemid` = pii.`purchase_item_id`

                            INNER JOIN `bs_store` AS s ON s.`storeid` = pii.`purchase_item_store_id`

                            WHERE pii.`purchase_id` = " . $invoiceid . "";

        $q = $this->db->query($sql);

        return $q->result();
    }

    function getsalesitems($invoiceid) {

        $sql = "SELECT 

  pii.`invoice_id`,

  s.`storename`,

  i.`itemname`,

  pii.* 

FROM

  `sales_invoice_items` AS pii 

  INNER JOIN `bs_item` AS i 

    ON i.`itemid` = pii.`product_id` 

  INNER JOIN `bs_store` AS s 

    ON s.`storeid` = pii.`store_id` 

WHERE pii.`invoice_id` =  " . $invoiceid . "";

        $q = $this->db->query($sql);

        return $q->result();
    }

    function getStoreItems($id) {

        $sql = "SELECT

            SUM(bi.`quantity`) AS totalquantity,  

             bi.*,

              `bs_category`.`catname`,

             `bs_suppliers`.`suppliername`,

             bi.`created` AS purchase_date,

             `i`.`itemid`,

             i.product_picture,

             `i`.`itemname`,

             `i`.`itemtype`,

             IF(

               i.`itemtype` = 'P',

               `i`.`serialnumber`,

               '-----'

             ) AS `serialnumber`,

             IF(

               i.`itemtype` = 'P',

               CONCAT(`i`.`purchase_price`, ' RO.'),

               '-----'

             ) AS `purchase_price`,

             IF(

               i.`itemtype` = 'P',

               CONCAT(`i`.`sale_price`, ' RO.'),

               '-----'

             ) AS `sale_price`,

             IF(

               i.`itemtype` = 'P',

               `i`.`min_sale_price`,

               '-----'

             ) AS `min_sale_price`,

             IF(

               i.`itemtype` = 'P',

               `i`.`point_of_re_order`,

               '-----'

             ) AS `point_of_re_order` 



           FROM

             `bs_item` AS i 

             INNER JOIN `bs_store_items` AS bi 

               ON bi.`item_id` = i.`itemid` 

               

             INNER JOIN `bs_category` 

               ON `i`.`categoryid` = `bs_category`.`catid` 

             

           WHERE bi.`store_id` = '" . $id . "' 

           GROUP BY i.`itemid`";


        $q = $this->db->query($sql);

        return $q->result();
    }

    function getStoreItem($id) {
       /* $sql = "SELECT si.*,
       i.*,
       store_value.`totalq`,
       ss.`store_id`,
      si.item_id as itemid,
       u1.userid,
       u.`unit_title`,
       u1.fullname,
       ss.soled_quantity,
       pit.`purchase_item_price`,
       bbit.`sale_price` AS sales,
       SUM(si.`quantity`) squantity,
       si.`quantity` AS qq,
       s.`storeid`,
       s.`storename`,
       ssquantity,
       ( IFNULL(( SUM(si.`quantity`) - IFNULL(ssquantity, 0) ), 0) *
         pit.`purchase_item_price` )AS
       totalpurchase,
       ( IFNULL(( IFNULL(ssquantity, 0) ), 0) * bbit.`sale_price` ) AS
       totalsales,
       SUM(si.`quantity`) - IFNULL(ssquantity, 0) AS rem
FROM   `raw_store_items` AS si
       LEFT JOIN store_value 
              ON store_value.`store_id` = si.`store_id` AND store_value.`item_id`= si.`item_id` 
       LEFT JOIN `raw_material` AS i
              ON i.`id` = si.`item_id`
       LEFT JOIN `bs_store` AS s
              ON `s`.`storeid` = `si`.`store_id`
       LEFT JOIN `bs_users` AS u1
              ON `u1`.`userid` = `si`.`supplier_id`
       LEFT JOIN (SELECT soldi.*,
                         SUM(soldi.`soled_quantity`) AS ssquantity
                  FROM   `store_soled_items` AS soldi
                         INNER JOIN `bs_item` AS bbit
                                 ON bbit.`itemid` = soldi.`item_id`
                  WHERE  soldi.`store_id` = '$id'
                         AND soldi.`is_cancel` = '0'
                  GROUP  BY soldi.`item_id`) ss
              ON `ss`.`store_id` = si.`store_id`
                 AND ss.`item_id` = si.`item_id`
       LEFT JOIN `bs_purchase_items` AS pit
              ON pit.`purchase_id` = si.`purchase_id`
                 AND pit.`purchase_item_id` = si.`item_id`
       INNER JOIN `bs_item` AS bbit
               ON bbit.`itemid` = si.`item_id`
       LEFT JOIN `units` AS u
              ON u.`unit_id` = bbit.`type_unit`
WHERE  si.`store_id` = '$id'
GROUP  BY si.`item_id` ";*/
        
        $sql = "
SELECT
  i.*,
  
  u.`unit_title`,
 
  asv.`totalq` AS rem,
  IFNULL(avp1.avg_value, 0) AS avgval
FROM
  `raw_material` AS i
  LEFT JOIN
    (SELECT
      avp.`avg_value`,
      avp.`item_id`
    FROM
      `average_package` AS avp
    WHERE avp.`store_id` =$id
      AND avp.`status` = '1') AS avp1
    ON avp1.item_id = i.`id`
 
  LEFT JOIN `store_value` AS asv
    ON asv.`item_id` = i.`id`
  LEFT JOIN `units` AS u
    ON u.`unit_id` = i.`unit_id`
WHERE asv.`store_id` = $id
GROUP BY asv.`item_id` ";


        $q = $this->db->query($sql);
        return $q->result();
    }

    function getStoreItem2($id) {

        $sql = "SELECT 

                i.itemname,

                i.itemid,

                i.storeid,

                i.serialnumber,

                s.storename,

                s.storeid,

                u1.fullname as fullname1,

                u1.userid as userid1,

                u2.fullname as fullname2,

                u2.userid as userid2,

                si.*,

                si.quantity AS squantity, 

                ppi.`purchase_item_price`,

                isi.`invoice_item_price`

              FROM

                `bs_store_items` AS si 

                

                LEFT JOIN `bs_item` AS i 

                  ON i.`itemid` = si.`item_id` 

                  

                LEFT JOIN `bs_store` AS s 

                  ON s.`storeid` = si.`store_id` 

                  

               

                LEFT JOIN `bs_invoice_items` AS isi

                  ON `isi`.`invoice_id` = `si`.`receipt_id` 

                  

                LEFT JOIN `bs_users` as u1 

                  ON `u1`.`userid` = `isi`.`customer_id` 

                  

                LEFT JOIN `bs_purchase_items` AS ppi

                  ON `ppi`.`purchase_id` = `si`.`purchase_id` 

                  

                LEFT JOIN `bs_users` as u2 

                  ON `u2`.`userid` = `ppi`.`supplier_id` 

                  



              WHERE si.`store_id` = '$id' 

              GROUP BY si.`item_id` ";

        $q = $this->db->query($sql);

        return $q->result();
    }

    public function storelist_old() {

        /* $sql="SELECT a.*,

          b.`location_name`,

          c.`financial`,

          COUNT(bi.`storeid`) AS storeitems,

          SUM(bi.`purchase_price`) AS totalpurchase,

          SUM(bi.`sale_price`) AS total

          FROM

          bs_location AS b,

          bs_financial AS c,

          bs_store AS a

          LEFT JOIN  `bs_item` AS bi

          ON bi.`storeid` = a.`storeid`

          LEFT JOIN `purchase_invoice_items` AS pit

          ON pit.`product_id` = bi.`itemid`

          WHERE a.`locationid` = b.`locationid`

          AND a.`ftypeid` = c.`ftypeid`

          AND a.`ownerid` = '".ownerid()."'

          GROUP BY a.`storeid`

          ORDER BY a.`storename` ASC"; */



        /* $sql="SELECT 

          a.*,

          b.`location_name`,

          c.`financial`,

          COUNT(bi.`storeid`) AS storeitems

          FROM

          bs_location AS b,

          bs_financial AS c,

          bs_store AS a

          LEFT JOIN  `bs_item` AS bi

          ON bi.`storeid` = a.`storeid`

          WHERE a.`locationid` = b.`locationid`

          AND a.`ftypeid` = c.`ftypeid`

          AND a.`ownerid` = '".ownerid()."'

          GROUP BY a.`storeid`

          ORDER BY a.`storename` ASC "; */





        /*

          $sql = "SELECT s.* , SUM(bi.`quantity`)  AS total

          ,bs_company_branch.branchid,bs_company_branch.branchname

          ,bs_company.companyid,bs_company.company_name

          FROM `bs_store` AS s

          LEFT JOIN `bs_store_items` AS bi ON bi.`store_id` = s.`storeid`

          LEFT JOIN bs_company on bs_company.companyid=s.companyid

          LEFT JOIN bs_company_branch on bs_company_branch.branchid=s.branchid

          GROUP BY s.`storeid`

          ORDER BY s.`storeid` DESC";

         */

        $sql = "SELECT 

            s.*,

            bs_company_branch.branchid,

            bs_company_branch.branchname,

            bs_company.companyid,

            bs_company.company_name,

            sum_sal,

            sum_pur

          FROM

            `bs_store` AS s 



            LEFT JOIN (

               SELECT 

                  si.*,

                  SUM(si.`quantity`) AS sum_sal



                FROM bs_store_items AS si

                WHERE si.receipt_id != '0'



            ) bi 

              ON bi.`store_id` = s.`storeid` 

            LEFT JOIN (

               SELECT 

                  si.*,

                  SUM(si.`quantity`) AS sum_pur



                FROM bs_store_items AS si

                WHERE si.purchase_id != '0'



            ) bi2 

              ON bi2.`store_id` = s.`storeid` 





            LEFT JOIN bs_company 

              ON bs_company.companyid = s.companyid 

            LEFT JOIN bs_company_branch 

              ON bs_company_branch.branchid = s.branchid 

          GROUP BY s.`storeid` 

          ORDER BY s.`storeid` DESC ";



        $q = $this->db->query($sql);

        return $q->result();
    }

    public function storelist($id = '', $pid = '') {

        $branchid = $this->session->userdata('bs_branchid');
        if ($id) {

            $ex = "WHERE s.storeid='$id' and bi.item_id='$pid'";
        } else {

            $ex = "";
        }

        if ($this->session->userdata('bs_memtype') != "5") {

            if ($ex != "") {
                $ex.=" AND s.`branchid` = '" . $branchid . "'";
            } else {
                $ex = " WHERE s.`branchid` = '" . $branchid . "'";
            }
        }

        $sql = "

				SELECT 

				  s.*,

				  si.*,

				  bs_company_branch.branchid,

				  bs_company_branch.branchname,

				  bs_company.companyid,

				  bs_company.company_name,

				  SUM(si.quantity) AS qs,

				  soledquantity,

				  SUM(si.quantity) - IFNULL(soledquantity, 0) AS remainingquantity1,
                                  SUM(`store_value`.`totalq`) AS remainingquantity

				FROM

				  `bs_store` AS s 
LEFT JOIN store_value ON `store_value`.`store_id` = s.`storeid`
				  

				  LEFT JOIN 

					(SELECT 

					  soldi.*,

					  SUM(soldi.`soled_quantity`) AS soledquantity 

					FROM

					  `store_soled_items` AS soldi 

					  

					LEFT JOIN bs_store AS sii ON sii.storeid = soldi.store_id 
					GROUP BY sii.`storeid`
					

					) AS sold 

					ON sold.store_id = s.`storeid` 

					

					

					

				  LEFT JOIN bs_store_items AS si 

					ON si.store_id = s.storeid 

				  LEFT JOIN bs_company 

					ON bs_company.companyid = s.companyid 

				  LEFT JOIN bs_company_branch 

					ON bs_company_branch.branchid = s.branchid 

					





						  $ex



						  GROUP BY s.`storeid`

						  ORDER BY s.`storeid` DESC  ";

        $q = $this->db->query($sql);
        $data = $q->result();
        $q->free_result();
        return $data;
    }

    function product_history($id = null, $type = null) {
        if ($type) {

            $exsql = "where si.supplier_id='$id' ";
        } else {

            $exsql = "where si.item_id='$id'";
        }


        $sql = "SELECT si.*,i.*,`bs_users`.`fullname`,bs_users.userid,bs_store.storeid,bs_store.storename,si.quantity AS ssquantity
                FROM `raw_store_items` AS si LEFT JOIN `raw_material` AS i ON i.id = si.`item_id` 
                LEFT JOIN `bs_users` ON `bs_users`.`userid` = `si`.`supplier_id` LEFT JOIN `bs_store` ON `bs_store`.`storeid` = `si`.`store_id` $exsql            

                GROUP BY si.`store_item_id`

                ORDER BY si.`store_item_id` DESC";
        /* $sql = "SELECT si.*,i.*,`bs_category`.`catname`,`bs_users`.`fullname`,bs_users.userid,bs_store.storeid,bs_store.storename,

          si.quantity as ssquantity,i.storeid as sitem

          FROM `raw_store_items` AS si



          LEFT JOIN `bs_item` AS i ON i.`itemid` = si.`item_id`



          LEFT JOIN `bs_category`

          ON `i`.`categoryid` = `bs_category`.`catid`

          LEFT JOIN `bs_users`

          ON `bs_users`.`userid` = `si`.`supplier_id`

          LEFT JOIN `bs_store`

          ON `bs_store`.`storeid` = `si`.`store_id`



          $exsql

          GROUP BY si.`store_item_id`

          ORDER BY si.`store_item_id` DESC"; */
//echo $sql;

        $q = $this->db->query($sql);
        return $q->result();
    }

    function product_history2($id = null, $type = null) {

        if ($type) {

            $exsql = "where si.supplier_id='$id' ";
        } else {
            $exsql = "where si.raw_id='$id'";
        }
        
        $sql = "SELECT 

                si.*,i.*,`bs_users`.`fullname`,bs_users.userid,bs_store.storeid,bs_store.storename,

                si.soled_quantity as ssquantity

                

                FROM `raw_soled_items` AS si

                

                LEFT JOIN `raw_material` AS i ON i.`id` = si.`raw_id`

                

               

		LEFT JOIN `bs_users` 

				ON `bs_users`.`userid` = `si`.`supplier_id`        

		LEFT JOIN `bs_store` 

				ON `bs_store`.`storeid` = `si`.`store_id` 

                                

                    $exsql            

                

                ORDER BY si.`raw_id` DESc";

       /* $sql = "SELECT 

                si.*,i.*,`bs_category`.`catname`,`bs_users`.`fullname`,bs_users.userid,bs_store.storeid,bs_store.storename,

                si.soled_quantity as ssquantity,i.storeid as sitem

                

                FROM `store_soled_items` AS si

                

                LEFT JOIN `bs_item` AS i ON i.`itemid` = si.`item_id`

                

                LEFT JOIN `bs_category` 

				ON `i`.`categoryid` = `bs_category`.`catid` 

		LEFT JOIN `bs_users` 

				ON `bs_users`.`userid` = `si`.`supplier_id`        

		LEFT JOIN `bs_store` 

				ON `bs_store`.`storeid` = `si`.`store_id` 

                                

                    $exsql            

                

                ORDER BY si.`item_id` DESC";*/
        
//echo $sql;
        //GROUP BY si.`item_id`

        $q = $this->db->query($sql);

        return $q->result();
    }



    function getPendingPurchase($userid) {

    



        $sql = "SELECT 

                bpi.*,

                u.userid,u.fullname

                

              FROM

                `an_purchase` AS bpi 







              LEFT JOIN `bs_users` AS u ON u.`userid` = bpi.`customer_id` 



              INNER JOIN `bs_purchase_items` AS ii ON ii.`purchase_id` = bpi.`purchase_id` and ii.invoice_itype='P'





              WHERE bpi.`confirm_status` = '0' 



              GROUP BY bpi.purchase_id



              ORDER BY bpi.`purchase_id` DESC ";

        $query = $this->db->query($sql);



        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 



        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    function get_purchase_items($id = null) {

        $sql = "SELECT 

                bpi.*,

                u.userid,u.fullname,

                s.storeid as ssid,

                s.storename,

                p.*,

                i.*

              FROM

                `bs_purchase_items` AS bpi 







              LEFT JOIN `bs_users` AS u ON u.`userid` = bpi.`supplier_id` 



              LEFT JOIN `bs_store` AS s ON s.`storeid` = bpi.`purchase_item_store_id`



              LEFT JOIN `an_purchase` AS p ON p.`purchase_id` = bpi.`purchase_id`

              

              LEFT JOIN `bs_item` AS i ON i.`itemid` = bpi.`purchase_item_id` 



              WHERE bpi.`purchase_id` = '$id'

                  

              and bpi.`confirm_status` = '0' 



              GROUP BY bpi.inovice_product_id



              ORDER BY bpi.`inovice_product_id` DESC ";

        $query = $this->db->query($sql);



        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 



        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    function inserData($table, $data) {

        $this->db->insert($table, $data);

        return $this->db->insert_id();
    }

    function getTransferListItemsIds($id) {
        $sql = "SELECT
  GROUP_CONCAT(ti.`store_item_id`) AS totalitems
FROM
  `transfer_items` AS ti
  WHERE ti.`transfer_id` = '" . $id . "'
GROUP BY ti.`transfer_id` ";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->row();
        } else {

            return false;
        }
    }

    function getTransferlist() {
        $sql = "SELECT
              t.*,
              COUNT(ti.`transfer_id`) AS totalcount,
              u.`fullname`,
              s.`storename` AS storefrom,
              s2.`storename` AS storeto
            FROM
              `transfer` AS t
              INNER JOIN `transfer_items` AS ti
                ON ti.`transfer_id` = t.`transfer_id`
              INNER JOIN `bs_users` AS u
                ON u.`userid` = t.`user_id`
              INNER JOIN `bs_store` AS s
                ON s.`storeid` = t.`transfer_from`
              INNER JOIN `bs_store` AS s2
                ON s2.`storeid` = t.`transfer_to`
            WHERE t.`delete_record` = '0'
            GROUP BY ti.`transfer_id` ";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    function product_list_store_transfer($ids) {

        $sql = "select si.*,s1.*,s2.*, i.itemname,i.itemid,i.serialnumber,i.itemtype,i.product_picture,

                c.catname,c.catid,s1.storename as sname1,s2.storename as sname2 

                from bs_store_items as si 

            

                LEFT JOIN bs_item as i ON i.itemid=si.item_id

                LEFT JOIN bs_category as c ON c.catid=i.categoryid

                

                left join bs_store as s1 on s1.storeid=si.store_id

                left join bs_store as s2 on s2.storeid=si.fromstore



                where si.transfer='1' and store_item_id IN($ids)";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    function getTransferByDataId($id) {
        $sql = "SELECT t.*,u.`fullname` FROM `transfer` AS t
INNER JOIN `bs_users` AS u ON u.`userid` = t.`user_id`
WHERE t.`transfer_id` = '" . $id . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {

            return $query->row();
        } else {

            return false;
        }
    }

    function getTransferPrint($id) {
        $sql = "SELECT
  si.*,
  s1.*,
  s2.*,
  i.itemname,
  i.id,
  i.barecode,
unit_title,
  
  i.image,
    si.`quantity` AS qq,

  s1.storename AS sname1,
  s2.storename AS sname2 
FROM
  raw_store_items AS si 
  LEFT JOIN raw_material AS i 
    ON i.id = si.item_id 
  left join units on units.unit_id = i.unit_id
  LEFT JOIN bs_store AS s1 
    ON s1.storeid = si.store_id 
  LEFT JOIN bs_store AS s2 
    ON s2.storeid = si.fromstore 
WHERE si.transfer = '1' AND si.`store_item_id` IN($id)";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    function store_item_all($id) {

        $sql = "select quantity,store_item_id from bs_store_items where store_item_id='$id'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->row()->quantity;
        } else {

            return false;
        }
    }

    function get_sold_it($sid) {

        $sql = "select soled_quantity,store_item_id,soled_id  from store_soled_items where store_item_id='$sid'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->row();
        } else {

            return false;
        }
    }

    function get_store_it($sid, $itid) {

        /* $sql="select 

          SUM(si.quantity) AS squan,

          si.store_item_id,

          si.store_id,si.item_id,

          remain,

          soldquant,

          si.quantity-soldquant as nquantity

          from bs_store_items as si

          LEFT JOIN (

          SELECT SUM(store_soled_items.soled_quantity) AS soldquant, store_soled_items.*

          FROM store_soled_items

          GROUP BY store_soled_items.`store_id`



          ) soldi

          ON soldi.item_id = si.item_id

          AND soldi.store_id = si.store_id





          where si.store_id='$sid' and si.item_id='$itid' GROUP BY si.`store_id` ";

         */



        $sql = "select 

                si.* ,

                soldi.*,

                soldi.soled_quantity as soled_quantity

                from bs_store_items as si



                LEFT JOIN  store_soled_items as soldi ON soldi.item_id = '$itid' AND soldi.store_id = '$sid' AND soldi.supplier_id=si.supplier_id



                    

                where si.store_id='$sid' and si.item_id='$itid'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    function get_store_it_next($sid, $itid, $prev) {

        //$sql="select quantity,store_item_id,store_id,item_id,remain  from bs_store_items where store_id='$sid' and item_id='$itid' and quantity >'0'";

        $sql = "select si.quantity,ss.soled_quantity,ss.store_item_id as ssid,si.store_item_id,si.store_id,si.item_id,remain,si.quantity-ss.soled_quantity as nquantity  "
                . "from bs_store_items as si"
                . " LEFT JOIN store_soled_items as ss ON ss.store_item_id=si.store_item_id"
                . " where si.store_id='$sid' and si.item_id='$itid'  and si.confirm_status='0' ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->next_row();
        } else {

            return false;
        }
    }

    function get_store_it_previous_row($sid, $itid) {

        $sql = "select quantity,store_item_id,store_id,item_id,remain  from bs_store_items where store_id='$sid' and item_id='$itid'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->previous_row();
        } else {

            return false;
        }
    }

    function getStoreItemsToSole0($itemid, $storeid) {

        $sql = "SELECT 

		  st.`item_id`,

		  st.*,

		  @t4 := IF(

			st.`quantity` IS NULL,

			0,

			st.`quantity`

		  ) - IF(

			ssi.`soled_quantity` IS NULL,

			0,

			ssi.`soled_quantity`

		  ) AS remaining ,

		  bpi.`purchase_item_price`

		FROM

		  `bs_store_items` AS st 

		  LEFT JOIN `store_soled_items` AS ssi 

			ON ssi.`store_item_id` = st.`store_item_id` 

		  INNER JOIN `bs_purchase_items` AS bpi 

			ON bpi.`purchase_id` = st.`purchase_id` 

		WHERE st.`item_id` = '" . $itemid . "' 

		  AND IF(

			st.`quantity` IS NULL,

			0,

			st.`quantity`

		  ) - IF(

			ssi.`soled_quantity` IS NULL,

			0,

			ssi.`soled_quantity`

		  ) != 0 AND bpi.`confirm_status` = '1'

		  AND  st.`store_id` = '" . $storeid . "'

                      

                    GROUP BY st.store_item_id

		ORDER BY st.`store_item_id` ASC ";

        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $q->result();
        } else {

            return false;
        }
    }

    function getStoreItemsToSole($itemid, $storeid, $si = null) {

        $sql = "SELECT 

		  st.`item_id`,

		  st.*,

		  @t4 := IF(

			st.`quantity` IS NULL,

			0,

			st.`quantity`

		  ) - IF(

			ssi.`soled_quantity` IS NULL,

			0,

			ssi.`soled_quantity`

		  ) AS remaining ,

		  bpi.`purchase_item_price`

		FROM

		  `bs_store_items` AS st 

		  LEFT JOIN `store_soled_items` AS ssi 

			ON ssi.`store_item_id` = st.`store_item_id` 

		  INNER JOIN `bs_purchase_items` AS bpi 

			ON bpi.`purchase_id` = st.`purchase_id` 

		WHERE st.`item_id` = '" . $itemid . "' 

		  AND IF(

			st.`quantity` IS NULL,

			0,

			st.`quantity`

		  ) - IF(

			ssi.`soled_quantity` IS NULL,

			0,

			ssi.`soled_quantity`

		  ) != 0 AND bpi.`confirm_status` = '1'

		  AND  st.`store_id` = '" . $storeid . "'

		  AND  st.`store_item_id` = '" . $si . "'

                      

                    GROUP BY st.store_item_id

		ORDER BY st.`store_item_id` ASC ";

        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $q->row();
        } else {

            return false;
        }
    }

    function getStoreItemsToSole2($itemid, $storeid) {

        //$sql="select quantity,store_item_id,store_id,item_id,remain  from bs_store_items where store_id='$sid' and item_id='$itid' and quantity >'0'";

        $sql = "SELECT 

		  st.`item_id`,

		  st.*,

		  @t4 := IF(

			st.`quantity` IS NULL,

			0,

			st.`quantity`

		  ) - IF(

			ssi.`soled_quantity` IS NULL,

			0,

			ssi.`soled_quantity`

		  ) AS remaining ,

		  bpi.`purchase_item_price`

		FROM

		  `bs_store_items` AS st 

		  LEFT JOIN `store_soled_items` AS ssi 

			ON ssi.`store_item_id` = st.`store_item_id` 

		  INNER JOIN `bs_purchase_items` AS bpi 

			ON bpi.`purchase_id` = st.`purchase_id` 

		WHERE st.`item_id` = '" . $itemid . "' 

		  AND IF(

			st.`quantity` IS NULL,

			0,

			st.`quantity`

		  ) - IF(

			ssi.`soled_quantity` IS NULL,

			0,

			ssi.`soled_quantity`

		  ) != 0 AND bpi.`confirm_status` = '1'

		  AND  st.`store_id` = '" . $storeid . "'

                      

                    GROUP BY st.store_item_id

		ORDER BY st.`store_item_id` ASC ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->next_row();
        } else {

            return false;
        }
    }

    function get_purchase_items_re($id = null) {

        $sql = "SELECT 

                bpi.*,

                u.userid,u.fullname,

                s.storeid as ssid,

                s.storename,

                p.*,

                i.*

              FROM

                `bs_purchase_items` AS bpi 







              LEFT JOIN `bs_users` AS u ON u.`userid` = bpi.`supplier_id` 



              LEFT JOIN `bs_store` AS s ON s.`storeid` = bpi.`purchase_item_store_id`



              LEFT JOIN `an_purchase` AS p ON p.`purchase_id` = bpi.`purchase_id`

              

              LEFT JOIN `bs_item` AS i ON i.`itemid` = bpi.`purchase_item_id` 



              WHERE bpi.`purchase_id` = '$id'

                  

              and bpi.`confirm_status` = '1'



              GROUP BY bpi.inovice_product_id



              ORDER BY bpi.`inovice_product_id` DESC ";

        $query = $this->db->query($sql);



        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 



        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    function getSalesInvoiceItems2($id) {

        /* $sql= 'SELECT i.*,piv.*, piv.`id` AS piv_id FROM `purchase_invoice_items` AS piv

          INNER JOIN `bs_item`  AS i

          ON i.`itemid` = piv.`product_id`

          WHERE piv.`invoice_id`='.$inv_id.''; */

        /* $sql ="SELECT piv.`store_id` AS store, i.*,piv.*, piv.`id` AS piv_id, s.`storename` FROM `sales_invoice_items` AS piv

          INNER JOIN `bs_item`  AS i

          ON i.`itemid` = piv.`product_id`

          INNER JOIN `bs_store` AS s ON s.`storeid`=piv.`store_id`

          WHERE piv.`invoice_id`=".$inv_id."" */;

        /* $sql = 'SELECT 

          storeitem,

          s.`ownerid` AS owneri,

          sii.`quantity` AS qt,

          sii.*,

          ss.* ,

          u.*,

          i.*

          FROM

          `sales_invoice_items` AS sii

          INNER JOIN `bs_sales_invoice` AS ss

          ON ss.`invoiceid` = sii.`invoice_id`

          INNER JOIN `bs_store` AS s

          ON s.`storeid` = sii.`store_id`

          INNER JOIN `bs_users` AS u ON u.`userid` = ss.`invoiceid`

          LEFT JOIN

          (SELECT

          bs_store_items.`store_id`,

          SUM(bs_store_items.`item_id`) AS storeitem,

          bs_store_items.`item_id`

          FROM

          `bs_store_items`

          INNER JOIN `bs_store` AS ss

          ON ss.`storeid` = bs_store_items.`store_id`

          GROUP BY bs_store_items.`item_id`,

          ss.`storeid`) AS bsi

          ON bsi.item_id = sii.`product_id`

          INNER JOIN `bs_item` AS i ON i.`itemid` = sii.`product_id`

          WHERE s.`ownerid` = ' . $inv_id . ' AND  ss.`confirm_status` = "0"

          GROUP BY s.`storeid`';

          $query = $this->db->query($sql);

          if ($query->num_rows() > 0) {

          return $query->result();

          } else {

          return false;

          }

         */



        $sql = "SELECT 

                bpi.*,

                bpi.invoice_item_store_id as sid,

                u.userid,u.fullname,

                s.storeid,

                s.storename,

                p.*,

                i.*

              FROM

                `bs_invoice_items` AS bpi 



              LEFT JOIN `an_invoice` AS p ON p.`invoice_id` = bpi.`invoice_id` 



              LEFT JOIN `bs_users` AS u ON u.`userid` = p.`customer_id` 



              LEFT JOIN `bs_store` AS s ON s.`storeid` = bpi.`invoice_item_store_id`



              

              

              LEFT JOIN `bs_item` AS i ON i.`itemid` = bpi.`invoice_item_id` 



              WHERE bpi.`invoice_id` = '$id' 

              

              and bpi.`confirm_status` = '1' 



              GROUP BY bpi.inovice_product_id

              



              ORDER BY bpi.`inovice_product_id` DESC ";

        $query = $this->db->query($sql);



        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 



        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    /* mahmoud */

    public function add_raw_material($file_name, $id = null) {
//itemname[arabic]
        $itemname['arabic'] = $this->input->post('itemname_arabic');
        $itemname['english'] = $this->input->post('itemname_english');
        $data = array(
            'itemname' => serialize($itemname),
            //'name_en' => $this->input->post('itemname_english'),
            //'category_id' => $this->input->post('categoryid'),
            'unit_id' => $this->input->post('type_unit'),
            'image' => $file_name,
            'shelf' => $this->input->post('shelf'),
            'price' => $this->input->post('purchase_price'),
            'barecode' => $this->input->post('barcodenumber'),
            //'status' => $this->input->post('product_status'),
            'limit' => $this->input->post('re_order'),
            'auto_name' => $this->input->post('itemname_arabic')
        );

        if ($id == null) {
            return $this->db->insert('raw_material', $data);
        } else {
            $this->db->where('id', $id);
            return $this->db->update('raw_material', $data);
        }
    }

    public function finishproduct_list() {

        $this->db->join('units', 'units.unit_id =raw_material.unit_id');
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('raw_material');
        return $query->result();
    }

    public function get_raw_material($id) {
        $query = $this->db->get_where('raw_material', array('id' => $id));
        return $query->row();
    }

    public function add_assign_dishes($arr, $id) {
        if ($id != null) {
            $this->db->where(array('item_id' => $id));
            $this->db->delete('dish_content');
        } else {
            $id = $this->input->post('itemid');
        }
        $size = $this->input->post('dish-size');
        foreach ($arr as $key => $value) {
            if ($value) {
                $data = array(
                    'item_id' => $id,
                    'raw_id' => $key,
                    'qty' => $value,
                    'size' => $size ? $size : 'large'
                );
                $this->db->insert('dish_content', $data);
            }
        }
    }

    function updateStoreValue($store_items) {
        $quantity = $store_items['quantity'];
        $id = $store_items['item_id'];
        $fromstore = $store_items['fromstore'];
        $oldquantity = $store_items['oldquantity'];
        $query = "update store_value set totalq = totalq+$oldquantity-$quantity where item_id = $id and store_id = $fromstore";
        $this->db->query($query);
        $sql = $this->db->get_where('store_value', array('store_id' => $store_items['store_id'], 'item_id' => $id));
        if ($sql->num_rows() > 0) {
            $store_id = $store_items['store_id'];
            $query = "update store_value set totalq = totalq-$oldquantity+$quantity where item_id = $id and store_id = $store_id";
            $this->db->query($query);
        } else {
            $this->db->insert('store_value', array('totalq' => $quantity, 'store_id' => $store_items['store_id'], 'item_id' => $id));
        }
    }

    public function get_transfer_list($id) {
        $sub = "";
        if ($id != null) {
            $sub = "where transfer.transfer_id = $id";
        }
        $query = "SELECT transfer.transfer_id,`raw_material`.`itemname`,sv2.`totalq` AS totalq1,sv1.`totalq` AS totalq2,`transfer_qty`,`unit_title`,`percent`,st1.`storename` AS st1n,st2.`storename` AS st2n,`transfer_from`,`transfer_to`,raw_material.id as raw_id,raw_material.price,`lossed_items`.`quantity` AS losed_qty,transfer.created
FROM `transfer_items` 
INNER JOIN `transfer` ON `transfer_items`.`transfer_id` = `transfer`.`transfer_id`
INNER JOIN `raw_store_items` ON `raw_store_items`.`store_item_id` = `transfer_items`.`store_item_id`
INNER JOIN `store_value` AS sv1 ON sv1.`store_id` = transfer.`transfer_to` AND `raw_store_items`.`item_id` = sv1.`item_id`
INNER JOIN `store_value` AS sv2 ON sv2.`store_id` = `transfer`.`transfer_from` AND `raw_store_items`.`item_id` = sv2.`item_id`
INNER JOIN `raw_material` ON `raw_material`.id = `raw_store_items`.`item_id`
INNER JOIN `units` ON `units`.`unit_id` = `raw_material`.`unit_id`
INNER JOIN `bs_store` st1 ON st1.`storeid` = transfer_from
INNER JOIN `bs_store` st2 ON st2.`storeid` = transfer_to
LEFT JOIN `lossed_items` ON `lossed_items`.`transfer_id` = `transfer`.`transfer_id` and `lossed_items`.`raw_id` = `raw_material`.`id` $sub ORDER BY transfer.`transfer_id` DESC";

        $result = $this->db->query($query);

        return $result->result();
    }

}
