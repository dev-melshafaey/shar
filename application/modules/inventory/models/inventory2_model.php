<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inventory2_model extends CI_Model {
    /*
     * Properties
     */

//----------------------------------------------------------------------

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();

        //Load Table Names from Config
    }

    public function re_order_list() {
        $ownerid = ownerid();
        $this->db->select('bs_point_of_re_order.poid,
		bs_point_of_re_order.quanitity,
		bs_point_of_re_order.unit_price,
		bs_point_of_re_order.paid_date,
		bs_item.serialnumber,
		bs_item.itemname,
		bs_item.point_of_re_order');
        $this->db->from('bs_point_of_re_order');
        $this->db->join('bs_item', 'bs_item.itemid = bs_point_of_re_order.productid');
        $this->db->where('bs_point_of_re_order.ownerid', $ownerid);
        $this->db->order_by("bs_point_of_re_order.paid_date", "DESC");
        $query = $this->db->get();
        return $query->result();
    }

    public function categorylist() {
        $ownerid = ownerid();
        $this->db->select('bs_category.catid,bs_category.catname,bs_category.catdescription,bs_category.catstatus,bs_company.company_name,bs_category.cattype');
        $this->db->from($this->config->item('table_cat'));
        $this->db->join('bs_company', 'bs_company.companyid = bs_category.companyid', 'LEFT');
        //$this->db->where('bs_category.ownerid', $ownerid);
        $this->db->order_by("bs_category.catname", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getPendingSales($userid) {
        /*
          $sql = 'SELECT
          s.`ownerid` AS owneri,
          ss.`invoiceid` AS invoice,
          sii.*,
          ss.*,
          u.*
          FROM
          `sales_invoice_items` AS sii
          INNER JOIN `bs_sales_invoice` AS ss
          ON ss.`invoiceid` = sii.`invoice_id`
          INNER JOIN `bs_store` AS s
          ON s.`storeid` = sii.`store_id`
          INNER JOIN `bs_users` AS u
          ON u.`userid` = ss.`customerid`
          WHERE s.`ownerid` = ' . $userid . '
          AND ss.`confirm_status` = "0" ';
          $query = $this->db->query($sql);
          if ($query->num_rows() > 0) {
          return $query->result();
          } else {
          return false;
          }
         */
        
        //echo $this->session->userdata('bs_storemanager');
        $sql = "SELECT 
                    bpi.*,
                    u.userid,u.fullname

                  FROM
                    `an_invoice` AS bpi 



                  LEFT JOIN `bs_users` AS u ON u.`userid` = bpi.`customer_id` 
                  
                  INNER JOIN `bs_invoice_items` AS ii ON ii.`invoice_id` = bpi.`invoice_id` and ii.invoice_itype='P' and ii.invoice_item_store_id='".$this->session->userdata('bs_storemanager')."'

                  


                  WHERE bpi.`confirm_status` = '0' AND bpi.invoice_type='itemscharges'

                  GROUP BY bpi.invoice_id

                  ORDER BY bpi.`invoice_id` DESC ";
        
        
        $query = $this->db->query($sql);

        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getSalesInvoiceItems($id) {
        /* $sql= 'SELECT i.*,piv.*, piv.`id` AS piv_id FROM `purchase_invoice_items` AS piv
          INNER JOIN `bs_item`  AS i
          ON i.`itemid` = piv.`product_id`
          WHERE piv.`invoice_id`='.$inv_id.''; */
        /* $sql ="SELECT piv.`store_id` AS store, i.*,piv.*, piv.`id` AS piv_id, s.`storename` FROM `sales_invoice_items` AS piv
          INNER JOIN `bs_item`  AS i
          ON i.`itemid` = piv.`product_id`
          INNER JOIN `bs_store` AS s ON s.`storeid`=piv.`store_id`
          WHERE piv.`invoice_id`=".$inv_id."" */;
        /* $sql = 'SELECT 
          storeitem,
          s.`ownerid` AS owneri,
          sii.`quantity` AS qt,
          sii.*,
          ss.* ,
          u.*,
          i.*
          FROM
          `sales_invoice_items` AS sii
          INNER JOIN `bs_sales_invoice` AS ss
          ON ss.`invoiceid` = sii.`invoice_id`
          INNER JOIN `bs_store` AS s
          ON s.`storeid` = sii.`store_id`
          INNER JOIN `bs_users` AS u ON u.`userid` = ss.`invoiceid`
          LEFT JOIN
          (SELECT
          bs_store_items.`store_id`,
          SUM(bs_store_items.`item_id`) AS storeitem,
          bs_store_items.`item_id`
          FROM
          `bs_store_items`
          INNER JOIN `bs_store` AS ss
          ON ss.`storeid` = bs_store_items.`store_id`
          GROUP BY bs_store_items.`item_id`,
          ss.`storeid`) AS bsi
          ON bsi.item_id = sii.`product_id`
          INNER JOIN `bs_item` AS i ON i.`itemid` = sii.`product_id`
          WHERE s.`ownerid` = ' . $inv_id . ' AND  ss.`confirm_status` = "0"
          GROUP BY s.`storeid`';
          $query = $this->db->query($sql);
          if ($query->num_rows() > 0) {
          return $query->result();
          } else {
          return false;
          }
         */

        $sql = "SELECT 
                bpi.*,
                bpi.invoice_item_store_id as sid,
                u.userid,u.fullname,
                s.storeid,
                s.storename,
                p.*,
                i.*
              FROM
                `bs_invoice_items` AS bpi 

              LEFT JOIN `an_invoice` AS p ON p.`invoice_id` = bpi.`invoice_id` 

              LEFT JOIN `bs_users` AS u ON u.`userid` = p.`customer_id` 

              LEFT JOIN `bs_store` AS s ON s.`storeid` = bpi.`invoice_item_store_id`

              
              
              LEFT JOIN `bs_item` AS i ON i.`itemid` = bpi.`invoice_item_id` 

              WHERE bpi.`invoice_id` = '$id' and bpi.invoice_item_store_id='".$this->session->userdata('bs_storemanager')."'
              
              and bpi.`confirm_status` = '0' 

              GROUP BY bpi.inovice_product_id
              

              ORDER BY bpi.`inovice_product_id` DESC ";
        $query = $this->db->query($sql);

        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getSalesInvoiceItems2($id) {
        /* $sql= 'SELECT i.*,piv.*, piv.`id` AS piv_id FROM `purchase_invoice_items` AS piv
          INNER JOIN `bs_item`  AS i
          ON i.`itemid` = piv.`product_id`
          WHERE piv.`invoice_id`='.$inv_id.''; */
        /* $sql ="SELECT piv.`store_id` AS store, i.*,piv.*, piv.`id` AS piv_id, s.`storename` FROM `sales_invoice_items` AS piv
          INNER JOIN `bs_item`  AS i
          ON i.`itemid` = piv.`product_id`
          INNER JOIN `bs_store` AS s ON s.`storeid`=piv.`store_id`
          WHERE piv.`invoice_id`=".$inv_id."" */;
        /* $sql = 'SELECT 
          storeitem,
          s.`ownerid` AS owneri,
          sii.`quantity` AS qt,
          sii.*,
          ss.* ,
          u.*,
          i.*
          FROM
          `sales_invoice_items` AS sii
          INNER JOIN `bs_sales_invoice` AS ss
          ON ss.`invoiceid` = sii.`invoice_id`
          INNER JOIN `bs_store` AS s
          ON s.`storeid` = sii.`store_id`
          INNER JOIN `bs_users` AS u ON u.`userid` = ss.`invoiceid`
          LEFT JOIN
          (SELECT
          bs_store_items.`store_id`,
          SUM(bs_store_items.`item_id`) AS storeitem,
          bs_store_items.`item_id`
          FROM
          `bs_store_items`
          INNER JOIN `bs_store` AS ss
          ON ss.`storeid` = bs_store_items.`store_id`
          GROUP BY bs_store_items.`item_id`,
          ss.`storeid`) AS bsi
          ON bsi.item_id = sii.`product_id`
          INNER JOIN `bs_item` AS i ON i.`itemid` = sii.`product_id`
          WHERE s.`ownerid` = ' . $inv_id . ' AND  ss.`confirm_status` = "0"
          GROUP BY s.`storeid`';
          $query = $this->db->query($sql);
          if ($query->num_rows() > 0) {
          return $query->result();
          } else {
          return false;
          }
         */

        $sql = "SELECT 
                bpi.*,
                bpi.invoice_item_store_id as sid,
                u.userid,u.fullname,
                s.storeid,
                s.storename,
                p.*,
                i.*
              FROM
                `bs_invoice_items` AS bpi 

              LEFT JOIN `an_invoice` AS p ON p.`invoice_id` = bpi.`invoice_id` 

              LEFT JOIN `bs_users` AS u ON u.`userid` = p.`customer_id` 

              LEFT JOIN `bs_store` AS s ON s.`storeid` = bpi.`invoice_item_store_id`

              
              
              LEFT JOIN `bs_item` AS i ON i.`itemid` = bpi.`invoice_item_id` 

              WHERE bpi.`invoice_id` = '$id' and bpi.invoice_item_store_id='".$this->session->userdata('bs_storemanager')."'
              
              and bpi.`confirm_status` = '1' 

              GROUP BY bpi.inovice_product_id
              

              ORDER BY bpi.`inovice_product_id` DESC ";
        $query = $this->db->query($sql);

        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function product_list($id) {
        $subq = "";
        /* if($id !=""){
          $subq = " AND bs_store.storeid =  '".$id."'";
          } */
        /*
          SELECT
          `bs_category`.`catname`,
          `bs_item`.`storeid`,
          `bs_suppliers`.`suppliername`,
          `bs_store`.`storename`,
          `bs_item`.`itemid`,
          `bs_item`.`itemname`,
          `bs_item`.`itemtype`,
          IF(
          bs_item.itemtype = 'P',
          `bs_item`.`serialnumber`,
          '-----'
          ) AS `serialnumber`,
          IF(
          bs_item.itemtype = 'P',
          CONCAT(`bs_item`.`purchase_price`,' RO.'),
          '-----'
          ) AS `purchase_price`,
          IF(
          bs_item.itemtype = 'P',
          CONCAT(`bs_item`.`sale_price`,' RO.'),
          '-----'
          ) AS `sale_price`,
          IF(
          bs_item.itemtype = 'P',
          `bs_item`.`min_sale_price`,
          '-----'
          ) AS `min_sale_price`,
          IF(
          bs_item.itemtype = 'P',
          `bs_item`.`point_of_re_order`,
          '-----'
          ) AS `point_of_re_order`,
          IF(
          bs_item.itemtype = 'P',
          `bs_item`.`quantity`,
          '-----'
          ) AS `quantity`,
          `bs_item`.`product_picture`
          FROM
          (`bs_item`)
          JOIN `bs_category`
          ON `bs_item`.`categoryid` = `bs_category`.`catid`
          JOIN `bs_store`
          ON `bs_item`.`storeid` = `bs_store`.`storeid`
          JOIN `bs_suppliers`
          ON `bs_item`.`suppliderid` = `bs_suppliers`.`supplierid`
          WHERE `bs_item`.`ownerid` = '".$ownerid."' ".$subq." ORDER BY `bs_item`.`itemname` ASC "
         */
        $sql = "SELECT 
			  `bs_category`.`catname`,
			  `bs_item`.`itemid`,
			  `bs_item`.`suppliderid`,
			  `bs_item`.`itemname`,
			  `bs_item`.`itemtype`,
			  `bs_item`.`storeid`,
			  `bs_item`.`submissiondate`,
                          `bs_item`.`re_order`,
                           bs_item.barcodenumber,
			  IF(
				bs_item.serialnumber != '',
				`bs_item`.`serialnumber`,
				'-----'
			  ) AS `serialnumber`,
			  IF(
				bs_item.purchase_price != '',
				CONCAT(`bs_item`.`purchase_price`,''),
				'-----'
			  ) AS `purchase_price`,
			  IF(
				bs_item.sale_price != '',
				CONCAT(`bs_item`.`sale_price`,''),
				'-----'
			  ) AS `sale_price`,
			  IF(
				bs_item.min_sale_price != '',
				`bs_item`.`min_sale_price`,
				'-----'
			  ) AS `min_sale_price`,
			  IF(
				bs_item.point_of_re_order != '',
				`bs_item`.`point_of_re_order`,
				'-----'
			  ) AS `point_of_re_order`,
			  IF(
				bs_item.quantity != '',
				`bs_item`.`quantity`,
				'-----'
			  ) AS `quantity`,
			  `bs_item`.`product_picture` 
			FROM
			  (`bs_item`) 
			  LEFT JOIN `bs_category` 
				ON `bs_item`.`categoryid` = `bs_category`.`catid` 
                                
			 ORDER BY `bs_item`.`itemid` DESC ";
        $ownerid = ownerid();
        $query = $this->db->query($sql);
        //$this->db->last_query();
        return $query->result();
    }

    public function cat_detail($catid) {
        $this->db->where('catid', $catid);
        $query = $this->db->get($this->config->item('table_cat'));
        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function get_item_detail($itemid) {
        $this->db->where('itemid', $itemid);
        $query = $this->db->get($this->config->item('table_item'));
        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function delete_categories() {

        $u = $this->input->post('u');
        if ($u != '') {
            $this->db->where_in('catid', $u);
            $this->db->delete($this->config->item('table_cat'));
        }
        do_redirect('categories?e=12');
    }

    public function bulk_point_or_reorders() {

        $u = $this->input->post('u');
        if ($u != '') {
            $this->db->where_in('poid', $u);
            $this->db->delete('bs_point_of_re_order');
        }
        do_redirect('point_reorder?e=12');
    }

    public function delete_stores() {

        $u = $this->input->post('u');
        if ($u != '') {
            $this->db->where_in('storeid', $u);
            $this->db->delete($this->config->item('table_store'));
        }
        do_redirect('stores?e=12');
    }

    public function add_update_category() {
        $data = $this->input->post();
        $catid = $this->input->post('catid');
        $data['catname'] = serialize(post('catname'));
        unset($data['sub_mit'], $data['sub_reset'], $data['catid']);
        if ($catid != '') {
            $this->db->where('catid', $catid);
            $this->db->update($this->config->item('table_cat'), $data);
            do_redirect('categories?e=10');
        } else {
            $this->db->insert($this->config->item('table_cat'), $data);
            do_redirect('categories?e=10');
        }
    }

    public function addStore() {
        $data = $this->input->post();
        $storeid = $this->input->post('storeid');
        $data['storename'] = serialize(post('storename'));
        unset($data['store_submit'], $data['sub_reset'], $data['storeid'], $data['sub_mit']);
        

        
        
        if ($storeid != '') {
            $this->db->where('storeid', $storeid);
            $this->db->update($this->config->item('table_store'), $data);
            
            $udata=array('storemanager'=>$storeid);
            $this->db->where('userid', post('storemanager'));
            $this->db->update('bs_users', $udata);
            
            //var_dump($udata);
            //die(post('storemanager'));
            do_redirect('stores?e=10');
        } else {
            $this->db->insert($this->config->item('table_store'), $data);
            $storeid=$this->db->insert_id();
            
            
            $udata=array('storemanager'=>$storeid);
            $this->db->where('userid', post('storemanager'));
            $this->db->update('bs_users', $udata);
            
            //die();
            do_redirect('stores?e=10');
        }
    }

    public function add_new_item() {
        $data = $this->input->post();

        $itemid = $this->input->post('itemid');
        $path = APPPATH . '../uploads/item/' . $this->input->post('storeid') . '/';
        $file_name = upload_file('product_picture', $path, true, 50, 50);
        if ($file_name != '') {
            $data['product_picture'] = $file_name;
        }
        $data['itemname'] = serialize(post('itemname'));


        if (post('get_option_p') == "0") {

            $data['sale_price'] = post('cal_sale_price');
            $data['salingbytotal'] = post('cal_salingbytotal');
            $data['min_sale_price'] = post('cal_min_sale_price');
        }

        unset($data['submit_item'], $data['sub_mit'], $data['sub_reset'], $data['itemid'], $data['cal_purchase_price'], $data['get_option_p'], $data['offer_dicounte']);
        unset($data['cal_sale_price'], $data['cal_salingbytotal'], $data['cal_min_sale_price']);

        if ($itemid != '') {
            $this->db->where('itemid', $itemid);
            $this->db->update($this->config->item('table_item'), $data);
            do_redirect('items?e=10');
        } else {

            $this->db->insert($this->config->item('table_item'), $data);
            do_redirect('items?e=10');
        }
    }

    public function add_new_point_of_order() {
        $data = $this->input->post();
        $poid = $this->input->post('poid');
        $path = APPPATH . '../uploads/point_of_re_order/' . $this->input->post('storeid') . '/';
        $attachment = upload_file('attachment', $path, true, 50, 50);
        if ($attachment != '') {
            $data['attachment'] = $attachment;
        }
        unset($data['submit_reorder'], $data['sub_reset'], $data['poid'], $data['serialnumber']);
        if ($itemid != '') {
            $this->db->where('poid', $poid);
            $this->db->update('bs_point_of_re_order', $data);
            do_redirect('point_reorder?e=10');
        } else {
            ;
            $this->db->insert('bs_point_of_re_order', $data);
            do_redirect('point_reorder?e=10');
        }
    }

    public function add_supplier() {
        $data = $this->input->post();
        $supplierid = $this->input->post('supplierid');
        unset($data['submit_supplier'], $data['sub_reset']);
        if ($supplierid != '') {
            $this->db->where('supplierid', $supplierid);
            $this->db->update($this->config->item('table_suppliers'), $data);
            echo('1');
        } else {
            $this->db->insert($this->config->item('table_suppliers'), $data);
            echo('1');
        }
    }

    public function get_store_detail($storeid) {
        $this->db->where('storeid', $storeid);
        $query = $this->db->get($this->config->item('table_store'));
        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    function purchase_receipt() {
        /*    $sql = "SELECT 
          bpi.*,
          u.userid,u.fullname,
          s.storeid,
          s.storename,
          sum(pi.purchase_item_quantity) as quantity,
          sum(pp.payment_amount) as paid

          FROM
          `an_purchase` AS bpi



          LEFT JOIN `bs_users` AS u ON u.`userid` = bpi.`customer_id`

          LEFT JOIN `bs_store` AS s ON s.`storeid` = bpi.`purchase_item_store_id`

          LEFT JOIN `bs_purchase_items` AS pi ON pi.`purchase_id` = bpi.`purchase_id`

          LEFT JOIN `purchase_payments` AS pp ON pp.`purchase_id` = bpi.`purchase_id`


          WHERE bpi.`confirm_status` = '1'

          GROUP BY bpi.purchase_id

          ORDER BY bpi.`purchase_id` DESC ";
         */
        $sql = "SELECT 
                    si.*,
                    u.userid,u.fullname,
                    s.storeid,
                    s.storename,
                    sum(si.quantity) as quantity
                  FROM
                    `bs_store_items` AS si 



                  LEFT JOIN `bs_users` AS u ON u.`userid` = si.`supplier_id` 

                  LEFT JOIN `bs_store` AS s ON s.`storeid` = si.`store_id`
                  
                  WHERE si.`transfer` = '0' 

                  GROUP BY si.purchase_id

                  ORDER BY si.`store_item_id` DESC ";
        $query = $this->db->query($sql);

        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function sales_recept() {
        /* $sql = "SELECT 
          pr.*
          FROM
          `sales_receipt` AS pr
          INNER JOIN `sales_confirm_items` AS pci
          ON pci.`receipt_id` = pr.`purchase_receipt_id`
          GROUP BY pci.`invoice_id` ";
          $q = $this->db->query($sql);
          return $q->result();
         */
        $sql = "SELECT 
                    si.*,
                    u.userid,u.fullname,
                    s.storeid,
                    s.storename,
                    sum(si.soled_quantity) as quantity,
                    sum(pp.payment_amount) as paid,
                    pi.itemid,pi.itemname,pi.product_picture,
                    ii.invoice_id,ii.invoice_item_price
                  FROM
                    `store_soled_items` AS si 



                  LEFT JOIN `bs_users` AS u ON u.`userid` = si.`userid` 

                  LEFT JOIN `bs_store` AS s ON s.`storeid` = si.`store_id`
                  
                  LEFT JOIN `bs_item` AS pi ON pi.`itemid` = si.`item_id`
                  
                  LEFT JOIN `bs_invoice_items` AS ii ON ii.`invoice_id` = si.`recipt_id`
                  
                  LEFT JOIN `invoice_payments` AS pp ON pp.`invoice_id` = si.`recipt_id`

                  GROUP BY si.item_id

                  ORDER BY si.`store_item_id` DESC  ";
        $query = $this->db->query($sql);

        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getitems($invoiceid) {
        $sql = "SELECT pii.`purchase_id`, s.`storename`, i.`itemname`,pii.* FROM `bs_purchase_items` AS pii
                            INNER JOIN `bs_item` AS i ON i.`itemid` = pii.`purchase_item_id`
                            INNER JOIN `bs_store` AS s ON s.`storeid` = pii.`purchase_item_store_id`
                            WHERE pii.`purchase_id` = " . $invoiceid . "";
        $q = $this->db->query($sql);
        return $q->result();
    }

    function getsalesitems($invoiceid) {
        $sql = "SELECT 
  pii.`invoice_id`,
  s.`storename`,
  i.`itemname`,
  pii.* 
FROM
  `sales_invoice_items` AS pii 
  INNER JOIN `bs_item` AS i 
    ON i.`itemid` = pii.`product_id` 
  INNER JOIN `bs_store` AS s 
    ON s.`storeid` = pii.`store_id` 
WHERE pii.`invoice_id` =  " . $invoiceid . "";
        $q = $this->db->query($sql);
        return $q->result();
    }

    function getStoreItems($id) {
        $sql = "SELECT 
            SUM(bi.`quantity`) AS totalquantity,  
             bi.*,
              `bs_category`.`catname`,
             `bs_suppliers`.`suppliername`,
             bi.`created` AS purchase_date,
             `i`.`itemid`,
             i.product_picture,
             `i`.`itemname`,
             `i`.`itemtype`,
             IF(
               i.`itemtype` = 'P',
               `i`.`serialnumber`,
               '-----'
             ) AS `serialnumber`,
             IF(
               i.`itemtype` = 'P',
               CONCAT(`i`.`purchase_price`, ' RO.'),
               '-----'
             ) AS `purchase_price`,
             IF(
               i.`itemtype` = 'P',
               CONCAT(`i`.`sale_price`, ' RO.'),
               '-----'
             ) AS `sale_price`,
             IF(
               i.`itemtype` = 'P',
               `i`.`min_sale_price`,
               '-----'
             ) AS `min_sale_price`,
             IF(
               i.`itemtype` = 'P',
               `i`.`point_of_re_order`,
               '-----'
             ) AS `point_of_re_order` 

           FROM
             `bs_item` AS i 
             INNER JOIN `bs_store_items` AS bi 
               ON bi.`item_id` = i.`itemid` 
               
             INNER JOIN `bs_category` 
               ON `i`.`categoryid` = `bs_category`.`catid` 
             
           WHERE bi.`store_id` = '" . $id . "' 
           GROUP BY i.`itemid`";
        $q = $this->db->query($sql);
        return $q->result();
    }

    function getStoreItem($id) {
//shah query	
/*		$sql ="SELECT 
  si.*,
  i.*,
  ss.`store_id`,
  ss.`item_id`,
  u1.userid,
  u1.fullname,
  ss.soled_quantity,
  SUM(si.`quantity`) squantity,
  ssquantity,
  s.`storeid`,
  s.`storename` ,totalsoledpurchase,totalsoledsales,(IFNULL((si.`quantity`),0)*pit.`purchase_item_price`) AS totalpurchase ,
      (IFNULL((si.`quantity`),0)*bit.`sale_price`) AS totalsales  
FROM
  `bs_store_items` AS si 
  LEFT JOIN `bs_item` AS i 
    ON i.`itemid` = si.`item_id` 
  LEFT JOIN `bs_store` AS s 
    ON `s`.`storeid` = `si`.`store_id` 
  LEFT JOIN `bs_users` AS u1 
    ON `u1`.`userid` = `si`.`supplier_id` 
  LEFT JOIN 
    (SELECT 
      soldi.*,
      (soldi.`soled_quantity`) AS ssquantity ,
      (IFNULL((soldi.`soled_quantity`),0)*pit.`purchase_item_price`) AS totalsoledpurchase ,
      (IFNULL((soldi.`soled_quantity`),0)*bit.`sale_price`) AS totalsoledsales      
    FROM
      `store_soled_items` AS soldi 
       INNER JOIN `bs_store_items` AS ss 
        ON ss.`store_item_id` = soldi.`store_item_id` 
       INNER JOIN  `bs_purchase_items` AS pit ON pit.`purchase_id` = ss.`purchase_id`
       INNER JOIN `bs_item` AS BIT ON bit.`itemid` = soldi.`item_id`
     WHERE soldi.`store_id` = '2' 
    GROUP BY soldi.`item_id`) ss 
    ON `ss`.`store_id` = si.`store_id` 
    AND ss.`item_id` = si.`item_id` 
     INNER JOIN  `bs_purchase_items` AS pit ON pit.`purchase_id` = si.`purchase_id`
     INNER JOIN `bs_item` AS BIT ON bit.`itemid` = si.`item_id`
WHERE si.`store_id` = '2' 
GROUP BY si.`item_id` ";
		*/
     /*   $sql = "SELECT 
                    si.*,
                    i.*,
                    ss.`store_id`,
                    ss.`item_id`,
                    u1.userid,
                    u1.fullname,
                    ss.soled_quantity,
                    SUM(si.`quantity`) squantity ,
                    ssquantity,
                    s.`storeid`,
                    s.`storename` 
                  FROM
                    `bs_store_items` AS si 
                    LEFT JOIN `bs_item` AS i 
                      ON i.`itemid` = si.`item_id` 
    
                    LEFT JOIN `bs_store` AS s 
                        ON `s`.`storeid` = `si`.`store_id`     

                    LEFT JOIN `bs_users` AS u1 
                      ON `u1`.`userid` = `si`.`supplier_id` 
                    LEFT JOIN 
                      (SELECT 
                        *,
                        SUM(soldi.`soled_quantity`) AS ssquantity 
                      FROM
                        `store_soled_items` AS soldi 
                      WHERE soldi.`store_id` = '$id' GROUP BY soldi.`item_id`) ss 
                      ON `ss`.`store_id` = si.`store_id` 
                      AND ss.`item_id` = si.`item_id` 




                  WHERE si.`store_id` = '$id' 
                  GROUP BY si.`item_id`  ";
        */
		$sql = "SELECT 
  si.*,
  i.*,
  ss.`store_id`,
  ss.`item_id`,
  u1.userid,
  u1.fullname,
  ss.soled_quantity,
  pit.`purchase_item_price`,
  bbit.`sale_price` AS sales,
  SUM(si.`quantity`) squantity,
  si.`quantity` AS qq,
  s.`storeid`,
  s.`storename`,
  IFNULL(totalsoledpurchase, 0) AS totalsoledpurchase,
  IFNULL(totalsoledsales, 0) AS totalsoledsales,
  (
    IFNULL((si.`quantity`), 0) * pit.`purchase_item_price`
  ) AS totalpurchase,
  (
    IFNULL((si.`quantity`), 0) * bbit.`sale_price`
  ) AS totalsales ,
  ssquantity
FROM
  `bs_store_items` AS si 
  LEFT JOIN `bs_item` AS i 
    ON i.`itemid` = si.`item_id` 
  LEFT JOIN `bs_store` AS s 
    ON `s`.`storeid` = `si`.`store_id` 
  LEFT JOIN `bs_users` AS u1 
    ON `u1`.`userid` = `si`.`supplier_id` 
  LEFT JOIN 
    (SELECT 
      soldi.*,
     SUM(soldi.`soled_quantity`) AS ssquantity,
      (
        IFNULL(SUM(soldi.`soled_quantity`), 0) * pit.`purchase_item_price`
      ) AS totalsoledpurchase,
      (
        IFNULL(SUM(soldi.`soled_quantity`), 0) * bbit.`sale_price`
      ) AS totalsoledsales 
    FROM
      `store_soled_items` AS soldi 
      INNER JOIN `bs_store_items` AS ss 
        ON ss.`store_item_id` = soldi.`store_item_id` 
      INNER JOIN `bs_purchase_items` AS pit 
        ON pit.`purchase_id` = ss.`purchase_id` 
        AND pit.`purchase_item_id` = ss.`item_id` 
      INNER JOIN `bs_item` AS bbit 
        ON bbit.`itemid` = soldi.`item_id` 
    WHERE soldi.`store_id` = '$id' 
    GROUP BY soldi.`item_id`) ss 
    ON `ss`.`store_id` = si.`store_id` 
    
    AND ss.`item_id` = si.`item_id` 
  INNER JOIN `bs_purchase_items` AS pit 
    ON pit.`purchase_id` = si.`purchase_id` AND pit.`purchase_item_id` = si.`item_id`
  INNER JOIN `bs_item` AS bbit 
    ON bbit.`itemid` = si.`item_id` 
WHERE si.`store_id` = '$id' 
GROUP BY si.`item_id`  ";
        /*$sql="SELECT 
                st.`item_id`,
                st.created,
                @t4 := IF(
                  SUM(st.`quantity`) IS NULL,
                  0,
                  SUM(st.`quantity`)
                ) - IF(
                  SUM(ssi.`soled_quantity`) IS NULL,
                  0,
                  SUM(ssi.`soled_quantity`)
                ) AS remaining,
                SUM(st.quantity) AS quantity,
                u1.userid,
                u1.fullname,
                i.itemname,
                i.itemid,
                i.barcodenumber
              FROM
                `bs_store_items` AS st 
                LEFT JOIN `store_soled_items` AS ssi 
                  ON ssi.`store_item_id` = st.`store_item_id` 

                LEFT JOIN `bs_users` AS u1 
                    ON `u1`.`userid` = `st`.`supplier_id` 

               LEFT JOIN `bs_item` AS i 
                      ON i.`itemid` = st.`item_id`

                WHERE st.`store_id` = '$id' 
              GROUP BY st.`item_id` 
              ORDER BY st.`store_item_id` ASC ";*/
        $q = $this->db->query($sql);
        return $q->result();
    }

    function getStoreItem2($id) {
        $sql = "SELECT 
                i.itemname,
                i.itemid,
                i.storeid,
                i.serialnumber,
                s.storename,
                s.storeid,
                u1.fullname as fullname1,
                u1.userid as userid1,
                u2.fullname as fullname2,
                u2.userid as userid2,
                si.*,
                si.quantity AS squantity, 
                ppi.`purchase_item_price`,
                isi.`invoice_item_price`
              FROM
                `bs_store_items` AS si 
                
                LEFT JOIN `bs_item` AS i 
                  ON i.`itemid` = si.`item_id` 
                  
                LEFT JOIN `bs_store` AS s 
                  ON s.`storeid` = si.`store_id` 
                  
               
                LEFT JOIN `bs_invoice_items` AS isi
                  ON `isi`.`invoice_id` = `si`.`receipt_id` 
                  
                LEFT JOIN `bs_users` as u1 
                  ON `u1`.`userid` = `isi`.`customer_id` 
                  
                LEFT JOIN `bs_purchase_items` AS ppi
                  ON `ppi`.`purchase_id` = `si`.`purchase_id` 
                  
                LEFT JOIN `bs_users` as u2 
                  ON `u2`.`userid` = `ppi`.`supplier_id` 
                  

              WHERE si.`store_id` = '$id' 
              GROUP BY si.`item_id` ";
        $q = $this->db->query($sql);
        return $q->result();
    }

    public function storelist_old() {
        /* $sql="SELECT a.*,
          b.`location_name`,
          c.`financial`,
          COUNT(bi.`storeid`) AS storeitems,
          SUM(bi.`purchase_price`) AS totalpurchase,
          SUM(bi.`sale_price`) AS total
          FROM
          bs_location AS b,
          bs_financial AS c,
          bs_store AS a
          LEFT JOIN  `bs_item` AS bi
          ON bi.`storeid` = a.`storeid`
          LEFT JOIN `purchase_invoice_items` AS pit
          ON pit.`product_id` = bi.`itemid`
          WHERE a.`locationid` = b.`locationid`
          AND a.`ftypeid` = c.`ftypeid`
          AND a.`ownerid` = '".ownerid()."'
          GROUP BY a.`storeid`
          ORDER BY a.`storename` ASC"; */

        /* $sql="SELECT 
          a.*,
          b.`location_name`,
          c.`financial`,
          COUNT(bi.`storeid`) AS storeitems
          FROM
          bs_location AS b,
          bs_financial AS c,
          bs_store AS a
          LEFT JOIN  `bs_item` AS bi
          ON bi.`storeid` = a.`storeid`
          WHERE a.`locationid` = b.`locationid`
          AND a.`ftypeid` = c.`ftypeid`
          AND a.`ownerid` = '".ownerid()."'
          GROUP BY a.`storeid`
          ORDER BY a.`storename` ASC "; */


        /*
          $sql = "SELECT s.* , SUM(bi.`quantity`)  AS total
          ,bs_company_branch.branchid,bs_company_branch.branchname
          ,bs_company.companyid,bs_company.company_name
          FROM `bs_store` AS s
          LEFT JOIN `bs_store_items` AS bi ON bi.`store_id` = s.`storeid`
          LEFT JOIN bs_company on bs_company.companyid=s.companyid
          LEFT JOIN bs_company_branch on bs_company_branch.branchid=s.branchid
          GROUP BY s.`storeid`
          ORDER BY s.`storeid` DESC";
         */
        $sql = "SELECT 
            s.*,
            bs_company_branch.branchid,
            bs_company_branch.branchname,
            bs_company.companyid,
            bs_company.company_name,
            sum_sal,
            sum_pur
          FROM
            `bs_store` AS s 

            LEFT JOIN (
               SELECT 
                  si.*,
                  SUM(si.`quantity`) AS sum_sal

                FROM bs_store_items AS si
                WHERE si.receipt_id != '0'

            ) bi 
              ON bi.`store_id` = s.`storeid` 
            LEFT JOIN (
               SELECT 
                  si.*,
                  SUM(si.`quantity`) AS sum_pur

                FROM bs_store_items AS si
                WHERE si.purchase_id != '0'

            ) bi2 
              ON bi2.`store_id` = s.`storeid` 


            LEFT JOIN bs_company 
              ON bs_company.companyid = s.companyid 
            LEFT JOIN bs_company_branch 
              ON bs_company_branch.branchid = s.branchid 
          GROUP BY s.`storeid` 
          ORDER BY s.`storeid` DESC ";

        $q = $this->db->query($sql);
        return $q->result();
    }

    public function storelist($id = '', $pid = '') {
        /* $sql="SELECT a.*,
          b.`location_name`,
          c.`financial`,
          COUNT(bi.`storeid`) AS storeitems,
          SUM(bi.`purchase_price`) AS totalpurchase,
          SUM(bi.`sale_price`) AS total
          FROM
          bs_location AS b,
          bs_financial AS c,
          bs_store AS a
          LEFT JOIN  `bs_item` AS bi
          ON bi.`storeid` = a.`storeid`
          LEFT JOIN `purchase_invoice_items` AS pit
          ON pit.`product_id` = bi.`itemid`
          WHERE a.`locationid` = b.`locationid`
          AND a.`ftypeid` = c.`ftypeid`
          AND a.`ownerid` = '".ownerid()."'
          GROUP BY a.`storeid`
          ORDER BY a.`storename` ASC"; */

        /*
          LEFT JOIN
          (SELECT
          si.*,
          SUM(si.`quantity`) AS sum_purchase
          FROM
          bs_store_items AS si
          LEFT JOIN bs_store AS ss ON ss.storeid=si.`store_id`

          WHERE si.purchase_id != '0' AND si.`store_id` = ss.`storeid` GROUP BY si.`store_id` ) bi
          ON bi.`store_id` = s.`storeid`

          LEFT JOIN
          (SELECT
          si.*,
          SUM(si.`quantity`) AS sum_sale
          FROM
          bs_store_items AS si
          LEFT JOIN bs_store AS ss ON ss.storeid=si.`store_id`

          WHERE si.receipt_id != '0' AND si.`store_id` = ss.`storeid` GROUP BY si.`store_id` ) bi2
          ON bi2.`store_id` = s.`storeid``
          . `
         */
        /*    $sql = "SELECT 
          s.*,
          si.*,
          bs_company_branch.branchid,
          bs_company_branch.branchname,
          bs_company.companyid,
          bs_company.company_name,
          SUM(si.quantity) as qs,
          SUM(solds.soled_quantity) as qsolds,
          solds.store_id,solds.soled_quantity
          FROM
          `bs_store` AS s



          LEFT JOIN bs_store_items as si
          ON si.store_id = s.storeid

          LEFT JOIN store_soled_items as solds
          ON solds.store_id = s.storeid

          LEFT JOIN bs_company
          ON bs_company.companyid = s.companyid

          LEFT JOIN bs_company_branch
          ON bs_company_branch.branchid = s.branchid

          $ex

          GROUP BY s.`storeid`
          ORDER BY s.`storeid` DESC  "; */

        if ($id) {
            $ex = "where s.storeid='$id' and bi.item_id='$pid'";
        } else {
            $ex = "";
        }
       /* $sql = "SELECT 
                s.*,
                bs_company_branch.branchid,
                bs_company_branch.branchname,
                bs_company.companyid,
                bs_company.company_name,
                ssquantity,
                SUM(ssi.`quantity`) AS squantity
              FROM
                `bs_store` AS s 

              LEFT JOIN bs_store_items AS ssi ON ssi.`store_id`=s.`storeid`

                LEFT JOIN 
                  (SELECT 
                    *,
                    SUM(soldi.`soled_quantity`) AS ssquantity 
                  FROM
                    `store_soled_items` AS soldi GROUP BY soldi.`store_id`) ss 
                  ON `ss`.`store_id` = s.`storeid` 

                LEFT JOIN bs_company 
                  ON bs_company.companyid = s.companyid 
                LEFT JOIN bs_company_branch 
                  ON bs_company_branch.branchid = s.branchid 
                  
                   $ex

              GROUP BY s.`storeid` 
              ORDER BY s.`storeid` DESC  ";*/
		$sql1 = 'SET @v1 := 0, @v2 :=0,@v3 :=0,@v4 :=0;';
		$q = $this->db->query($sql1);	
			$sql ='
SELECT 
  s.*,
  bs_company_branch.branchid,
  bs_company_branch.branchname,
  bs_company.companyid,
  bs_company.company_name,
  bssquantity - ssquantity AS remainingquantity,
  bssquantity,
  SUM(ssi.`quantity`) AS squantity ,
  @v1,@v2,
  (@v1-@v2) AS totalstpurchase,
  (@v4-@v3) AS totalstsales,
  @v1 := 0,@v2 := 0,@v3 := 0,@v4 := 0
FROM
  `bs_store` AS s 
  LEFT JOIN bs_store_items AS ssi 
    ON ssi.`store_id` = s.`storeid` 
 LEFT JOIN(
 SELECT 
      soldi.*,
      SUM(soldi.`soled_quantity`) AS soledquantity,
      pit.`purchase_item_price`,
      (
        IFNULL(SUM(soldi.`soled_quantity`), 0) * pit.`purchase_item_price`
      ) AS totalsoledpurchase ,
      (
        IFNULL(SUM(soldi.`soled_quantity`), 0) * bt.`sale_price`
      ) AS totalsoledsales,
      @v2  := (@v2+IFNULL(SUM(soldi.`soled_quantity`), 0) * pit.`purchase_item_price`),
       @v3  := (@v3+IFNULL(SUM(soldi.`soled_quantity`), 0) * bt.`sale_price`)
    FROM
      `store_soled_items` AS soldi 
      INNER JOIN `bs_store_items` AS ss 
        ON ss.`store_item_id` = soldi.`store_item_id` 
      INNER JOIN `bs_purchase_items` AS pit 
        ON pit.`purchase_id` = ss.`purchase_id` 
        AND pit.`purchase_item_id` = ss.`item_id` 
      INNER JOIN `bs_item` AS bt 
        ON bt.`itemid` = soldi.item_id 
    GROUP BY soldi.`item_id`) AS sold ON sold.store_id= s.`storeid`
  LEFT JOIN 
    (SELECT 
      *,
      SUM(soldi.`soled_quantity`) AS ssquantity 
    FROM
      `store_soled_items` AS soldi 
    GROUP BY soldi.`store_id`) ss 
    ON `ss`.`store_id` = s.`storeid` 
        LEFT JOIN(
    SELECT 
      soldi.*,
      SUM(soldi.`quantity`) AS squantity,
      pit.`purchase_item_price`	,
     @v1 := (@v1+soldi.`quantity`*pit.`purchase_item_price`),
     @v4  := (@v4+IFNULL(SUM(soldi.`quantity`), 0) * bt.`sale_price`)
    FROM
      bs_store_items AS soldi 
      INNER JOIN `bs_purchase_items` AS pit 
        ON pit.`purchase_id` = soldi.`purchase_id` 
        AND pit.`purchase_item_id` = soldi.`item_id` 
      INNER JOIN `bs_item` AS bt 
        ON bt.`itemid` = soldi.item_id 
    GROUP BY soldi.`item_id`
    ) AS ss2
	ON ss2.store_id = s.`storeid`
	LEFT JOIN 
    (SELECT 
      bss.`store_id`,
      SUM(bss.`quantity`) AS bssquantity 
    FROM
      `bs_store_items` AS bss 
    GROUP BY bss.`store_id`) bsssi 
    ON bsssi.store_id = s.`storeid` 
  LEFT JOIN bs_company 
    ON bs_company.companyid = s.companyid 
  LEFT JOIN bs_company_branch 
    ON bs_company_branch.branchid = s.branchid 
GROUP BY s.`storeid` 
ORDER BY s.`storeid` DESC';
        $q = $this->db->query($sql);
        return $q->result();
    }

    function product_history($id = null, $type = null) {
        if ($type) {

            $exsql = "where si.supplier_id='$id' ";
        } else {

            $exsql = "where si.item_id='$id'";
        }
        /* $sql = "SELECT pi.*,i.*,`bs_category`.`catname`,`bs_users`.`fullname`,bs_users.userid,bs_store.storeid,bs_store.storename
          FROM `bs_purchase_items` AS pi

          LEFT JOIN `bs_item` AS i ON i.`itemid` = pi.`purchase_item_id`

          LEFT JOIN `bs_category`
          ON `i`.`categoryid` = `bs_category`.`catid`
          LEFT JOIN `bs_users`
          ON `bs_users`.`userid` = `pi`.`supplier_id`
          LEFT JOIN `bs_store`
          ON `bs_store`.`storeid` = `pi`.`purchase_item_store_id`

          $exsql
          GROUP BY pi.`inovice_product_id`
          ORDER BY pi.`inovice_product_id` DESC";
         */
        $sql = "SELECT si.*,i.*,`bs_category`.`catname`,`bs_users`.`fullname`,bs_users.userid,bs_store.storeid,bs_store.storename,
                si.quantity as ssquantity,i.storeid as sitem,pi.purchase_item_price as piprice
                FROM `bs_store_items` AS si
                
                
                
                LEFT JOIN `bs_item` AS i ON i.`itemid` = si.`item_id`
                
                LEFT JOIN `bs_purchase_items` AS pi ON pi.`purchase_item_id` = i.`itemid`
                
                LEFT JOIN `bs_category` 
				ON `i`.`categoryid` = `bs_category`.`catid` 
		LEFT JOIN `bs_users` 
				ON `bs_users`.`userid` = `si`.`supplier_id`        
		LEFT JOIN `bs_store` 
				ON `bs_store`.`storeid` = `si`.`store_id` 
                                
                    $exsql            
                GROUP BY si.`store_item_id`
                ORDER BY si.`store_item_id` DESC";
        $q = $this->db->query($sql);
        return $q->result();
    }

    function product_history2($id = null, $type = null) {
        if ($type) {

            $exsql = "where si.supplier_id='$id' ";
        } else {

            $exsql = "where si.item_id='$id'";
        }
        $sql = "SELECT 
                si.*,i.*,`bs_category`.`catname`,`bs_users`.`fullname`,bs_users.userid,bs_store.storeid,bs_store.storename,
                si.soled_quantity as ssquantity,i.storeid as sitem
                
                FROM `store_soled_items` AS si
                
                LEFT JOIN `bs_item` AS i ON i.`itemid` = si.`item_id`
                
                LEFT JOIN `bs_category` 
				ON `i`.`categoryid` = `bs_category`.`catid` 
		LEFT JOIN `bs_users` 
				ON `bs_users`.`userid` = `si`.`supplier_id`        
		LEFT JOIN `bs_store` 
				ON `bs_store`.`storeid` = `si`.`store_id` 
                                
                    $exsql            
                GROUP BY si.`item_id`
                ORDER BY si.`item_id` DESC";
        $q = $this->db->query($sql);
        return $q->result();
    }

//----------------------------------------------------------------------

    /*     * ******* */
    function getPendingPurchase($userid) {
        /* echo  $sql = 'SELECT bpi.*,sp.* FROM `bs_store` AS s
          INNER JOIN `bs_purchase_invoice` AS bpi
          ON bpi.`store_id` = s.`storeid`
          INNER JOIN `an_suppliers` AS sp ON sp.`id` = bpi.`ownerid`
          WHERE s.`ownerid` = ' . $userid . ' AND bpi.`confirm_status` = "0"
          GROUP BY s.`storeid`'; */
        //echo $this->session->userdata('bs_storemanager');
        $sql = "SELECT 
                bpi.*,
                u.userid,u.fullname
                
              FROM
                `an_purchase` AS bpi 



              LEFT JOIN `bs_users` AS u ON u.`userid` = bpi.`customer_id` 

              INNER JOIN `bs_purchase_items` AS ii ON ii.`purchase_id` = bpi.`purchase_id` and ii.invoice_itype='P'


              WHERE bpi.`confirm_status` = '0' 

              GROUP BY bpi.purchase_id

              ORDER BY bpi.`purchase_id` DESC ";
        $query = $this->db->query($sql);

        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function get_purchase_items($id = null) {
        $sql = "SELECT 
                bpi.*,
                u.userid,u.fullname,
                s.storeid as ssid,
                s.storename,
                p.*,
                i.*
              FROM
                `bs_purchase_items` AS bpi 



              LEFT JOIN `bs_users` AS u ON u.`userid` = bpi.`supplier_id` 

              LEFT JOIN `bs_store` AS s ON s.`storeid` = bpi.`purchase_item_store_id`

              LEFT JOIN `an_purchase` AS p ON p.`purchase_id` = bpi.`purchase_id`
              
              LEFT JOIN `bs_item` AS i ON i.`itemid` = bpi.`purchase_item_id` 

              WHERE bpi.`purchase_id` = '$id'
                  
              and bpi.`confirm_status` = '0' and bpi.purchase_item_store_id='".$this->session->userdata('bs_storemanager')."'

              GROUP BY bpi.inovice_product_id

              ORDER BY bpi.`inovice_product_id` DESC ";
        $query = $this->db->query($sql);

        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function get_purchase_items_re($id = null) {
        $sql = "SELECT 
                bpi.*,
                u.userid,u.fullname,
                s.storeid as ssid,
                s.storename,
                p.*,
                i.*
              FROM
                `bs_purchase_items` AS bpi 



              LEFT JOIN `bs_users` AS u ON u.`userid` = bpi.`supplier_id` 

              LEFT JOIN `bs_store` AS s ON s.`storeid` = bpi.`purchase_item_store_id`

              LEFT JOIN `an_purchase` AS p ON p.`purchase_id` = bpi.`purchase_id`
              
              LEFT JOIN `bs_item` AS i ON i.`itemid` = bpi.`purchase_item_id` 

              WHERE bpi.`purchase_id` = '$id'
                  
              and bpi.`confirm_status` = '1' and bpi.purchase_item_store_id='".$this->session->userdata('bs_storemanager')."'

              GROUP BY bpi.inovice_product_id

              ORDER BY bpi.`inovice_product_id` DESC ";
        $query = $this->db->query($sql);

        //WHERE s.`ownerid` = " . $userid . " AND p.`confirm_status` = '0' 

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function inserData($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    function product_list_store_transfer() {
        $sql = "select si.*,s1.*,s2.*, i.itemname,i.itemid,i.serialnumber,i.itemtype,i.product_picture, 
                c.catname,c.catid,s1.storename as sname1,s2.storename as sname2 
                from bs_store_items as si 
            
                LEFT JOIN bs_item as i ON i.itemid=si.item_id
                LEFT JOIN bs_category as c ON c.catid=i.categoryid
                
                left join bs_store as s1 on s1.storeid=si.store_id
                left join bs_store as s2 on s2.storeid=si.fromstore

                where si.transfer='1'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function store_item_all($id) {
        $sql = "select quantity,store_item_id from bs_store_items where store_item_id='$id'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row()->quantity;
        } else {
            return false;
        }
    }

    function get_sold_it($sid) {
        $sql = "select soled_quantity,store_item_id,soled_id  from store_soled_items where store_item_id='$sid'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function get_store_it($sid, $itid) {
        /* $sql="select 
          SUM(si.quantity) AS squan,
          si.store_item_id,
          si.store_id,si.item_id,
          remain,
          soldquant,
          si.quantity-soldquant as nquantity
          from bs_store_items as si
          LEFT JOIN (
          SELECT SUM(store_soled_items.soled_quantity) AS soldquant, store_soled_items.*
          FROM store_soled_items
          GROUP BY store_soled_items.`store_id`

          ) soldi
          ON soldi.item_id = si.item_id
          AND soldi.store_id = si.store_id


          where si.store_id='$sid' and si.item_id='$itid' GROUP BY si.`store_id` ";
         */

        $sql = "select 
                si.* ,
                soldi.*,
                soldi.soled_quantity as soled_quantity
                from bs_store_items as si

                LEFT JOIN  store_soled_items as soldi ON soldi.item_id = '$itid' AND soldi.store_id = '$sid' AND soldi.supplier_id=si.supplier_id

                    
                where si.store_id='$sid' and si.item_id='$itid'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function get_store_it_next($sid, $itid, $prev) {
        //$sql="select quantity,store_item_id,store_id,item_id,remain  from bs_store_items where store_id='$sid' and item_id='$itid' and quantity >'0'";
        $sql = "select si.quantity,ss.soled_quantity,ss.store_item_id as ssid,si.store_item_id,si.store_id,si.item_id,remain,si.quantity-ss.soled_quantity as nquantity  "
                . "from bs_store_items as si"
                . " LEFT JOIN store_soled_items as ss ON ss.store_item_id=si.store_item_id"
                . " where si.store_id='$sid' and si.item_id='$itid'  and si.confirm_status='0' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->next_row();
        } else {
            return false;
        }
    }

    function get_store_it_previous_row($sid, $itid) {
        $sql = "select quantity,store_item_id,store_id,item_id,remain  from bs_store_items where store_id='$sid' and item_id='$itid'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->previous_row();
        } else {
            return false;
        }
    }

    function getStoreItemsToSole0($itemid, $storeid) {
        $sql = "SELECT 
		  st.`item_id`,
		  st.*,
		  @t4 := IF(
			st.`quantity` IS NULL,
			0,
			st.`quantity`
		  ) - IF(
			ssi.`soled_quantity` IS NULL,
			0,
			ssi.`soled_quantity`
		  ) AS remaining ,
		  bpi.`purchase_item_price`
		FROM
		  `bs_store_items` AS st 
		  LEFT JOIN `store_soled_items` AS ssi 
			ON ssi.`store_item_id` = st.`store_item_id` 
		  INNER JOIN `bs_purchase_items` AS bpi 
			ON bpi.`purchase_id` = st.`purchase_id` 
		WHERE st.`item_id` = '" . $itemid . "' 
		  AND IF(
			st.`quantity` IS NULL,
			0,
			st.`quantity`
		  ) - IF(
			ssi.`soled_quantity` IS NULL,
			0,
			ssi.`soled_quantity`
		  ) != 0 AND bpi.`confirm_status` = '1'
		  AND  st.`store_id` = '" . $storeid . "'
                      
                    GROUP BY st.store_item_id
		ORDER BY st.`store_item_id` ASC ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getStoreItemsToSole($itemid, $storeid,$si=null) {
        $sql = "SELECT 
		  st.`item_id`,
		  st.*,
		  @t4 := IF(
			st.`quantity` IS NULL,
			0,
			st.`quantity`
		  ) - IF(
			ssi.`soled_quantity` IS NULL,
			0,
			ssi.`soled_quantity`
		  ) AS remaining ,
		  bpi.`purchase_item_price`
		FROM
		  `bs_store_items` AS st 
		  LEFT JOIN `store_soled_items` AS ssi 
			ON ssi.`store_item_id` = st.`store_item_id` 
		  INNER JOIN `bs_purchase_items` AS bpi 
			ON bpi.`purchase_id` = st.`purchase_id` 
		WHERE st.`item_id` = '" . $itemid . "' 
		  AND IF(
			st.`quantity` IS NULL,
			0,
			st.`quantity`
		  ) - IF(
			ssi.`soled_quantity` IS NULL,
			0,
			ssi.`soled_quantity`
		  ) != 0 AND bpi.`confirm_status` = '1'
		  AND  st.`store_id` = '" . $storeid . "'
		  AND  st.`store_item_id` = '" . $si . "'
                      
                    GROUP BY st.store_item_id
		ORDER BY st.`store_item_id` ASC ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    function getStoreItemsToSole2($itemid, $storeid) {
        //$sql="select quantity,store_item_id,store_id,item_id,remain  from bs_store_items where store_id='$sid' and item_id='$itid' and quantity >'0'";
         $sql = "SELECT 
		  st.`item_id`,
		  st.*,
		  @t4 := IF(
			st.`quantity` IS NULL,
			0,
			st.`quantity`
		  ) - IF(
			ssi.`soled_quantity` IS NULL,
			0,
			ssi.`soled_quantity`
		  ) AS remaining ,
		  bpi.`purchase_item_price`
		FROM
		  `bs_store_items` AS st 
		  LEFT JOIN `store_soled_items` AS ssi 
			ON ssi.`store_item_id` = st.`store_item_id` 
		  INNER JOIN `bs_purchase_items` AS bpi 
			ON bpi.`purchase_id` = st.`purchase_id` 
		WHERE st.`item_id` = '" . $itemid . "' 
		  AND IF(
			st.`quantity` IS NULL,
			0,
			st.`quantity`
		  ) - IF(
			ssi.`soled_quantity` IS NULL,
			0,
			ssi.`soled_quantity`
		  ) != 0 AND bpi.`confirm_status` = '1'
		  AND  st.`store_id` = '" . $storeid . "'
                      
                    GROUP BY st.store_item_id
		ORDER BY st.`store_item_id` ASC ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->next_row();
        } else {
            return false;
        }
    }

}
