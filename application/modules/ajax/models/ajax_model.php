<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ajax_model extends CI_Model {
    /*

     * Properties

     */

    private $_table_users;
    private $_table_banks;
    private $_table_modules;
    private $_table_permissions;
    private $_table_companies;
    private $_table_company_branches;
    private $_table_users_bank_transactions;

//----------------------------------------------------------------------



    /*

     * Constructor

     */

    function __construct() {

        parent::__construct();



        //Load Table Names from Config

        $this->_table_users = $this->config->item('table_users');

        $this->_table_banks = $this->config->item('table_banks');

        $this->_table_modules = $this->config->item('table_modules');

        $this->_table_permissions = $this->config->item('table_permissions');

        $this->_table_companies = $this->config->item('table_companies');

        $this->_table_company_branches = $this->config->item('table_company_branches');

        $this->_table_users_bank_transactions = $this->config->item('table_users_bank_transactions');
    }

//----------------------------------------------------------------------



    /*

     * Get all banks

     * return Object

     */

    public function get_banks_list() {

        $query = $this->db->get($this->_table_banks);



        if ($query->num_rows() > 0) {

            return $query->result();
        }
    }

    function getSearchProductBySubItem($itemname, $category) {



        $subsq = "";

        $sub_type = '';







        if ($itemname != "") {

            $subsq = "WHERE  i.`itemname` LIKE '%" . $itemname . "%' or i.`barcodenumber` LIKE '" . $itemname . "%' ";
        }



        if (isset($category) && $category != "") {



            $subsq .= " AND categoryid = " . $category;
        }











        $sql = "SELECT * FROM `bs_item` AS i " . $sub_type . $subsq . ' GROUP BY i.`itemid`';
        // echo $sql ;
        $q = $this->db->query($sql);

        //return $q->row();

        if ($q->num_rows() > 0) {



            $locs = $q->result();

            foreach ($locs as $eachLoc) {

                $product[] = array('prod' => _s($eachLoc->itemname, get_set_value('site_lang')), 'id' => $eachLoc->itemid, 'sale_price' => $eachLoc->sale_price, 'min_sale_price' => $eachLoc->min_sale_price);
            }
        } else {

            return '0';
        }

        return $product;
    }

    function getSearchProductBySubItemByStore($itemname, $category, $storeid) {



        $subsq = "";

        $sub_type = '';







        if ($itemname != "") {

            $subsq = "WHERE  (i.item_type ='raw' OR i.item_type ='product') AND   i.`itemname` LIKE '%" . $itemname . "%' or i.`barcodenumber` LIKE '" . $itemname . "%' ";
        }



        if (isset($category) && $category != "") {



            $subsq .= " AND categoryid = " . $category;
        }



        if (isset($_GET['storeid']) and $_GET['storeid'] != 0) {



            $sub_type = "  INNER JOIN `bs_store_items` AS ssi ON ssi.`item_id` = i.`itemid` AND ssi.store_id = '$storeid'  ";
        }







        $sql = "SELECT * FROM `bs_item` AS i " . $sub_type . $subsq . ' GROUP BY i.`itemid`';
        // echo $sql ;
        $q = $this->db->query($sql);

        //return $q->row();

        if ($q->num_rows() > 0) {



            $locs = $q->result();

            foreach ($locs as $eachLoc) {

                $product[] = array('prod' => _s($eachLoc->itemname, get_set_value('site_lang')), 'id' => $eachLoc->itemid, 'sale_price' => $eachLoc->sale_price, 'min_sale_price' => $eachLoc->min_sale_price);
            }
        } else {

            return '0';
        }

        return $product;
    }

    function getSearchAssignedFurnsihedProduct($itemname, $category, $storeid) {



        $subsq = " ";

        $sub_type = '';







        if ($itemname != "") {

            $subsq = " AND  i.`itemname` LIKE '%" . $itemname . "%' or i.`barcodenumber` LIKE '" . $itemname . "%' ";

            /*

              if(is_numeric($itemname)){

              $subsq = "WHERE i.`barcodenumber` LIKE '%" . $itemname . "%' ";

              }else{

              $subsq = "WHERE i.`itemname` LIKE '%" . $itemname . "%'";

              }

             */
        }



        if (isset($category) && $category != "") {



            $subsq .= " AND categoryid = " . $category;
        }



        if (isset($_GET['storeid']) and $_GET['storeid'] != 0) {

            $sub_type = "  INNER JOIN `finsh_store_items` AS ssi ON ssi.`finish_item_id` = i.`itemid` AND ssi.store_id = '$storeid'  ";
        }







        $sql = "SELECT * FROM `bs_item` AS i" . $sub_type . " WHERE  i.`item_type` = 'finishgood'  " . $subsq . ' GROUP BY i.`itemid`';
        //echo $sql;
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {

            $locs = $q->result();

            foreach ($locs as $eachLoc) {

                $product[] = array('prod' => _s($eachLoc->itemname, get_set_value('site_lang')), 'id' => $eachLoc->itemid, 'sale_price' => $eachLoc->sale_price, 'min_sale_price' => $eachLoc->min_sale_price);
            }
        } else {

            return '0';
        }

        return $product;
    }

    function getCategoryProducts($category_id) {

        //$sql = "SELECT * FROM `bs_item` AS i WHERE i.`categoryid` = '$category_id' AND `product_status` = 'A'";
        $sql = "SELECT i.*,
IF(`categ_type` = 'hot',(SELECT storeid FROM bs_store WHERE default_hot = 1),(SELECT storeid FROM bs_store WHERE default_cold = 1)) AS store_id
FROM `bs_item` AS i 
WHERE i.`categoryid` = '$category_id' AND `product_status` = 'A'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function getMosellingByDate($fdate, $todate) {

        $fdate = getSingelDate('from', $fdate);
        $todate = getSingelDate('to', $todate);

        $sql = "SELECT
  i.`itemname`,
    SUM(it.`invoice_item_quantity`) AS totalitems ,MONTH(a.`invoice_date`)
FROM
  `bs_invoice_items` AS it
  INNER JOIN `bs_item` AS i
    ON i.`itemid` = it.`invoice_item_id`
  INNER JOIN `an_invoice` AS a
    ON a.`invoice_id` = it.`invoice_id`
    WHERE a.`invoice_date`>= '" . $fdate . "' AND a.`invoice_date`<= '" . $todate . "'
GROUP BY it.`invoice_item_id`
ORDER BY SUM(it.`invoice_item_quantity`) DESC ";
        //echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getProductsByBarcode($category_id) {

        $sql = "SELECT * FROM `bs_item` AS item WHERE item.`barcodenumber` LIKE  '$category_id%'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function getdharamval() {
        $sql = "SELECT s.`is_dharam_static` , s.`dharm_value` FROM `setting` AS s";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    function getCustomerList($id) {

        $this->db->select('bs_users.*');

        if ($id != "") {

            $this->db->where('bs_users.userid', $id);
        }

        //$this->db->join('units','units.unit_id=bs_item.type_unit','LEFT');

        $query = $this->db->get('bs_users');



        if ($query->num_rows() > 0) {



            if ($id != "") {

                return $query->row();
            } else {

                return $query->resut();
            }
        }
    }

    function getPendingLimitCustomerAmount() {
        $sql = "
SELECT
  u.`fullname`,
  SUM(i.`sales_amount`) AS sales,
  IFNULL(totalpaid,0) AS totalpaid,
  u.`amount_limit`,
  SUM(i.`sales_amount`) - IFNULL(totalpaid,0) AS remaining
FROM
  `an_invoice` AS i
  INNER JOIN `bs_users` AS u
    ON u.`userid` = i.`customer_id`
  LEFT JOIN
    (SELECT
      ip.`invoice_id`,
      SUM(ip.`payment_amount`) AS totalpaid
    FROM
      `invoice_payments` AS ip
    GROUP BY ip.`invoice_id`) AS ipp
    ON ipp.invoice_id = i.`invoice_id`
WHERE i.`invoice_status` = '0'
GROUP BY i.`customer_id` ORDER BY totalpaid ASC  LIMIT 20  ";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $q->result();
        } else {

            return false;
        }
    }

    //get avg value for sale




    function getPendingDaysInvoicesNotification() {
        $sql = "SELECT
i.`invoice_id`,u.`fullname`,
  DATE(i.`invoice_date`) AS invdate,
  DATE(
    i.`invoice_date` + INTERVAL u.`days_limit` DAY
  ) AS daylimit ,CURRENT_DATE() AS currentdate
FROM
  `an_invoice` AS i
  INNER JOIN `bs_users` AS u
    ON u.`userid` = i.`customer_id`
WHERE i.`invoice_status` = '0'
  AND DATE(
    i.`invoice_date` + INTERVAL u.`days_limit` DAY
  ) <= CURRENT_DATE()  LIMIT 10 ";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $q->result_array();
        } else {

            return false;
        }
    }

    function getavgSalesvalue($item, $storeid) {

        $sql = "SELECT 

					i.*, 

				  st.`item_id`,

				  st.*,

				  @t4 := IF(

					st.`quantity` IS NULL,

					0,

					st.`quantity`

				  ) - IF(

					ssi.`soled_quantity` IS NULL,

					0,

					ssi.`soled_quantity`

				  ) AS remaining,

				  bpi.`purchase_item_price`,

				  (SELECT 

					SUM(bssi.`quantity`) 

				  FROM

					`bs_store_items` AS bssi 

				  WHERE bssi.`item_id` = '" . $item . "' 

					AND bssi.`store_id` = '" . $storeid . "') AS totalproduct,

					bpi.purchase_id

				FROM

				  `bs_store_items` AS st 

				  INNER JOIN `bs_item` AS i 

					ON i.`itemid` = st.`item_id` 

				  LEFT JOIN `store_soled_items` AS ssi 

					ON ssi.`store_item_id` = st.`store_item_id` 

				  INNER JOIN `bs_purchase_items` AS bpi 

					ON bpi.`purchase_id` = st.`purchase_id` 

				WHERE st.`item_id` = '" . $item . "' 

				  AND IF(

					st.`quantity` IS NULL,

					0,

					st.`quantity`

				  ) - IF(

					ssi.`soled_quantity` IS NULL,

					0,

					ssi.`soled_quantity`

				  ) != 0 

				  AND bpi.`confirm_status` = '1' 

				  AND st.`store_id` = '" . $storeid . "' 

				ORDER BY st.`store_item_id` ASC";

        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $q->result();
        } else {

            return false;
        }
    }

    ///get all expenses of invocies



    function getPurchaseItemsCount($invocieid) {

        $sql = "SELECT COUNT( bpi.`inovice_product_id`) AS total FROM `bs_purchase_items` AS bpi WHERE bpi.`purchase_id` = '" . $invocieid . "'";

        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $q->row()->total;
        } else {

            return false;
        }
    }

    function getavgExpenseBYExpenseId($expenseid) {



        $sql = "SELECT 

                TRIM(@t1),

                @t1 := (SELECT 

                SUM(ap.`purchase_total_amount`) AS total 

              FROM

                `an_purchase` AS ap 

                INNER JOIN `expense_purchase` AS epp 

                  ON epp.`invoice_id` = ap.`purchase_id` 

              WHERE epp.`expense_id` = exxxxid

              GROUP BY epp.`expense_id` ) AS ttttt,

                exxxxid,

                expenseval,

                @t2 := expenseval * 100 /@t1 AS expense_percantage,

                @t2/100*i.purchase_total_amount AS perpercentagevalue,

                ep.`invoice_id` AS epinvocieid,

                ep.`expense_id` AS expenseid,

                i.`customer_id`,

                i.`purchase_id` AS purchase_id,

                i.`sales_amount`,

                i.`purchase_total_amount`,

                c.*,

                pamount,

                bii.purchase_item_discount_type AS discount_type,

                i.`purchase_date` AS i_date,

                SUM(pamount) AS amount,

                SUM(purchase_item_discount) AS iidiscount,

                SUM(purchase_item_price) AS iiprice,

                SUM(purchase_item_price_purchase) AS iiprice_purchase,

                sales_amount - pamount AS remaining 

              FROM

                `an_purchase` AS i 

                INNER JOIN `bs_users` AS c 

                  ON c.`userid` = i.`customer_id` 

                LEFT JOIN 

                  (SELECT 

                    SUM(ip.`payment_amount`) AS pamount,

                    ip.`purchase_id` 

                  FROM

                    `purchase_payments` AS ip 

                    INNER JOIN `an_purchase` AS ii 

                      ON ii.`purchase_id` = ip.`purchase_id` 

                    INNER JOIN `bs_users` AS c 

                      ON c.`userid` = ii.`customer_id` 

                  GROUP BY ip.`purchase_id`) d 

                  ON d.purchase_id = i.`purchase_id` 

                LEFT JOIN bs_purchase_items AS bii 

                  ON bii.purchase_id = i.purchase_id 

                LEFT JOIN `expense_purchase` AS ep 

                  ON ep.`invoice_id` = i.purchase_id 

                LEFT JOIN 

                  (SELECT 

                    an_expenses.`id` AS exxxxid,

                    an_expenses.`value` AS expenseval

                   FROM

                    `an_expenses` 

                    INNER JOIN `expense_purchase` AS exp2 

                      ON exp2.`expense_id` = an_expenses.`id`ORDER BY exp2.invoice_id) AS ex 

                  ON ex.exxxxid = ep.`expense_id`            

                  WHERE  ep.`expense_id` = " . $expenseid . "    

                        GROUP BY i.`purchase_id`";

        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $q->result();
        } else {

            return false;
        }
    }

    function getAllExpensesByinvoiceId($invoiceid) {

        $sql = "SELECT ep.`expense_id` FROM `expense_purchase`  AS ep  WHERE ep.`invoice_id` = '" . $invoiceid . "'";

        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $q->result();
        } else {

            return false;
        }
    }

    function getProductList($id) {

        $sql1 = "SET SQL_BIG_SELECTS=1";
        $q = $this->db->query($sql1);
        $this->db->select('raw_material.*,units.unit_title,units.unit_id');

        if ($id != "") {

            $this->db->where('raw_material.id', $id);
        }

        $this->db->join('units', 'units.unit_id=raw_material.unit_id', 'LEFT');

        $query = $this->db->get('raw_material');



        if ($query->num_rows() > 0) {



            if ($id != "") {

                return $query->row();
            } else {

                return $query->resut();
            }
        }
    }

//----------------------------------------------------------------------



    /*

     * Add Transaction Data

     * @param $data array

     * return TRUE

     */

    function add_transaction($data) {

        $this->db->insert($this->_table_users_bank_transactions, $data);



        return TRUE;
    }

//----------------------------------------------------------------------



    /*

     * Get Compnay Name

     * @param $companyid integer

     * return OBJECT

     */

    public function get_branch_code($companyid, $branchid) {

        $this->db->select('branch_code');

        $this->db->where('companyid', $companyid);

        $this->db->where('branchid', $branchid);



        $query = $this->db->get($this->_table_company_branches);



        if ($query->num_rows() > 0) {

            return $query->row()->branch_code;
        }
    }

//----------------------------------------------------------------------



    /*

     * Check Account Number Exist OR Not

     * @param $companyid integer

     * return OBJECT

     */

    public function account_num_exist($bankid, $account_number) {

        $this->db->select('account_number');



        $this->db->where('bankid', $bankid);

        $this->db->where('account_number', $account_number);



        $query = $this->db->get($this->_table_companies);



        if ($query->num_rows() > 0) {

            return false;
        } else {

            return true;
        }
    }

    function getStoreByBranchId($branchid) {

        $sql = "select * from `bs_store` as s where s.`branchid` = '" . $branchid . "'";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function getSearchProductByPos($itemname, $storeid) {


        $subsq = "";

        $sub_type = '';







        if ($itemname != "") {

            $subsq = "WHERE   i.`itemname` LIKE '%" . $itemname . "%' or i.`barcodenumber` LIKE '" . $itemname . "%'   GROUP BY i.`itemid`";

            /*

              if(is_numeric($itemname)){

              $subsq = "WHERE i.`barcodenumber` LIKE '%" . $itemname . "%' ";

              }else{

              $subsq = "WHERE i.`itemname` LIKE '%" . $itemname . "%'";

              }

             */
        }



        if (isset($category) && $category != "") {



            $subsq .= " AND categoryid = " . $category;
        }



        if (isset($storeid) and $storeid != 0) {



            $sub_type = "  Left JOIN `bs_store_items` AS ssi ON ssi.`item_id` = i.`itemid` ";
        }







        $sql = "SELECT * FROM `bs_item` AS i " . $sub_type . $subsq;


        $sql = "SELECT
  si.*,
  i.*,
  ss.`store_id`,
  ss.`item_id`,
  SUM(si.`quantity`) squantity,
  ssquantity,
  s.`storeid`,

  IFNULL(SUM(si.`quantity`),0) - IFNULL(ssquantity,0) AS totalquantity
FROM
  `bs_item` AS i
    LEFT JOIN `bs_store_items` AS si
    ON si.`item_id` = i.`itemid`
  LEFT JOIN `bs_store` AS s
    ON `s`.`storeid` = `si`.`store_id`
  LEFT JOIN `bs_users` AS u1
    ON `u1`.`userid` = `si`.`supplier_id`
  LEFT JOIN
    (SELECT
      *,
      SUM(soldi.`soled_quantity`) AS ssquantity
    FROM
      `store_soled_items` AS soldi
    WHERE soldi.`store_id` = '" . $storeid . "'
      AND soldi.`is_cancel` = '0'
    GROUP BY soldi.`item_id`) ss
    ON `ss`.`store_id` = si.`store_id`
    AND ss.`item_id` = si.`item_id`
WHERE  ( i.`itemname` LIKE '%" . $itemname . "%'
  OR i.`barcodenumber` LIKE '" . $itemname . "%')
 ";
        //echo $sql;
        $q = $this->db->query($sql);

        //return $q->row();

        if ($q->num_rows() > 0) {



            $locs = $q->result();

            foreach ($locs as $eachLoc) {

                $product[] = array('prod' => _s($eachLoc->itemname, 'english'), 'prod_ar' => _s($eachLoc->itemname, 'arabic'), 'brnumber' => $eachLoc->barcodenumber, 'id' => $eachLoc->itemid, 'totalquantity' => $eachLoc->totalquantity, 'sale_price' => $eachLoc->sale_price, 'min_sale_price' => $eachLoc->min_sale_price, 'small_price' => $eachLoc->small_price, 'medium_price' => $eachLoc->medium_price, 'large_price' => $eachLoc->large_price);
            }
        } else {

            return '0';
        }

        return $product;
    }

    function getSearchProductByInvenBranch($itemname) {


        $subsq = "";

        $sub_type = '';






        $bid = $this->session->userdata('bs_branchid');

        if ($itemname != "") {

            $subsq = "   AND (ibv.`inven_item_name_eng` LIKE '" . $itemname . "%' OR ibv.`inven_item_name_ar` LIKE '" . $itemname . "%') ";


            /*

              if(is_numeric($itemname)){

              $subsq = "WHERE i.`barcodenumber` LIKE '%" . $itemname . "%' ";

              }else{

              $subsq = "WHERE i.`itemname` LIKE '%" . $itemname . "%'";

              }

             */
        }



        $sql = "SELECT
  ibv.*,
  IFNULL(storeq, 0) - IFNULL(soledstoreq, 0) AS availablestock
FROM
  `inventory_items` AS ibv
  LEFT JOIN
    (SELECT
      SUM(ibs.`inventory_branch_item_q`) AS storeq,
      ibs.`inventory_branch_item_id`,
      ibs.`inventory_branch_id`
    FROM
      `inventory_branch_store_items` AS ibs
    WHERE ibs.`inventory_branch_id` = '" . $bid . "'
    GROUP BY ibs.`inventory_branch_item_id`) AS invs
    ON invs.inventory_branch_item_id = ibv.`inven_item_id`
  LEFT JOIN
    (SELECT
      SUM(ibsi1.`item_b_invitem_q`) AS soledstoreq,
      ibsi1.`branch_inven_item_id`
    FROM
      `inventory_soled_branch_items` AS ibsi1
    WHERE ibsi1.`branch_id` = '" . $bid . "'
    GROUP BY ibsi1.`branch_inven_item_id`) AS ibsii
    ON ibsii.branch_inven_item_id = ibv.`inven_item_id`
WHERE invs.`inventory_branch_id` = '" . $bid . "' " . $subsq . "
GROUP BY ibv.`inven_item_id`";
        //echo $sql;
        $q = $this->db->query($sql);

        //return $q->row();

        if ($q->num_rows() > 0) {



            $locs = $q->result();

            foreach ($locs as $eachLoc) {

                $product[] = array('prod' => $eachLoc->inven_item_name_eng, 'prod_ar' => $eachLoc->inven_item_name_ar, 'id' => $eachLoc->inven_item_id, 'totalquantity' => $eachLoc->availablestock);
            }
        } else {

            return '0';
        }

        return $product;
    }

    function getSearchProduct($itemname, $category, $storeid) {



        $subsq = "";

        $sub_type = '';







        if ($itemname != "") {

            $subsq = "WHERE i.`itemname` LIKE '%" . $itemname . "%' or i.`barcodenumber` LIKE '" . $itemname . "%'   GROUP BY i.`itemid`";

            /*

              if(is_numeric($itemname)){

              $subsq = "WHERE i.`barcodenumber` LIKE '%" . $itemname . "%' ";

              }else{

              $subsq = "WHERE i.`itemname` LIKE '%" . $itemname . "%'";

              }

             */
        }



        if (isset($category) && $category != "") {



            $subsq .= " AND categoryid = " . $category;
        }



        if (isset($_GET['storeid']) and $_GET['storeid'] != 0) {



            $sub_type = "  INNER JOIN `bs_store_items` AS ssi ON ssi.`item_id` = i.`itemid` AND ssi.store_id = '$storeid'  ";
        }







        $sql = "SELECT * FROM `bs_item` AS i " . $sub_type . $subsq;
        // echo $sql ;
        $q = $this->db->query($sql);

        //return $q->row();

        if ($q->num_rows() > 0) {



            $locs = $q->result();

            foreach ($locs as $eachLoc) {

                $product[] = array('prod' => _s($eachLoc->itemname, get_set_value('site_lang')), 'id' => $eachLoc->itemid, 'sale_price' => $eachLoc->sale_price, 'min_sale_price' => $eachLoc->min_sale_price);
            }
        } else {

            return '0';
        }

        return $product;
    }

    function getSearchProduct2($itemname, $category, $storeid) {



        $subsq = "";

        $sub_type = '';







        if ($itemname != "") {

            $subsq = "WHERE i.`itemname` LIKE '" . $itemname . "%' or i.`barcodenumber` LIKE '" . $itemname . "%'   GROUP BY i.`itemid`";

            /*

              if(is_numeric($itemname)){

              $subsq = "WHERE i.`barcodenumber` LIKE '%" . $itemname . "%' ";

              }else{

              $subsq = "WHERE i.`itemname` LIKE '%" . $itemname . "%'";

              }

             */
        }



        if (isset($category) && $category != "") {



            $subsq .= " AND categoryid = " . $category;
        }



        if (isset($_GET['storeid']) and $_GET['storeid'] != 0) {



            $sub_type = "  INNER JOIN `bs_store_items` AS ssi ON ssi.`item_id` = i.`itemid` AND ssi.store_id = '$storeid'  ";
        }







        $sql = "SELECT * FROM `bs_item` AS i " . $sub_type . $subsq;

        $q = $this->db->query($sql);

        //return $q->row();

        if ($q->num_rows() > 0) {



            $locs = $q->result();

            foreach ($locs as $eachLoc) {

                $product[] = array('prod' => _s($eachLoc->itemname, get_set_value('site_lang')), 'id' => $eachLoc->itemid);
            }
        } else {

            return '0';
        }

        return $product;
    }

    function gettransferProduct($itemname, $category, $storeid) {
        $subsq = "";
        //echo $storeid;
        //exit;
        if (isset($storeid)) {




            $subsq.= " 	INNER JOIN `bs_store_items` AS ssi ON ssi.item_id = i.`itemid` LEFT JOIN 
    (SELECT 
      st.`item_id`,
      SUM(st.`soled_quantity`) AS squnatity 
    FROM
      `store_soled_items` AS st 
    WHERE st.`store_id` = '" . $storeid . "' AND st.`is_cancel` = '0'
    GROUP BY st.`item_id`) AS sst 
    ON sst.item_id = i.`itemid` ";
        }
        $subsqc = '';
        $subsq .=" WHERE ssi.store_id='" . $storeid . "' ";
        if (isset($category) && $category != "") {


            if ($subsq != "")
                $subsqc = " AND categoryid = '" . $category . "'";
        }
        if ($itemname != "") {

            $subsq .= " AND i.`itemname` LIKE '" . $itemname . "%' or i.`barcodenumber` LIKE '" . $itemname . "%' ";
        }


        // $sql = "SELECT  i.*, SUM(ssi.`quantity`) AS q,  SUM(ssi.`quantity`) - IFNULL(squnatity,0)  AS fqunat FROM `bs_item` AS i " . $subsq." GROUP BY i.itemid";


        $sql = "SELECT
  si.*,
  i.*,
  ss.`store_id`,
  ss.`item_id`,
  SUM(si.`quantity`) squantity,
  ssquantity,
  s.`storeid`,

  IFNULL(SUM(si.`quantity`),0) - IFNULL(ssquantity,0) AS totalquantity
FROM
  `bs_store_items` AS si
  LEFT JOIN `bs_item` AS i
    ON i.`itemid` = si.`item_id`
  LEFT JOIN `bs_store` AS s
    ON `s`.`storeid` = `si`.`store_id`
  LEFT JOIN `bs_users` AS u1
    ON `u1`.`userid` = `si`.`supplier_id`
  LEFT JOIN
    (SELECT
      *,
      SUM(soldi.`soled_quantity`) AS ssquantity
    FROM
      `store_soled_items` AS soldi
    WHERE soldi.`store_id` = '" . $storeid . "'
      AND soldi.`is_cancel` = '0'
    GROUP BY soldi.`item_id`) ss
    ON `ss`.`store_id` = si.`store_id`
    AND ss.`item_id` = si.`item_id`
WHERE si.store_id =' " . $storeid . "' " . $subsqc . "
  AND ( i.`itemname` LIKE '%" . $itemname . "%'
  OR i.`barcodenumber` LIKE '" . $itemname . "%')
GROUP BY si.`item_id` ";


        // echo $sql;

        $q = $this->db->query($sql);

        //return $q->row();

        if ($q->num_rows() > 0) {



            $locs = $q->result();

            foreach ($locs as $eachLoc) {

                $product[] = array('prod' => _s($eachLoc->itemname, get_set_value('site_lang')), 'id' => $eachLoc->itemid, 'quant' => ($eachLoc->totalquantity), 'bsstoreitems' => $eachLoc->bsstoreitems);
            }
        } else {

            return '0';
        }

        return $product;
    }

    function getProductList2($itemname, $category, $storeid) {



        $subsq = "";

        $sub_type = '';






        if (isset($storeid)) {



            $subsq.= " 	LEFT JOIN `bs_store_items` AS ssi ON ssi.`store_item_id` = i.`itemid` and  ssi.store_id='$storeid'";
        }



        if ($itemname != "") {

            $subsq .= "WHERE i.`itemname` LIKE '" . $itemname . "%' or i.`barcodenumber` LIKE '" . $itemname . "%' ";

            /*

              if(is_numeric($itemname)){

              $subsq = "WHERE i.`barcodenumber` LIKE '%" . $itemname . "%' ";

              }else{

              $subsq = "WHERE i.`itemname` LIKE '%" . $itemname . "%'";

              }

             */
        }



        if (isset($category) && $category != "") {


            if ($subsq != "")
                $subsq .= " AND categoryid = " . $category;
            else
                $subsq .= " WHERE categoryid = " . $category;
        }










        $sql = "SELECT * FROM `bs_item` AS i " . $subsq;

        //echo $sql;
        //  exit;
        $q = $this->db->query($sql);

        //return $q->row();

        if ($q->num_rows() > 0) {



            $locs = $q->result();

            foreach ($locs as $eachLoc) {

                $product[] = array('prod' => _s($eachLoc->itemname, get_set_value('site_lang')), 'id' => $eachLoc->itemid, 'quant' => ($eachLoc->quant - $eachLoc->soldquant), 'bsstoreitems' => $eachLoc->bsstoreitems);
            }
        } else {

            return '0';
        }

        return $product;



        /*

          if ($itemname != "") {

          //$subsq = " AND  i.`itemname` LIKE '%" . $itemname . "%'";

          if(is_numeric($itemname)){

          $subsq = " AND i.`barcodenumber` LIKE '%" . $itemname . "%'";

          }else{

          $subsq = " AND i.`itemname` LIKE '%" . $itemname . "%'";

          }

          }



          if (isset($type)) {

          if ($type == 'category')

          $sub_type = "i.categoryid = " . $type_val;

          }



          if ($sub_type != "" && $itemname != "") {

          $subsq = $subsq . " AND " . $sub_type;

          } elseif ($sub_type != "") {

          $subsq = " AND " . $sub_type;

          }



          //$sql = "SELECT * FROM `bs_item` AS i " . $subsq;

          $sql = "SELECT si.*,i.*,

          soldquant,

          SUM(si.quantity) as quant,

          GROUP_CONCAT(si.`store_item_id`) AS bsstoreitems

          FROM `bs_store_items` AS si

          LEFT JOIN bs_item AS i ON i.itemid=si.item_id



          LEFT JOIN (

          SELECT SUM(store_soled_items.soled_quantity) AS soldquant, store_soled_items.*

          FROM store_soled_items

          GROUP BY store_soled_items.`store_id`



          ) soldi

          ON soldi.item_id = si.item_id

          AND soldi.store_id = si.store_id





          Where si.store_id='$store'

          $subsq

          GROUP BY i.`itemid` ";

          $q = $this->db->query($sql);

          //return $q->row();

          if ($q->num_rows() > 0) {

          $locs = $q->result();

          foreach ($locs as $eachLoc) {

          $product[] = array('prod' => _s($eachLoc->itemname,get_set_value('site_lang')), 'id' => $eachLoc->itemid, 'quant' => ($eachLoc->quant-$eachLoc->soldquant),'bsstoreitems'=>$eachLoc->bsstoreitems);

          //,'store_item_id' => $eachLoc->store_item_id

          }

          } else {

          return '0';

          }

          return $product;

         */
    }

    function getSearchCustomer($customer_name) {

        if (is_numeric($customer_name)) {
            $sql = "SELECT * FROM `bs_users` AS u WHERE u.`member_type`='6' AND u.type='1' AND u.`fullname` LIKE '%" . $customer_name . "%' OR u.`phone_number` LIKE '%" . $customer_name . "%'";
        } else {
            $sql = "SELECT * FROM `bs_users` AS u WHERE u.`member_type`='6' AND u.type='1' AND u.`fullname` LIKE '%" . $customer_name . "%'";
        }

        $q = $this->db->query($sql);

        // echo $q->num_rows();

        if ($q->num_rows() > 0) {

            $locs = $q->result();

            foreach ($locs as $eachLoc) {

                $customers[] = array('cus' => $eachLoc->fullname, 'id' => $eachLoc->userid, 'buywithtotal' => $eachLoc->buywithtotal, 'blacklist' => $eachLoc->blacklist, 'reson_blacklist' => $eachLoc->reson_blacklist, 'storeid' => $eachLoc->storeid);
            }
        } else {

            return '0';
        }
        //echo count($customers);

        return $customers;
    }

    function getSearchCustomer2($customer_name) {



        if (is_numeric($customer_name)) {

            $sql = "SELECT * FROM `bs_users` AS u WHERE u.`member_type`='6' AND u.type='2' AND u.`phone_number` LIKE '%" . $customer_name . "%' ";
        } else {

            $sql = "SELECT * FROM `bs_users` AS u WHERE u.`member_type`='6' AND u.type='2' AND u.`fullname` LIKE '%" . $customer_name . "%'";
        }

        $q = $this->db->query($sql);

        //return $q->row();

        if ($q->num_rows() > 0) {

            $locs = $q->result();

            foreach ($locs as $eachLoc) {

                $customers[] = array('cus' => $eachLoc->fullname, 'id' => $eachLoc->userid, 'buywithtotal' => $eachLoc->buywithtotal);
            }
        } else {

            return '0';
        }

        return $customers;
    }

    function getSearchExpense($customer_name) {



        if (is_numeric($customer_name)) {

            $sql = "SELECT * FROM `an_expenses_charges` AS ec WHERE ec.`expense_title` LIKE '%" . $customer_name . "%' ";
        } else {

            $sql = "SELECT * FROM `an_expenses_charges` AS ec WHERE ec.`expense_title` LIKE '%" . $customer_name . "%'";
        }

        $q = $this->db->query($sql);

        //return $q->row();

        if ($q->num_rows() > 0) {

            $locs = $q->result();

            foreach ($locs as $eachLoc) {

                $customers[] = array('cus' => $eachLoc->expense_title, 'id' => $eachLoc->id);
            }
        } else {

            return '0';
        }

        return $customers;
    }

    function getLastEmployeeCode() {

        $sql = "SELECT employeecode FROM `bs_users`  WHERE  employeecode!=''  ORDER BY `bs_users`.`userid` DESC LIMIT 1";

        $q = $this->db->query($sql);

        return $q->row();
    }

    function getAllDocuments($company_id) {

        $sql = "    SELECT * FROM `compnay_documents` AS cd WHERE cd.`company_id` = '" . $company_id . "'";

        $q = $this->db->query($sql);

        return $q->result();
    }

    public function InsertId() {

        return $this->db->insert_id();
    }

    public function AffectedRows() {

        return $this->db->affected_rows();
    }

    public function insertDatabase($table, $data) {

        $query = $this->db->insert($table, $data);

        return $query;
    }

    public function update_db($table, $data, $condition) {

        return $this->db->update($table, $data, $condition);
    }

//----------------------------------------------------------------------





    function getCusmer_invoices__($customer_id) {

        /* $sql = "SELECT   j.`id`,

          (SELECT SUM(`payment_amount`)

          FROM `payments`

          WHERE payments.`refrence_id` = '".$customer_id."') AS payments,



          (SELECT

          SUM(jobs_charges.`sales_value`)

          FROM

          `jobs_charges`

          WHERE jobs_charges.`job_id` = j.`id`)  AS charge_amount,

          (SELECT

          SUM(job_payment.`payment_amount`)

          FROM

          `job_payment`

          WHERE job_payment.`job_id` = j.`id`)  AS payment_amount,

          ir.`invoice_id`

          FROM

          `jobs_charges` AS jc , `an_jobs` AS j

          INNER JOIN `an_jobs_invoice_relation` AS ir

          ON ir.`job_id` = j.`id`

          WHERE j.`customer_id` = '".$customer_id."' AND j.id IN(jc.`job_id`)

          GROUP BY j.`id`

          ORDER BY j.`id` ASC"; */

///new query 

        /* $sql = "SELECT 

          i.*,

          @total :=  i.`totalamount`,

          @payment :=  SUM(p.payment_amount) AS amount,

          (SELECT  SUM(pp.`payment_amount`) AS remaining

          FROM `payments` AS pp



          WHERE pp.`id` NOT IN (SELECT  ip2.`payment_id` FROM `sale_payments` AS ip2) AND pp.`user_id` = '" . $customer_id . "') AS remaining,



          (SELECT SUM(pp.`payment_amount`) AS remaining



          FROM `payments` AS pp WHERE pp.`refrence_id` = '2' ) AS amount



          FROM `bs_sales_invoice` AS i



          LEFT JOIN `sale_payments` AS ip ON ip.`invoice_id` = i.`invoiceid`

          LEFT JOIN `payments` AS p ON p.`id` = ip.`payment_id`   AND p.`payment_type` = 'receipt'

          WHERE i.`customerid` = '" . $customer_id . "'

          AND i.`invoicstatus` != 'C'

          GROUP BY i.`invoiceid`  ";

         */







        $this->db->select('bs_sales_invoice.*,sale_payments.*');

        $q = $this->db->get('bs_sales_invoice');

        if ($q->num_rows() > 0) {

            return $return = $q->result_array();
        } else {

            return false;
        }
    }

    function getCusmer_invoices($customer_id) {

        /* $sql = "SELECT   j.`id`,

          (SELECT SUM(`payment_amount`)

          FROM `payments`

          WHERE payments.`refrence_id` = '".$customer_id."') AS payments,



          (SELECT

          SUM(jobs_charges.`sales_value`)

          FROM

          `jobs_charges`

          WHERE jobs_charges.`job_id` = j.`id`)  AS charge_amount,

          (SELECT

          SUM(job_payment.`payment_amount`)

          FROM

          `job_payment`

          WHERE job_payment.`job_id` = j.`id`)  AS payment_amount,

          ir.`invoice_id`

          FROM

          `jobs_charges` AS jc , `an_jobs` AS j

          INNER JOIN `an_jobs_invoice_relation` AS ir

          ON ir.`job_id` = j.`id`

          WHERE j.`customer_id` = '".$customer_id."' AND j.id IN(jc.`job_id`)

          GROUP BY j.`id`

          ORDER BY j.`id` ASC"; */

///new query 

        /*        $sql = "SELECT 

          i.*,

          @total :=  i.`invoice_total_amount`,

          @payment :=  SUM(p.payment_amount) AS amount,

          (SELECT

          SUM(pp.`payment_amount`) AS remaining

          FROM

          `payments` AS pp

          WHERE pp.`id` NOT IN

          (SELECT

          ip2.`payment_id`

          FROM

          `invoice_payments` AS ip2)

          AND pp.`refrence_id` = '" . $customer_id . "') AS remaining

          ,

          (SELECT

          SUM(pp.`payment_amount`) AS remaining

          FROM

          `payments` AS pp

          WHERE pp.`refrence_id` = '2' ) AS amount

          FROM

          `an_invoice` AS i

          INNER JOIN `an_jobs` AS j

          ON j.`id` = i.`job_id`

          LEFT JOIN `invoice_payments` AS ip

          ON ip.`invoice_id` = i.`invoice_id`

          LEFT JOIN `payments` AS p

          ON p.`id` = ip.`payment_id`   AND p.`payment_type` = 'receipt'

          WHERE j.`customer_id` = '" . $customer_id . "'

          AND i.`invoice_status` != 1

          GROUP BY i.`invoice_id`  "; */

        $sql = "SELECT 

                i.`customer_id`,

                i.`invoice_id` AS invoice_id,

                i.`sales_amount`,
                 i.`invoice_total_amount`,
                i.`invoice_total_amount` AS invtotal,

               IFNULL(pamount,0) AS pamount,

                i.`invoice_date`,

                SUM(pamount) AS amount 

              FROM

                `an_invoice` AS i 

                INNER JOIN `bs_users` AS c 

                  ON c.`userid` = i.`customer_id` 

                LEFT JOIN 

                  (SELECT 

                    SUM(ip.`payment_amount`) AS pamount,

                    ip.`invoice_id` 

                  FROM

                    `invoice_payments` AS ip 

                    INNER JOIN `an_invoice` AS ii 

                      ON ii.`invoice_id` = ip.`invoice_id` 

                    INNER JOIN `bs_users` AS c 

                      ON c.`userid` = ii.`customer_id` 

                  WHERE c.`userid` = '" . $customer_id . "' 

                  GROUP BY ip.`invoice_id`) d 

                  ON d.invoice_id = i.`invoice_id` 

              WHERE i.customer_id = '" . $customer_id . "'  AND i.`invoice_status` = '0' AND i.`invoice_type` !='duecharges'

              GROUP BY i.`invoice_id`  HAVING invtotal > pamount";



        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $return = $q->result_array();
        } else {

            return false;
        }
    }

    function getCusmer_invoices2($customer_id) {



        $sql = "SELECT 

            i.`customer_id`,

            i.`purchase_id` AS invoice_id,

            i.`sales_amount`,

            i.`purchase_total_amount`,

            pamount,

            i.`purchase_date`,

            SUM(pamount) AS amount 

          FROM

            `an_purchase` AS i 

            INNER JOIN `bs_users` AS c 

              ON c.`userid` = i.`customer_id` 

            LEFT JOIN 

              (SELECT 

                SUM(ip.`payment_amount`) AS pamount,

                ip.`purchase_id` 

              FROM

                `purchase_payments` AS ip 

                INNER JOIN `an_purchase` AS ii 

                  ON ii.`purchase_id` = ip.`purchase_id` 

                INNER JOIN `bs_users` AS c 

                  ON c.`userid` = ii.`customer_id` 

              WHERE c.`userid` = '" . $customer_id . "' 

              GROUP BY ip.`purchase_id`) d 

              ON d.purchase_id = i.`purchase_id` 

          WHERE i.customer_id = '" . $customer_id . "'  AND i.`purchase_status` = '0' AND i.`purchase_type` !='duecharges'

          GROUP BY i.`purchase_id` ";



        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $return = $q->result_array();
        } else {

            return false;
        }
    }

    /**/

    function add_due_charges2($customer_id) {

        //echo "asasd";

        $all_ids = $this->input->post();

        if ($all_ids['due_charges']) {

            $userid = $this->input->post('userid');

            $due['purchase_total_amount'] = $all_ids['due_charges'];

            $due['sales_amount'] = $all_ids['due_charges'];

            $due['purchase_type'] = 'duecharges';

            $due['customer_id'] = $customer_id;

            //echo "<pre>";
            //print_r($due);

            $this->db->insert('an_purchase', $due);
        }
    }

    function add_due_charges($customer_id) {

        //echo "asasd";

        $all_ids = $this->input->post();

        if ($all_ids['due_charges']) {

            $userid = $this->input->post('userid');

            $due['invoice_total_amount'] = $all_ids['due_charges'];

            $due['sales_amount'] = $all_ids['due_charges'];

            $due['invoice_type'] = 'duecharges';

            $due['customer_id'] = $customer_id;

            //echo "<pre>";
            //print_r($due);

            $this->db->insert('an_invoice', $due);
        }
    }

    function addopening($customer_id) {

        $all_ids = $this->input->post();

        if ($all_ids['opening_balance']) {

            $userid = $this->input->post('userid');

            $payment['payment_type'] = 'receipt';

            $payment['refernce_type'] = 'opening';

            $payment['payment_amount'] = $all_ids['opening_balance'];

            $payment['user_id'] = $userid;

            $payment['refrence_id'] = $customer_id;

            $this->db->insert('payments', $payment);
        }
    }

    public function product_list($id) {

        $subq = "";

        $sql = "select * from raw_material";

        /* $sql = "SELECT 

          `bs_category`.`catname`,

          `bs_item`.`itemid`,

          `bs_item`.`suppliderid`,

          `bs_item`.`itemname`,

          `bs_item`.`itemtype`,

          `bs_item`.`storeid`,

          `bs_item`.`submissiondate`,

          `bs_item`.`re_order`,

          bs_item.barcodenumber,

          IF(

          bs_item.serialnumber != '',

          `bs_item`.`serialnumber`,

          '-----'

          ) AS `serialnumber`,

          IF(

          bs_item.purchase_price != '',

          CONCAT(`bs_item`.`purchase_price`,''),

          '-----'

          ) AS `purchase_price`,

          IF(

          bs_item.sale_price != '',

          CONCAT(`bs_item`.`sale_price`,''),

          '-----'

          ) AS `sale_price`,

          IF(

          bs_item.min_sale_price != '',

          `bs_item`.`min_sale_price`,

          '-----'

          ) AS `min_sale_price`,

          IF(

          bs_item.point_of_re_order != '',

          `bs_item`.`point_of_re_order`,

          '-----'

          ) AS `point_of_re_order`,

          IF(

          bs_item.quantity != '',

          `bs_item`.`quantity`,

          '-----'

          ) AS `quantity`,

          `bs_item`.`product_picture`

          FROM

          (`bs_item`)

          LEFT JOIN `bs_category`

          ON `bs_item`.`categoryid` = `bs_category`.`catid`



          ORDER BY `bs_item`.`itemid` DESC "; */

        $ownerid = ownerid();

        $query = $this->db->query($sql);

        //$this->db->last_query();

        return $query->result();
    }

    public function users_list($id = null) {



        if ($id != 0) {

            $query = " AND c.userid='" . $id . "' ";
        } else {

            $query = "";
        }

        $this->db->query('SET SQL_BIG_SELECTS=1');

        $sql = "SELECT 

                sales,

                ptamount,

                paaaa,

                c.`userid`,

                IF(sales IS NULL, 0, sales) AS sales_amount,

                i.`invoice_total_amount`,

                IF(ptamount IS NULL, 0, ptamount) - IF(paaaa IS NULL, 0, paaaa) AS amount,

                i.`invoice_date`,

                SUM(pamount) AS pamount,

                totalinvoice,

                IF(sales IS NULL, 0, sales) - pamount AS totalremaining,

                IF(sales IS NULL, 0, sales) - IF(paaaa IS NULL, 0, paaaa) AS totalremaining,

                c.* 

              FROM

                bs_users AS c 

                LEFT JOIN `an_invoice` AS i 

                  ON i.`customer_id` = c.`userid` 

                LEFT JOIN 

                  (SELECT 

                    (p.`payment_amount`) - (ip.`payment_amount`) AS pamount,

                    ip.`invoice_id` 

                  FROM

                    `invoice_payments` AS ip 

                    INNER JOIN `an_invoice` AS ii 

                      ON ii.`invoice_id` = ip.`invoice_id` 

                    INNER JOIN `bs_users` AS c 

                      ON c.`userid` = ii.`customer_id` 

                    INNER JOIN `payments` AS p 

                      ON p.`id` = ip.`payment_id` 

                  GROUP BY ii.`customer_id`,

                    p.id) d 

                  ON d.invoice_id = i.`invoice_id` 

                  AND i.`invoice_status` = '0' 

                LEFT JOIN 

                  (SELECT 

                    COUNT(ai.`invoice_id`) AS totalinvoice,

                    ai.`customer_id`

                  FROM

                    `an_invoice` AS ai 

                  WHERE ai.invoice_type != 'duecharges' 

                  GROUP BY ai.`customer_id`) dd 

                  ON dd.customer_id = c.`userid` 

                  AND i.`invoice_status` = '0' 

                LEFT JOIN 

                  (SELECT 

                    SUM(ipp.`payment_amount`) paaaa,

                    iio.`customer_id` 

                  FROM

                    `invoice_payments` AS ipp 

                    INNER JOIN `an_invoice` AS iio 

                      ON iio.`invoice_id` = ipp.invoice_id 

                  GROUP BY iio.customer_id) pp 

                  ON pp.customer_id = c.`userid` 

                LEFT JOIN 

                  (SELECT 

                    SUM(pt.`payment_amount`) ptamount,

                    pt.`refrence_id` 

                  FROM

                    `payments` AS pt 

                  GROUP BY pt.`refrence_id`) ppt 

                  ON ppt.`refrence_id` = c.`userid` 

                LEFT JOIN 

                  (SELECT 

                    SUM(ic.`sales_amount`) AS sales,

                    ic.`customer_id` 

                  FROM

                    `an_invoice` AS ic 

                  GROUP BY ic.`customer_id`) AS icc 

                  ON icc.customer_id = c.userid 



              WHERE c.type='1'and c.member_type!='5' $query

              GROUP BY c.`userid` 

              ORDER BY c.userid DESC ";

        $q = $this->db->query($sql);

        return $q->result();
    }

    public function users_list2($id = 0) {





        $sql = "SELECT 

  sales,

  ptamount,

  paaaa,

  c.`userid`,

  IF(sales IS NULL, 0, sales) AS sales_amount,

  i.`invoice_total_amount`,

  IF(ptamount IS NULL, 0, ptamount) - IF(paaaa IS NULL, 0, paaaa) AS amount,

  i.`invoice_date`,

  SUM(pamount) AS pamount,

  totalinvoice,

  IF(sales IS NULL, 0, sales) - pamount AS totalremaining,

  IF(sales IS NULL, 0, sales) - IF(paaaa IS NULL, 0, paaaa) AS totalremaining,

  c.* 

FROM

  bs_users AS c 

  LEFT JOIN `an_invoice` AS i 

    ON i.`customer_id` = c.`userid` 

  LEFT JOIN 

    (SELECT 

      (p.`payment_amount`) - (ip.`payment_amount`) AS pamount,

      ip.`invoice_id` 

    FROM

      `invoice_payments` AS ip 

      INNER JOIN `an_invoice` AS ii 

        ON ii.`invoice_id` = ip.`invoice_id` 

      INNER JOIN `bs_users` AS c 

        ON c.`userid` = ii.`customer_id` 

      INNER JOIN `payments` AS p 

        ON p.`id` = ip.`payment_id` 

    GROUP BY ii.`customer_id`,

      p.id) d 

    ON d.invoice_id = i.`invoice_id` 

    AND i.`invoice_status` = '0' 

  LEFT JOIN 

    (SELECT 

      COUNT(ai.`invoice_id`) AS totalinvoice,

      ai.`customer_id`

    FROM

      `an_invoice` AS ai 

    WHERE ai.invoice_type != 'duecharges' 

    GROUP BY ai.`customer_id`) dd 

    ON dd.customer_id = c.`userid` 

    AND i.`invoice_status` = '0' 

  LEFT JOIN 

    (SELECT 

      SUM(ipp.`payment_amount`) paaaa,

      iio.`customer_id` 

    FROM

      `invoice_payments` AS ipp 

      INNER JOIN `an_invoice` AS iio 

        ON iio.`invoice_id` = ipp.invoice_id 

    GROUP BY iio.customer_id) pp 

    ON pp.customer_id = c.`userid` 

  LEFT JOIN 

    (SELECT 

      SUM(pt.`payment_amount`) ptamount,

      pt.`refrence_id` 

    FROM

      `payments` AS pt 

    GROUP BY pt.`refrence_id`) ppt 

    ON ppt.`refrence_id` = c.`userid` 

  LEFT JOIN 

    (SELECT 

      SUM(ic.`sales_amount`) AS sales,

      ic.`customer_id` 

    FROM

      `an_invoice` AS ic 

    GROUP BY ic.`customer_id`) AS icc 

    ON icc.customer_id = c.userid 

    

WHERE c.type='2'and c.member_type!='5'

GROUP BY c.`userid` 

ORDER BY c.userid DESC ";

        $q = $this->db->query($sql);

        return $q->result();
    }

    function getAvgValue($storeid, $itemid) {
        $sql = "SELECT
  avp.`avg_value`
FROM
  `average_package` AS avp
WHERE avp.`status` = '1' AND avp.`store_id` = '$storeid'
  AND avp.`item_id` = '$itemid'
ORDER BY avp.`avg_pkg_id` DESC
LIMIT 1";

        //echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row()->avg_value;
        } else {
            return 0;
        }
    }

}

?>