<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['main']= 'Customers';
$lang['Customer-Name']= 'Customer Name';
$lang['Number-of-Invoices']= 'Number of Invoices';
$lang['Mobile-Number']= 'Mobile Number';
$lang['Type']= 'Type';
$lang['Code']= 'Code';
$lang['Debit']= 'Debit';
$lang['Credit']= 'Credit';

$lang['totalDebit']= 'Total Debit';
$lang['totalCredit']= 'Total Credit';

$lang['Total']= 'Total';
$lang['Status']= 'Status';
$lang['time']= 'Time of pay';
$lang['invoicedate'] = 'Invoice Date';

$lang['paytoinvoice']='pay to invoice';

$lang['remaining'] = 'Remaining';
$lang['balance'] = 'Balance';
$lang['totalremaining'] = 'Total Remaining';
$lang['totalbalance'] = 'Total Balance';

$lang['Notes']= 'Notes';
$lang['Responsable-Phone']= 'Responsable Phone';
$lang['Responsable-Name']= 'Responsable Name';
$lang['Address']= 'Address';
$lang['Email-Address']= 'Email-Address';
$lang['Fax-Number']= 'Fax-Number';
$lang['Contact-Number']= 'Contact-Number';
$lang['due_charges']= 'Due charges';
$lang['clear_due_charges']= 'Clear Due Charges';
$lang['from']= 'From';

$lang['on-Account']= 'On Account';
$lang['Advance']= 'Advance';
$lang['Against-Reference']= 'Against Reference';
$lang['Value']= 'Amount';
$lang['Accounts']= 'Accounts';
$lang['Payment-Type']= 'Payment Type';


$lang['Invoice-Number']= 'Invoice Number';
$lang['Total-Price']= 'Total-Price';
$lang['Net-Price']= 'Net-Price';
$lang['discountamount']= 'Discount amount';
$lang['Customer']= 'Customer';
$lang['Date']= 'Date';
$lang['Status']= 'Status';
$lang['Payments']= 'Payments';
$lang['Incomplete']= 'Incomplete';
$lang['Complete']= 'Complete';
$lang['Extra']= 'Extra';
$lang['Cash']= 'Cash';
$lang['Bank']= 'Bank';
$lang['choose']= 'Choose';
/*Form*/

$lang['add-edit']= 'Add & Edit Customer';
$lang['mess1']= 'Please Understand Clearly All The Data Before Entering';
$lang['Add']= 'Add';
$lang['Reset']= 'Reset';
$lang['Branch-Name']= 'Branch Name';
$lang['RePassword']= 'RePassword';
$lang['Password']= 'Password';
$lang['User-Name']= 'User Name';
$lang['Add-Account']= 'Add Account';
$lang['Company-Name']= 'Company-Name';




$lang['Opening-Balance']= 'Opening Balance';
$lang['Credit-Amount-Limit']= 'Credit Amount Limit';
$lang['Credit-Days-Limit']= 'Credit Days Limit';
$lang['Account-Type']= ' Account Type';
$lang['buywithtotal']= 'Sale by total';

$lang['All-statement']= 'All statement';
$lang['View-All-Debit-Credit']= 'View All Debit Credit';
$lang['Returen']= 'Returen';


$lang['add_tabss']= 'Add';
$lang['view_all_tabss']= 'View all';



$lang['All-statement']= 'All statement';

/**/

$lang['show_datatable']= 'Showing _START_ to _END_ of _TOTAL_ entries';
$lang['Previous']= 'Previous';
$lang['First']= 'First';
$lang['Last']= 'Last';
$lang['Next']= 'Next';
$lang['Search']= 'Search';
$lang['show_bylist']= 'Show _MENU_ entries';

$lang['supplier-Name']= 'Supplier';

$lang['Store-Name']= 'Store';


$lang['Item-Picture']= 'Picture';
$lang['Name']= 'Name  ';
$lang['Category-Name']= 'Category ';
$lang['BarCode-Number']= 'BarCode';

$lang['Product-Name-ar']= 'Product Name ar';
$lang['Product-Name-en']= 'Product Name en';
$lang['Category-Type']= 'Type';
$lang['Serial-No']= 'Serial No';
$lang['Category']= 'Category ';