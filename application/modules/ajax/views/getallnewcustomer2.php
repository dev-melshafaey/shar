
<script>
    //Writing by Noor 
    if ($('#new_data_table').length > 0)
    {
        var thleng = $('#new_data_table thead th').length - 1;
        $('#new_data_table thead th').each(function (index, value) {
            var title = $.trim($(this).html());
            var attr_id = $('#new_data_table thead th').eq($(this).index()).attr('id');
            if (index != '0' && thleng != index)
            {
                $(this).html(title + '<input type="search" class="form-control ' + attr_id + ' search_filter" placeholder="' + title + '" />');
            }
        });

        var new_data_table = $('#new_data_table').DataTable({
            "ordering": false,
            "oLanguage": {
                "sSearch": "",
                "oPaginate": {"sNext": "التالی", "sPrevious": "السابق"}
            }
        });

        new_data_table.columns().eq(0).each(function (colIdx)
        {
            $('input', new_data_table.column(colIdx).header()).on('keyup change', function ()
            {
                new_data_table.column(colIdx).search(this.value).draw();
            });
        });

        $('.search_filter').keyup(function () {
            $('#new_data_table td').removeHighlight().highlight($(this).val());
        });

        new_data_table.on('draw', function () {
            //tiptop();
        });
    }
</script>
<table id="new_data_table">
    <thead>
        <tr>
            <th  id="no_filter"></th>
            <th ><?php echo lang('Customer-Name') ?></th>
            <th   style="border: 0px !important;"><?php echo lang('Mobile-Number') ?></th>
            <th   style="border: 0px !important;"></th>

        </tr>
    </thead>
    <?php
    $cnt = 0;
    foreach ($this->ajax->users_list2() as $userdata) {
        $cnt++;
        ?>
        <tr>
            <td ><a href="#" onclick="add_get_customer(<?php echo $userdata->userid;?>)"><?php echo $userdata->userid; ?></a></td>
            <td ><a href="<?php echo base_url(); ?>customers/statics/<?php echo $userdata->userid; ?>" target="_new"><?php echo $userdata->fullname; ?></a></td>
            <!--<td><a href="<?php echo base_url(); ?>customers/invoices/<?php echo $userdata->userid; ?>"><?php echo $userdata->totalinvoice; ?></a></td>-->
            <td colspan="2"><a href="#" onclick="add_get_customer(<?php echo $userdata->userid;?>)"><?php echo $userdata->phone_number; ?></a></td>   


        </tr>
    <?php } ?>
</table>

