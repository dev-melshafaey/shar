
<div class="table-wrapper users-table">
    
        <div class="row head">
            <div class="col-md-12">
                <h4>

                    <div class="title"> <span><?php //echo lang('main') ?><?php breadcramb();  ?></span> </div>


                </h4>
                <?php error_hander($this->input->get('e')); ?>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">

<div class="form">
<div class="CSSTableGenerator" >
					<div style="float: left; margin-left: 12px; margin-top: 4px;">
            From Date 
            <input type="text" id="from_date_filter" class="form-control2" class="hasDatepicker"> 
            To Date 
            <input type="text" id="to_date_filter"  class="form-control2" class="hasDatepicker">
            <input type="button" value="Search" onclick="updateDataTableDiv();"></div>
                <table width="100%" align="left" >
                    <tr>
                        <td width="17%">Name</td>
                        <td width="17%">Debit</td>
                        <td width="80%" >Credit</td>
                    </tr>
                    <tr>
                        <td>Sale</td>
                        <td></td>
                        <td><?php echo $t_sales = $directData->total_sales; ?></td>
                    </tr>
                    <tr>
                        <td>Direct Expense</td>
                        <td><?php echo $t_purchase = $directData->total_purchase; ?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Gross Profit</td>
                        <td></td>
                        <td><?php echo $gross_profit = $t_sales- $t_purchase; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>Indirect Expense</h3></td>
                    </tr>
                    <?php 
						$total_ind = 0;	
						foreach($inDirectData as $indData){
							?>
                      	
                    <tr>
                        <td><?php  echo $indData->expense_title; ?></td>
                        <td><?php  $total_ind+= $indData->total_expense; echo $indData->total_expense; ?></td>
                        <td></td>
                    </tr>      
                            
                            <?php
						
						}
					?>
                    
                    <tr>
                        <td>Net Profit:</td>
                        <td><?php echo $gross_profit- $total_ind;?></td>
                        <td></td>
                    </tr>      
                  </table>
            </div>
            
</div>
</div>
      <!-- END PAGE -->  
   </div>
   </div>


<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>$(document).ready(function(){	

$( "#from_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to_date_filter" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from_date_filter" ).datepicker( "option", "maxDate", selectedDate );
}
});
});
</script>
<?php $this->load->view('common/footer');?>
