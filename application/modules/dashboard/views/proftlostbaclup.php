
<div class="table-wrapper users-table">
    
        <div class="row head">
            <div class="col-md-12">
                <h4>

                    <div class="title"> <span><?php //echo lang('main') ?><?php breadcramb();  ?></span> </div>


                </h4>
                <?php error_hander($this->input->get('e')); ?>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">

<div class="form">
<div class="CSSTableGenerator" >
					<div style="float: left; margin-left: 12px; margin-top: 4px;">
            From Date 
            <input type="text" id="from_date_filter" class="form-control2" class="hasDatepicker"> 
            To Date 
            <input type="text" id="to_date_filter"  class="form-control2" class="hasDatepicker">
            <input type="button" value="Search" onclick="updateDataTableDiv();"></div>
                <table width="100%" align="left" >
                    <tr>
                        <td width="17%">Name</td>
                        <td width="17%">Debit</td>
                        <td width="80%" >Credit</td>
                    </tr>
                    <tr>
                        <td>Sale</td>
                        <td><?php echo $t_sales = $directData->total_sales; ?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Direct Expense</td>
                        <td></td>
                        <td><?php echo $t_purchase = $directData->total_purchase; ?></td>
                    </tr>
                    <tr>
                        <td>Gross Profit</td>
                        <td><?php echo $gross_profit = $t_sales- $t_purchase; ?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>Indirect Expense</h3></td>
                    </tr>
                    <?php 
						$total_ind = 0;	
						foreach($inDirectData as $indData){
							?>
                      	
                    <tr>
                        <td><?php  echo $indData->expense_title; ?></td>
                        <td></td>
                        <td><?php  $total_ind+= $indData->total_expense; echo $indData->total_expense; ?></td>
                    </tr>      
                            
                            <?php
						
						}
					?>
                    
                    <tr>
                        <td>Net Profit:</td>
                        <td></td>
                        <td><?php echo $gross_profit- $total_ind;?></td>
                    </tr>      
                  </table>
            </div>
            
</div>
</div>
      <!-- END PAGE -->  
   </div>
   </div>


 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
$(function() {
//	alert('asd');
$( "#from_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to_date_filter" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from_date_filter" ).datepicker( "option", "maxDate", selectedDate );
}
});
/*$('#selectall').click(function(e) {
var table = $(e.target).parents('table:first');
$('td input:checkbox', table).attr('checked', e.target.checked);
});*/
/*$('#selectall').click(function(){
  var checked_status = this.checked;
  $(this).closest('table').find('input:checkbox').each(function(){
    this.checked = checked_status;
  });
})*/
/*$("#selectall").each(function() {
    // attach a click event on the checkbox in the header in each of table
    $(this).find("th:first input:checkbox").click(function() {
        var val = this.checked;
        // when clicked on a particular header, get the reference of the table & hence all the rows in that table, iterate over each of them.
        $(this).parents("table").find("tr").each(function() {
            // access the checkbox in the first column, and updates its value.
            $(this).find("td:first input:checkbox")[0].checked = val;
        });
    });
});*/
$('#selectall').change(function() {
                        var isSelected = $(this).is(':checked');
                        if(isSelected){
                            $('.allcb').prop('checked', true);   
                        }else{
                            $('.allcb').prop('checked', false);
                        }
                    });
					
					$('#btn_listing').click(function(){
						$('#listing').submit();
					})
					$('#btn_listing2').click(function(){
						$('#listing').submit();
					})
});
</script><!-- End Section-->
<script type="text/javascript">
$(function() {
	
        $('#expense_date').datepicker({dateFormat: 'yy-mm-dd'});
	    });
$(document).ready(function(){
	//alert('ready');
			var ac_config = {
		source: "<?php echo base_url();?>outcome/getAutoSearch",
		select: function(event, ui){
			$("#expense_title").val(ui.item.cus);
			$("#expense_id").val(ui.item.cus);
			console.log(ui);
			//swapme();
			setTimeout('swapgeneral()',500);
		},
		minLength:1
	};
	
	$("#expense_title").autocomplete(ac_config);
	
	$("#is_fixed").click(function(){
		//alert('asdasd');
		checked_status = $(this).is(':checked');
			//alert(checked_status);
		if(checked_status){
			$("#period_parent").show();
		}
		else{
			$("#period_parent").hide();	
		}
	})
});
</script>
<!--footer-->
<?php $this->load->view('common/footer');?>
