<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
</header>
<?php $this->load->view('common/left-navigations');?>
    
    
   
 
   <!-- END GLOBAL MANDATORY STYLES -->
   <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
<link href="<?php echo base_url()?>/mdash//plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url()?>/mdash//plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>/mdash//plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url()?>/mdash//plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url()?>/mdash//plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN THEME STYLES --> 
<link href="<?php echo base_url()?>/mdash//css/style-metronic.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url()?>/mdash//css/style.css" rel="stylesheet" type="text/css"/>

<!--<link href="<?php echo base_url()?>/mdash//css/style-responsive.css" rel="stylesheet" type="text/css"/>-->

<link href="<?php echo base_url()?>/mdash//css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url()?>/mdash//css/pages/tasks.css" rel="stylesheet" type="text/css"/>

<!--<link href="<?php echo base_url()?>/mdash//css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>-->
<!--<link href="<?php echo base_url()?>/mdash//css/custom.css" rel="stylesheet" type="text/css"/>-->
 

  <div class="content">

        <!-- settings changer -->
        <div class="skins-nav">
            <a href="#" class="skin first_nav selected">
                <span class="icon"></span><span class="text">Default skin</span>
            </a>
            <a href="#" class="skin second_nav" data-file="<?php echo base_url() ?>css/compiled/skins/dark.css">
                <span class="icon"></span><span class="text">Dark skin</span>
            </a>
        </div>

        <!-- upper main stats -->
<!--        <div id="main-stats">
            <div class="row stats-row">
                <div class="col-md-3 col-sm-3 stat">
                    <div class="data">
                        <span class="number">0</span>
                        <?php echo lang('Customers')?>
                    </div>
                    <span class="date">Today</span>
                </div>
                <div class="col-md-3 col-sm-3 stat">
                    <div class="data">
                        <span class="number">0</span>
                        <?php echo lang('users')?>
                    </div>
                    <span class="date"><?php echo date('M Y')?></span>
                </div>
                <div class="col-md-3 col-sm-3 stat">
                    <div class="data">
                        <span class="number">0</span>
                        <?php echo lang('sales')?>
                    </div>
                    <span class="date">This week</span>
                </div>
                <div class="col-md-3 col-sm-3 stat last">
                    <div class="data">
                        <span class="number">0 RO </span>
                         <?php echo lang('purchase')?>
                    </div>
                    <span class="date">last 30 days</span>
                </div>
            </div>
        </div>-->
        <!-- end upper main stats -->

<!--        <div id="pad-wrapper">

             statistics chart built with jQuery Flot 
            <div class="row chart">
                <div class="col-md-12">
                    <h4 class="clearfix pull-left">
                        <?php echo lang('Statistics')?>                         
                    </h4>
                    <div class="btn-group pull-right">
                        <button class="glow left"><?php echo lang('DAY')?>     </button>
                        <button class="glow middle active"><?php echo lang('MONTH')?> </button>
                        <button class="glow right"><?php echo lang('YEAR')?> </button>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="statsChart"></div>
                </div>
            </div>
          
        </div>-->
    
  
          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
               <div class="dashboard-stat blue">
                  <div class="visual">
                     <i class="icon-comments"></i>
                  </div>
                  <div class="details">
                     <div class="number">
                        1349
                     </div>
                     <div class="desc">                           
                        New Feedbacks
                     </div>
                  </div>
                  <a class="more" href="#">
                  View more <i class="m-icon-swapright m-icon-white"></i>
                  </a>                 
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
               <div class="dashboard-stat green">
                  <div class="visual">
                     <i class="icon-shopping-cart"></i>
                  </div>
                  <div class="details">
                     <div class="number">549</div>
                     <div class="desc">New Orders</div>
                  </div>
                  <a class="more" href="#">
                  View more <i class="m-icon-swapright m-icon-white"></i>
                  </a>                 
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
               <div class="dashboard-stat purple">
                  <div class="visual">
                     <i class="icon-globe"></i>
                  </div>
                  <div class="details">
                     <div class="number">+89%</div>
                     <div class="desc">Brand Popularity</div>
                  </div>
                  <a class="more" href="#">
                  View more <i class="m-icon-swapright m-icon-white"></i>
                  </a>                 
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
               <div class="dashboard-stat yellow">
                  <div class="visual">
                     <i class="icon-bar-chart"></i>
                  </div>
                  <div class="details">
                     <div class="number">12,5M$</div>
                     <div class="desc">Total Profit</div>
                  </div>
                  <a class="more" href="#">
                  View more <i class="m-icon-swapright m-icon-white"></i>
                  </a>                 
               </div>
            </div>
         </div>
         <!-- END DASHBOARD STATS -->
         <div class="clearfix"></div>
         <div class="row">
            <div class="col-md-6 col-sm-6">
               <!-- BEGIN PORTLET-->
               <div class="portlet solid bordered light-grey">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-bar-chart"></i>Site Visits</div>
                     <div class="tools">
                        <div class="btn-group" data-toggle="buttons">
                           <label class="btn default btn-sm active">
                           <input type="radio" name="options" class="toggle" id="option1">Users
                           </label>
                           <label class="btn default btn-sm">
                           <input type="radio" name="options" class="toggle" id="option2">Feedbacks
                           </label>
                        </div>
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div id="site_statistics_loading">
                        <img src="<?php echo base_url()?>mdash/img/loading.gif" alt="loading"/>
                     </div>
                     <div id="site_statistics_content" class="display-none">
                        <div id="site_statistics" class="chart"></div>
                     </div>
                  </div>
               </div>
               <!-- END PORTLET-->
            </div>
            <div class="col-md-6 col-sm-6">
               <!-- BEGIN PORTLET-->
               <div class="portlet solid light-grey bordered">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-bullhorn"></i>Activities</div>
                     <div class="tools">
                        <div class="btn-group pull-right" data-toggle="buttons">
                           <a href="" class="btn blue btn-sm active">Users</a>
                           <a href="" class="btn blue btn-sm">Orders</a>
                        </div>
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div id="site_activities_loading">
                        <img src="<?php echo base_url()?>mdash/img/loading.gif" alt="loading"/>
                     </div>
                     <div id="site_activities_content" class="display-none">
                        <div id="site_activities" style="height: 100px;"></div>
                     </div>
                  </div>
               </div>
               <!-- END PORTLET-->
               <!-- BEGIN PORTLET-->
               <div class="portlet solid bordered light-grey">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-signal"></i>Server Load</div>
                     <div class="tools">
                        <div class="btn-group pull-right" data-toggle="buttons">
                           <a href="" class="btn red btn-sm active">Database</a>
                           <a href="" class="btn red btn-sm">Web</a>
                        </div>
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div id="load_statistics_loading">
                        <img src="<?php echo base_url()?>mdash/img/loading.gif" alt="loading" />
                     </div>
                     <div id="load_statistics_content" class="display-none">
                        <div id="load_statistics" style="height: 108px;"></div>
                     </div>
                  </div>
               </div>
               <!-- END PORTLET-->
            </div>
         </div>
         <div class="clearfix"></div>
         <div class="row ">
            <div class="col-md-6 col-sm-6">
               <div class="portlet box blue">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-bell"></i>Recent Activities</div>
                     <div class="actions">
                        <div class="btn-group">
                           <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                           Filter By
                           <i class="icon-angle-down"></i>
                           </a>
                           <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                              <label><input type="checkbox" /> Finance</label>
                              <label><input type="checkbox" checked="" /> Membership</label>
                              <label><input type="checkbox" /> Customer Support</label>
                              <label><input type="checkbox" checked="" /> HR</label>
                              <label><input type="checkbox" /> System</label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                        <ul class="feeds">
                           <li>
                              <div class="col1">
                                 <div class="cont">
                                    <div class="cont-col1">
                                       <div class="label label-sm label-info">                        
                                          <i class="icon-check"></i>
                                       </div>
                                    </div>
                                    <div class="cont-col2">
                                       <div class="desc">
                                          You have 4 pending tasks.
                                          <span class="label label-sm label-warning ">
                                          Take action 
                                          <i class="icon-share-alt"></i>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col2">
                                 <div class="date">
                                    Just now
                                 </div>
                              </div>
                           </li>
                           <li>
                              <a href="#">
                                 <div class="col1">
                                    <div class="cont">
                                       <div class="cont-col1">
                                          <div class="label label-sm label-success">                        
                                             <i class="icon-bar-chart"></i>
                                          </div>
                                       </div>
                                       <div class="cont-col2">
                                          <div class="desc">
                                             Finance Report for year 2013 has been released.   
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col2">
                                    <div class="date">
                                       20 mins
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <div class="col1">
                                 <div class="cont">
                                    <div class="cont-col1">
                                       <div class="label label-sm label-danger">                      
                                          <i class="icon-user"></i>
                                       </div>
                                    </div>
                                    <div class="cont-col2">
                                       <div class="desc">
                                          You have 5 pending membership that requires a quick review.                       
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col2">
                                 <div class="date">
                                    24 mins
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="col1">
                                 <div class="cont">
                                    <div class="cont-col1">
                                       <div class="label label-sm label-info">                        
                                          <i class="icon-shopping-cart"></i>
                                       </div>
                                    </div>
                                    <div class="cont-col2">
                                       <div class="desc">
                                          New order received with <span class="label label-sm label-success">Reference Number: DR23923</span>             
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col2">
                                 <div class="date">
                                    30 mins
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="col1">
                                 <div class="cont">
                                    <div class="cont-col1">
                                       <div class="label label-sm label-success">                      
                                          <i class="icon-user"></i>
                                       </div>
                                    </div>
                                    <div class="cont-col2">
                                       <div class="desc">
                                          You have 5 pending membership that requires a quick review.                       
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col2">
                                 <div class="date">
                                    24 mins
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="col1">
                                 <div class="cont">
                                    <div class="cont-col1">
                                       <div class="label label-sm label-default">                        
                                          <i class="icon-bell-alt"></i>
                                       </div>
                                    </div>
                                    <div class="cont-col2">
                                       <div class="desc">
                                          Web server hardware needs to be upgraded. 
                                          <span class="label label-sm label-default ">Overdue</span>             
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col2">
                                 <div class="date">
                                    2 hours
                                 </div>
                              </div>
                           </li>
                           <li>
                              <a href="#">
                                 <div class="col1">
                                    <div class="cont">
                                       <div class="cont-col1">
                                          <div class="label label-sm label-default">                        
                                             <i class="icon-briefcase"></i>
                                          </div>
                                       </div>
                                       <div class="cont-col2">
                                          <div class="desc">
                                             IPO Report for year 2013 has been released.   
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col2">
                                    <div class="date">
                                       20 mins
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <div class="col1">
                                 <div class="cont">
                                    <div class="cont-col1">
                                       <div class="label label-sm label-info">                        
                                          <i class="icon-check"></i>
                                       </div>
                                    </div>
                                    <div class="cont-col2">
                                       <div class="desc">
                                          You have 4 pending tasks.
                                          <span class="label label-sm label-warning ">
                                          Take action 
                                          <i class="icon-share-alt"></i>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col2">
                                 <div class="date">
                                    Just now
                                 </div>
                              </div>
                           </li>
                           <li>
                              <a href="#">
                                 <div class="col1">
                                    <div class="cont">
                                       <div class="cont-col1">
                                          <div class="label label-sm label-danger">                        
                                             <i class="icon-bar-chart"></i>
                                          </div>
                                       </div>
                                       <div class="cont-col2">
                                          <div class="desc">
                                             Finance Report for year 2013 has been released.   
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col2">
                                    <div class="date">
                                       20 mins
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <div class="col1">
                                 <div class="cont">
                                    <div class="cont-col1">
                                       <div class="label label-sm label-default">                      
                                          <i class="icon-user"></i>
                                       </div>
                                    </div>
                                    <div class="cont-col2">
                                       <div class="desc">
                                          You have 5 pending membership that requires a quick review.                       
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col2">
                                 <div class="date">
                                    24 mins
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="col1">
                                 <div class="cont">
                                    <div class="cont-col1">
                                       <div class="label label-sm label-info">                        
                                          <i class="icon-shopping-cart"></i>
                                       </div>
                                    </div>
                                    <div class="cont-col2">
                                       <div class="desc">
                                          New order received with <span class="label label-sm label-success">Reference Number: DR23923</span>             
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col2">
                                 <div class="date">
                                    30 mins
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="col1">
                                 <div class="cont">
                                    <div class="cont-col1">
                                       <div class="label label-sm label-success">                      
                                          <i class="icon-user"></i>
                                       </div>
                                    </div>
                                    <div class="cont-col2">
                                       <div class="desc">
                                          You have 5 pending membership that requires a quick review.                       
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col2">
                                 <div class="date">
                                    24 mins
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="col1">
                                 <div class="cont">
                                    <div class="cont-col1">
                                       <div class="label label-sm label-warning">                        
                                          <i class="icon-bell-alt"></i>
                                       </div>
                                    </div>
                                    <div class="cont-col2">
                                       <div class="desc">
                                          Web server hardware needs to be upgraded. 
                                          <span class="label label-sm label-default ">Overdue</span>             
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col2">
                                 <div class="date">
                                    2 hours
                                 </div>
                              </div>
                           </li>
                           <li>
                              <a href="#">
                                 <div class="col1">
                                    <div class="cont">
                                       <div class="cont-col1">
                                          <div class="label label-sm label-info">                        
                                             <i class="icon-briefcase"></i>
                                          </div>
                                       </div>
                                       <div class="cont-col2">
                                          <div class="desc">
                                             IPO Report for year 2013 has been released.   
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col2">
                                    <div class="date">
                                       20 mins
                                    </div>
                                 </div>
                              </a>
                           </li>
                        </ul>
                     </div>
                     <div class="scroller-footer">
                        <div class="pull-right">
                           <a href="#">See All Records <i class="m-icon-swapright m-icon-gray"></i></a> &nbsp;
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6 col-sm-6">
               <div class="portlet box green tasks-widget">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-check"></i>Tasks</div>
                     <div class="tools">
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="" class="reload"></a>
                     </div>
                     <div class="actions">
                        <div class="btn-group">
                           <a class="btn default btn-xs" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                           More
                           <i class="icon-angle-down"></i>
                           </a>
                           <ul class="dropdown-menu pull-right">
                              <li><a href="#"><i class="i"></i> All Project</a></li>
                              <li class="divider"></li>
                              <li><a href="#">AirAsia</a></li>
                              <li><a href="#">Cruise</a></li>
                              <li><a href="#">HSBC</a></li>
                              <li class="divider"></li>
                              <li><a href="#">Pending <span class="badge badge-important">4</span></a></li>
                              <li><a href="#">Completed <span class="badge badge-success">12</span></a></li>
                              <li><a href="#">Overdue <span class="badge badge-warning">9</span></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div class="task-content">
                        <div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="1">
                           <!-- START TASK LIST -->
                           <ul class="task-list">
                              <li>
                                 <div class="task-checkbox">
                                    <input type="checkbox" class="liChild" value=""  />                                       
                                 </div>
                                 <div class="task-title">
                                    <span class="task-title-sp">Present 2013 Year IPO Statistics at Board Meeting</span>
                                    <span class="label label-sm label-success">Company</span>
                                    <span class="task-bell"><i class="icon-bell"></i></span>
                                 </div>
                                 <div class="task-config">
                                    <div class="task-config-btn btn-group">
                                       <a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                       <i class="icon-cog"></i><i class="icon-angle-down"></i></a>
                                       <ul class="dropdown-menu pull-right">
                                          <li><a href="#"><i class="icon-ok"></i> Complete</a></li>
                                          <li><a href="#"><i class="icon-pencil"></i> Edit</a></li>
                                          <li><a href="#"><i class="icon-trash"></i> Cancel</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </li>
                              <li>
                                 <div class="task-checkbox">
                                    <input type="checkbox" class="liChild" value=""/>                                       
                                 </div>
                                 <div class="task-title">
                                    <span class="task-title-sp">Hold An Interview for Marketing Manager Position</span>
                                    <span class="label label-sm label-danger">Marketing</span>
                                 </div>
                                 <div class="task-config">
                                    <div class="task-config-btn btn-group">
                                       <a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                       <i class="icon-cog"></i><i class="icon-angle-down"></i></a>
                                       <ul class="dropdown-menu pull-right">
                                          <li><a href="#"><i class="icon-ok"></i> Complete</a></li>
                                          <li><a href="#"><i class="icon-pencil"></i> Edit</a></li>
                                          <li><a href="#"><i class="icon-trash"></i> Cancel</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </li>
                              <li>
                                 <div class="task-checkbox">
                                    <input type="checkbox" class="liChild" value=""/>                                       
                                 </div>
                                 <div class="task-title">
                                    <span class="task-title-sp">AirAsia Intranet System Project Internal Meeting</span>
                                    <span class="label label-sm label-success">AirAsia</span>
                                    <span class="task-bell"><i class="icon-bell"></i></span>
                                 </div>
                                 <div class="task-config">
                                    <div class="task-config-btn btn-group">
                                       <a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                       <i class="icon-cog"></i><i class="icon-angle-down"></i></a>
                                       <ul class="dropdown-menu pull-right">
                                          <li><a href="#"><i class="icon-ok"></i> Complete</a></li>
                                          <li><a href="#"><i class="icon-pencil"></i> Edit</a></li>
                                          <li><a href="#"><i class="icon-trash"></i> Cancel</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </li>
                              <li>
                                 <div class="task-checkbox">
                                    <input type="checkbox" class="liChild" value=""  />                                       
                                 </div>
                                 <div class="task-title">
                                    <span class="task-title-sp">Technical Management Meeting</span>
                                    <span class="label label-sm label-warning">Company</span>
                                 </div>
                                 <div class="task-config">
                                    <div class="task-config-btn btn-group">
                                       <a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class="icon-cog"></i><i class="icon-angle-down"></i></a>
                                       <ul class="dropdown-menu pull-right">
                                          <li><a href="#"><i class="icon-ok"></i> Complete</a></li>
                                          <li><a href="#"><i class="icon-pencil"></i> Edit</a></li>
                                          <li><a href="#"><i class="icon-trash"></i> Cancel</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </li>
                              <li>
                                 <div class="task-checkbox">
                                    <input type="checkbox" class="liChild" value=""  />                                       
                                 </div>
                                 <div class="task-title">
                                    <span class="task-title-sp">Kick-off Company CRM Mobile App Development</span>
                                    <span class="label label-sm label-info">Internal Products</span>
                                 </div>
                                 <div class="task-config">
                                    <div class="task-config-btn btn-group">
                                       <a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class="icon-cog"></i><i class="icon-angle-down"></i></a>
                                       <ul class="dropdown-menu pull-right">
                                          <li><a href="#"><i class="icon-ok"></i> Complete</a></li>
                                          <li><a href="#"><i class="icon-pencil"></i> Edit</a></li>
                                          <li><a href="#"><i class="icon-trash"></i> Cancel</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </li>
                              <li>
                                 <div class="task-checkbox">
                                    <input type="checkbox" class="liChild" value=""  />                                       
                                 </div>
                                 <div class="task-title">
                                    <span class="task-title-sp">
                                    Prepare Commercial Offer For SmartVision Website Rewamp 
                                    </span>
                                    <span class="label label-sm label-danger">SmartVision</span>
                                 </div>
                                 <div class="task-config">
                                    <div class="task-config-btn btn-group">
                                       <a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class="icon-cog"></i><i class="icon-angle-down"></i></a>
                                       <ul class="dropdown-menu pull-right">
                                          <li><a href="#"><i class="icon-ok"></i> Complete</a></li>
                                          <li><a href="#"><i class="icon-pencil"></i> Edit</a></li>
                                          <li><a href="#"><i class="icon-trash"></i> Cancel</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </li>
                              <li>
                                 <div class="task-checkbox">
                                    <input type="checkbox" class="liChild" value=""  />                                       
                                 </div>
                                 <div class="task-title">
                                    <span class="task-title-sp">Sign-Off The Comercial Agreement With AutoSmart</span>
                                    <span class="label label-sm label-default">AutoSmart</span>
                                    <span class="task-bell"><i class="icon-bell"></i></span>
                                 </div>
                                 <div class="task-config">
                                    <div class="task-config-btn btn-group">
                                       <a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class="icon-cog"></i><i class="icon-angle-down"></i></a>
                                       <ul class="dropdown-menu pull-right">
                                          <li><a href="#"><i class="icon-ok"></i> Complete</a></li>
                                          <li><a href="#"><i class="icon-pencil"></i> Edit</a></li>
                                          <li><a href="#"><i class="icon-trash"></i> Cancel</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </li>
                              <li>
                                 <div class="task-checkbox">
                                    <input type="checkbox" class="liChild" value=""  />                                       
                                 </div>
                                 <div class="task-title">
                                    <span class="task-title-sp">Company Staff Meeting</span>
                                    <span class="label label-sm label-success">Cruise</span>
                                    <span class="task-bell"><i class="icon-bell"></i></span>
                                 </div>
                                 <div class="task-config">
                                    <div class="task-config-btn btn-group">
                                       <a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class="icon-cog"></i><i class="icon-angle-down"></i></a>
                                       <ul class="dropdown-menu pull-right">
                                          <li><a href="#"><i class="icon-ok"></i> Complete</a></li>
                                          <li><a href="#"><i class="icon-pencil"></i> Edit</a></li>
                                          <li><a href="#"><i class="icon-trash"></i> Cancel</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </li>
                              <li class="last-line">
                                 <div class="task-checkbox">
                                    <input type="checkbox" class="liChild" value=""  />                                       
                                 </div>
                                 <div class="task-title">
                                    <span class="task-title-sp">KeenThemes Investment Discussion</span>
                                    <span class="label label-sm label-warning">KeenThemes</span>
                                 </div>
                                 <div class="task-config">
                                    <div class="task-config-btn btn-group">
                                       <a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class="icon-cog"></i><i class="icon-angle-down"></i></a>
                                       <ul class="dropdown-menu pull-right">
                                          <li><a href="#"><i class="icon-ok"></i> Complete</a></li>
                                          <li><a href="#"><i class="icon-pencil"></i> Edit</a></li>
                                          <li><a href="#"><i class="icon-trash"></i> Cancel</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </li>
                           </ul>
                           <!-- END START TASK LIST -->
                        </div>
                     </div>
                     <div class="task-footer">
                        <span class="pull-right">
                        <a href="#">See All Tasks <i class="m-icon-swapright m-icon-gray"></i></a> &nbsp;
                        </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
         <div class="row ">
            <div class="col-md-6 col-sm-6">
               <div class="portlet box purple">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-calendar"></i>General Stats</div>
                     <div class="actions">
                        <a href="javascript:;" class="btn btn-sm yellow easy-pie-chart-reload"><i class="icon-repeat"></i> Reload</a>
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div class="row">
                        <div class="col-md-4">
                           <div class="easy-pie-chart">
                              <div class="number transactions" data-percent="55"><span>+55</span>%</div>
                              <a class="title" href="#">Transactions <i class="m-icon-swapright"></i></a>
                           </div>
                        </div>
                        <div class="margin-bottom-10 visible-sm"></div>
                        <div class="col-md-4">
                           <div class="easy-pie-chart">
                              <div class="number visits" data-percent="85"><span>+85</span>%</div>
                              <a class="title" href="#">New Visits <i class="m-icon-swapright"></i></a>
                           </div>
                        </div>
                        <div class="margin-bottom-10 visible-sm"></div>
                        <div class="col-md-4">
                           <div class="easy-pie-chart">
                              <div class="number bounce" data-percent="46"><span>-46</span>%</div>
                              <a class="title" href="#">Bounce <i class="m-icon-swapright"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6 col-sm-6">
               <div class="portlet box blue">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-calendar"></i>Server Stats</div>
                     <div class="tools">
                        <a href="" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="" class="reload"></a>
                        <a href="" class="remove"></a>
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div class="row">
                        <div class="col-md-4">
                           <div class="sparkline-chart">
                              <div class="number" id="sparkline_bar"></div>
                              <a class="title" href="#">Network <i class="m-icon-swapright"></i></a>
                           </div>
                        </div>
                        <div class="margin-bottom-10 visible-sm"></div>
                        <div class="col-md-4">
                           <div class="sparkline-chart">
                              <div class="number" id="sparkline_bar2"></div>
                              <a class="title" href="#">CPU Load <i class="m-icon-swapright"></i></a>
                           </div>
                        </div>
                        <div class="margin-bottom-10 visible-sm"></div>
                        <div class="col-md-4">
                           <div class="sparkline-chart">
                              <div class="number" id="sparkline_line"></div>
                              <a class="title" href="#">Load Rate <i class="m-icon-swapright"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
         <div class="row ">
            <div class="col-md-6 col-sm-6">
               <!-- BEGIN REGIONAL STATS PORTLET-->
               <div class="portlet">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-globe"></i>Regional Stats</div>
                     <div class="tools">
                        <a href="" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="" class="reload"></a>
                        <a href="" class="remove"></a>
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div id="region_statistics_loading">
                        <img src="<?php echo base_url()?>mdash/img/loading.gif" alt="loading"/>
                     </div>
                     <div id="region_statistics_content" class="display-none">
                        <div class="btn-toolbar margin-bottom-10">
                           <div class="btn-group" data-toggle="buttons">
                              <a href="" class="btn default btn-sm active">Users</a>
                              <a href="" class="btn default btn-sm">Orders</a> 
                           </div>
                           <div class="btn-group pull-right">
                              <a href="" class="btn default btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                              Select Region <span class="icon-angle-down"></span>
                              </a>
                              <ul class="dropdown-menu pull-right">
                                 <li><a href="javascript:;" id="regional_stat_world">World</a></li>
                                 <li><a href="javascript:;" id="regional_stat_usa">USA</a></li>
                                 <li><a href="javascript:;" id="regional_stat_europe">Europe</a></li>
                                 <li><a href="javascript:;" id="regional_stat_russia">Russia</a></li>
                                 <li><a href="javascript:;" id="regional_stat_germany">Germany</a></li>
                              </ul>
                           </div>
                        </div>
                        <div id="vmap_world" class="vmaps display-none"></div>
                        <div id="vmap_usa" class="vmaps display-none"></div>
                        <div id="vmap_europe" class="vmaps display-none"></div>
                        <div id="vmap_russia" class="vmaps display-none"></div>
                        <div id="vmap_germany" class="vmaps display-none"></div>
                     </div>
                  </div>
               </div>
               <!-- END REGIONAL STATS PORTLET-->
            </div>
            <div class="col-md-6 col-sm-6">
               <!-- BEGIN PORTLET-->
               <div class="portlet paddingless">
                  <div class="portlet-title line">
                     <div class="caption"><i class="icon-bell"></i>Feeds</div>
                     <div class="tools">
                        <a href="" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="" class="reload"></a>
                        <a href="" class="remove"></a>
                     </div>
                  </div>
                  <div class="portlet-body">
                     <!--BEGIN TABS-->
                     <div class="tabbable tabbable-custom">
                        <ul class="nav nav-tabs">
                           <li class="active"><a href="#tab_1_1" data-toggle="tab">System</a></li>
                           <li><a href="#tab_1_2" data-toggle="tab">Activities</a></li>
                           <li><a href="#tab_1_3" data-toggle="tab">Recent Users</a></li>
                        </ul>
                        <div class="tab-content">
                           <div class="tab-pane active" id="tab_1_1">
                              <div class="scroller" style="height: 290px;" data-always-visible="1" data-rail-visible="0">
                                 <ul class="feeds">
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-success">                        
                                                   <i class="icon-bell"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   You have 4 pending tasks.
                                                   <span class="label label-sm label-danger ">
                                                   Take action 
                                                   <i class="icon-share-alt"></i>
                                                   </span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             Just now
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <div class="col1">
                                             <div class="cont">
                                                <div class="cont-col1">
                                                   <div class="label label-sm label-success">                        
                                                      <i class="icon-bell"></i>
                                                   </div>
                                                </div>
                                                <div class="cont-col2">
                                                   <div class="desc">
                                                      New version v1.4 just lunched!   
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col2">
                                             <div class="date">
                                                20 mins
                                             </div>
                                          </div>
                                       </a>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-danger">                      
                                                   <i class="icon-bolt"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   Database server #12 overloaded. Please fix the issue.                      
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             24 mins
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-info">                        
                                                   <i class="icon-bullhorn"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New order received. Please take care of it.                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             30 mins
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-success">                        
                                                   <i class="icon-bullhorn"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New order received. Please take care of it.                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             40 mins
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-warning">                        
                                                   <i class="icon-plus"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New user registered.                
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             1.5 hours
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-success">                        
                                                   <i class="icon-bell-alt"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   Web server hardware needs to be upgraded. 
                                                   <span class="label label-sm label-default ">Overdue</span>             
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             2 hours
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-default">                       
                                                   <i class="icon-bullhorn"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New order received. Please take care of it.                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             3 hours
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-warning">                        
                                                   <i class="icon-bullhorn"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New order received. Please take care of it.                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             5 hours
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-info">                        
                                                   <i class="icon-bullhorn"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New order received. Please take care of it.                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             18 hours
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-default">                       
                                                   <i class="icon-bullhorn"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New order received. Please take care of it.                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             21 hours
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-info">                        
                                                   <i class="icon-bullhorn"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New order received. Please take care of it.                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             22 hours
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-default">                       
                                                   <i class="icon-bullhorn"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New order received. Please take care of it.                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             21 hours
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-info">                        
                                                   <i class="icon-bullhorn"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New order received. Please take care of it.                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             22 hours
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-default">                       
                                                   <i class="icon-bullhorn"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New order received. Please take care of it.                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             21 hours
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-info">                        
                                                   <i class="icon-bullhorn"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New order received. Please take care of it.                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             22 hours
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-default">                       
                                                   <i class="icon-bullhorn"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New order received. Please take care of it.                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             21 hours
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-info">                        
                                                   <i class="icon-bullhorn"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   New order received. Please take care of it.                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             22 hours
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="tab-pane" id="tab_1_2">
                              <div class="scroller" style="height: 290px;" data-always-visible="1" data-rail-visible1="1">
                                 <ul class="feeds">
                                    <li>
                                       <a href="#">
                                          <div class="col1">
                                             <div class="cont">
                                                <div class="cont-col1">
                                                   <div class="label label-sm label-success">                        
                                                      <i class="icon-bell"></i>
                                                   </div>
                                                </div>
                                                <div class="cont-col2">
                                                   <div class="desc">
                                                      New user registered
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col2">
                                             <div class="date">
                                                Just now
                                             </div>
                                          </div>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <div class="col1">
                                             <div class="cont">
                                                <div class="cont-col1">
                                                   <div class="label label-sm label-success">                        
                                                      <i class="icon-bell"></i>
                                                   </div>
                                                </div>
                                                <div class="cont-col2">
                                                   <div class="desc">
                                                      New order received 
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col2">
                                             <div class="date">
                                                10 mins
                                             </div>
                                          </div>
                                       </a>
                                    </li>
                                    <li>
                                       <div class="col1">
                                          <div class="cont">
                                             <div class="cont-col1">
                                                <div class="label label-sm label-danger">                      
                                                   <i class="icon-bolt"></i>
                                                </div>
                                             </div>
                                             <div class="cont-col2">
                                                <div class="desc">
                                                   Order #24DOP4 has been rejected.    
                                                   <span class="label label-sm label-danger ">Take action <i class="icon-share-alt"></i></span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col2">
                                          <div class="date">
                                             24 mins
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <div class="col1">
                                             <div class="cont">
                                                <div class="cont-col1">
                                                   <div class="label label-sm label-success">                        
                                                      <i class="icon-bell"></i>
                                                   </div>
                                                </div>
                                                <div class="cont-col2">
                                                   <div class="desc">
                                                      New user registered
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col2">
                                             <div class="date">
                                                Just now
                                             </div>
                                          </div>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <div class="col1">
                                             <div class="cont">
                                                <div class="cont-col1">
                                                   <div class="label label-sm label-success">                        
                                                      <i class="icon-bell"></i>
                                                   </div>
                                                </div>
                                                <div class="cont-col2">
                                                   <div class="desc">
                                                      New user registered
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col2">
                                             <div class="date">
                                                Just now
                                             </div>
                                          </div>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <div class="col1">
                                             <div class="cont">
                                                <div class="cont-col1">
                                                   <div class="label label-sm label-success">                        
                                                      <i class="icon-bell"></i>
                                                   </div>
                                                </div>
                                                <div class="cont-col2">
                                                   <div class="desc">
                                                      New user registered
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col2">
                                             <div class="date">
                                                Just now
                                             </div>
                                          </div>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <div class="col1">
                                             <div class="cont">
                                                <div class="cont-col1">
                                                   <div class="label label-sm label-success">                        
                                                      <i class="icon-bell"></i>
                                                   </div>
                                                </div>
                                                <div class="cont-col2">
                                                   <div class="desc">
                                                      New user registered
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col2">
                                             <div class="date">
                                                Just now
                                             </div>
                                          </div>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <div class="col1">
                                             <div class="cont">
                                                <div class="cont-col1">
                                                   <div class="label label-sm label-success">                        
                                                      <i class="icon-bell"></i>
                                                   </div>
                                                </div>
                                                <div class="cont-col2">
                                                   <div class="desc">
                                                      New user registered
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col2">
                                             <div class="date">
                                                Just now
                                             </div>
                                          </div>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <div class="col1">
                                             <div class="cont">
                                                <div class="cont-col1">
                                                   <div class="label label-sm label-success">                        
                                                      <i class="icon-bell"></i>
                                                   </div>
                                                </div>
                                                <div class="cont-col2">
                                                   <div class="desc">
                                                      New user registered
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col2">
                                             <div class="date">
                                                Just now
                                             </div>
                                          </div>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <div class="col1">
                                             <div class="cont">
                                                <div class="cont-col1">
                                                   <div class="label label-sm label-success">                        
                                                      <i class="icon-bell"></i>
                                                   </div>
                                                </div>
                                                <div class="cont-col2">
                                                   <div class="desc">
                                                      New user registered
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col2">
                                             <div class="date">
                                                Just now
                                             </div>
                                          </div>
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="tab-pane" id="tab_1_3">
                              <div class="scroller" style="height: 290px;" data-always-visible="1" data-rail-visible1="1">
                                 <div class="row">
                                    <div class="col-md-6 user-info">
                                       <img alt="" src="<?php echo base_url()?>mdash/img/avatar.png" class="img-responsive" />
                                       <div class="details">
                                          <div>
                                             <a href="#">Robert Nilson</a> 
                                             <span class="label label-sm label-success label-mini">Approved</span>
                                          </div>
                                          <div>29 Jan 2013 10:45AM</div>
                                       </div>
                                    </div>
                                    <div class="col-md-6 user-info">
                                       <img alt="" src="<?php echo base_url()?>mdash/img/avatar.png" class="img-responsive" />
                                       <div class="details">
                                          <div>
                                             <a href="#">Lisa Miller</a> 
                                             <span class="label label-sm label-info">Pending</span>
                                          </div>
                                          <div>19 Jan 2013 10:45AM</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6 user-info">
                                       <img alt="" src="<?php echo base_url()?>mdash/img/avatar.png" class="img-responsive" />
                                       <div class="details">
                                          <div>
                                             <a href="#">Eric Kim</a> 
                                             <span class="label label-sm label-info">Pending</span>
                                          </div>
                                          <div>19 Jan 2013 12:45PM</div>
                                       </div>
                                    </div>
                                    <div class="col-md-6 user-info">
                                       <img alt="" src="<?php echo base_url()?>mdash/img/avatar.png" class="img-responsive" />
                                       <div class="details">
                                          <div>
                                             <a href="#">Lisa Miller</a> 
                                             <span class="label label-sm label-danger">In progress</span>
                                          </div>
                                          <div>19 Jan 2013 11:55PM</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6 user-info">
                                       <img alt="" src="<?php echo base_url()?>mdash/img/avatar.png" class="img-responsive" />
                                       <div class="details">
                                          <div>
                                             <a href="#">Eric Kim</a> 
                                             <span class="label label-sm label-info">Pending</span>
                                          </div>
                                          <div>19 Jan 2013 12:45PM</div>
                                       </div>
                                    </div>
                                    <div class="col-md-6 user-info">
                                       <img alt="" src="<?php echo base_url()?>mdash/img/avatar.png" class="img-responsive" />
                                       <div class="details">
                                          <div>
                                             <a href="#">Lisa Miller</a> 
                                             <span class="label label-sm label-danger">In progress</span>
                                          </div>
                                          <div>19 Jan 2013 11:55PM</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6 user-info">
                                       <img alt="" src="<?php echo base_url()?>mdash/img/avatar.png" class="img-responsive" />
                                       <div class="details">
                                          <div><a href="#">Eric Kim</a> <span class="label label-sm label-info">Pending</span></div>
                                          <div>19 Jan 2013 12:45PM</div>
                                       </div>
                                    </div>
                                    <div class="col-md-6 user-info">
                                       <img alt="" src="<?php echo base_url()?>mdash/img/avatar.png" class="img-responsive" />
                                       <div class="details">
                                          <div>
                                             <a href="#">Lisa Miller</a> 
                                             <span class="label label-sm label-danger">In progress</span>
                                          </div>
                                          <div>19 Jan 2013 11:55PM</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6 user-info">
                                       <img alt="" src="<?php echo base_url()?>mdash/img/avatar.png" class="img-responsive" />
                                       <div class="details">
                                          <div><a href="#">Eric Kim</a> <span class="label label-sm label-info">Pending</span></div>
                                          <div>19 Jan 2013 12:45PM</div>
                                       </div>
                                    </div>
                                    <div class="col-md-6 user-info">
                                       <img alt="" src="<?php echo base_url()?>mdash/img/avatar.png" class="img-responsive" />
                                       <div class="details">
                                          <div>
                                             <a href="#">Lisa Miller</a> 
                                             <span class="label label-sm label-danger">In progress</span>
                                          </div>
                                          <div>19 Jan 2013 11:55PM</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6 user-info">
                                       <img alt="" src="<?php echo base_url()?>mdash/img/avatar.png" class="img-responsive" />
                                       <div class="details">
                                          <div>
                                             <a href="#">Eric Kim</a> 
                                             <span class="label label-sm label-info">Pending</span>
                                          </div>
                                          <div>19 Jan 2013 12:45PM</div>
                                       </div>
                                    </div>
                                    <div class="col-md-6 user-info">
                                       <img alt="" src="<?php echo base_url()?>mdash/img/avatar.png" class="img-responsive" />
                                       <div class="details">
                                          <div>
                                             <a href="#">Lisa Miller</a> 
                                             <span class="label label-sm label-danger">In progress</span>
                                          </div>
                                          <div>19 Jan 2013 11:55PM</div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!--END TABS-->
                  </div>
               </div>
               <!-- END PORTLET-->
            </div>
         </div>
         <div class="clearfix"></div>
         <div class="row ">
  
  
  
  </div><!--footer-->
  
<script src="<?php echo base_url()?>mdash//plugins/jquery-1.10.2.min.js" type="text/javascript"></script>  
  


<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.js"></script> 


<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script src="<?php echo base_url(); ?>js/formvalidate.js"></script> 
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script> 

<!-- knob --> 
<script src="<?php echo base_url(); ?>js/jquery.knob.js"></script> 
<!-- flot charts --> 
<script src="<?php echo base_url(); ?>js/jquery.flot.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.flot.stack.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.flot.resize.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.uniform.min.js"></script> 
<script src="<?php echo base_url(); ?>js/select2.min.js"></script> 
<script src="<?php echo base_url(); ?>js/theme.js"></script> 
<script src="<?php echo base_url(); ?>js/fuelux.wizard.js"></script> 
<script src="<?php echo base_url(); ?>js/wysihtml5-0.3.0.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/formValidation.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/formValidation.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/framework/bootstrap.js"></script> 
<script src="<?php echo base_url(); ?>fileUpload/js/vendor/jquery.ui.widget.js"></script> 


<?php //$this->load->view('common/footer');?>


<script src="<?php echo base_url()?>mdash//plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>   
 <!--IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip--> 
<script src="<?php echo base_url()?>mdash//plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>

<script src="<?php echo base_url()?>mdash//plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>mdash//plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url()?>mdash//plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>mdash//plugins/jquery.blockui.min.js" type="text/javascript"></script>  
<script src="<?php echo base_url()?>mdash//plugins/jquery.cookie.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url()?>mdash//plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>-->
  
<script src="<?php echo base_url()?>mdash/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
 <script src="<?php echo base_url()?>mdash/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
 <script src="<?php echo base_url()?>mdash/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
 <script src="<?php echo base_url()?>mdash/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
 <script src="<?php echo base_url()?>mdash/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
 <script src="<?php echo base_url()?>mdash/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
 <script src="<?php echo base_url()?>mdash/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
 <script src="<?php echo base_url()?>mdash/plugins/flot/jquery.flot.js" type="text/javascript"></script>
 <script src="<?php echo base_url()?>mdash/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
 <script src="<?php echo base_url()?>mdash/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
 <script src="<?php echo base_url()?>mdash/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
 <script src="<?php echo base_url()?>mdash/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
 <script src="<?php echo base_url()?>mdash/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
 <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
 <script src="<?php echo base_url()?>mdash/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
 <script src="<?php echo base_url()?>mdash/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
 <script src="<?php echo base_url()?>mdash/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
 <!-- END PAGE LEVEL PLUGINS -->
 <!-- BEGIN PAGE LEVEL SCRIPTS -->
 <script src="<?php echo base_url()?>mdash/scripts/app.js" type="text/javascript"></script>
 <script src="<?php echo base_url()?>mdash/scripts/index.js" type="text/javascript"></script>
 <script src="<?php echo base_url()?>mdash/scripts/tasks.js" type="text/javascript"></script>        
 <!-- END PAGE LEVEL SCRIPTS -->  
 <script>
    jQuery(document).ready(function() {    
       App.init(); // initlayout and core plugins
       Index.init();
       Index.initJQVMAP(); // init index page's custom scripts
       Index.initCalendar(); // init index page's custom scripts
       Index.initCharts(); // init index page's custom scripts
       Index.initChat();
       Index.initMiniCharts();
       Index.initDashboardDaterange();
       Index.initIntro();
       Tasks.initDashboardWidget();
    });
 </script>