
<div class="table-wrapper users-table">
    
        <div class="row head">
            <div class="col-md-12">
                <h4>

                    <div class="title"> <span><?php //echo lang('main') ?><?php breadcramb();  ?></span> </div>


                </h4>
                <?php error_hander($this->input->get('e')); ?>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">

      
      <form action="<?php echo base_url().'invoices/invoices/getAllInvoices'?>" id="listing" method="post" autocomplete="off">
        <div class="form" style="background:none;">
          <div class="CSSTableGenerator " id="printdiv" >
          	<div style="margin-top:30px;">
            <div class="g4">
            <div class="form_title"> From Date</div>  <div class="form_field"> <input type="text" id="from_date_filter" class="form-control2"></div>
            </div>
            <div class="g4">
            <div class="form_title"> To Date</div>  <div class="form_field">  <input type="text" id="to_date_filter"  class="form-control2" ></div>
            </div> 
            <div class="g3"><div class="form_title" style="visibility:hidden;">submit</div><div class="form_field"><input type="button" onclick="searcReports('balancesheet')" value="Search"></div></div></div>
     </div>
      <div class="CSSTableGenerator g4 form-group">
                                    <label class="text-warning"><?php echo lang('selectbranch') ?> </label>
                                    <div class="">
                                        <div class="ui-select" style="width:100%">
                                            <div class="">
                                             <?php getBranhes(1,''); ?>
                                                <span class="arrow arrowselectbox">&amp;</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
      <!-- END PAGE -->  
      <div class="CSSTableGenerator"  id="balance_data">
      		 <table width="100%" align="left" style=" border-style: solid;border-top:thick groove #ff0000;border-left:thick groove #ff0000;border-right:thick groove #ff0000;">
                	<tr>
                    <td colspan="4"><h3>Assets </h3></td>
                    </tr>
                    <tr>
                        <td width="27%"><strong>Fixed Assets</strong></td>
                        <td width="17%">Total</td>
                        <td width="30%" >Depressionation</td>
                    	<td width="17%">Current value</td>
                    </tr>
                    <?php
					$a_total = "";
					$total_asssests = 0;
					$total_deprission = 0;
					$total_current_value = 0;
						if(!empty($fixedData)){
								foreach($fixedData as $fixed){
									 $now = time(); // or your date as well
     								$your_date = strtotime($fixed->start_depression_date);
 			    					$datediff = $now - $your_date;
 								   $current_day =  floor($datediff/(60*60*24));
									?>
                                    <!--<tr>
                                        <td><?php //echo $fixed->assets_title; ?></td>
                                        <td><?php //echo $total = $fixed->total_value; ?></td>
                                        <td><?php //$depression_percent = 100/$fixed->depression_percentage;  $depression_percent_val = $fixed->total_value/$depression_percent;  $perday_percentage = 365/$depression_percent_val;  $final_deprsiation= $perday_percentage*$current_day; echo number_format($final_deprsiation, 2, '.', ' '); //echo number_format($final_deprsiation,2,'.', '');  //echo $fixed->total_values; ?></td>
                                        <td><?php  //$r_fix = $fixed->total_value - $final_deprsiation; echo  $r_fix =  number_format($r_fix,2, '.', ' '); $a_total=$a_total+$fixed->total_value; $total_asssests = $total_asssests+$fixed->total_value ?></td>
                                    </tr>-->
                                    <?php	
									$depression_percent = 100/$fixed->depression_percentage;  $depression_percent_val = $fixed->total_value/$depression_percent;  $perday_percentage = 365/$depression_percent_val;  $final_deprsiation= $perday_percentage*$current_day;  number_format($final_deprsiation, 2, '.', ' ');  $total_deprission+=$final_deprsiation;//echo number_format($final_deprsiation,2,'.', '');  //echo $fixed->total_values;							
									$r_fix = $fixed->total_value - $final_deprsiation;   $r_fix =  number_format($r_fix,2, '.', ' ');  $total_current_value+=$r_fix; $a_total=$a_total+$fixed->total_value; $total_asssests = $total_asssests+$fixed->total_value;
								}
						
						}
						
					?>
                    <tr>
                    	<td><a target="_blank" href="<?php echo base_url(); ?>assets/index"><?php echo "Total Assets (".count($fixedData).")";//echo $a_total."a_total"; ?></a></td>
                   		<td><a target="_blank" href="<?php echo base_url(); ?>assets/index"><?php  echo $total_asssests; ?></a></td>
                    	<td><a target="_blank" href="<?php echo base_url(); ?>assets/index"><?php echo number_format($total_deprission,2, '.', ' ');?></a></td>
                        <td><a target="_blank" href="<?php echo base_url(); ?>assets/index"><?php echo $total_current_value;  ?></a></td>	
                    </tr>
                  </table>
                 <table width="100%" align="left"  style="border-left:thick groove #ff0000;border-right:thick groove #ff0000 ">   
                    <tr>
                        <td width="17%" colspan="4"><h3><strong>Current Assets</strong></h3></td>
                     </tr>
                    <tr>
                        <td colspan="2">Cash</td>
                        <td colspan="2"><a target="_blank" href="<?php echo base_url() ?>cash_management/cash"><?php if(!empty($cashDataCredit)){ $cashcredit = $cashDataCredit->val; } else { $cashcredit =0; } if(!empty($cashDataDebit)){ $cashdebit = $cashDataDebit->val; } else { $cashdebit =0; } echo $casht = $cashdebit-$cashcredit; $a_total+=$casht;    ?></a></td>
                    </tr>
                    <tr>
                        <td colspan="2">Bank</td>
                        <td colspan="2"><a target="_blank" href="<?php echo base_url(); ?>transaction_management/banks"><?php  if(!empty($bankData) && $bankData[0]->transaction_type == 'credit' && count($bankData)==1){ echo '-'.$bankData[0]->val; } elseif(!empty($bankData)){ $debit = $bankData[0]->val; if(isset($bankData[1])){  $credit = $bankData[1]->val; } else{ $credit=0; } echo $a_banktotal  = $debit-$credit; $a_total+=$a_banktotal; } ?></a></td>
                    </tr>
                   </table>
                   <table width="100%" align="left"  style="border-left:thick groove #ff0000; border-right:thick groove #ff0000;" > 
                    <tr>
                        <td width="27%" colspan="2" rowspan="2">Loan & Advance</td>
                        <td width="30%"  colspan="2"><a href="<?php echo base_url() ?>dashboard/show_detail_report/advanceloan"><?php if(!empty($totaladvance)) echo $totaladvance->total_advance; else  echo 0; ?></a></td>
                    </tr>
                    </table>
                   <table width="100%" align="left"  style="border-left:thick groove #ff0000;border-right:thick groove #ff0000;border-bottom:thick groove #ff0000; ">  
                    <tr>
                        <td width="27%" colspan="2" rowspan="2">Customers</td>
                        <td width="30%" >Debit</td>
                    	<td width="17%">Credit</td>
                    </tr>
                    <tr>
                        
                        <td><a href="<?php echo base_url() ?>dashboard/show_detail_report/customersdebit"> <?php if(!empty($customerPayment) && $customerPayment->total_amount !="") if($customerPayment->payment_amount> $customerPayment->total_amount) echo $totaldebit = $customerPayment->payment_amount-$customerPayment->total_amount;  else echo $totaldebit = $customerPayment->total_amount-$customerPayment->payment_amount ; $a_total+=$totaldebit; ?></a></td>
                        <td><?php if(!empty($customerAdvance) && $customerAdvance->remaining_advance !="") echo $customercredit = $customerAdvance->remaining_advance; else echo $customercredit =0  ; $a_total+=-$customercredit; ?></td>
                    </tr>
                    <tr style="border: thick groove rgb(255, 0, 0);">
                        <td colspan="4">Total Assests & Current Assets value <?php echo $a_total; ?></td>
                    </tr>
                   </table>
                  </div>
                    <?php $l_total = 0;?>
                    <div class="CSSTableGenerator"  id="balance_data"> 
                               	 <table width="100%" align="left" style="margin-top:40px;border-top:thick groove #060000;border-right:thick groove #060000;border-left:thick groove #060000;border-bottom:thick groove #060000;">
                	<tr>
                    
                    <td colspan="4"><h3>Liablities </h3></td>
                    <tr>
                        <td width="27%" colspan="2" rowspan="2"><strong>Capital Account</strong></td>
                        <td width="30%"  colspan="2"><?php if(!empty($totalCapital)) echo $totalCapital->total_capital; else echo "0"; $l_total+=$totalCapital->total_capital; ?></td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td width="27%" colspan="2" rowspan="2"><strong>Loan Liablities</strong></td>
                        <td width="30%"  colspan="2"><a href="<?php echo base_url() ?>dashboard/show_detail_report/loan_liblities"><?php if(!empty($totalLiablities)) echo $totalLiablities->liablity_amount; else echo "0"; $l_total+=$totalLiablities->liablity_amount; ?></a></td>
                    </tr>
                    <table width="100%" align="left"  style="border-left:thick groove #060000;border-right:thick groove #060000;border-bottom:thick groove #060000;">   
                    <tr>
                        <td width="17%" colspan="4"><strong>Current Liablities</strong></td>
                     </tr>
                    <tr>
                        <td width="27%" colspan="2" rowspan="2">Supplier</td>
                        <td width="30%" >Debit</td>
                    	<td width="17%">Credit</td>
                    </tr>
                    <tr>
                        
                        <td><?php  if(!empty($advancePayment)) echo $advancePayment->remaining_advance; ?></td>
                        <td><a target="_blank" href="<?php echo base_url() ?>dashboard/show_detail_report/suppliercredit"><?php if(!empty($pendingPayment) && $pendingPayment->pending_payments !="") echo $pendingPayment->pending_payments;  elseif(!empty($pendingPayment)) echo $pendingPayment->total_amount;  $l_total+=$pendingPayment->total_amount;?></a></td>
                    </tr>
                    <!--<tr>
                        <td width="27%" colspan="2">Cash</td>
                        <td width="30%" >Debit</td>
                    	<td width="17%">Credit</td>
                    </tr>-->
                    
                        <?php
							/*if(!empty($cashliblities)){
								foreach($cashliblities as $cash){
									?>
                    				<tr>
                                    <td width="27%" colspan="2"><?php echo $cash->branch_cash_name; ?></td>
                        			<td width="30%" ><?php echo $cash->totaldebit; ?></td>
                    				<td width="17%"><?php echo $cash->totalcredit; ?></td>
                    
                                    </tr>                
                                    <?php
								
								}
							}	*/
						?>
                        
                       
                    
                    
                    <tr>
                        <td width="27%" colspan="1" rowspan="2">Accured Expense</td>
                        <td width="30%"  colspan="1" rowspan="2"><?php if(!empty($accuredExpense)) echo $accuredExpense->accured_val; ?></td>
                    </tr>
                    <tr><td></td><td></td></tr>
                     <tr>
                        <td width="27%" colspan="2" >Profit & Loss </td>
                        <td width="30%"  colspan="2"><?php if(!empty($totalProfit)) { echo $totalProfit;  $l_total+=$totalProfit; } ?></td>
                    </tr>
                    <tr style="border-top:thick groove #060000;">
                        <td colspan="3">Total Liablities Values (<?php echo $l_total; ?>)</td>
                    </tr>
                   </table>
                    </table>

            </div>
</div>
</div>
</div>
</div>

<!-- End Section-->
<!--footer-->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<!--<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>-->
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(function() {

$( "#from_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to_date_filter" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from_date_filter" ).datepicker( "option", "maxDate", selectedDate );
}
});
/*$('#selectall').click(function(e) {
var table = $(e.target).parents('table:first');
$('td input:checkbox', table).attr('checked', e.target.checked);
});*/
/*$('#selectall').click(function(){
  var checked_status = this.checked;
  $(this).closest('table').find('input:checkbox').each(function(){
    this.checked = checked_status;
  });
})*/
/*$("#selectall").each(function() {
    // attach a click event on the checkbox in the header in each of table
    $(this).find("th:first input:checkbox").click(function() {
        var val = this.checked;
        // when clicked on a particular header, get the reference of the table & hence all the rows in that table, iterate over each of them.
        $(this).parents("table").find("tr").each(function() {
            // access the checkbox in the first column, and updates its value.
            $(this).find("td:first input:checkbox")[0].checked = val;
        });
    });
});*/
$('#selectall').change(function() {
                        var isSelected = $(this).is(':checked');
                        if(isSelected){
                            $('.allcb').prop('checked', true);   
                        }else{
                            $('.allcb').prop('checked', false);
                        }
                    });
					
					$('#btn_listing').click(function(){
						$('#listing').submit();
					})
					$('#btn_listing2').click(function(){
						$('#listing').submit();
					})
});


</script>
<?php //$this->load->view('common/footer');?>
