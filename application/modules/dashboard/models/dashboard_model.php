<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_model extends CI_Model {
    /*
     * Properties
     */

    private $_table_users;
    private $_table_donate;
    private $_table_applied_donation;

//----------------------------------------------------------------------

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();

        //Load Table Names from Config
        $this->_table_users = $this->config->item('table_users');
        $this->_table_donate = $this->config->item('table_donate');
        $this->_table_user_profile = $this->config->item('table_user_profile');
        $this->_table_applied_donation = $this->config->item('table_applied_donation');
    }

    function getProftLossData($from, $to) {

        if ($from != "" && $to != "") {
            $subsq = " DATE(i.`invoice_date`) <= '" . $from . "' AND  DATE(i.`invoice_date`) >='" . $to . "'";
        } else {
            $subsq = " DATE(i.`invoice_date`) <= CURRENT_DATE()";
        }
        $sql = "SELECT 
					  SUM(i.`sales_amount`) AS total_sales,
					  (SELECT 
						SUM(jc.`purchase_value`) 
					  FROM
						`jobs_charges` AS jc 
					  WHERE DATE(jc.`created`) <= CURRENT_DATE()) AS total_purchase 
					FROM
					  `an_invoice` AS i 
					WHERE " . $subsq . "";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function getExpensesList() {
        $sql = "SELECT 
			  jc.purchase_value,
			  c.`charge_name` ,
			  jc.`job_id`
			FROM
			  `jobs_charges` AS jc 
			  INNER JOIN `an_charges` AS c
			  ON c.`charge_id` =  jc.`charges_id`
			WHERE DATE(jc.`created`) <= CURRENT_DATE() AND jc.`purchase_value`>0";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getCustomersSales() {
        $sql = "SELECT 
			  c.`customer_name`,
			  SUM(i.`sales_amount`) AS total_sales,
			  SUM(i.`purchase_amount`) AS total_purchase,
 
			  j.`id` ,
      			c.`id` AS customer_id
			FROM
			  `an_customers` AS c 
			  INNER JOIN `an_jobs` AS j 
				ON j.`customer_id` = c.`id` 
			  INNER JOIN `an_invoice` AS i 
				ON i.`job_id` = j.`id` 
			GROUP BY c.`id` ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getCustomerInvoices($invoice_id) {

        $sql = "SELECT 
			  c.`customer_name`,
			  SUM(i.`sales_amount`) AS total_sales,
			  j.`id`,
			  i.`invoice_id` 
			FROM
			  `an_customers` AS c 
			  INNER JOIN `an_jobs` AS j 
				ON j.`customer_id` = c.`id` 
			  INNER JOIN `an_invoice` AS i 
				ON i.`job_id` = j.`id` 
			WHERE c.`id` = '" . $invoice_id . "'";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getCurrentCustomersAdvance() {
        $sql = "  SELECT 
				  SUM(p.`payment_amount`),
				  SUM(ip.`payment_amount`),
				  (
					SUM(p.`payment_amount`) - SUM(ip.`payment_amount`)
				  ) AS remaining_advance 
				FROM
				  `payments` AS p 
				  LEFT JOIN `invoice_payments` AS ip 
					ON ip.`payment_id` = p.`id` 
				WHERE p.`payment_type` = 'receipt' 
				  AND (
					p.`refernce_type` = 'advance' 
					OR p.`refernce_type` = 'onaccount'
				  )";

        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function getPendingCustomersPayment() {
        /* $sql = "SELECT SUM(i.`invoice_total_amount`) AS total_amount ,SUM(ip.`payment_amount`), (SUM(i.`invoice_total_amount`) - SUM(ip.`payment_amount`)) AS pending_payments  FROM `an_invoice` AS i 
          LEFT JOIN `invoice_payments`  AS ip ON ip.`invoice_id` = i.`invoice_id`"; */
        $sql = "SELECT 
  SUM(i.`invoice_total_amount`) AS total_amount,
     SUM(jpam+pamount) AS payment_amount
FROM
  `an_invoice` AS i
  LEFT JOIN (
  SELECT 
      j.`customer_id`,
      SUM(ip.`payment_amount`) AS pamount,
      ip.`invoice_id`,
      (jp.`payment_amount`) AS jpam 
    FROM
      `invoice_payments` AS ip 
      INNER JOIN `an_jobs` AS j 
        ON j.`id` = ip.`job_id` 
      LEFT JOIN `job_payment` AS jp 
        ON jp.`job_id` = j.`id` 
        AND jp.`payment_purpose` = 'advance' 
      INNER JOIN `an_customers` AS c 
        ON c.`id` = j.`customer_id` 
    GROUP BY c.`id`
) AS dd
ON dd.invoice_id= i.`invoice_id`  AND i.`invoice_status` = '0' ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function getCustomersDebit() {
        $sql = "
				SELECT 
  c.userid AS customer_id,
  c.fullname,
  SUM(i.`invoice_total_amount`) AS total,
  totalpaid1,
  SUM(pamount) AS totalpaid 
FROM
  `an_invoice` AS i 
  INNER JOIN `bs_users` AS c 
    ON c.`userid` = i.`customer_id` 
  LEFT JOIN 
    (SELECT 
      i.`customer_id`,
      SUM(ip.`payment_amount`) AS totalpaid1,
      ip.`invoice_id` 
    FROM
      `invoice_payments` AS ip 
      INNER JOIN `an_invoice` AS i 
        ON i.`invoice_id` = ip.`invoice_id` 
      INNER JOIN `bs_users` AS c 
        ON c.`userid` = i.`customer_id` 
    GROUP BY c.`userid`) d 
    ON d.customer_id = i.`customer_id` 
  LEFT JOIN 
    (SELECT 
      i.`customer_id`,
      SUM(ip.`payment_amount`) AS pamount,
      ip.`invoice_id` 
    FROM
      `invoice_payments` AS ip 
      INNER JOIN `an_invoice` AS i 
        ON i.`invoice_id` = ip.`invoice_id` 
      INNER JOIN `payments` AS p 
        ON p.`id` = ip.`payment_id` 
        AND (
          p.`refernce_type` = 'advance' 
          OR p.`refernce_type` = 'onaccount' 
          OR p.`refernce_type` = 'opening'
        ) 
      INNER JOIN `bs_users` AS c 
        ON c.`userid` = i.`customer_id` 
    GROUP BY c.`userid`) dc 
    ON dc.customer_id = c.`userid` 
GROUP BY c.`userid`  ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getCustomersInvoices($id) {

		$sql = "SELECT 
  i.`invoice_id`,
  i.`sales_amount`,
  totalpayment,
  i.`invoice_date`
FROM
  `an_invoice` AS i
    LEFT JOIN 
    (SELECT 
      (ip.`payment_amount`)  AS totalpayment,
      i.`invoice_id`
    FROM
      `invoice_payments` AS ip
      INNER JOIN `an_invoice` AS i
      ON i.`invoice_id` = ip.`invoice_id`
      ) AS dd2 
    ON dd2.`invoice_id` = i.`invoice_id`
WHERE i.`customer_id` = " . $id . "";
        /*$sql = "SELECT 
  i.`invoice_id`,
  c.`id`,
  c.`customer_name`,
  SUM(i.`invoice_total_amount`) AS total,
   SUM(jpam+pamount) AS totalpaid,
  i.`invoice_date` 
FROM
  `an_invoice` AS i 
  INNER JOIN `an_jobs` AS j 
    ON j.`id` = i.`job_id` 
  INNER JOIN `an_customers` AS c 
    ON c.`id` = j.`customer_id` 
  LEFT JOIN 
    (SELECT 
      j.`customer_id`,
      SUM(ip.`payment_amount`) AS pamount,
      ip.`invoice_id`,
      (jp.`payment_amount`) AS jpam 
    FROM
      `invoice_payments` AS ip 
      INNER JOIN `an_jobs` AS j 
        ON j.`id` = ip.`job_id` 
      LEFT JOIN `job_payment` AS jp 
        ON jp.`job_id` = j.`id` 
        AND jp.`payment_purpose` = 'advance' 
      INNER JOIN `an_customers` AS c 
        ON c.`id` = j.`customer_id` 
    WHERE c.`id` = " . $id . " 
    GROUP BY ip.`invoice_id`) d 
    ON d.invoice_id = i.`invoice_id` 
WHERE j.customer_id = " . $id . " 
GROUP BY i.`invoice_id`  ";*/
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function LoadAllCashManagement() {
        /* $sql = "SELECT 
          ab.*,
          atc.*,
          atc2.*,
          b.*,
          ab.`created` AS account_created
          FROM
          `an_branch_cash` AS ab
          INNER JOIN `an_branch` AS b
          ON b.`branch_id` = ab.`branch_id`
          LEFT JOIN
          (SELECT
          act.`branch_id`,
          SUM(act.`value`) AS totaldebit
          FROM
          `an_cash_management` AS act
          WHERE act.`transaction_type` = 'debit'
          GROUP BY act.`branch_id`) AS atc
          ON atc.branch_id = ab.`branch_id`
          LEFT JOIN
          ( SELECT
          act2.`branch_id`,
          SUM(act2.`value`) AS totalcredit
          FROM
          `an_cash_management` act2
          WHERE act2.`transaction_type` = 'credit'
          GROUP BY act2.`branch_id`) AS atc2
          ON atc2.branch_id = ab.branch_id
          "; */
        $sql = "SELECT 
  c.*,
  atc.*,
  atc2.*,
  bc.`branch_cash_name`,
  bc.`id` AS branchId 
FROM
  `an_cash_management` AS c 
  INNER JOIN `an_branch_cash` AS bc 
    ON bc.`id` = c.`branch_cash_id` 
  LEFT JOIN 
    (SELECT 
      act.`branch_cash_id`,
      SUM(act.`value`) AS totaldebit 
    FROM
      `an_cash_management` AS act 
    WHERE act.`transaction_type` = 'debit' 
    GROUP BY act.`branch_cash_id`) AS atc 
    ON atc.`branch_cash_id` = bc.`id` 
  LEFT JOIN 
    (SELECT 
      act2.`branch_cash_id`,
      SUM(act2.`value`) AS totalcredit 
    FROM
      `an_cash_management` act2 
    WHERE act2.`transaction_type` = 'credit' 
    GROUP BY act2.`branch_cash_id`) AS atc2 
    ON atc2.`branch_cash_id` = bc.`id` 
GROUP BY bc.id ";
        $q = $this->db->query($sql);
        return $q->result();
    }

    //debit
    function getDebitCustomersAmont() {
        /* $sql = "


          SELECT
          totalcharges,
          totaljobpayment,
          totalpayment,
          c.`id`,
          c.`customer_name`
          FROM
          `an_customers` AS c
          LEFT JOIN
          (SELECT
          SUM(jp.`payment_amount`) AS totaljobpayment,
          jp.`customer_id`
          FROM
          `job_payment` AS jp
          GROUP BY jp.`customer_id`) AS jjp
          ON jjp.customer_id = c.`id`
          LEFT JOIN
          (SELECT
          SUM(p.`payment_amount`) AS totalpayment,
          p.`refrence_id`
          FROM
          `payments` AS p
          GROUP BY p.`refrence_id`) AS pp
          ON pp.refrence_id = c.`id`
          LEFT JOIN
          (SELECT
          SUM(jc.`sales_value`) AS totalcharges,
          j.customer_id,
          ij.`invoice_id`
          FROM
          `an_jobs` AS j
          INNER JOIN `jobs_charges` AS jc
          ON jc.`job_id` = j.`id`
          INNER JOIN `an_invoice` AS ij
          ON ij.`job_id` = j.`id`
          GROUP BY j.`customer_id`) AS cc
          ON cc.customer_id = c.`id`
          GROUP BY c.`id` "; */
        $sql = "
SELECT 
  totalcharges,
  totaljobpayment,
  totalpayment,
  c.`id`,
  c.`customer_name` ,
  SUM(jpam+pamount) AS amount
FROM
  `an_customers` AS c 
  LEFT JOIN 
    (SELECT 
      SUM(jp.`payment_amount`) AS totaljobpayment,
      jp.`customer_id` 
    FROM
      `job_payment` AS jp 
    GROUP BY jp.`customer_id`) AS jjp 
    ON jjp.customer_id = c.`id` 
  LEFT JOIN 
    (SELECT 
      SUM(p.`payment_amount`) AS totalpayment,
      p.`refrence_id` 
    FROM
      `payments` AS p 
    GROUP BY p.`refrence_id`) AS pp 
    ON pp.refrence_id = c.`id` 
  LEFT JOIN 
    (SELECT 
      SUM(jc.`sales_value`) AS totalcharges,
      j.customer_id,
      ij.`invoice_id` 
    FROM
      `an_jobs` AS j 
      INNER JOIN `jobs_charges` AS jc 
        ON jc.`job_id` = j.`id` 
      INNER JOIN `an_invoice` AS ij 
        ON ij.`job_id` = j.`id` 
    GROUP BY j.`customer_id`) AS cc 
    ON cc.customer_id = c.`id` 
    LEFT JOIN 
    (SELECT 
      j.`customer_id`,
      SUM(ip.`payment_amount`) AS pamount,
      ip.`invoice_id`,
      (jp.`payment_amount`) AS jpam 
    FROM
      `invoice_payments` AS ip 
      INNER JOIN `an_jobs` AS j 
        ON j.`id` = ip.`job_id` 
      LEFT JOIN `job_payment` AS jp 
        ON jp.`job_id` = j.`id` 
        AND jp.`payment_purpose` = 'advance' 
      INNER JOIN `an_customers` AS c 
        ON c.`id` = j.`customer_id` 
    GROUP BY c.`id`) dc 
    ON dc.customer_id = c.`id` 
GROUP BY c.`id`";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $return = $q->result();
        } else {
            return false;
        }
    }

    //debit
    function getCustomerPaymetns() {
        $sql = "
							SELECT 
							  (SELECT 
								SUM(jc.`sales_value`) 
							  FROM
								`an_jobs` AS j 
								INNER JOIN `jobs_charges` AS jc 
								  ON jc.`job_id` = j.`id` 
								INNER JOIN `an_jobs_invoice_relation` AS ij 
								  ON ij.`job_id` = j.`id` 
							  ) AS totalcharges,
							  (SELECT 
								SUM(jp.`payment_amount`) 
							  FROM
								`job_payment` AS jp 
							 ) AS totaljobpayment,
							  (SELECT 
								SUM(p.`payment_amount`) 
							  FROM
								`payments` AS p 
							   ) AS totalpayment 
							FROM
							  `an_customers` AS c  LIMIT 1  ";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $return = $q->row();
        } else {
            return false;
        }
    }

    function getCurrentAdvance() {
        $sql = "SELECT 
  SUM(p.`payment_amount`),
  SUM(ip.`payment_amount`),
  (
    SUM(p.`payment_amount`) - SUM(ip.`payment_amount`)
  ) AS remaining_advance 
FROM
  `payments` AS p 
  LEFT JOIN `purchase_payments` AS ip 
    ON ip.`payment_id` = p.`id` 
WHERE p.`payment_type` = 'purchase' 
  AND (
    p.`refernce_type` = 'advance' 
    OR p.`refernce_type` = 'onaccount'
  )";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function getCustomersDebitByInvoice($customer_id) {

        $sql = 'SELECT 
  totalcharges,
  totaljobpayment,
  totalpayment,
  c.`id`,
  c.`customer_name` ,
   inv_id
FROM
  `an_customers` AS c 
  LEFT JOIN 
    (SELECT 
      SUM(jp.`payment_amount`) AS totaljobpayment,
      i.`invoice_id`  AS inv_id,
      j.customer_id
    FROM
      `invoice_payments` AS jp 
      INNER JOIN `an_invoice` AS i 
        ON i.`job_id` = jp.`job_id` 
      INNER JOIN `an_jobs` AS j 
        ON j.id = i.job_id 
    WHERE j.`customer_id` = ' . $customer_id . ' 
    GROUP BY i.`invoice_id`) AS jjp 
    ON jjp.customer_id = c.`id` 
  LEFT JOIN 
    (SELECT 
     SUM(p.`payment_amount`) AS totalpayment,
      p.`refrence_id` ,
      p.`created` AS payment_date
    FROM
      `payments` AS p 
    WHERE p.`refrence_id` = ' . $customer_id . ' AND `payment_type` = "receipt") AS pp 
    ON pp.refrence_id = c.`id` 
  LEFT JOIN 
    (SELECT 
      SUM(jc.`sales_value`) AS totalcharges,
      j.customer_id,
      ij.`invoice_id` 
    FROM
      `an_jobs` AS j 
      INNER JOIN `jobs_charges` AS jc 
        ON jc.`job_id` = j.`id` 
      INNER JOIN `an_invoice` AS ij 
        ON ij.`job_id` = j.`id` 
    WHERE j.`customer_id` = ' . $customer_id . ' 
    GROUP BY ij.`invoice_id`) AS cc 
    ON cc.customer_id = c.`id` 
	WHERE c.`id` = ' . $customer_id . '';
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $return = $q->result();
        } else {
            return false;
        }
    }

    function getloanliblities() {
        $sql = "SELECT 
			  ll.*,
			  SUM(pl.`payment_amount`) AS total_payments,
			  llp.`party_name` 
			FROM
			  `loan_liabalities` AS ll 
			  INNER JOIN `loan_liabilities_parties` AS llp 
				ON llp.`party_id` = ll.`party_id` 
			  LEFT JOIN `payments_liablities` AS pl 
				ON pl.`liabilty_id` = ll.`libality_id` 
			GROUP BY ll.`libality_id` ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getSuppierInvoices($supplier_id) {
        $sql = "SELECT 
			  s.`supplier_name`,	
			  i.`invoice_id`,
			  SUM(jc.`purchase_value`) AS total_purchase ,
			  total_payment,
			  i.`invoice_date`
			FROM
			  `an_invoice` AS i
			  INNER JOIN `jobs_charges` AS jc 
				ON jc.`job_id` = i.`job_id`
				INNER JOIN `an_suppliers` AS s
				ON s.`id` = jc.`purchase_id`
				LEFT JOIN 
					(SELECT 
					  SUM(ip.`payment_amount`) AS total_payment,
					  ip.`invoice_id`
					FROM
					  `purchase_invoice_payments` AS ip 
					  INNER JOIN `payments` AS p 
						ON p.`id` = ip.`payment_id` GROUP BY ip.`invoice_id`) d 
					ON d.invoice_id = i.`invoice_id`
					WHERE s.`id` = '" . $supplier_id . "'
				GROUP BY i.`invoice_id`";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    //suppliers
    function getCreditSuppliers() {
        $sql = "SELECT 
  s.`suppliername`,
  s.`supplierid`,
  SUM(p.`purchase_total_amount`) AS total_purchase,
  totalpaid 
FROM
  `bs_suppliers` AS s
  LEFT JOIN `an_purchase` AS p
  ON p.`customer_id` = s.`supplierid`
  LEFT JOIN 
    (SELECT 
      SUM(ip.`payment_amount`) AS totalpaid,
      p.`user_id` 
    FROM
      `purchase_payments` AS ip 
      INNER JOIN `payments` AS p 
        ON p.`id` = ip.`payment_id` 
    GROUP BY p.`user_id`) d 
    ON d.`user_id` = s.`supplierid` 
GROUP BY s.`supplierid` ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    ///customenrs
    function getPendingPayment() {
        $sql = "SELECT 
  SUM(i.purchase_total_amount) AS total_amount ,
  totalpaid,
  (
    SUM(i.purchase_total_amount) - totalpaid
  ) AS pending_payments
FROM
  `an_purchase` AS i 
  LEFT JOIN 
    (SELECT 
      SUM(ip.`payment_amount`) AS totalpaid,
      ip.`purchase_id` 
    FROM
      `purchase_payments` AS ip 
      GROUP BY ip.`purchase_id`) d 
    ON d.purchase_id = i.`purchase_id`  WHERE i.`purchase_status` = '0'  ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function getBanks() {
        $sql = "SELECT 
					  c.`transaction_type`,
					  SUM(c.`value`) AS val 
					FROM
					  `an_company_transaction` AS c 
					GROUP BY c.`transaction_type`";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getCash($type) {
        $sql = "SELECT 
  a.`transaction_type`,
  SUM(a.`value`) AS val  FROM `an_cash_management` AS a
Where a.transaction_type = '" . $type . "'";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function getCashByBrabch($type) {
        $sql = "SELECT 
  a.`transaction_type`,
  SUM(a.`value`) AS val  FROM `an_cash_management` AS a
Where a.transaction_type = '" . $type . "'";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function getTotalCapital() {
        $sql = "SELECT 
				  SUM(c.`capital_amount`)  AS total_capital
				FROM
				  `capitals` AS c ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function totalAdvance() {
        $sql = "SELECT SUM(la.`loan_amount`) AS total_advance FROM `loan_advance` AS la";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function getloanAdvanceDetails() {
        $sql = "SELECT 		
		  la.`loan_amount` AS total_advance ,
		  la.`loan_reazon`,
		  u.`username`,
		  SUM(lac.`payment_amount`) AS paid  
		FROM
		  `loan_advance` AS la 
		  INNER JOIN `bs_users` AS u 
			ON u.`userid` = la.`user_id` 
			INNER JOIN `loan_advance_received` AS lac
			ON lac.`loan_advance_id` = la.`loan_advance_id`
			GROUP BY lac.`loan_advance_id`";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getLoanLibalities() {
        $sql = "SELECT SUM(ll.`libality_amount`) AS liablity_amount FROM `loan_liabalities` AS ll";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function getFixedAssets() {
        $sql = "SELECT * FROM `assets`";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getAllFixedAssets() {
        $sql = "SELECT SUM(a.`total_value`) AS total_asset_val FROM `assets` AS a";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    //expecess
    function getIndirectData($from, $to) {
        if ($from != "" && $to != "") {
            $subsq = " DATE(e.`expense_date`) <= '" . $from . "' AND  DATE(e.`expense_date`) >='" . $to . "'";
        } else {
            $subsq = " DATE(e.`expense_date`) <= CURRENT_DATE()";
        }

        $sql = "  SELECT 
							ec.`expense_title`,
							SUM(e.`value`) AS total_expense 
						  FROM
							`an_expenses` AS e 
							INNER JOIN `an_expenses_charges` AS ec 
							  ON ec.`id` = e.`expense_charges_id` 
						  WHERE " . $subsq . " AND ec.`expense_type` ='fixed'
						  GROUP BY ec.`id`";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function getAccuredExpense() {

        $sql = "SELECT 
					ec.`expense_title`,
					SUM(ec.`value`) AS accured_val 
				  FROM
					`an_expenses_charges` AS ec 
					INNER JOIN `an_expenses` AS e 
					  ON ec.`id` = e.`expense_charges_id` 
				  WHERE ec.`expense_type` = 'accured' 
					AND DATE(e.`expense_date`) <= CURRENT_DATE()";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function block_admin() {

        $sql = "
               SELECT 
                si.*,
                i.*,
                ss.`store_id`,
                ss.`item_id`,
                u1.userid,
                u1.fullname,
                ss.soled_quantity,
                SUM(si.`quantity`) squantity,
                ssquantity,
                s.`storeid`,
                s.`storename` 
              FROM
                `bs_store_items` AS si 
                LEFT JOIN `bs_item` AS i 
                  ON i.`itemid` = si.`item_id` 
                LEFT JOIN `bs_store` AS s 
                  ON `s`.`storeid` = `si`.`store_id` 
                LEFT JOIN `bs_users` AS u1 
                  ON `u1`.`userid` = `si`.`supplier_id` 
                LEFT JOIN 
                  (SELECT 
                    soldi.*,
                    SUM(soldi.`soled_quantity`) AS ssquantity 
                  FROM
                    `store_soled_items` AS soldi 
                  GROUP BY soldi.`item_id`) ss 
                  ON  ss.`item_id` = si.`item_id` 
                GROUP BY si.`item_id`  limit 10
                ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    function block_admin3() {

        $sql = "
               SELECT 
                qi.*,
                u1.`userid`,u1.`fullname` AS cfullname,
                u2.`userid`,u2.`fullname` AS ufullname

              FROM
                `an_quotation` AS qi 


                LEFT JOIN `bs_users` AS u1 
                  ON `u1`.`userid` = `qi`.`customer_id` 

                LEFT JOIN `bs_users` AS u2 
                  ON `u2`.`userid` = `qi`.`userid` 



              GROUP BY qi.`quotation_id`  
                ORDER BY qi.quotation_id DESC limit 10 
                ";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    
    function block_admin2_1() {

       $sql = "
            SELECT 
            i.*,
            c.userid,
            c.fullname,
            pamount,
            i.purchase_total_amount-pamount AS remaining
          FROM
            `an_purchase` AS i 
            
            LEFT JOIN `bs_users` AS c 
              ON c.`userid` = i.`supplier_id` 
              
            LEFT JOIN 
              (SELECT 
                SUM(ip.`payment_amount`) AS pamount,
                ip.`purchase_id` 
              FROM
                `purchase_payments` AS ip 
                INNER JOIN `an_purchase` AS ii 
                  ON ii.`purchase_id` = ip.`purchase_id` 
                INNER JOIN `bs_users` AS c 
                  ON c.`userid` = ii.`customer_id` 
               
              GROUP BY ip.`purchase_id`) d 
              ON d.purchase_id = i.`purchase_id` 
          
          
          
           
          GROUP BY i.`purchase_id`  
            ORDER BY i.purchase_id DESC limit 5";
       
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    
    function block_admin2_2() {

       $sql = "
            SELECT 
            i.*,
            c.userid,
            c.fullname,
            pamount,
            i.invoice_total_amount-pamount AS remaining
          FROM
            `an_invoice` AS i 
            
            LEFT JOIN `bs_users` AS c 
              ON c.`userid` = i.`customer_id` 
              
            LEFT JOIN 
              (SELECT 
                SUM(ip.`payment_amount`) AS pamount,
                ip.`invoice_id` 
              FROM
                `invoice_payments` AS ip 
                INNER JOIN `an_invoice` AS ii 
                  ON ii.`invoice_id` = ip.`invoice_id` 
                INNER JOIN `bs_users` AS c 
                  ON c.`userid` = ii.`customer_id` 
               
              GROUP BY ip.`invoice_id`) d 
              ON d.invoice_id = i.`invoice_id` 
          
          
          
           
          GROUP BY i.`invoice_id`  
            ORDER BY i.invoice_id DESC limit 5";
       
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    
    function block_admin7() {

       $sql = "
            SELECT 
            main.*,
            i.itemid,i.itemname
          FROM
            `bs_maintenance` AS main 
            
            LEFT JOIN `bs_item` AS i 
              ON i.`itemid` = main.`m_itemid` 
              
           
          
          
          
           
          GROUP BY main.`m_invoice`  
            ORDER BY main.m_fdate DESC limit 10";
       
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

//----------------------------------------------------------------------	
}

?>