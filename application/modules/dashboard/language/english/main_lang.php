<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['main']= 'Dashboard';
$lang['Customer-Name']= 'Customer Name';
$lang['Number-of-Invoices']= 'Number of Invoices';
$lang['Mobile-Number']= 'Mobile Number';
$lang['Type']= 'Type';
$lang['selectbranch']= ' Select Branch';
$lang['Code']= 'Code';
$lang['All']= 'All';
$lang['Debit']= 'Debit';
$lang['Credit']= 'Credit';
$lang['Total']= 'Total';
$lang['Status']= 'Status';
$lang['Notes']= 'Notes';
$lang['Responsable-Phone']= 'Responsable Phone';
$lang['Responsable-Name']= 'Responsable Name';
$lang['Address']= 'Address';
$lang['Email-Address']= 'Email-Address';
$lang['Fax-Number']= 'Fax-Number';
$lang['Contact-Number']= 'Contact-Number';

$lang['All']= 'All';
$lang['Category-Name']= 'Category Name';
$lang['Company-Name']= 'Company Name';
$lang['Category-Type']= 'Category Type';
$lang['Description']= 'Description';
$lang['Status']= 'Status';
$lang['Action']= 'Action';


$lang['Item-Picture']= 'Item Picture';
$lang['Name']= 'Name';
$lang['Supplier-Name']= 'Supplier Name';
$lang['Serial-Number']= 'Serial Number';
$lang['Store']= 'Store';
$lang['Purchase-Price']= 'Purchase Price';
$lang['Sale-Price']= 'Sale Price';
$lang['Min-Sale-Price']= 'Min Sale Price';
$lang['Point-of-Order']= 'Point of Order';
$lang['Quantity']= 'Quantity';
$lang['Date']= 'Date';
$lang['Location-Address']= 'Location &amp; Address';
$lang['Finacial-Type']= 'Finacial Type';
$lang['Store-Manager']= 'Store Manager';
$lang['Manager-Phone']= 'Manager-Phone';
$lang['Total-Qunatity-Available']= 'Total Qunatity Available';

$lang['Rent-Value']= 'Rent Value';
$lang['Per']= 'Per';
$lang['Date-Of-Paid-Rent']= 'Date Of Paid Rent';
$lang['Owner-Phone']= 'Owner Phone';
$lang['Store-Phone-Number']= 'Store Phone Number';
$lang['Fax']= 'Fax';

$lang['Capcity']= 'Capacity';
$lang['Note']= 'Note';
$lang['Date-Of-Paid']= 'Date Of Paid';
$lang['Instock']= 'Instock';
$lang['Point-of-Sale']= 'Point of Sale';
$lang['Net-Cost']= 'Net-Cost';
$lang['Net-Profit']= 'Net-Profit';


$lang['users']= 'Sales Person ';
$lang['sales']= 'Sales ';
$lang['purchase']= 'Purchase ';
$lang['DAY']= 'DAY ';
$lang['MONTH']= 'MONTH ';
$lang['YEAR']= 'YEAR ';
$lang['noti_alert']= 'You have 1 new notifications ';
$lang['noti_alert1']= 'New user registration ';
$lang['Your_account']= 'Your account';
$lang['Personal_info']= 'Personal info';
/*Form*/

$lang['add-edit']= 'Add & Edit Customer';
$lang['mess1']= 'Please Understand Clearly All The Data Before Entering';
$lang['Add']= 'Add';
$lang['Reset']= 'Reset';
$lang['Branch-Name']= 'Branch Name';
$lang['RePassword']= 'RePassword';
$lang['Password']= 'Password';
$lang['User-Name']= 'User Name';
$lang['Add-Account']= 'Add Account';
$lang['Company-Name']= 'Company-Name';



$lang['Assets']= 'Assets';
$lang['Fixed-Assets']= 'Fixed Assets';
$lang['Current-value']= 'Current value';
$lang['Depressionation']= 'Depressionation';
$lang['Current-Assets']= 'Current Assets';
$lang['Loan-Advance']= 'Loan Advance';
$lang['Cash']= 'Cash';
$lang['Total-Assests']= 'Total Assests & Current Assets value';
$lang['Liablities']= 'Liablities';
$lang['Capital-Account']= 'Capital Account';
$lang['Loan-Liablities']= 'Loan Liablities';
$lang['Current-Liablities']= 'Current Liablities';
$lang['Accured-Expense']= 'Accured Expense';
$lang['Profit-Loss']= 'Profit Loss';
$lang['Total-Liablities-Values']= 'Total Liablities Values';

/**/

$lang['show_datatable']= 'Showing _START_ to _END_ of _TOTAL_ entries';
$lang['Previous']= 'Previous';
$lang['First']= 'First';
$lang['Last']= 'Last';
$lang['Next']= 'Next';
$lang['Search']= 'Search';
$lang['show_bylist']= 'Show _MENU_ entries';
