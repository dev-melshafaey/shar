<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Assets_model extends CI_Model {
    /*
     * Properties
     */

//----------------------------------------------------------------------

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();

        //Load Table Names from Config
    }

    function add_asssets() {
        
    }

    function getLists() {
        $sql = "SELECT * from assets";
        $q = $this->db->query($sql);
        $res = $q->result();
        return $res;
    }

    function insertNewcash($data = '', $emp_id = '') {
        if ($data['branch_id']) {
            $data['branch_cash_id'] = $this->getBranchCashId($data['branch_id']);
        }
        if ($emp_id == '') {
            $data['emp_id'] = $this->session->userdata('userid');
        }
        $this->db->insert('an_cash_management', $data);
        return $this->db->insert_id();
    }

//----------------------------------------------------------------------	
}

?>