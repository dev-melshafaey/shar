<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Assets extends CI_Controller {
    /*
     * Properties
     */

    private $_data = array();

//----------------------------------------------------------------------
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        //load Home Model
        $this->load->model('assets_model', 'assets');
        $this->load->model('customers/customers_model', 'customers');
        $this->lang->load('main', get_set_value('site_lang'));
        $this->_data['udata'] = userinfo_permission();
    }

//----------------------------------------------------------------------

    /*
     * Home Page
     */
    public function index() {
        // Load Home View
        $this->_data['inside'] = 'assets';
        $this->load->view('common/main', $this->_data);
    }

    function add_assets() {

        if ($this->input->post()) {
            //echo "<pre>";
            $assets_data = $this->input->post();
            $post = $assets_data;
            $this->load->model('users/users_model', 'users');
            $userid = $this->session->userdata('userid');
            $user = $this->users->get_user_detail($userid);

            unset($assets_data['payment_type']);
            unset($assets_data['payment_method']);
            unset($assets_data['bank_id']);
            unset($assets_data['cheque_date']);
            unset($assets_data['cheque_number']);
            unset($assets_data['deposite_recipt']);
            unset($assets_data['account_id']);
            unset($assets_data['notes']);
            unset($assets_data['sub_mit']);
            $this->db->insert('assets', $assets_data);
            //			
            if ($post['payment_method'] == 'bank') {

                //echo "ifff";
                $transaction_data['branch_id'] = $user->branchid;
                $transaction_data['transaction_by'] = 'assets';
                $transaction_data['transaction_type'] = 'credit';
                $transaction_data['value'] = $post['total_value'];
                $transaction_data['amount_type'] = 'sales';
                $transaction_data['amount_type'] = $post['payment_type'];
                $transaction_data['account_id'] = $post['account_id'];
                $transaction_data['notes'] = $post['notes'];
                $transaction_data['clearance_date'] = date('Y-m-d');
                $transaction_id = $this->customers->insert($transaction_data, 'an_company_transaction');
            } else {

                //echo "else";
                $cash_data['branch_id'] = $user->branchid;

                $cash_data['transaction_type'] = 'credit';
                $cash_data['value'] = $post['total_value'];
                $cash_data['created'] = date('Y-m-d');
                $transaction_id = $this->assets->insertNewcash($cash_data);
            }
        }

        //$this->load->view('add-assets', $this->_data);
        $this->_data['inside'] = 'add-assets';
        $this->load->view('common/main', $this->_data);
    }

//----------------------------------------------------------------------

    /*
     * Add Customer
     */
    public function add() {
        // Load Home View
        
        $this->load->view('add-quotation-form', $this->_data);
    }

//----------------------------------------------------------------------
}

?>