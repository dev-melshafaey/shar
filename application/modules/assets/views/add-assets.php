
<div id="main-content" class="main_content">
<!--    <div class="asset head">
        <div class="">
            <h4>

                <div class="title"> <span><?php breadcramb(); ?></span>  Assets</div>


            </h4>
           
        </div>
    </div>-->
    <!--<div class="title">Add Fixed Assets</div>-->
 <?php error_hander($this->input->get('e')); ?>
    <form action=""  method="post">
        <div class="form">
            <div class="g4">
                <div class="form_title">Assets</div>
                <div class="form_field">
                    <input name="assets_title" id="assets_title" type="text"  class="formtxtfield"/>
                </div>
            </div>
            <div class="g4">
                <div class="form_title">Total Value</div>
                <div class="form_field">
                    <input name="total_value" id="total_value" type="text"  class="formtxtfield_small"/>
                    
                </div>
            </div>
            <div class="g4">
                <div class="form_title">Depression Start</div>
                <div class="form_field">
                    <input name="start_depression_date" id="start_depression_date" type="text"  class="formtxtfield_small"/>
                </div>
            </div>
            <div class="g4">
                <div class="form_title">Depression End</div>
                <div class="form_field">
                    <input name="end_depression_date" id="end_depression_date" type="text"  class="formtxtfield_small"/>
                </div>
            </div>
            <br clear="all"/>
            <div class="g4">
                <div class="form_title">Depression %</div>
                <div class="form_field">
                    <input name="depression_percentage" id="depression_percentage" type="text"  class="formtxtfield_small"/>
                </div>
            </div>
            <div class="g3 form-group">
                <div class="form_title">Payment Method:</div>
                <div class="form_field">
                    <div class="defaultP">
                        <input type="radio" name="payment_method" class="payment_by" value="bank" id="payment_type1"> Bank
                        <input type="radio" name="payment_method" class="payment_by" value="cash" id="payment_type2">Cash</div>
                </div>
            </div>
            
            <div class="g3 form-group" id="div_checkque" style="display:none;">
                <div class="form_title">Deposite Recipt </div>
                <div class="form_field">
                    <div class="dropmenu">
                        <div class="styled-select">
                            <input type="file"  class="formtxtfield" name="deposite_recipt" id="deposite_recipt" />
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="g3 form-group" id="payment_tp" style="display:none;">
                <div class="form_title">Payment Type</div>
                <div class="form_field">
                    <div class="dropmenu">
                        <div class="styled-select">
                            <select name="payment_type" id="payment_type" onchange="BankOrCashToggle();">
                                <option value="0">Select Payment Way</option>
                                <option value="cheque">Cheque</option>
                                <option value="withdg4">Withdrow</option>
                            </select>
                             <span class="arrow arrowselectbox">&amp;</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="g3 form-group" id="div_banks" style="display:none;">
                <div class="form_title">Banks </div>
                <div class="form_field">
                    <div class="dropmenu">
                        <div class="styled-select">
                            <?php company_bank_dropbox('bank_id', '', 'english', ' onchange="getBankAccounts();" '); ?>
                             <span class="arrow arrowselectbox">&amp;</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="g3 form-group" style="display:none" id="div_accounts">
                <div class="form_title">Accounts </div>
                <div class="form_field">
                    <div class="dropmenu">
                        <div class="styled-select" id="div_accounts_responce">

                        </div>
                    </div>
                </div>
            </div>
            <br clear="all"/>
            <div class="g4 form-group" style="display:none" id="div_tr_accounts">
                <div class="form_title">Accounts </div>
                <div class="form_field">
                    <div class="dropmenu">
                        <div class="styled-select" id="div_tr_accounts_responce">

                        </div>
                    </div>
                </div>
            </div>
            <div class="g4 form-group" style="display:none" id="cheque_div_date">
                <div class="form_title">Cheque Date </div>
                <div class="form_field">
                    <input name="cheque_date" id="cheque_date" type="text"  class="formtxtfield"/>

                </div>
            </div>
            <div class="g4 form-group" style="display:none" id="cheque_div_number">
                <div class="form_title">Cheque Number </div>
                <div class="form_field">
                    <input name="cheque_number" id="cheque_nummber" type="text"  class="formtxtfield"/>

                </div>
            </div>
            <br clear="all"/>
            <div class="g11">
                <div class="form_title">Notes</div>
                <div class="form_field">
                    <textarea name="notes" cols="" assets="" class="formareafield"></textarea>

                </div>
            </div>
            <br clear="all"/>
            <div class="" align="">
                <input name="sub_mit" id="sub_mit" type="submit" class="btn-flat primary green flt-r g3" value="<?php echo lang('Add') ?>" />
              <input name="sub_reset" type="reset" class="btn-flat primary gary flt-r g3" value="<?php echo lang('Reset') ?>" />
            </div>
            <!--end of g4-->
        </div>
    </form>
    <!-- END PAGE -->  
</div>
<!-- END PAGE -->  

<script type="text/javascript">
    $(function () {
        $('#start_depression_date').datepicker({dateFormat: 'yy-mm-dd'});
        $('#end_depression_date').datepicker({dateFormat: 'yy-mm-dd'});
    });
    $(document).ready(function () {
        //alert('ready');
        var ac_config = {
            source: "<?php echo base_url(); ?>outcome/getAutoSearch",
            select: function (event, ui) {
                $("#expense_title").val(ui.item.cus);
                $("#expense_id").val(ui.item.cus);
                console.log(ui);
                //swapme();
                setTimeout('swapgeneral()', 500);
            },
            minLength: 1
        };

        $("#expense_title").autocomplete(ac_config);

        $("#is_fixed").click(function () {
            //alert('asdasd');
            checked_status = $(this).is(':checked');
            //alert(checked_status);
            if (checked_status) {
                $("#period_parent").show();
            }
            else {
                $("#period_parent").hide();
            }
        });

        $(".payment_by").click(function () {
            pval = $(this).val();
            if (pval == 'bank') {
                $("#payment_tp").show();
            }
            else {
                $("#payment_tp").hide();
            }

        })

        $("#payment_type").change(function () {
            val = $(this).val();
            //alert(val);
            if (val == 'accounts') {
                $("#account").show();
            }
            else {
                $("#account").hide();
            }
        })

        $(".payment_by").click(function () {
            transaction_val = $(this).val();
            if (transaction_val == 'bank') {
                $("#div_banks").show();
                $("#payment_tp").show();
            }
            else {
                $("#div_banks").hide();
                $("#payment_tp").hide();

            }


        });
        $('#cheque_date').datepicker({dateFormat: 'yy-mm-dd'});
    });
</script>
<!-- End Section-->
<!--footer-->

