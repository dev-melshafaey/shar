<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['main']= 'صفحة العملاء';
$lang['Customer-Name']= 'العميل';
$lang['Number-of-Invoices']= 'الفواتير';
$lang['Mobile-Number']= 'رقم الهاتف';
$lang['Type']= 'النوع';
$lang['Code']= 'الكود';

$lang['debit'] = 'الديون';

$lang['Total']= 'الاجمالي المعاملات';


$lang['Status']= 'الحالة';
$lang['Notes']= 'ملاحظات';
$lang['Responsable-Phone']= 'جوال المسؤل';
$lang['Responsable-Name']= 'اسم المسؤل';
$lang['Address']= 'العنوان';
$lang['Email-Address']= 'البريد الالكتروني';
$lang['Fax-Number']= 'الفاكس';
$lang['Contact-Number']= 'رقم اتصال اخر';
$lang['due_charges']= 'الرسوم المستحقه ';
$lang['clear_due_charges']= 'الرسوم المستحقه ';
$lang['from']= 'من';
$lang['paytoinvoice']='دفع الفاتورة';

$lang['Date']= 'التاريخ';
$lang['Status']= 'الحالة';
$lang['Payments']= 'الدفع';
$lang['Incomplete']= 'غير مكتمل';
$lang['Complete']= 'تم';
$lang['Extra']= 'اضافي';
$lang['Cash']= 'كاش';
$lang['Bank']= 'البنك';
$lang['choose']= 'اختر';
/*Form*/

$lang['add-edit']= 'اضافة جديد & تحديث حالي'.' عميل';
$lang['mess1']= 'يرجى فهم بوضوح جميع البيانات قبل الادخال';
$lang['Add']= 'اضافة';
$lang['Reset']= 'اعاده تعيين';
$lang['Branch-Name']= 'الفرع';
$lang['RePassword']= 'تاكيد كلمة المرور';
$lang['Password']= 'كلمة المرور';
$lang['User-Name']= 'اسم المستخدم';
$lang['Add-Account']= 'اضافة عضويه';
$lang['Company-Name']= 'الشركة';



$lang['Opening-Balance']= 'رصيد فتح حساب';
$lang['Credit-Amount-Limit']= 'الحد الافل لفتح حساب';
$lang['Credit-Days-Limit']= 'الحد الاقل لايام الايداع';
$lang['Account-Type']= 'نوع الحساب';
$lang['buywithtotal']= 'البيع بالجمله';

$lang['Value']= 'المبلغ';
$lang['Total']= 'اجمالي التعاملات';
$lang['remaining']= 'المتبقي';
$lang['Payment-Method']= 'طريقة الدفع';
$lang['Payment-Type']= 'نوع الدفع';
$lang['on-Account']= 'الى الحساب';
$lang['Advance']= 'من فاتوره';
$lang['Advance']= 'من فاتوره';
$lang['Against-Reference']= 'لاجمالي الفواتير';
$lang['Accounts']= 'الحساب';
$lang['Cheque-Date']= 'تاريخ استحقاق الشيك';
$lang['Cheque-Number']= 'رقم الشيك';
$lang['Deposite-Recipt']= 'ايصال الايداع';




/**/

$lang['Invoice-Number']= ' الفاتوره';
$lang['offer-Number']= 'عرض ';
$lang['Total-Price']= 'الاجمالي';
$lang['Net-Price']= ' الصافي';
$lang['discountamount']= 'الخصم';
$lang['Customer']= 'العميل';
$lang['typepay']= 'نوع الدفع';
$lang['amountpay']= ' المدفوعات';
$lang['remain']= 'الباقي';
$lang['NetPrice']= ' الصافي';
$lang['saleprice']= 'قيمة الشراء';
$lang['expences']= 'المصروفات';
$lang['Statusinvoice']= 'حالة الفاتوره';
$lang['salesperson']= 'المسوق';
$lang['salesperson_percent']= 'نسبة.م';

$lang['Debit']= 'واجب الدفع';
$lang['Credit']= 'الديون';
$lang['time']= 'فتره السداد';
$lang['remaining'] = 'المتبقيات';
$lang['balance'] = 'الرصيد';
$lang['totalremaining'] = 'إجمالي المتبقيات';
$lang['totalbalance'] = 'إجمالي الرصيد ';
$lang['invoicedate'] = 'تاريخ التعامل';

$lang['totalDebit']= 'إجمالي واجب الدفع ';
$lang['totalCredit']= 'إجمالي الرصيد';
$lang['blacklist']= 'القائمة السوداء';
$lang['reson-blacklist']= 'السبب للوضع في القائمة السوداء';



$lang['assets_title']= 'العنوان';
$lang['start_depression_date']= 'تاريخ بداية الاستهلاك';
$lang['end_depression_date']= 'تاريخ نهاية الاستهلاك';
$lang['depression_percentage']= ' نسبة الاستهلاك';

/**/


$lang['show_datatable']= ' عرض  من _START_   الي   _END_  سجلات '.' الاجمالي _TOTAL_ ';
$lang['Previous']= 'السابق';
$lang['First']= 'الاول';
$lang['Last']= 'الاخير';
$lang['Next']= 'التالي';
$lang['Search']= 'البحث';
$lang['show_bylist']= 'عرض_MENU_ سجلات';
