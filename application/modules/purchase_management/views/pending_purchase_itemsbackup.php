<script type="text/javascript">
function check_status(obj,id){
	is_val = $(obj).val();
	if(is_val == '1'){
		$("#rej_res"+id).hide();
	}
	else{
		$("#rej_res"+id).show();
	}
}
</script>
<div class="row form-wrapper">
    <div class="col-md-12 col-xs-12">



        <div id="main-content" class="main_content">
 
         <div class="row">
            <div class="col-md-12">
                <table id="new_data_table">
                    <thead>
                        <tr>
                            <th width="1%" id="no_filter"><label for="checkbox"></label></th>
                            <th width="20%"><?php echo lang('item_name')?></th>
                            <th width="20%"><?php echo lang('item_quantity')?></th>
                            <th width="5%"><?php echo lang('Invoice-Date')?></th>
                            <th width="7%" id="no_filter">&nbsp;</th>
                        </tr>
                    </thead>

                    <?php
                    $cnt = 0;
                    $total_amount = 0;
                    $total_paid = 0;
					$userid = $this->session->userdata('bs_userid');
					$pendingPurchase = $this->purchase_management->getPendingPurchase($userid);	
                    if($pendingPurchase){
                    foreach ($pendingPurchase as $purchasedata) {
                       // echo "<pre>";
                       // print_r($purchasedata);
                        $cnt++;
                        ?>
                        <tr>
                            <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $purchasedata->id; ?>" value="<?php echo $purchasedata->id; ?>" /></td>
                            <td ><a href="<?php echo base_url(); ?>purchase_management/purchase_invoices/<?php echo $purchasedata->supplier_id; ?>"><?php echo $purchasedata->suppliername; ?></a></td>
                            <td><?php echo $purchasedata->quantity; ?></td>
                            <td><?php echo $purchasedata->invoicedate; ?></td>
                            <td><?php //edit_button('addnewcustomer/'.$userdata->userid);  ?>
                  				 <input type="radio" name="receipt[<?php echo $i; ?>]" value="1" id="accept<?php echo $purchasedata->piv_id;  ?>"   onclick="check_status(this,'<?php echo $purchasedata->piv_id;  ?>')"/><span style="padding-left: 5px;"><?php echo lang('complete') ?></span>
                                <input type="radio" name="receipt[<?php echo $i; ?>]" value="0" id="reject<?php echo $purchasedata->piv_id;  ?>"  onclick="check_status(this,'<?php echo $purchasedata->piv_id;  ?>')"/><span style="padding-left: 5px;"><?php echo lang('incomplete') ?></span>
                           		<input type="hidden" name="store_id[<?php echo $i; ?>]" id="store_id" value="<?php echo $purchasedata->store; ?>" />
                        		
                                <!-- modal content -->

                                </td>
                                
                        </tr>
<?php } ?>
<?php } ?>
                </table>
            </div>
        </div> 
        </div>
        <!-- END PAGE -->  
    </div>
</div>
