<script type="text/javascript">
function check_status(obj,id){
	is_val = $(obj).val();
	if(is_val == '1'){
		$("#rej_res"+id).hide();
	}
	else{
		$("#rej_res"+id).show();
	}
}
</script>
<div class="row form-wrapper">
    <div class="col-md-12 col-xs-12">



        <div id="main-content" class="main_content">
 
            <?php
			//	echo "<pre>";
			//	print_r($purchase_invocie);
			?>
            <?php error_hander($this->input->get('e')); ?>
            <form action="<?php echo base_url(); ?>inventory/add_confirmation_items" method="post" id="form1" class="" name="frm_customer" autocomplete="off">
                <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
                <input type="hidden" name="userid" id="userid" value="<?php echo $user->userid; ?>" />
                <input type="hidden" name="invoiceid" id="invoiceid" value="<?php if(!empty($purchase_invocie)){ echo $purchase_invocie[0]->invoice_id; } ?>" />
                <div class="form">
                		<?php
							if(!empty($purchase_invocie)){
							
						//	echo "<pre>";
						//	print_r($purchase_invocie);
			
								foreach($purchase_invocie as $i=>$purchase){
								?>
                                <input type="hidden" name="itemid[]" id="itemid" value="<?php echo $purchase->itemid; ?>" />
                <input type="hidden" name="pivid[]" id="pivid" value="<?php echo $purchase->piv_id; ?>" />
                <input type="hidden" name="quantity[]" id="quantity" value="<?php echo $purchase->quantity; ?>" />
                
								<div class="field-box"> 
                        <label class="col-lg-3"><?php echo $purchase->itemname; ?> Quantity(<?php echo $purchase->quantity; ?>)</label>
                        <div class="col-lg-4">
                        
                                <input type="radio" name="receipt[<?php echo $i; ?>]" value="1" id="accept<?php echo $purchase->piv_id;  ?>"   onclick="check_status(this,'<?php echo $purchase->piv_id;  ?>')"/><span style="padding-left: 5px;"><?php echo lang('complete') ?></span>
                                <input type="radio" name="receipt[<?php echo $i; ?>]" value="0" id="reject<?php echo $purchase->piv_id;  ?>"  onclick="check_status(this,'<?php echo $purchase->piv_id;  ?>')"/><span style="padding-left: 5px;"><?php echo lang('incomplete') ?></span>
                           		<input type="hidden" name="store_id[<?php echo $i; ?>]" id="store_id" value="<?php echo $purchase->store; ?>" />
                        </div>

                    </div>
                    <div class="field-box" style="display:none;" id="rej_res<?php echo $purchase->piv_id;  ?>"> 
                        <label class="col-lg-2"><?php echo lang('available'); ?></label>
                        <div class="col-lg-2">
                        <input type="text" id="quantiy_avail<?php echo $purchase->piv_id;  ?>" name="quantiy_avail[<?php echo $i; ?>]" />
                        </div>
                        <label class="col-lg-2" style="text-align:center;"><?php echo lang('reason'); ?></label>
                        <div class="col-lg-2">
                        <textarea id="reason<?php echo $purchase->piv_id;  ?>" name="reason<?php echo $purchase->piv_id;  ?>"></textarea>
                        </div>

                    </div>
                    	
                                    	
								<?php
								} 
							}
						?>
					

					

                    <div class="field-box"> 
                        <input type="hidden" name="member_type" id="member_type" value="6">
                        <input name="sub_mit" id="sub_mit" type="submit" class="btn-glow primary" value="<?php echo lang('Add') ?>" />
                        <input name="sub_reset" type="reset" class="btn-glow primary" value="<?php echo lang('Reset') ?>" />
                    </div>
                    <!--end of raw--> 
                </div>
            </form>
        </div>
        <!-- END PAGE -->  
    </div>
</div>
