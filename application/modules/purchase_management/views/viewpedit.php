<!--<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" type="text/css"/>-->
<!--<div class=" head">
    <div class="">
        <h4>
            <div class="title"> <span><?php breadcramb(); ?> >> عرض تفاصيل فاتوره</span> </div>
        </h4>
<?php error_hander($this->input->get('e')); ?>
    </div>
</div>-->
<div id="wizard" class="box nav-box wizard g16">
    <ul class="nav">
        <li data-nav="#Customer" class="sel"><?php echo lang('supplier-Name') ?> </li>
        <li data-nav="#tproduct"><?php echo lang('tproduct') ?><span class="arrow">(</span></li>

    </ul>
    <div id="wizard-body" class="nav-cont vt">
        <div id="Customer" class="nav-item show pad">
            <div class=" form-group" id="customer">
                <?php if (get_member_type() == '1' OR get_member_type() == '5'): ?> 
                    <div class="g4 form-group">
                        <label class="text-warning"><?php echo lang('fullname') ?></label>
                        <div><?php echo $invoice[0]->fullname ?></div>
                    </div>
                    <div class="g4 form-group">
                        <label class="text-warning"><?php echo lang('Email-Address') ?></label>
                        <div><?php echo $invoice[0]->email_address ?></div>
                    </div>
                    <div class="g4 form-group">
                        <label class="text-warning"><?php echo lang('Phone') ?></label>
                        <div><?php echo $invoice[0]->phone_number ?></div>
                    </div>
                    <br clear="all"/>
                    <br clear="all"/>
                    <div class="g4 form-group">
                        <label class="text-warning"><?php echo lang('Company-Name') ?> :</label>
                        <br>
                        <div  class="ui-select" style="width:100%">
                            <div class="styled-select " style="width:100%">
                                <?php company_dropbox('companyid', $invoice[0]->companyid, $user->companyid); ?>
                                <span class="arrow arrowselectbox">&amp;</span>
                            </div>
                        </div>
                    </div>
                    <div class="g4 form-group">
                        <div class="field-box">
                            <label class="text-warning"><?php echo lang('Branch-Name') ?> :</label>
                            <br>
                            <div  class="ui-select" style="width:100%">
                                <div class="styled-select " style="width:100%">
                                    <?php company_branch_dropbox('branchid', $invoice[0]->branchid, $user->branchid, $user->companyid); ?>
                                    <span class="arrow arrowselectbox">&amp;</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?PHP endif; ?>
                <div class="g3 form-group">
                    <input id="customerid-val" name="customerid-val" type="hidden" value=''/>
                    <input id="customerid" name="customerid" type="hidden" value=''/>
                    <label class="text-warning"><?php echo lang('Customer') ?> :</label>
                    <br>
                    <input id="customername" disabled="disabled" name="customername" value="<?php echo $invoice[0]->fullname ?>" type="text" class="form-control"/>
                </div>
                <br clear="all"/>
                <script>
                    $(document).ready(function () {
                        $(".get_balance").load(config.BASE_URL + "sales_management/get_balance/" +<?php echo $invoice[0]->customer_id ?>);
                        $.ajax({
                            url: config.BASE_URL + "ajax/branch_list",
                            type: 'post',
                            data: {companyid: "<?php echo $invoice[0]->companyid ?>", branch:<?php echo $invoice[0]->branchid ?>},
                            cache: false,
                            //dataType:"json",
                            success: function (data)
                            {
                                var response = $.parseJSON(data);
                                $('#branchid').html(response.dropdown);
                                $('#storeid').html(response.store);
                                $('#customerid').html(response.customer);
                            }
                        });
                    });
                </script>
                <div class=" get_balance">
                    <div class="g4 green"><?php echo lang('balance1') ?> 0</div>
                    <div class="g4 green"><?php echo lang('balance2') ?> 0</div>
                    <div class="g4 red"><?php echo lang('balance4') ?> 0</div>
                    <div class="g4 red"><?php echo lang('balance3') ?> 0</div>
                </div>
                <br clear="all"/>
            </div>
            <br clear="all"/>
        </div>
        <div id="tproduct" class="nav-item pad-m">
            <div class="g18">
                <div class="panel panel-default" style="display: inline-block;float: right;height: auto;min-height: 240px;padding: 5px;width: 60%;">
                    <div class="panel-body">
                        <div class="g8 form-group">
                            <label class="text-warning"><?php echo lang('Category') ?> :</label>
                            <br>
                            <div class="ui-select" style="width: 100%;">
                                <select name="category_id" id="category_id" class="has-search">
                                    <option ><?php echo lang('choose') ?></option>
                                    <?php
                                    if (isset($categories) && !empty($categories)) {
                                        foreach ($categories as $categ) {
                                            ?>
                                            <option value="<?php echo $categ->catid; ?>"><?php echo _s($categ->catname, get_set_value('site_lang')); ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <span class="arrow arrowselectbox">&amp;</span>
                            </div>
                        </div>

                        <div class="g8 form-group">
                            <label class="text-warning"><?php echo lang('Product-Name') ?> :</label>
                            <input  type="hidden" name="productid"  id="productid" class="formControl"/>
                            <input style="margin-top: 5px" type="text" name="productname"  id="productname" class="g12 form-control "/>
                            <!--<a style="margin-top: 5px" onClick="show_dialog2('#demo-modal', '<?php echo base_url(); ?>ajax/getAddnew')" href="javascript:void" style="cursor:pointer;" class="g2"><img  src="<?php echo base_url(); ?>images/internal/add_customer.png" border="0" /></a> -->
                            <a style="margin-top: 5px" onClick="show_dialog2('#demo-modal', '<?php echo base_url(); ?>ajax/getallitems/p')" href="javascript:void" data-toggle="" class="g2"><i class="icon-th-list left" style="font-size:20px;color:black"></i></a> 
                            <input type="hidden" name="productname2"  id="productname2" class="form-control "/>
                        </div>

                        <br clear="all"/>

                        <div class="g8 form-group">
                            <label class="text-warning"><?php echo lang('Quantity') ?> :<span id="quantity_text"></span></label>
                            <br>
                            <!--<input id="quantity" name="quantity" type="text" onchange="calculatePrice()"  class=" currentProduct form-control"/>-->
                            <input id="quantity" name="quantity" type="text" onchange=""  class=" currentProduct form-control"/>
                        </div>

                        <div class="g8 form-group">
                            <label class="text-warning" id="fortaotal"><?php echo lang('sforone') ?> :</label>
                            <br>
                            <input name="item_total_price" id="item_total_price" type="text" onchange="changedharamtoOmr(this)"  class="item_total_price0 currentProduct form-control"/>
                        </div>

                        <br clear="all"/>

                        <div class="g8 form-group">
                            <label class="text-warning"><?php echo lang('expireddate') ?> :</label>
                            <br>
                            <input name="expireddate" id="expireddate" type="text"  class="datapic_input  currentProduct form-control"/>
                        </div>

                        <div class="g8 form-group">
                            <label class="text-warning"><?php echo lang('Serial-No') ?> :</label>
                            <br>
                            <input name="serialnumber" id="serialnumber" type="text"  class=" currentProduct form-control"/>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default" style="border: 1px solid #ccc;display: inline-block;float: left;height: auto;min-height: 240px;padding: 10px;width: 30%;">
                    <div class="panel-body">

                        <div class="g16 form-group">
                            <label class="text-warning"> <?php echo lang('store_quantity') ?></label>
                            <br>
                            <input name="" id="storequantity" type="text" style="color:green;font-size: 16px;text-align: center" value="<?php //echo get_store_quantity(3);                  ?>" disabled="disabled" class="g8 currentProduct form-control"/>
                            <input name="" id="shelf" type="text" style="color:green;font-size: 16px;text-align: center" value="<?php //echo get_store_quantity(3);                  ?>" disabled="disabled" class="g8 currentProduct form-control"/>
                        </div>

                        <div class="g16 form-group" style="display: none">
                            <label class="text-warning" id="fortaotal"><?php echo lang('p_price_aed') ?> :</label>
                            <br>
                            <input name="item_aed_price" id="item_aed_price" type="text"  onchange="changedharamtoOmr(this)" class="currentProduct form-control"/>
                        </div>

                        <div class="g16 form-group">
                            <label class="text-warning"><?php echo lang('item_total_price_prev') ?> :</label>
                            <input name="item_total_price_prev" id="item_total_price_prev" type="text"  class="item_total_price_prev currentProduct form-control"/>
                        </div>

                        <div class="g16 form-group">
                            <label class="text-warning"><?php echo lang('avrage') ?> :</label>
                            <br>
                            <input id="avrage" name="" onchange="" type="text"  class="form-control"/>
                        </div>

                        <div class="g16 form-group" style="display: none">
                            <label class="text-warning"><?php echo lang('Discount') ?> :</label>
                            <br>
                            <input id="item_discount" name="item_discount--" onkeyup="calculateDis()" type="text"  class="form-control"/>
                        </div>

                    </div>
                </div>

                <div class="g8 form-group">
                    <label class="text-warning"><?php echo lang('Discription') ?> :</label>
                    <br>
                    <textarea cols="100" style="height: 55px;"  rows="10"  name="product_comment" id="product_comment" class="dis_txtarea currentProduct form-control" ></textarea>
                </div>

                <div class="g8 form-group">
                    <label class="text-warning"><?php echo lang('Notes') ?> :</label>
                    <br>
                    <textarea name="product_notes" style="height: 55px;"  id="product_notes" cols="100" rows="10" class="dis_txtarea currentProduct form-control" ></textarea>
                </div>

                <?php $get_product = get_purchase_items($id);  //echo "<pre>"; print_r($get_product); ?>
                <input type="hidden" id="storeid_p" name="storeid_p" value="<?php echo $get_product[0]->store; ?>" />
                <input type="hidden" id="product_picture" name="product_picture" />
                <input type="hidden" id="product_type" name="invoice_itype" />
                <div class=" form-group">
                    <div class="" onclick="addEditItem2()" style="border-bottom: 1px solid green;margin-bottom: -1px;"> 
                        <div style="border-width: 1px 1px 0px 1px;width: 113px;cursor: pointer;border-color: green;border-style: solid;clear: both;padding: 7px;">
                            <i class="icon-addtocart left" style="font-size:20px;"></i>
                            <?php echo lang('add-product') ?>
                            <br clear="all"/>
                        </div>
                    </div>
                </div>

            </div>
            <?php
            ///echo "<pre>";
            //print_r($get_product[0]->store);
            //echo $get_product[0]->store;
            ?>
            <!-- invoice table -->
            <form  id="p_order" method="post" action="<?php echo base_url(); ?>purchase_management/update_purchase_order">
                <input type="hidden" name="store_id" id="store_id"  value="<?php echo $get_product[0]->store; ?>" />
                <input type="hidden" name="customerid" id="customerid"  value="<?php echo $invoice[0]->customer_id; ?>" />
                <div class="invoice_raw_product_items"></div>	
                <div id="product_list">
                    <table class="table table-bordered invoice-table mb20 tablesorter" id="table">
                        <thead class="thead">
                        <th>#</th>
                        <th><?php echo lang('Image') ?></th>
                        <th><?php echo lang('Product-Name') ?></th>
                        <th><?php echo lang('Discription') ?></th>
                        <th><?php echo lang('sforone') ?></th>
<!--                        <th><?php echo lang('p_price_aed') ?></th>-->
                        <th><?php echo lang('Quantity') ?></th>
                        <th><?php echo lang('Total') ?></th>
<!--                        <th><?php echo lang('Totalaed') ?></th>-->
                        <th></th>
                        </tr>
                        </thead>
                        <tbody>

                            <?php if ($get_product): ?>
                                <?php $count = 1 ?>
                                <?php
                                $totalp = 0;
                                $totalpd = 0;
                                ?>
                                <?php foreach ($get_product as $product): // echo "<pre>"; print_r($product);  ?>
                                    <tr id="tr_<?php echo $product->inovice_product_id ?>">
<!--                                        <td ><?php echo $product->inovice_product_id ?></td>-->
                                        <td><?php echo $product->store_item_id ?></td>
                                        <td ><a href="<?php echo base_url() ?>uploads/item/<?php echo $product->storeid ?>/<?php echo $product->product_picture ?>" class="aimg"><img src="<?php echo base_url() ?>uploads/item/<?php echo $product->storeid ?>/<?php echo $product->product_picture ?>" width="32" height="32" /></a></td>
                                        <td style="text-align:center"> <?php echo _s($product->itemname, get_set_value('site_lang')) ?> </td>
                                        <td id="description" contentEditable="true"><?php echo $product->notes ?> </td>
                                        <td  id="oneitem<?php echo $product->inovice_product_id ?>" style="text-align:center" contentEditable="true"><?php
                                            echo $product->iip;
                                            $totalp = $totalp + ($product->iip * $product->purchase_item_quantity);
                                            ?></td>
<!--                                        <td id="oneitemaed<?php echo $product->inovice_product_id ?>" style="text-align:center" contentEditable="true"><?php
                                            echo $product->purchase_item_aed_price;
                                            $totalpd = $totalpd + ($product->purchase_item_aed_price * $product->purchase_item_quantity);
                                            ?></td>-->
                                        <td id="quantity_tp<?php echo $product->inovice_product_id ?>" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculatePriceOrder('<?php echo $product->inovice_product_id ?>')"><?php echo $product->purchase_item_quantity; ?> </td>
                                        <td class="thth"  id="totalAmount<?php echo $product->inovice_product_id ?>" style="text-align:center" ><?php echo getAmountFormat($product->purchase_item_price * $product->purchase_item_quantity); ?></td>
                                        <!--<td class="aedcc" id="totalAedAmount<?php echo $product->inovice_product_id ?>" style="text-align:center" ><?php echo $product->purchase_item_aed_price * $product->purchase_item_quantity; ?></td>-->
                                <input type="hidden" id="purchase_item_price<?php echo $product->inovice_product_id; ?>" name="purchase_item_price[<?php echo $product->inovice_product_id; ?>]" value="<?php echo $product->purchase_item_price; ?>">
                                <input type="hidden" id="purchase_item_quantity<?php echo $product->inovice_product_id; ?>" name="purchase_item_quantity[<?php echo $product->inovice_product_id; ?>]" value="<?php echo $product->purchase_item_quantity; ?>">
                                <input type="hidden" id="store_item_id<?php echo $product->inovice_product_id; ?>" name="store_item_id[<?php echo $product->inovice_product_id; ?>]" value="<?php echo $product->store_item_id; ?>">
                                <input type="hidden" id="purchase_item_aedprice<?php echo $product->inovice_product_id; ?>" name="purchase_item_aedprice[<?php echo $product->inovice_product_id; ?>]" value="<?php echo $product->purchase_item_aed_price; ?>">
                                <td><a href="javascript:void(0)"></a> <i class="icon-remove-sign" style="font-size: 20px; color: red;cursor: pointer;" onclick="delete_purhcase_req_item('<?php echo $product->inovice_product_id; ?>','<?php echo $product->store_item_id ?>')"></i></td>
                                </tr>
                                <?php $pcount = $product->inovice_product_id; //$totalp+=$product->iip;    ?>
                            <?php endforeach; ?>
                        <?php else: ?>        
                            <tr><td colspan="7"><?php echo lang('no-data') ?></td></tr>
                        <?php endif; ?>
                        <tr  class="grand-total-discount">
                            <td colspan="1" >
                            </td> 
                            <td></td>
                            <td><div id="totalmin" style="display: none"></div></td>
                            <td>   
                            </td>
                            <td></td>                  
                            <td><?php echo lang('discount') ?></td>
                            <td><strong id="netTotalDiscount"><?php echo $invoice[0]->purchase_totalDiscount; ?></strong></td>
                        </tr>
                        <tr  class="grand-total">
                            <td colspan="1" >
                            </td> 
                            <td></td>
                            <td><div id="totalmin" style="display: none"></div></td>
                            <td>   
                            </td>
                            <td></td>
                            <td><?php echo lang('Total-Price') ?></td>
                            <td></td>
                            <td><strong id="netTotal"><?php echo getAmountFormat($totalp); ?></strong></td>
                            <td><strong id="netTotalaed"><?php echo getAmountFormat($totalpd); ?></strong></td>
                        </tr>




                        <tr  class="grand-total">
                            <td colspan="1" >
                            </td> 
                            <td></td>
                            <td></td>
                            <td>   
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><lable>المبلغ المدفوع الجديد</lable></td>
                        <td><input name="newpayment" value="" type="text"></td>
                        </tr>

                        <?PHP $get_sales = get_invoice_sales($id); ?>
                        <?php if ($get_sales): ?>
                            <?php foreach ($get_sales as $get_sale): ?>
                                <tr  class="">
                                    <td colspan="1" ></td> 
                                    <td><?php echo lang('Sales-Name') ?></td>
                                    <td><?php echo $get_sale->fullname ?></td>
                                    <td><?php echo lang('Payment-amount') ?></td>
                                    <td><?php echo $get_sale->amount ?></td>
                                    <td></td>
                                    <td></td>    
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <div class="g5" id="" style="">
                        <div class="col-lg-4">
                            <?php echo lang('option_1') ?>
                        </div>
                        <div class=" col-lg-1">
                            <input name="sale_direct_store"  type="checkbox"  value='1'  class=" form-control" style="width: 20px;height: 20px;"/>
                        </div>
                    </div>
                    <div class="g1" id="" style="float:left;">
                        <div class=" col-lg-1">
                            <input type="hidden" id="order_id" name="order_id" value="<?php echo $id ?>">
                            <button name="" type="button" class=" btn-glow primary"   onclick="editpurchaseInvoice()"/><?php echo lang('update') ?></button>
                        </div>
                    </div>
                </div>
                <!-- #end invoice table -->
            </form>
        </div>
    </div>    
</div>    
<br clear="all"/>
<br clear="all"/>
<a href="<?php echo base_url() ?>sales_management/printed/<?php echo $id ?>" class="btn btn-default"><i class="icon-print"></i><?php echo lang('Print') ?></a>
<br clear="all"/>
<br clear="all"/>
<script type="text/javascript" src="<?php echo base_url(); ?>js/function.js" ></script> 
<!--------->
<div class='hide_form_payment' style="display:none"> 
    <script type="text/javascript">
                                $('.datapic_input').datepicker({dateFormat: 'yy-mm-dd'});
                                pcount = '<?php echo $pcount; ?>';
    </script>
    <div class=" form-group" style="" id="">
        <label class="text-warning"><?php echo lang('howpay') ?> :</label><br>
        <div class="ui-select"  style="width:100%">                       
            <select class="validate[required]" name="payment[howpay][]" onchange="">
                <option value="" selected="selected" ><?php echo lang('choose') ?> </option>
                <option value="1"><?php echo lang('frombalance') ?></option>
                <option value="2"><?php echo lang('outbalance') ?></option>
            </select>                            
        </div>
    </div>
    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('TypePayment') ?> </label><br>
        <div class="ui-select styled-select2"  style="width:100%">
            <select name="payment[p_type][]" class="" onchange="" payment='5' class='1'>
                <option value="" selected="selected" ><?php echo lang('choose') ?> </option>
                <option value="1"><?php echo lang('Cash') ?></option>
                <option value="2"><?php echo lang('Bank') ?></option>
            </select>
        </div>
    </div>
    <div class="g4 form-group ">
        <label class="text-warning"><?php echo lang('Payment-date') ?></label><br>
        <input name="payment[p_date][]"  type="text"  class="datapic_input payment_date form-control"/>
    </div>
    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('Payment-amount') ?></label><br>
        <input name="payment[p_amount][]"  type="text" id="payment_amountt" onchange="do_payment_amountt(this.value)" value=""  class=" form-control"/>
    </div>
    <div class="g4 form-group" id="div_banks" style="display:none;">
        <label class=""><?php echo lang('Bank'); ?> </label>
        <div class="ui-select styled-select3" style="width:100%">
            <?php company_bank_dropbox('payment[p_bank][]', '', 'english', '', 'arr_bank'); ?>
        </div>
    </div>
    <div class="g4 form-group" style="display:none" id="div_accounts">
        <label class=""><?php echo lang('Accounts') ?> </label>
        <div class="ui-select" id="div_accounts_responce" style="width:100%">
        </div>
    </div>
    <div class="g4 form-group" style="display:none" id="div_TypePayment2">
        <label class="text-warning"><?php echo lang('TypePayment2') ?> :</label><br>
        <div class="ui-select"  style="width:100%">                       
            <select class="" name="payment[p_type2][]" onchange="showPaymentLabel(this.value, 0)">
                <option value="" selected="selected" ><?php echo lang('choose') ?> </option>
                <option value="cheque"><?php echo lang('Cheque') ?></option>
                <option value="transfer"><?php echo lang('Transfer') ?></option>
                <option value="deposite"><?php echo lang('Deposite') ?></option>
                <option value="withdraw"><?php echo lang('withdraw') ?></option>
                <option value="later"><?php echo lang('later') ?></option>
            </select>                            
        </div>
    </div>
    <div class="g4 form-group" style="display: none" id="div_pdoc">
        <label class="text-warning"><?php echo lang('Payment-doc') ?> :</label><br>
        <input name="payment[p_doc][]"  type="file"  class=" form-control"/>
    </div>   
    <div class="g4 form-group" style="display: none" id="div_pnumper">
        <label class="text-warning"><?php echo lang('Number') ?> :</label><br>
        <input name="payment[p_numper][]"  type="text"  class=" form-control"/>
    </div>   
    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('Payment-note') ?> :</label><br><textarea name='payment[p_note][]' class=" form-control"></textarea>
    </div>
</div>
<div class="hide_form_notes" style="display:none">
    <div class="g4 form-group" style="" id="div_pnumper">
        <label class="text-warning">عنوان<?php //echo lang('Number')             ?> :</label><br>
        <input name="notes[title][]"  type="text"  class=" form-control"/>
    </div>   
    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('Payment-note') ?> :</label><br><textarea name='notes[note][]' class=" form-control"></textarea>
    </div> 
    <br clear="all"/>
</div>
<div class="hide_form_files" style="display:none">
    <div class="g4 form-group" style="" id="div_pnumper">
        <label class="text-warning">عنوان<?php //echo lang('Number')             ?> :</label><br>
        <input name="files[title][]"  type="text"  class=" form-control"/>
    </div>   
    <div class="g4 form-group">
        <label class="text-warning">الملف<?php //echo lang('Number')             ?> :</label><br>
        <input name="files[doc][]"  type="file"  class=" form-control"/>
    </div> 
</div>
<div class='hide_form_expences' style="display:none"> 
    <script type="text/javascript">


        $(document).ready(function () {
            $(".js-example-basic-single").select2();
            $('.datapic_input').datepicker({dateFormat: 'yy-mm-dd'});
        });
    </script>
    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('expense_title') ?></label>
        <div class="">
            <input name="expense[expense_title][]" id="" type="text"  value="" class="form-control"/>
        </div>
    </div>
    <input type="hidden" name="type" id="type" value="indirect" />  
    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('value') ?></label>
        <div class="">
            <input name="expense[value][]" value="" id="value" type="text"  class="form-control"/>
        </div>
    </div>
    <div class="g3 form-group" id="div_banks">
        <label class="text-warning"><?php echo lang('cat') ?> </label>
        <div class="">
            <div class="ui-select" style="width:100%">
                <div class="">
                    <?php expense_charges2('expense[expense_charges_id][]', $exdata->expense_charges_id, 'english', ''); ?>
                    <span class="arrow arrowselectbox">&amp;</span>
                </div>
            </div>
        </div>
    </div>
    <div class="g4 form-group">
        <label class="col-lg-12 text-warning"><?php echo lang('clearanceDate') ?></label>
        <input name="clearance_date" id="clearance_date" value="" type="text"  class="required  valid datapic_input datapic_input form-control" style=""/>
    </div>
    <input type="hidden" id="expense_type1" class="expense_period" value="direct" name="expense[expense_type][]">
    <br clear="all"    />
    <div class="g4 form-group" id="div_banks">
        <label class="text-warning"><?php echo lang('Bank') ?> </label>
        <div class="">
            <div class="ui-select" style="width:100%">
                <div class="styled-select3" style="width:100%">
                    <?php company_bank_dropbox('expense[bank_id_ex][]', '', 'english', ' ', 'bank_id_ex'); ?>
                    <span class="arrow arrowselectbox">&amp;</span>
                </div>
            </div>
        </div>
    </div>
    <div class="g4 form-group" style="" id="div_accounts">
        <label class="text-warning"><?php echo lang('Accounts') ?> </label>  
        <div class="styled-select" id="div_accounts_responce_ex" style="width:100%">
            <select id="div_accounts_responce" class="div_accounts_responce_ex required  valid " name="account_id" onchange="loadtranserBankaccounts();" >
            </select>
            <span class="arrow arrowselectbox">&amp;</span>
        </div>
    </div>
    <div class="g4 form-group" id="div_checkque" style="display:none;">
        <label class="text-warning">Deposite Recipt </label>
        <div class="">
            <div class="ui-select" >
                <div class="styled-select">
                    <input type="expense[file][]"  class="form-control" name="deposite_recipt" id="deposite_recipt" />
                </div>
            </div>
        </div>
    </div>
    <!--                            <div class="g4 form-group" style="display:none" id="div_tr_accounts">
                                    <label class="text-warning">Accounts </label>
                                    <div class="">
                                        <div class="ui-select" >
                                            <div class="styled-select" id="div_tr_accounts_responce">
    
                                            </div>
                                        </div>
                                    </div>
                                </div>  -->
    <br clear="all"/>
    <div class="g14 form-group">
        <label class="text-warning"><?php echo lang('Notes') ?></label>
        <div class="">
            <textarea name="expense[Notes][]" cols="" rows="" class="form-control"></textarea>
        </div>
    </div> 
</div>
<script type="text/javascript">
    $(function () {
        $('#expense_date').datepicker({dateFormat: 'yy-mm-dd'});
        $(".expense_period").click(function () {
            //alert('asdasd');
            //checked_status = $(this).is(':checked');
            expense_period_val = $(this).val();
            //alert(checked_status);
            if (expense_period_val == 'fixed') {
                $("#period_parent").show();
            }
            else {
                $("#period_parent").hide();
            }
        });
    });
    $(document).ready(function () {
        //alert('ready');
        var ac_config = {
            source: "<?php echo base_url(); ?>outcome/getAutoSearch",
            select: function (event, ui) {
                $("#expense_title").val(ui.item.cus);
                $("#expense_id").val(ui.item.cus);
                console.log(ui);
                //swapme();
                setTimeout('swapgeneral()', 500);
            },
            minLength: 1
        };
        $("#expense_title").autocomplete(ac_config);
    });
    function delete_purhcase_req_item(rowid,store_item_id) {
        message = 'هل أنت متأكد أنك تريد حذف؟';
        ret = confirm(message);
        if (ret) {
            abc = $("#invoice_input_" + rowid).html();
            if (abc) {
                confirmpurchaseEditRemove(rowid,store_item_id);
            }
            else {
                delete_payorder_record(rowid,store_item_id);
            }

        }
    }
    function delete_payorder_record(id,store_item_id) {
        $.ajax({
            url: config.BASE_URL + "purchase_management/delete_purchase_order",
            type: 'post',
            data: {purchase_item_id: id,store_item_id:store_item_id},
            cache: false,
            //dataType:"json",
            success: function (data) {
                if (data) {
                    $("#tr_" + id).remove();
                }
            }
        });
        //$("#tr_"+id).remove();
    }
    function calculatePriceOrder(ob) {
        ttpriceaed = 0;
        quantity = $("#quantity_tp" + ob).text().trim();
        oneitem = $("#oneitem" + ob).text().trim();
        oneitemaed = $("#oneitemaed" + ob).text().trim();
        //totalLength = netTotals.length;
        //totalDiscount = $("#totalDiscount").val();
        //console.log(totalLength);
        // var editArray = geteditProductData(ob);
        //pProduct = $("#item_total_price").val();
        if (quantity > 0) {
            totalPrice = (quantity * oneitem);
            $("#tr_" + ob + " #totalAmount" + ob + "").text(totalPrice.toFixed(3));
            ttprice = 0;
            if ($('.thth').length > 0) {
                len = $('.thth').length;
                for (a = 0; a < len; a++) {
                    tprice = $('.thth').eq(a).html();
                    ttprice = parseFloat(ttprice) + parseFloat(tprice);
                    //console.log(tprice + 'tprice');
                }
            }
            $("#netTotal").text(ttprice.toFixed(3));
            $(".netTotal").text(ttprice.toFixed(3));
            tAmount = oneitem * quantity;
            aedontime = $("#oneitemaed" + ob + "").text();
            aedontime = parseFloat(aedontime).toFixed(3);
            totalontme = aedontime * parseInt(quantity);
            $(" #totalAedAmount" + ob + "").text(totalontme.toFixed(3))
            //  alert(ob);
            // alert('iff');
            // $("#tr_" + ob + " #totalAmount" + ob + "").text(tAmount.toFixed(3));
        } else {
            // alert('else');
            $("#tr_" + ob + " #totalAmount" + ob + "").text(oneitem.toFixed(3));
            ttprice = 0;
            if ($('.thth').length > 0) {
                len = $('.thth').length;
                for (a = 0; a < len; a++) {
                    tprice = $('.thth').eq(a).html();
                    ttprice = parseFloat(ttprice.toFixed(3)) + parseFloat(tprice.toFixed(3));
                    //console.log(tprice + 'tprice');
                }
            }
            //$("#totalAmount").text(oneitem);
            //  alert('net total');
            $("#netTotal").text(ttprice.toFixed(3));
            $(".netTotal").text(ttprice.toFixed(3));
            //$("#netTotal").text(oneitem);
            //$("#totalnet").val(oneitem);
        }
        // calculateaed();
        if ($('.aedcc').length > 0) {
            len = $('.aedcc').length;
            for (a = 0; a < len; a++) {
                tpriceaed = $('.aedcc').eq(a).html();
                //alert(tpriceaed);
                ttpriceaed = parseFloat(ttpriceaed) + parseFloat(tpriceaed);
                //console.log(tprice + 'tprice');
            }
        }
        // $("#purchase_item_price"+obj).val();
        $("#purchase_item_quantity" + ob).val(quantity);
        //$("#purchase_item_price"+obj).val();
        //alert(ttpriceaed);
        $("#netTotalaed").text(ttpriceaed.toFixed(3));
    }
    function calculateaedd() {
        ttaedprice = 0;
        //console.log($('.thth').length);
        unit_total
        if ($('.aedcc').length > 0) {
            len = $('.aedcc').length;
            for (a = 0; a < len; a++) {
                tprice = $('.aedcc').eq(a).text();
                tprice = tprice;
                ttaedprice = parseFloat(ttaedprice) + parseFloat(tprice);
                //alert(tprice);
                //console.log(tprice + 'tprice');
            }
            $("#unit_total").html(ttaedprice);
            $(".unit_total").html(ttaedprice);
            //unit_total
            // $("#totalaed").val(ttaedprice);
        }
        ttaedprice = 0;
        if ($('.ttaaed ').length > 0) {
            len = $('.ttaaed ').length;
            for (a = 0; a < len; a++) {
                tprice = $('.ttaaed ').eq(a).text();
                ttaedprice = parseFloat(ttaedprice) + parseFloat(tprice);
                //alert(tprice);
                //console.log(tprice + 'tprice');
            }
            $("#netaedTotal").html(ttaedprice);
            $(".netaedTotal").html(ttaedprice);
            $("#totalaed").val(ttaedprice);
        }
    }
    function editpurchaseInvoice() {
        var formData = $("#p_order").serialize();
        att = $("#p_order").attr("action");
        $.post(att, formData).done(function (data) {
            if (data == '1') {
                alert('تم تحديث البيانات');
                window.top.location = base_url + 'purchase_management/purchase_invoices';
            } 
        });
    }
</script>
<script type="text/javascript">
    function swapme2p() {
        pid = $("#productid").val();
        pname = $("#productname2").val();
        $("#productname").val(pname);
        $("#productid").val(pid);
    }
    function swapmec() {
        //alert('swapme');
        pid = $("#customerid").val();
        pname = $("#customername2").val();
        $("#customername").val(pname);
        $("#customerid").val(pid);
        $("#customerid-val").val(pname);
    }
    $(document).ready(function () {
        //	alert('asd');
        $("#category_id").change(function () {
            val = $(this).val();
            //alert(val);
            $("#productname").trigger('keydown');
        });
        ///setInterval('checkData()', 1000);
        storeId = $("#store_id").val();
        /*var ac_config = {
         source: "<?php echo base_url(); ?>ajax/getAutoSearchProducts?store_id=",
         select: function(event, ui){
         $("#productname").val(ui.item.id);
         $("#productid").val(ui.item.prod);
         getProductData(ui.item.id);
         console.log(ui);
         //swapme();
         setTimeout('swapme2()',500);
         },
         minLength:1
         };
         
         $("#productname").focusin(function(){
         //alert('asd');
         storeId = $("#store_id").val();
         
         });*/
        //$("#productname").autocomplete(ac_config);
//        $("#productname").autocomplete({
//            source: function (request, response) {
//                //$.getJSON("<?php echo base_url(); ?>ajax/getAutoSearchProducts", {category: $('#category_id').val(), term: $('#productname').val()},
//                $.getJSON("<?php echo base_url(); ?>ajax/getAutoSearchProducts2", {category: $('#category_id').val(), term: $('#productname').val(), storeid: $('#store_id').val()},
//                response);
//            },
//            minLength: 0,
//            select: function (event, ui) {
//                //action
//                $("#productname").val(ui.item.prod);
//                $("#productname2").val(ui.item.prod);
//                $("#productid").val(ui.item.id);
//                getProductData2(ui.item.id, '', $('#store_id').val());
//                console.log(ui);
//                //swapme();
//                setTimeout('swapme2p()', 500);
//            }
//        });
//        $("#productname").focus(function () {
//            $("#productname").trigger('keydown');
//        });


        $(function () {
            $("#productname").autocomplete({
                source: function (request, response) {
                    var sUrl = "<?php echo base_url(); ?>ajax/ajaxAutocomplete";
                    $.getJSON(sUrl, request, function (result) {
                        response(result);
                    });
                },
                select: function (event, ui) {

                    getProductData2(ui.item.id, $('#store_id').val());
                    $("#productid").val(ui.item.id);
                    $("#productname2").val($("#productname").val());
                }
            });
        });
        var ac_config = {
            source: "<?php echo base_url(); ?>ajax/getAutoSearchCustomer2",
            select: function (event, ui) {
                $("#customerid").val(ui.item.id);
                $("#customername").val(ui.item.cus);
                $("#customername2").val(ui.item.cus);
                //$("#customer_ftotal").val(ui.item.buywithtotal);
                console.log(ui);
                //swapme();
                setTimeout('swapmec()', 500);
                $.ajax({
                    url: "<?php echo base_url() ?>purchase_management/get_balance",
                    type: "POST",
                    data: {val: ui.item.id},
                    success: function (e) {
                        $('.get_balance').html(e);
                    }
                });
            },
            minLength: 1
        };
        $("#customername").autocomplete(ac_config);
    });
    function checkData() {
        ///  plen = allProductsData.length;
        if (plen > 0) {
            $('#customername').attr("disabled", true);
        }
        else {
            $('#customername').attr("disabled", false);
        }
    }
    $(document).ready(function () {
        $('#Alternate_Price').click(function () {
            if ($("#Alternate_Price:checked").val() == 1) {
                $('#Alternate_Price_input').css('display', 'block');
                //$('#item_total_price').css('display', 'block');
                //$('#item_total_price').attr('disabled', true);
                $('.item_total_price0').attr('id', 'item_total_price2');
                $('.item_total_price0').attr('name', 'item_total_price2');
                $('.item_total_price_old').attr('id', 'item_total_price');
                $('.item_total_price_old').attr('name', 'item_total_price');
            } else {
                $('#Alternate_Price_input').css('display', 'none');
                //$('#item_total_price').removeAttr('disabled');
                $('.item_total_price0').attr('id', 'item_total_price');
                $('.item_total_price0').attr('name', 'item_total_price');
                $('.item_total_price_old').attr('id', 'item_total_price_old');
                $('.item_total_price_old').attr('name', 'item_total_price_old');
            }
            $("#item_total_price").change(function () {
                //$(this).val()
                $('#total_price').val($(this).val());
                //alert($(this).val());
            });
        });
        $('#is_payment').click(function () {
            if ($("#is_payment:checked").val() == 1) {
                $('.payment_div').css('display', 'block');
            }
        });
        $('#customerType').change(function () {
            if ($(this).val() == 'newcustomer') {
                $('#newcustomer').css('display', 'block');
                $('#customer').css('display', 'none');
                //$('.payment_div').css('display', 'block');
            } else {
                $('#customer').css('display', 'block');
                $('#newcustomer').css('display', 'none');
            }
        });
        //        $('#payment_amount').change(function () {
        //            //$("#remin_view_all_total").html($("#remin_view_all_total").html()+$("#payment_amount").html());
        //            $("#remin_view_all_total").html(parseInt($("#remin_view_all_total").html()) - parseInt($('#payment_amount').val()));
        //
        //            $("#recievedamount").val(parseInt($("#remin_view_all_total").html()));
        //            $("#totalrecievedamount").val(parseInt($('#payment_amount').val()));
        //            //alert($("#howpay").val());
        //            if ($("#howpay").val() == "1") {
        //                $("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) + parseInt($('#payment_amount').val()));
        //            }
        //        });
        $('#payment_amount').change(function () {
            //$("#remin_view_all_total").html($("#remin_view_all_total").html()+$("#payment_amount").html());

            $("#remin_view_all_total").html(parseInt($("#remin_view_all_total").html()) - parseInt($('#payment_amount').val()));
            $("#recievedamount").val(parseInt($("#payment_amount").val()));
            $("#totalrecievedamount").val(parseInt($('#payment_amount').val()));

            //alert($("#howpay").val());
            ppshow = parseInt($("#get_my_allaccount").html()) - parseInt($('#payment_amount').val());
            ppshow2 = parseInt($("#view_all_total").html()) - parseInt($('#payment_amount').val());


            if ($("#howpay").val() == "1") {
                if (parseInt($("#remin_view_all_total").html()) < 0) {

                    $("#increse_amount_span").text(Math.abs(ppshow2));
                    $("#increse_amount").css('display', 'block');
                    $("#remin_view_all_total").text("0");
                    $("#payment_amount").val($('#view_all_total').html());
                    if ($("#remin_view_all_total").text() > 0) {
                        $("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) - parseInt($('#view_all_total').html()));
                    }
                    /*
                     $("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) - parseInt($('#view_all_total').html()));
                     $("#recievedamount").val(parseInt($('#view_all_total').html()));
                     $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                     $("#remin_view_all_total").text("0");
                     */
                } else {
                    $("#get_my_allaccount").text(ppshow);
                }
            } else {
                if (parseInt($("#remin_view_all_total").html()) < 0) {
                    //alert('if');
                    $("#increse_amount").css('display', 'block');
                    $("#increse_amount_span").text(Math.abs(ppshow2));
                    $("#remin_view_all_total").text("0");
                    $("#payment_amount").val($('#view_all_total').html());
                    //$("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) - parseInt($('#view_all_total').html()));
                    /*
                     $("#recievedamount").val(parseInt($('#view_all_total').html()));
                     $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                     $("#remin_view_all_total").text("0");
                     $("#increse_amount").show();
                     */
                } else {
                    //alert('else');
                    //$("#increse_amount_span").text(Math.abs(ppshow2));
                    $("#remin_view_all_total").text(ppshow2);
                }
            }
        });
        $('#store_id').change(function () {
            var val = $(this).val();
            var pid = $("#productid").val();
            $.ajax({
                url: "<?php echo base_url() ?>sales_management/get_store_quantity",
                type: "POST",
                data: {val: val, pid: pid},
                success: function (e) {
                    $('#storequantity').val(e);
                }
            });
        });
    });
    function  do_payment_amountt(e) {
        //$("#remin_view_all_total").html($("#remin_view_all_total").html()+$("#payment_amount").html());
        //alert(e);
        $("#remin_view_all_total").html(parseInt($("#remin_view_all_total").html()) - parseInt(e));
        $("#recievedamount").val(parseInt($("#remin_view_all_total").html()));
        $("#totalrecievedamount").val(parseInt($("#totalrecievedamount").val()) + parseInt(e));
        if ($("#howpay").val() == "1") {
            $("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) + parseInt(e));
        }
    }
    function rest_payment() {
        $('#get_my_allaccount').html("0");
        $('#howpay').val("");
        $('#recievedamount').val("");
        $('#remin_view_all_total').html($('#view_all_total').html());
        $('#increse_amount').hide("");
        $('#payment_amount').val("");
        $("#add_new_payment_btn").show();
    }
</script>