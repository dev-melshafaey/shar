

<?php $this->load->view('common/meta'); ?>
<!--body with bg-->


<div class="body">
    <header>
        <?php $this->load->view('common/header'); ?>

        <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
        <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>fileUpload/css/jquery.fileupload.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>fileUpload/css/jquery.fileupload-ui.css">
        <!-- CSS adjustments for browsers with JavaScript disabled -->
        <link href="<?php echo base_url(); ?>css/lib/bootstrap.datepicker.css" type="text/css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>jasny-bootstrap/css/jasny-bootstrap.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/validationEngine.jquery.css" type="text/css"/>

    </header>
    <?php $this->load->view('common/left-navigations'); ?>

    <div class="content">
        <?php
//	echo "<pre>";
//	print_r($company);
        ?>

        <!-- settings changer -->
        <div class="skins-nav">
            <a href="#" class="skin first_nav selected">
                <span class="icon"></span><span class="text">Default</span>
            </a>
            <a href="#" class="skin second_nav" data-file="<?php echo base_url(); ?>css/compiled/skins/dark.css">
                <span class="icon"></span><span class="text">Dark skin</span>
            </a>
        </div>

        <div id="pad-wrapper">
            <div class="row form-wrapper">
                <div class="col-md-12 col-xs-12">



                    <div id="main-content" class="main_content">
                        <div class="title title alert alert-info">
                            <span>Add Customer</span>
                        </div>

                        <div class="notion title title alert alert-info">* Please Understand  Clearly All The Data Before  Entering</div>

<form action="" method="get">
<div class="form">
<div class="raw">
<div class="form_title">Date of Paid</div>
<div class="form_field">
<input name="date_paid" id="date_paid" type="text"  class="formtxtfield"/>
<div class="date_icon"><a href="#"><img src="<?php echo base_url();?>images/internal/date_icon.png" width="22" height="24" border="0" /></a></div>
</div>
</div>
<div class="raw">
<div class="form_title">Type of Purchase</div>
<div class="form_field">
    <label>
      <input type="radio" name="RadioGroup1" value="product" id="RadioGroup1_0" />
      Product</label>
    <br />
    <label>
      <input type="radio" name="RadioGroup1" value="service" id="RadioGroup1_1" />
      Service</label>
    <br />

  <input type="hidden" id="total_price" name="total_price"  value="" />
            <input type="hidden" id="price_product" name="price_product" />
            <input type="hidden" id="store_id" name="store_id" />
</div>
</div>
<div class="raw">
<div class="form_title">Supplier Name</div>
<div class="form_field">
  <div class="dropmenu">
    <div class="styled-select">
      <select name="supplier_id" id="supplier_id" onchange="getproductsBySupplier(this.value)">
        <option selected="selected" >Current Suppliers</option>
        <?php
			if(!empty($suppliers)){
				foreach($suppliers as $supplier){
			    	?>
                    <option  value="<?php echo $supplier->supplierid; ?>"><?php echo $supplier->suppliername; ?></option>
                    <?php
				}
			}
		?>
        
      </select>
    </div>
  </div>
</div>
<div class="left_div2">
<div id='osx-modal'>
			<div class="invoice_add_customer"><a href='#' class='osx'><img src="<?php echo base_url();?>images/internal/add_customer.png" width="20" height="20" border="0" /></a>		    </div>
</div>
</div>
<div id="osx-modal-content">
			<div id="osx-modal-title">Add New Supplier</div>
			<div class="close"><a href="#" class="simplemodal-close">x</a></div>
		<div id="osx-modal-data">
<div class="raw">
<div class="form_title">Supplier Name</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Phone  Number</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Fax  Number</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Office Number</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Email</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Website</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Contact Person</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Products</div>
<div class="form_field">
  <div class="dropmenu">
    <div class="styled-select">
      <select name="select">
        <option selected="selected" >Current Products</option>
        <option>Supplier 1</option>
        <option >Supplier 2</option>
      </select>
    </div>
  </div>
</div>

</div>
<div class="raw">
<div class="form_title">Notes</div>
<div class="form_field">
<textarea name="" cols="" rows="" class="formareafield"></textarea>
</div>
</div>
                    <div class="raw" align="center">
                    <input name="" type="submit" class="submit_btn" value="Submit" />
                    <input name="" type="reset" class="reset_btn" value="Reset" />
                    </div>
<!--end of raw-->

</div>
		</div>
</div>

<div class="raw">
<div class="form_title">Product Name</div>
<div class="form_field">
  <div class="dropmenu">
    <div class="styled-select">
      <select name="product_id" id="product_id">
        <option selected="selected" >Current Products</option>
      </select>
    </div>
  </div>
</div>
<div class="left_div2">
<div id='osx2-modal'>
			<div class="invoice_add_customer"><a href='#' class='osx2'><img src="<?php echo base_url();?>images/internal/add_customer.png" width="20" height="20" border="0" /></a>		    </div>
</div>
</div>
</div>
<div id="osx2-modal-content">
			<div id="osx2-modal-title">Add New Product</div>
			<div class="close"><a href="#" class="simplemodal-close">x</a></div>
		<div id="osx2-modal-data">
        
<div class="raw">
<div class="form_title">Category</div>
<div class="form_field">
  <div class="dropmenu">
    <div class="styled-select">
      <select name="select">
        <option selected="selected" >Current Category</option>
        <option>Category 1</option>
        <option >Category 2</option>
      </select>
    </div>
  </div>
</div>

</div>
<div class="raw">
<div class="form_title">Pruduct  Name</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Product Type</div>
<div class="form_field">
  <div class="dropmenu">
    <div class="styled-select">
      <select name="select">
        
        <option  selected="selected">Product</option><option>Service</option>
      </select>
    </div>
  </div>
</div>
<div id="osx-modal-content">
  <div id="osx-modal-title">Add New Supplier</div>
			<div class="close"><a href="#" class="simplemodal-close">x</a></div>
		<div id="osx-modal-data">
<div class="raw">
<div class="form_title">Supplier Name</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Phone  Number</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Fax  Number</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Office Number</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield"/>
</div>
</div>
<div class="raw">
<div class="form_title">Products</div>
<div class="form_field">
  <div class="dropmenu">
    <div class="styled-select">
      <select name="select">
        <option selected="selected" >Current Products</option>
        <option>Supplier 1</option>
        <option >Supplier 2</option>
      </select>
    </div>
  </div>
</div>

</div>
<div class="raw">
<div class="form_title">Notes</div>
<div class="form_field">
<textarea name="" cols="" rows="" class="formareafield"></textarea>
</div>
</div>
                    <div class="raw" align="center">
                    <input name="" type="submit" class="submit_btn" value="Submit" />
                    <input name="" type="reset" class="reset_btn" value="Reset" />
                    </div>
<!--end of raw-->

</div>
		</div>
</div>
<div class="raw">
<div class="form_title">Suppliers Name</div>
<div class="form_field">
  <div class="dropmenu">
    <div class="styled-select">
      <select name="select">
        <option selected="selected" >Current Suppliers</option>
        <option>Supplier 1</option>
        <option >Supplier 2</option>
      </select>
    </div>
  </div>
</div>


</div>
<div class="raw">
<div class="form_title">Serial  Number</div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield_small"/>
</div>
</div>
<div class="raw">
<div class="form_title">Purchase Price </div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield_small"/>
</div>
</div>
<div class="raw">
<div class="form_title">Sale Price </div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield_small"/>
</div>
</div>
<div class="raw">
<div class="form_title">Min Sale Price </div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield_small"/>
</div>
</div>
<div class="raw">
<div class="form_title">Point of Sale </div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield_small"/>
</div>
</div>
<div class="raw">
<div class="form_title">Quntity </div>
<div class="form_field">
<input name="" type="text"  class="formtxtfield_small"/>
</div>
</div>
<div class="raw">
  <div class="form_title">Notes</div>
<div class="form_field">
<textarea name="" cols="" rows="" class="formareafield"></textarea>
</div>
</div>
                    <div class="raw" align="center">
                    <input name="" type="submit" class="submit_btn" value="Submit" />
                    <input name="" type="reset" class="reset_btn" value="Reset" />
                    </div>
		</div>
</div>
<div class="raw">
<div class="form_title">Product Serial Number</div>
<div class="form_field">
<input name="" type="password"  class="formtxtfield_small"/>
</div>
</div>
<div class="raw">
<div class="form_title">Quantity</div>
<div class="form_field">
<input name="" type="password"  class="formtxtfield_small"/><span class="left_div">Units</span>
</div>
</div>
<div class="raw">
<div class="form_title">Unit Price</div>
<div class="form_field">
<input name="" type="password"  class="formtxtfield_small"/>
<span class="left_div">RO.</span></div>

</div>
<div class="raw">
  <div class="form_title">Attachment Files</div>
<div class="form_field">
 <div id="FileUpload">
 
    <input type="file" size="24" id="BrowserHidden" onchange="getElementById('FileField').value = getElementById('BrowserHidden').value;" />
 
    <div id="BrowserVisible"><input type="text" id="FileField" /></div>
</div>
</div>
</div>

                    <div class="raw" align="center">
                    <input name="" type="submit" class="submit_btn" value="Submit" />
                    <input name="" type="reset" class="reset_btn" value="Reset" />
                    </div>
<!--end of raw-->
</div>
</form>

</div>
      <!-- END PAGE -->  
   </div>
   </div>
   </div>
   </div>
   </div>

<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
