<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Reports_model extends CI_Model {

    /*

     * Properties

     */



//----------------------------------------------------------------------



    /*

     * Constructor

     */



    function __construct() {

        parent::__construct();



        //Load Table Names from Config

    }

    function inserData($table, $data) {

        $this->db->insert($table, $data);

        return $this->db->insert_id();

    }

    function checkCashMoneyexist($date){
        $sql = "SELECT
  COUNT(cdv.cash_daily_val)  AS total
FROM
  `cash_daily_value` AS cdv
WHERE DATE(cdv.`cash_report_date`) = '".$date."' ";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }

    function getExpencesReports(){

        $get = $this->input->get();
        if ($get['frm']!=null && $get['to']!=null) {
            $datt = date_range($get['frm'],$get['to'], "+1 day", "Y-m-d");
            $uid = $get['uid'];
            // echo "<pre>";
            // print_r($datt);
            $cases = ' ';
            if(!empty($datt)){
                foreach($datt as $d){
                    //
                    $responseDate = getFromTo($d);
                    $dateArr = json_decode($responseDate);
                    // $responseDate;
                    $from = $dateArr->from;
                    $to = $dateArr->to;
                    $then = date('Y-m-d',strtotime($from));
                    $cases.=" WHEN e.`expense_date` >=  '$from' AND e.`expense_date` <= '$to' THEN '$then'";

                }
                $sql = "
                SELECT
  IFNULL(SUM(t.`value`), 0) AS totalexpence,
  t.*
FROM
  (SELECT
    e.`expense_date`,
    e.`value`,
    CASE
     $cases
    END AS erange
  FROM
    `an_expenses` AS e) AS t
WHERE t.erange != ''
GROUP BY t.erange
ORDER BY t.erange DESC
                ";

                //  echo $sql;

                $q = $this->db->query($sql);

                if ($q->num_rows() > 0) {
                    return $q->result_array();
                }
                else{
                    return false;
                }
            }

        }
    }

    function getClearCashAMountByReports(){

        $get = $this->input->get();
        if ($get['frm']!=null && $get['to']!=null) {
            $datt = date_range($get['frm'],$get['to'], "+1 day", "Y-m-d");
            $uid = $get['uid'];
            // echo "<pre>";
            // print_r($datt);
            $cases = ' ';
            if(!empty($datt)){
                foreach($datt as $d){
                    //
                    $responseDate = getFromTo($d);
                    $dateArr = json_decode($responseDate);
                    // $responseDate;
                    $from = $dateArr->from;
                    $to = $dateArr->to;
                    $then = date('Y-m-d',strtotime($from));
                    $cases.=" WHEN cur.`created` >=  '$from' AND cur.`created` <= '$to' THEN '$then'";

                }
                $sql = "
                SELECT
  IFNULL(SUM(t.`closing_amount`), 0) AS totalclosingamount,
  t.*
FROM
  (SELECT
    cur.created AS cccd,
    cur.`closing_amount`,
    Case
    $cases
    END AS crange
   FROM
    `closing_cash_entries` AS cur) AS t
WHERE  t.crange !='' AND t.closing_amount>'0'
GROUP BY t.crange
ORDER BY t.crange DESC";

                //echo $sql;
                $q = $this->db->query($sql);

                if ($q->num_rows() > 0) {
                    return $q->result_array();
                }
                else{
                    return false;
                }

                //echo $sql;
            }

        }
        //        $start,$end;


    }

    function getClearAMountByReports(){

        $get = $this->input->get();
        if ($get['frm']!=null && $get['to']!=null) {
            $datt = date_range($get['frm'],$get['to'], "+1 day", "Y-m-d");
            $uid = $get['uid'];
            // echo "<pre>";
            // print_r($datt);
            $cases = ' ';
            if(!empty($datt)){
                foreach($datt as $d){
                    //
                    $responseDate = getFromTo($d);
                    $dateArr = json_decode($responseDate);
                    // $responseDate;
                    $from = $dateArr->from;
                    $to = $dateArr->to;
                    $then = date('Y-m-d',strtotime($from));
                    $cases.=" WHEN cur.`created` >=  '$from' AND cur.`created` <= '$to' THEN '$then'";

                }
                $sql = "
                SELECT
  IFNULL(SUM(t.`clear_amount`), 0) AS totalclearamount,
  t.*
FROM
  (SELECT
    cur.created AS cccd,
    cur.`clear_amount`,
    Case
    $cases
    END AS drange
  FROM
    `clear_users_orders` AS cur) AS t
WHERE t.drange !=''
GROUP BY t.drange
ORDER BY t.drange DESC";

                $q = $this->db->query($sql);

                if ($q->num_rows() > 0) {
                    return $q->result();
                }
                else{
                    return false;
                }

                //echo $sql;
            }

        }
        //        $start,$end;


    }

    function getLastCloseTime($date){

        //$date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        $from = $dateArr->from;
        $to = $dateArr->to;


        $sql = "SELECT
  *
FROM
  `closing_cash_entries` AS cce
WHERE cce.`closing_time` >= '$from'
  AND cce.`closing_time` <= '$to'
  ORDER BY cce.`closing_id` DESC LIMIT 1 ";

        //     echo $sql;
        $q = $this->db->query($sql);

        //return $q->row();

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }

    function getLastClosingTime(){
        $sql = "SELECT cce.closing_time FROM closing_cash_entries AS cce ORDER BY  cce.closing_id DESC LIMIT 1";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row()->closing_time;
        }
        else{
            return false;
        }
    }
    function getCashDetails($closingid){
        $sql = "SELECT
  cdv.*,
  cf.`cash_fixed_type`
FROM
  `cash_daily_value` AS cdv
  INNER JOIN `cash_fixed_type` AS cf
    ON cf.`cash_fixed_id` = cdv.`cash_type`
WHERE cdv.closing_id = '".$closingid."' ";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        else{
            return false;
        }
    }
    function getClosingById($id){
        $sql = "SELECT cce.closing_time FROM closing_cash_entries AS cce WHERE cce.`closing_id` = '$id'";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row()->closing_time;
        }
        else{
            return false;
        }
    }
    function getClearAmountByUserDate($date){
        //$date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;
        $sql = "SELECT
  cur.`user_id`,
  u.`fullname`,
  IFNULL(SUM(cur.`clear_amount`),0) AS totalclearamount
FROM
  `clear_users_orders` AS cur
    INNER JOIN `bs_users` AS u
    ON u.`userid` = cur.`user_id`
WHERE cur.`created` >= '" . $from . "'  AND cur.`created` <= '" . $to . "'   GROUP BY cur.`user_id`";
        //  echo $sql;
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        else{
            return false;
        }
    }
    function getBrnachDetails(){
        $sql = "SELECT * FROM `bs_company_branch` AS cb WHERE cb.`branchid` = '1'";
        $q = $this->db->query($sql);



        if ($q->num_rows() > 0) {

            return $q->row();

        } else {

            return false;

        }
    }
    function getDiscountAmountByDate($date){
        //$date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;
        $sql = "SELECT
  SUM(i.`invoice_totalDiscount`) AS totaldiscount,SUM(i.`coupon_value`) AS totalcoupon
FROM
  `an_invoice` AS i
  WHERE i.`invoice_date` >= '" . $from . "'  AND i.`invoice_date` <= '" . $to . "' AND i.`cancel_sales`='0'";
        //echo $sql;
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }
    function viewCLosingReports(){
            $sql = "SELECT
  ce.*,u.fullname
FROM
  `closing_cash_entries` AS ce
  INNER JOIN `bs_users` AS u
    ON u.userid = ce.user_id
    ORDER BY ce.closing_id DESC";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        else{
            return false;
        }
    }
    function getCashMoneyexist($date){
        $sql = "SELECT
  *
FROM
  `cash_daily_value` AS cdv
WHERE DATE(cdv.`cash_report_date`) = '".$date."' ";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        else{
            return false;
        }
    }


    function getCancelInvoicesPrev($date){
        //$date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;
        $sql = "SELECT
  COUNT(i.`invoice_id`) AS totalcancel,IFNULL(SUM(i.invoice_total_amount),0) AS totalcancelamount
FROM
  `an_invoice` AS i
WHERE i.`cancel_sales` = '1' AND i.`invoice_date` >= '" . $from . "'
  AND i.`invoice_date` <= '" . $to ."' ";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }

    function getCancelInvoices(){
        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;
        $sql = "SELECT
  COUNT(i.`invoice_id`) AS totalcancel,IFNULL(SUM(i.invoice_total_amount),0) AS totalcancelamount
FROM
  `an_invoice` AS i
WHERE i.`cancel_sales` = '1' AND i.`invoice_date` >= '" . $from . "'
  AND i.`invoice_date` <= '" . $to ."' ";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }

    function getDailyCatReport($categ){
        $cats = implode(',',$categ);
        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;

        $sql = "SELECT
  c.`catid`,
  c.`catname`,
  SUM((it.`invoice_item_quantity`*it.`invoice_item_price`)) AS totalsales
FROM
  `bs_invoice_items` AS it
  INNER JOIN `bs_item` AS i
    ON i.`itemid` = it.`invoice_item_id`
  INNER JOIN `bs_category` AS c
    ON c.`catid` = i.`categoryid`
  INNER JOIN `an_invoice` AS inv
    ON inv.`invoice_id` = it.`invoice_id`
WHERE  c.`catid`IN($cats) AND inv.`invoice_date` >= '" . $from . "'  AND inv.`invoice_date` <= '" . $to . "'
GROUP BY i.`categoryid` ;
 ";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        else{
            return false;
        }
    }

    function getSalesByUsersPrev($date){
        //$date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;

        $sql = "SELECT
  u.`userid`,
  u.`fullname`,
  IFNULL(SUM(inv.`invoice_total_amount`), 0) AS totalinvamount
FROM
  `bs_users` AS u
  INNER JOIN `an_invoice` AS inv
    ON inv.`user_id` = u.`userid`
WHERE u.`member_type` != '6'
  AND inv.`cancel_sales` = '0' AND inv.`invoice_date` >= '$from'
  AND inv.`invoice_date` <= '$to'
GROUP BY u.`userid`
ORDER BY COUNT(inv.`user_id`) DESC ";
       
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        else{
            return false;
        }
    }
    function getSalesByUsers(){
        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);

        $from = $dateArr->from;
        $to = $dateArr->to;

        $sql = "SELECT
  u.`userid`,
  u.`fullname`,
  IFNULL(SUM(inv.`invoice_total_amount`), 0) AS totalinvamount
FROM
  `bs_users` AS u
  INNER JOIN `an_invoice` AS inv
    ON inv.`user_id` = u.`userid`
WHERE u.`member_type` != '6'
  AND inv.`cancel_sales` = '0' AND inv.`invoice_date` >= '$from'
  AND inv.`invoice_date` <= '$to'
GROUP BY u.`userid`
ORDER BY COUNT(inv.`user_id`) DESC ";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        else{
            return false;
        }
    }

    function getClearAmountByDatePrev($date){
        //$date = date('Y-m-d');

        //$date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;
        $sql = "SELECT
  IFNULL(SUM(cur.`clear_amount`),0) AS totalclearamount
FROM
  `clear_users_orders` AS cur
WHERE cur.`created` >= '" . $from . "'  AND cur.`created` <= '" . $to . "'";
//echo $sql;
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }

    function getClearAmountByDate($cdate){
        //$date = date('Y-m-d');

        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;
        $sql = "SELECT
  IFNULL(SUM(cur.`clear_amount`),0) AS totalclearamount
FROM
  `clear_users_orders` AS cur
WHERE cur.`created` >= '" . $from . "'  AND cur.`created` <= '" . $to . "'";
//echo $sql;
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }


    function getDashboardReports(){
        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
  //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;
        $sql = "SELECT
  (SELECT
    IFNULL(SUM(ex.`value`), 0)
  FROM
    `an_expenses` AS ex WHERE ex.`expense_date`>='".$from."'  AND ex.`expense_date`<='".$to."') AS totalexpenses,
  (SELECT
    SUM(
      it.purchase_item_price * it.`purchase_item_quantity`
    )
  FROM
    `bs_purchase_items` AS it
    INNER JOIN `an_purchase` AS i
      ON i.`purchase_id` = it.`purchase_id`
  WHERE i.`cancel_pruchase` = '0' AND i.`purchase_date`>='".$from."'  AND i.`purchase_date`<='".$to."') AS sum_purchasninvoiceprice,
  (SELECT
    SUM(
      it.invoice_item_price * it.invoice_item_quantity
    ) AS sum_invoiceprice
  FROM
    `bs_invoice_items` AS it
    INNER JOIN `an_invoice` AS i
      ON i.`invoice_id` = it.`invoice_id`
  WHERE i.`cancel_sales` = '0' AND i.`invoice_date`>='".$from."'  AND i.`invoice_date`<='".$to."') AS sum_invoiceprice

 ";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }
    function getExpenceReportByDatePrev($date)
    {
        //$date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;
        $sql = "SELECT
  SUM(an_expenses.value) AS totalexpenses
FROM
  `an_expenses`
WHERE
   an_expenses.expense_date >= '" . $from . "'
  AND an_expenses.expense_date <= '" . $to . "'
  AND`an_expenses`.`value` > '0' ";
        //echo $sql;
        $q = $this->db->query($sql);

        //return $q->row();

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }

    function getExpenceReportByDate($cdate)
    {
        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;
        $sql = "SELECT
  SUM(an_expenses.value) AS totalexpenses
FROM
  `an_expenses`
WHERE
   an_expenses.expense_date >= '" . $from . "'
  AND an_expenses.expense_date <= '" . $to . "'
  AND`an_expenses`.`value` > '0' ";
        //echo $sql;
        $q = $this->db->query($sql);

        //return $q->row();

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }
    function getPurchaseReportByDate($cdate){
        $sql = "SELECT
  sum_invoiceprice,
  sum_invoicediscount,
  sumipurchase,
  i.`purchase_date`,
  IFNULL(totalpaid, 0) AS totalpaid
FROM
  `an_purchase` AS i
  LEFT JOIN
    (SELECT
      i.`purchase_id`,
      SUM(it.`purchase_item_price`) AS suminvoicepurchase,
      SUM(
        it.purchase_item_price * it.`purchase_item_quantity`
      ) AS sum_invoiceprice,
      SUM(
        it.`purchase_item_discount` * it.purchase_item_quantity
      ) AS sum_invoicediscount,
      SUM(
        bs_item.purchase_price * it.purchase_item_quantity
      ) AS sumipurchase
    FROM
      `bs_purchase_items` AS it
      INNER JOIN `an_purchase` AS i
        ON i.`purchase_id` = it.`purchase_id`
      LEFT JOIN `bs_item`
        ON bs_item.itemid = it.`purchase_item_id`
    WHERE DATE(i.`purchase_date`) = DATE('".$cdate."')
    GROUP BY DATE(i.`purchase_date`)) AS iit
    ON iit.`purchase_id` = i.`purchase_id`
  LEFT JOIN
    (SELECT
      ip.`purchase_id`,
      SUM(ip.`payment_amount`) AS totalpaid
    FROM
      `purchase_payments` AS ip
    WHERE DATE(ip.`created`) = DATE('".$cdate."')
    GROUP BY DATE(ip.`created`)) AS ipp
    ON ipp.purchase_id = i.`purchase_id`
WHERE DATE(i.`purchase_date`) = DATE('".$cdate."')
GROUP BY DATE(i.`purchase_date`)";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }

    function sendSalesReportsByDatePrev($date){

        //$date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;

        $sql  = "SELECT
  SUM(i.`invoice_total_amount`) sum_invoiceprice
  FROM
   `an_invoice` AS i
WHERE
   i.`invoice_date` >= '" . $from . "'
  AND i.`invoice_date` <= '" . $to . "'
  AND i.`cancel_sales` = '0' ";
        // echo $sql;
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }

    function sendSalesReportsByDate($cdate){

        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);
        //      echo "<pre>";
//        print_r($dateArr);
        $from = $dateArr->from;
        $to = $dateArr->to;

        $sql  = "SELECT
  SUM(i.`invoice_total_amount`) sum_invoiceprice
  FROM
   `an_invoice` AS i
WHERE
   i.`invoice_date` >= '" . $from . "'
  AND i.`invoice_date` <= '" . $to . "'
  AND i.`cancel_sales` = '0' ";
       // echo $sql;
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }


    function sendCardSalesReportsByDate($cdate){

        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);

        $from = $dateArr->from;
        $to = $dateArr->to;

        $sql  = "SELECT
  SUM(i.`invoice_total_amount`) sum_invoiceprice
  FROM
   `an_invoice` AS i
WHERE
   i.`invoice_date` >= '" . $from . "'
  AND i.`invoice_date` <= '" . $to . "'
  AND i.`cancel_sales` = '0' and i.invoice_deal_type='card'";
        // echo $sql;
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }


    function sendTotalDiscountReportsByDate($cdate){

        $date = date('Y-m-d');
        $responseDate = getFromTo($date);
        $dateArr = json_decode($responseDate);

        $from = $dateArr->from;
        $to = $dateArr->to;

        $sql  = "SELECT
  SUM(i.`invoice_totalDiscount`) sum_discount, SUM(i.`coupon_value`) coupon_discount
  FROM
   `an_invoice` AS i
WHERE
   i.`invoice_date` >= '" . $from . "'
  AND i.`invoice_date` <= '" . $to . "'
  AND i.`cancel_sales` = '0'";
        // echo $sql;
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else{
            return false;
        }
    }

}



