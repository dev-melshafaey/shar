<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends CI_Controller {

    public function __construct() {

        parent::__construct();

        //    $this->lang->load('main', get_set_value('site_lang'));

        $this->load->model('reports_model', 'report_mangement');
        //   $this->load->model('ajax/ajax_model', 'ajax');
        //$this->_data['udata'] = userinfo_permission();
    }

    function index() {
        
    }

    function get_cash_details() {
        $postData = $this->input->post();
        $closingid = $postData['closingid'];
        $this->_data['p_invoices'] = $this->report_mangement->getCashDetails($closingid);
    }

    function viewClosingReports() {
        $this->_data['p_invoices'] = $this->report_mangement->viewCLosingReports();
        $this->_data['inside'] = 'daily_cash_list';
        $this->load->view('common/main', $this->_data);
    }

    function getClosingReportByDate($closingid) {

        $d = $this->report_mangement->getClosingById($closingid);
        $cdate = date('Y-m-d ', (strtotime('-1 day', strtotime($d))));
        
        $this->_data['sales_report'] = $this->report_mangement->sendSalesReportsByDate($cdate);
        $this->_data['card_sales_report'] = $this->report_mangement->sendCardSalesReportsByDate($cdate);
        $this->_data['discount_sales_report'] = $this->report_mangement->sendTotalDiscountReportsByDate($cdate);
        $this->_data['last_close_report'] = $this->report_mangement->getLastCloseTime($cdate);
        $this->_data['purchase_report'] = $this->report_mangement->getPurchaseReportByDate($cdate);
        $this->_data['expences_reports'] = $this->report_mangement->getExpenceReportByDate($cdate);
        $this->_data['clear_reports'] = $this->report_mangement->getClearAmountByDate($cdate);
        $this->_data['clear_by_user'] = $this->report_mangement->getClearAmountByUserDate($cdate);
        $this->_data['discount_reports'] = $this->report_mangement->getDiscountAmountByDate($cdate);
        $this->_data['branch_details'] = $this->report_mangement->getBrnachDetails();
        $this->_data['cancel_reports'] = $this->report_mangement->getCancelInvoices($cdate);
        $this->_data['sales_users_reports'] = $this->report_mangement->getSalesByUsers($cdate);
        
        /*$this->_data['sales_report'] = $this->report_mangement->sendSalesReportsByDatePrev($cdate);
        $this->_data['last_close_report'] = $this->report_mangement->getLastCloseTime($cdate);
        $this->_data['purchase_report'] = $this->report_mangement->getPurchaseReportByDate($cdate);
        $this->_data['expences_reports'] = $this->report_mangement->getExpenceReportByDatePrev($cdate);
        $this->_data['clear_reports'] = $this->report_mangement->getClearAmountByDatePrev($cdate);
        $this->_data['clear_by_user'] = $this->report_mangement->getClearAmountByUserDate($cdate);
        $this->_data['discount_reports'] = $this->report_mangement->getDiscountAmountByDate($cdate);
        $this->_data['branch_details'] = $this->report_mangement->getBrnachDetails();
        $this->_data['cancel_reports'] = $this->report_mangement->getCancelInvoicesPrev($cdate);
        $this->_data['sales_users_reports'] = $this->report_mangement->getSalesByUsersPrev($cdate);*/
        //$this->_data['cash_money'] = $this->report_mangement->getCashMoneyexist($cdate);
        //$cdate = date('d/m/Y h:i:s');
        $this->_data['report_date'] = $d;

        $this->load->view('email_prev_view', $this->_data);
    }

    function viewClosingReports1() {
        $this->_data['p_invoices'] = $this->report_mangement->viewCLosingReports();

        //daily_cash_list
        $this->load->view('daily_cash_list', $this->_data);
    }

    function getDailyReprtsHistory() {



        $this->_data['selectitemid'] = '';
        $get = $this->input->get();
        $this->_data['uid'] = $get['uid'];

        //$this->_data['sale_invoices3'] = $this->report_mangement->getClearCashAMountByReports();
        $this->_data['sale_invoices4'] = $this->report_mangement->getExpencesReports();
        //getExpencesReports
        $this->_data['sale_invoices2'] = $this->report_mangement->getClearCashAMountByReports();
        $this->_data['sale_invoices'] = $this->report_mangement->getClearAMountByReports();

        //$this->load->view('sales-invoices', $this->_data);

        $this->_data['inside'] = 'sales-daily-reports';

        $this->load->view('common/main', $this->_data);
    }

    function save_cah_report() {
        $postData = $this->input->post();
        /*
          [fifty_val] => 2
          [twenty_val] => 2
          [ten_val] => 1
          [five_val] => 3
          [one_val] => 3
          [fivehundred_paisa_val] => 4
          [hundred_paisa_val] => 2
          [fifty_paisa_val] => 4
          [twentyfive_paisa_val] => 5
          [ten_paisa_val] => 1
          [five_paisa_val] => 2

         */
        //echo "<pre>";
        //print_r($postData);
        //exit;
        $date = date('Y-m-d');
        //$resp = $this->report_mangement->checkCashMoneyexist($date);
        if ($postData) {

            $closingData['closing_time'] = date('Y-m-d H:i:s');
            $closingData['closing_amount'] = $postData['total'];
            $closingData['user_id'] = $this->session->userdata('bs_userid');
            $closingid = $this->report_mangement->inserData('closing_cash_entries', $closingData);

            $cashData['cash_quantity'] = $postData['fifty_val'];
            $cashData['cash_type'] = 1;
            $cashData['closing_id'] = $closingid;
            $cashData['cash_report_date'] = date('Y-m-d');
            $this->report_mangement->inserData('cash_daily_value', $cashData);

            $cashData['cash_quantity'] = $postData['twenty_val'];
            $cashData['cash_type'] = 2;
            $cashData['closing_id'] = $closingid;
            $cashData['cash_report_date'] = date('Y-m-d');
            $this->report_mangement->inserData('cash_daily_value', $cashData);

            $cashData['cash_quantity'] = $postData['ten_val'];
            $cashData['cash_type'] = 3;
            $cashData['closing_id'] = $closingid;
            $cashData['cash_report_date'] = date('Y-m-d');
            $this->report_mangement->inserData('cash_daily_value', $cashData);

            $cashData['cash_quantity'] = $postData['five_val'];
            $cashData['cash_type'] = 4;
            $cashData['closing_id'] = $closingid;
            $cashData['cash_report_date'] = date('Y-m-d');
            $this->report_mangement->inserData('cash_daily_value', $cashData);

            $cashData['cash_quantity'] = $postData['one_val'];
            $cashData['cash_type'] = 5;
            $cashData['closing_id'] = $closingid;
            $cashData['cash_report_date'] = date('Y-m-d');
            $this->report_mangement->inserData('cash_daily_value', $cashData);

            $cashData['cash_quantity'] = $postData['fivehundred_paisa_val'];
            $cashData['cash_type'] = 6;
            $cashData['closing_id'] = $closingid;
            $cashData['cash_report_date'] = date('Y-m-d');
            $this->report_mangement->inserData('cash_daily_value', $cashData);

            $cashData['cash_quantity'] = $postData['hundred_paisa_val'];
            $cashData['cash_type'] = 7;
            $cashData['closing_id'] = $closingid;
            $cashData['cash_report_date'] = date('Y-m-d');
            $this->report_mangement->inserData('cash_daily_value', $cashData);

            $cashData['cash_quantity'] = $postData['fifty_paisa_val'];
            $cashData['cash_type'] = 8;
            $cashData['closing_id'] = $closingid;
            $cashData['cash_report_date'] = date('Y-m-d');
            $this->report_mangement->inserData('cash_daily_value', $cashData);

            $cashData['cash_quantity'] = $postData['twentyfive_paisa_val'];
            $cashData['cash_type'] = 9;
            $cashData['closing_id'] = $closingid;
            $cashData['cash_report_date'] = date('Y-m-d');
            $this->report_mangement->inserData('cash_daily_value', $cashData);

            $cashData['cash_quantity'] = $postData['ten_paisa_val'];
            $cashData['cash_type'] = 10;
            $cashData['closing_id'] = $closingid;
            $cashData['cash_report_date'] = date('Y-m-d');
            $this->report_mangement->inserData('cash_daily_value', $cashData);

            $cashData['cash_quantity'] = $postData['five_paisa_val'];
            $cashData['cash_type'] = 11;
            $cashData['closing_id'] = $closingid;
            $cashData['cash_report_date'] = date('Y-m-d');
            $this->report_mangement->inserData('cash_daily_value', $cashData);
        }
    }

    function testdate() {
        //
        $d = date('Y-m-d');
        $yesdate = date('Y-m-d ', (strtotime('-1 day', strtotime($d))));
        echo $dateString = $yesdate . ' 02:00:00';
        echo "<br>";
        echo $dateString = $d . ' 01:45:00';

        echo date('Y-m-d H:i:s', strtotime($dateString));
        //echo $dateObject = new DateTime($dateString);
    }

    function viewDailyReport() {

        $cdate = date('Y-m-d');
        // $cdate = $this->report_mangement->getLastClosingTime();
        $this->_data['sales_report'] = $this->report_mangement->sendSalesReportsByDate($cdate);
//        $this->_data['total_credit'] = $this->report_mangement->totalPaidByCredit($cdate);
        $this->_data['card_sales_report'] = $this->report_mangement->sendCardSalesReportsByDate($cdate);

        $this->_data['discount_sales_report'] = $this->report_mangement->sendTotalDiscountReportsByDate($cdate);
        $this->_data['last_close_report'] = $this->report_mangement->getLastCloseTime($cdate);
        $this->_data['purchase_report'] = $this->report_mangement->getPurchaseReportByDate($cdate);
        $this->_data['expences_reports'] = $this->report_mangement->getExpenceReportByDate($cdate);
        $this->_data['clear_reports'] = $this->report_mangement->getClearAmountByDate($cdate);
        $this->_data['clear_by_user'] = $this->report_mangement->getClearAmountByUserDate($cdate);
        $this->_data['discount_reports'] = $this->report_mangement->getDiscountAmountByDate($cdate);
        $this->_data['branch_details'] = $this->report_mangement->getBrnachDetails();
        $this->_data['cancel_reports'] = $this->report_mangement->getCancelInvoices($cdate);
        $this->_data['sales_users_reports'] = $this->report_mangement->getSalesByUsers($cdate);
        //$this->_data['cash_money'] = $this->report_mangement->getCashMoneyexist($cdate);
        $cdate = date('d/m/Y H:i:s');
        $this->_data['report_date'] = $cdate;
        //$this->_data['cash_money'] = $this->report_mangement->getCashMoneyexist($cdate);
        $this->load->view('email_view', $this->_data);
    }

    function getDailyCategoryReport() {
        $categories[] = 11;
        $categories[] = 15;
        $this->_data['sales_report'] = $this->report_mangement->getDailyCatReport($categories);
        //echo "<pre>";
        // print_r($this->_data['sales_report']);
        // exit;
        $this->load->view('categ_report_view', $this->_data);
    }

    function testphpmailer() {
        // error_reporting(E_ALL);
        require './phpmailer/PHPMailerAutoload.php';

//Create a new PHPMailer instance
        $mail = new PHPMailer;
//Set who the message is to be sent from
        $mail->setFrom('noreply@omanbs.com', 'Oman Bs');
//Set an alternative reply-to address
        //      $mail->addReplyTo('replyto@example.com', 'First Last');
//Set who the message is to be sent to
        $mail->addAddress('jamal4334@gmail.com');
//Set the subject line
        $cdate = date('Y-m-d');
        $this->_data['sales_report'] = $this->report_mangement->sendSalesReportsByDate($cdate);
        $this->_data['purchase_report'] = $this->report_mangement->getPurchaseReportByDate($cdate);
        $this->_data['expences_reports'] = $this->report_mangement->getExpenceReportByDate($cdate);


        $mail->Subject = 'Daily Report ' . date('d/m/Y');
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
        $mail->msgHTML($this->load->view('email_view', $this->_data, TRUE), dirname(__FILE__));
        //$mail->msgHTML('asdasd');
//Replace the plain text body with one created manually
        $mail->AltBody = 'This is a plain-text message body';
//Attach an image file
//        $mail->addAttachment('images/phpmailer_mini.png');
//send the message, check for errors
        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message sent!";
        }
    }

    public function send_email2() {
        $to = 'shahzaib@durar-it.com';



        $list = array($to);

        $config = array();
        $config['protocol'] = 'sendmail';
        //$config['mailpath'] 	= '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['smtp_host'] = "localhost";
        $config['smtp_port'] = "25";
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->load->library('email');

        $this->email->initialize($config);
        $this->email->from('no-reply@omanbs.com', 'No Reply');
        $this->email->to($list);
        $this->email->subject('test');
        $this->email->message('message');



        $send = $this->email->send();
        if (!$send) {
            print_r($this->email->clear_debugger_messages());
        }
        //echo $this->email->print_debugger();
    }

    function sendmail2() {

        $email = 'shahzaib.memon69@gmail.com';

        $cdate = date('Y-m-d');
        $this->_data['sales_report'] = $this->report_mangement->sendSalesReportsByDate($cdate);

        //print_r($this_data['sales_report']);
        $cdate = date('Y-m-d');
        $this->_data['purchase_report'] = $this->report_mangement->getPurchaseReportByDate($cdate);

        $cdate = date('Y-m-d');
        $this->_data['expences_reports'] = $this->report_mangement->getExpenceReportByDate($cdate);



        //     $messageData=  $this->mobile->get_contents($slug);
        //   $messageData=  $this->mobile->get_contents($slug);
        // $nmessage.=$messageData->content_text." ".$message;

        $list = array($email);

        $config = array();
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['smtp_host'] = "localhost";
        $config['smtp_port'] = "25";
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->load->library('email');

        $this->email->initialize($config);
        $this->email->from('no-reply@omanbsc.com', 'No Reply');
        $this->email->to($list);
        $this->email->subject('Daily Reports');
        $this->email->message($this->load->view('pos_view', $this->_data, TRUE));



        echo $this->email->send();
    }

    function testmail() {
        $config = Array(
            //'protocol' => 'smtp',
            'mailtype' => 'html',
                //'smtp_host' => 'ssl://smtp.googlemail.com',
                //'smtp_port' => 465,
                //'smtp_user' => 'myaccountname@gmail.com',
                //'smtp_pass' => 'mygmailpassword',
        );

        $this->load->library('email', $config);
        $this->email->from('norepy@omanbs.com', 'BS');
        // $this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
        $this->email->to('shahzaib.memon69@gmail.com');
        $this->email->subject('Daily Reports');
        //$this->email->message($this->load->view('email_view', $this->_data,TRUE));
        //       $this->email->set_alt_message($this->load->view('email/'.$type.'-txt', $data, TRUE));
        //$this->email->set_newline("\r\n"); // require this, otherwise sending via gmail times out

        echo $this->email->send();
    }

    function sendEmail() {
//        $this->load->library('email');

        $cdate = date('Y-m-d', strtotime('15-08-2016'));
        $this->_data['sales_report'] = $this->report_mangement->sendSalesReportsByDate($cdate);

        //print_r($this_data['sales_report']);
        $cdate = date('Y-m-d');
        $this->_data['purchase_report'] = $this->report_mangement->getPurchaseReportByDate($cdate);

        $cdate = date('Y-m-d');
        $this->_data['expences_reports'] = $this->report_mangement->getExpenceReportByDate($cdate);

        sendtestMail();
    }

    function ttt() {
        echo "<pre>";
        //print_r($_SERVER);
        exit;
    }

    function sendEmailReports() {


        $cdate = date('Y-m-d');
        $this->_data['sales_report'] = $this->report_mangement->sendSalesReportsByDate($cdate);

        //print_r($this_data['sales_report']);
        $cdate = date('Y-m-d');
        $this->_data['purchase_report'] = $this->report_mangement->getPurchaseReportByDate($cdate);

        $cdate = date('Y-m-d');
        $this->_data['expences_reports'] = $this->report_mangement->getExpenceReportByDate($cdate);

        $this->load->view('email_view', $this->_data);
    }

    function sendMessageTest() {
        sendMessage();
    }

}
