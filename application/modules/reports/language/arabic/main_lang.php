<?php



/* 

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */


$lang['From-Date']= 'من تاريخ';

$lang['To-Date']= 'الي تاريخ';

$lang['Search']= 'بحث';

$lang['select']= 'اختر';

$lang['From-Date']= 'من تاريخ';

$lang['To-Date']= 'الي تاريخ';

$lang['Search']= 'بحث';


$lang['selectbranch']= ' اختر فرع';

$lang['All']= ' كل';

$lang['totalPaid']= ' مجموع المبالغ المدفوعة';
$lang['discount']=' Discount';
$lang['totalpayable']=' مجموعه المستحقة';
$lang['totalreturn']= 'كمية عودة';
//مجموع المبالغ المدفوعة
$lang['Invoice-Number']= 'رقم الفاتوره';

$lang['Total-Price']= 'اجمالي السعر';

$lang['Net-Price']= 'المبلغ النهائي';

$lang['discountamount']= 'الخصم';

$lang['Customer']= 'العميل';

$lang['Date']= 'التاريخ';

$lang['Status']= 'الحالة';

$lang['Payments']= 'الدفع';

$lang['Incomplete']= 'غير مكتمل';

$lang['Complete']= 'تم';

$lang['Extra']= 'اضافي';

$lang['Cash']= 'كاش';

$lang['Bank']= 'البنك';

$lang['choose']= 'اختر';





$lang['Mobile-Number']= 'رقم الجوال';

$lang['Contact-Number']= 'رقم الاتصال';

$lang['Fax-Number']= 'رقم الفاكس';

$lang['Email-Address']= 'البريد الالكتروني';

$lang['Address']= 'العنوان';

$lang['Type']= 'النوع';

$lang['Responsable-Name']= 'اسم المسئول';

$lang['Responsable-Phone']= 'جوال المسئول';

$lang['Notes']= 'ملاحظات';

$lang['Status']= 'الحالة';

$lang['Prev']= 'السابق';

$lang['Next']= 'التالي';

$lang['Total-Purchase']= 'اجمالي المشتريات';

$lang['Total-Paid']= 'اجمالي الدفع';

$lang['Remaining']= 'المتبقي';





$lang['Company-Name']= 'اسم الشركة';

$lang['Branch-Name']= 'اسم الفرع';

$lang['Customer']= 'العميل';

$lang['Type']= 'نوع التعامل';

$lang['Company']= 'شركة';

$lang['Individual']= 'فرد';

$lang['Store']= 'المخزن';

$lang['Category']= 'القسم';

$lang['Product-Name']= 'اسم المنتج';

$lang['Quantity']= 'الكمية';

$lang['Serial-No']= 'رقم السيريال';

$lang['Total']= 'الاجمالي';

$lang['Discount']= 'تخفيض';

$lang['Minimum']= 'الحد الادني';

$lang['Discription']= 'الوصف';

$lang['Notes']= 'ملاحظات';

$lang['Image']= 'الصوره';







$lang['Net']= 'الصافي';

$lang['Total-Price']= 'اجمالي السعر';



$lang['Net-Price']= 'السعر الصافي';

$lang['Recieved']= 'المستلم';

$lang['Rest-of-Amount']= 'المتبقي ';

$lang['Products-Maintenance']= 'صيانة المنتج';

$lang['Sales-Name']= 'اسم البائع';

$lang['Sales-Amount']= 'قيمة المبيعات';

$lang['Add-Sales']= 'اضافة بائع';



$lang['Payments']= 'الدفع';

$lang['Remianing-Amount']= 'القيمة المتبقية';

$lang['Cash']= 'كاش';

$lang['Cheque']= 'شيك';

$lang['Bank']= 'بنك';

$lang['Cheque-Number']= 'رقم الشيك';

$lang['Period']= 'فترة';

$lang['Weeks']= 'أسابيع';

$lang['Months']= 'اشهر';

$lang['Years']= 'اعوام';

$lang['Payment-Numbers']= 'رقم الدفع';

$lang['Payment-Date']= 'تاريخ الدفع';

$lang['TypePayment']= 'نوع الدفع';

$lang['Receipt-Number']= 'رقم الايصال';



$lang['Sales-Responsable']= 'التسويق';

$lang['Code']= 'كود';







$lang['Invoice']= 'الفاتوره';

$lang['Invoice-No']= 'رقم الفاتوره';

$lang['Date']= 'تاريخ الانشاء';

$lang['Print']= 'طباعة';

$lang['Sent-To']= 'المستلم';

$lang['Recieved-From']= 'المستلم منه';

$lang['Price']= 'السعر';





$lang['newcustomer']= 'عميل جديد';

$lang['customer']= 'عميل حالي ';

$lang['Deposite']= 'الإيداع ';

$lang['Transfer']= 'تحويل ';





$lang['add-edit']= 'اضافة جديد & تحديث حالي'.'';

$lang['mess1']= 'يرجى فهم بوضوح جميع البيانات قبل الادخال';

$lang['Add']= 'اضافة';

$lang['Reset']= 'اعاده تعيين';

$lang['Branch-Name']= 'الفرع';

$lang['RePassword']= 'تاكيد كلمة المرور';

$lang['Password']= 'كلمة المرور';

$lang['User-Name']= 'اسم المستخدم';

$lang['Add-Account']= 'اضافة عضويه';

$lang['Company-Name']= 'الشركة';

$lang['Customer-Name']= 'العميل';





$lang['Phone']= 'الهاتف';

$lang['Mobile']= 'تليفون اخر';

$lang['Alternate-Price']= 'سعر بديل';

$lang['afterDiscount']= 'بعد التخفيض';





$lang['tclient']= 'العملاء';

$lang['tproduct']= 'المنتجات';

$lang['tpay']= 'الدفع';

$lang['tbill']= 'خصائص  الطباعه';



$lang['Payment-date']= 'تاريخ الدفع';

$lang['Payment-amount']= 'المبلغ';

$lang['Payment-Numbers']= 'رقم الايصال';

$lang['Payment-doc']= 'المستندات';

$lang['Payment-note']= 'الملاحظات';



$lang['ptype1']= "رقم الشيك";

$lang['ptype2']= "رقم الحساب المحول منه";

$lang['ptype3']= "رقم الحساب المودع منه";





$lang['Add']= 'اضافة جديد';

$lang['userlist']= 'قائمة الاعضاء';

$lang['fromwhat']= 'حساب العموله';

$lang['commession']= ' العموله';

$lang['typecommession']= ' حساب النسبه';

$lang['fromtotal']= ' من الاجمالي';

$lang['fromnetprice']= ' من الربح';

$lang['percent']= 'نسبه % ';

$lang['amount']= 'مبلغ';

$lang['addsales']= 'التسويق >> اضافة جديد';

$lang['later']= 'آجل';





$lang['sforone']= 'سعر المنتج';



$lang['balance1']= 'اجمالي التعاملات  : ';

$lang['balance2']= 'الرصيد: ';

$lang['balance3']= ' المبالغ المستحقة علي العميل ';

$lang['balance4']= '  ما تم دفعه مسبقا: ';

$lang['Store']= 'المخزن';

$lang['Accounts']= 'الحساب';

$lang['TypePayment2']= 'نوع التعامل';

$lang['withdraw']= 'سحب مباشر';

$lang['Number']= 'رقم ';

$lang['howpay']= 'كيفية الدفع ';

$lang['frombalance']= 'من حسابي ';

$lang['outbalance']= 'من خارج حسابي ';

$lang['store_quantity']= 'الكمية المتوفره في المخزن  : ';

/**/





$lang['show_datatable']= ' عرض  من _START_   الي   _END_  سجلات '.' الاجمالي _TOTAL_ ';

$lang['Previous']= 'السابق';

$lang['First']= 'الاول';

$lang['Last']= 'الاخير';

$lang['Next']= 'التالي';

$lang['Search']= 'البحث';

$lang['show_bylist']= 'عرض_MENU_ سجلات';









$lang['Invoice-Number']= ' الفاتوره';

$lang['offer-Number']= 'العرض';

$lang['invoice_type'] = 'invoice type';

$lang['invoice_status'] = 'invoice status';

$lang['branch'] = 'Branch';

$lang['Total-Price']= 'اجمالي ';

$lang['Net-Price']= 'السعر الصافي';

$lang['discountamount']= 'الخصم';

$lang['Customer']= 'العميل';

$lang['typepay']= 'نوع الدفع';

$lang['amountpay']= ' المدفوعات';

$lang['remain']= 'الباقي';

$lang['NetPrice']= 'الصافي ';

$lang['saleprice']= 'قيمة الشراء';

$lang['expences']= 'المصروفات';

$lang['Statusinvoice']= 'حالة الفاتوره';

$lang['salesperson']= 'المسوق';

$lang['salesperson_percent']= 'نسبة م';

$lang['add-product']= 'أضافة المنتج';

$lang['texpences']= 'المصاريف';





$lang['expense_title']= 'عنوان';

$lang['expense_type']= 'نوع الصرف';

$lang['expense_period']= 'الفتره';

$lang['value']= 'القيمة';

$lang['created']= 'تاريخ الانشاء';

$lang['expense_cat']= 'قسم المصاريف';

$lang['Fixed']= 'ثابت';

$lang['Accured']= 'متغير';

$lang['Daily']= 'يوميا';

$lang['Weekly']= 'اسبوعيا';

$lang['Monthly']= 'شهريا';

$lang['Yearly']= 'سنويا';

$lang['cat']= 'القسم';





$lang['view_title']= 'عرض تفاصيل العرض';

$lang['no-data']= 'لا يوجد بيانات';

$lang['discount']= 'التخفيض';



$lang['Customer2']= 'الاعتماد بواسطة';

$lang['resetbalance']= 'تهيئة الحساب';



$lang['addinvoice']= 'اضافة عرض سعر';

$lang['next']= 'التالي';

$lang['back']= 'السابق';

$lang['activeq']= 'اعتماد العرض';

$lang['save']= 'حفظ';

$lang['addninvoice']= 'اضافة فاتورة';



$lang['option_1']= 'البيع مباشر من المخزن';

$lang['option_2']= 'طباعة الشروط والاحكام في الفاتوره';

$lang['option_3']= 'اظهار الوصف في نموذج الطباعة';

$lang['option_4']= 'اظهار الملاحظات في نموذج الطباعة';

$lang['option_5']= 'اظهار السعر الصافي في نموذج الطباعة';

$lang['option_6']= 'اظهار مبلغ التخفيض في نموذج الطباعة';

$lang['option_7']= 'اظهار مبلغ الاجمالي في نموذج الطباعة';

$lang['option_8']= 'اظهار المدفوعات في نموذج الطباعة';

$lang['option_9']= 'اظهار الدفعات في نموذج الطباعة';



