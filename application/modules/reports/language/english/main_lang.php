<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['Invoice-Number']= 'Invoice Number';
$lang['Total-Price']= 'Total-Price';
$lang['Net-Price']= 'Net-Price';
$lang['discountamount']= 'Discount amount';
$lang['Customer']= 'Customer';
$lang['Date']= 'Date';
$lang['Status']= 'Status';
$lang['Payments']= 'Payments';
$lang['Incomplete']= 'Incomplete';
$lang['Complete']= 'Complete';
$lang['Extra']= 'Extra';
$lang['Cash']= 'Cash';
$lang['Bank']= 'Bank';
$lang['choose']= 'Choose';

$lang['Mobile-Number']= 'Mobile Number';
$lang['Contact-Number']= 'Contact Number';
$lang['Fax-Number']= 'Fax Number';
$lang['Email-Address']= 'Email Address';
$lang['Address']= 'Address';
$lang['Type']= 'Type';
$lang['Responsable-Name']= 'Responsable Name';
$lang['Responsable-Phone']= 'Responsable Phone';
$lang['Notes']= 'Notes';
$lang['Status']= 'Status';
$lang['Prev']= 'Previous';
$lang['Next']= 'Next';

$lang['Total-Purchase']= 'Total Purchase';
$lang['Total-Paid']= 'Total Paid';
$lang['Remaining']= 'Remaining';



$lang['Company-Name']= 'Company Name';
$lang['Branch-Name']= 'Branch Name';
$lang['Customer']= 'Customer';
$lang['Company']= 'Company';

$lang['Individual']= 'Individual';
$lang['Store']= 'Store';
$lang['Category']= 'Category';
$lang['Product-Name']= 'Product Name';
$lang['Quantity']= 'Quantity';
$lang['Serial-No']= 'Serial No';
$lang['Total']= 'Total';
$lang['Discount']= 'Discount';
$lang['Minimum']= 'Minimum';
$lang['Discription']= 'Discription';
$lang['Notes']= 'Notes';
$lang['Image']= 'Image';




$lang['Net']= 'Net';
$lang['Total-Price']= 'Total Price';

$lang['Net-Price']= 'Net Price';
$lang['Recieved']= 'Recieved';
$lang['Rest-of-Amount']= 'Rest of Amount';
$lang['Products-Maintenance']= 'Products-Maintenance';
$lang['Sales-Name']= 'Sales Name';
$lang['Sales-Amount']= 'Sales Amount';
$lang['Add-Sales']= 'Add Sales';


$lang['Payments']= 'Payments';
$lang['Remianing-Amount']= 'Remianing Amount';
$lang['Cash']= 'Cash';
$lang['Cheque']= 'Cheque';
$lang['Bank']= 'Bank';
$lang['Cheque-Number']= 'Cheque Number';
$lang['Period']= 'Period';
$lang['Weeks']= 'Weeks';
$lang['Months']= 'Months';
$lang['Years']= 'Years';
$lang['Payment-Numbers']= 'Payment Numbers';
$lang['Payment-Date']= 'Payment Date';
$lang['TypePayment']= 'Type';
$lang['Receipt-Number']= 'Receipt Number';
$lang['Sales-Responsable']= 'Sales Responsable';
$lang['Code']= 'Code';


$lang['Invoice']= 'Invoice';
$lang['Invoice-Date']= 'Invoice Date';

$lang['Invoice-No']= 'Invoice-No';
$lang['Date']= 'Date';
$lang['Print']= 'Print';
$lang['Sent-To']= 'Sent To';
$lang['Recieved-From']= 'Recieved-From';
$lang['Price']= 'Price';


$lang['newcustomer']= 'new customer';
$lang['customer']= 'customer ';
$lang['Deposite']= 'Deposite ';
$lang['Transfer']= 'Transfer ';


$lang['add-edit']= 'Add & Edit';
$lang['mess1']= 'Please Understand Clearly All The Data Before Entering';
$lang['Add']= 'Add';
$lang['Reset']= 'Reset';
$lang['Branch-Name']= 'Branch Name';
$lang['RePassword']= 'RePassword';
$lang['Password']= 'Password';
$lang['User-Name']= 'User Name';
$lang['Add-Account']= 'Add Account';
$lang['Company-Name']= 'Company-Name';
$lang['Customer-Name']= 'Customer Name';


$lang['Phone']= 'Phone';
$lang['Mobile']= 'Mobile';
$lang['Alternate-Price']= 'Alternate Price';
$lang['afterDiscount']= 'After Discount';


$lang['tclient']= 'Clients';
$lang['tproduct']= 'Products';
$lang['tpay']= 'Payment';
$lang['tbill']= 'Option print';


$lang['Payment-date']= 'Payment date';
$lang['Payment-amount']= 'Payment amount';
$lang['Payment-Numbers']= 'رقم الايصال';
$lang['Payment-doc']= 'Payment attachment';
$lang['Payment-note']= 'Payment note';

$lang['Add']= 'Add';
$lang['later']= 'Later';


$lang['show_datatable']= 'Showing _START_ to _END_ of _TOTAL_ entries';
$lang['Previous']= 'Previous';
$lang['First']= 'First';
$lang['Last']= 'Last';
$lang['Next']= 'Next';
$lang['Search']= 'Search';
$lang['show_bylist']= 'Show _MENU_ entries';




$lang['Invoice-Number']= ' Invoice-Number';
$lang['offer-Number']= 'Offer-Number';
$lang['Total-Price']= 'Total Price ';
$lang['Net-Price']= 'Net Price';
$lang['discountamount']= 'Discount';
$lang['Customer']= 'Customer';
$lang['typepay']= 'Type';
$lang['amountpay']= ' Pay';
$lang['remain']= 'Remaining';
$lang['NetPrice']= 'NetPrice ';
$lang['saleprice']= 'Sale price';
$lang['expences']= 'Expences';
$lang['Statusinvoice']= 'Status';
$lang['salesperson']= 'Marketer';
$lang['salesperson_percent']= 'Percent M';
$lang['add-product']= 'Add';
$lang['texpences']= 'Expenses';


$lang['expense_title']= 'Title';
$lang['expense_type']= 'Type';
$lang['expense_period']= 'Period';
$lang['value']= 'Value';
$lang['created']= 'Date';
$lang['expense_cat']= 'Category';
$lang['Fixed']= 'Fixed';
$lang['Accured']= 'Accured';
$lang['Daily']= 'Daily';
$lang['Weekly']= 'Weekly';
$lang['Monthly']= 'Monthly';
$lang['Yearly']= 'Yearly';
$lang['cat']= 'Category';

$lang['view_title']= 'View Offer Details';
$lang['no-data']= 'There are no data';
$lang['discount']= 'Discount';

$lang['Customer2']= 'Approved by ';

$lang['resetbalance']= 'Reset balance';
$lang['addinvoice']= 'Add Quotation';
$lang['next']= 'Next';
$lang['back']= 'Prev';
$lang['save']= 'Save';
$lang['addninvoice']= 'Add Invoice';
$lang['activeq']= 'Active Quotation';
$lang['option_1']= 'Direct sales from the store';
$lang['option_2']= 'Print Terms & Conditions in the invoice';
$lang['option_3']= 'Show description in the print model';
$lang['option_4']= 'The notes show in the print model';
$lang['option_5']= 'Show net price in the print model';
$lang['option_6']= 'Show the amount of the Discount in printing model';
$lang['option_7']= 'Show the total amount in the print model';
$lang['option_8']= 'Show payments in the print model';
$lang['option_9']= 'Show the payments in the print model';

