<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<html lang="ar">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Email Robots</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>emailcss/css/style.css">
    <style>
        /* CSS doc by Asif */

        /*================ Fonts ==================*/
        @font-face {
            font-family: 'helvatica';
            src:url('arfonts/helvatica.ttf') format('truetype');
            font-weight: 300;
            font-style: normal;
        }

        @font-face {
            font-family: 'helvatica';
            src:url('arfonts/helvatica.ttf') format('truetype');
            font-weight: 400;
            font-style: normal;

        }

        @font-face {
            font-family: 'helvatica';
            src:url('arfonts/helvatice.ttf') format('truetype');
            font-weight: 700;
            font-style: normal;

        }

        /*================ General ==================*/

        body {background: #dfdfdf;margin: 0; padding: 0;font-family: helvatica; font-size: 14px;}

        h1 {font-size: 24px; text-indent: 1em; margin: 0; padding:0;}
        p {font-size: 14px;}
        table {width: 100%;}
        table tr td { margin: 0; padding:0; }

        /*================ Specific ==================*/
        .gaptd {padding: .5em 1em; width: 33%; text-align: center;}
        .ar_txt {text-align: center;}
        .heading {background: none; color: #fff; padding: 5px; border: 1px solid #000;}

        /* Multi Colors */
        .greenclr {background: #fff; color: #000;}
        .greenltclr {background: #fff; color: #000;}
        .redclr {background: #fff; color: #000;}
        .greyclr {background: #fff; color: #000;}

        .greyborder {border-bottom: 1px solid #000;}

        .wrapper {width: 100%; background: #fff; margin: 0 auto; padding: 0;}
        .logo {margin: .5em auto; display: table;}

        .bslogo {width: 120px; margin-top: 1em; float: left; padding-left: 3em;}
        .footer {text-align: center;}

    </style>
</head>
<body style="background: #dfdfdf;margin: 0; padding: 0;font-family: helvatica; font-size: 14px;">
<div class="wrapper" style="background: #fff; margin: 0 auto; padding: 0;">
    <table style="width: 100%;">
        <tbody>
        <tr>
            <td><img src="<?php echo base_url() ?>durarthem/rtl/img/logo-loginprint.png" class="logo" style="margin: .5em auto; display: table; width:10%;  "></td>
        </tr>
        <tr>
            <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center; color: #000; padding: 5px;"><?php echo date('d/m/Y'); ?> </h1></td>

        </tr>

        <?php
                if(!empty($sales_report)){
                        foreach($sales_report as $sales){
                            ?>
                            <tr>
                                <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;"><?php $cname = unserialize($sales->catname); echo $cname['arabic']; ?> -<?php echo $cname['english'];  ?> </h1></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="greyclr" style="color: #000;">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Sales</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($sales->totalsales); ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                }
        ?>



        </tbody>
        </table>
    <div style="text-align: center;">

        <a href="javascript:void" onclick="saveDataInDb()" class="btn btn-default"><i class="icon-print"></i>Print</a>

    </div>

    <table><tbody>

        <tr>
            <td>
                <div class="footer" style="text-align: center;">
                    <img src="<?php echo base_url() ?>emailcss/img/logo.png" class="logo bslogo" style="margin: .5em auto; display: table;width: 64px; margin-top: 1em; float: left; padding-left: 3em;">
                    <p style="padding-top: 3em;font-size: 9px;">Thank you for using our Business Solution System.<br>
                        Developed by : <a href="http://durar-it.com/" target="_blank"><img src="<?php echo base_url() ?>emailcss/img/durar-smart-solutions.png"></a> Durar Smart Solutions L.L.C <br> <br></p>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

</div>
</body>
</html>
<script src="<?php echo base_url(); ?>durarthem/rtl/js/jquery.js"></script>
<script type="text/javascript">

    function saveDataInDb(){
        window.print();
    }
    function calculateMoney(name,m){
        //alert(m);
         val = $("#"+name+"_val").val();
        ttprice = 0;
        if(val !=""){
            result = parseInt(val)*m;
            //result
            $("#"+name+"_html").html(result.toFixed(3));

            if ($('.tt_val').length > 0) {
                len = $('.tt_val').length;
                for (a = 0; a < len; a++) {
                    tprice = $('.tt_val').eq(a).html();
                   if(tprice !=""){
                       ttprice = parseFloat(ttprice) + parseFloat(tprice);
                   }

                    //console.log(tprice + 'tprice');
                }
                $("#total_ht").html(ttprice.toFixed(3));
                $("#total").val(ttprice.toFixed(3));
            }
        }
        //console.log(obj);
    }
</script>