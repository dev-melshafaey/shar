<h3 style="text-align:center;">Sales Report Date <?php echo date('d-m-Y'); ?></h3>
<style>
    .green{
        background-color: green;
    }
    .red{
        background-color: red;

    }
    h3{
        text-align: center;
    }
</style>
<?php
//print_r($sales_report);
//echo $sales_report->sum_invoiceprice;
?>
<table  style="text-align: center" align="center" border="1">

    <thead class="thead">

    <tr>
        <th style="width: 10%;">Total Sales</th>
        <th style="width: 85px;">Amount Paid</th>
        <th style="width: 59px;">Remaining</th>
        <th style="width: 13%;">Purchase Net Profit</th>
   </tr>

    </thead>

    <tr>
        <td ><?php echo getAmountFormat($sales_report->sum_invoiceprice); ?></td>
        <td class="green"><?php echo getAmountFormat($sales_report->totalpaid); ?></td>

        <td class="red"><?php echo getAmountFormat($sales_report->sum_invoiceprice - $sales_report->totalpaid); ?></td>
        <td ><?php echo getAmountFormat($sales_report->sum_invoiceprice - $sales_report->sumipurchase); ?></td>
    </tr>

</table>
<h3>Purchase Report Date <?php echo date('d-m-Y'); ?></h3>
<table  style="text-align: center" align="center" border="1">

    <thead class="thead">

    <tr>
        <th style="width: 10%;">Total Purchase</th>
        <th style="width: 85px;">Amount Paid</th>
        <th style="width: 59px;">Remaining</th>
    </tr>

    </thead>

    <tr>
        <td ><?php echo getAmountFormat($purchase_report->sum_invoiceprice); ?></td>
        <td class="green"><?php echo getAmountFormat($purchase_report->totalpaid); ?></td>

        <td class="red"><?php echo getAmountFormat($purchase_report->sum_invoiceprice - $purchase_report->totalpaid); ?></td>
    </tr>

</table>

<h3>Expence Report Date <?php echo date('d-m-Y'); ?></h3>
<table  style="text-align: center" align="center" border="1">

    <thead class="thead">

    <tr>
        <th style="width: 85px;">Total Expences</th>
    </tr>

    </thead>

    <tr>
        <td class="green"><?php echo getAmountFormat($expences_reports->totalexpenses); ?></td>
 </tr>

</table>