<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <html lang="ar">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Email Robots</title>
            <link rel="stylesheet" href="<?php echo base_url() ?>emailcss/css/style.css">

            <?php
            if (!$last_close_report) {
                ?>
                <style>
                    .beforeprint{
                        display:none !important;
                    }

                    @media print {
                        .beforeprint{
                            display:block !important;
                        }
                    }
                    *{
                        color:#000;
                    }
                </style>

                <?php
            }
            ?>


            <style>
                /* CSS doc by Asif */






                /*================ Fonts ==================*/
                @font-face {
                    font-family: 'helvatica';
                    src:url('arfonts/helvatica.ttf') format('truetype');
                    font-weight: 300;
                    font-style: normal;
                }

                @font-face {
                    font-family: 'helvatica';
                    src:url('arfonts/helvatica.ttf') format('truetype');
                    font-weight: 400;
                    font-style: normal;

                }

                @font-face {
                    font-family: 'helvatica';
                    src:url('arfonts/helvatice.ttf') format('truetype');
                    font-weight: 700;
                    font-style: normal;

                }

                /*================ General ==================*/

                body {background: #dfdfdf;margin: 0; padding: 0;font-family: helvatica; font-size: 14px;}

                h1 {font-size: 24px; text-indent: 1em; margin: 0; padding:0;}
                p {font-size: 14px;}
                table {width: 100%;}
                table tr td { margin: 0; padding:0; }

                /*================ Specific ==================*/
                .gaptd {padding: .5em 1em; width: 33%; text-align: center;}
                .ar_txt {text-align: center;}
                .heading {background: none; color: #fff; padding: 5px; border: 1px solid #000;}

                /* Multi Colors */
                .greenclr {background: #fff; color: #000;}
                .greenltclr {background: #fff; color: #000;}
                .redclr {background: #fff; color: #000;}
                .greyclr {background: #fff; color: #000;}

                .greyborder {border-bottom: 1px solid #000;}

                .wrapper {width: 100%; background: #fff; margin: 0 auto; padding: 0;}
                .logo {margin: .5em auto; display: table;}

                .bslogo {width: 120px; margin-top: 1em; float: left; padding-left: 3em;}
                .footer {text-align: center;}

            </style>
        </head>
        <body style="background: #dfdfdf;margin: 0; padding: 0;font-family: helvatica; font-size: 14px;">
            <div class="wrapper" background: #fff; margin: 0 auto; padding: 0;">
                 <table style="width: 100%;" class="beforeprint">
                    <tbody>
                        <tr>
                            <td><img src="<?php echo base_url() ?>durarthem/rtl/img/logo-loginprint.png" class="logo" style="margin: .5em auto; display: table; width:40%;  "></td>
                        </tr>
                        <tr>
                            <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center; color: #000; padding: 5px;"><?php echo _s($branch_details->branchname, get_set_value('site_lang')); ?> - المبيعات </h1><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;color: #000; padding: 5px;"><?php echo $report_date; //date('d/m/Y H:i:s');     ?> </h1></td>

                            </tr>
                            <tr>
                                <td>
                                    <div class="greenclr">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Sales</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php
                                                    $totalsales = $sales_report->sum_invoiceprice;
                                                    echo getAmountFormat($sales_report->sum_invoiceprice);
                                                    ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المبيعات</td>
                                            </tr>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Cancel Sales Invoice</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo $cancel_reports->totalcancel; ?> (<?php echo $cancel_reports->totalcancelamount; ?>)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">الفواتير الملغية</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="greenltclr" style="display: none">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Received</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($sales_report->totalpaid); ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المدفوعات</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="redclr" style="display: none">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Remaining</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($sales_report->sum_invoiceprice - $sales_report->totalpaid); ?>(OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المبيعات الآجلة</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="" style="display: none">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Net Sales</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($sales_report->sum_invoiceprice - $sales_report->sumipurchase); ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">صافي المبيعات</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr >
                                <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Purchase - المشتريات </h1></td>
                            </tr>
                            <tr >
                                <td>
                                    <div class="greyclr greyborder" style="color: #000;border-bottom: 1px solid #000;">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Purchase</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($purchase_report->sum_invoiceprice); ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المشتريات</td>
                                            </tr>
                                        </table>
                                    </div>
                                <div class="greyclr greyborder" style="color: #000;border-bottom: 1px solid #000;">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Paid</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($purchase_report->totalpaid); ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المدفوعات</td>
                                            </tr>
                                        </table>
                                    </div>
                                <div class="greyclr greyborder" style="color: #000;border-bottom: 1px solid #000;">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Remaining</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($purchase_report->sum_invoiceprice - $purchase_report->totalpaid); ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المشتريات الآجلة</td>
                                            </tr>
                                        </table>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Card Sales - اجمالي المبيعات عن طريق البطاقات البنكيه </h1></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="greyclr" style="color: #000;">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Payment</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($card_sales_report->sum_invoiceprice); ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Discount - الخصومات </h1></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="greyclr" style="color: #000;">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Payment</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($discount_sales_report->sum_discount); ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Coupons - الكوبونات </h1></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="greyclr" style="color: #000;">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Payment</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($discount_sales_report->coupon_discount); ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Received Amount - اجمالى النقود المستلمة </h1></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="greyclr" style="color: #000;">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Payment</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($clear_reports->totalclearamount); ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                            <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Received Extra Amount/ lost -  مجموع ما تلقاه المبلغ إضافي \ العجز </h1></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="greyclr" style="color: #000;">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Payment</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php
                                                    $extra = $clear_reports->totalclearamount - $sales_report->sum_invoiceprice;
                                                if ($extra) {
                                                        echo getAmountFormat($extra);
                                                } else {
                                                    echo "0";
                                                }
                                                ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Discount Amount  -  اجمالي الخصومات </h1></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="greyclr" style="color: #000;">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Payment</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php
                                                if ($discount_reports->totaldiscount > 0) {
                                                    echo getAmountFormat($discount_reports->totaldiscount);
                                                } else {
                                                    echo "0";
                                                }
                                                    ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;color: #000; padding: 5px;">Total Expence - إجمالى المصروفات </h1></td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="greyclr">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Expence</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php
                                                    $totalexpence = $expences_reports->totalexpenses;
                                                    echo getAmountFormat($expences_reports->totalexpenses);
                                                    ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المصروفات</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td>
                                    <div class="greyclr" style="color: #000;">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Paid Expence</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($expences_reports->totalpaidexpenses); ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المصروفات المدفوعة</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td>
                                    <div class="greyclr" style="color: #000;">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total UnPaid Expence</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($expences_reports->totalunpaidexpenses); ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المصروفات الآجلة</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Income - اجمالى الدخل </h1></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="greyclr" style="color: #000;">
                                        <table>
                                            <tr>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Amount</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php
                                                $totalincome = $clear_reports->totalclearamount - $totalexpence;
                                                    echo getAmountFormat($totalincome);
                                                    ?> (OMR)</td>
                                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <?php // if ($last_close_report) {  ?>
                            <tr>
                                <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Employee Sales - مبيعات الموظفين </h1></td>
                            </tr>
                            <?php
                            if (!empty($sales_users_reports)) {
                                foreach ($sales_users_reports as $sales_users) {
                                    ?>
                                    <tr>
                                        <td>
                                            <div class="greyclr" style="color: #000;">
                                                <table>
                                                    <tr>
                                                        <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo $sales_users->fullname; ?></td>
                                                        <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($sales_users->totalinvamount); ?> (OMR)</td>
                                                        <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"> <?php
                                                            $searched = array('user_id' => $sales_users->userid);
                                                            $result = multidimensional_search_array($clear_by_user, $searched);
                                                            $ind = $result[0];
                                                            echo $clear_by_user[$ind]['totalclearamount']; //print_r();  
                                                            ?> (OMR) Received</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>

                                    <?php
                                }
                            }
                        
                        ?>

                    </tbody>
                </table>
                <form  id="daily_cash_data">
                    <table style="text-align: center">
                        <tr>
                            <td colspan="3"><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;color: #000; padding: 5px;">نقدا - cash </h1></td>
                        </tr>
                        <?php
                        if (!$last_close_report) {
                            ?>
                            <tr><th>النوع - type</th><th>الكمية - QTY</th><th>total - المجموع</th></tr>
                            <tr><td>50</td><td><input type="text" id="fifty_val" name="fifty_val" onkeyup="calculateMoney('fifty', '50')"></td><td class="tt_val" id="fifty_html"></td></tr>
                            <tr><td>20</td><td><input type="text" id="twenty_val" name="twenty_val" onkeyup="calculateMoney('twenty', '20')"></td><td class="tt_val" id="twenty_html"></td></tr>
                            <tr><td>10</td><td><input type="text" id="ten_val" name="ten_val" onkeyup="calculateMoney('ten', '10')"></td><td class="tt_val" id="ten_html"></td></tr>
                            <tr><td>5</td><td><input type="text" id="five_val" name="five_val" onkeyup="calculateMoney('five', '5')"></td><td class="tt_val" id="five_html"></td></tr>
                            <tr><td>1</td><td><input type="text" id="one_val" name="one_val" onkeyup="calculateMoney('one', '1')"></td><td class="tt_val" id="one_html"></td></tr>
                            <tr><td>0.500</td><td><input type="text" id="fivehundred_paisa_val" name="fivehundred_paisa_val" onkeyup="calculateMoney('fivehundred_paisa', '0.500')"></td><td class="tt_val" id="fivehundred_paisa_html"></td></tr>
                            <tr><td>0.100</td><td><input type="text" id="hundred_paisa_val" name="hundred_paisa_val" onkeyup="calculateMoney('hundred_paisa', '0.100')"></td><td class="tt_val" id="hundred_paisa_html"></td></tr>
                            <tr><td>0.050</td><td><input type="text" id="fifty_paisa_val" name="fifty_paisa_val" onkeyup=" calculateMoney('fifty_paisa', '0.050')"></td><td class="tt_val" id="fifty_paisa_html"></td></tr>
                            <!--<tr><td>0.025</td><td><input type="text" id="twentyfive_paisa_val" name="twentyfive_paisa_val" onkeyup="calculateMoney('twentyfive_paisa', '0.025')"></td><td class="tt_val" id="twentyfive_paisa_html"></td></tr>
                            <tr><td>0.010</td><td><input type="text" id="ten_paisa_val" name="ten_paisa_val" onkeyup="calculateMoney('ten_paisa', '0.010')"></td><td class="tt_val" id="ten_paisa_html"></td></tr>
                            <tr><td>0.005</td><td><input type="text" id="five_paisa_val" name="five_paisa_val" onkeyup="calculateMoney('five_paisa', '0.005')"></td><td class="tt_val" id="five_paisa_html"></td></tr>-->
                            <tr><td>Total </td><td></td><td id="total_ht"></td><input type="hidden" id="total" name="total"></tr>

                            <?php
                        }
                        ?>


                    </table>
                    <?php
                    if (!$last_close_report) {
                        ?>
                        <table style="text-align: center" class="beforeprint">

                            <tr>
                                <td colspan="3"><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;color: #000; padding: 5px;">Final </h1></td>
                            </tr>

                            <table  class="beforeprint">
                                <tr>
                                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Amount</td>
                                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;" id="total_rht"><?php //echo getAmountFormat($last_close_report->closing_amount - $totalincome);    ?> (OMR)</td>
                                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                </tr>
                            </table>


                        </table>

                        <?php
                    }
                    ?>

                </form>
                <?php
                if (!$last_close_report) {
                    ?>

                    <div style="text-align: center;">

                        <a href="javascript:void" onclick="saveDataInDb()" class="btn btn-default"><i class="icon-print"></i>Print</a>

                    </div>
                    <?php
                } else {
                    ?>
                    <div style="text-align: center;">

                        <?php echo $last_close_report->closing_amount; ?>

                    </div>

                    <table style="text-align: center">

                        <tr>
                            <td colspan="3"><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;color: #000; padding: 5px;">Final </h1></td>
                        </tr>

                        <table>
                            <tr>
                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Amount</td>
                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($last_close_report->closing_amount - $totalincome); ?> (OMR)</td>
                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                            </tr>
                        </table>


                    </table>
                    <?php
                }
                ?>

                <table><tbody>

                        <tr>
                            <td>
                                <div class="footer" style="text-align: center;">
                                    <img src="<?php echo base_url() ?>emailcss/img/logo.png" class="logo bslogo" style="margin: .5em auto; display: table;width: 64px; margin-top: 1em; float: left; padding-left: 3em;">
                                    <p style="padding-top: 3em;font-size: 9px;">Thank you for using our Business Solution System.<br>
                                        Developed by : <a href="http://durar-it.com/" target="_blank"><img src="<?php echo base_url() ?>emailcss/img/durar-smart-solutions.png"></a> Durar Smart Solutions L.L.C <br> <br></p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </body>
    </html>
    <script src="<?php echo base_url(); ?>durarthem/rtl/js/jquery.js"></script>
    <script type="text/javascript">

                        totalinc = '<?php echo $totalincome; ?>';
                        function saveDataInDb() {
                            formData = $("#daily_cash_data").serialize();
                            $.ajax({
                                url: "<?php echo base_url(); ?>reports/save_cah_report",
                                type: "POST",
                                data: formData,
                                success: function (response) {
                                    //$('#storequantity').val(e);
                                    //getProductData(pid, '', val);
                                    // alert('closing Data Save Successfully');
                                    window.top.location = '<?php echo base_url(); ?>reports/viewDailyReport';
                                }
                            });
                            window.print();
                        }
                        function calculateMoney(name, m) {
                            //alert(m);
                            val = $("#" + name + "_val").val();
                            ttprice = 0;
                            if (val != "") {
                                result = parseInt(val) * m;
                                //result
                                $("#" + name + "_html").html(result.toFixed(3));

                                if ($('.tt_val').length > 0) {
                                    len = $('.tt_val').length;
                                    for (a = 0; a < len; a++) {
                                        tprice = $('.tt_val').eq(a).html();
                                        if (tprice != "") {
                                            ttprice = parseFloat(ttprice) + parseFloat(tprice);
                                        }

                                        //console.log(tprice + 'tprice');
                                    }
                                    $("#total_ht").html(ttprice.toFixed(3));
                                    $("#total").val(ttprice.toFixed(3));

                                    fnl = ttprice - totalinc;
                                    $("#total_rht").html(fnl.toFixed(3));
                                }
                            }
                            //console.log(obj);
                        }
    </script>