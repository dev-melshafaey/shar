<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <html lang="ar">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Email Robots</title>
            <link rel="stylesheet" href="<?php echo base_url() ?>emailcss/css/style.css">

            <style>
                .beforeprint{
                    display:none !important;
                }

                @media print {
                }
            </style>



            <style>
                /* CSS doc by Asif */






                /*================ Fonts ==================*/
                @font-face {
                    font-family: 'helvatica';
                    src:url('arfonts/helvatica.ttf') format('truetype');
                    font-weight: 300;
                    font-style: normal;
                }

                @font-face {
                    font-family: 'helvatica';
                    src:url('arfonts/helvatica.ttf') format('truetype');
                    font-weight: 400;
                    font-style: normal;

                }

                @font-face {
                    font-family: 'helvatica';
                    src:url('arfonts/helvatice.ttf') format('truetype');
                    font-weight: 700;
                    font-style: normal;

                }

                /*================ General ==================*/

                body {background: #dfdfdf;margin: 0; padding: 0;font-family: helvatica; font-size: 14px;}

                h1 {font-size: 24px; text-indent: 1em; margin: 0; padding:0;}
                p {font-size: 14px;}
                table {width: 100%;}
                table tr td { margin: 0; padding:0; }

                /*================ Specific ==================*/
                .gaptd {padding: .5em 1em; width: 33%; text-align: center;}
                .ar_txt {text-align: center;}
                .heading {background: none; color: #fff; padding: 5px; border: 1px solid #000;}

                /* Multi Colors */
                .greenclr {background: #fff; color: #000;}
                .greenltclr {background: #fff; color: #000;}
                .redclr {background: #fff; color: #000;}
                .greyclr {background: #fff; color: #000;}

                .greyborder {border-bottom: 1px solid #000;}

                .wrapper {width: 100%; background: #fff; margin: 0 auto; padding: 0;}
                .logo {margin: .5em auto; display: table;}

                .bslogo {width: 120px; margin-top: 1em; float: left; padding-left: 3em;}
                .footer {text-align: center;}

                .watermark {
                    opacity: 0.5;
                    color: BLACK;
                }
            </style>
        </head>
        <body style="background: #dfdfdf;margin: 0; padding: 0;font-family: helvatica; font-size: 14px;">
            <div class="wrapper" style="background: #fff; margin: 0 auto; padding: 0;">
                <button class="btn-glow primary  submit_btn g3 green" style="margin: 10px;background: #ccc;border: 1px solid #000;padding: 5px 20px;" onclick="window.print()" >Print</button>
                <table style="width: 100%;" class="">
                    <tbody>
                        <tr>
                            <td><img src="<?php echo base_url() ?>durarthem/rtl/img/logo-loginprint.png" class="logo" style="margin: .5em auto; display: table; width:10%;  "></td>
                        </tr>
                        <tr>
                            <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;color: #000; padding: 5px;"><div class="watermark">Second Copy</div><?php echo _s($branch_details->branchname, get_set_value('site_lang')); ?> - االمبيعات </h1><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center; color: #000; padding: 5px;"><?php echo date('d/m/Y h:i:s', strtotime($report_date)); ?> </h1></td>

                        </tr>
                        <tr>
                            <td>
                                <div class="greenclr">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Sales</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php $totalsales = $sales_report->sum_invoiceprice;
echo getAmountFormat($sales_report->sum_invoiceprice); ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المبيعات</td>
                                        </tr>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Cancel Sales Invoice</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo $cancel_reports->totalcancel; ?> (<?php echo $cancel_reports->totalcancelamount; ?>)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">الفواتير الملغية</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="greenltclr" style="display: none">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Received</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($sales_report->totalpaid); ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المدفوعات</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="redclr" style="display: none">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Remaining</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($sales_report->sum_invoiceprice - $sales_report->totalpaid); ?>(OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المبيعات الآجلة</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="" style="display: none">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Net Sales</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($sales_report->sum_invoiceprice - $sales_report->sumipurchase); ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">صافي المبيعات</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Purchase - المشتريات </h1></td>
                        </tr>
                        <tr style="display: none">
                            <td>
                                <div class="greyclr greyborder" style="color: #000;border-bottom: 1px solid #000;">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Purchase</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($purchase_report->sum_invoiceprice); ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المشتريات</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="greyclr greyborder" style="color: #000;border-bottom: 1px solid #000;">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Paid</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($purchase_report->totalpaid); ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المدفوعات</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="greyclr greyborder" style="color: #000;border-bottom: 1px solid #000;">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Remaining</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($purchase_report->sum_invoiceprice - $purchase_report->totalpaid); ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المشتريات الآجلة</td>
                                        </tr>
                                    </table>
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Received Amount - اجمالى النقود المستلمة </h1></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="greyclr" style="color: #000;">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Payment</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($clear_reports->totalclearamount); ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Received Extra Amount/ lost -  مجموع ما تلقاه المبلغ إضافي \ العجز </h1></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="greyclr" style="color: #000;">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Payment</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php $extra = $clear_reports->totalclearamount - $sales_report->sum_invoiceprice;
if ($extra) {
    echo getAmountFormat($extra);
} else {
    echo "0";
} ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Discount Amount  -  اجمالي الخصومات </h1></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="greyclr" style="color: #000;">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Payment</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php if ($discount_reports->totaldiscount > 0) {
    echo getAmountFormat($discount_reports->totaldiscount);
} else {
    echo "0";
} ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Coupons Amount  -  اجمالي الكوبونات </h1></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="greyclr" style="color: #000;">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Payment</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php if ($discount_reports->totalcoupon > 0) {
    echo getAmountFormat($discount_reports->totalcoupon);
} else {
    echo "0";
} ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;color: #000; padding: 5px;">Total Expence - إجمالى المصروفات </h1></td>
                        </tr>

                        <tr>
                            <td>
                                <div class="greyclr">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Expence</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php $totalexpence = $expences_reports->totalexpenses;
echo getAmountFormat($expences_reports->totalexpenses); ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المصروفات</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td>
                                <div class="greyclr" style="color: #000;">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Paid Expence</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($expences_reports->totalpaidexpenses); ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المصروفات المدفوعة</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td>
                                <div class="greyclr" style="color: #000;">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total UnPaid Expence</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($expences_reports->totalunpaidexpenses); ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">إجمالي المصروفات الآجلة</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Income - اجمالى الدخل </h1></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="greyclr" style="color: #000;">
                                    <table>
                                        <tr>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Amount</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php $totalincome = $clear_reports->totalclearamount - $totalexpence;
echo getAmountFormat($totalincome); ?> (OMR)</td>
                                            <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;background: none; color: #000; padding: 5px;">Total Employee Sales - مبيعات الموظفين </h1></td>
                        </tr>
<?php
if (!empty($sales_users_reports)) {
    // print_r($clear_by_user);
    foreach ($sales_users_reports as $sales_users) {
        //  print_r($sales_users);
        ?>
                                <tr>
                                    <td>
                                        <div class="greyclr" style="color: #000;">
                                            <table>
                                                <tr>
                                                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo $sales_users->fullname; ?></td>
                                                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($sales_users->totalinvamount); ?> (OMR)</td>
                                                    <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"> <?php $searched = array('user_id' => $sales_users->userid);
                        $result = multidimensional_search_array($clear_by_user, $searched);
                        $ind = $result[0];
                        echo $clear_by_user[$ind]['totalclearamount']; //print_r();  ?> (OMR) Received</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>

        <?php
    }
}
?>

                    </tbody>
                </table>
                <form  id="daily_cash_data">
                    <table style="text-align: center">
                        <tr>
                            <td colspan="3"><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;color: #000; padding: 5px;">نقدا - cash </h1></td>
                        </tr>

                    </table>
                </form>
<?php
if (!$last_close_report) {
    ?>

                    <div style="text-align: center;">

                        0 OMR

                    </div>
                    <table style="text-align: center">

                        <tr>
                            <td colspan="3"><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;color: #000; padding: 5px;">Final </h1></td>
                        </tr>

                        <table>
                            <tr>
                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Amount</td>
                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($last_close_report->closing_amount - $totalincome); ?> (OMR)</td>
                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                            </tr>
                        </table>


                    </table>

    <?php
} else {
    ?>
                    <div style="text-align: center;">

                    <?php echo $last_close_report->closing_amount; ?>

                    </div>
                    <table style="text-align: center">

                        <tr>
                            <td colspan="3"><h1 class="ar_txt heading" style="font-size: 24px; text-indent: 1em; margin: 0; padding:0;text-align: center;color: #000; padding: 5px;">Final </h1></td>
                        </tr>

                        <table>
                            <tr>
                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">Total Amount</td>
                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;"><?php echo getAmountFormat($last_close_report->closing_amount - $totalincome); ?> (OMR)</td>
                                <td class="gaptd" style="margin: 0; padding:0;padding: .5em 1em; width: 33%; text-align: center;">المبلغ الإجمالي</td>
                            </tr>
                        </table>


                    </table>
    <?php
}
?>

                <table><tbody>

                        <tr>
                            <td>
                                <div class="footer" style="text-align: center;">
                                    <img src="<?php echo base_url() ?>emailcss/img/logo.png" class="logo bslogo" style="margin: .5em auto; display: table;width: 64px; margin-top: 1em; float: left; padding-left: 3em;">
                                    <p style="padding-top: 3em;font-size: 9px;">Thank you for using our Business Solution System.<br>
                                        Developed by : <a href="http://durar-it.com/" target="_blank"><img src="<?php echo base_url() ?>emailcss/img/durar-smart-solutions.png"></a> Durar Smart Solutions L.L.C <br> <br></p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </body>
    </html>
    <script src="<?php echo base_url(); ?>durarthem/rtl/js/jquery.js"></script>
    <script type="text/javascript">

            /*$(document).ready(function () {
             window.print();
             });*/
            totalinc = '<?php echo $totalincome; ?>';
            function saveDataInDb() {
                formData = $("#daily_cash_data").serialize();
                $.ajax({
                    url: "<?php echo base_url(); ?>reports/save_cah_report",
                    type: "POST",
                    data: formData,
                    success: function (response) {
                        //$('#storequantity').val(e);
                        //getProductData(pid, '', val);
                        // alert('closing Data Save Successfully');
                        window.top.location = '<?php echo base_url(); ?>reports/viewDailyReport';
                    }
                });
                window.print();
            }
            function calculateMoney(name, m) {
                //alert(m);
                val = $("#" + name + "_val").val();
                ttprice = 0;
                if (val != "") {
                    result = parseInt(val) * m;
                    //result
                    $("#" + name + "_html").html(result.toFixed(3));

                    if ($('.tt_val').length > 0) {
                        len = $('.tt_val').length;
                        for (a = 0; a < len; a++) {
                            tprice = $('.tt_val').eq(a).html();
                            if (tprice != "") {
                                ttprice = parseFloat(ttprice) + parseFloat(tprice);
                            }

                            //console.log(tprice + 'tprice');
                        }
                        $("#total_ht").html(ttprice.toFixed(3));
                        $("#total").val(ttprice.toFixed(3));
                    }
                }
                //console.log(obj);
            }
    </script>