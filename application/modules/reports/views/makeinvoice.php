<?php $row = rand(33, 800) ?>
<style>
    .ui-autocomplete {
        height: 120px !important;
        overflow: scroll !important;
    }
</style>
<script type="text/javascript">
    function swapme2p() {
        pid = $("#productid").val();
        pname = $("#productname2").val();
        $("#productname").val(pname);
        $("#productid").val(pid);
    }
    function swapmeu() {

        pid = $("#customerid").val();
        pname = $("#customername2").val();
        $("#customername").val(pname);
        $("#customerid").val(pid);
        $("#customerid-val").val(pname);
    }
    $(document).ready(function () {
        //	alert('asd');

        $("#category_id").change(function () {
            val = $(this).val();
            //alert(val);
            $("#productname").trigger('keydown');
        });
        setInterval('checkData()', 1000);
        storeId = $("#store_id").val();
        /*var ac_config = {
         source: "<?php echo base_url(); ?>ajax/getAutoSearchProducts?store_id=",
         select: function(event, ui){
         $("#productname").val(ui.item.id);
         $("#productid").val(ui.item.prod);
         getProductData(ui.item.id);
         console.log(ui);
         //swapme();
         setTimeout('swapme2()',500);
         },
         minLength:1
         };
         
         $("#productname").focusin(function(){
         //alert('asd');
         storeId = $("#store_id").val();
         
         });*/
        //$("#productname").autocomplete(ac_config);

        $("#productname").autocomplete({
            source: function (request, response) {
                $.getJSON("<?php echo base_url(); ?>ajax/getAutoSearchProducts", {category: $('#category_id').val(), term: $('#productname').val()},
                response);
            },
            minLength: 0,
            select: function (event, ui) {
                //action
                $("#productname").val(ui.item.prod);
                $("#productname2").val(ui.item.prod);
                $("#productid").val(ui.item.id);
                getProductData(ui.item.id);
                console.log(ui);
                //swapme();
                setTimeout('swapme2p()', 500);
            }
        });

        $("#productname").focus(function () {
            $("#productname").trigger('keyup');
        });
        var ac_config = {
            source: "<?php echo base_url(); ?>ajax/getAutoSearchCustomer",
            select: function (event, ui) {
                $("#customerid").val(ui.item.id);
                $("#customername").val(ui.item.cus);
                $("#customername2").val(ui.item.cus);
                $("#customer_ftotal").val(ui.item.buywithtotal);
                console.log(ui);
                //swapme();
                setTimeout('swapmeu()', 500);
                if (ui.item.blacklist === "1") {

                    $(".viewmess").html(ui.item.reson_blacklist);
                    $(".get_balance").html("");
                    $(".main_viewmess").css("display", "block");
                    $(".submit_btn").hide();
                } else {
                    $.ajax({
                        url: "<?php echo base_url() ?>sales_management/get_balance",
                        type: "POST",
                        data: {val: ui.item.id},
                        success: function (e) {

                            $('.get_balance').html(e);
                        }
                    });
                    $(".main_viewmess").css("display", "none");
                    $(".submit_btn").show();
                }

            },
            minLength: 1
        };
        $("#customername").autocomplete(ac_config);



        $('select[name=store_id]').change(function () {
            var val = $(this).val();
            var pid = $("#productid").val();
            if (val != 0) {
                $.ajax({
                    url: "<?php echo base_url() ?>sales_management/get_store_quantity",
                    type: "POST",
                    data: {val: val, pid: pid},
                    success: function (e) {

                        $('#storequantity').val(e);
                        getProductData(pid, '', val);
                    }
                });
            } else {
                $('#storequantity').val(0);
                getProductData(pid, '', val);
            }

        });

    });
    function checkData() {
        plen = allProductsData.length;
        if (plen > 0) {
            $('#customername').attr("disabled", true);
        }
        else {
            $('#customername').attr("disabled", false);
        }
    }





    $(document).ready(function () {
        $('#Alternate_Price').click(function () {
            if ($("#Alternate_Price:checked").val() == 1) {

                $('#Alternate_Price_input').css('display', 'block');
                //$('#item_total_price').css('display', 'block');
                //$('#item_total_price').attr('disabled', true);
                $('.item_total_price0').attr('id', 'item_total_price2');
                $('.item_total_price0').attr('name', 'item_total_price2');
                $('.item_total_price_old').attr('id', 'item_total_price');
                $('.item_total_price_old').attr('name', 'item_total_price');
            } else {
                $('#Alternate_Price_input').css('display', 'none');
                //$('#item_total_price').removeAttr('disabled');

                $('.item_total_price0').attr('id', 'item_total_price');
                $('.item_total_price0').attr('name', 'item_total_price');
                $('.item_total_price_old').attr('id', 'item_total_price_old');
                $('.item_total_price_old').attr('name', 'item_total_price_old');
            }

            $("#item_total_price").change(function () {
                //$(this).val()
                $('#total_price').val($(this).val());
                //alert($(this).val());

            });
        });
        $('#is_payment').click(function () {
            if ($("#is_payment:checked").val() == 1) {
                $('.payment_div').css('display', 'block');
            }
        });
        $('#customerType').change(function () {
            if ($(this).val() == 'newcustomer') {

                $('#newcustomer').css('display', 'block');
                $('#customer').css('display', 'none');
                //$('.payment_div').css('display', 'block');

            } else {
                $('#customer').css('display', 'block');
                $('#newcustomer').css('display', 'none');
            }
        });



        $('.go_to_nexttabs').click(function () {
            //$( "input[value='Hot Fuzz']" ).next().text( "Hot Fuzz" );

            $("div[aria-labelledby|='tab_item-0']").css("display", "none");
            $("li[aria-controls|='tab_item-0']").removeClass("resp-tab-item resp-tab-active");
            $("li[aria-controls|='tab_item-0']").addClass("resp-tab-item");
            //$( "h2[aria-controls|='tab_item-0']" ).css( "display", "none" );

            $("div[aria-labelledby|='tab_item-1']").css("display", "block");
            $("li[aria-controls|='tab_item-1']").addClass("resp-tab-item resp-tab-active");
            //$( "h2[aria-controls|='tab_item-0']" ).css( "display", "block" );


        });
        $('.go_to_nexttabs2').click(function () {
            //$( "input[value='Hot Fuzz']" ).next().text( "Hot Fuzz" );

            $("div[aria-labelledby|='tab_item-1']").css("display", "none");
            $("li[aria-controls|='tab_item-1']").removeClass("resp-tab-item resp-tab-active");
            $("li[aria-controls|='tab_item-1']").addClass("resp-tab-item");
            //$( "h2[aria-controls|='tab_item-0']" ).css( "display", "none" );

            $("div[aria-labelledby|='tab_item-2']").css("display", "block");
            $("li[aria-controls|='tab_item-2']").addClass("resp-tab-item resp-tab-active");
            //$( "h2[aria-controls|='tab_item-0']" ).css( "display", "block" );


        });
        $('.go_to_nexttabs3').click(function () {
            //$( "input[value='Hot Fuzz']" ).next().text( "Hot Fuzz" );

            $("div[aria-labelledby|='tab_item-2']").css("display", "none");
            $("li[aria-controls|='tab_item-2']").removeClass("resp-tab-item resp-tab-active");
            $("li[aria-controls|='tab_item-2']").addClass("resp-tab-item");
            //$( "h2[aria-controls|='tab_item-0']" ).css( "display", "none" );

            $("div[aria-labelledby|='tab_item-3']").css("display", "block");
            $("li[aria-controls|='tab_item-3']").addClass("resp-tab-item resp-tab-active");
            //$( "h2[aria-controls|='tab_item-0']" ).css( "display", "block" );


        });



        $('#payment_amount').change(function () {
            //$("#remin_view_all_total").html($("#remin_view_all_total").html()+$("#payment_amount").html());
            $("#remin_view_all_total").html(parseInt($("#remin_view_all_total").html()) - parseInt($('#payment_amount').val()));

            $("#recievedamount").val(parseInt($("#payment_amount").val()));
            $("#totalrecievedamount").val(parseInt($('#payment_amount').val()));
            //alert($("#howpay").val());
            ppshow = parseInt($("#get_my_allaccount").html()) - parseInt($('#payment_amount').val());
            ppshow2 = parseInt($("#view_all_total").html()) - parseInt($('#payment_amount').val());
            if ($("#howpay").val() == "1") {

                if (parseInt($("#remin_view_all_total").html()) < 0) {
                    $("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) - parseInt($('#view_all_total').html()));
                    $("#recievedamount").val(parseInt($('#view_all_total').html()));
                    $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                    $("#remin_view_all_total").text("0");
                } else {
                    $("#get_my_allaccount").text(ppshow);
                }

            } else {

                if (parseInt($("#remin_view_all_total").html()) < 0) {

                    //alert('if');
                    $("#increse_amount_span").text(Math.abs(ppshow2));
                    $("#recievedamount").val(parseInt($('#view_all_total').html()));
                    $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                    $("#remin_view_all_total").text("0");
                    $("#increse_amount").show();
                } else {
                    //alert('else');
                    //$("#increse_amount_span").text(Math.abs(ppshow2));
                    $("#remin_view_all_total").text(ppshow2);

                }
            }
        });



        //        $('#store_id').change(function () {
        //            var val = $(this).val();
        //            var pid = $("#productid").val();
        //
        //            $.ajax({
        //                url: "<?php echo base_url() ?>sales_management/get_store_quantity",
        //                type: "POST",
        //                data: {val: val, pid: pid},
        //                success: function (e) {
        //
        //                    $('#storequantity').val(e);
        //                }
        //            });
        //
        //        });



    });
    function rest_payment() {

        $('#get_my_allaccount').html("0");
        $('#howpay').val("");
        $('#recievedamount').val("");
        $('#remin_view_all_total').html($('#view_all_total').html());
        $('#increse_amount').hide("");
        $('#payment_amount').val("");
        $("#add_new_payment_btn").show();
    }
    function  do_payment_amountt(e, id) {
        //$("#remin_view_all_total").html($("#remin_view_all_total").html()+$("#payment_amount").html());
        //alert(e);
        $("#remin_view_all_total").html(parseInt($("#remin_view_all_total").html()) - parseInt(e));
        $("#recievedamount").val(parseInt($("#remin_view_all_total").html()));
        $("#totalrecievedamount").val(parseInt($("#totalrecievedamount").val()) + parseInt(e));

        var remin_view = parseInt($("#remin_view_all_total").html());
        var dopshow = parseInt($("#get_my_allaccount").html()) - parseInt(e);
        var dopshow2 = remin_view - parseInt(e);

        if ($("#howpay").val() == "1") {


            //alert(remin_view);
            if (remin_view < 0) {
                $("#get_my_allaccount").text(Math.abs(dopshow));
                $("#recievedamount").val(parseInt($('#view_all_total').html()));
                $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                $("#remin_view_all_total").text("0");
                $("#payment_amountt_" + id).val(Math.abs(parseInt($('#increse_amount_span').html()) - parseInt($("#payment_amountt_" + id).val())));

                $("#add_new_payment_btn").remove();

            } else {
                $("#get_my_allaccount").text(dopshow);
            }


        } else {

            //alert(remin_view);
            //$("#remin_view_all_total").text();
            if (remin_view < 0) {
                //alert(dopshow2);
                $("#increse_amount_span").text(Math.abs(remin_view));
                $("#increse_amount").show();
                $("#recievedamount").val(parseInt($('#view_all_total').html()));
                $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                $("#remin_view_all_total").text("0");
                $("#payment_amountt_" + id).val(Math.abs(parseInt($('#increse_amount_span').html()) - parseInt($("#payment_amountt_" + id).val())));

                $("#add_new_payment_btn").hide();

            } else {
                /*
                 $("#recievedamount").val(parseInt($('#view_all_total').html()));
                 $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                 $("#remin_view_all_total").text("0");
                 */

            }
        }



    }

</script>


<script>

    $(document).ready(function () {



        $(".get_balance").load(config.BASE_URL + "sales_management/get_balance/" + <?php echo $invoice[0]->customer_id ?>);


        $.ajax({
            url: config.BASE_URL + "ajax/branch_list",
            type: 'post',
            data: {companyid: "<?php echo $invoice[0]->companyid ?>", branch: "<?php echo $invoice[0]->branchid ?>"},
            cache: false,
            //dataType:"json",
            success: function (data)
            {
                var response = $.parseJSON(data);

                $('#branchid').html(response.dropdown);
                $('#storeid').html(response.store);
                $('#customerid').html(response.customer);
            }
        });





    });

</script>
<div class="table-wrapper users-table section" style="margin-top:0px" >
    <div class="row head">
        <div class="">
            <h4>
                <div class="title"> <span>
                        <?php breadcramb(); ?>
                    </span> </div>
            </h4>
            <?php error_hander($this->input->get('e')); ?>
        </div>
    </div>
    <style>

    </style>
    <div class="row">
        <div class="" style=""> 

<!--<script src="http://www.unverse.net/whizzery/whizzywig.js"></script>--> 

            <!--Section-->
            <div id="show_message"></div>
            <div id="main-content" class="main_content" > 
              <!--<div class="title"> <span><?php breadcramb(); ?></span> </div>-->
                <form action="<?php echo base_url() ?>quotation/makeinvoice" method="POST" id="frm_invoice" name="frm_invoice" autocomplete="off">
                    <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
                    <input type="hidden" name="userid" id="userid" value="<?php echo $udata->userid; ?>" />
                    <input type="hidden"  name="invoiceData" id="invoiceData" value=""/>
                    <input type="hidden"  name="purchase" id="invoiceData" value="0"/>
                    <input type="hidden"  name="qid" id="qid" value="<?php echo $invoice[0]->quotation_id ?>"/>

                    <div class="invoice_raw_product_items">

                        <?php if ($invoice): ?>
                            <?php $count = 1 ?>
                            <?php foreach ($invoice as $product): ?>

                                <div id='invoice_input_<?php echo $row ?>'> <input type='hidden' class='pi<?php echo $row ?>' id='productId<?php echo $row ?>'  name='items[productId][]' value='<?php echo $product->quotation_product_id ?>' />
                                    <input type='hidden' class='pi<?php echo $row ?>' id='productQuantity<?php echo $row ?>'  name='items[quantity][]' value='<?php echo $product->quotation_item_quantity ?>' />
                                    <input type='hidden' class='pi<?php echo $row ?>' id='productTotal<?php echo $row ?>'  name='items[productTotal][]' value='<?php echo $product->quotation_item_price ?>' />
                                    <input type='hidden' class='pi<?php echo $row ?>' id='minimum<?php echo $row ?>'  name='items[minimum][]' value=''>
                                    <input type='hidden' class='pi<?php echo $row ?>' id='productPerPrice<?php echo $row ?>'  name='items[productperPrice][]' value='<?php echo $product->sale_price ?>' />
                                    <input type='hidden' class='pi<?php echo $row ?>' id='productDiscount<?php echo $row ?>'  name='items[productDiscount][]' value='<?php echo $product->quotation_item_discount ?>' />
                                    <input type='hidden' class='pi<?php echo $row ?>' id='productDiscountType<?php echo $row ?>'  name='items[productDiscountType][]' value='<?php echo $product->quotation_item_discount_type ?>' />
                                    <input type='hidden' class='pi<?php echo $row ?>' id='productDiscription<?php echo $row ?>'  name='items[productDiscription][]' value='<?php echo $product->quotation_item_notes ?>' />
                                    <input type='hidden' class='pi<?php echo $row ?>' id='productNotes<?php echo $row ?>'  name='items[productNotes][]' value='<?php echo $product->quotation_item_notes ?>' />
                                    <input type='hidden' class='pi<?php echo $row ?>' id='supplier<?php echo $row ?>'  name='items[supplier_id][]' value='' />
                                    <input type='hidden' class='pi<?php echo $row ?>' id='unit_price<?php echo $row ?>'  name='items[unit_price][]' value='' />
                                    <input type='hidden' class='pi<?php echo $row ?>' id='store_id<?php echo $row ?>'  name='items[store_id][]' value='' />
                                    <input type='hidden' class='pi<?php echo $row ?>' id='supplier_id<?php echo $row ?>'  name='items[supplier_id][]' value='' />
                                    <input type='hidden' class='pi<?php echo $row ?>' id='item_id<?php echo $row ?>'  name='items[item_id][]' value='<?php echo $product->quotation_item_id ?>' />
                                    <input type='hidden' class='pi<?php echo $row ?>' id='item_id<?php echo $row ?>'  name='items[invoice_itype][]' value='<?php echo $product->invoice_itype ?>' />

                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>



                    <div id="wizard" class="box nav-box wizard g16">
                        <ul class="nav">
                            <li data-nav="#Customer" class="sel"><?php echo lang('tclient') ?> </li>
                            <li data-nav="#tproduct"><?php echo lang('tproduct') ?><span class="arrow">(</span></li>
                            <li data-nav="#tpay"><?php echo lang('tpay') ?><span class="arrow">(</span></li>
                            <!--<li data-nav="#texpences"> <?php echo lang('texpences') ?><span class="arrow">(</span></li>-->
                            <li data-nav="#tbill"> <?php echo lang('tbill') ?><span class="arrow">(</span></li>



                        </ul>



                        <div id="wizard-body" class="nav-cont vt">
                            <!--First tabs-->
                            <div id="Customer" class="nav-item show pad">
                                <div class=" form-group">
                                    <div class=" form-group" id="customer">
                                        <div class="g4 form-group">
                                            <label class="text-warning g8"><?php echo lang('Customer') ?></label>
                                            <div class="g8"><?php echo $invoice[0]->fullname ?></div>

                                        </div>

                                        <div class="g4 form-group">
                                            <label class="g8 text-warning"><?php echo lang('Email-Address') ?></label>
                                            <div class="g8"><?php echo $invoice[0]->email_address ?></div>

                                        </div>

                                        <div class="g4 form-group">
                                            <label class="g8 text-warning"><?php echo lang('Phone') ?></label>
                                            <div class="g8"><?php echo $invoice[0]->phone_number ?></div>

                                        </div>
                                        <br clear="all"/>
                                        <br clear="all"/>

                                        <?php //if (get_member_type() == '1' OR get_member_type() == '5'): ?>
                                        <div class="g4 form-group">
                                            <label class="text-warning"><?php echo lang('Company-Name') ?> :</label>
                                            <br>
                                            <div  class="ui-select" style="width:100%">
                                                <div class="styled-select " style="width:100%">
                                                    <?php company_dropbox('companyid', $invoice[0]->companyid); ?>
                                                    <span class="arrow arrowselectbox">&amp;</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="g4 form-group">
                                            <div class="field-box" >
                                                <label class="text-warning"><?php echo lang('Branch-Name') ?> :</label>
                                                <br>
                                                <div  class="ui-select" style="width:100%">
                                                    <div class="styled-select " style="width:100%">
                                                        <?php company_branch_dropbox('branchid', $invoice[0]->branchid, $invoice[0]->companyid); ?>
                                                        <span class="arrow arrowselectbox">&amp;</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?PHP //endif; ?>
                                        <div class="g5 form-group">
                                            <input id="customerid-val" name="customerid-val" type="hidden" value=''/>
                                            <input id="customerid" name="customerid" type="hidden" value='<?php echo $invoice[0]->userid ?>'/>
                                            <input id="customer_ftotal" name="customer_ftotal" type="hidden" value='0'/>
                                            <input id="customername2" name="customername2" type="hidden" value=''/>
                                            <label class="text-warning"><?php echo lang('Customer') ?> :</label>
                                            <br>
                                            <input id="customername" name="customername" type="text" class="g12 form-control" value="<?php echo $invoice[0]->fullname ?>" disabled="disabled"/>
                                            <!--<a onClick="show_dialog2('#demo-modal', '<?php echo base_url(); ?>customers/getallnewcustomer')" href="javascript:void" data-toggle="" class="g2"><i class="icon-th-list left" style="font-size:20px;color:black"></i></a>--> 
                                            <!--<a class="g2" onClick="show_dialog2('#demo-modal', '<?php echo base_url(); ?>customers/getAddnewcustomer')" href="javascript:void" style="cursor:pointer;"><img  src="<?php echo base_url(); ?>images/internal/add_customer.png" border="0" /></a>--> 
                                        </div>
                                        <div class="g1 form-group">
                                            <label class="text-warning"></label>
                                            <br>   


                                        </div>
                                        <br clear="all"/>
                                        <div class=" get_balance">
                                            <div class="g3 tag green"><?php echo lang('balance1') ?> 0</div>
                                            <div class="g3 tag green"><?php echo lang('balance2') ?> 0</div>
                                            <div class="g3 tag red"><?php echo lang('balance4') ?> 0</div>
                                            <div class="g3 tag red"><?php echo lang('balance3') ?> 0</div>
                                        </div>

                                        <div class="main_viewmess" style="display: none;clear: both;color: red;text-align: center;">
                                            <span><?php echo lang('mess_blacklist') ?> </span>
                                            <span class="viewmess"></span>
                                        </div>
                                    </div>
                                    <!--New Customer-->
                                    <div class=" form-control invoice" id="newcustomer" style="display:none;">
                                        <div class="g3 form-control">
                                            <label class="text-warning"><?php echo lang('Customer') ?> :</label>
                                            <br>
                                            <input id="customername" name="customername" type="text" class="form-control"/>
                                        </div>
                                        <div class="g3 form-control">
                                            <label class="text-warning">Mobile  No :</label>
                                            <br>
                                            <input name="phone_number" id="phone_number" type="text"  class="form-control"/>
                                        </div>
                                        <div class="g3 form-control">
                                            <label class="text-warning">Email :</label>
                                            <br>
                                            <input name="office_number" id="office_number" type="text"  class="form-control"/>
                                        </div>
                                        <div class="g3 form-control">
                                            <label class="text-warning">Address :</label>
                                            <br>
                                            <input name="product_notes" id="product_notes" type="text"  class="form-control"/>
                                        </div>
                                    </div>
                                    <br clear="all"/>
                                                                    <div class="nav-act btn-m">
                                    <!--<button class="mini prev">Back</button>-->
                                    <a href="javascript:void" style="padding-right: 20px;padding-left: 20px;"class="green next flt-r tag" onclick="gotonext('tproduct','Customer')"><?php echo lang('next')?>  >>  </a>
                                </div>
                                    <!--<a href="javascript:void" class="go_to_nexttabs reset_btn btn-glow primary" style="margin-top: 10px;">Next &gt;&gt;</a> <br clear="all">-->
                                </div>
                            </div>
                            <!--SECOND TAB-->
                            <div id="tproduct" class="nav-item pad-m">
                                <div class=" form-group">
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Category') ?> :</label>
                                        <br>
                                        <div class="ui-select" style="width: 100%;">
                                            <select name="category_id" id="category_id">
                                                <option><?php echo lang('choose') ?></option>
                                                <?php
                                                if (isset($categories) && !empty($categories)) {
                                                    foreach ($categories as $categ) {
                                                        ?>
                                                        <option value="<?php echo $categ->catid; ?>"><?php echo $categ->catname; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <span class="arrow arrowselectbox">&amp;</span>
                                        </div>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Product-Name') ?> :</label>
                                        <br>
                                        <input  type="hidden" name="productid"  id="productid" class="formControl"/>
                                        <input type="text" name="productname"  id="productname" class="g14 form-control "/>
                                        <a onClick="show_dialog2('#demo-modal', '<?php echo base_url(); ?>inventory/getallitems')" href="javascript:void" data-toggle="" class="g2"><i class="icon-th-list left" style="font-size:20px;color:black"></i></a> 
                                        <input type="hidden" name="productname2"  id="productname2" class="form-control "/>
                                        <input type="hidden" name="price_purchase"  id="price_purchase" class="form-control " value=""/>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Serial-No') ?> :</label>
                                        <br>
                                        <input name="serialnumber" id="serialnumber" type="text"  class=" currentProduct form-control"/>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Store') ?> :</label>
                                        <br>
                                        <div class="ui-select" style="width: 100%;">
                                            <?php store_dropbox('store_id', '', ''); ?>
                                            <span class="arrow arrowselectbox">&amp;</span>
                                        </div>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Quantity') ?> :</label>
                                        <br>
                                        <!--<input id="quantity" name="quantity" type="text" onchange="calculatePrice()"  class=" currentProduct form-control"/>-->
                                        <input id="quantity" name="quantity" type="text" onchange=""  class=" currentProduct form-control"/>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"> <?php echo lang('store_quantity') ?></label>
                                        <br>
                                        <input name="" id="storequantity" type="text" style="color:green;font-size: 16px;text-align: center" value="<?php //echo get_store_quantity(3);      ?>" disabled="disabled" class=" currentProduct form-control"/>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning" id="fortaotal"><?php echo lang('sforone') ?> :</label>
                                        <br>
                                        <input name="item_total_price" id="item_total_price" type="text"  class="item_total_price0 currentProduct form-control"/>
                                    </div>
                                    <div class="g2 form-group divAlternate_Price">
                                        <label class="text-warning"><?php echo lang('Alternate-Price') ?> :</label>
                                        <br>
                                        <input name="Alternate_Price" id="Alternate_Price" type="checkbox" value="1"  class="" style="width:10%"/>
                                    </div>
                                    <div class="g2 form-group"> <br>
                                        <div  id="Alternate_Price_input" style="display: none">
                                            <input name="item_total_price_old" id="item_total_price_old" type="text"  class="item_total_price_old currentProduct form-control"/>
                                        </div>
                                    </div>
                                    <br clear="all">
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Discount') ?> :</label>
                                        <br>
                                        <input id="item_discount" name="item_discount--" onkeyup="calculateDis()" type="text"  class="form-control"/>
                                    </div>
                                    <div class="g4 form-group"> <br>
                                        &nbsp; %
                                        <input type="radio" onchange="calculateDis()" name="type" id="type1" value="1"  class="currentProduct" />
                                        RO
                                        <input type="radio" onchange="calculateDis()" name="type" id="type2" value="2" class="currentProduct"/>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('afterDiscount') ?> :</label>
                                        <br>
                                        <input id="afterDiscount" name="item_discount" onchange="" type="text"  class="form-control"/>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Minimum') ?> :</label>
                                        <br>
                                        <input name="min_price" id="min_price" type="text"  class="currentProduct form-control" disabled="disabled"/>
                                        <input type="hidden" id="total_price" name="total_price"  value="" />
                                        <input type="hidden" id="price_product" name="price_product" />
                                        <input type="hidden" id="store_id" name="store_id" />
                                    </div>
                                    <div class="g8 form-group">
                                        <label class="text-warning"><?php echo lang('Discription') ?> :</label>
                                        <br>
                                        <textarea cols="100" style="height: 55px;"  rows="10"  name="product_comment" id="product_comment" class="dis_txtarea currentProduct form-control" ></textarea>
                                    </div>
                                    <div class="g8 form-group">
                                        <label class="text-warning"><?php echo lang('Notes') ?> :</label>
                                        <br>
                                        <textarea name="product_notes" style="height: 55px;"  id="product_notes" cols="100" rows="10" class="dis_txtarea currentProduct form-control" ></textarea>
                                    </div>
                                    <input type="hidden" id="product_picture" name="product_picture" />
                                    <input type="hidden" id="storeid_p" name="storeid_p" />
                                    <br  clear="all"/>
                                    <br  clear="all"/>
                                    <div class=" form-group">
                                        <div class="" onclick="addItem_edit()" style="border-bottom: 1px solid green;margin-bottom: -1px;position: relative;"> 
                                            <div style="border-width: 1px 1px 0px 1px;width: 113px;cursor: pointer;border-color: green;border-style: solid;clear: both;padding: 7px;">
                                                <i class="icon-addtocart left" style="font-size:20px;"></i>
                                                <?php echo lang('add-product') ?>
                                                <br clear="all"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group product_list">
                                        <table width="100%" align="left" id="product_list" >
                                            <thead>
                                                <tr>

                                                    <th></th>
                                                    <th><?php echo lang('Image') ?></th>
                                                    <th><?php echo lang('Product-Name') ?></th>
                                                    <th><?php echo lang('Discription') ?></th>
                                                    <th><?php echo lang('sforone') ?></th>
                                                    <th><?php echo lang('Quantity') ?></th>
                                                    <th><?php echo lang('Total') ?></th>
                                                    <th><i class="icon-remove-sign" style="font-size:20px;color: red;"></i></th>
                                                    <th><i class="nav-icon icon-settingstwo-gearalt" style="font-size:20px;"></i></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if ($invoice): ?>
                                                    <?php $count = 1 ?>
                                                    <?php foreach ($invoice as $product): ?>

                                                        <tr id="tr_<?php echo $row ?>">
                                                            <td ></td>
                                                            <td ><img src="<?php echo base_url() ?>uploads/item/<?php echo $product->product_picture ?>" width="50" height="50" /></td>



                                                            <td style="text-align:center"> <?php echo _s($product->itemname, get_set_value('site_lang')) ?> </td>
                                                            <td id="description" contentEditable="true"><?php echo $product->notes ?> </td>


                <!--                                                            <td id="oneitem<?php echo $product->itemid ?>" style="text-align:center" contentEditable="true"><?php echo $product->purchase_price ?></td>

                                                                            <td id="quantity_tp<?php echo $product->itemid ?>" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculatePrice2(this,<?php echo $product->itemid ?>)"><?php echo $product->quotation_item_quantity ?> </td>

                                                                            <td id="totalAmount" style="text-align:center" contentEditable="true" ><?php echo $product->quotation_item_price ?></td>-->



                                                            <td id="oneitem<?php echo $row ?>" style="text-align:center" onkeyup="calculate_pre_Price(this, '<?php echo $row ?>')" contentEditable="true"><?php echo $product->sale_price ?></td>

                                                            <td class="quantity_tp" id="quantity_tp<?php echo $row ?>" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculatePrice2(this, '<?php echo $row ?>')" ><?php echo $product->quotation_item_quantity ?> </td>

                                                            <td id="totalAmount<?php echo $row ?>" style="text-align:center" contentEditable="true"  class="ttamm thth"><?php echo $product->quotation_item_price ?></td>    




                                                            <td style="text-align:center;cursor:pointer;" ><img src="<?php echo base_url() ?>images/internal/delete_small.png" width="7" height="8" border="0"   onclick="confirmRemove('<?php echo $row ?>')"/></td>
                                                            <td align="center" valign="middle" style="text-align:center"><input class="mainten" name="editmainten" id="editmainten" type="checkbox" value="" onchange="addMaintenance('<?php echo $row ?>', this)" /></td>
                                                        </tr>

                                                        <?php $quan+=$product->quotation_item_quantity ?>
                                                        <?php $total+=$product->quotation_item_price ?>
                                                    <?php endforeach; ?>


                                                <?php endif; ?>
                                                <tr  class="grand-total" style="height: 50px;border-bottom: 1px solid #EFEFEF;">



                                                    <td colspan="5" bgcolor="#FFFFFF" >

                                                    </td> 


                                                    <td><?php echo lang('Total-Price') ?></td>
                                                    <td><strong id="netTotal"><?php echo $invoice[0]->quotation_total_amount ?></strong></td>    
                                                    <td><input type="hidden" name="totalnet_before" id="totalnet_before" value="<?php echo $invoice[0]->quotation_total_amount ?>"/></td>
                                                    <td></td>

                                                </tr>
                                                <tr  class="" style="background: #CCCCCC">




                                                    <td colspan="1" bgcolor="" >

                                                    </td> 
                                                    <td></td>


                                                    <td><div id="totalmin" style="display: none"></div></td>
                                                    <td>   
                                                        <div class="col-md-1"> 
                                                            <?php //echo lang('Discount') ?>
                                                        </div>
                                                        <div class="col-md-3"> 
                                                            <input id="totalDiscount" name="totalDiscount"  type="text" onchange=""  class="formtxtfield_small form-control" style="width:100px;"/>
                                                            <br clear="all"/>

                                                        </div>
                                                        <div class="col-md-4">
                                                            &nbsp; %
                                                            <input type="radio" onclick="calculateNetTotal()" name="type_ds" id="type_ds" value="1"  class="currentProduct">
                                                            RO
                                                            <input type="radio" onclick="calculateNetTotal()" name="type_ds" id="type_ds" value="2" class="currentProduct">
                                                        </div>
                                                    </td>







                                                    <td> </td>

                                                    <td style="text-align: center"><?php echo lang('afterDiscount') ?></td>
                                                    <td style="text-align: center">
                                                        <strong id="fnetTotal"></strong>
                                                        <input type="hidden" name="totalnet" id="totalnet" value="<?php echo $invoice[0]->quotation_total_amount ?>"/>
                                                    </td>    
                                                    <td><input type="hidden" name="totalnet_before" id="totalnet_before" value="<?php echo $invoice[0]->quotation_total_amount ?>"/></td>
                                                    <td></td>


                                                </tr>






                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="" id="product_maintenance"></div>


                                    <div class="g8 form-group">
                                        <label class="text-warning"><?php echo lang('Sales-Responsable') ?> :</label>
                                        <br>
                                        <div  class="ui-select">
                                            <div class="" >
                                                <select name="sale[sales_list][]" id="sales_list" onchange="showCode(this.value)">
                                                    <?php
                                                    if (!empty($sales)) {
                                                        $html = '';
                                                        foreach ($sales as $saler) {
                                                            $html .= '<input type="hidden"  id="salesCode' . $saler->bs_sales_id . '" name="salesCode' . $saler->bs_sales_id . '"  value="' . $saler->userid . '"/>';
                                                            $html .= '<input type="hidden"  id="salesName' . $saler->bs_sales_id . '" name="salesName' . $saler->bs_sales_id . '"  value="' . $saler->fullname . '"/>';
                                                            $html .= '<input type="hidden"  id="salescomission' . $saler->bs_sales_id . '" name="salescomission' . $saler->bs_sales_id . '"  value="' . $saler->bs_sales_commession . '"/>';
                                                            $html .= '<input type="hidden"  id="salescomissionType' . $saler->bs_sales_id . '" name="salescomissionType' . $saler->bs_sales_id . '"  value="' . $saler->bs_sales_commession_type . '"/>';
                                                            echo '<option value="' . $saler->bs_sales_id . '">' . $saler->fullname . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <span class="arrow arrowselectbox">&amp;</span>


                                            </div>
                                        </div>
                                        <!--<a id="osx2" href='#' class='osx2'><img src="<?php echo base_url(); ?>images/internal/add_customer.png" width="20" height="20" border="0" /></a>-->
                                        <?PHP echo $html; ?>
                                    </div>
                                    <div class="g8 form-group">
                                        <label class="text-warning"><?php echo lang('Code') ?> : (<a href="javascript:void(0)" onclick="addSales()"><?php echo lang('Add-Sales') ?></a>)</label>
                                        <br>
                                        <input name="sale[sale_code][]" id="sale_code" type="text"  class="form-control"/>

                                    </div>
                                    <div class=" form-group" id="sales_persons"></div>
                                    <div id="fff">


                                        <?php $gspersons = get_sales_items($invoice[0]->quotation_id, 'bs_sales_quotation_persons'); ?>

                                        <?php if ($gspersons): ?>

                                            <?php foreach ($gspersons as $gsperson): ?>
                                                <div class="raw" id="purchase_div<?php echo $gsperson->bs_sales_id ?>">
                                                    <div class="form_title"><?php echo lang('Sales-Name') ?>: </div>
                                                    <div class="form_field form-group" id="total_purchase_price"><?php echo $gsperson->fullname ?></div>
                                                    <div class="form_title"><?php echo lang('Sales-Amount') ?>: </div>
                                                    <div class="form_field form-group" id="total_purchase_price"><?php echo $gsperson->amount ?> RO</div></div>
                                                <input type='hidden' class='salerId' id='salerId<?php echo $gsperson->bs_sales_id ?>'  name='salers[salerId][]' value='<?php echo $gsperson->bs_sales_id ?>'>


                                            <?php endforeach; ?>

                                        <?php endif; ?>

                                    </div>

                                    <br clear="all">
                                    <!--<a href="javascript:void" class="go_to_nexttabs2 reset_btn btn-glow primary" style="margin-top: 10px;">Next &gt;&gt;</a>-->
                                    <br clear="all">
                                                                        <div class="nav-act btn-m">
                                    <!--<button class="">Back</button>-->
                                        <a href="javascript:void" style="" class="gotobutn mini prev tag" onclick="gotonext('Customer','tproduct')"><< <?php echo lang('back')?>  </a>
                                        <a href="javascript:void" style="" class="gotobutn green next  tag" onclick="gotonext('tpay','tproduct')"><?php echo lang('next')?>  >>  </a>
                                    </div>
                                </div>


                                <!--aria-controls="tab_item-1"--> 
                                <!--aria-labelledby="tab_item-1"--> 
                            </div>
                            <!--Third tabs-->
                            <div id="tpay" class="nav-item pad-m">
                                <div class=" form-group">
                                    <div class=" form-group">
                                        <div class="tag green"> <?php echo lang("Total") ?><span id="view_all_total"><?php echo $invoice[0]->quotation_total_amount ?></span></div>
                                        <div class="tag red"><?php echo lang("Remaining") ?> : <span id="remin_view_all_total"><?php echo $invoice[0]->quotation_total_amount ?></span></div>
                                        <div style="background:#398A42;border: 1px solid #398A42;" class="tag green"><?php echo lang("balance2") ?><span id="get_my_allaccount">0</span></div>
                                        <div style="background:red;border: 1px solid #C52033;"  class="tag green" onclick="rest_payment()"><?php echo lang("resetbalance") ?></div>

                                        <input type="hidden" name="last_payment_operation" id="lastpaymentoperation" value="<?php echo $invoice[0]->quotation_total_amount ?>"/>
                                    </div>
                                    <input type="hidden" name="receiverd_amount" id="recievedamount" value="<?php echo $invoice[0]->quotation_total_amount ?>"/>
                                    <input type="hidden" name="totalrecievedamount" id="totalrecievedamount" value="<?php echo $invoice[0]->quotation_total_amount ?>"/>
                                </div>

                                <div class=" title alert alert-success" id="increse_amount" style="display: none;font-weight: bold">
                                    <?php echo lang("increase_balance") ?>       <span id="increse_amount_span"></span><br/>
                                    <?php echo lang("Cashincrease") ?><input type="radio" name="method_return" value="1"/>
                                    <?php echo lang("addam_acc") ?><input type="radio" name="method_return" value="2"/>

                                </div>




                                <div class=" form-group" style="" id="">
                                    <label class="text-warning"><?php echo lang('howpay') ?> :</label><br>
                                    <div class="ui-select"  style="width:100%">                       
                                        <select class="validate[required]" name="payment[howpay][]" onchange="get_my_allaccount(this.value)" id="howpay">

                                            <option value="2"><?php echo lang('outbalance') ?></option>
                                            <option value="1"><?php echo lang('frombalance') ?></option>


                                        </select>                            
                                    </div>

                                </div>
                                <div class="g4 form-group">
                                    <label class="text-warning"><?php echo lang('TypePayment') ?> :</label><br>
                                    <div class="ui-select"  style="width:100%">                       
                                        <select class="" name="payment[p_type][]" onchange="showPaymentLabel(this.value, 0)">
                                            <option value="" selected="selected" ><?php echo lang('choose') ?> </option>
                                            <option value="1"><?php echo lang('Cash') ?></option>
                                            <option value="2"><?php echo lang('Bank') ?></option>
                                        </select>                            
                                    </div>

                                </div>
                                <div class="g4 form-group">
                                    <label class="text-warning"><?php echo lang('Payment-date') ?> :</label><br>
                                    <input name="payment[p_date][]"  type="text"  class="datapic_input payment_date form-control"/>
                                </div>
                                <div class="g4 form-group">
                                    <label class="text-warning"><?php echo lang('Payment-amount') ?> :</label><br>
                                    <input name="payment[p_amount][]"  type="text"  id="payment_amount" value=""  class=" form-control"/>
                                </div>
                                <div class="g3 form-group" id="div_banks" style="display:none;">
                                    <label class="col-lg-12"><?php echo lang('Bank'); ?> </label>


                                    <div class="ui-select" style="width:100%">
                                        <?php company_bank_dropbox('payment[p_bank][]', '', 'english', ' onchange="getBankAccounts2();"', 'bank_id'); ?>
                                    </div>


                                </div>
                                <div class="g3 form-group" style="display:none" id="div_accounts">
                                    <label class="col-lg-12"><?php echo lang('Accounts') ?> </label>


                                    <div class="ui-select" id="div_accounts_responce" style="width:100%">

                                    </div>


                                </div>

                                <div class="g3 form-group" style="display:none" id="div_TypePayment2">
                                    <label class="text-warning"><?php echo lang('TypePayment2') ?> :</label><br>
                                    <div class="ui-select"  style="width:100%">                       
                                        <select class="" name="payment[p_type2][]" onchange="showPaymentLabel(this.value, 0)">
                                            <option value="" selected="selected" ><?php echo lang('choose') ?> </option>
                                            <option value="cheque"><?php echo lang('Cheque') ?></option>
                                            <option value="transfer"><?php echo lang('Transfer') ?></option>
                                            <option value="deposite"><?php echo lang('Deposite') ?></option>
                                            <option value="withdraw"><?php echo lang('withdraw') ?></option>
                                            <option value="later"><?php echo lang('later') ?></option>
                                        </select>                            
                                    </div>

                                </div>

                                <div class="g3 form-group" style="display: none" id="div_pdoc">
                                    <label class="text-warning"><?php echo lang('Payment-doc') ?> :</label><br>
                                    <input name="payment[p_doc][]"  type="file"  class=" form-control"/>
                                </div>   

                                <div class=" form-group" style="display: none" id="div_pnumper">
                                    <label class="text-warning"><?php echo lang('Number') ?> :</label><br>
                                    <input name="payment[p_numper][]"  type="text"  class=" form-control"/>
                                </div>   

                                <div class=" form-group">
                                    <label class="text-warning"><?php echo lang('Payment-note') ?> :</label><br><textarea name='payment[p_note][]' class=" form-control"></textarea>
                                </div>


                                <div class="">
                                    <div class="">

                                        <div id="paymemts_div"> 







                                            <br claer="all"/>









                                        </div>


                                        <div class='more_add_new_payment_main'> </div>
                                        <br claer="all"/>
                                        <a href="javascript:void" onclick='add_new_payment()' class="btn btn-default" id="add_new_payment_btn" style="float: right;"><i class="ion-plus-circled"></i><?php echo lang("more") ?></a>

                                    </div>


                                </div>


                                <br clear="all"   />
                                                                <br clear="all"   />
                                <br clear="all"   />
                                <br clear="all"   />
                                    <div class="nav-act btn-m">
                                    <!--<button class="">Back</button>-->
                                        <a href="javascript:void" style="" class="gotobutn mini prev tag" onclick="gotonext('tproduct','tpay')"><< <?php echo lang('back')?>  </a>
                                        <a href="javascript:void" style="" class="gotobutn green next  tag" onclick="gotonext('texpences','tpay')"><?php echo lang('next')?>  >>  </a>
                                    </div>
                                <!--<a href="javascript:void" class="go_to_nexttabs3 reset_btn btn-glow primary" style="margin-top: 10px;">Next &gt;&gt;</a> </div>-->
                            </div>    
                            <!--Four tabs-->



                            <!--Four tabs-->
                            <div id="tbill" class="nav-item pad-m">
                                <div class="">
<!--                                    <div class="g5 divsale_direct_store" id="" style="">
                                        <div class="col-lg-4">
                                            <?php echo lang('option_1') ?>
                                        </div>
                                        <div class=" col-lg-1">
                                            <input name="sale_direct_store"  type="checkbox"  value='1'  class=" form-control" style="width: 20px;height: 20px;"/>
                                        </div>
                                    </div>-->
                                    <div class="g5" id="" style="">
                                        <div class="col-lg-4">
                                            <?php echo lang('option_2') ?>
                                        </div>
                                        <div class=" col-lg-1">
                                            <input name="print_option_pt"  type="checkbox"  value='1'  class=" form-control" style="width: 20px;height: 20px;"/>
                                        </div>
                                    </div>
                                    <div class="g5" id="" style="">
                                        <div class="col-lg-4">
                                            <?php echo lang('option_3') ?>
                                        </div>
                                        <div class=" col-lg-1">
                                            <input name="print_option_desc"  type="checkbox"  value='1'  class=" form-control" style="width: 20px;height: 20px;"/>
                                        </div>
                                    </div>
                                    <div class="g5" id="" style="">
                                        <div class="col-lg-4">
                                            <?php echo lang('option_4') ?>
                                        </div>
                                        <div class=" col-lg-1">
                                            <input name="print_option_note"  type="checkbox"  value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                        </div>
                                    </div>
                                    <div class="g5" id="" style="">
                                        <div class="col-lg-4">
                                            <?php echo lang('option_5') ?>
                                        </div>
                                        <div class=" col-lg-1">
                                            <input name="print_option_netprice"  type="checkbox"  value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                        </div>
                                    </div>
                                    <div class="g5" id="" style="">
                                        <div class="col-lg-4">
                                            <?php echo lang('option_6') ?>
                                        </div>
                                        <div class=" col-lg-1">
                                            <input name="print_option_dicount"  type="checkbox"   value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                        </div>
                                    </div>
                                    <div class="g5" id="" style="">
                                        <div class="col-lg-4">
                                            <?php echo lang('option_7') ?>
                                        </div>
                                        <div class=" col-lg-1">
                                            <input name="print_option_total"  type="checkbox" value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                        </div>
                                    </div>
                                    <div class="g5" id="" style="">
                                        <div class="col-lg-4">
                                            <?php echo lang('option_8') ?>
                                        </div>
                                        <div class=" col-lg-1">
                                            <input name="print_option_pay"  type="checkbox" value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                        </div>
                                    </div>
                                    <div class="g5" id="" style="">
                                        <div class="col-lg-4">
                                            <?php echo lang('option_9') ?>
                                        </div>
                                        <div class=" col-lg-1">
                                            <input name="print_option_total_pay"  type="checkbox" value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                        </div>
                                    </div>

                                    <?php $get_setting_option = unserialize(get_setting_option()->options_print); ?>
                                    <?php if ($get_setting_option): ?>
                                        <?php $count = 0 ?>
                                        <?php foreach ($get_setting_option['name'] as $get_setting_option_row): ?>
                                            <div class="g5" id="" style="">
                                                <div class="col-lg-4">
                                                    <?php echo $get_setting_option['name'][$count] ?>
                                                </div>
                                                <div class=" col-lg-1">
                                                    <input name="options_print_pages[name][]"  type="checkbox" value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                                    <input name="options_print_pages[text][]"  type="hidden" value='<?php echo $get_setting_option['text'][$count] ?>'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>

                                    <?php endif; ?>


                                </div>
                                <br clear="all"/>
<!--                                <div class="g2" align="left"> 
                                    <input name="" type="submit" class="submit_btn btn-glow primary " value="Add"  onclick=""/>
                                    <input name="" type="button" class="submit wide flt-r mod-tg" value="اضافة الفاتوره"  onclick="addInvoice()"/>
                                    <input name="" type="reset" class="reset_btn btn-glow primary" value="Reset" />
                                </div>-->
                                <br clear="all"/>
                                <br clear="all"/>
                                <br clear="all"/>
                                <div class="nav-act btn-m">
                                    <!--<button class="">Back</button>-->
                                    <a href="javascript:void" style="" class="gotobutn mini prev  tag" onclick="gotonext('texpences','tbill')"><< <?php echo lang('back')?>  </a>
                                    <button name="" type="button" class="submit_btn btn-glow primary"   onclick="addInvoice()"/><?php echo lang('addninvoice')?></button>
                                </div>
                            </div>




                        </div>
                    </div>

                    <!--end raw---> 

                    <!--end raw---> 
                    <!--end raw---> 

                    <!--                    <br clear="all"/>
                                        <div class="raw" align="left"> 
                                          <input name="" type="submit" class="submit_btn btn-glow primary " value="Add"  onclick=""/>
                                            <input name="" type="button" class="submit_btn btn-glow primary" value="اضافة الفاتوره"  onclick="addInvoice()"/>
                                            <input name="" type="reset" class="reset_btn btn-glow primary" value="Reset" />
                                        </div>-->
                </form>
                <div id="basic-modal-content" class="digadduser" style="display: none">
                    <form action="<?php echo base_url(); ?>sales_management/add_new_customer" method="post" id="form1" class="frm_customer" name="frm_customer" autocomplete="off">
                        <h3>
                            <?php //echo lang('add-edit')           ?>
                        </h3>
                        <br />
                        <div id="main-content" class="main_content" style="width:800px">
                            <div class="notion title title alert alert-info"> * <?php echo lang('mess1') ?> </div>
                            <?php error_hander($this->input->get('e')); ?>
                            <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
                            <input type="hidden" name="userid" id="userid" value="<?php echo $user->userid; ?>" />
                            <div class="form">
                                <?php if (get_member_type() == '1' OR get_member_type() == '5'): ?>
                                    <div class="raw form-group">
                                        <label class="col-lg-3"><?php echo lang('Company-Name') ?></label>
                                        <div class="col-lg-4">
                                            <div class="ui-select" >
                                                <div class="styled-select">
                                                    <?php company_dropbox('companyid', $user->companyid); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form_title">
                                            <input type="checkbox" value="1" id="ad_account" />
                                            <span style="padding-left: 5px;"><?php echo lang('Add-Account') ?></span> </div>
                                        <br />
                                        <br />
                                        <div class="raw field-box login_account" style="">
                                            <label class="col-lg-3"><?php echo lang('User-Name') ?></label>
                                            <div class="col-lg-4">
                                                <input name="username"  id="username" value="<?php echo $user->username; ?>" type="text"  class="validate[required] form-control" style=""/>
                                            </div>
                                        </div>
                                        <div class="raw field-box form-group login_account" style="">
                                            <label class="col-lg-3"><?php echo lang('Password') ?></label>
                                            <div class="col-lg-4">
                                                <input name="upassword" id="upassword" value="" type="password"  class="validate[required] form-control" style=""/>
                                            </div>
                                        </div>
                                        <div class="raw field-box form-group login_account" style="">
                                            <label class="col-lg-3"><?php echo lang('RePassword') ?></label>
                                            <div class="col-lg-4">
                                                <input name="confpassword" id="confpassword" value="" type="password"   class="form-control" style=""/>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                    </div>
                                    <div class="raw form-group">
                                        <label class="col-lg-3"><?php echo lang('Branch-Name') ?></label>
                                        <div class="col-lg-4">
                                            <div class="ui-select" >
                                                <div class="styled-select">
                                                    <?php company_branch_dropbox('branchid', $user->branchid, $user->companyid); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <br />
                                <br />
                                <div class="raw field-box">
                                    <label class="col-lg-3"><?php echo lang('Customer-Name') ?></label>
                                    <div class="col-lg-4">
                                        <input name="fullname" id="fullname" value="<?php echo $user->fullname; ?>" type="text"  class="validate[required] form-control"/>
                                    </div>
                                </div>
                                <div class="raw field-box">
                                    <label class="col-lg-3"><?php echo lang('Mobile-Number') ?></label>
                                    <div class="col-lg-4">
                                        <input name="phone_number" id="phone_number" value="<?php echo $user->phone_number; ?>" type="text"  class="validate[required] form-control"/>
                                    </div>
                                </div>
                                <div class="raw field-box">
                                    <label class="col-lg-3"><?php echo lang('Contact-Number') ?></label>
                                    <div class="col-lg-4">
                                        <input name="office_number" id="office_number" value="<?php echo $user->office_number; ?>" type="text"  class="form-control"/>
                                    </div>
                                </div>
                                <div class="raw field-box">
                                    <label class="col-lg-3"><?php echo lang('Fax-Number') ?></label>
                                    <div class="col-lg-4">
                                        <input name="fax_number" id="fax_number" value="<?php echo $user->fax_number; ?>" type="text"  class="form-control"/>
                                    </div>
                                </div>
                                <div class="raw field-box">
                                    <label class="col-lg-3"><?php echo lang('Email-Address') ?></label>
                                    <div class="col-lg-4">
                                        <input name="email_address" id="email_address" value="<?php echo $user->email_address; ?>" type="text"  class="validate[required] form-control"/>
                                    </div>
                                </div>
                                <div class="raw field-box">
                                    <label class="col-lg-3"><?php echo lang('Address') ?></label>
                                    <div class="col-lg-4">
                                        <textarea name="address" id="address" cols="" rows="" class="form-control"><?php echo $user->address; ?></textarea>
                                    </div>
                                </div>
                                <div class="raw field-box">
                                    <label class="col-lg-3"><?php echo lang('Status') ?></label>
                                    <div class="col-lg-4">
                                        <div class="ui-select" >
                                            <div class="styled-select">
                                                <?php get_statusdropdown($user->status, 'status'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="raw field-box">
                                    <label class="col-lg-3"><?php echo lang('Notes') ?></label>
                                    <div class="col-lg-4">
                                        <textarea name="notes" id="notes" cols="" rows="" class="form-control"><?php echo $user->notes; ?></textarea>
                                    </div>
                                </div>
                                <div class="raw field-box">
                                    <?php if (get_member_type() != '1' OR get_member_type() != '5'): ?>
                                        <div class="raw form-group">
                                            <div class="form_title"></div>
                                            <div class="col-lg-4">
                                                <input type="hidden" name="companyid" id="companyid"  value="<?php echo $company_id; ?>" />
                                            </div>
                                        </div>
                                        <div class="raw form-group">
                                            <div class="form_title"></div>
                                            <div class="col-lg-4">
                                                <input type="hidden" name="branchid"  id="branchid" value="<?php echo $branch_id; ?>" />
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <input type="hidden" name="member_type" id="member_type" value="6"/>
                                <input name="sub_mit" onclick="save_new_customer();" id="sub_mit" type="submit" class="btn-flat primary" value='<?php echo lang('Add') ?>'>
                                <!--<input name="sub_reset" type="reset" class="btn-flat primary" value="<?php echo lang('Reset') ?>" />-->
                            </div>
                            <script>
                                function save_new_customer() {
                                    /* var url = $(".frm_customer").attr("action");
                                     alert(url);
                                     var formData = {};
                                     $(".frm_customer").find("input[name]").each(function (index, node) {
                                     formData[node.name] = node.value;
                                     });
                                     $.ajax({
                                     url:url,
                                     type:"POST",
                                     data:{formData:formData},
                                     success:function(res){
                                     
                                     //$("").html(res);
                                     //alert(res);
                                     console.log(res);
                                     }
                                     
                                     });*/

                                }
                            </script> 
                            <!--end of raw--> 
                        </div>
                    </form>
                </div>

                <!------> 

            </div>

            <!-- END PAGE --> 

        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    var allProductsData = new Array();
    var allSalesData = new Array();
    totalAmountexComision = '';
    totalnetAmountexComision = '';
    $(document).keypress(function (e) {
        if (e.which == 13) {
            //   alert('You pressed enter!');
            if ($(".currentProduct").is(":focus")) {
                //  alert('You pressed enter!');
                if ($("#productid").val() != "")
                {
                    addItem();
                }
            }
            else {
                //alert('not');
            }
        }
    });</script> 
<script type="text/javascript">
    netTotals = new Array(); /// for keep rocrds net total for each produc
    Totalsmin = new Array(); /// for keep rocrds net total for each produc
    Totalsquan = new Array(); /// for keep rocrds net total for each produc
    rowCounter = 0; /// counter for table products

    netCouner = 0; // counter for net totals
    tbHtml = ''; // for adding new row in the table 

    editArray = new Array(); // edit any product fromm tab;e 



    total_value = 0; // total cacluate value



    productData = {}; // array for current working product



    counter = 0; // counter for products in the hiddden

</script> 
<script type="text/javascript">
    $(function () {
        /*$(".from_date").datepicker({
         defaultDate: "today",
         changeMonth: true,
         numberOfMonths: 1,
         });
         $(".payment_date").datepicker({
         defaultDate: "today",
         changeMonth: true,
         numberOfMonths: 1,
         });
         $(".dpayment_date input").datepicker({
         defaultDate: "today",
         changeMonth: true,
         numberOfMonths: 1,
         });
         */
        $("#is_payment").change(function () {


            ckPayment = $(this).is(":checked");
            //alert(ckPayment);
            if (ckPayment) {
                if (netTotals.length > 0) {
                    $(".payment_div").show();
                }
            }
            else {
                $(".payment_div").hide();
            }
            //alert(ckPayment);

        });
    });</script> 



<script type="text/javascript">
    $(function () {
        $('#expense_date').datepicker({dateFormat: 'yy-mm-dd'});
        $(".expense_period").click(function () {
            //alert('asdasd');

            //checked_status = $(this).is(':checked');
            expense_period_val = $(this).val();

            //alert(checked_status);
            if (expense_period_val == 'fixed') {

                $("#period_parent").show();
            }
            else {
                $("#period_parent").hide();
            }
        });
    });
    $(document).ready(function () {
        //alert('ready');
        var ac_config = {
            source: "<?php echo base_url(); ?>outcome/getAutoSearch",
            select: function (event, ui) {
                $("#expense_title").val(ui.item.cus);
                $("#expense_id").val(ui.item.cus);
                console.log(ui);
                //swapme();
                setTimeout('swapgeneral()', 500);
            },
            minLength: 1
        };

        $("#expense_title").autocomplete(ac_config);


    });



</script>


<!--<script type="text/javascript" src="<?php echo base_url(); ?>js/function.js" ></script>--> 
<!-- End Section--> 
<!--footer--> 




<div class='hide_form_expences' style="display:none"> 


    <div class="g3 form-group">
        <label class="text-warning"><?php echo lang('expense_title') ?></label>
        <div class="">
            <input name="expense[expense_title][]" id="" type="text"  value="" class="form-control"/>
        </div>
    </div>

    <input type="hidden" name="type" id="type" value="indirect" />  
    <div class="g3 form-group">
        <label class="text-warning"><?php echo lang('value') ?></label>
        <div class="">
            <input name="expense[value][]" value="" id="value" type="text"  class="form-control"/>

        </div>
    </div>

    <div class="g2 form-group" id="div_banks">
        <label class="text-warning"><?php echo lang('cat') ?> </label>
        <div class="">
            <div class="ui-select" style="width:100%">
                <div class="">
                    <?php expense_charges('expense[expense_charges_id][]', $exdata->expense_charges_id, 'english', ''); ?>
                </div>
            </div>
        </div>
    </div>
    <!--            <div class="raw field-box">
                    <label class="col-lg-3"><?php echo lang('created') ?></label>
                    <div class="col-lg-4">
                        <input name="created" value="<?php echo (post('created')) ? post('created') : $exdata->created; ?>" id="expense_date" type="text"  class="form-control"/>
                    </div>
                </div>-->
    <div class="g2 form-group">
        <label class="text-warning"><?php echo lang('expense_type') ?></label>
        <div class="">
            <label>
                <input type="radio" id="expense_type1" class="expense_period" value="fixed" name="expense[expense_type][]"><?php echo lang('Fixed') ?></label>
            <label>
                <input type="radio" id="expense_type2" class="expense_period" value="accured" name="expense[expense_type][]"><?php echo lang('Accured') ?></label>
        </div>
    </div>
    <div class="g2 form-group" id="period_parent" style="display:none;">
        <label class="text-warning"><?php echo lang('choose') . lang('expense_period') ?></label>
        <div class="">
            <div class="ui-select" >
                <div class="">
                    <select name="expense[expense_period][]" id="expense_period" style="width:100%">
                        <option  selected="selected"><?php echo lang('choose') ?></option>
                        <option value="daily"><?php echo lang('Daily') ?></option>
                        <option value="weekly"><?php echo lang('Weekly') ?> </option>
                        <option value="monthly"><?php echo lang('Monthly') ?> </option>
                        <option value="yearly"><?php echo lang('Yearly') ?> </option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <br clear="all"    />





    <div class="g3 form-group" id="div_banks">
        <label class="text-warning"><?php echo lang('Bank') ?> </label>
        <div class="">
            <div class="ui-select" style="width:100%">
                <div class="styled-select3" style="width:100%">
                    <?php company_bank_dropbox('expense[bank_id_ex][]', '', 'english', ' ', 'bank_id_ex'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="g3 form-group" style="" id="div_accounts">
        <label class="text-warning">Accounts </label>
        <div class="">
            <div class="ui-select" style="width:100%">
                <div class="styled-select" id="div_accounts_responce_ex" style="width:100%">

                </div>
            </div>
        </div>
    </div>

    <div class="g4 form-group" id="div_checkque" style="display:none;">
        <label class="text-warning">Deposite Recipt </label>
        <div class="">
            <div class="ui-select" >
                <div class="styled-select">
                    <input type="expense[file][]"  class="form-control" name="deposite_recipt" id="deposite_recipt" />
                </div>
            </div>
        </div>
    </div>




    <!--                            <div class="g4 form-group" style="display:none" id="div_tr_accounts">
                                    <label class="text-warning">Accounts </label>
                                    <div class="">
                                        <div class="ui-select" >
                                            <div class="styled-select" id="div_tr_accounts_responce">
    
                                            </div>
                                        </div>
                                    </div>
                                </div>  -->
    <div class="g3 form-group">
        <label class="text-warning"><?php echo lang('Notes') ?></label>
        <div class="">
            <textarea name="expense[Notes][]" cols="" rows="" class="form-control"></textarea>

        </div>
    </div> 

</div>


<!----->



<div class='hide_form_payment' style="display:none"> 
    <script type="text/javascript">
        $('.datapic_input').datepicker({dateFormat: 'yy-mm-dd'});
    </script>

    <?php $row = rand(33, 800) ?>

    <!--    <div class=" form-group" style="" id="">
            <label class="text-warning"><?php echo lang('howpay') ?> :</label><br>
            <div class="ui-select"  style="width:100%">                       
                <select class="validate[required]" name="payment[howpay][]" onchange="">
                    <option value="" selected="selected" ><?php echo lang('choose') ?> </option>
                    <option value="1"><?php echo lang('frombalance') ?></option>
                    <option value="2"><?php echo lang('outbalance') ?></option>
    
                </select>                            
            </div>
    
        </div>-->
    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('TypePayment') ?> </label><br>
        <div class="ui-select styled-select2"  style="width:100%">
            <select name="payment[p_type][]" class="" onchange="" payment='5' class='1'>
                <option value="" selected="selected" ><?php echo lang('choose') ?> </option>
                <option value="1"><?php echo lang('Cash') ?></option>
                <option value="2"><?php echo lang('Bank') ?></option>
            </select>
        </div>

    </div>
    <div class="g4 form-group ">
        <label class="text-warning"><?php echo lang('Payment-date') ?></label><br>

        <input name="payment[p_date][]"  type="text"  class="datapic_input payment_date form-control"/>

    </div>
    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('Payment-amount') ?></label><br>

        <input name="payment[p_amount][]"  type="text" id="payment_amountt_<?php echo $row ?>" onchange="do_payment_amountt(this.value,<?php echo $row ?>)" value=""  class=" form-control"/>



    </div>



    <div class="g3 form-group" id="div_banks" style="display:none;">
        <label class="col-lg-12"><?php echo lang('Bank'); ?> </label>


        <div class="ui-select styled-select3" style="width:100%">
            <?php company_bank_dropbox('payment[p_bank][]', '', 'english', '', 'arr_bank'); ?>
        </div>


    </div>

    <div class="g3 form-group" style="display:none" id="div_accounts">
        <label class="col-lg-12"><?php echo lang('Accounts') ?> </label>


        <div class="ui-select" id="div_accounts_responce" style="width:100%">

        </div>


    </div>

    <div class="g3 form-group" style="display:none" id="div_TypePayment2">
        <label class="text-warning"><?php echo lang('TypePayment2') ?> :</label><br>
        <div class="ui-select"  style="width:100%">                       
            <select class="" name="payment[p_type2][]" onchange="showPaymentLabel(this.value, 0)">
                <option value="" selected="selected" ><?php echo lang('choose') ?> </option>
                <option value="cheque"><?php echo lang('Cheque') ?></option>
                <option value="transfer"><?php echo lang('Transfer') ?></option>
                <option value="deposite"><?php echo lang('Deposite') ?></option>
                <option value="withdraw"><?php echo lang('withdraw') ?></option>
                <option value="later"><?php echo lang('later') ?></option>
            </select>                            
        </div>

    </div>

    <div class="g3 form-group" style="display: none" id="div_pdoc">
        <label class="text-warning"><?php echo lang('Payment-doc') ?> :</label><br>
        <input name="payment[p_doc][]"  type="file"  class=" form-control"/>
    </div>   

    <div class=" form-group" style="display: none" id="div_pnumper">
        <label class="text-warning"><?php echo lang('Number') ?> :</label><br>
        <input name="payment[p_numper][]"  type="text"  class=" form-control"/>
    </div>   

    <div class=" form-group">
        <label class="text-warning"><?php echo lang('Payment-note') ?> :</label><br><textarea name='payment[p_note][]' class=" form-control"></textarea>
    </div>



</div>



<div id="osx2-modal-content" class="osx2-modal-content" style="display:none;"> 
    <!--<form action="javascript:void(0)" method="post" id="sales_form">-->
    <div id="osx2-modal-title">Add New Sales</div>
    <div class="close"><a href="#" class="simplemodal-close">x</a></div>
    <div id="osx2-modal-data">
        <div class="raw">
            <div class="form_title">Name</div>
            <div class=" col-lg-4">
                <input id="fullname" name="fullname" type="text"  class="formtxtfield"/>
            </div>
        </div>
        <div class="raw">
            <div class="form_title">Phone  Number</div>
            <div class=" col-lg-4">
                <input id="phone" name="phone" type="text"  class="formtxtfield"/>
            </div>
        </div>
        <div class="raw">
            <div class="form_title">Mobile</div>
            <div class=" col-lg-4">
                <input id="mobile" name="mobile" type="text"  class="formtxtfield"/>
            </div>
        </div>
        <div class="raw">
            <div class="form_title">Salary</div>
            <div class=" col-lg-4">
                <input id="salary" name="salary" type="text"  class="formtxtfield"/>
            </div>
            <span> RO.</span> </div>
        <div class="raw">
            <div class="form_title">Calcuate from</div>
            <div class=" col-lg-4">
                <div class="defaultP">
                    <input type="radio" id="month6" value="total" name="month1">
                    <label for="month6">Total Price</label>
                    <br>
                    <input type="radio" id="month3" value="net" name="month1">
                    <label for="month3">Net Profit</label>
                </div>
            </div>
        </div>
        <div class="raw">
            <div class="form_title">Commession</div>
            <div class=" col-lg-4" style="w">
                <input id="commession" name="commession" type="text"  class="formtxtfield" size="3" maxlength="3"/>
            </div>
            <span> %</span> </div>
        <div class="raw">
            <div class="form_title">Notes</div>
            <div class=" col-lg-4">
                <textarea id="notes" name="notes" cols="" rows="" class="formareafield"></textarea>
            </div>
        </div>
        <div class="raw" align="center">
            <input name="" type="button" class="submit_btn btn-glow primary next" value="Submit" onclick="addingNewSales()"  />
            <input name="" type="reset" class="reset_btn btn-glow primary next" value="Reset" />
        </div>
        <!--end of raw--> 

    </div>
    <!--</form>--> 
</div>





<!--</Expencices>--> 
<div id="texpences" class="nav-item pad-m" style="display:none">









    <div class="g3 form-group">
        <label class="text-warning"><?php echo lang('expense_title') ?></label>
        <div class="">
            <input name="expense[expense_title][]" id="" type="text"  value="" class="form-control"/>
        </div>
    </div>

    <input type="hidden" name="type" id="type" value="indirect" />  
    <div class="g3 form-group">
        <label class="text-warning"><?php echo lang('value') ?></label>
        <div class="">
            <input name="expense[value][]" value="" id="value" type="text"  class="form-control"/>

        </div>
    </div>

    <div class="g2 form-group" id="div_banks">
        <label class="text-warning"><?php echo lang('cat') ?> </label>
        <div class="">
            <div class="ui-select" style="width:100%">
                <div class="">
                    <?php expense_charges('expense[expense_charges_id][]', $exdata->expense_charges_id, 'english', ''); ?>
                </div>
            </div>
        </div>
    </div>
    <!--            <div class="raw field-box">
                    <label class="col-lg-3"><?php echo lang('created') ?></label>
                    <div class="col-lg-4">
                        <input name="created" value="<?php echo (post('created')) ? post('created') : $exdata->created; ?>" id="expense_date" type="text"  class="form-control"/>
                    </div>
                </div>-->
    <div class="g2 form-group">
        <label class="text-warning"><?php echo lang('expense_type') ?></label>
        <div class="">
            <label>
                <input type="radio" id="expense_type1" class="expense_period" value="fixed" name="expense[expense_type][]"><?php echo lang('Fixed') ?></label>
            <label>
                <input type="radio" id="expense_type2" class="expense_period" value="accured" name="expense[expense_type][]"><?php echo lang('Accured') ?></label>
        </div>
    </div>
    <div class="g2 form-group" id="period_parent" style="display:none;">
        <label class="text-warning"><?php echo lang('choose') . lang('expense_period') ?></label>
        <div class="">
            <div class="ui-select" >
                <div class="">
                    <select name="expense[expense_period][]" id="expense_period" style="width:100%">
                        <option  selected="selected"><?php echo lang('choose') ?></option>
                        <option value="daily"><?php echo lang('Daily') ?></option>
                        <option value="weekly"><?php echo lang('Weekly') ?> </option>
                        <option value="monthly"><?php echo lang('Monthly') ?> </option>
                        <option value="yearly"><?php echo lang('Yearly') ?> </option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <br clear="all"    />





    <div class="g3 form-group" id="div_banks">
        <label class="text-warning"><?php echo lang('Bank') ?> </label>
        <div class="">
            <div class="ui-select" style="width:100%">
                <div class="styled-select" style="width:100%">
                    <?php company_bank_dropbox('expense[bank_id_ex][]', '', 'english', ' onchange="getBankAccounts5();" ', 'bank_id_ex'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="g3 form-group" style="" id="div_accounts">
        <label class="text-warning">Accounts </label>
        <div class="">
            <div class="ui-select" style="width:100%">
                <div class="styled-select" id="div_accounts_responce_ex" style="width:100%">

                </div>
            </div>
        </div>
    </div>

    <div class="g4 form-group" id="div_checkque" style="display:none;">
        <label class="text-warning">Deposite Recipt </label>
        <div class="">
            <div class="ui-select" >
                <div class="styled-select">
                    <input type="file"  class="form-control" name="expense[deposite_recipt][]" id="deposite_recipt" />
                </div>
            </div>
        </div>
    </div>




    <!--                            <div class="g4 form-group" style="display:none" id="div_tr_accounts">
                                    <label class="text-warning">Accounts </label>
                                    <div class="">
                                        <div class="ui-select" >
                                            <div class="styled-select" id="div_tr_accounts_responce">
    
                                            </div>
                                        </div>
                                    </div>
                                </div>  -->
    <div class="col-md-6 form-group">
        <label class="text-warning"><?php echo lang('Notes') ?></label>
        <div class="">
            <textarea name="expense[Notes][]" cols="" rows="" class="form-control"></textarea>

        </div>
    </div>

    <br clear="all"/>
    <div class='more_add_new_expences_main'> </div>
    <br clear="all"/>
    <a href="javascript:void" onclick='add_new_expences()' class="btn btn-default" style="float: right;"><i class="ion-plus-circled"></i><?php echo lang("more") ?></a>   
    <br clear="all"/>   
</div>



<script>
    $(document).ready(function () {
            searchabout_permession('divAlternate_Price');
            searchabout_permession('divsale_direct_store');
    });

</script>