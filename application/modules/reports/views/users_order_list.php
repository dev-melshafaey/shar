<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>POS System</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>newdesign/css/bootstrap.min.css" rel="stylesheet">
    <!-- Reset -->
    <link href="<?php echo base_url(); ?>newdesign/css/normalize.css" rel="stylesheet">
    <!-- Custom -->
    <link href="<?php echo base_url(); ?>newdesign/css/style.css" rel="stylesheet">
    <!-- Arabic -->
    <link href="<?php echo base_url(); ?>newdesign/css/ar.css" rel="stylesheet">
    <!-- Select -->
    <link href="<?php echo base_url(); ?>newdesign/css/bootstrap-select.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>newdesign/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="<?php echo base_url(); ?>newdesign/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>newdesign/css/DateTimePicker.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="<?PHP echo base_url(); ?>durarthem/ltr/databale/examples/resources/syntax/shCore.css">

    <script type="text/javascript" language="javascript" src="<?PHP echo base_url(); ?>durarthem/ltr/databale/media/js/jquery.dataTables.js"></script>

    <script type="text/javascript" language="javascript" src="<?PHP echo base_url(); ?>durarthem/ltr/databale/examples/resources/syntax/shCore.js"></script>

    <style>
        .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9 {float: right;}
        .panel-heading {text-align: right;}
        .bootstrap-select:not([class*="span"]):not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {width: 100%;}
        .bootstrap-select.btn-group .btn .filter-option {text-align: right;}
        .bootstrap-select.btn-group .btn .caret {left: 12px; right: 0;}
        .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9 {float: right;}
        .dropdown-menu {text-align: right !important;}
        .green_gate {background: #5cb85c; border: none; color: #fff; margin-bottom: 1em;}
        .red_gate {background: #d9534f; border: none; color: #fff; margin-bottom: 1em;}
        .orange_gate {background: #f4a034; border: none; color: #fff; margin-bottom: 1em;}
        .yellow_gate {background: #c4c641; border: none; color: #fff; margin-bottom: 1em;}
        .form-control::-moz-placeholder {color: #777; text-align: center;}
        .table > thead > tr > th {text-align: center;}
        .form-horizontal .control-label {float: right;}
        .pagination {margin: 0 auto; display: table;}
        .loginmodal-container {background: #fff;text-align: center;}
        .panel-btns {padding: 1em 0 0 1em;}
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <![endif]-->
</head>
<body>
<header>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="col-sm-3 col-xs-12">
                    <ul class="list-inline">
                        <li><div class="dateplace"><h4 style="text-align:center"><img src="<?php echo base_url(); ?>newdesign/images/calendar-icon.png" width="33"> April 27</h4>
                                <h5>00 : 00 : 00</h5></div></li>
                        <li><img src="<?php echo base_url(); ?>newdesign/images/logo.png" alt="Business Solutions" class="img-responsive bslogo"></li>
                    </ul>

                </div>
                <div class="col-sm-7 col-xs-12">
                  </div>
                <div class="col-sm-2 col-xs-12"><img src="<?php echo base_url(); ?>newdesign/images/logo.png" alt="Business Solutions" class="img-responsive bslogo"></div>
            </div> <!--./col-sm-12-->
        </div>
    </div>

</header>
<section>
    <div class="container">
        <div class="row" style="display:none;">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <!-- Row start -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading clearfix">
                                <i class="icon-calendar"></i>
                                <h3 class="panel-title moverite">البيانات العامة </h3>
                            </div>

                            <div class="panel-body">
                                <form class="form-horizontal row-border" action="#">
                                    <div class="col-sm-12 moverite">

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label style="padding-right:0; padding-left:0;" class="control-label"> من تاريخ </label>
                                                        <input type="text" class="form-control" data-field="date" readonly /><div id="dtBox"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label style="padding-right:0; padding-left:0;" class="control-label"> الي تاريخ</label>
                                                        <input type="text" class="form-control" data-field="date" readonly /><div id="dtBox"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label style="padding-right:0; padding-left:0;" class="control-label">اختر </label>
                                                        <select class="selectpicker" style="display: none;">
                                                            <option> كل</option>
                                                            <option>المتبقية</option>
                                                            <option>المدفوعه جزئيا</option>
                                                            <option>الملغية</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <div class="col-sm-6">
                                                        <label style="padding-right:0; padding-left:0;" class="control-label"> اختر فرع </label>
                                                        <select class="selectpicker" style="display: none;">
                                                            <option>المتبقية</option>
                                                            <option> كل</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label style="padding-right:0; padding-left:0;color:#fff" class="control-label" >.</label>
                                                        <input type="button" value="البحث" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!--.row-->

                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row end -->
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="panel panel-default">
        <!--
            <div class="panel-btns">
                <div class="row">
                    <ul class="list-inline pull-left nofluid" style="padding-left:1em;padding-bottom:0;">
                        <li><button type="submit" class="btn btn-default"><img src="<?php echo base_url(); ?>newdesign/images/acrobat-flat.png" width="24" height="24"> PDF</button></li>
                        <li><button type="submit" class="btn btn-default"><img src="<?php echo base_url(); ?>newdesign/images/csv-icon.png" width="24" height="24"> CSV</button></li>
                        <li><button type="submit" class="btn btn-default"><img src="<?php echo base_url(); ?>newdesign/images/xl-icon.png" width="24" height="24"> Excel</button></li>
                        <li><button type="submit" class="btn btn-default"><img src="<?php echo base_url(); ?>newdesign/images/print-flat.png" width="24" height="24"> Print</button></li>
                    </ul>
                </div>
            </div>
            -->
            <div class="panel-body" style="padding:0">



                <div class="qrow qcol-sm-12 qcol-md-offset-2 custyle">
                    <div class="table-responsive">
                        <table id="new_data_table" class="table table-striped custab">
                            <thead>

                            <tr>
                                <th></th>
                                <th><label style="padding-right:0; padding-left:0;" class="control-label"> Username </label>
                                    <input type="text" class="form-control" placeholder="Username" /></th>
                                <th><label style="padding-right:0; padding-left:0;" class="control-label"> Total Orders </label>
                                    <input type="text" class="form-control" placeholder='Total Orders' /></th>
                                <th><label style="padding-right:0; padding-left:0;" class="control-label"> Total Unclear Amount </label>
                                    <input type="text" class="form-control" placeholder="Total Unclear Amount" /></th>
                                <th><label style="padding-right:0; padding-left:0;" class="control-label"> Action </label>
                                    <input type="text" class="form-control" placeholder="Action" /></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                if($usersorders){
                                    foreach($usersorders as $uorders){
                                        ?>
                                        <tr>
                                            <td><input type="checkbox"></td>
                                            <td><?php echo $uorders->fullname; ?></td>
                                            <td><?php echo $uorders->totalorders; ?></td>
                                            <td><?php echo $uorders->totalpendingamount; ?></td>
                                            <td><a href="javascript:void(0)" onclick="showPayPrice('<?php echo $uorders->userid; ?>','<?php  echo $uorders->totalpendingamount; ?>')" data-toggle="modal" data-target="#actions" class="btn btn-success">Pay</a> <a href="<?php echo base_url(); ?>users/payment_history/<?php echo $uorders->userid; ?>" class="btn btn-warning">View History</a></td>
                                     
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>

                            </tbody>




                        </table></div>
                </div>

            </div> <!-- card -->

        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p>تصميم و برمجة شركة <a target="_blank" href="http://durar-it.com/en/"><img alt="Durar Smart Solutions" src="images/durar-smart-solutions.png"></a>  درر للحلول الذكية ش.م.م </p>
            </div><!--./grid -->
        </div><!--./row -->
    </div><!--./container-->
</footer>
<!--menu-->
<div id="wrapper" class="enlarged forced">
    <button class="button-menu-mobile open-left">
        <i class="fa fa-bars"></i>
    </button>
    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <div class="user-details hiddex-xs">
                <div class="pull-left">
                    <img src="images/avatar-1.jpg" alt="" class="thumb-md img-circle img-responsive">
                </div>
                <div class="user-info">
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">عبد الله <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)"><i class="fa fa-face-unlock"></i> الملف الشخصي<div class="ripple-wrapper"></div></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-settings"></i> إعدادات</a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-settings-power"></i> خروج</a></li>
                        </ul>
                    </div>
                    <p class="text-muted m-0">مدير</p>
                </div>
            </div>
            <!--- Divider -->
            <div id="sidebar-menu">
                <ul>
                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-home" style="display:block; padding:0;"></span><span>  رئيسية  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> تقارير الارصده </a></li>
                            <li><a href=""> نقارير المكاسب والخسائر </a></li>
                            <li><a href=""> تقارير المراجعات </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-shopping-cart"></span><span>  أمر شراء </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> قائمة طلبات الشراء</a></li>
                            <li><a href=""> إضافة أمر الشراء</a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-shopping-cart"></span><span>  نقطة البيع </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> إضافة بيع </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-list-alt"></span><span>  المبيعات  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> فاتورة جديدة </a></li>
                            <li><a href=""> قائمة الفواتير </a></li>
                            <li><a href=""> الفواتير المرتجعة </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-shopping-cart"></span><span>  المشتريات  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> فاتورة جديدة </a></li>
                            <li><a href=""> قائمة الفواتير </a></li>
                            <li><a href=""> الفواتير المرتجعة </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-file"></span><span>  العروض  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> عروض غير معتمدة </a></li>
                            <li><a href=""> عرض سعر جديد </a></li>
                            <li><a href=""> قائمة عروض الاسعار </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-credit-card"></span><span>  المسوقين  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> الدفع </a></li>
                            <li><a href=""> المسوقين </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-open-file"></span><span>  المصروفات  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> اقسام المصروفات </a></li>
                            <li><a href=""> عرض الكل </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-equalizer"></span><span>  الشركات  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> اضافة شركة جديدة </a></li>
                            <li><a href=""> قائمة شركات  </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-pawn"></span><span>  الحسابات  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> اضافة بنك </a></li>
                            <li><a href=""> اضافة حساب بنكي  </a></li>
                            <li><a href=""> اضافة معامله </a></li>
                            <li><a href=""> البنوك  </a></li>
                            <li><a href=""> المعاملات والتحويلات </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-retweet"></span><span>  الفروع  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> عرض كل الفروع </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-usd"></span><span>  الاموال  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> اضافة دفع للفرع جديد </a></li>
                            <li><a href=""> اضافة جديد </a></li>
                            <li><a href=""> اموال الفروع </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-user"></span><span>  الاعضاء  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> عرض الكل </a></li>
                            <li><a href=""> التصاريح </a></li>
                            <li><a href=""> مجموعات التصاريح </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-th-large"></span><span>  الاصول والمستهلكات  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> عرض الكل </a></li>
                            <li><a href=""> اضافة جديد </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-cog"></span><span>  الاعدادات العامة  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> الاعدادات </a></li>
                            <li><a href=""> الموقع </a></li>
                            <li><a href=""> المناطق </a></li>
                            <li><a href=""> الدول </a></li>
                            <li><a href=""> المدن </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-book"></span><span> الوحدات  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> كل الوحدات </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-file"></span><span> الملفات  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> الاقسام </a></li>
                            <li><a href=""> كل الملفات </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-file"></span><span> عقود  </span><span class="pull-right"><i class="fa fa-add"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href=""> عرض الكل </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><span class="glyphicon glyphicon-log-out"></span><span> خروج  </span></a>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>	
    <!-- Left Sidebar End -->

</div>
<!-- END wrapper -->
<!--.menu-->

<!-- login modal start-->
<div class="modal fade" id="actions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>newdesign/images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="g4 form-group search_div" style="text-align: center;">
                                <label id="pay_am_html" class="text-warning"></label>
                                <input type="hidden" id="uid" name="uid"><input type="hidden" id="order_pending_amount" name="order_pending_amount">
                                <input id="amount_val" name="amount_val" type="text" class="form-control" style="width:35%; text-align: center; position: relative; left: 34%;"/>
                                <br clear="all">
                                <input type="button" value="Pay" class="btn btn-success ordbtn   green" onclick="addPayment()">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="actions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>newdesign/images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-xs-12">
                                <div class="g4 form-group search_div">

                                    <label id="pay_am_html" class="text-warning"></label>
                                    <input type="hidden" id="uid" name="uid">
                                    <input id="amount_val" name="amount_val" type="text" class="form-control" style="width:35%; text-align: center; position: relative; left: 34%;"/>
                                    <br clear="all">
                                    <input type="button" value="Pay" class="btn btn-success   green" onclick="addPayment()">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <div class="modal fade" id="pay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div><img src="<?php echo base_url(); ?>newdesign/images/logo.png" width="100" class="makeCenter"></div>
            </div> <!-- /.modal-header -->
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="g4 form-group search_div">

                                <label id="pay_am_html" class="text-warning"></label>
                                <input type="hidden" id="uid" name="uid">
                                <input id="amount_val" name="amount_val" type="text" class="form-control" style="width:35%; text-align: center; position: relative; left: 34%;"/>
                                <br clear="all">
                                <input type="button" value="Pay" class="btn btn-success   green" onclick="addPayment()">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- login modal end -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url(); ?>newdesign/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>newdesign/js/bootstrap.min.js"></script>
    <!-- Select Picker -->
    <script src="<?php echo base_url(); ?>newdesign/js/bootstrap-select.js"></script>
    <!-- Owl Carousel -->
    <script src="<?php echo base_url(); ?>newdesign/js/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function () {
            var carousel = $("#owl-demo");
            carousel.owlCarousel({
                navigation:true,
                navigationText: [
                    "<i class='icon-chevron-left icon-white'><</i>",
                    "<i class='icon-chevron-right icon-white'>></i>"
                ],
            });
        });

        function showPayPrice(uid,am){
            amount = am; //am.toFixed(3);
            $("#pay_am_html").html(amount);
			 $("#order_pending_amount").val(amount);
            $("#uid").val(uid);
        }

        function addPayment(){
           am_val = $("#amount_val").val();
            uid = $("#uid").val();
			$('.ordbtn').prop('disabled',true);
			 order_pending_amount = $("#order_pending_amount").val();
            $.ajax({
                url: "<?php echo base_url(); ?>pos/save_payment_amount",
                type: 'post',
                data: {'uid':uid,am:am_val,'order_amount':order_pending_amount},
                cache: false,
                success: function (data) {
                    //alert(data);
                    $("#tb_viewdata").html(data);
                    //additemList();
						window.top.location = '<?php echo base_url(); ?>users/users_orders'
                    $(".demo").hide();
                }	
            });
        }
    </script>
    <!-- Double click -->
    <script>
        var oriVal;
        $("#eVal1").on('dblclick', 'span', function () {
            oriVal = $(this).text();
            $(this).text("");
            $("<input type='text' class='thVal'>").appendTo(this).focus();
        });
        $("#eVal1").on('focusout', 'span > input', function () {
            var $this = $(this);
            $this.parent().text($this.val() || oriVal);
            $this.remove(); // Don't just hide, remove the element.
        });

        var oriVal;
        $("#eVal2").on('dblclick', 'span', function () {
            oriVal = $(this).text();
            $(this).text("");
            $("<input type='text' class='thVal'>").appendTo(this).focus();
        });
        $("#eVal2").on('focusout', 'span > input', function () {
            var $this = $(this);
            $this.parent().text($this.val() || oriVal);
            $this.remove(); // Don't just hide, remove the element.
        });

        var oriVal;
        $("#eVal3").on('dblclick', 'span', function () {
            oriVal = $(this).text();
            $(this).text("");
            $("<input type='text' class='thVal'>").appendTo(this).focus();
        });
        $("#eVal3").on('focusout', 'span > input', function () {
            var $this = $(this);
            $this.parent().text($this.val() || oriVal);
            $this.remove(); // Don't just hide, remove the element.
        });

        var oriVal;
        $("#eVal4").on('dblclick', 'span', function () {
            oriVal = $(this).text();
            $(this).text("");
            $("<input type='text' class='thVal'>").appendTo(this).focus();
        });
        $("#eVal4").on('focusout', 'span > input', function () {
            var $this = $(this);
            $this.parent().text($this.val() || oriVal);
            $this.remove(); // Don't just hide, remove the element.
        });
    </script>
    <!-- C Slider -->
    <script src="<?php echo base_url(); ?>newdesign/js/jquery.cslide.js"></script>
    <script>
        $(document).ready(function(){
            $("#cslide-slides").cslide();
        });
    </script>

    <!-- datepicker -->
    <script type="text/javascript" src="<?php echo base_url(); ?>newdesign/js/DateTimePicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $("#dtBox").DateTimePicker();
        });
    </script>
    <!-- script for sidebar menu -->
    <script>
        var resizefunc = [];
    </script>

    <script src="<?php echo base_url(); ?>newdesign/js/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>newdesign/js/fastclick.js"></script>
    <script src="<?php echo base_url(); ?>newdesign/js/jquery.slimscroll.js"></script>
    <script src="<?php echo base_url(); ?>newdesign/js/jquery.app.js"></script>
    <script src="<?php echo base_url(); ?>newdesign/js/detect.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/buttons.dataTables.css">
    <script type="text/javascript" src="<?PHP echo base_url(); ?>js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?PHP echo base_url(); ?>js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?PHP echo base_url(); ?>js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?PHP echo base_url(); ?>js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="<?PHP echo base_url(); ?>js/buttons.flash.js"></script>
    <script type="text/javascript" src="<?PHP echo base_url(); ?>js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="<?PHP echo base_url(); ?>js/buttons.print.min.js"></script>

    <script type="text/javascript">

        if ($('#new_data_table').length > 0)
        {
            var bFilter = false;
            if($('#new_data_table #remin').length > 0){
                //bFilter = true;
                //alert('found');
                //$("#new_data_table #remin").prop("class","sorting_disabled");
                //$("#new_data_table #remin").toggleClass('sorting');
            }


            /*
             var new_data_table = $('#new_data_table').DataTable({
             "ordering": false,
             columnDefs: [ {
             className: 'no-sort',
             orderable: true,
             targets:   11
             } ],
             order: [ 11, 'asc' ],
             //"scrollY": "200px",
             "oLanguage": {
             "sSearch": "",
             "oPaginate": {"sNext": "التالی", "sPrevious": "السابق"}
             }
             });

             buttons: [
             'copy', 'csv', 'excel', 'pdf', 'print'
             ],

             */
            var new_data_table = $('#new_data_table').DataTable({
                "ordering": false,
                dom: 'Bfrtip',
                "iDisplayLength": 100,
                "lengthMenu": [ [10, 100, 1000, -1], [10, 100, 1000, "All"] ],
                "columnDefs": [{
                    "orderSequence": ["desc", "asc"],
                    "searchable": false,
                    "orderable": false,
                    "targets": 4
                }]
                ,
                "order": [[5, 'desc' ]]
            } );


            new_data_table.on( 'order.dt search.dt', function () {
                new_data_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            }).draw();

            /*
             new $.fn.DataTable.FixedColumns(new_data_table, {
             leftColumns: 2,
             rightColumns: 0
             });
             */

            new_data_table.columns().eq(0).each(function (colIdx)
            {
                $('input', new_data_table.column(colIdx).header()).on('keyup change', function ()
                {
                    new_data_table.column(colIdx).search(this.value).draw();
                });



            });



            $('.search_filter').keyup(function () {
                $('#new_data_table td').removeHighlight().highlight($(this).val());
            });

            new_data_table.on('draw', function () {
                //tiptop();
            });
        }
    </script>
</body>
</html>