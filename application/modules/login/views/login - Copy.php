<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Businness Solution-Durar- Login</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
        <link rel="shortcut icon" href="favicon.png">
        <?php if (get_set_value('site_lang') == 'english'): ?>
        <!---CSS Files-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/core.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/login.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/inputs.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/ui.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/anims.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/icons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/ltr/css/global.css">
        <!---jQuery Files-->
        <script src="<?php echo base_url(); ?>durarthem/ltr/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>durarthem/ltr/js/inputs.js"></script>
        <script src="<?php echo base_url(); ?>durarthem/ltr/js/functions.js"></script>
        <?php else:?>
        <!---CSS Files-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/core.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/login.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/inputs.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/ui.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/anims.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/icons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>durarthem/rtl/css/global.css">
        <!---jQuery Files-->
        <script src="<?php echo base_url(); ?>durarthem/rtl/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>durarthem/rtl/js/inputs.js"></script>
        <script src="<?php echo base_url(); ?>durarthem/rtl/js/functions.js"></script>
        <?php endif;?>
    </head>

    <body>
        
        <div id="wrapper">
            <div style="text-align: center;margin-bottom: 50px;"> <img class="logo" src="<?php echo base_url()?>img/logo-login.png" alt="logo"></div>    
            <div id="login-body">

                <form id="login-form" class="show" method="post" action="<?php echo base_url() . 'login/validate'; ?>">

                    <div id="login">

                        <div id="login-user">
                            <div class="icon-user"><span class="arrow">"</span></div>
                            <input type="text" id="username" name="login_txt"  class="login-input" placeholder="Username" value="" autocomplete="off">
                            <div id="user-select">
                                <ul class="grad2">
                                    <li class="sel">
                                        <img src="<?php echo base_url(); ?>durarthem/rtl/img/avatars/alex.jpg" alt="User avatar">
                                        <div class="av-overlay"></div><span>novalex</span></li>
                                    <li><img src="<?php echo base_url(); ?>durarthem/rtl/img/avatars/michael.jpg" alt="User avatar">
                                        <div class="av-overlay"></div><span>m1chael</span></li>
                                    <li><img src="<?php echo base_url(); ?>durarthem/rtl/img/avatars/johnny.jpg" alt="User avatar">
                                        <div class="av-overlay"></div><span>Johnny 1337</span></li>
                                </ul>
                            </div>

<!--                            <div id="register">
                                <p>Username not found.</p>
                                <button type="button" id="reg-btn" class="green btn-s">Register</button>
                            </div>-->

                        </div>

                        <div id="login-pass">
                            <span class="icon-securityalt-shieldalt"></span>
                            <span id="forgot-psw" style="display: none">Forgot?</span>
                            <input type="password" id="password" name="pass_txt" class="login-input required passwf" placeholder="Password">
                        </div>

                    </div>

                    <!--<div id="avatar"><img src="<?php echo base_url(); ?>durarthem/img/avatars/alex.jpg" alt="Selected user avatar"><div id="av-overlay"></div></div>-->

                    <button id="login-btn" type="submit" class="button submit"><?php echo lang('Log-in') ?></button>

                </form>

<!--                <form id="register-form" action="http://amxui.novalx.com/dash.html">
                    <div id="register-inner">
                        <input type="text" id="reg-user" class="login-input required" placeholder="Username" autocomplete="off">
                        <input type="password" id="reg-pass" class="login-input required" placeholder="Password">
                        <input type="text" id="reg-email" class="login-input required email" placeholder="E-mail">
                    </div>
                    <button id="register-btn" type="submit" class="button submit">Register</button>
                </form>-->

                <div id="login-action">
                    <!--<div id="logo"></div>-->
                    <div id="rb-check-cont">
                        <label for="rb-check"><?php echo lang('Remember-me') ?></label>
                        <input type="checkbox" name="remember" id="rb-check" checked>
                    </div>
                </div>
            </div>

        </div><!--END WRAPPER-->

        <div id="load"><div id="spinner"></div><div id="load-inner"></div></div>

        <!---jQuery Code-->
        <script type='text/javascript'>
            $('#login-body').hide();
            $(window).load(function () {
                $('#load').fadeOut(600, function () {
                    $('#login-body').fadeIn(10, function () {
                        $(this).addClass('show');
                        $('#password').focus();
                        if ($(window).width() <= 480)
                            $('#password').blur();
                    });
                });
            });

            var browserSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
            if (browserSafari)
                $('#wrapper').addClass('safari-fix');

            if ($.storage() == true) {
                localStorage.removeItem('user-name');
                localStorage.removeItem('user-avatar');
            } else {
                function lsnotif() {
                    $('#wrapper').append('<div class="notif orange full slideUp">LocalStorage is disabled. Your settings will not be saved!<span class="icon icon-resistor"></span><p class="nt-det">Upgrade your browser or enable local data storage.</p></div>');
                }
                ;
                setTimeout(lsnotif, 2000);
                $.fn.notif();
            }

            $('#login-user .icon-user').click(function () {
                $('#login-user').toggleClass('showusr');
            });

            $('#user-select li').click(function () {
                $('#login-user').removeClass('showreg showusr');
                $(this).addClass('sel').siblings('.sel').removeClass('sel');
                var name = $(this).children('span').text();
                img = $(this).children('img').attr('src');
                $('#username').val(name).removeClass('error').addClass('valid');
                $('#avatar img').attr('src', img).fadeIn(300);
                $('#login-user').find('.input-error').remove();
                $('#login-pass').find('.input-error').remove();
                $('#forgot-psw').fadeOut(200);
                $('#password').removeClass('error').val('').focus();

                if ($.storage() == true) {
                    localStorage.setItem('user-name', name);
                    localStorage.setItem('user-avatar', img);
                }
                ;
            });

            var users = ["novalex", "m1chael", "Johnny 1337"];
            var username = $('#username');
            var typeDelay;
            $(username).keyup(function () {
                clearTimeout(typeDelay);
                $('#user-select').find('.sel').removeClass('sel');
                typeDelay = setTimeout(checkuser, 500);
            });
            function checkuser() {
                var usrval = username.val();
                usrlen = usrval.length;
                if ($.inArray(usrval, users) == -1 && usrlen > 0) {
                    $('#avatar img').fadeOut(300);
                    $('#login-user').addClass('showreg').removeClass('showusr');
                    $('#user-select').find('.sel').removeClass('sel');
                } else if (usrlen == 0) {
                    $('#avatar img').fadeOut(200);
                    $('#login-user').removeClass('showreg');
                } else {
                    $('#avatar img').fadeIn(300);
                    $('#login-user').removeClass('showreg showusr');
                    $('#user-select li').each(function () {
                        var name = $(this).find('span').text();
                        img = $(this).children('img').attr('src');
                        if (name == usrval) {
                            $(this).addClass('sel');
                            $('#avatar img').attr('src', img);
                        }
                    });
                    $('#login-pass').find('.input-error').remove();
                    $('#password').removeClass('error').focus();
                }
                ;
            }
            ;

            $('.input-error').click(function () {
                var trigger = $(this).data('trigger');
                $(this).offsetParent().find('input').eq(trigger).focus();
            });

            $('#password').keyup(function () {
                if ($(this).val())
                    $('#forgot-psw').fadeOut(200)
                else
                    $('#forgot-psw').fadeIn(300);
            });

            $('#reg-btn').click(function () {
                $('#login-form').removeClass('show');
                $('#register-form').addClass('show');
                $('#reg-user').focus();
            });

            $('#rb-check').checkfn();

            $('#login-form, #register-form').validate();

        </script>
    </body>
</html>














<!--
<head>
    <title>Businness Solution</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     bootstrap 
    <link href="<?php echo base_url(); ?>css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>css/bootstrap/bootstrap-overrides.css" type="text/css" rel="stylesheet" />

     global styles 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/compiled/layout.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/compiled/elements.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/compiled/icons.css" />

     libraries 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/lib/font-awesome.css" />

     this page specific styles 
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/compiled/signin.css" type="text/css" media="screen" />

     open sans font 
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    [if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]
   
</head>
<body>
<?php
$lang = $this->session->userdata('site_lang');

if ($lang == 'english') {
    ?>
             <style>
        .loginfield{
                    direction:ltr;
            }
        </style>
    <?php
} elseif ($lang == 'arabic') {
    ?>
             <style>
        .loginfield{
                    direction:rtl;
            }
        </style>
    <?php
} else {
    ?>
             <style>
        .loginfield{
                    direction:rtl;
            }
        </style>
    <?php
}
?>


     background switcher 
    <div class="bg-switch visible-desktop">
        <div class="bgs">
            <a href="#" data-img="landscape.jpg" class="bg active">
                <img src="<?php echo base_url(); ?><?php echo base_url(); ?>durarthem/img/bgs/landscape.jpg" alt="background" />
            </a>
            <a href="#" data-img="blueish.jpg" class="bg">
                <img src="<?php echo base_url(); ?><?php echo base_url(); ?>durarthem/img/bgs/blueish.jpg" alt="background" />
            </a>            
            <a href="#" data-img="7.jpg" class="bg">
                <img src="<?php echo base_url(); ?><?php echo base_url(); ?>durarthem/img/bgs/7.jpg" alt="background" />
            </a>
            <a href="#" data-img="8.jpg" class="bg">
                <img src="<?php echo base_url(); ?><?php echo base_url(); ?>durarthem/img/bgs/8.jpg" alt="background" />
            </a>
            <a href="#" data-img="9.jpg" class="bg">
                <img src="<?php echo base_url(); ?><?php echo base_url(); ?>durarthem/img/bgs/9.jpg" alt="background" />
            </a>
            <a href="#" data-img="10.jpg" class="bg">
                <img src="<?php echo base_url(); ?><?php echo base_url(); ?>durarthem/img/bgs/10.jpg" alt="background" />
            </a>
            <a href="#" data-img="11.jpg" class="bg">
                <img src="<?php echo base_url(); ?><?php echo base_url(); ?>durarthem/img/bgs/11.jpg" alt="background" />
            </a>
        </div>
    </div>


    <div class="login-wrapper">
        <a href="<?php echo base_url() ?>">
            <img class="logo" src="<?php echo base_url(); ?><?php echo base_url(); ?>durarthem/img/logo-login.png" alt="logo" />
        </a>

        <div class="box">
            <div class="content-wrap">
                <h6><?php echo lang('Log-in') ?></h6>
                <form action="<?php echo base_url() . 'login/validate'; ?>" method="post" name="frm_login" id="frm_login"> 
                    <input class="form-control loginfield" id="login_txt" name="login_txt" type="text" placeholder="<?php echo lang('Username') ?>">
                    <input class="form-control loginfield" id="pass_txt" name="pass_txt" type="password" placeholder="<?php echo lang('Your-password') ?>">
                    <a href="#" class="forgot"><?php echo lang('Forgot-password') ?>?</a>
                    <div class="remember">
                        <input id="remember-me" type="checkbox">
                        <label for="remember-me"><?php echo lang('Remember-me') ?></label>
                    </div>
                    <a id="loginbtn" class="btn-glow primary login"><?php echo lang('Log-in') ?></a>
                </form>
            </div>
        </div>

        <div class="no-account">
            <p>Don't have an account?</p>
            <a href="signup.html">Sign up</a>
        </div>
    </div>

     scripts 
    <script src="<?php echo base_url(); ?>js/jquery.latest.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>js/theme.js"></script>

     pre load bg imgs 
    <script type="text/javascript">
        $(function () {
            // bg switcher
    //        $("html").css("background-image", "url('<?php echo base_url(); ?>durarthem/img/bgs/landscape.jpg')");
            var $btns = $(".bg-switch .bg");
            $btns.click(function (e) {
                e.preventDefault();
                $btns.removeClass("active");
                $(this).addClass("active");
                var bg = $(this).data("img");

                $("html").css("background-image", "url('<?php echo base_url(); ?>durarthem/img/bgs/" + bg + "')");
            });
            $("#loginbtn").click(function () {
               
                        //alert('You pressed enter!');
                loginAttempt();
                   

            });

            $(document).keypress(function (e) {
                if (e.which == 13) {
                    //   alert('You pressed enter!');
                    if ($("#pass_txt").is(":focus")) {
                        //alert('You pressed enter!');
                        loginAttempt();
                    }
                }
                                if(e.which == 65) {
                                                //redirectPage('jobs/add_jobs');
                                                //$("#site_lang").val('arabic');
                                                //$("#change_lang").submit();

                                        $("#site_lang").val('arabic');
                                        $("#c_lang").submit();
                        }
                                if(e.which == 69) {
                                                //redirectPage('jobs/add_jobs');
                                                //$("#site_lang").val('english');
                                                //$("#change_lang").submit();
                                        $("#site_lang").val('english');
                                        $("#c_lang").submit();
                        }
                                

            });
        });

        function loginAttempt() {
            userid = $("#login_txt").val();
            userpass = $("#pass_txt").val();
            if (userid != "" && userpass != "") {
                $("#frm_login").submit();
            }
            else {
                alert('Please Enter the username and Password');
            }

        }
    </script>
    <form id="c_lang" action="<?php echo base_url(); ?>login" method="post">
    <input  type="hidden"  id="site_lang" name="site_lang">
    </form>
</body>-->