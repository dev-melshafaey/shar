<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Transaction_management_model extends CI_Model {
	
	/*
	* Properties
	*/

//----------------------------------------------------------------------
    
	/*
	* Constructor
	*/
	
	function __construct()
    {
        parent::__construct();
		
		//Load Table Names from Config
    }
	
	function getBranchCashId($branch_id=''){
		$sql = "SELECT id from an_branch_cash where branch_id = ".$branch_id;	
		$q = $this->db->query($sql);
		$res = $q->row();	
		return $res->id;
	}
	
	function insertNewtransaction($data='')
	{
		//if($data['account_id']=='' || !isset($data['account_id'])){
			//$data['branch_cash_id'] = $this->getBranchCashId($data['branch_id']);
		//}
		$data['emp_id'] = $this->session->userdata('userid');
		$this->db->insert('an_company_transaction',$data);
		return $this->db->insert_id();
	}
	
	
	function LoadTransactions($account_id='')
	{
		$sql = "SELECT 
				  t.id,
				  t.`job_id`,
				  t.payment_type,
				  t.value,
				  t.created,
				  c.`customer_name`,
				  IF(
					t.transaction_type = 'debit',
					t.value,
					'0'
				  ) AS 'debit',
				  IF(
					t.transaction_type = 'credit',
					t.value,
					'0'
				  ) AS 'credit',
				  t.value,
				  t.notes 
				FROM
				  an_company_transaction AS t, an_customers AS c
				  WHERE c.`id` = t.`customer_id` ";
		if($account_id!=''){
			$sql .= "  AND t.`account_id` = '".$account_id."'  ";
		}
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function LoadBankAccountByBankId($bank_id='')
	{
		$sql = "SELECT 
				  * 
				FROM
				  an_bank_accounts 
				WHERE bank_id = '".$bank_id."' 
				  AND STATUS = 'Active' ";
		$q = $this->db->query($sql);
		return $q->result();
		
	}
	
	
	function LoadBankAccountExceptAccountId($account_id='')
	{
		$sql = "SELECT 
				  * 
				FROM
				  an_bank_accounts 
				WHERE account_id != '".$account_id."' 
				  AND STATUS = 'Active' ";
		$q = $this->db->query($sql);
		return $q->result();
		
	}
	
	
	function updateBranchCash($branch_id='',$amount='0')
	{
		$sql = "UPDATE 
				  an_branch_cash 
				SET
				  branch_cash = branch_cash - $amount 
				WHERE branch_id = '".$branch_id."' ";
		$q = $this->db->query($sql);
		return true;
	}
	
	
	function LoadAllCashManagement()
	{
		$sql = "SELECT 
				  * 
				FROM
				  an_branch,
				  an_branch_cash 
				WHERE an_branch.`branch_id` = an_branch_cash.`branch_id` ";
		$q = $this->db->query($sql);
		return $q->result();		
	}
	
	function LoadAllbanksAccounts()
	{
		$sql = "SELECT 
  ab.*,atc.*,atc2.*,b.*,b.`created` AS account_created 
FROM
  `an_bank_accounts` AS ab
   INNER JOIN`an_banks` AS b
   ON b.`id`= ab.`bank_id`
  LEFT JOIN 
    (SELECT 
	act.`account_id`,SUM(act.`value`) AS totaldebit
    FROM
      `an_company_transaction` act 
    WHERE act.`transaction_type` = 'debit' GROUP BY act.`account_id`) AS atc
    ON atc.account_id = ab.`account_id`
      LEFT JOIN 
    (SELECT 
	act2.`account_id`,SUM(act2.`value`) AS totalcredit
    FROM
      `an_company_transaction` act2 
    WHERE act2.`transaction_type` = 'credit' GROUP BY act2.`account_id`) AS atc2
    ON atc2.account_id = ab.`account_id`";
		$q = $this->db->query($sql);
		return $q->result();		
	}
	
	function updateBankAccountCash($account_id='',$amount='0')
	{
		$sql = "UPDATE 
				  an_bank_accounts 
				SET
				  account_cash = account_cash - $amount 
				WHERE account_id = '".$account_id."' ";
		$q = $this->db->query($sql);
		return true;
	}
	
	function updateBankAccountCashSender($account_id='',$amount='0')
	{
		
		if($amount !=""){ 
				$sql = "UPDATE 
						  an_bank_accounts 
						SET
						  account_cash = account_cash + $amount 
						WHERE account_id = '".$account_id."' ";
				$q = $this->db->query($sql);
		}
		return true;
	}
	
	
	
	function udpateJobStatus($job_id='')
	{
		$data['job_status'] = 'close';
		$this->db->where('id', $job_id);
		$this->db->update('an_jobs',$data);
		return true;
	}
	
	function udpateJobStatusPaid($job_id='',$charge_id='')
	{
		$rec = $this->getJobType($job_id,$charge_id);
		
		$tablename = $rec->tablename;
		
		$data['is_company_paid'] = '1';
		$this->db->where('job_id', $job_id);
		$this->db->where('charge_id', $charge_id);
		$this->db->update($tablename,$data);
		return true;
	}
	
	// Created By M.Ahmed
	
	function getJobType($job_id='',$charge_id=''){
		if($job_id!=''){
			$sql = "SELECT *,'an_contract_job_charges' as tablename FROM an_contract_job_charges AS cj, an_charges 													AS c WHERE 
					cj.`charge_id` = c.`charge_id`
					AND job_id = '".$job_id."' AND c.`charge_id` = '".$charge_id."' ";
			$q = $this->db->query($sql);
			
			$q = $this->db->query($sql);
			if($q->num_rows() > 0)
			{
				return $q->row();
			}else{
				$sql = "SELECT *,'an_fixed_job_charges' as tablename FROM an_fixed_job_charges AS cj, an_charges AS c WHERE 
						cj.`charge_id` = c.`charge_id`
						AND job_id = '".$job_id."'  AND c.`charge_id` = '".$charge_id."' ";
				$q = $this->db->query($sql);
				
				$q = $this->db->query($sql);
				if($q->num_rows() > 0)
				{
					return $q->row();
				}else{
					$sql = "SELECT *,'an_cash_job_charges' as tablename FROM an_cash_job_charges AS cj, an_charges AS c WHERE 
							cj.`charge_id` = c.`charge_id`
							AND job_id = '".$job_id."'  AND c.`charge_id` = '".$charge_id."'  ";
					$q = $this->db->query($sql);
					
					$q = $this->db->query($sql);
					if($q->num_rows() > 0)
					{
						return $q->row();
					}else{
						return false;
					}
				}
			}
		}else{
			return false;
		}
		
	}
	
	
//----------------------------------------------------------------------	
}
?>