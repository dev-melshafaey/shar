<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transaction_management_model extends CI_Model {
    /*
     * Properties
     */

//----------------------------------------------------------------------

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();

        //Load Table Names from Config
    }

    function getBranchCashId($branch_id = '') {
        $sql = "SELECT id from an_branch_cash where branch_id = " . $branch_id;
        $q = $this->db->query($sql);
        $res = $q->row();
        return $res->id;
    }

    function get_banks_list() {
        $sql = "SELECT * from an_banks";
        $q = $this->db->query($sql);
        $res = $q->result();
        return $res;
    }

    function get_bank_detail($id) {
        $sql = "SELECT * from an_banks where id=" . $id;
        $q = $this->db->query($sql);
        $res = $q->row();
        return $res;
    }

    function loadcancelamount($account_id) {
        $sql = ' SELECT 
    act.`value`,
    act.`transaction_type` 
  FROM
    `an_company_transaction` AS act 
  WHERE DATE(act.`clearance_date`) > CURDATE() 
    OR act.`clearance_date` IS NULL 
    AND act.`account_id` = ' . $account_id . '
  ORDER BY act.`account_id` ASC 
  LIMIT 1 ';
    }

    function loadamount($accountdId) {
        if ($accountdId != "") {
            $sql = "SELECT 
				  act.`value` ,act.`transaction_type`
				FROM
				  `an_company_transaction` AS act 
				  WHERE DATE(act.`clearance_date`) > CURDATE() 
  OR act.`clearance_date` IS NULL AND act.`account_id` = " . $accountdId . " 
				ORDER BY act.`account_id` ASC 
				LIMIT 1 ";
            $q = $this->db->query($sql);
            return $q->result();
        } else {
            return false;
        }
    }

    ///get remaing Customer Amount
    function getRemaiingCustomerAmount($job) {

        $sql = "SELECT p.`id`,p.`payment_amount`, ip.`payment_amount` AS used_amount, (p.`payment_amount` - ip.`payment_amount`) AS remaining_value FROM `an_jobs` AS j
					  LEFT JOIN `payments` AS p
					  ON p.`refrence_id` = j.`customer_id`
					  LEFT JOIN `invoice_payments` AS ip ON ip.`payment_id` = p.`id`
					  WHERE j.`id` = '" . $job . "' AND  p.`refernce_type` = 'onaccount'		
					  GROUP BY p.id";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    function insertNewtransaction($data) {
        //print_r($data);
        //if($data['account_id']=='' || !isset($data['account_id'])){
        //$data['branch_cash_id'] = $this->getBranchCashId($data['branch_id']);
        //}
        if ($data['payment_type'] == 'cheque') {
            $data['clearance_date'] = $data['cheque_date'];
        }
        //$data['userid'] = $this->session->userdata('userid');
        $data['emp_id'] = $this->session->userdata('userid');

        $this->db->insert('an_company_transaction', $data);
        return $this->db->insert_id();
    }

    function getCustomerId($jobId) {
        $sql = "SELECT j.`customer_id` FROM `an_jobs` AS j WHERE j.`id` = '" . $jobId . "'";
        $query = $this->db->query($sql);
        $r = $query->row();
        return $r->customer_id;
    }

    function getPaymentAdvance($jobId, $chargesId) {
        $sql = "SELECT  * FROM `job_payment` AS jp WHERE  jp.`job_id` = " . $jobId . " AND jp.`charges_id` = " . $chargesId . " AND jp.`advance` !=''";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function checkPaymentTransaction($job_id, $charges_id, $type) {
        $sbq = '';
        if ($type == 'Job') {
            $sbq = " AND jp.`charges_id` = " . $charges_id . " ";
        }

        $sql = "SELECT * FROM `job_payment` AS jp WHERE jp.`job_id` = " . $job_id . " " . $sbq . " AND jp.`payment_by`='" . $type . "'";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function LoadTransactionsWithFilter($account_id = '', $from = '', $to = '') {
        $sql = "SELECT 
				  t.id,
				  t.`job_id`,
				  t.payment_type,
				  t.value,
				  t.created,
				  t.transaction_type,
				  t.cheque_date,
				  IF(
					t.transaction_type = 'debit',
					t.value,
					'0'
				  ) AS 'debit',
				  IF(
					t.transaction_type = 'credit',
					t.value,
					'0'
				  ) AS 'credit',
				  t.value,
				  t.notes 
				FROM
				  an_company_transaction AS t
				   ";

        if ($account_id != '') {
            $sql .= "  WHERE  t.`account_id` = '" . $account_id . "'  ";
            $sql .= " and t.created  BETWEEN '" . $from . "' AND '" . $to . "'  ";
        } else {
            $sql .= " WHERE DATE(t.created)  BETWEEN DATE('" . $from . "') AND DATE('" . $to . "')  ";
        }
        $sql .= " order by t.id ASC ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function LoadTransactions($account_id = '') {
        $sql = "SELECT 
				  t.id,
				  t.`job_id`,
				  t.payment_type,
				  t.value,
				  t.created,
				  t.transaction_type,
				  t.cheque_date,
				  IF(
					t.transaction_type = 'debit',
					t.value,
					'0'
				  ) AS 'debit',
				  IF(
					t.transaction_type = 'credit',
					t.value,
					'0'
				  ) AS 'credit',
				  t.value,
				  t.notes 
				FROM
				  an_company_transaction AS t 
				  ";
        if ($account_id != '') {
            $sql .= "  WHERE t.`account_id` = '" . $account_id . "'  ";
        }
        $sql .= " order by t.id ASC ";


        $query = $this->db->query($sql);
        return $query->result();
    }

    function LoadCancelTransactions($account_id = '') {
        $sql = "SELECT 
				  t.id,
				  t.`job_id`,
				  t.payment_type,
				  t.value,
				  t.created,
				  t.transaction_type,
				  t.cheque_date,
				  IF(
					t.transaction_type = 'debit',
					t.value,
					'0'
				  ) AS 'debit',
				  IF(
					t.transaction_type = 'credit',
					t.value,
					'0'
				  ) AS 'credit',
				  t.value,
				  t.notes 
				FROM
				  an_company_transaction AS t 
				    WHERE DATE(t.`clearance_date`) > CURDATE() OR  t.`clearance_date`IS NULL 
				  ";
        if ($account_id != '') {
            $sql .= "  AND t.`account_id` = '" . $account_id . "'  ";
        }
        $sql .= " order by t.id ASC ";


        $query = $this->db->query($sql);
        return $query->result();
    }

    function LoadBankAccountByBankId($bank_id = '') {
        $sql = "SELECT 
				  * 
				FROM
				  an_bank_accounts 
				WHERE bank_id = '" . $bank_id . "' 
				  AND STATUS = 'Active' ";
        $q = $this->db->query($sql);
        return $q->result();
    }

    function get_transactionbyId($transction_id = '') {
        $sql = "SELECT 
				  * 
				FROM
				  an_company_transaction 
				WHERE id = '" . $transction_id . "' ";

        $q = $this->db->query($sql);
        return $q->row();
    }

    function LoadBankAccountExceptAccountId($account_id = '') {
        $sql = "SELECT 
				  * 
				FROM
				  an_bank_accounts 
				WHERE account_id != '" . $account_id . "' 
				  AND STATUS = 'Active' ";
        $q = $this->db->query($sql);
        return $q->result();
    }

    function updateBranchCash($branch_id = '', $amount = '0') {
        $sql = "UPDATE 
				  an_branch_cash 
				SET
				  branch_cash = branch_cash - $amount 
				WHERE branch_id = '" . $branch_id . "' ";
        $q = $this->db->query($sql);
        return true;
    }

    function update_transactionbyId($id = '', $cheque_date = '') {
        $sql = "UPDATE 
				  an_company_transaction 
				SET
				  cheque_date = '$cheque_date' 
				WHERE id = '" . $id . "' ";

        $q = $this->db->query($sql);
        return true;
    }

    function LoadAllCashManagement() {
        $sql = "SELECT 
				  * 
				FROM
				  an_branch,
				  an_branch_cash 
				WHERE an_branch.`branch_id` = an_branch_cash.`branch_id` ";
        $q = $this->db->query($sql);
        return $q->result();
    }

    function LoadAllbanksAccountsWithFilter($from = '', $to = '') {
        $sql = "SELECT 
  ab.*,atc.*,atc2.*,b.*,b.`created` AS account_created 
FROM
  `an_bank_accounts` AS ab
   INNER JOIN`an_banks` AS b
   ON b.`id`= ab.`bank_id`
  LEFT JOIN 
    (SELECT 
	act.`account_id`,SUM(act.`value`) AS totaldebit
    FROM
      `an_company_transaction` act 
    WHERE act.`transaction_type` = 'debit' GROUP BY act.`account_id`) AS atc
    ON atc.account_id = ab.`account_id`
      LEFT JOIN 
    (SELECT 
	act2.`account_id`,SUM(act2.`value`) AS totalcredit
    FROM
      `an_company_transaction` act2 
    WHERE act2.`transaction_type` = 'credit' GROUP BY act2.`account_id`) AS atc2
    ON atc2.account_id = ab.`account_id` 
	WHERE b.created  BETWEEN '" . $from . "' AND '" . $to . "' ";
        $q = $this->db->query($sql);
        return $q->result();
    }

    function printStatement($accountid, $from, $to) {

        $sql = "SELECT * 
FROM `an_company_transaction` AS t 
WHERE t.`account_id` = " . $accountid . " 
  AND t.`clearance_date` >= '" . $from . "' 
  AND t.`clearance_date` <= '" . $to . "'";
        $q = $this->db->query($sql);
        return $q->result();
    }

    function openingStatement($account_id, $date) {
        $sql = "SELECT 
  ab.*,atc.*,atc2.*,b.*,b.`created` AS account_created ,
	  ab.`account_id`  AS account_pk
	FROM
	  `an_bank_accounts` AS ab
	   INNER JOIN`an_banks` AS b
	   ON b.`id`= ab.`bank_id`
	  LEFT JOIN 
		(SELECT 
		act.`account_id`,SUM(act.`value`) AS totaldebit
		FROM
		  `an_company_transaction` act 
		WHERE act.`transaction_type` = 'debit'  AND act.`clearance_date` != '' 
			AND `clearance_date` <'" . $date . "' AND act.`account_id` =" . $account_id . ") AS atc
		ON atc.account_id = ab.`account_id`
		  LEFT JOIN 
		(SELECT 
		act2.`account_id`,SUM(act2.`value`) AS totalcredit
		FROM
		  `an_company_transaction` act2 
		WHERE act2.`transaction_type` = 'credit' AND act2.`clearance_date` != '' 
			AND `clearance_date` <'" . $date . "' AND  act2.`account_id` =" . $account_id . ") AS atc2
		ON atc2.account_id = ab.`account_id`";
        $q = $this->db->query($sql);
        return $q->row();
    }

    function LoadAllbanksAccounts() {
        $sql = "SELECT 
  ab.*,atc.*,atc2.*,b.*,b.`created` AS account_created ,
  ab.`account_id`  AS account_pk
FROM
  `an_bank_accounts` AS ab
   INNER JOIN`an_banks` AS b
   ON b.`id`= ab.`bank_id`
  LEFT JOIN 
    (SELECT 
	act.`account_id`,SUM(act.`value`) AS totaldebit
    FROM
      `an_company_transaction` act 
    WHERE act.`transaction_type` = 'debit'  AND act.`clearance_date` != '' 
        AND `clearance_date` <= CURRENT_DATE() GROUP BY act.`account_id`) AS atc
    ON atc.account_id = ab.`account_id`
      LEFT JOIN 
    (SELECT 
	act2.`account_id`,SUM(act2.`value`) AS totalcredit
    FROM
      `an_company_transaction` act2 
    WHERE act2.`transaction_type` = 'credit' AND act2.`clearance_date` != '' 
        AND `clearance_date` <= CURRENT_DATE() GROUP BY act2.`account_id`) AS atc2
    ON atc2.account_id = ab.`account_id`";
        $q = $this->db->query($sql);
        return $q->result();
    }

    function updateBankAccountCash($account_id = '', $amount = '0') {
        $sql = "UPDATE 
				  an_bank_accounts 
				SET
				  account_cash = account_cash - $amount 
				WHERE account_id = '" . $account_id . "' ";
        $q = $this->db->query($sql);
        return true;
    }

    function updateBankAccountCashSender($account_id = '', $amount = '0') {
        $sql = "UPDATE 
				  an_bank_accounts 
				SET
				  account_cash = account_cash + $amount 
				WHERE account_id = '" . $account_id . "' ";
        $q = $this->db->query($sql);
        return true;
    }

    function udpateJobStatus($job_id = '') {
        $data['job_status'] = 'close';
        $this->db->where('id', $job_id);
        $this->db->update('an_jobs', $data);
        return true;
    }

    function udpateJobStatusPaid($job_id = '', $charge_id = '') {
        $rec = $this->getJobType($job_id, $charge_id);

        $tablename = $rec->tablename;

        $data['is_company_paid'] = '1';
        $this->db->where('job_id', $job_id);
        $this->db->where('charge_id', $charge_id);
        $this->db->update($tablename, $data);
        return true;
    }

    // Created By M.Ahmed

    function getJobType($job_id = '', $charge_id = '') {
        if ($job_id != '') {
            $sql = "SELECT *,'an_contract_job_charges' as tablename FROM an_contract_job_charges AS cj, an_charges 													AS c WHERE 
					cj.`charge_id` = c.`charge_id`
					AND job_id = '" . $job_id . "' AND c.`charge_id` = '" . $charge_id . "' ";
            $q = $this->db->query($sql);

            $q = $this->db->query($sql);
            if ($q->num_rows() > 0) {
                return $q->row();
            } else {
                $sql = "SELECT *,'an_fixed_job_charges' as tablename FROM an_fixed_job_charges AS cj, an_charges AS c WHERE 
						cj.`charge_id` = c.`charge_id`
						AND job_id = '" . $job_id . "'  AND c.`charge_id` = '" . $charge_id . "' ";
                $q = $this->db->query($sql);

                $q = $this->db->query($sql);
                if ($q->num_rows() > 0) {
                    return $q->row();
                } else {
                    $sql = "SELECT *,'an_cash_job_charges' as tablename FROM an_cash_job_charges AS cj, an_charges AS c WHERE 
							cj.`charge_id` = c.`charge_id`
							AND job_id = '" . $job_id . "'  AND c.`charge_id` = '" . $charge_id . "'  ";
                    $q = $this->db->query($sql);

                    $q = $this->db->query($sql);
                    if ($q->num_rows() > 0) {
                        return $q->row();
                    } else {
                        return false;
                    }
                }
            }
        } else {
            return false;
        }
    }
    
    function udpateb($id='',$data) {
        $this->db->where('id', $id);
        $this->db->update('an_banks', $data);
        return true;
    }

//----------------------------------------------------------------------	
}

?>