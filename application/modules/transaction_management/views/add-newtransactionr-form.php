
<div class="row form-wrapper">
    <div class=" col-xs-12">
        <div id="main-content" class="main_content"> 
          <!--      <div class="title title alert alert-info"> <span><?php echo lang('add-edit') ?></span> </div>
          <div class="notion title title alert alert-info"> * <?php echo lang('mess1') ?> </div>-->
            <?php error_hander($this->input->get('e')); ?>
            <form action="<?php echo base_url(); ?>transaction_management/create_new_account" method="post" id="form1" class="" name="frm_customer" autocomplete="off">
                <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
                <input type="hidden" name="userid" id="userid" value="<?php echo $user->userid; ?>" />
                <div class="form">
                    <div class="">
                        <div class="g4 form-group">
                            <label class="text-warning"><?php echo lang('Company-Name') ?> </label>
                            <div class="">
                                <div class="ui-select" style="width:100%">
                                    <div class="">
                                        <?php company_dropbox('companyid', $user->companyid); ?>
                                        <span class="arrow arrowselectbox">&amp;</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="g4">
                            <label class=""><?php echo lang('Branch-Name') ?></label>
                            <div class="">
                                <div class="ui-select" >
                                    <div class="">

                                        <?php //company_branch_dropbox('branchid', $user->branchid, $user->companyid); ?>
                                        <?php company_branch_dropbox('branchid', $user->branchid, $user->companyid); ?>
                                        <span class="arrow arrowselectbox">&amp;</span>
                                    </div>
                                </div>
                            </div>



                        </div> 

                        <div class="g4 form-group">
                            <label class="col-lg-12 text-warning"><?php echo lang('accountTitle') ?></label>
                            <input name="account_name" id="account_name" value="" type="text"   class="form-control" style=""/>
                        </div>
                        <div class="g4 form-group">
                            <label class="col-lg-12 text-warning"><?php echo lang('accountNumber') ?></label>
                            <input name="account_number" id="account_number" value="" type="text"   class="form-control" style=""/>
                        </div>
                        <br  clear="all"/> 
                        <div class="g4 form-group">
                            <label class="text-warning"><?php echo lang('Banks') ?> </label>
                            <div class="">
                                <div class="ui-select" style="width:100%">
                                    <div class="">
                                        <?php company_bank_dropbox('bank_id', '', 'english', ' onchange="getBankAccounts();" '); ?>
                                        <span class="arrow arrowselectbox">&amp;</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="g4 form-group">
                            <label class="col-lg-12 text-warning"><?php echo lang('openingBalance') ?> </label>
                            <input name="opening_balance" id="opening_balance" value="<?php //echo isset($customer->credit_limit)?$customer->credit_limit:'';  ?>" type="text"  class="form-control"/>
                        </div>
                        <div class="g4 form-group">
                            <label class="col-lg-12 text-warning"><?php echo lang('clearanceDate') ?></label>
                            <input name="clearance_date" id="clearance_date" value="" type="text"  class="datapic_input form-control" style=""/>
                        </div>
                        <div class="g4 form-group">
                            <label class="col-lg-12 text-warning"><?php echo lang('Notes') ?></label>
                            <textarea class="form-control" name="notes" id="notes"><?php echo $user->notes; ?></textarea>
                        </div>
                        <br  clear="all"/> 
                        <div class=" form-group">
                            <input type="hidden" name="member_type" id="member_type" value="6">
                            <input name="sub_mit" id="sub_mit" type="submit" class="btn-flat primary green flt-r g3" value="<?php echo lang('Add') ?>" />
                            <input name="sub_reset" type="reset" class="btn-flat primary gray flt-r g3" value="<?php echo lang('Reset') ?>" />

                        </div>

                    </div>


                    <!--end of raw--> 
                </div>


            </form>
        </div>
        <!-- END PAGE --> 
    </div>
</div>
