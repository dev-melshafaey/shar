<div class="table-wrapper users-table">
  <form action="<?php //echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">

    <div class="row">
      <div class="col-md-12">

    <div id="main-content" class="main_content">


        
    <div class="title title alert alert-info">
        <span><?php breadcramb(); ?> >> <?php echo lang('add-edit') ?></span>
    </div>
    <div class="row head">
      <div class="col-md-12">
       
        <?php error_hander($this->input->get('e')); ?>
      </div>
    </div>
		<form id="form_transactions" action="<?php echo base_url();?>transaction_management/add_transaction" method="post" enctype="multipart/form-data">
			<input type="hidden" id="job_charges_id" name="job_charges_id" />
			<div class="form" id="form-refresh">
          
				<input type="hidden"  name="branch_id" name="branch_id" value="<?php echo $user->branchid;  ?>" />
                
                 <input  type="hidden" name="transaction_by" class="transaction_by" value="Job" id="transaction_type1">
                <div class="raw form-group" id="div_jobs">
                    <div class="form_title">Jobs</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select" id="div_jobs_responce">
						 <select name="jobs" id="jobs" onchange="getCharges(this)">
                            <option value="">Select</option>
                            <?php
								foreach($jobs as $job){
									?>
                                   <option value="<?php echo $job->id; ?>" ><?php echo $job->id; ?></option> 
                                    <?php
								}
							?>
                          </select>
                			           
                        </div>
                      </div>
                    </div>
                </div>
               
     <div class="raw form-group" style="display:none;" id="bl_div">
                        <div class="form_title">Bl Number: </div>
                        <div class="form_field">
                            <span id="bl_span"></span>
                        </div>
                    </div>                <div class="raw form-group" id="div_invoices" style="display:none;">
                    <div class="form_title">Invoices</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select" id="div_jobs_responce">
						 <select name="invoice" id="invoice" onchange="getAmount(this)">
                            <option value="">Select</option>
                            <?php
								foreach($invoices as $invoice){
									?>
                                   <option value="<?php echo $invoice->job_id; ?>" ><?php echo $invoice->invoice_id; ?></option> 
                                    <?php
								}
							?>
                          </select>
                			           
                        </div>
                      </div>
                    </div>
                    
                </div>
                
				<?php
						  if(!empty($jobs)){
								  foreach($jobs as $job){
								   ?>
								   <input type="hidden" name="<?php echo $job->id; ?>" id="<?php echo $job->id; ?>" class="jobs_way" value="<?php echo $job ->way_of_shipment;?>" />
								   <?php
								  }
						  }
						  
						   ?>
                <div class="raw form-group" id="div_charges" style="display:none;">
                    <div class="form_title">Charges</div>
                    <div class="form_field">
                        <div class="dropmenu">
                            <div class="styled-select" id="div_charges_responce">
                            <select name="charges_job" id="charges_job">
                          </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="raw form-group">
                    <div class="form_title">Transaction Type</div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
                          <select name="transaction_type" id="transaction_type">
                            <option value="debit">Debit</option>
                            <option value="credit">Credit</option>
                          </select>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="raw form-group">
                    <div class="form_title">Amount: </div>
                    <div class="form_field">
                        <input name="value" id="value" type="text"  class="formtxtfield_small"/>
                        <span>RO.</span>
                    </div>
                    <div class="form_title">Amount Type</div>
                    <div class="form_field">
                        <div class="dropmenu">
                            <div class="styled-select">
                            <select name="amount_type" id="amount_type">
                          	<option value="">Select</option>
                          	<option value="advance">Advance Received</option>
                            <option value="sales">Sales</option>
                            <option value="purchase">Purchase</option>
                          </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="raw form-group" style="display:none;" id="payment_payment">
                    <div class="form_title">Paid Amount: </div>
                    <div class="form_field">
						<span id="paid_amount"></span>
                        <span>RO.</span>
                    </div>
                </div>-->
                <!--<div class="raw form-group" style="display:none;" id="payment_payment">
                    <div class="form_title">Paid To: </div>
                    <div class="form_field">
						<span id="paid_amount"></span>
                        <span>RO.</span>
                    </div>
                </div>-->
                <div class="raw form-group">
                <div class="form_title">Payment Type</div>
                    <div class="form_field">
                        <div class="dropmenu">
                            <div class="styled-select">
                                <select name="payment_type" id="payment_type" onchange="BankOrCashToggle();">
                                    <option value="0">Select Payment Way</option>
                                    <option value="transfer">Transfer</option>
                                    <option value="deposite">Deposite</option>
                                    <option value="cheque">Cheque</option>
                                    <option value="withdraw">Withdraw</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="raw form-group" style="display:none" id="cheque_div_date">
                    <div class="form_title">Cheque Date </div>
                    <div class="form_field">
                        <input name="cheque_date" id="cheque_date" type="text"  class="formtxtfield"/>
                        
                    </div>
                </div>
                <div class="raw form-group" style="display:none" id="cheque_div_clearance">
                    <div class="form_title">Cheque Clearance Date: </div>
                    <div class="form_field">
                        <input name="clearance_date" id="clearance_date" type="text"  class="formtxtfield"/>
                        
                    </div>
                </div>
                <div class="raw form-group" style="display:none" id="cheque_div_number">
                    <div class="form_title">Cheque Number </div>
                    <div class="form_field">
                        <input name="cheque_number" id="cheque_number" type="text"  class="formtxtfield"/>
                        
                    </div>
                </div>
                
                
                <div class="raw form-group" id="div_banks">
                    <div class="form_title">Banks </div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
                          <?php company_bank_dropbox('bank_id','','english',' onchange="getBankAccounts();" ');?>
                        </div>
                      </div>
                    </div>
                </div>
                
                <div class="raw form-group" style="display:none" id="div_accounts">
                    <div class="form_title">Accounts </div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select" id="div_accounts_responce">
                          
                        </div>
                      </div>
                    </div>
                </div>
                
                <div class="raw form-group" id="div_checkque" style="display:none;">
                    <div class="form_title">Deposite Recipt </div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select">
	                        <input type="file"  class="formtxtfield" name="deposite_recipt" id="deposite_recipt" />
                        </div>
                      </div>
                    </div>
                </div>
                
               
                
                
                <div class="raw form-group" style="display:none" id="div_tr_accounts">
                    <div class="form_title">Accounts </div>
                    <div class="form_field">
                      <div class="dropmenu">
                        <div class="styled-select" id="div_tr_accounts_responce">
                          
                        </div>
                      </div>
                    </div>
                </div>
                
                <div class="raw">
                    <div class="form_title">Notes </div>
                    <div class="form_field">
                        <textarea name="notes" id="notes" cols="" rows="" class="formareafield"></textarea>
                    </div>
                </div>
                
                <div class="raw" align="center">
                    <input name="submit_transaction" id="submit_transaction" type="submit" class="submit_btn" value="Add" />
                    <!-- <input name="id" type="hidden"  value="" /> -->
                    
                </div>
                <!--end of raw-->
			</div>
		</form>
		<!-- END PAGE -->  
	</div>
    <!-- END PAGE -->  
</div>
</div>
</div>

<script>
function getAmount(obj){
	inv_id = $(obj).val();
	if(inv_id!= ""){
	$.ajax({
				url: config.BASE_URL + "ajax/getinvoice_amount",
				type: 'post',
				data:{inv_id:inv_id},
				cache: false,
				success: function(data){
					console.log(data);
					//res = $.parseJSON(data);
					//console.log(res);
					//res.sales;
					//$("#value").val(res.sales);
					//$("#bl_span").html(res.bl);
					//$("#bl_div").show();
					
				}
				});
	}
}

$(function() {
		$(".transaction_by").click(function(){
				transaction_val = $(this).val();
				if(transaction_val == 'Job'){
					$("#div_jobs").show();
					$("#div_invoices").hide();
				}
				else{
					
					$("#div_jobs").hide();
					$("#div_invoices").show();
				}
		
		});
		
		$("#payment_type").change(function(){
				pval = $(this).val();	
				if(pval == 'cheque'){
					$("#cheque_div_clearance").show();
				}
				else{
					$("#cheque_div_clearance").hide();
				}
		})
		
		$("#jobs").change(function(){
			
				//alert('change');
				jb_val = $(this).val();
				
				jb_type= $("#"+jb_val).val();
				//alert(jb_type);
				$.ajax({
				url: config.BASE_URL + "ajax/getcharges_ajax",
				type: 'post',
				data:{jb_type:jb_type,jb_val:jb_val},
				cache: false,
				success: function(data){
					res = jQuery.parseJSON(data);
					console.log(res);
					//console.log(res);
					$("#value").val(res.sales);
					$("#bl_span").html(res.bl);
					$("#bl_div").show();
			
					$("#charges_job").html(res.html);
					$("#div_charges").fadeIn();
				}
				});
			
			});
		
		$("#submit_transaction").click(function(){
						$(".jobs_way").remove();
		});
				
		$("#charges_job").change(function(){
				
				c_id = $(this).val();
				jobId	= $("#jobs").val();
				
				$.ajax({
				url: config.BASE_URL + "ajax/check_charges",
				type: 'post',
				data:{c_id:c_id,job_id:jobId},
				cache: false,
				success: function(data){
					
						//console.log(data+'data');
						res = $.parseJSON(data);
						console.log(res+'res');
						if(res.jb_c_id){
							$("#job_charges_id").val(res.jb_c_id);
						}
						
						if(res.is_actual== '1'){
							//alert(res.is_actual);
							
							/*if(res.charges_advance){
								$("#advance").val(res.charges_advance);	
							}*/
							
							if(res.charges_advance){
								$("#advance").val(0);	
							}
							
							$("#advance_payment").show();
						}
						else{
							$("#advance_payment").hide();
						}
						
						if(res.total_value){
								//$("#value").val(res.total_value);	
						}
						
								//$("#paid_amount").html(res.payment_amount);
								$("#payment_payment").show();
						
							
					//$("#charges_job").html(data);
					//$("#div_charges").fadeIn();
				}
				});
				
				$("#submit_cash").click(function(){
						$(".jobs_way").remove();
				});
			
			})
				
        $('#cheque_date').datepicker({dateFormat: 'yy-mm-dd'});
		$('#clearance_date').datepicker({dateFormat: 'yy-mm-dd'});
		
    });
</script>
<!-- End Section-->
<!--footer-->
