
<div class="table-wrapper users-table">
    <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">

        <?php error_hander($this->input->get('e')); ?>

        <div class="">
            <div class="">
                <?php /* action_buttons('addnewcustomer', $cnt); */ ?>
                <table id="new_data_table">
                    <thead class="thead">
                        <tr>
                            <th  id="no_filter"><label for="checkbox">
                                    <!--<input type="checkbox" class="" id="checkAll" data-set=""/>-->
                                </label></th>
                            <th ><?php echo lang('accountTitle') ?></th>
                            <th ><?php echo lang('accountNumber') ?></th>
                            <th ><?php echo lang('Debit') ?></th>
                            <th ><?php echo lang('Credit') ?></th>
                            <th ><?php echo lang('Total') ?></th>
                            <!--
                            <th ><?php echo lang('Debit') ?></th>
                            <th ><?php echo lang('Credit') ?></th>
                            <th ><?php echo lang('Total') ?></th>-->
                            <th  id="no_filter">&nbsp;</th>
                        </tr>
                    </thead>
                    <?php
                    $cnt = 0;
                    foreach ($this->transaction_management->LoadAllbanksAccounts() as $userdata) {
                        $cnt++;
                        ?>
                        <tr>
                            <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $userdata->account_pk; ?>" value="<?php echo $userdata->account_id; ?>" /></td>
                            <td ><a href="<?php echo base_url(); ?>customers/invoices/<?php echo $userdata->account_pk; ?>"><?php echo $userdata->account_name; ?></a></td>
                            <td><?php echo $userdata->account_number; ?></td>
                            <td class="green"><?php echo ($userdata->totaldebit == '') ? '00:00' : $userdata->totaldebit; ?></td>
                            <td class="red"><?php echo ($userdata->totalcredit == '') ? '00:00' : $userdata->totalcredit; ?></td>
                            <td><?php $total = $userdata->totaldebit - $userdata->totalcredit;
                    echo ($total == '') ? '00:00' : $total;
                        ?></td>
                            <td>            
                                <a  href="<?php echo base_url(); ?>transaction_management/index/<?php echo $userdata->account_pk; ?>"><i class="icon-eye-view"></i></a>
                                <!--<a onClick="getCustomerData('<?php echo $userdata->account_id; ?>');" data-toggle="modal" href="#myModal"><i class="icon-search"></i></a>--> 


                            </td>
                        </tr>
<?php } ?>
                </table>
            </div>
        </div>
        <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>

<!--<input type="submit" class="send_icon" value=""/>-->
    </form>
</div>
