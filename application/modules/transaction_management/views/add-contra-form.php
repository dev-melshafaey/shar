
<div class=" form-wrapper">
  <div class=" col-xs-12">
    <div id="main-content" class="main_content"> 
      <!--      <div class="title title alert alert-info"> <span><?php echo lang('add-edit') ?></span> </div>
      <div class="notion title title alert alert-info"> * <?php echo lang('mess1') ?> </div>-->
      <?php error_hander($this->input->get('e')); ?>
      <form action="<?php echo base_url();?>transaction_management/add_contra_submit" method="post" id="form1" class="" name="frm_customer" autocomplete="off">
        <input type="hidden" name="userid" id="userid" value="<?php echo $user->userid; ?>" />
        <div class="form">
          <div class="">
          	<div class="g3 form-group "><label><?php echo lang('transfer_from'); ?></label><div class="row inp-cont"><label><?php echo lang('cash'); ?></label><input name="transfer_type" class=" transfer_type" value="cash" id="transfer_type1"  type="radio"></div></div>
            <div class="g3 form-group ">
            <label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
            	<div class="row inp-cont">
            <label><?php echo lang('bank'); ?></label><input name="transfer_type" class=" transfer_type" value="bank" id="transfer_type"  type="radio"></div>
            </div> 
         	<br  clear="all"/>
            <div class="g4 form-group banks" id="bank_div" style="display:none;">
                <label class="text-warning"><?php echo lang('Banks') ?> </label>
                <div class="">
                    <div class="ui-select" style="width:100%">
                        <div class="">
                         <?php company_bank_dropbox('bank_id','','english',' onchange="getBankAccounts();" ');?>
                            <span class="arrow arrowselectbox">&amp;</span>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="g4 form-group" style="display:none" id="div_accounts">
                    <label class="text-warning"><?php echo lang('accounts'); ?> </label>
                   <div class="div_accounts">
                    <div class="ui-select" style="width:100%">
                     <div class="" id="div_accounts_responce">
                          
                        </div>
                      </div>
                    </div>
                </div>
            <div class="g4 form-group cashbranch" style="display:none;">
                <label class="text-warning"><?php echo lang('cash_branch') ?> </label>
                <div class="">
                    <div class="ui-select" style="width:100%">
                        <div class="">
                       	<?php echo getCashBranhes('from',1); ?>
                            <span class="arrow arrowselectbox">&amp;</span>
                        </div>
                    </div>
                </div>
            </div>
            <br  clear="all"/>
              <div class="g3 form-group "><label><?php echo lang('transfer_to'); ?></label><div class="row inp-cont"><label><?php echo lang('cash'); ?></label><input name="transfer_to_type" class="transfer_to_type" value="cash" id="transfer_to_type1"  type="radio"></div></div>
            <div class="g3 form-group ">
            <label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
            	<div class="row inp-cont">
            <label><?php echo lang('bank'); ?></label><input name="transfer_to_type" class=" transfer_to_type" value="bank" id="transfer_to_type"  type="radio"></div>
            </div>
            <br clear="all" />
            <div class="g4 form-group bank_to_div" id="bank_to_div" style="display:none;">
                <label class="text-warning"><?php echo lang('Banks') ?> </label>
                <div class="">
                    <div class="ui-select" style="width:100%">
                        <div class="">
                         <?php company_bank_dropbox('bank_to_id','','english',' onchange="getBankAccountsTo();" ');?>
                            <span class="arrow arrowselectbox">&amp;</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="g4 form-group" style="display:none" id="div_to_accounts">
                    <label class="text-warning"><?php echo lang('accounts'); ?> </label>
                   <div class="div_accounts">
                    <div class="ui-select" style="width:100%">
                     <div class="" id="div_to_accounts_responce">
                          
                        </div>
                      </div>
                    </div>
                </div>
            <div class="g4 form-group cashtobranch" style="display:none;">
                <label class="text-warning"><?php echo lang('cash_branch') ?> </label>
                <div class="">
                    <div class="ui-select" style="width:100%">
                        <div class="">
                       	<?php echo getCashBranhes('to',1); ?>
                            <span class="arrow arrowselectbox">&amp;</span>
                        </div>
                    </div>
                </div>
            </div>
            <br clear="all"    />
            <div class="g4 form-group">
                    <label class="col-lg-12 text-warning"><?php echo lang('amount') ?> </label>
             		<input name="value" id="value" type="text"  class="form-control"/>
             </div>
             <div class="g4 form-group">
                            <label class="col-lg-12 text-warning"><?php echo lang('clearanceDate') ?></label>
                            <input name="clearance_date" id="clearance_date" value="" type="text"  class="datapic_input form-control" style=""/>
                        </div>
                        <div class="g4 form-group">
              <label class="col-lg-12 text-warning"><?php echo lang('Notes') ?></label>
				<textarea class="form-control" name="notes" id="notes"><?php echo $user->notes; ?></textarea>
            </div>
                <br  clear="all"/> 
                <div class=" form-group">
              	<input type="hidden" name="member_type" id="member_type" value="6">
              <input name="sub_mit" id="sub_mit" type="submit" class="btn-flat primary green flt-r g3" value="<?php echo lang('Add') ?>" />
              <input name="sub_reset" type="reset" class="btn-flat primary gary flt-r g3" value="<?php echo lang('Reset') ?>" />
              
                </div>
                
                     </div>
          
          
          <!--end of raw--> 
        </div>
        
 
      </form>
    </div>
    <!-- END PAGE --> 
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
		//alert('ready');
		$(".transfer_type").click(function(){
			//alert('asd');
			c_val = $(".transfer_type:checked").val();
				// c_val = $(this).val();
				//alert(c_val);
				if(c_val == 'cash'){
					//ttp = $("#transfer_type").is(":checked");
					$(".cashbranch").show();
					$(".banks").hide();
					$(".div_accounts").hide();
					//alert(ttp);
					
				}
				else{
					$(".banks").show();
					$(".cashbranch").hide();
					//ttp = $("#transfer_type1").is(":checked");
					//alert(ttp);
				}
		});
		$(".transfer_to_type").click(function(){
			//alert('asd');
			c_val = $(".transfer_to_type:checked").val();
				// c_val = $(this).val();
				//alert(c_val);
				if(c_val == 'cash'){
					//ttp = $("#transfer_type").is(":checked");
					$(".cashtobranch").show();
					$(".bank_to_div").hide();
					$("#div_to_accounts").hide();
					//alert(ttp);
					
				}
				else{
					$(".bank_to_div").show();
					$(".cashtobranch").hide();
					//ttp = $("#transfer_type1").is(":checked");
					//alert(ttp);
				}
		});
});
</script>