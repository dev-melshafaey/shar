<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>

<style type="text/css">
@import "compass/css3";
/* elements */
body {
  font: 400 16px 'Muli', sans-serif !important;
  margin: 0;
  padding: 0;
}

header {
  margin: 0;
  max-width: 100%;
  padding: 5px;
  text-align: center;
  overflow: auto;
}

ul{
  list-style-type: none;
  margin: 0 auto;
  padding: 0
}

li{
  background: slategrey;
  display: inline-block;
  margin: 5px;
  padding: 5px 7px;
}

li > a {
  color: white; 
  font-size: 16px;
}

li > a:hover{
  color: #262626;
  text-decoration: none;
}

.inner {
  padding: 30px;
}
/* headings */

.container-fluid h2 {
  font-family: 'Montserrat', sans-serif;
}

.site-title {
  font-size: 50px;
  font-weight: 300;
  text-transform: uppercase;
}

/* text colors */

.black,
.k {
  font-color: #262626;
}

/* background colors */

.sq {
  /*  alignment  */
  float: left;
  margin: 5px;
  /*  size  */
  width: 200px;
  height: 200px;
  /*  box text  */
  color: #262626;
/*   font: 200 16px/180px 'Muli', helvetica; */
  text-align: center;
}

.sq:hover {
  border: 6px solid rgba(255,255,255, 0.5);
}

.sq p {
  vertical-align: middle;
  text-align: center;
  position: relative;
  top: 40px;
}

.c {
  display: block;
  width: 100px;
  height: 100px;
  border-radius: 100%;
  margin: 10px;
}

/* table */
table{
  margin: 10px auto;
}

table > tbody > tr.tableizer-firstrow > th {
  padding: 10px;
  background: #7aaef6;
}

body > div.container-fluid.inner > table > tbody > tr > td{
  border: 4px solid #fff;
  width: 170px;
  padding: 10px;
  background: #f8f8f8;
}
</style>

</head>

<body>
<div class="container-fluid inner">
  
<table width="100%" class="tableizer-table">
  
  <tr>
    <td class="tg-asmn" colspan="4" align="center" style="border-bottom:2px solid #333;border-top:2px solid #333;border-left: 0;border-right: 0; background-color:#666; color:#fff"><strong>Account Details</strong></td>
  </tr>
  
  <tr style="border:hidden;">
    <td style="border: hidden; text-align: ; font-weight: bold;" class="tg-0ord">Account number:</td>
    <td style="border: hidden; color: #06F;" class="tg-031e"><?php echo $opening_balance->account_number; ?></td>
    <td style="border: hidden; color: #06F;" class="tg-031e" colspan="2">&nbsp;</td>
  </tr>
  </table>
  <table width="100%" class="tableizer-table">
  <tr class="tableizer-firstrow">
    <th width="11%" class="tg-t3kt">Transaction No</th>
    <th width="59%" class="tg-t3kt">Description</th>
    <th width="15%" class="tg-t3kt">Debit</th>
    <th width="15%" class="tg-t3kt">Credit</th>
  </tr>
  <tr>
                <td class="tg-s6z2"></td>
                <td class="tg-031e">Opening Balance</td>
                <td class="tg-031e">OMR <?php echo $opening_balance->totaldebit-$opening_balance->totalcredit; ?></td>
                <td class="tg-2thk"></td>
              </tr>
               
  <?php

  	if(!empty($print_account_sheet)){
			$counter = 1;
			$totaldebit = '';
			$totalcredit = '';
			foreach($print_account_sheet as $account_sheet){
				?>
	           <tr>
                <td class="tg-s6z2"><?php echo $counter ; ?></td>
                <td class="tg-031e"><?php echo 'Payment Type '.$account_sheet->payment_type.'<br> Notes '.$account_sheet->notes; ?></td>
                <td class="tg-031e"> <?php  if($account_sheet->transaction_type == 'debit') { echo 'OMR '.$account_sheet->value; $totaldebit += $account_sheet->value;}?></td>
                <td class="tg-2thk"> <?php  if($account_sheet->transaction_type == 'credit') {  echo 'OMR '.$account_sheet->value; $totalcredit += $account_sheet->value; }?></td>
              </tr>
                <?php
				$counter++;
			}
	}
  ?>
  <tr>
    <td class="tg-s6z2" colspan="2" style="font-weight:bold">TOTAL:</td>
    <td class="tg-hgcj">OMR <?php echo $totaldebit; ?></td>
    <td class="tg-2thk">OMR <?php echo $totalcredit; ?></td>
  </tr>
  <tr>
    <td colspan="2" rowspan="2"  class="tg-hgcj" style="font-weight:bold">Clsoing Balance</td>
    <td class="tg-ipa1" style="font-weight:bold">OMR <?php echo $totaldebit-$totalcredit; ?></td>
  </tr>
</table>


</div>
</body>
</html>
