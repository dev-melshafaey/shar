<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['main']= 'Dashboard';
$lang['Customer-Name']= 'Customer Name';
$lang['Number-of-Invoices']= 'Number of Invoices';
$lang['Mobile-Number']= 'Mobile Number';
$lang['Type']= 'Type';
$lang['Code']= 'Code';
$lang['Debit']= 'Debit';
$lang['Credit']= 'Credit';
$lang['Total']= 'Total';
$lang['transfer_type']= 'Transfer Type';

$lang['Debitrecancel']= 'Debit';
$lang['Creditrecancel']= 'Credit';
$lang['Totalrecancel']= 'Total';
$lang['transction_id'] = 'Transaction Id';
$lang['payment_type'] ='Payment Type';
$lang['transaction_amount'] ='Transation Amount';
$lang['date_transaction'] ='Date of Transaction';
$lang['cash_branch'] ='Cash Branch';
$lang['transfer_from'] ='Transfer From';
$lang['transfer_to'] ='Transfer To';
$lang['Status']= 'Status';
$lang['Notes']= 'Notes';
$lang['Responsable-Phone']= 'Responsable Phone';
$lang['Responsable-Name']= 'Responsable Name';
$lang['Address']= 'Address';
$lang['Email-Address']= 'Email-Address';
$lang['Fax-Number']= 'Fax-Number';
$lang['Contact-Number']= 'Contact-Number';

$lang['All']= 'All';
$lang['Category-Name']= 'Category Name';
$lang['Company-Name']= 'Company Name';
$lang['Category-Type']= 'Category Type';
$lang['Description']= 'Description';
$lang['Status']= 'Status';
$lang['Action']= 'Action';


$lang['accountTitle'] = 'Account Title';
$lang['accountNumber'] = 'Account Number';
$lang['clearanceDate'] = 'Clearance Date';
$lang['openingBalance'] ='Opening Balance';
$lang['Banks'] ='Bank';
$lang['debite'] ='Debit';
$lang['credite']='Credit';
$lang['balancee']='Balance';
$lang['accountant_name'] ='Accountant Name';

$lang['Item-Picture']= 'Item Picture';
$lang['Name']= 'Name';
$lang['Supplier-Name']= 'Supplier Name';
$lang['Serial-Number']= 'Serial Number';
$lang['Store']= 'Store';
$lang['Purchase-Price']= 'Purchase Price';
$lang['Sale-Price']= 'Sale Price';
$lang['Min-Sale-Price']= 'Min Sale Price';
$lang['Point-of-Order']= 'Point of Order';
$lang['Quantity']= 'Quantity';
$lang['Date']= 'Date';
$lang['Location-Address']= 'Location &amp; Address';
$lang['Finacial-Type']= 'Finacial Type';
$lang['Store-Manager']= 'Store Manager';
$lang['Manager-Phone']= 'Manager-Phone';
$lang['Total-Qunatity-Available']= 'Total Qunatity Available';

$lang['Rent-Value']= 'Rent Value';
$lang['Per']= 'Per';
$lang['Date-Of-Paid-Rent']= 'Date Of Paid Rent';
$lang['Owner-Phone']= 'Owner Phone';
$lang['Store-Phone-Number']= 'Store Phone Number';
$lang['Fax']= 'Fax';

$lang['Capcity']= 'Capacity';
$lang['Note']= 'Note';
$lang['Date-Of-Paid']= 'Date Of Paid';
$lang['Instock']= 'Instock';
$lang['Point-of-Sale']= 'Point of Sale';
$lang['Net-Cost']= 'Net-Cost';
$lang['Net-Profit']= 'Net-Profit';


$lang['users']= 'Sales Person ';
$lang['sales']= 'Sales ';
$lang['purchase']= 'Purchase ';
$lang['DAY']= 'DAY ';
$lang['MONTH']= 'MONTH ';
$lang['YEAR']= 'YEAR ';
$lang['noti_alert']= 'You have 1 new notifications ';
$lang['noti_alert1']= 'New user registration ';
$lang['Your_account']= 'Your account';
$lang['Personal_info']= 'Personal info';
/*Form*/

$lang['add-edit']= 'Add & Edit Customer';
$lang['mess1']= 'Please Understand Clearly All The Data Before Entering';
$lang['Add']= 'Add';
$lang['Reset']= 'Reset';
$lang['Branch-Name']= 'Branch Name';
$lang['RePassword']= 'RePassword';
$lang['Password']= 'Password';
$lang['User-Name']= 'User Name';
$lang['Add-Account']= 'Add Account';
$lang['Company-Name']= 'Company-Name';

$lang['bank']= 'Bank';
$lang['cash']= 'Cash';
//البنك
//في النقد
$lang['add_tabss']= 'Add';
$lang['view_all_tabss']= 'View all';


/**/

$lang['show_datatable']= 'Showing _START_ to _END_ of _TOTAL_ entries';
$lang['Previous']= 'Previous';
$lang['First']= 'First';
$lang['Last']= 'Last';
$lang['Next']= 'Next';
$lang['Search']= 'Search';
$lang['show_bylist']= 'Show _MENU_ entries';
$lang['transaction_no']='Transaction No';
$lang['amount']='Amount';
$lang['transaction_type'] ='Transaction Type';
$lang['accounts'] = 'Accounts';
$lang['Account-Number'] = 'Account Number';
