<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction_management extends CI_Controller
{
	/*
	* Properties
	*/

    private $_data	=	array();

//----------------------------------------------------------------------
	/*
	* Constructor
	*/
   public function __construct()
   {
		parent::__construct();
		
		//load Home Model
	   $this->load->model('transaction_management_model','transaction_management');
	   $this->load->model('jobs/jobs_model','jobs');	 
	   $this->load->model('customers/customers_model','customers');	 
	   $this->load->model('users/users_model','users');	  
	   $this->_data['udata'] = userinfo_permission();
   }
   
//----------------------------------------------------------------------

	/*
	* transaction Home Page
	*/
   public function index($account_id='')
   {
	   // Load Home View
	   $this->_data['transactions'] = $this->transaction_management->LoadTransactions($account_id);
	   $this->load->view('transactions',$this->_data);
   }
//----------------------------------------------------------------------


   public function banks()
   {
	   // Load Home View
	   $this->_data['bank_accounts'] = $this->transaction_management->LoadAllbanksAccounts();
	   $this->load->view('banks',$this->_data);
   }
   
   
   public function cash()
   {
	   // Load Home View
	   $this->_data['CashManagement'] = $this->transaction_management->LoadAllCashManagement();
	   $this->load->view('cash-management',$this->_data);
   }
	/*
	* Add transaction Form Page
	*/
   public function add_transaction()
   {
	   // Load Home View
	   $data = $this->input->post();
	   
	   unset($data['job_status'],$data['submit_transaction']);
	   $data['charge_id'] = $data['charges_selection'];
	   unset($data['charges_selection'],$data['bank_id']);
	   $data['created'] = date('Y-m-d');
	   $id = $this->transaction_management->insertNewtransaction($data);
	   if($id){
		   //update account - minus balance from compnay account.
		   if($data['rec_account_id']=='' || !isset($data['rec_account_id'])){
		       
			   // here we are deposite cash so it will + plus in compnay accounts.
			   $this->transaction_management->updateBankAccountCashSender($data['account_id'],$data['value']);
			   //logic for image upload start
			   
			   // this logic charges end
				//-----------------------------------------------------
				$config['upload_path'] = './uploads/recipt/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '1000';
				$config['max_width']  = '10240';
				$config['max_height']  = '7680';
		
				$this->load->library('upload', $config);
		
				if ( ! $this->upload->do_upload('deposite_recipt'))
				{
					$error = array('error' => $this->upload->display_errors());
					print_r($error);
					exit;
					//$this->load->view('upload_form', $error);
					//do_redirect('options?e=10');
				}
				else
				{
					$upload_data = $this->upload->data(); 
					
					$file_name = $upload_data['file_name'];
					//$exts = split("[/\\.]", $file_name);
					//$n    = count($exts)-1;
					//$ext  = $exts[$n];
					
					$datas['deposite_recipt'] = $file_name;
					$this->db->where('id', $id);
					$this->db->update('an_company_transaction', $datas);
					//do_redirect('options?e=10');
				}
			   
			   //logic for image upload end
			   
			   //update branch cash - minus balance from branch cash.
			   //$this->transaction_management->updateBranchCash($data['branch_id'],$data['value']);
		   }else{
		       $this->transaction_management->updateBankAccountCash($data['account_id'],$data['value']);
			   $this->transaction_management->updateBankAccountCashSender($data['rec_account_id'],$data['value']);
		   }
	       
		   //update status of charge which is paid and it not further list in add transcation for this customer.
		   if($data['charge_id']!='7')
		   $this->transaction_management->udpateJobStatusPaid($data['job_id'],$data['charge_id']);
	   }
	   redirect(base_url().'transaction_management');
   }
//----------------------------------------------------------------------


	/*
	* Add transaction Form Page
	*/
   public function new_transaction()
   {
	   // Load Home View
	   $this->_data['customers'] = $this->jobs->get_all_customers();
	   $this->load->view('add-transaction-form',$this->_data);
   }



	/*
	* Add loadtranserBankaccounts
	*/
   public function loadtranserBankaccounts()
   {
	   $account_id = $this->input->post('account_id');
	   $res = $this->transaction_management->LoadBankAccountExceptAccountId($account_id);
	   ?>
       <select id="rec_account_id" name="rec_account_id" onchange="" >
       	   <option value="">Select Bank Account</option>
		   <?php
           foreach($res as $eachRes){
               ?>
               <option value="<?php echo $eachRes->account_id;?>"><?php echo $eachRes->account_name;?></option>
               <?php
           }
           ?>
       </select>
       <?php
	   
   }


	/*
	* Add getBankAccounts
	*/
   public function getBankAccounts()
   {
	   $bank_id = $this->input->post('bank_id');
	   $res = $this->transaction_management->LoadBankAccountByBankId($bank_id);
	   ?>
       <select id="account_id" name="account_id" onchange="loadtranserBankaccounts(); " >
       	   <option value="">Select Bank Account</option>
		   <?php
           foreach($res as $eachRes){
               ?>
               <option value="<?php echo $eachRes->account_id;?>"><?php echo $eachRes->account_name;?></option>
               <?php
           }
           ?>
       </select>
       <?php
	   
   }



	/*
	* Add transaction Form Page
	*/
   public function transaction_methods()
   {
	   // Load Home View
	   $this->load->view('transaction-methods',$this->_data);
   }
 //----------------------------------------------------------------------

	/*
	* Add New Invoice
	*/
   public function add_invoice()
   {
	   // Load Home View
	   $this->load->view('add-invoice',	$this->_data);
   }
 //----------------------------------------------------------------------

	/*
	* Add New Invoice
	*/
   public function invoice()
   {
	   // Load Home View
	   $this->load->view('invoice',	$this->_data);
   }
//----------------------------------------------------------------------
}
?>