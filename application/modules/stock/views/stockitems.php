
<div id="pad-wrapper">

    <div class="table-wrapper users-table ">
        <form action="<?php //echo form_action_url('delete_customers');   ?>" id="listing" method="post" autocomplete="off">
            <div class=" head">
                <div class="">
                    <h4>

                        <div class="title"> <span><?php //breadcramb(); ?></span> </div>


                    </h4>
                    
                </div>
            </div>

            <?php
            $addpermsall = $this->session->userdata('bs_addperms');
            //echo "<pre>";
            //print_r($addperms->addmodules);
            $addperms = $addpermsall->addmodules;
            if($addperms){
                $mPermission = explode(',',$addperms);
                //  print_r($mPermission);
            }

            if(!empty($mPermission)) {
                if (in_array('9', $mPermission)) {
                    $Purchase_Price = 1;
                }
            }
            if(!empty($mPermission)) {
                if (in_array('10', $mPermission)) {
                    $Total_Purchase = 1;
                }
            }
            if(!empty($mPermission)) {
                if (in_array('12', $mPermission)) {
                    $Sale_Price = 1;
                }
            }
            if(!empty($mPermission)) {
                if (in_array('11', $mPermission)) {
                    $Total_Sales = 1;
                }
            }
            ?>

            <div class="">
                <div class="">
                    <?php //action_buttons('add_item', $cnt); ?>   
                    <table id="new_data_table">
                        <form action="<?php //echo form_action_url('bulk_delete');   ?>" id="listing" method="post" autocomplete="off">
                            <thead class="thead">
                                <tr>
                                    <th  id="no_filter"><label for="checkbox"></label>
                                        <?php echo lang('All') ?></th>
                                    <th ><?php echo lang('Item-Picture') ?></th>
                                    <th ><?php echo lang('Name') ?></th>
                                    <th  ><?php echo lang('Category-Name') ?></th>
                                    <th><?php echo lang('type_unit'); ?></th>
                                    <th  ><?php echo lang('Type') ?></th>
                                    <th  ><?php echo lang('Shelf') ?></th>
                                    <!--<th width="8%" >Store</th>-->
                                    <!--<th ><?php echo lang('Supplier-Name') ?></th>-->
                                    <th ><?php echo lang('BarCode-Number') ?></th>
                                    <?php
                                    if ($this->session->userdata('bs_memtype') != 8) {
                                        ?>

                                        <th ><?php echo lang('Purchase-Price') ?> </th>
                                        <th ><?php echo lang('Sale-Price') ?> </th>
                                        <th ><?php echo lang('Min-Sale-Price') ?> </th>
                                        <th ><?php echo lang('Re-order') ?> </th>
                                        <?php
                                    }
                                    ?>
                                    <th ><?php echo lang('Date') ?></th>
                                    <!--<th width="6%">Quantity</th>-->
                                    <th  id="no_filter">&nbsp;</th>
                                </tr>
                            </thead>

                            <?php
                            $cnt = 0;
                            $data=$this->inventory->product_list($storeid);
                            if($data){
                            foreach ($data as $inv) {
                                $cnt++;
                                ?>
                                <tr>
                                    <td ><input type="checkbox" class="allcb" name="u[]" id="u_<?php echo $inv->itemid; ?>" value="<?php echo $inv->itemid; ?>" /></td>
                                    <td ><a href="<?php echo base_url(); ?>uploads/item/<?php echo $inv->storeid; ?>/<?php echo $inv->product_picture; ?>" class="aimg"><img src="<?php echo base_url(); ?>uploads/item/<?php echo $inv->storeid; ?>/<?php echo $inv->product_picture; ?>" width="30" height="30" /></a></td>
                                    <td ><a href="<?php echo base_url() ?>inventory/history/<?php echo $inv->itemid; ?>">
                                        <?php //echo $inv->itemname; ?>
                                        <?php  echo _s($inv->itemname,get_set_value('site_lang')); ?>
                                    </a></td>
                                    <td><?php echo _s($inv->catname,get_set_value('site_lang')) ?></td>
                                    <td><?php  echo _s($inv->unit_title,get_set_value('site_lang')) ?></td>
                                     <td><?php product_type('', '', $inv->itemtype); ?></td>

                                    <td><?php echo  $inv->shelf; ?></td>
                                    <!--<td><?php echo $inv->storename; ?></td>-->
                                    <!--<td><a href="<?php echo base_url() ?>inventory/storeItems/<?php echo $inv->suppliderid; ?>"><?php echo $inv->suppliername; ?></a></td>-->
                                    <td><?php echo $inv->barcodenumber; ?></td>
                                    <?php
                                    if ($this->session->userdata('bs_memtype') != 8) {
                                        ?>
                                        <td><?php if($Purchase_Price == 1){   echo $inv->purchase_price; } ?></td>
                                        <td><?php if($Total_Purchase == 1){ echo $inv->sale_price; } ?></td>
                                        <td><?php if($Sale_Price ==1){ echo $inv->min_sale_price; } ?> </td>
                                        <td ><?php echo $inv->re_order; ?></td>
                                        <?php
                                    }
                                    ?>
                                        <td ><?php echo date('Y-m-d',  strtotime($inv->submissiondate)); ?></td>
                                    <!--<td ><?php //echo $inv->quantity;   ?></td>-->
                                    <td >
                                        <?php
                                        if ($udata['owner_id'] == 0) {
                                            edit_button('add_item/' . $inv->itemid);
                                        }
                                        ?>
                                        <!--<a href='javascript:void'  id='<?php echo $inv->itemid; ?>' class='basic'><img src="<?php echo base_url(); ?>images/internal/more_info.png" width="24" height="16" border="0" /></a>-->

                                        <!-- modal content -->

                                        <div id="basic-modal-content" class="dig<?php echo $inv->itemid; ?>" style="display: none">
                                            <h3><?php echo $inv->itemname; ?><br />
                                                <?php echo lang('Serial-Number') ?> : #<?php echo $inv->serialnumber; ?></h3>
                                            <!--line--> 
                                            <code> <span class="pop_title"><?php echo lang('Category-Name') ?> : </span>
                                                <div class="pop_txt"><?php echo $inv->catname; ?></div>
                                            </code> 
                                            <!--line--> 
                                            <code> <span class="pop_title"><?php echo lang('Type') ?> : </span>
                                                <div class="pop_txt"><?php product_type('', '', $inv->itemtype); ?></div>
                                            </code> 
                                            <!--line--> 
                                            <code> <span class="pop_title"><?php echo lang('Store') ?> : </span>
                                                <div class="pop_txt"><?php echo $inv->storename; ?></div>
                                            </code> 
                                            <!--line--> 
                                            <code> <span class="pop_title"><?php echo lang('Purchase-Price') ?> : </span>
                                                <div class="pop_txt"><?php echo $inv->purchase_price; ?> RO.</div>
                                            </code> 
                                            <!--line--> 
                                            <code> <span class="pop_title"><?php echo lang('Sale-Price') ?>  : </span>
                                                <div class="pop_txt"><?php echo $inv->sale_price; ?> RO.</div>
                                            </code> 
                                            <!--line--> 
                                            <code> <span class="pop_title"><?php echo lang('Min-Sale-Price') ?> : </span>
                                                <div class="pop_txt"><?php echo $inv->min_sale_price; ?> %</div>
                                            </code> 
                                            <!--line--> 
                                            <code> <span class="pop_title"><?php echo lang('Point-of-Re-order') ?> : </span>
                                                <div class="pop_txt"><?php echo $inv->point_of_re_order; ?></div>
                                            </code> 
                                            <!--line--> 
                                            <code> <span class="pop_title"><?php echo lang('Quantity') ?> :</span>
                                                <div class="pop_txt"><?php echo $inv->quantity; ?></div>
                                            </code> 
                                            <!-- <code> <span class="pop_title">Note :</span>
                                            <div class="pop_txt">We can't Reply The Supplier Because He Is Cheet  Us</div>
                                            </code> -->
                                            <!--line--> 

                                        </div></td>
                                </tr>
                            </form>
                        <?php } } ?>
                    </table>
                </div>
            </div>
            <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>

    </div>
    <!-- END PAGE --> 
</div>
