<script type="text/javascript" src="<?php echo base_url(); ?>durarthem/rtl/js/function.js" ></script>
<div id=" main-content" class="row main_content" >
    <form id="form1" action="<?php echo base_url(); ?>stock/adjust_stock" method="post">

        <div class="g16" style="display: none;">
            <div class="g3" style="float: left;">
                <input type="button" id="clear" name="clear" value="clear" onclick="clearValues()" style="cursor: pointer;">
            </div>
        </div>

        <div class="g2 form-group">

            <label class="text-warning"><?php echo lang('adjustment_type') ?> :</label>

            <br>

            <div class="ui-select" style="width: 100%;">

                <?php //store_dropbox('store_id', '', ''); ?>
                <select id="adjustment_type" name="adjustment_type">
                    <option value="">Select</option>
                    <option value="stock_adjustment"><?php echo lang('stock_adjustment') ?></option>
                    <option value="opening_stock"><?php echo lang('opening_stock') ?></option>
                    <!--<option value="damage_stock"><?php //echo lang('damage_stock')      ?></option>-->
                </select>

            </div>

        </div>

        <div class="g2 form-group">

            <label class="text-warning"><?php echo lang('Store') ?> :</label>

            <br>

            <div class="ui-select" style="width: 100%;">

                <?php store_dropbox('store_id', '', ''); ?>

            </div>

        </div>



        <div class="g3" id="item_size_div" style="display: none;">

            <label class=""><?php echo lang('item-size') ?></label>

            <div class="">

                <div class="ui-select" >

                    <?php
///,'finishgood'=>'Finish Goods'
// $ptypes = array('raw'=>'RAW Material','product'=>'Product');
//$ptypes = getitemtype('',2);
                    ?>
                    <div class="">
                        <select id="item_size" name="item_size">
                            <option value="">Select</option>

                        </select>


                    </div>

                </div>

            </div>
        </div>

        <div class=" form-group">

            <div class="g4 form-group" style="display: none">

                <label class="text-warning"><?php echo lang('Category') ?> :</label>

                <br>

                <div class="ui-select" style="width: 100%;">

                    <select name="category_id" id="category_id" class="has-search">

                        <option ><?php echo lang('choose') ?></option>

                        <?php
                        if (isset($categories) && !empty($categories)) {

                            foreach ($categories as $categ) {
                                ?>

                                <option value="<?php echo $categ->catid; ?>"><?php echo _s($categ->catname, get_set_value('site_lang')); ?></option>

                                <?php
                            }
                        }
                        ?>

                    </select>

                </div>

            </div>


            <div class="g4 form-group" id="subsub_categ_div" style="display: none;">

                <label class="text-warning"><?php echo lang('subchildCateg') ?> :</label>

                <br>

                <div class="ui-select" style="width: 100%;">

                    <select name="sub_sub_cateogry_id" id="sub_sub_cateogry_id" class="has-search">

                        <option ><?php echo lang('choose') ?></option>
                    </select>

                </div>

            </div>


            <div class="g4 form-group">

                <label class="text-warning"><?php echo lang('Product-Name') ?> :</label>





                <input  type="hidden" name="productid"  id="productid" class="formControl"/>
                <input  type="hidden" name="productdetailid"  id="productdetailid" class="formControl"/>



                <input type="text" name="productname"  id="productname" class="g12 form-control " onchange="searchItemBarcode(this.value)"/>

                <a onClick="show_dialog2('#demo-modal', '<?php echo base_url(); ?>ajax/getAddnew')" href="javascript:void" style="cursor:pointer;" class="g2"><img  src="<?php echo base_url(); ?>images/internal/add_customer.png" border="0" /></a>

                <a onClick="show_dialog2('#demo-modal', '<?php echo base_url(); ?>ajax/getallitems/p')" href="javascript:void" data-toggle="" class="g2"><i class="icon-th-list left" style="font-size:20px;color:black"></i></a>





                <input type="hidden" name="productname2"  id="productname2" class="form-control "/>



            </div>

            <div class="g4 form-group">

                <label class="text-warning"><?php echo lang('Serial-No') ?> :</label>

                <br>

                <input name="serialnumber" id="serialnumber" type="text"  class=" currentProduct form-control"/>

            </div>



            <div class="g4 form-group">

                <label class="text-warning"><?php echo lang('expireddate') ?> :</label>

                <br>

                <input name="expireddate" id="expireddate" type="text"  class="datapic_input  currentProduct form-control"/>

            </div>



            <br clear="all"/>


            <div class="g2 form-group" id="box_quantity_div" style="display: none;">

                <label class="text-warning"><?php echo lang('each_box_quantity') ?> :<span id="quantity_text" style="display: none;"></span></label>

                <br>

    <!--<input id="quantity" name="quantity" type="text" onchange="calculatePrice()"  class=" currentProduct form-control"/>-->

                <input id="each_box_quantity" name="each_box_quantity" type="text" onchange="getTotalQuantityByBox()"  class="  form-control"/>

            </div>

            <div class="g2 form-group" id="total_box_quantity_div" style="display: none;">

                <label class="text-warning"><?php echo lang('box_quantity') ?> :<span id="quantity_text" style="display: none;"></span></label>

                <br>

    <!--<input id="quantity" name="quantity" type="text" onchange="calculatePrice()"  class=" currentProduct form-control"/>-->

                <input id="total_box" name="total_box" type="text" onchange="getTotalQuantityByBox()"  class="  form-control"/>

            </div>

            <div class="g4 form-group">

                <label class="text-warning"><?php echo lang('Quantity') ?> :<span id="quantity_text" style="display: none;"></span></label>

                <br>

    <!--<input id="quantity" name="quantity" type="text" onchange="calculatePrice()"  class=" currentProduct form-control"/>-->

                <input id="quantity" name="quantity" type="text" onchange="" value="1"  class="g8 currentProduct form-control"/>
                <input id="unit_title" name="unit_title" type="text" class="g8 currentProduct form-control"/>

            </div>

            <div class="g4 form-group">

                <label class="text-warning"> <?php echo lang('store_quantity') ?></label>

                <br>

                <input name="" id="storequantity" type="text" style="color:green;font-size: 16px;text-align: center" value="<?php //echo get_store_quantity(3);            ?>" disabled="disabled" class="g8 currentProduct form-control"/>

                <input name="" id="shelf" type="text" style="color:green;font-size: 16px;text-align: center" value="<?php //echo get_store_quantity(3);            ?>" disabled="disabled" class="g8 currentProduct form-control"/>

            </div>

            <div class="g4 form-group" style="display: none;">

                <label class="text-warning" id="fortaotal"><?php echo lang('p_price_aed') ?> :</label>

                <br>

                <input name="item_aed_price" id="item_aed_price" type="text"  onchange="changedharamtoOmr(this)" class="currentProduct form-control"/>

            </div>

            <div class="g3 form-group">

                <label class="text-warning" id="fortaotal"><?php echo lang('sforone') ?> :</label>

                <br>

                <input name="item_total_price" id="item_total_price" type="text"   class="item_total_price0 currentProduct form-control"/>

            </div>
            <div class="g3 form-group" style="display: none;" id="item_each_box_div">

                <label class="text-warning" id="fortaotal"><?php echo lang('box-price') ?> :</label>

                <br>

                <input name="item_each_box_total_price" id="item_each_box_total_price" type="text"  onchange="calculateeachPriceByBox()"  class="item_total_price0 currentProduct form-control"/>

            </div>
            <div class="g4 form-group">

                <label class="text-warning"><?php echo lang('item_total_price_prev') ?> :</label>

                <input name="item_total_price_prev" id="item_total_price_prev" type="text"  class="item_total_price_prev currentProduct form-control"/>

            </div>

            <br clear="all"/>



            <div class="g4 form-group">

                <label class="text-warning"><?php echo lang('avrage') ?> :</label>

                <br>

                <input id="average_id" name="avrage" onchange="" type="text"  class="form-control"/>

            </div>

            <div class="g4 form-group">

                <label class="text-warning"><?php echo lang('prod-date') ?> :</label>

                <br>

                <input id="production_date" name="production_date" onchange="" type="text"  class="form-control datapic_input"/>

            </div>

            <div class="g4 form-group">

                <label class="text-warning"><?php echo lang('expire-date') ?> :</label>

                <br>

                <input id="expire_date" name="expire_date" onchange="" type="text"  class="form-control datapic_input"/>

            </div>





            <div class="g4 form-group" style="visibility: hidden;">

                <label class="text-warning"><?php echo lang('Discount') ?> :</label>

                <br>

                <input id="item_discount" name="item_discount--" onkeyup="calculateDis()" type="text"  class="form-control"/>

            </div>





            <div class="g4 form-group" style="visibility: hidden;"> <br>

                &nbsp; %

                <input type="radio" onchange="calculateDis()" name="type" id="type1" value="1"  class="currentProduct" />

                RO

                <input type="radio" onchange="calculateDis()" name="type" id="type2" value="2" class="currentProduct"/>

            </div>

            <div class="g4 form-group" style="visibility: hidden;">

                <label class="text-warning"><?php echo lang('afterDiscount') ?> :</label>

                <br>

                <input id="afterDiscount" name="item_discount" onchange="" type="text"  class="form-control"/>

            </div>



            <br clear="all"/>




            <div class="g8 form-group">

                <label class="text-warning"><?php echo lang('Discription') ?> :</label>

                <br>

                <textarea cols="100" style="height: 55px;"  rows="10"  name="product_comment" id="product_comment" class="dis_txtarea currentProduct form-control" ></textarea>

            </div>

            <div class="g8 form-group">

                <label class="text-warning"><?php echo lang('Notes') ?> :</label>

                <br>

                <textarea name="product_notes" style="height: 55px;"  id="product_notes" cols="100" rows="10" class="dis_txtarea currentProduct form-control" ></textarea>

            </div>





            <input type="hidden" id="storeid_p" name="storeid_p" value="" />

            <input type="hidden" id="product_picture" name="product_picture" />

            <input type="hidden" id="product_type" name="invoice_itype" />

            <br  clear="all"/><br clear="all"/><br clear="all"/>

            <div class=" form-group">

                <div class="" onclick="addItemnew2()" style="border-bottom: 1px solid green;margin-bottom: -1px;position: relative;">

                    <div style="border-width: 1px 1px 0px 1px;width: 113px;cursor: pointer;border-color: green;border-style: solid;clear: both;padding: 7px;">

                        <i class="icon-addtocart left" style="font-size:20px;"></i>

                        <?php echo lang('add-product') ?>

                        <br clear="all"/>

                    </div>

                </div>

            </div>

            <div class=" form-group">

                <table width="100%" align="left" id="product_list" >

                    <thead>

                        <tr>



                            <th></th>

                            <th><?php echo lang('Image') ?></th>

                            <th><?php echo lang('Product-Name') ?></th>
                            <th><?php echo lang('BarCode-Number') ?></th>

                            <th><?php echo lang('Discription') ?></th>

                            <th><?php echo lang('sforone') ?></th>
                            <th style="display: none;" class="dhram_price"><?php echo lang('p_price_aed') ?></th>

                            <th><?php echo lang('Quantity') ?></th>

                            <th><?php echo lang('Total') ?></th>
                            <th style="display: none;" class="dhram_price"><?php echo lang('Totalaed') ?></th>
                            <th><i class="icon-remove-sign" style="font-size:20px;color: red;"></i></th>

                            <th><i class="nav-icon icon-settingstwo-gearalt" style="font-size:20px;"></i></th>

                        </tr>

                    </thead>

                    <tbody>

                        <tr  class="grand-total" style="height: 50px;border-bottom: 1px solid #EFEFEF;">







                            <td colspan="4" bgcolor="#FFFFFF" >



                            </td>





                            <td><?php echo lang('Total-Price') ?></td>
                            <td><strong id="unit_total"></strong></td>
                            <td></td>

                            <td><strong id="netTotal"></strong><input type="hidden" name="totalnet_before" id="totalnet_before" /></td>
                            <td><strong id="netaedTotal"></strong></td>
                            <td></td>



                        </tr>

                    </tbody>

                </table>

            </div>







            <input type="hidden" name="totalnet" id="totalnet" />
            <input type="hidden" name="totalaed" id="totalaed" />
            <input type="hidden" name="totalnet_before" id="totalnet_before" />

            <div class="" id="product_maintenance"></div>







            <br clear="all">

            <!--<a href="javascript:void" class="go_to_nexttabs2 reset_btn btn-glow primary" style="margin-top: 10px;">Next &gt;&gt;</a>-->

            <br clear="all">

        </div>
        <div class="invoice_raw_product_items"></div>

        <br clear="all"><br clear="all"><br clear="all"><br clear="all">
        <div class="nav-act btn-m">

            <!--<button class="">Back</button>-->
            <button name="" type="button" class="submit_btn btn-glow primary"  onclick="submitForm()"  /><?php echo lang('add') ?></button>

        </div>

    </form>

</div>

<script type="text/javascript">

    function swapme2p() {

        pid = $("#productid").val();

        pname = $("#productname2").val();

        $("#productname").val(pname);

        $("#productid").val(pid);

    }


    function triggerMatch() {
        $("#ui-id-1 .ui-menu-item .ui-corner-all").eq(0).trigger('click');
    }


    function selectValues() {

        $("#item_size").val(uu.item.item_size_id);


        $("#category_id").val(uu.item.categoryid);
        $("#sub_cateogry_id").val(uu.item.sub_category_id);

        $("#sub_cateogry_id").val(uu.item.sub_category_id);
        $("#sub_sub_cateogry_id").val(uu.item.sub_sub_catogory_id);

    }

    function searchItemBarcode(br) {
        //br = $("#barcodenumber").val();
        if (br > 10) {
            $.ajax({
                url: "<?php echo base_url(); ?>inventory/search_barcode",
                type: 'post',
                data: {'br': br},
                cache: false,
                success: function (data) {
                    //$(".demo").hide();
                    //$("#tb_data").html(data);
                    if (!data) {
                        alert('Not Found');
                    }
                }
            });
        }
    }
    function swapmec() {

        //alert('swapme');

        pid = $("#customerid").val();

        pname = $("#customername2").val();

        $("#customername").val(pname);

        $("#customerid").val(pid);

        $("#customerid-val").val(pname);

    }

    $(document).ready(function () {

        //	alert('asd');


        $.fn.focusTextToEnd = function () {
            this.focus();
            var $thisVal = this.val();
            this.val('').val($thisVal);
            return this;
        }

        $("#category_id").change(function () {

            val = $(this).val();

            //alert(val);

            $("#productname").trigger('keydown');

        });

        setInterval('checkData()', 1000);

        storeId = $("#store_id").val();

        /*var ac_config = {
         
         source: "<?php echo base_url(); ?>ajax/getAutoSearchProducts?store_id=",
         
         select: function(event, ui){
         
         $("#productname").val(ui.item.id);
         
         $("#productid").val(ui.item.prod);
         
         getProductData(ui.item.id);
         
         console.log(ui);
         
         //swapme();
         
         setTimeout('swapme2()',500);
         
         },
         
         minLength:1
         
         };
         
         
         
         $("#productname").focusin(function(){
         
         //alert('asd');
         
         storeId = $("#store_id").val();
         
         
         
         });*/

        //$("#productname").autocomplete(ac_config);



        $("#productname").autocomplete({
            /* source: function (request, response) {
             $.getJSON("<?php echo base_url(); ?>ajax/getAutoSearchTransferShop", {category: $('#categoryid').val(),term: $('#productname').val(),store: $('#fstore_id').val(),productid: $('#productid').val()},response);
             },*/
            source: function (request, response) {
                var sUrl = "<?php echo base_url(); ?>ajax/ajaxAutocomplete2/" + $('#fstore_id').val();
                $.getJSON(sUrl, request, function (result) {
                    response(result);
                });
            },
            minLength: 0,
            autoFocus: true,
            select: function (event, ui) {

                //action
                uu = ui;
                $("#productname").val(ui.item.label);

                $("#productname2").val(ui.item.label);

                $("#productid").val(ui.item.id);
                $("#productdetailid").val(uu.item.item_detail_id);
                //item_detail_id
                $("#each_box_quantity").val(ui.item.item_box_qunatity);
                $("#item_type").val(ui.item.item_type_id);

                
                // alert(ui.item.item_type_id);
                $("#item_type").val(ui.item.item_type_id);

                $("#item_size").val(ui.item.item_size_id);

               
                $("#category_id").val(ui.item.categoryid);
               

                //getchild(1,ui.item.sub_category_id);
                getProductData2(ui.item.id, '', $('#store_id').val());

               
          searchAvgValue();

                //swapme();

                setTimeout('swapme2p()', 500);
                //selectValues(ob);
                setTimeout('selectValues()', 500);

            }
        });


        $("#productname").focus(function () {

            $("#productname").trigger('keydown');

        });

        var ac_config = {
            source: "<?php echo base_url(); ?>ajax/getAutoSearchCustomer2",
            select: function (event, ui) {

                $("#customerid").val(ui.item.id);

                $("#customername").val(ui.item.cus);

                $("#customername2").val(ui.item.cus);

                //$("#customer_ftotal").val(ui.item.buywithtotal);

                console.log(ui);

                //swapme();

                setTimeout('swapmec()', 500);



                $.ajax({
                    url: "<?php echo base_url() ?>purchase_management/get_balance",
                    type: "POST",
                    data: {val: ui.item.id},
                    success: function (e) {



                        $('.get_balance').html(e);

                    }

                });

            },
            minLength: 1

        };

        $("#customername").autocomplete(ac_config);



    });


    function checkData() {

        plen = allProductsData.length;

        if (plen > 0) {

            $('#customername').attr("disabled", true);

        }

        else {

            $('#customername').attr("disabled", false);

        }

    }











    $(document).ready(function () {

        $('#Alternate_Price').click(function () {

            if ($("#Alternate_Price:checked").val() == 1) {



                $('#Alternate_Price_input').css('display', 'block');

                //$('#item_total_price').css('display', 'block');

                //$('#item_total_price').attr('disabled', true);

                $('.item_total_price0').attr('id', 'item_total_price2');

                $('.item_total_price0').attr('name', 'item_total_price2');

                $('.item_total_price_old').attr('id', 'item_total_price');

                $('.item_total_price_old').attr('name', 'item_total_price');

            } else {

                $('#Alternate_Price_input').css('display', 'none');

                //$('#item_total_price').removeAttr('disabled');



                $('.item_total_price0').attr('id', 'item_total_price');

                $('.item_total_price0').attr('name', 'item_total_price');

                $('.item_total_price_old').attr('id', 'item_total_price_old');

                $('.item_total_price_old').attr('name', 'item_total_price_old');

            }



            $("#item_total_price").change(function () {

                //$(this).val()

                $('#total_price').val($(this).val());

                //alert($(this).val());



            });

        });

        $('#is_payment').click(function () {

            if ($("#is_payment:checked").val() == 1) {

                $('.payment_div').css('display', 'block');

            }

        });

        $('#customerType').change(function () {

            if ($(this).val() == 'newcustomer') {



                $('#newcustomer').css('display', 'block');

                $('#customer').css('display', 'none');

                //$('.payment_div').css('display', 'block');



            } else {

                $('#customer').css('display', 'block');

                $('#newcustomer').css('display', 'none');

            }

        });















//        $('#payment_amount').change(function () {

//            //$("#remin_view_all_total").html($("#remin_view_all_total").html()+$("#payment_amount").html());

//            $("#remin_view_all_total").html(parseInt($("#remin_view_all_total").html()) - parseInt($('#payment_amount').val()));

//

//            $("#recievedamount").val(parseInt($("#remin_view_all_total").html()));

//            $("#totalrecievedamount").val(parseInt($('#payment_amount').val()));

//            //alert($("#howpay").val());

//            if ($("#howpay").val() == "1") {

//                $("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) + parseInt($('#payment_amount').val()));

//            }

//        });



        $('#payment_amount').change(function () {

            //$("#remin_view_all_total").html($("#remin_view_all_total").html()+$("#payment_amount").html());



            $("#remin_view_all_total").html(parseInt($("#remin_view_all_total").html()) - parseInt($('#payment_amount').val()));

            $("#recievedamount").val(parseInt($("#payment_amount").val()));

            $("#totalrecievedamount").val(parseInt($('#payment_amount').val()));



            //alert($("#howpay").val());

            ppshow = parseInt($("#get_my_allaccount").html()) - parseInt($('#payment_amount').val());

            ppshow2 = parseInt($("#view_all_total").html()) - parseInt($('#payment_amount').val());





            if ($("#howpay").val() == "1") {



                if (parseInt($("#remin_view_all_total").html()) < 0) {



                    $("#increse_amount_span").text(Math.abs(ppshow2));

                    $("#increse_amount").css('display', 'block');

                    $("#remin_view_all_total").text("0");

                    $("#payment_amount").val($('#view_all_total').html());

                    if ($("#remin_view_all_total").text() > 0) {

                        $("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) - parseInt($('#view_all_total').html()));

                    }

                    /*
                     
                     $("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) - parseInt($('#view_all_total').html()));
                     
                     $("#recievedamount").val(parseInt($('#view_all_total').html()));
                     
                     $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                     
                     $("#remin_view_all_total").text("0");
                     
                     */

                } else {

                    $("#get_my_allaccount").text(ppshow);

                }



            } else {



                if (parseInt($("#remin_view_all_total").html()) < 0) {



                    //alert('if');

                    $("#increse_amount").css('display', 'block');

                    $("#increse_amount_span").text(Math.abs(ppshow2));

                    $("#remin_view_all_total").text("0");

                    $("#payment_amount").val($('#view_all_total').html());

                    //$("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) - parseInt($('#view_all_total').html()));

                    /*
                     
                     $("#recievedamount").val(parseInt($('#view_all_total').html()));
                     
                     $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                     
                     $("#remin_view_all_total").text("0");
                     
                     $("#increse_amount").show();
                     
                     */

                } else {

                    //alert('else');

                    //$("#increse_amount_span").text(Math.abs(ppshow2));

                    $("#remin_view_all_total").text(ppshow2);



                }

            }

        });







        $('#store_id').change(function () {

            var val = $(this).val();

            var pid = $("#productid").val();

            $.ajax({
                url: "<?php echo base_url() ?>sales_management/get_store_quantity",
                type: "POST",
                data: {val: val, pid: pid},
                success: function (e) {



                    $('#storequantity').val(e);



                }

            });



        });







    });



    function  do_payment_amountt(e) {

        //$("#remin_view_all_total").html($("#remin_view_all_total").html()+$("#payment_amount").html());

        //alert(e);

        $("#remin_view_all_total").html(parseInt($("#remin_view_all_total").html()) - parseInt(e));

        $("#recievedamount").val(parseInt($("#remin_view_all_total").html()));



        $("#totalrecievedamount").val(parseInt($("#totalrecievedamount").val()) + parseInt(e));

        if ($("#howpay").val() == "1") {

            $("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) + parseInt(e));

        }

    }

    function rest_payment() {



        $('#get_my_allaccount').html("0");

        $('#howpay').val("");

        $('#recievedamount').val("");

        $('#remin_view_all_total').html($('#view_all_total').html());

        $('#increse_amount').hide("");

        $('#payment_amount').val("");

        $("#add_new_payment_btn").show();

    }



</script>
<script type="text/javascript">

    var allProductsData = new Array();

    var allSalesData = new Array();

    totalAmountexComision = '';

    totalnetAmountexComision = '';

    $(document).keypress(function (e) {

        if (e.which == 13) {

            //   alert('You pressed enter!');

            if ($(".currentProduct").is(":focus")) {

                //  alert('You pressed enter!');

                if ($("#productid").val() != "")

                {

                    addItem();

                }

            }

            else {

                //alert('not');

            }

        }

    });</script>

<script type="text/javascript">

    netTotals = new Array(); /// for keep rocrds net total for each produc

    Totalsmin = new Array(); /// for keep rocrds net total for each produc

    Totalsquan = new Array(); /// for keep rocrds net total for each produc

    rowCounter = 0; /// counter for table products



    netCouner = 0; // counter for net totals

    tbHtml = ''; // for adding new row in the table



    editArray = new Array(); // edit any product fromm tab;e







    total_value = 0; // total cacluate value







    productData = {}; // array for current working product







    counter = 0; // counter for products in the hiddden


    function clearValues() {
        $("#item_type").val('');
        $("#item_size").val('');
        $("#category_id").val('');
        $("#sub_cateogry_id").val('');
        $("#sub_sub_cateogry_id").val('');
        $("#productname").val('');
        $("#productid").val('');
        $("#productdetailid").val('');
        $("#productname2").val('');

        setTimeout(function () {
            $("#clear").attr("disabled", false); // this will execute after 4 sec delay only once.
            $("#productname").focus();
        }, 1000);
        //$('#customername').attr("disabled", false);
    }

</script>

<script type="text/javascript">

    $(function () {

        /*$(".from_date").datepicker({
         
         defaultDate: "today",
         
         changeMonth: true,
         
         numberOfMonths: 1,
         
         });
         
         $(".payment_date").datepicker({
         
         defaultDate: "today",
         
         changeMonth: true,
         
         numberOfMonths: 1,
         
         });
         
         $(".dpayment_date input").datepicker({
         
         defaultDate: "today",
         
         changeMonth: true,
         
         numberOfMonths: 1,
         
         });
         
         */

        $("#is_payment").change(function () {





            ckPayment = $(this).is(":checked");

            //alert(ckPayment);

            if (ckPayment) {

                if (netTotals.length > 0) {

                    $(".payment_div").show();

                }

            }

            else {

                $(".payment_div").hide();

            }

            //alert(ckPayment);



        });

    });</script>







<script type="text/javascript">

    $(function () {

        $('#expense_date').datepicker({dateFormat: 'yy-mm-dd'});

        $(".expense_period").click(function () {

            //alert('asdasd');



            //checked_status = $(this).is(':checked');

            expense_period_val = $(this).val();



            //alert(checked_status);

            if (expense_period_val == 'fixed') {



                $("#period_parent").show();

            }

            else {

                $("#period_parent").hide();

            }

        });

    });

    $(document).ready(function () {

        //alert('ready');

        var ac_config = {
            source: "<?php echo base_url(); ?>outcome/getAutoSearch",
            select: function (event, ui) {

                $("#expense_title").val(ui.item.cus);

                $("#expense_id").val(ui.item.cus);

                console.log(ui);

                //swapme();

                setTimeout('swapgeneral()', 500);

            },
            minLength: 1

        };



        $("#expense_title").autocomplete(ac_config);





    });







</script>
<script type="text/javascript">

    $('.datapic_input').datepicker({dateFormat: 'yy-mm-dd'});

    function submitForm() {
        $("#form1").submit();
    }

</script>
