<script type="text/javascript">
    function swapme2() {
        pid = $("#productid").val();
        pname = $("#productname").val();
        $("#productname").val(pname);
        $("#productid").val(pid);
    }
   
    $(document).ready(function () {
        //	alert('asd');

        $("#category_id").change(function () {
            val = $(this).val();
            //alert(val);
            $("#productname").trigger('keydown');
        });
        setInterval('checkData()', 1000);
        storeId = $("#store_id").val();
        /*var ac_config = {
         source: "<?php echo base_url(); ?>ajax/getAutoSearchProducts?store_id=",
         select: function(event, ui){
         $("#productname").val(ui.item.id);
         $("#productid").val(ui.item.prod);
         getProductData(ui.item.id);
         console.log(ui);
         //swapme();
         setTimeout('swapme2()',500);
         },
         minLength:1
         };
         
         $("#productname").focusin(function(){
         //alert('asd');
         storeId = $("#store_id").val();
         
         });*/
        //$("#productname").autocomplete(ac_config);

        $("#productname").autocomplete({
            source: function (request, response) {
                $.getJSON("<?php echo base_url(); ?>ajax/getAutoSearchTransfer", {category: $('#categoryid').val(),term: $('#productname').val(),store: $('#fstore_id').val(),productid: $('#productid').val()},response);
            },
            minLength: 0,
            select: function (event, ui) {
                //action
                $("#productname").val(ui.item.prod);
                $("#productid").val(ui.item.id);
                
				tt = ui.item;
              //  alert(ui);
                getTransferProductData(ui.item.id,ui.item.quant,ui.item.bsstoreitems);
                
                //console.log(ui);
                //swapme();
                setTimeout('swapme2()', 500);
            }
        });
        $("#productname").focus(function () {
            $("#productname").trigger('keydown');
        });



    });
    function checkData() {
        plen = allProductsData.length;
        if (plen > 0) {
            $('#customername').attr("disabled", true);
        }
        else {
            $('#customername').attr("disabled", false);
        }
    }

    var allProductsData = new Array();
    var allSalesData = new Array();
    totalAmountexComision = '';
    totalnetAmountexComision = '';
    $(document).keypress(function (e) {
        if (e.which == 13) {
            //   alert('You pressed enter!');
            if ($(".currentProduct").is(":focus")) {
                //  alert('You pressed enter!');
                if ($("#productid").val() != "")
                {
                    addItem();
                }
            }
            else {
                //alert('not');
            }
        }
    });


    netTotals = new Array(); /// for keep rocrds net total for each produc
    Totalsmin = new Array(); /// for keep rocrds net total for each produc
    rowCounter = 0; /// counter for table products

    netCouner = 0; // counter for net totals
    tbHtml = ''; // for adding new row in the table 

    editArray = new Array(); // edit any product fromm tab;e 



    total_value = 0; // total cacluate value



    productData = {}; // array for current working product



    counter = 0; // counter for products in the hiddden

</script> 



<div id="pad-wrapper">
    <div class="row form-wrapper">
        <div class=" col-xs-12">

            <div class="title title alert blue">
                <span><?php echo lang('mess_inv_4') ?> </span><span class="icon icon-infographic left" ></span>
            </div>
            <?php error_hander($this->input->get('e')); ?>
            <?php //$get_product = $this->inventory->getSalesInvoiceItems($id); ?>
            <?php $get_product = array(); ?>
            <div id="main-content" class="main_content ">
                <form name="" action="<?php echo base_url() ?>inventory/save_transfer" method="post">
                    <!--/<?php echo $id ?>-->

                    <div class="g3 form-group">
                        <label class=""><?php echo lang('fStore') ?>   :  </label>
                        <?php  if($lang == 'arabic'){  store_dropbox2('fstore_id', '', ''); } else{ store_dropbox_eng('fstore_id', '', ''); } //store_dropbox2('fstore_id', '', ''); ?>

                    </div>
                    <div class="g3 form-group">
                        <label class=""><?php echo lang('tStore') ?>   : </label>
                        <?php  $lang = $this->session->userdata('site_lang');  if($lang == 'arabic'){  store_dropbox2('tstore_id', '', ''); } else{ store_dropbox_eng('tstore_id', '', ''); }   ?>

                    </div>
                    
                    <div class="g3">
                        <label class=""><?php echo lang('Category-Name-ar') ?></label>

                        <div style="width: 90%">
                            <?php category_dropbox('categoryid','','',''); ?>
                        </div>


                    </div>
                    <div class="g3 form-group">
                        <label class="text-warning"><?php echo lang('Product-Name') ?> :</label>
                        <br>
                        <input  type="hidden" name="productid"  id="productid" class="formControl"/>
                        <input type="text" name="productname"  id="productname" class="form-control "/>
                    </div>
                    
                    <div class="g2 form-group">
                       <label class="text-warning"><?php  $lang = $this->session->userdata('site_lang'); if($lang == 'arabic'){
                                ?>
                               اضافة المنتج
                            <?php
                           }else{
                               ?>
                                Add Item
                           <?php
                           }
                            //echo get_set_value('site_lang'); if() ?> </label>
                       <a href="#" onclick="addItemT()" style="float: right;margin-top: 10px;"><i class="icon-addtocart left" style="font-size:20px;"></i></a>
                    </div>




                    <input type="hidden" name="invoiceid" value="<?php echo $get_product[0]->invoice_id ?>"/>
                    <br clear="all"/>
                    <br clear="all"/>
                    <br clear="all"/>

                    <style>
                        #table-pend{background-color: white !important;}
                        #table-pend tr {background-color: white !important;}
                    </style>


                    <table class="table table-bordered invoice-table mb20" id="table-pend" style="text-align:center !important">
                        <thead style="text-align:center !important">
                        
                        <th><?php echo lang('code') ?></th>
                        <th><?php echo lang('Product-Name') ?></th>
                        <th><?php echo lang('Image') ?></th>
                        
                        <!--<th><?php //echo lang('Store') ?></th>-->
                        <!--<th><?php // echo lang('Supplier-Name') ?></th>-->
                        <th><?php echo lang('Unit') ?></th>
                        <th><?php echo lang('Quantity') ?></th>
                        <th><?php echo lang('store_quantity') ?></th>
                        <th><?php echo lang('pricetype') ?></th>
                        <th><?php echo lang('pprice') ?></th>
                        <th><?php echo lang('pprice') ?></th>
                        <th><?php echo lang('notes') ?></th>
                        <th><?php //echo lang('notes') ?></th>
                        </tr>
                        </thead>
                        <tbody>

                            <?php //if ($get_product): ?>
                                <?php //$count = 0 ?>
                                <?php //$totalp = 0 ?>
                                <?php //foreach ($get_product as $product): ?>
<!--                                    <tr id="tr_<?php echo $product->itemid ?>">
                                        <td >           


                                            <input type="checkbox" class="checkbox" name="product[ids][]" id="u_<?php echo $product->inovice_product_id; ?>" value="<?php echo $product->inovice_product_id; ?>" />



                                        </td>
                                        <td ><?php echo $product->itemid ?></td>
                                        <td ><?php echo _s($product->storename, get_set_value('site_lang')); ?></td>
                                        <td ><img src="<?php echo base_url() ?>uploads/item/<?php echo $product->storeid ?>/<?php echo $product->product_picture ?>" width="50" height="50" /></td>



                                        <td style="text-align:center"> <?php echo $product->itemname ?> </td>
                                        <td style="text-align:center"> <?php echo $product->fullname ?> </td>
                                        <td style="text-align:center">  </td>



                                        <td style="text-align:center" id="oneitem<?php //echo $product->itemid     ?>" style="text-align:center" contentEditable="true"><?php echo $product->invoice_item_quantity ?></td>




                                        <td id="description" contentEditable="true" style="text-align:center"><?php echo $product->notes ?> </td>
                                        <td id="totalAmount" style="text-align:center" contentEditable="true" ><?php echo $product->invoice_item_price ?></td>

                                        <td id="" style="text-align:center">

                                            <textarea name="product[note][]"></textarea>

                                        </td>

                                <input type="hidden" name="product[itemid]" value="<?php echo $product->invoice_item_id ?>"/>
                                <input type="hidden" name="product[invoice_id]" value="<?php echo $product->invoice_id ?>"/>
                                <input type="hidden" name="product[store_id]" value="<?php echo $product->storeid ?>"/>
                                <input type="hidden" name="product[quantity]" value="<?php echo $product->invoice_item_quantity ?>"/>


                                </tr>-->
                                <?php //$totalp+=$product->invoice_item_price; ?>

                                <?php //$quantity+=$product->invoice_item_quantity; ?>
                                <?php //$count++; ?>
                            <?php //endforeach; ?>

                        <?php //else: ?>        
                            <!--<tr><td colspan="11"><?php echo lang('no-data') ?></td></tr>-->
                        <?php //endif; ?>
                        <tr  class="grand-total">



                            <td colspan="1" >

                            </td> 


                            <td><div id="totalmin" style="display: none"></div></td>
                            <td>   


                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
<!--                            <td></td>
                            <td></td>-->


                            <td style="text-align:center"><strong id="netTotal">      <?php echo $totalp; ?></strong></td>    
                            <td></td>


                        </tr>

                        </tbody>
                    </table>
                    <div class="g16" style="margin-right: 1%;border: 2px dotted white;height: 4px;">

                    </div>
                    <div class="g10" style="">

                    </div>
                    <div class="g2" style="">
                        <?php echo lang('Total-quantity') ?> : <span id="all_quant"></span>
                    </div>
                    <div class="g2" style="">
                        <?php echo lang('count-type') ?>  : <span id="count"></span>
                    </div>
                    <div class="g2" style="">
                        <?php echo lang('Total-Price') ?> :   <span id="all_price"></span>
                    </div>
                    <div class="field-box raw" align="">

                        <input name="sub_mit" id="sub_mit" type="submit" class="green flt-r g2" value="<?php echo lang('save') ?>" />

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>    

<script>
    $(document).ready(function () {

        $("#get_all_quant").html($("#all_quant").text());
        $("#get_all_price").html($("#all_price").text());


    });

    function assign_sale_price(rand){

        var sale_price_type = $("#u_price_type"+rand).val();
        //a
        //alert(sale_price_type);
        if(sale_price_type == 'sale_price'){
            u_price = $("#u_sale_price"+rand).val();

        }
        else{
            u_price = $("#u_avg_price"+rand).val();
        }

        $("#price_view"+rand).html(u_price);

        assignTotalPriceByq(rand);

    }


    function assignTotalPriceByq(rand){
        //$()assignTotalPriceByq
        //obb = obj;
       // alert(rand);
        var sale_price_type = $("#u_price_type"+rand).val();
      //  alert(sale_price_type);

        q = $("#u_quantity"+rand).val();
        salep = $("#u_sale_price"+rand).val();

        if(sale_price_type == 'sale_price'){
            u_price = $("#u_sale_price"+rand).val();

        }
        else{
            u_price = $("#u_avg_price"+rand).val();
        }

        totalprice = parseFloat(u_price)*q;

        $("#totalprice_view"+rand).html(totalprice.toFixed(3));
        //totalprice_view

        calculate_total();
    }


    function calculate_total(){

        var total_val  = 0;
        var total_q = 0;

        var totalsplen = $(".totalprice_sp").length;
        var totalqlen = $(".qt").length;

        if(totalsplen>0){
            totalsp = $(".totalprice_sp");
            $.each(totalsp, function( key, value ) {
                //alert( key + ": " + value );
                spval = $(".totalprice_sp").eq(key).html();
                //alert(spval);
                total_val = parseFloat(total_val)+parseFloat(spval);
               // alert( key + ": " + value );
            });
        }

        if(totalqlen>0){
            totalq = $(".qt");
            $.each(totalq, function( key, value ) {
               // alert( key + ": " + value );
                spq = $(".qt").eq(key).val();
                //alert(spval);
                total_q = parseInt(total_q)+parseInt(spq);
            });
        }

        total_val = total_val.toFixed(3);
        $("#all_price").html(total_val);
        $("#all_quant").html(total_q);

        //total_val


    }
</script> 