<?php $this->load->view('common/meta');?>
<!--body with bg-->

<div class="body">
<header>
  <?php $this->load->view('common/header');?>
  <?php $this->load->view('common/search');?>
  <nav>
    <?php $this->load->view('common/navigations');?>
  </nav>
</header>

<!--Section-->
<section>
  <div id="container" class="row-fluid"> 
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/left-navigations');?>
    <!-- END SIDEBAR --> 
    <!-- BEGIN PAGE -->
    <div id="main-content" class="main_content">
      <div class="title"> <span>
        <?php breadcramb(); ?>
        </span> </div>
      <?php suppliers_popup(); ?>
      <form name="frm_point_of_order" enctype="multipart/form-data" action="<?php echo form_action_url('add_point_of_order'); ?>" id="frm_point_of_order" method="post" autocomplete="off">
        <input type="hidden" name="poid" id="poid" value="<?php echo $hd->poid; ?>" />
        <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
        <div class="form">
          <div class="raw form-group">
            <div class="form_title">Company</div>
            <div class="form_field">
              <div class="dropmenu">
                <div class="styled-select">
                  <?php company_dropbox('comid',$hd->comid); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="raw form-group">
            <div class="form_title">Category</div>
            <div class="form_field">
              <div class="dropmenu">
                <div class="styled-select">
                  <?php category_dropbox('categoryid',$hd->categoryid,$hd->comid,$hd->itemtype); ?>
                </div>
              </div>
            </div>
            <div class="left_div2">
              <div class="invoice_add_customer"><a href='<?php echo base_url(); ?>inventory/categories/addnew'><img src="<?php echo base_url();?>images/internal/add_customer.png" width="20" height="20" border="0" /></a> </div>
            </div>
          </div>
          <div class="raw form-group">
            <div class="form_title">Type of Purchase</div>
            <div class="form_field">
              <div class="dropmenu">
                <div class="styled-select">
                  <?php product_type('itemtype',$hd->itemtype); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="raw form-group">
            <div class="form_title">Store</div>
            <div class="form_field">
              <div class="dropmenu">
                <div class="styled-select">
                  <?php store_dropbox('storeid',$hd->storeid,$hd->comid); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="raw form-group">
            <div class="form_title">Supplier</div>
            <div class="form_field">
              <div class="dropmenu">
                <div class="styled-select">
                  <?php supplier_dropbox('supplierid',$hd->supplierid); ?>
                </div>
              </div>
            </div>
            <div class="left_div2">
              <div id="osx2-modal">
                <div class="invoice_add_customer"><a href="#1" class="osx2"><img src="<?php echo base_url(); ?>images/internal/add_customer.png" width="20" height="20" border="0" /></a> </div>
              </div>
            </div>
          </div>
          <div class="raw form-group">
            <div class="form_title">Invoice Number</div>
            <div class="form_field">
              <input name="invoicnumber" id="invoicnumber" value="<?php echo $hd->invoicnumber; ?>" type="text"  class="formtxtfield_small"/>
            </div>
          </div>
          <div class="raw form-group">
            <div class="form_title">Date of Paid</div>
            <div class="form_field">
              <input name="paid_date" id="paid_date" type="text" value="<?php echo $hd->paid_date; ?>"  class="formtxtfield"/>
              <div class="date_icon"><a href="#"><img src="<?php echo base_url();?>images/internal/date_icon.png" width="22" height="24" border="0" /></a></div>
            </div>
          </div>
          <div class="raw form-group">
            <div class="form_title">Product Name</div>
            <div class="form_field">
              <div class="dropmenu">
                <div class="styled-select">
                  <?php product_dropbox('productid',$hd->productid,$hd->comid); ?>
                </div>
              </div>
            </div>
            <div class="left_div2">
              <div> <a href="<?php echo base_url(); ?>inventory/add_item"><img src="<?php echo base_url();?>images/internal/add_customer.png" width="20" height="20" border="0" /></a> </div>
            </div>
          </div>
          <div class="raw form-group">
            <div class="form_title">Product Serial Number</div>
            <div class="form_field">
              <input name="serialnumber" id="serialnumber" type="text"  class="formtxtfield_small"/>
            </div>
          </div>
          <div class="raw form-group">
            <div class="form_title">Quantity</div>
            <div class="form_field">
              <input name="quanitity" id="quanitity" value="<?php echo $hd->quanitity; ?>" type="text"  class="formtxtfield_small"/>
              <span class="left_div">Units</span> </div>
          </div>
          <div class="raw form-group">
            <div class="form_title">Unit Price</div>
            <div class="form_field">
              <input name="unit_price" id="unit_price" value="<?php echo $hd->unit_price; ?>" type="text"  class="formtxtfield_small"/>
              <span class="left_div">RO.</span></div>
          </div>
          <div class="raw">
            <div class="form_title">Attachment Files</div>
            <div class="form_field">
              <div id="FileUpload">
                <input type="file" name="attachment" size="24" id="BrowserHidden" onchange="getElementById('FileField').value = getElementById('BrowserHidden').value;" />
                <div id="BrowserVisible">
                  <input type="text" id="FileField" />
                </div>
              </div>
            </div>
          </div>
          <div class="raw" align="center">
            <input name="submit_reorder" type="submit" class="submit_btn" value="Submit" />
            <input name="" type="reset" class="reset_btn" value="Reset" />
          </div>
          <!--end of raw--> 
        </div>
      </form>
    </div>
    <!-- END PAGE --> 
  </div>
</section>
<!-- End Section--> 
<!--footer-->
<?php $this->load->view('common/footer');?>
<script>
$(function(){
	$('.pod').change(function(){
		var comid = $('#comid').val();
		var categoryid = $('#categoryid').val();
		var itemtype = $('#itemtype').val();
		var storeid = $('#storeid').val();
		var supplierid = $('#supplierid').val();
		ajaxrequest = $.ajax({
			url: config.BASE_URL + "ajax/filter_product",
			type: 'post',
			data:{comid:comid,categoryid:categoryid,itemtype:itemtype,storeid:storeid,supplierid:supplierid},
			cache: false,			
			success: function(data)
			{
				$('#productid').html(data);
			}
		});
	});	
});
</script>
