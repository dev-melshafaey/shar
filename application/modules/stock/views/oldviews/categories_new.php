
<div id="pad-wrapper">
    <div class="row form-wrapper">
        <div class="col-md-12 col-xs-12">



            <div id="main-content" class="main_content">
                <div class="title title alert alert-info">
                    <span><?php echo lang('add-edit') ?> <span class="icon icon-fire" style="float: left;"></span></span>
                </div>

                <div class="notion title title alert alert-info">*<?php echo lang('mess1') ?> <span class="icon icon-fire" style="float: left;"></span></div>


                <form action="<?php echo form_action_url('categories'); ?>" method="post" id="frm_category" name="frm_category" autocomplete="off">
                    <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
                    <input type="hidden" name="catid" id="catid" value="<?php echo $cat->catid; ?>" />
                    <div class="form mycontent"> 
                        <div class="g4"  style="display:none;">
                            <label class=""><?php echo lang('Company-Name') ?></label>
                            <div class="">
                                <div class="ui-select">
                                    <div class="">
                                        <?php company_dropbox('companyid', $cat->companyid); ?>
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <div class="g4" style="display:none;">
                            <label class=""><?php echo lang('branch-Name') ?></label>
                            <div class="">
                                <div class="ui-select" >
                                    <div class="">

                                        <?php company_branch_dropbox('branchid', $user->branchid, $user->companyid); ?>
                                    </div>
                                </div>
                            </div>



                        </div>
                        
                        <div class="g4" style="display:none;">
                                  <label class=""><?php echo lang('Store') ?></label>
                                  <div class="">
                                     <div class="ui-select" >
                                      <div class="">
                                <?php store_dropbox('storeid',$hd->storeid,$hd->comid); ?>
                                      </div>
                                    </div>
                                  </div>
                        </div>


                        <div class="g4" style="display: none;">
                            <label class=""><?php echo lang('Category-Type') ?></label>
                            <div class="">
                                <div class="ui-select">
                                    <div class="">
                                        <?php product_type('cattype', $cat->cattype); ?>
                                    </div>
                                </div>
                            </div>
                        </div>       
                        <div class="g4">
                            <input type="hidden" name="cattype" id="cattype" value="P">
                            <label class=""><?php echo lang('Category-Name-ar') ?></label>
                            <div class="">
                                <input name="catname[arabic]" class="form-control" id="catname" value="<?php echo _s($cat->catname,"arabic"); ?>" data-msg="Enter Category" type="text" />
                            </div>
                        </div>
                        <div class="g4">
                            <label class=""><?php echo lang('Category-Name-en') ?></label>
                            <div class="">
                                <input name="catname[english]" class="form-control" id="catname" value="<?php echo _s($cat->catname,"english"); ?>" data-msg="Enter Category" type="text" />
                            </div>
                        </div>
                        <div class="g4">
                            <label class=""><?php echo lang('categType') ?></label>
                            <div class="">
                                <div class="ui-select">
                                    <div class="">
                                        <?php //get_statusdropdown($cat->catstatus, 'catstatus'); ?>
                                        <select id="category_type" name="category_type" onchange="getCatergoriesByType(this.value)">

                                            <?php
                                            if(isset($cat) && !empty($cat)){
                                                if($cat->parent_id == '0'){
                                                    ?>
                                                    <option value="parent" selected><?php echo lang('mainCateg'); ?></option>
                                                    <option value="child"><?php echo lang('childCateg'); ?></option>
                                                    <option value="subchild"><?php echo lang('subchildCateg'); ?></option>
                                                    <?php
                                                }
                                                elseif($cat->parent_id != '0' && $cat->parent_parent == ''){
                                                    ?>
                                                    <option value="parent" ><?php echo lang('mainCateg'); ?></option>
                                                    <option value="child" selected><?php echo lang('childCateg'); ?></option>
                                                    <option value="subchild"><?php echo lang('subchildCateg'); ?></option>
                                                <?php
                                                }
                                                elseif($cat->parent_id != '0' && $cat->parent_parent != ''){
                                                    ?>
                                                    <option value="parent" ><?php echo lang('mainCateg'); ?></option>
                                                    <option value="child" ><?php echo lang('childCateg'); ?></option>
                                                    <option value="subchild" selected><?php echo lang('subchildCateg'); ?></option>
                                                <?php
                                                }

                                            }
                                            else{
                                                ?>
                                                <option value="parent"><?php echo lang('mainCateg'); ?></option>
                                                <option value="child"><?php echo lang('childCateg'); ?></option>
                                                <option value="subchild"><?php echo lang('subchildCateg'); ?></option>
                                            <?php
                                            }

                                            ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $parentCategories = get_catgoriesbyparent(0);
                     //   echo "<pre>";
                   //     print_r($parentCategories);

                        //$cat->parent_id;

                        ?>
                        <div class="g4"  id="main_categ_div"  <?php if(isset($cat)) {if(!empty($cat) && $cat->parent_id == '0') { ?> style="display: none;" <?php } }else{?>style="display: none;"  <?php }  ?> >
                            <label class=""><?php echo lang('mainCateg') ?></label>
                            <div class="">
                                <div class="ui-select">
                                    <div class="">
                                        <select id="parent_cateogory_id" name="parent_cateogory_id" onchange="getsubcateg(this.value)">
                                            <option value=""><?php echo lang('choose') ?></option>
                                            <?php
                                                if(!empty($parentCategories)){
                                                    foreach($parentCategories as $parentc){
                                                        if(!empty($cat) && $cat->parent_id != '0'){
                                                                if($cat->parent_id == $parentc->catid){
                                                                    ?>
                                                                    <option value="<?php echo $parentc->catid; ?>" selected><?php  echo _s($parentc->catname,get_set_value('site_lang'));//echo $parentc->catname; ?></option>
                                                                <?php
                                                                }
                                                            else{
                                                                ?>
                                                                <option value="<?php echo $parentc->catid; ?>"><?php  echo _s($parentc->catname,get_set_value('site_lang'));//echo $parentc->catname; ?></option>
                                                            <?php
                                                            }

                                                        }
                                                        else{
                                                            ?>
                                                            <option value="<?php echo $parentc->catid; ?>"><?php  echo _s($parentc->catname,get_set_value('site_lang'));//echo $parentc->catname; ?></option>
                                                        <?php
                                                        }

                                                    }
                                                }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if(isset($cat)) { if(!empty($cat) && $cat->parent_id != '0' && $cat->parent_parent != ''){  $childCategories = get_catgoriesbyparent($cat->parent_id); } }?>
                        <div class="g4"  id="sub_categ_div" <?php if(!empty($cat) && $cat->parent_id == '0' && $cat->parent_parent == ''){ ?> style="display: none;padding-bottom: 120px;" <?php } ?>>
                            <label class=""><?php echo lang('childCateg') ?></label>
                            <div class="">
                                <div class="ui-select">
                                    <div class="">
                                        <?php //get_statusdropdown($cat->catstatus, 'catstatus'); ?>
                                        <select id="sub_cateogory_id" name="sub_cateogory_id" >
                                            <option value=""><?php echo lang('choose') ?></option>
                                            <?php
                                                    if(!empty($childCategories)){
                                                        foreach($childCategories as $cCateg){
                                                            if($cat->parent_parent !="0"){
                                                                ?>
                                                                <option value="<?php echo $cCateg->catid ?>" selected><?php  echo _s($cCateg->catname,get_set_value('site_lang')); ?></option>
                                                            <?php
                                                            }
                                                            else{
                                                                ?>
                                                                <option value="<?php echo $cCateg->catid ?>"><?php  echo _s($cCateg->catname,get_set_value('site_lang')); ?></option>
                                                                <?php
                                                            }

                                                        }

                                                    }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="g4">
                            <label class=""><?php echo lang('Status') ?></label>
                            <div class="">
                                <div class="ui-select">
                                    <div class="">
                                        <?php get_statusdropdown($cat->catstatus, 'catstatus'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br clear="all"/>
                        <div class="col-lg-12">
                            <label class=""><?php echo lang('Description') ?></label>                        <br clear="all"/>
                            <div class="">
                                <textarea name="catdescription" id="catdescription" class="formareafield ckeditor  form-control"><?php echo $cat->catdescription; ?></textarea>
                            </div>
                        </div>
                        
                        <br clear="all"/>
                        <div class="raw field-box" align="">
                            <input name="sub_mit" id="sub_mitx" type="submit" class="green flt-r g2 submit_btn" value="<?php echo lang('Add') ?>" />
                            <input name="sub_reset" type="reset" class="green flt-r g2 reset_btn" value="<?php echo lang('Reset') ?>" />
                        </div>
                        <!--end of raw field-box--> 
                    </div>
                </form>
            </div>
            <!-- END PAGE --> 
        </div>
    </div>
</div>
