<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['main']= '';
$lang['Customer-Name']= 'Customer Name';
$lang['Number-of-Invoices']= 'Number of Invoices';
$lang['Mobile-Number']= 'Mobile Number';
$lang['Type']= 'Type';
$lang['Code']= 'Code';
$lang['Debit']= 'Debit';
$lang['Credit']= 'Credit';
$lang['Total']= 'Total';
$lang['Status']= 'Status';
$lang['Notes']= 'Notes';
$lang['Responsable-Phone']= 'Responsable Phone ';
$lang['Responsable-Name']= 'Responsable Name';
$lang['Address']= 'Address';
$lang['Email-Address']= 'Email Address';
$lang['Fax-Number']= 'Fax Number';
$lang['Contact-Number']= 'Contact Number';


$lang['All']= 'All';
$lang['Category-Name']= 'Category Name';
$lang['Category-Name']= 'Category Name Ar';
$lang['Category-Name']= 'Category Name En';
$lang['Company-Name']= 'Company Name';
$lang['Category-Type']= 'Category Type';
$lang['Description']= 'Description';
$lang['Status']= 'Status';
$lang['Action']= 'Action';

$lang['Item-Picture']= 'Item Picture';
$lang['Name']= 'Name';
$lang['Supplier-Name']= 'Supplier Name';
$lang['Serial-Number']= 'Serial Number';
$lang['Store']= 'Store';
$lang['Purchase-Price']= 'Purchase Price';
$lang['Sale-Price']= 'Sale Price';
$lang['Min-Sale-Price']= 'Min Sale Price';
$lang['Point-of-Re-order']= 'Point of Re order';
$lang['Quantity']= 'Quantity';
$lang['Date']= 'Date';
$lang['Location-Address']= 'Location Address';
$lang['Finacial-Type']= 'Finacial Type';
$lang['Store-Manager']= 'Store Manager';
$lang['Manager-Phone']= 'Manager-Phone ';
$lang['Total-Qunatity-Available']= 'Total Qunatity Available';

$lang['Rent-Value']= 'Rent-Value';
$lang['Per']= 'Per';
$lang['Date-Of-Paid-Rent']= 'Date Of Paid Rent';
$lang['Owner-Phone']= 'Owner Phone';
$lang['Store-Phone-Number']= 'Store Phone Number';
$lang['Fax']= 'Fax';

$lang['Capcity']= 'Capcity';
$lang['Note']= 'Note';
$lang['Date-Of-Paid']= 'Date Of Paid ';
$lang['Instock']= 'Instock';
$lang['Point-of-Sale']= 'Point of Sale';
$lang['Net-Cost']= 'Net Cost';
$lang['Net-Profit']= 'Net Profit';

/**/
$lang['add-edit']= 'Add & Edit';
$lang['mess1']= 'Please Understand Clearly All The Data Before Entering';
$lang['Add']= 'Add';
$lang['Reset']= 'Reset';
$lang['Branch-Name']= 'Branch Name';
$lang['RePassword']= 'RePassword';
$lang['Password']= 'Password';
$lang['User-Name']= 'User Name';
$lang['Add-Account']= 'Add Account';
$lang['Company-Name']= 'Company-Name';
$lang['branch-Name']= 'Branch Name';
$lang['Store-Name']= 'Store Name';
$lang['Store-Name-ar']= 'Store Name Ar';
$lang['Store-Name-en']= 'Store Name En';
$lang['BarCode-Number']= 'Bar Code Number';
$lang['Re-order']= 'Re order';
$lang['salingbytotal']= 'In total sales';
/**/
$lang['Total-Paid']= 'Total Paid';

$lang['show_datatable']= 'Showing _START_ to _END_ of _TOTAL_ entries';
$lang['Previous']= 'Previous';
$lang['First']= 'First';
$lang['Last']= 'Last';
$lang['Next']= 'Next';
$lang['Search']= 'Search';
$lang['show_bylist']= 'Show _MENU_ entries';
$lang['receipt_no']='Receipt no';
$lang['date']='Date';
$lang['store_name']= 'Store name';
$lang['item_name']='Item Name';
$lang['item_quantity']='Quantity';
$lang['invoice_no']='Invoice Number';
$lang['customer_name']='Customer Name';

$lang['Invoice-Date']= 'Invoice Date';
$lang['complete']= 'Complete';
$lang['incomplete']= 'In complete';


$lang['Date']= 'Date';
$lang['Invoice-Number']= 'Invoice Number';
$lang['Total-Price']= 'Total-Price';
$lang['Net-Price']= 'Net-Price';
$lang['discountamount']= 'Discount amount';
$lang['Customer']= 'Customer';
$lang['Invoice-No']= 'Invoice-No';

//purchasedata

$lang['cal_purchase_price']= 'Percentage of the purchase price';
$lang['offer_dicounte']= 'Discount';
$lang['offer_dicount_sprice']= 'Selling price';
$lang['offer_dicount_from']= 'Period';
$lang['offer_dicount_to']= 'To a period';
$lang['offer_dicount_afterf']= 'After the end of the offer price';



$lang['archivepro']= 'Archive Product Purchases';


$lang['created']= 'Created ';
$lang['supplier-Name']= 'Supplier Name';
$lang['Total-p']= 'Total ';
$lang['confirm']= 'Confirm ';
$lang['code']= 'Code ';
$lang['Image']= 'Image ';
$lang['Product-Name']= 'Product Name  ';
$lang['Unit']= 'Unit ';
$lang['Discription']= 'Discription  ';
$lang['notes']= 'Notes ';
$lang['Total-quantity']= 'Total quantity ';
$lang['count-type']= 'Count';
$lang['save']= 'Save ';
$lang['fStore']= 'From Store';
$lang['tStore']= 'To Store';
$lang['pprice']= 'Purchase Price';

$lang['mess_inv_1']= 'Approval of a permit request is received';
$lang['mess_inv_1_1']= 'Orders Received';
$lang['mess_inv_2']= 'To approve the request permission disbursed';
$lang['mess_inv_3']= 'To approve the request a transfer from store to store';
$lang['mess_inv_4']= 'Transfer from store to store Orders';


$lang['type_unit']= 'Unit';

$lang['add_tabss']= 'Add';
$lang['view_all_tabss']= 'view all';

$lang['Storem']= 'Store manager';
$lang['Shelf']= 'Shelf';