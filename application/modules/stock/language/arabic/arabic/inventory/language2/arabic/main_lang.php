<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['main']= 'ادارة المنتجات';
$lang['Customer-Name']= 'العميل';
$lang['Number-of-Invoices']= 'الفواتير';
$lang['Mobile-Number']= 'رقم الجوال';$lang['p_price_aed'] = 'سعر الشراء درهم';
$lang['Type']= 'النوع';
$lang['Code']= 'الكود';
$lang['Debit']= 'مدين';
$lang['Credit']= 'ائتمان';
$lang['Total']= 'الاجمالي ';
$lang['Status']= 'الحالة';
$lang['Notes']= 'ملاحظات';
$lang['Responsable-Phone']= 'جوال المسؤل';
$lang['Responsable-Name']= 'اسم المسؤل';
$lang['Address']= 'العنوان';
$lang['Email-Address']= 'البريد الالكتروني';
$lang['Fax-Number']= 'الفاكس';
$lang['Contact-Number']= 'رقم اتصال اخر';


$lang['All']= 'الكل';
$lang['Category-Name']= 'الصنف ';
$lang['Category-Name-ar']= 'الصنف للغة العربية';
$lang['Category-Name-en']= 'الصنف للغة الانجليزيه';
$lang['Company-Name']= 'اسم الشركة';
$lang['Category-Type']= 'نوع الصنف';
$lang['Description']= 'الوصف';
$lang['Status']= 'الحالة';
$lang['Action']= 'الحدث';

$lang['Item-Picture']= 'صوره المنتج';
$lang['Name']= 'اسم المنتج  ';
$lang['Name-ar']= 'اسم المنتج للغة العربية';
$lang['Name-en']= 'اسم المنتج للغة الانجليزية';
$lang['Supplier-Name']= 'المورد';
$lang['Serial-Number']= 'السيريال';
$lang['Store']= 'المخزن';
$lang['Purchase-Price']= 'سعر الشراء';
$lang['Sale-Price']= 'سعر البيع';
$lang['Min-Sale-Price']= 'اقل سعر للبيع';
$lang['Point-of-Re-order']= 'نقاط الطلبيات';
$lang['Quantity']= 'الكمية';
$lang['Date']= 'تاريخ الانشاء';
$lang['Location-Address']= 'العنوان والمكان';
$lang['Finacial-Type']= 'نوع التعامل المادي';
$lang['Store-Manager']= 'مدير المخزن';
$lang['Manager-Phone']= 'هاتف المدير';
$lang['Total-Qunatity-Available']= 'مجموع الكميات المتاحة';

$lang['Rent-Value']= 'قيمة الايجار';
$lang['Per']= 'لكل';
$lang['Date-Of-Paid-Rent']= 'تاريخ دفع الايجار';
$lang['Owner-Phone']= 'جوال المالك';
$lang['Store-Phone-Number']= 'جوال المخزن';
$lang['Fax']= 'الفاكس';

$lang['Capcity']= 'سعة المخزن';
$lang['Note']= 'ملاحظات';
$lang['Date-Of-Paid']= 'تاريخ الدفع';
$lang['Instock']= 'في المخزن';
$lang['Point-of-Sale']= 'النقاط علي البيع';
$lang['Net-Cost']= 'صافي التكلفة';
$lang['Net-Profit']= 'تكلفة الربح';

$lang['Sale-Price_fort']= 'سعر البيع للجملة';

/*Form*/

$lang['add-edit']= 'اضافة جديد & تحديث حالي';
$lang['mess1']= 'يرجى فهم بوضوح جميع البيانات قبل الادخال';
$lang['Add']= 'اضافة';
$lang['Reset']= 'اعاده تعيين';
$lang['Branch-Name']= 'الفرع';
$lang['RePassword']= 'تاكيد كلمة المرور';
$lang['Password']= 'كلمة المرور';
$lang['User-Name']= 'اسم المستخدم';
$lang['Add-Account']= 'اضافة عضويه';
$lang['Company-Name']= 'الشركة';
$lang['branch-Name']= 'الفرع';
$lang['Store-Name']= 'اسم المخزن  ';
$lang['Store-Name-ar']= 'اسم المخزن للغة العربية';
$lang['Store-Name-en']= 'اسم المخزن للغة الانجليزية';
$lang['BarCode-Number']= 'الباركود';
$lang['Re-order']= 'اعاده الطلب';
$lang['salingbytotal']= 'سعر البيع بالجملة';
$lang['Total-Paid']= 'اجمالي الدفع';
$lang['Incomplete']= 'غير مكتمل';
$lang['Complete']= 'تم';


/**/


$lang['show_datatable']= ' عرض  من _START_   الي   _END_  سجلات '.' الاجمالي _TOTAL_ ';
$lang['Previous']= 'السابق';
$lang['First']= 'الاول';
$lang['Last']= 'الاخير';
$lang['Next']= 'التالي';
$lang['Search']= 'البحث';
$lang['show_bylist']= 'عرض_MENU_ سجلات';
$lang['receipt_no']='رقم الايصال';
$lang['date']='تاريخ';

$lang['store_name']= 'اسم المتجر';
$lang['item_name']='اسم العنصر';
$lang['item_quantity']='كمية';
$lang['invoice_no']='رقم الفاتورة';
$lang['customer_name']='اسم الزبون';

$lang['Invoice-Number']= 'رقم الفاتوره';
$lang['Total-Price']= 'اجمالي السعر';
$lang['Net-Price']= 'المبلغ النهائي';
$lang['discountamount']= 'الخصم';
$lang['Customer']= 'العميل';
$lang['Date']= 'التاريخ';

$lang['Invoice']= 'الفاتوره';
$lang['Invoice-No']= 'رقم الفاتوره';
$lang['Date']= 'تاريخ الانشاء';


$lang['cal_purchase_price']= 'النسبة من سعر الشراء';
$lang['offer_dicounte']= 'تخفيضات';
$lang['offer_dicount_sprice']= 'سعر البيع';
$lang['offer_dicount_from']= 'من فتره';
$lang['offer_dicount_to']= 'الي فتره';
$lang['offer_dicount_afterf']= 'السعر بعد انتهاء العرض';

$lang['archivepro']= 'ارشيف مشتريات المنتج';


$lang['created']= 'تاريخ الانشاء';
$lang['supplier-Name']= 'المورد';
$lang['Total-p']= 'الاجمالي ';
$lang['confirm']= 'تاكيد ';
$lang['code']= 'الكود ';
$lang['Image']= 'صورة المنتج ';
$lang['Product-Name']= 'اسم المنتج ';
$lang['Unit']= 'الوحدة ';
$lang['Discription']= 'وصف المنتج ';
$lang['notes']= 'ملاحظات ';
$lang['Total-quantity']= 'الكميات ';
$lang['count-type']= 'عدد الاصناف ';
$lang['save']= 'حفظ ';
$lang['fStore']= 'من مخزن';
$lang['tStore']= 'الي  مخزن';
$lang['pprice']= 'سعر الشراء';

$lang['mess_inv_1']= 'الموافقة علي طلب اذن استلام';
$lang['mess_inv_1_1']= 'طلبات مستلمة';
$lang['mess_inv_2']= 'الموافقة علي طلب اذن صرف';
$lang['mess_inv_3']= 'الموافقة علي طلب  تحويل من مخزن لمخزن';
$lang['mess_inv_4']= 'طلبات التحويل من مخزن لمخزن';

$lang['type_unit']= 'الوحدة';
$lang['FromStore']= 'من مخزن';
$lang['ToStore']= 'الي مخزن';
$lang['Quantity-transfer']= 'الكمية المحولة';

$lang['add_tabss']= 'أضافة';
$lang['view_all_tabss']= 'عرض الكل';
$lang['Storem']= 'مدير المخزن';
$lang['Shelf']= ' الرف';