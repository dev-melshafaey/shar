<?php $this->load->view('common/meta');?>
<!--body with bg-->

<div class="body">
<header>
  <?php $this->load->view('common/header');?>
  <?php $this->load->view('common/search');?>
  <nav>
    <?php $this->load->view('common/navigations');?>
  </nav>
</header>

<!--Section-->
<section>
  <div id="container" class="row-fluid"> 
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/left-navigations');?>
    <!-- END SIDEBAR --> 
    <!-- BEGIN PAGE -->
    <div id="main-content" class="main_content">
      <div id="note_list">
        <div class="note">
          <div class="box_titlebar"> <span>Users</span>
            <div class="closediv"><img src="<?php echo base_url();?>images/internal/close-div.png" width="12" height="12" border="0" /></div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">New Users</a></div>
              <div class="box_raw_num">25</div>
            </div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">Online Users</a></div>
              <div class="box_raw_num">21</div>
            </div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">Online Users</a></div>
              <div class="box_raw_num">21</div>
            </div>
            
            <!--space-->
            <div class="space">&nbsp;</div>
            <!--end space--> 
          </div>
        </div>
        <div class="note">
          <div class="box_titlebar"> <span>Maintenance &amp; Visits</span>
            <div class="closediv"><img src="<?php echo base_url();?>images/internal/close-div.png" width="12" height="12" border="0" /></div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">Maintenance & Visits</a></div>
              <div class="box_raw_num">25</div>
            </div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">Customers</a></div>
              <div class="box_raw_num">22</div>
            </div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">First Visit After (Days)</a></div>
              <div class="box_raw_num">25 Days</div>
            </div>
            <!--space-->
            <div class="space">&nbsp;</div>
            <!--end space--> 
          </div>
        </div>
        <div class="note">
          <div class="box_titlebar"> <span>Payments</span>
            <div class="closediv"><img src="<?php echo base_url();?>images/internal/close-div.png" width="12" height="12" border="0" /></div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">Payments</a></div>
              <div class="box_raw_num">25</div>
            </div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#"> Customers</a></div>
              <div class="box_raw_num">21</div>
            </div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">First Payment After (Days)</a></div>
              <div class="box_raw_num">52 Day</div>
            </div>
            <!--space-->
            <div class="space">&nbsp;</div>
            <!--end space--> 
          </div>
        </div>
        <div class="note">
          <div class="box_titlebar"> <span>Inventory</span>
            <div class="closediv"><img src="<?php echo base_url();?>images/internal/close-div.png" width="12" height="12" border="0" /></div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">Products</a></div>
              <div class="box_raw_num">25</div>
            </div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">Instock</a></div>
              <div class="box_raw_num">21</div>
            </div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">Point of Re-Order</a></div>
              <div class="box_raw_num">255</div>
            </div>
            <!--space-->
            <div class="space">&nbsp;</div>
            <!--end space--> 
          </div>
        </div>
        <div class="note">
          <div class="box_titlebar"> <span>Cheques</span>
            <div class="closediv"><img src="<?php echo base_url();?>images/internal/close-div.png" width="12" height="12" border="0" /></div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">Cheques</a></div>
              <div class="box_raw_num">25</div>
            </div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">Customers</a></div>
              <div class="box_raw_num">21</div>
            </div>
            <div class="box_raw">
              <div class="box_raw_title"><a href="#">First Cheque After (Days)</a></div>
              <div class="box_raw_num">81 Day</div>
            </div>
            <!--space-->
            <div class="space">&nbsp;</div>
            <!--end space--> 
          </div>
        </div>
      </div>
    </div>
    <!-- END PAGE --> 
  </div>
</section>
<!-- End Section--> 
<!--footer-->
<?php $this->load->view('common/footer');?>
