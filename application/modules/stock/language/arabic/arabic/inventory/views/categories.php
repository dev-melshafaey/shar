
<div id="pad-wrapper">

    <div class="table-wrapper users-table ">
        <form action="<?php //echo form_action_url('delete_customers');  ?>" id="listing" method="post" autocomplete="off">
            <div class=" head">
                <div class="">
                    <h4>

                        <div class="title"> <span><?php breadcramb(); ?></span> </div>


                    </h4>
                    <?php error_hander($this->input->get('e')); ?>
                </div>
            </div>



            <div class="">
                <div class="">
                    <?php //action_buttons('categories/addnew', $cnt); ?>
                    <table id="new_data_table">

                        <form action="<?php //echo form_action_url('bulk_delete');  ?>" id="listing" method="post" autocomplete="off">
                            <thead class="thead">
                                <tr>
                                    <th width="1%" id="no_filter"><label for="checkbox"></label>
                                        <?php echo lang('All') ?></th>
                                    <th width="20%"><?php echo lang('Category-Name-ar') ?></th>
                                    <th width="20%"><?php echo lang('Company-Name') ?></th>
                                    <th width="20%" ><?php echo lang('Category-Type') ?></th>
                                    <th width="25%" ><?php echo lang('Description') ?></th>
                                    <th width="5%" ><?php echo lang('Status') ?></th>
                                    <th width="10%"  id="no_filter"><?php echo lang('Action') ?></th>
                                </tr>
                            </thead>

                            <?php
                            $cnt = 0;
                            foreach ($this->inventory->categorylist() as $cat) {
                                $cnt++;
                                $j = json_decode($cat->company_name, TRUE);
                                ?>
                                <tr>
                                    <td ><input type="checkbox" class="allcb" name="u[]" id="u_<?php echo $cat->catid; ?>" value="<?php echo $cat->catid; ?>" /></td>
                                    <td ><?php //echo $cat->catname; ?><?php echo _s($cat->catname,get_set_value('site_lang')); ?></td>
                                    <td ><?php echo($j['en']); ?><br /><?php echo($j['ar']); ?></td>
                                    <td><?php product_type('', '', $cat->cattype); ?></td>
                                    <td><?php echo $cat->catdescription; ?></td>
                                    <td><?php get_status($cat->catstatus); ?></td>
                                    <td><?php edit_button('categories/addnew/' . $cat->catid); ?></td>
                                </tr>
                            <?php } ?>
                        </form>
                    </table>
                </div>
            </div>


    </div>
    <!-- END PAGE --> 
</div>
