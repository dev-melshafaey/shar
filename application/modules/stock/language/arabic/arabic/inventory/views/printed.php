<meta charset="UTF-8">
<script src="<?php echo base_url(); ?>durarthem/rtl/js/jquery.js"></script>

<style>
    table {
        border-collapse: collapse;
        border-spacing: 0;
                width: 70%;
		margin: 0 auto;
    }
    #table-pend{background-color: white !important;}
    #table-pend tr {background-color: white !important;}
    #table-pend {
        background-color: white !important;
    }



    #table-pend tr {
        background-color: white !important;
    }
    table tr {
        border-bottom: 1px solid #aaa;
        border-top: 1px solid #aaa;
        height: 40px !important;
    }
    th {
        font-size: 13px;
        font-size: 12px;
        font-weight: 700;
        line-height: 22px;
        /* border-right-color: rgba(0, 0, 0, .15); */
    }
    td, th {
        height: 20px;
        padding: 0 10px;
        text-align: center;
        border: 1px solid #999;
        line-height: 20px;
    }
</style>    

<div id="pad-wrapper" dir="rtl">
    <div class="row form-wrapper">
        <div class=" col-xs-12">

<!--            <div class="title title alert blue">
                <span> </span><span class="icon icon-infographic left" ></span>
            </div>-->
            <?php error_hander($this->input->get('e')); ?>
            <?php $get_product = $this->inventory2->get_purchase_items_re($id) ?>
            <div id="main-content" class="main_content ">


                <h2 style="text-align: center"><?php echo lang('mess_inv_1_1') ?></h2>



                <style>
                    #table-pend{background-color: white !important;}
                    #table-pend tr {background-color: white !important;}
                </style>


                <table class="table table-bordered invoice-table mb20" id="table-pend" style="text-align:center !important">
                    <thead style="text-align:center !important">
                        <tr>
                            <th class="g12 form-group">
                            </th    >
                            <th class="g12 form-group">
                                <label class=""><?php echo lang('Invoice-Number') ?>   : <?php //echo lang('Store-Name-ar')      ?> <?php echo $get_product[0]->purchase_id ?></label>


                            </th>

                            <th class="g12 form-group">
                                <label class=""><?php echo lang('Supplier-Name') ?>   : <?php //echo lang('Store-Name-ar')      ?> <?php echo $get_product[0]->fullname ?></label>


                            </th>
                            <th class="g12 form-group">
                                <label class=""><?php echo lang('Quantity') ?>   : <?php //echo lang('Store-Name-ar')      ?> <span id="get_all_quant"></span></label>


                            </th>
                             <th class="g12 form-group" colspan="7">
                             </th>
                        </tr>

                        <tr>  
                            <th>NO.<?php //echo lang('BarCode-Number') ?></th>
                            <th><?php echo lang('BarCode-Number') ?></th>
                            <th><?php echo lang('Shelf') ?></th>
                            <th><?php echo lang('Store') ?></th>
                            <th><?php echo lang('Image') ?></th>
                            <th><?php echo lang('Product-Name') ?></th>
                            <th><?php echo lang('Supplier-Name') ?></th>
                            <th><?php echo lang('Unit') ?></th>
                            <th><?php echo lang('Quantity') ?></th>
                            <th><?php echo lang('Discription') ?></th>

                            <th><?php echo lang('notes') ?></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php if ($get_product): ?>
                            <?php $count = 1 ?>
                            <?php $totalp = 0 ?>
                            <?php foreach ($get_product as $product): ?>
                                <tr id="tr_<?php echo $product->itemid ?>">

                                    <td ><?php echo $count ?></td>
                                    <td ><?php echo $product->barcodenumber ?></td>
                                    <td ><?php echo $product->shelf ?></td>
                                    <td ><?php echo _s($product->storename, get_set_value('site_lang')); ?></td>
                                    <td ><img src="<?php echo base_url() ?>uploads/item/<?php echo $product->storeid ?>/<?php echo $product->product_picture ?>" width="50" height="50" /></td>



                                    <td style="text-align:center"> <?php echo _s($product->itemname, get_set_value('site_lang')) ?> </td>
                                    <td style="text-align:center"> <?php echo $product->fullname ?> </td>
                                    <td style="text-align:center"> <?php echo get_unit($product->type_unit) ?> </td>



                                    <td style="text-align:center" id="oneitem<?php //echo $product->itemid      ?>" style="text-align:center" contentEditable="true"><?php echo $product->purchase_item_quantity ?></td>




                                    <td id="description" contentEditable="true" style="text-align:center"><?php echo $product->notes ?> </td>


                                    <td id="" style="text-align:center">

                                        

                                    </td>

                            <input type="hidden" name="product[itemid][]" value="<?php echo $product->purchase_item_id ?>"/>
                            <input type="hidden" name="product[purchase_id][]" value="<?php echo $product->purchase_id ?>"/>
                            <input type="hidden" name="product[store_id][]" value="<?php echo $product->ssid ?>"/>
                            <input type="hidden" name="product[quantity][]" value="<?php echo $product->purchase_item_quantity ?>"/>
                            <input type="hidden" name="product[inovice_product_id][]" value="<?php echo $product->inovice_product_id ?>"/>
                            <input type="hidden" name="product[supplier_id][]" value="<?php echo $product->supplier_id ?>"/>


                            </tr>
                            <?php $totalp+=$product->purchase_item_price * $product->purchase_item_quantity; ?>

                            <?php $quantity+=$product->purchase_item_quantity; ?>
                            <?php $count++; ?>
                        <?php endforeach; ?>

                    <?php else: ?>        
                        <tr><td colspan="11"><?php echo lang('no-data') ?></td></tr>
                    <?php endif; ?>
<!--                        <tr  class="grand-total">



        <td colspan="1" >

        </td> 

        <td></td>
        <td></td>
        <td></td>

        <td><div id="totalmin" style="display: none"></div></td>
        <td>   


        </td>

        <td></td>                  
        <td><?php echo lang('discount') ?></td>
        <td><strong id="netTotal"><?php echo $invoice[0]->invoice_totalDiscount; ?></strong></td>   



    </tr>-->
                    <tr  class="grand-total">



                        <td colspan="1" >

                        </td> 


                        <td><div id="totalmin" style="display: none"></div></td>
                        <td>   


                        </td>

                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                          <td></td>  <td></td>
                        <td> <?php echo lang('Total-quantity') ?> : <span id="all_quant"><?php echo $quantity ?></span></td>



                        <td style="text-align:center"><?php echo lang('count-type') ?>  : <?php echo $count-1 ?></td>    
                      


                    </tr>

                    </tbody>
                </table>
                

                <br clear="all"/>
                <br clear="all"/>

            </div>
        </div>
		<div style="text-align: center;">							
<a href="javascript:void" onclick="window.print()" class="btn btn-default"><i class="icon-print"></i>Print<?php //echo lang('Print') ?></a>
</div>
    </div>
</div>    

<script>
    $(document).ready(function () {

        $("#get_all_quant").html($("#all_quant").text());
        $("#get_all_price").html($("#all_price").text());


    });
</script> 