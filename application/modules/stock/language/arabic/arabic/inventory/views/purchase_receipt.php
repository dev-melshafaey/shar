
<div class="table-wrapper users-table">
    <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">
        <div class="row head">
            <div class="col-md-12">
                <h4>

                    <div class="title"> <span><?php echo lang('main') ?><?php //breadcramb();   ?></span> </div>


                </h4>
                <?php error_hander($this->input->get('e')); ?>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <table id="new_data_table">
                    <thead class="thead">
                        <tr>
                            <th width="" id="no_filter"><label for="checkbox"></label></th>
                            <!--<th width=""><?php echo lang('Name-ar') ?></th>-->
                            <th width=""><?php echo lang('Store') ?></th>
                            <th width=""><?php echo lang('supplier-Name') ?></th>
                            <th width=""><?php echo lang('Invoice-No') ?></th>
<!--                            <th width=""><?php echo lang('Total-Price') ?> </th>
                            <th width=""><?php echo lang('Total-Paid') ?></th>-->
                            <th width=""><?php echo lang('Quantity') ?></th>
                            <th width=""><?php echo lang('created') ?></th>
                            <th width="7%" id="no_filter" style="border-right: 1px solid gray;">&nbsp;</th>
                        </tr>
                    </thead>

                    <?php
                    $cnt = 0;
                    $total_amount = 0;
                    $total_paid = 0;
                    $userid = $this->session->userdata('bs_userid');
                    $pendingPurchase = $this->inventory2->purchase_receipt($userid);
                    if ($pendingPurchase) {
                        foreach ($pendingPurchase as $purchasedata) {
                            //echo "<pre>";
                            //print_r($purchasedata);
                            $cnt++;
                            ?>
                            <tr>
                                <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $purchasedata->purchase_id; ?>" value="<?php echo $purchasedata->purchase_id; ?>" /></td>
                                <!--<td ><a href=""><?php echo _s($purchasedata->itemname,get_set_value('site_lang')); ?></a></td>-->
                                <!--<td ><a href="<?php echo base_url(); ?>uploads/item/<?php echo $purchasedata->storeid; ?>/<?php echo $purchasedata->product_picture; ?>" class="aimg"><img src="<?php echo base_url(); ?>uploads/item/<?php echo $purchasedata->store_id; ?>/<?php echo $purchasedata->product_picture; ?>" width="32" height="32" /></a></td>-->
                                <td ><?php echo _s($purchasedata->storename,get_set_value('site_lang')); ?></td>
                                <td ><?php echo $purchasedata->fullname; ?></td>
                                <td><?php echo $purchasedata->purchase_id; ?></td>
<!--                                <td><?php echo $purchasedata->purchase_item_price;
                                     $total_amount+=$purchasedata->purchase_item_price;
                                    ?></td>
                                <td><?php echo ($purchasedata->paid) ? $purchasedata->paid : '0';
                                        $total_paid+=$purchasedata->paid;
                                ?></td>-->
                                <td><?php echo $purchasedata->quantity; ?></td>
                                <td><?php echo date('d/m/Y', strtotime($purchasedata->created)); ?></td>
                                <td style="border-right: 1px solid gray;"><?php //edit_button('addnewcustomer/'.$userdata->userid);   ?>
                                            
                                    <!--<a class="btn-flat success "  href=""><i class="icon-preview"></i></a>-->
                                    <a class="btn-flat success "  href="<?php echo base_url(); ?>inventory2/views/<?php echo $purchasedata->purchase_id; ?>"><i class="icon-eye-view"></i></a>
                                    <a class="btn-flat success "  href="<?php echo base_url(); ?>inventory2/printed/<?php echo $purchasedata->purchase_id; ?>"><i class="icon-print"></i></a>
                                    <!--<div class="groub_buuton"><br clear="all"><a class="btn-flat success "  href="<?php echo base_url(); ?>purchase_management/getPurcahseItems/<?php echo $purchasedata->purchase_id; ?>">Confirm order</a></div>-->
                                    <!-- modal content -->

                                </td>

                            </tr>
    <?php } ?>
<?php } ?>
                </table>
            </div>
        </div>
        <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>


<!--<input type="submit" class="send_icon" value=""/>-->
    </form>
</div>
