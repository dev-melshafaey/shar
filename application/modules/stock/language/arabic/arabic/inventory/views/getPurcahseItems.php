<div id="pad-wrapper">
    <div class="row form-wrapper">
        <div class=" col-xs-12">
  
            <div class="title title alert blue">
                <span><?php echo lang('mess_inv_1')?> </span><span class="icon icon-infographic left" ></span>
            </div>
            <?php error_hander($this->input->get('e')); ?>
            <?php $get_product = $this->inventory2->get_purchase_items($id) ?>
            <div id="main-content" class="main_content ">
                <form name="" action="<?php echo base_url()?>inventory2/dogetPurcahseItems/<?php echo $id?>" method="post">

       
                <div class="g12 form-group">
                    <label class=""><?php echo lang('Invoice-Number')?>   : <?php //echo lang('Store-Name-ar')  ?> <?php echo $get_product[0]->purchase_id?></label>
                    

                </div>
       
                <div class="g12 form-group">
                    <label class=""><?php echo lang('Supplier-Name')?>   : <?php //echo lang('Store-Name-ar')  ?> <?php echo $get_product[0]->fullname?></label>
                    

                </div>
                <div class="g12 form-group">
                    <label class=""><?php echo lang('Quantity')?>   : <?php //echo lang('Store-Name-ar')  ?> <span id="get_all_quant"></span></label>
                    

                </div>
                
                <input type="hidden" name="purchaseid" value="<?php echo $get_product[0]->purchase_id?>"/>
                <br clear="all"/>
                <br clear="all"/>
                <br clear="all"/>

                <style>
                    #table-pend{background-color: white !important;}
                    #table-pend tr {background-color: white !important;}
                </style>


                <table class="table table-bordered invoice-table mb20" id="table-pend" style="text-align:center !important">
                    <thead style="text-align:center !important">
                    <th><?php echo lang('confirm')?></th>
                    <th><?php echo lang('code')?></th>
                    <th><?php echo lang('Store')?></th>
                    <th><?php echo lang('Image') ?></th>
                    <th><?php echo lang('Product-Name') ?></th>
                    <th><?php echo lang('Supplier-Name') ?></th>
                    <th><?php echo lang('Unit') ?></th>
                    <th><?php echo lang('Quantity')  ?></th>
                    <th><?php echo lang('Discription') ?></th>
                    
                    <th><?php echo lang('notes') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                        
                        <?php if ($get_product): ?>
                            <?php $count =0 ?>
                            <?php $totalp = 0 ?>
                            <?php foreach ($get_product as $product): ?>
                                <tr id="tr_<?php echo $product->itemid ?>">
                                    <td >           
                                        
                                        
                                        <input type="checkbox" class="checkbox" name="product[ids][]" id="u_<?php echo $product->inovice_product_id; ?>" value="<?php echo $product->inovice_product_id; ?>" />

                                    
                                    
                                    </td>
                                    <td ><?php echo $product->itemid ?></td>
                                    <td ><?php echo _s($product->storename,get_set_value('site_lang')); ?></td>
                                    <td ><img src="<?php echo base_url() ?>uploads/item/<?php echo $product->storeid ?>/<?php echo $product->product_picture ?>" width="50" height="50" /></td>



                                    <td style="text-align:center"> <?php echo _s($product->itemname,get_set_value('site_lang')) ?> </td>
                                    <td style="text-align:center"> <?php echo $product->fullname ?> </td>
                                    <td style="text-align:center">  </td>



                                    <td style="text-align:center" id="oneitem<?php //echo $product->itemid  ?>" style="text-align:center" contentEditable="true"><?php echo $product->purchase_item_quantity ?></td>




                                    <td id="description" contentEditable="true" style="text-align:center"><?php echo $product->notes ?> </td>
                                    
                                    
                                    <td id="" style="text-align:center">
                                        
                                        <textarea name="product[note][]"></textarea>
                                        
                                    </td>
                                    
                                    <input type="hidden" name="product[itemid][]" value="<?php echo $product->purchase_item_id ?>"/>
                                    <input type="hidden" name="product[purchase_id][]" value="<?php echo $product->purchase_id ?>"/>
                                    <input type="hidden" name="product[store_id][]" value="<?php echo $product->ssid ?>"/>
                                    <input type="hidden" name="product[quantity][]" value="<?php echo $product->purchase_item_quantity ?>"/>
                                    <input type="hidden" name="product[inovice_product_id][]" value="<?php echo $product->inovice_product_id ?>"/>
                                    <input type="hidden" name="product[supplier_id][]" value="<?php echo $product->supplier_id ?>"/>
                                    

                                </tr>
                                <?php $totalp+=$product->purchase_item_price*$product->purchase_item_quantity; ?>
                                
                                <?php $quantity+=$product->purchase_item_quantity;?>
                                <?php $count++;?>
                            <?php endforeach; ?>

                        <?php else: ?>        
                            <tr><td colspan="11"><?php echo lang('no-data') ?></td></tr>
                        <?php endif; ?>
<!--                        <tr  class="grand-total">



                            <td colspan="1" >

                            </td> 

                            <td></td>
                            <td></td>
                            <td></td>

                            <td><div id="totalmin" style="display: none"></div></td>
                            <td>   


                            </td>

                            <td></td>                  
                            <td><?php echo lang('discount') ?></td>
                            <td><strong id="netTotal"><?php echo $invoice[0]->invoice_totalDiscount; ?></strong></td>   



                        </tr>-->
                        <tr  class="grand-total">



                            <td colspan="1" >

                            </td> 


                            <td><div id="totalmin" style="display: none"></div></td>
                            <td>   


                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            

                            
                            <td style="text-align:center"><strong id="netTotal">      <?php echo $totalp; ?></strong></td>    
                            <td></td>


                        </tr>

                    </tbody>
                </table>
                <div class="g16" style="border: 2px dotted white;height: 4px;">

                </div>
                <div class="g10" style="">

                </div>
                <div class="g2" style="">
                    <?php echo lang('Total-quantity') ?> : <span id="all_quant"><?php echo $quantity?></span>
                </div>
                <div class="g2" style="">
                    <?php echo lang('count-type') ?>  : <?php echo $count?>
                    <input type="hidden" name="countitem" value="<?php echo $count?>"/>
                </div>
                
                <div class="field-box raw" align="">

                    <input name="sub_mit" id="sub_mit" type="submit" class="green flt-r g2" value="<?php echo lang('save')  ?>" />

                </div>
                </form>
            </div>
        </div>
    </div>
</div>    

<script>
    $(document).ready(function() {

        $("#get_all_quant").html($("#all_quant").text());
        $("#get_all_price").html($("#all_price").text());


    }); 
</script> 