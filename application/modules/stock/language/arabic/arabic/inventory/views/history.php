
<div id="pad-wrapper">
    <?php $data = $this->inventory->product_history($pid,$ty);?>
    <?php $data2 = $this->inventory->product_history2($pid);?>
    <div class="table-wrapper users-table ">
        <form action="<?php //echo form_action_url('delete_customers');     ?>" id="listing" method="post" autocomplete="off">
            <div class="row head">
                <div class="">
                    <h4>

                        <div class="title"> <span> <?php echo ($ty!=null)? lang('Supplier-Name').">>".$data[0]->fullname : lang('archivepro').">>"._s($data[0]->itemname,get_set_value('site_lang'));?>  </span> </div>


                    </h4>
                    
                </div>
            </div>

        <?php error_hander($this->input->get('e')); ?>

            <div class="row">
                <div class="col-md-12">
                    <?php //action_buttons('add_item', $cnt); ?>   
                    <table id="new_data_table">
                        <form action="<?php //echo form_action_url('bulk_delete');     ?>" id="listing" method="post" autocomplete="off">
                            <thead class="thead">
                                <tr>
                                    <th  id="no_filter"><label for="checkbox"></label> <?php echo lang('All') ?></th>
                                    <th ><?php echo lang('Item-Picture') ?></th>
                                    <th ><?php echo lang('Name') ?></th>
                                    <th ><?php echo lang('invoice_no') ?></th>
                                    <th ><?php echo lang('Supplier-Name') ?></th>
                                    <th ><?php echo lang('BarCode-Number') ?></th>
                                    <th ><?php echo lang('Store')  ?></th>


                                    <th ><?php echo lang('item_quantity') ?> </th>

                                    


                                    <th ><?php echo lang('Date') ?></th>
                                    <!--<th width="6%">Quantity</th>-->
                                    <th  id="no_filter">&nbsp;</th>
                                </tr>
                            </thead>

                            <?php
                            $cnt = 0;
                            
                            if ($data) {
                                foreach ($data as $inv) {
                                    $cnt++;
                                    ?>
                                    <tr>
                                        <td ><input type="checkbox" class="allcb" name="u[]" id="u_<?php echo $inv->itemid; ?>" value="<?php echo $inv->itemid; ?>" /></td>
                                        <td ><a href="<?php echo base_url(); ?>uploads/item/<?php echo $inv->sitem; ?>/<?php echo $inv->product_picture; ?>" class="aimg"><img src="<?php echo base_url(); ?>uploads/item/<?php echo $inv->sitem; ?>/<?php echo $inv->product_picture; ?>" width="32" height="32" /></a></td>
                                        <td ><?php echo _s($inv->itemname,get_set_value('site_lang')); ?></td>
                                        
                                        <td >purchase#<?php echo $inv->purchase_id?></td>
                                        
                                        <td><?php echo $inv->fullname; ?></td>
                                        
                                        <td><?php echo $inv->barcodenumber; ?></td>

                                        <td><a href="<?php echo base_url() ?>inventory/storeItems/<?php echo $inv->storeid; ?>"><?php echo _s($inv->storename,get_set_value('site_lang')); ?></a></td>



                                        
                                        <td ><?php echo $inv->ssquantity; ?></td>
                                        

                                        

                                        <td ><?php echo date('Y-m-d', strtotime($inv->submissiondate)); ?></td>

                                        <td >
                                            <?php
                                            if ($udata['owner_id'] == 0) {
                                                //edit_button('add_item/' . $inv->itemid);
                                            }
                                            ?>

                                        </td>
                                    </tr>
                                </form>
                            <?php }
                        } 
                            
                            if ($data2) {
                                foreach ($data2 as $inv2) {
                                    $cnt++;
                                    ?>
                                    <tr>
                                        <td ><input type="checkbox" class="allcb" name="u[]" id="u_<?php echo $inv2->itemid; ?>" value="<?php echo $inv2->itemid; ?>" /></td>
                                        <td ><a href="<?php echo base_url(); ?>uploads/item/<?php echo $inv2->sitem; ?>/<?php echo $inv2->product_picture; ?>" class="aimg"><img src="<?php echo base_url(); ?>uploads/item/<?php echo $inv2->sitem; ?>/<?php echo $inv2->product_picture; ?>" width="32" height="32" /></a></td>
                                        <td ><?php echo _s($inv2->itemname,get_set_value('site_lang')); ?></td>
                                        
                                        <td >sale#<?php echo $inv2->recipt_id?></td>
                                        
                                        <td><?php echo $inv2->fullname; ?></td>
                                        
                                        <td><?php echo $inv2->barcodenumber; ?></td>

                                        <td><a href="<?php echo base_url() ?>inventory/storeItems/<?php echo $inv2->storeid; ?>"><?php echo _s($inv2->storename,get_set_value('site_lang')); ?></a></td>




                                        <td ><?php echo $inv2->ssquantity; ?></td>
                                        

                                        

                                        <td ><?php echo date('Y-m-d', strtotime($inv2->submissiondate)); ?></td>

                                        <td >
                                            <?php
                                            if ($udata['owner_id'] == 0) {
                                                //edit_button('add_item/' . $inv->itemid);
                                            }
                                            ?>

                                        </td>
                                    </tr>
                                </form>
                            <?php }
                        } 
                        
                        
                        ?>
                    </table>
                </div>
            </div>
            <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>

    </div>
    <!-- END PAGE --> 
</div>
