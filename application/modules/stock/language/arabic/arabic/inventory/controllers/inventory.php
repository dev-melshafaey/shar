<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inventory extends CI_Controller {
    /*
     * Properties
     */

    private $_data = array();
    private $remin = 0;

//----------------------------------------------------------------------
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        //load Home Model
		//echo get_set_value('site_lang');
        $this->load->model('inventory_model', 'inventory');
        $this->lang->load('main', get_set_value('site_lang'));
        //$this->_data['udata'] = userinfo_permission();
    }



    /*

     * Properties

     */







    /*

     * Home Page

     */

    public function index() {

        // Load Home View

        $this->load->view('inventory', $this->_data);

    }



//----------------------------------------------------------------------



    /*

     * Home Page

     */

    public function categories($act = '', $catid = '') {

        // Load Home View

        if ($act == 'addnew') {

            $this->_data['cat'] = $this->inventory->cat_detail($catid);

            //$this->load->view('categories_new', $this->_data);



            $this->_data['inside'] = 'categories_new';

            $this->_data['inside2'] = 'categories';

            //$this->load->view('common/main', $this->_data);

        } else {

            if ($this->input->post('sub_mit')) {

                $this->inventory->add_update_category();

            } else {

//                $this->load->view('categories', $this->_data);

                $this->_data['inside'] = 'categories_new';

                $this->_data['inside2'] = 'categories';

            }

        }

        $this->load->view('common/tabs', $this->_data);

    }



    public function bulk_delete() {

        $this->inventory->delete_categories();

    }



    public function bulk_delete_stores() {

        $this->inventory->delete_stores();

    }



    public function bulk_point_or_reorder() {

        $this->inventory->bulk_point_or_reorders();

    }



//----------------------------------------------------------------------



    /*

     * Add Purchase Form

     */

    public function add_purchase() {

        // Load Home View

        $this->load->view('add-purchase', $this->_data);

    }


    function test(){
        echo "dsasdasd";
        exit;
    }
    function add_sales_confirmation_items() {

        $data = $this->input->post();

        //echo "<pre>";

        //print_r($data);

        if (!empty($data['receipt'])) {



            $purchase_receipt['invoice_id'] = $data['invoiceid'];

            $this->db->insert('sales_receipt', $purchase_receipt);

            $receipt_id = $this->db->insert_id();



            foreach ($data['receipt'] as $i => $receipt) {

                if ($receipt != 0) {

                    $receipt_item_data['invoice_id'] = $data['invoiceid'];

                    $receipt_item_data['item_id'] = $data['itemid'][$i];

                    $receipt_item_data['sale_item_id'] = $data['pivid'][$i];

                    $receipt_item_data['store_id'] = $data['store_id'][$i];

                    $receipt_item_data['is_complete'] = 1;

                    $receipt_item_data['receipt_id'] = $receipt_id;

                    $this->db->insert('sales_confirm_items', $receipt_item_data);





                    $store_data['store_id'] = $data['store_id'][$i];

                    $store_data['item_id'] = $data['itemid'][$i];

                    $store_data['soled_quantity'] = $data['quantity'][$i];

                    $this->db->insert('store_soled_items', $store_data);

                }

            }





            $invoice['confirm_status'] = 1;

            $this->db->where('invoiceid', $data['invoiceid']);

            $this->db->update('bs_sales_invoice', $invoice);



            redirect(base_url() . 'inventory/stores?e=10');

        }

    }



    //

    function add_confirmation_items() {

        $data = $this->input->post();



        //echo "<pre>";

        //print_r($data);

        //exit;

        //$data['receipt'];

        if (!empty($data['receipt'])) {



            $purchase_receipt['invoice_id'] = $data['invoiceid'];

            $this->db->insert('purchase_receipt', $purchase_receipt);

            $receipt_id = $this->db->insert_id();



            foreach ($data['receipt'] as $i => $receipt) {

                if ($receipt != 0) {





                    //$receipt_item_data['quantity'] = $receipt_item_data['quantity'][$i];

                    $receipt_item_data['invoice_id'] = $data['invoiceid'];

                    $receipt_item_data['item_id'] = $data['itemid'][$i];

                    $receipt_item_data['purchase_item_id'] = $data['pivid'][$i];

                    $receipt_item_data['store_id'] = $data['store_id'][$i];

                    $receipt_item_data['is_complete'] = 1;

                    $receipt_item_data['receipt_id'] = $receipt_id;



                    //is_complete

                    $this->db->insert('purchase_confirm_items', $receipt_item_data);



                    $store_data['store_id'] = $data['store_id'][$i];

                    $store_data['item_id'] = $data['itemid'][$i];

                    $store_data['quantity'] = $data['quantity'][$i];

                    $store_data['purchase_id'] = $data['invoiceid'];

                    $this->db->insert('bs_store_items', $store_data);

                } else {



                    $receipt_item_data['invoice_id'] = $data['invoiceid'];

                    $receipt_item_data['item_id'] = $data['itemid'][$i];

                    $receipt_item_data['purchase_item_id'] = $data['pivid'][$i];

                    $receipt_item_data['store_id'] = $data['store_id'][$i];

                    $receipt_item_data['other_quantity'] = $data['quantiy_avail'][$i];

                    $receipt_item_data['is_complete'] = 0;

                    $receipt_item_data['receipt_id'] = $receipt_id;

                    $receipt_item_data['reason'] = $data['reason'][$a];



                    $this->db->insert('purchase_confirm_items', $receipt_item_data);

                    if ($data['quantiy_avail'][$i] != '0') {

                        $store_data['store_id'] = $data['store_id'][$i];

                        $store_data['item_id'] = $data['itemid'][$i];

                        $store_data['quantity'] = $data['quantiy_avail'][$i];

                        $store_data['purchase_id'] = $data['invoiceid'];

                        $this->db->insert('bs_store_items', $store_data);

                    }

                }

            }



            $invoice['confirm_status'] = 1;

            $this->db->where('invoiceid', $data['invoiceid']);

            $this->db->update('bs_purchase_invoice', $invoice);

        }



        //inventory/stores

        redirect(base_url() . 'inventory/stores?e=10');

    }



//----------------------------------------------------------------------



    /*

     * Item Listing Page

     */

    function ttt(){
        echo "items";
        exit;

    }
    public function items($id = "") {


        /* $this->_data['products']	=	$this->inventory->product_list();



          echo '<pre>'; print_r($this->_data['products']); */

        // Load Home View

        $this->_data['storeid'] = $id;

        //$this->load->view('items', $this->_data);



        $this->_data['inside2'] = 'items';

        $this->_data['inside'] = 'add-item';





        $this->load->view('common/tabs', $this->_data);

    }



//----------------------------------------------------------------------



    /*

     * Add New Item

     */

    public function add_item($itemid) {

        // Load Home View

        if (post()) {

            $this->inventory->add_new_item();

            //var_dump($_POST);

            //die();

        }



        if ($itemid != '') {

            $this->_data['hd'] = $this->inventory->get_item_detail($itemid);

        }

        //$this->load->view('add-item', $this->_data);

        $this->_data['inside'] = 'add-item';

        $this->load->view('common/main', $this->_data);

    }



    function history($id = null, $ty = null) {



        $this->_data['pid'] = $id;

        $this->_data['ty'] = $ty;

        $this->_data['inside'] = 'history';

        $this->load->view('common/main', $this->_data);

    }



    public function add_point_of_order($poid) {

        // Load Home View

        if ($this->input->post('submit_reorder') == 'Submit') {

            $this->inventory->add_new_point_of_order();

        }



        if ($poid != '') {

            $this->_data['hd'] = $this->inventory->get_pod_detail($poid);

        }

        $this->load->view('add_purchase', $this->_data);

    }



//----------------------------------------------------------------------



    /*

     * Stores Listing Page

     */

    public function stores() {

        // Load Home View

        //$this->load->view('stores', $this->_data);

        $this->_data['inside2'] = 'stores';

        $this->_data['inside'] = 'add-store';

        $this->load->view('common/tabs', $this->_data);

    }



    function storeItems($id) {



        $this->_data['id'] = $id;



        //$this->load->view('store_items', $this->_data);

        $this->_data['inside'] = 'store_items';



        $this->load->view('common/main', $this->_data);

    }



    function purchase_receipt() {

        $this->_data['recept'] = $this->inventory->purchase_receipt();



        $this->_data['inside'] = 'purchase_receipt';

        $this->load->view('common/main', $this->_data);



        // $this->load->view('store_items', $this->_data);

    }



    function sales_recept() {



        $this->_data['recept'] = $this->inventory->sales_recept();



        $this->_data['inside'] = 'sales_receipt';

        $this->load->view('common/main', $this->_data);

    }



    function viewItems($invoice_id) {

        $this->_data['invoice_items'] = $this->inventory->getitems($invoice_id);

        $this->_data['inside'] = 'invoice_items_receipt';

        $this->load->view('common/main', $this->_data);

    }



    function viewSalesItems($invoice_id) {



        $this->_data['invoice_items'] = $this->inventory->getsalesitems($invoice_id);

        $this->_data['inside'] = 'invoice_salesitems_receipt';

        $this->load->view('common/main', $this->_data);

    }



    /*

     * Add New Item

     */



    public function add_store($storeid = '') {

        // Load Home View

        if (post()) {



            $this->inventory->addStore();

        }

        if ($storeid != '') {

            $this->_data['cat'] = $this->inventory->get_store_detail($storeid);

        }





        //$this->load->view('add-store', $this->_data);

        $this->_data['inside2'] = 'stores';

        $this->_data['inside'] = 'add-store';

        $this->load->view('common/tabs', $this->_data);

    }



    public function add_suppliers() {

        // Load Home View

        $this->inventory->add_supplier();

    }



//----------------------------------------------------------------------



    /*

     * Stores Listing Page

     */

    public function point_reorder() {

        // Load Home View

        $this->load->view('point-of-reorder', $this->_data);

    }



//----------------------------------------------------------------------







    /*     * ********************************************************* */

    ///pending_sales_list

    function pending_sales_list($id = '') {

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);

        $userid = $this->session->userdata('bs_userid');

        $this->_data['sales_invoice'] = $this->inventory->getPendingSales($userid);

        $this->_data['inside'] = 'pending_sales_invoice';

        //pending_sales_list

        $this->load->view('common/main', $this->_data);



        //  $this->_data['inside2']='pending_purchase_invoice';

        // $this->_data['inside']='add-customer-form';

        //   $this->load->view('common/tabs', $this->_data);

    }



    function getSalesItems($id = null) {

        $this->_data['id'] = $id;

        ///print_r($this->_data['sales_items']);

        $this->_data['inside'] = 'pending_sales_items';

        $this->load->view('common/main', $this->_data);

    }



    function sold_onebyone($it = null, $st = null, $sales_quantity = null, $userid = null,$inv=null) {



        //$count=1;

        //echo "<pre>";

        //echo $it.','.$st;

        $get_store_its0 = $this->inventory->getStoreItemsToSole0($it, $st);



        //print_r($get_store_its0);

        //die();

        //$sales_quantity = 23;

        foreach ($get_store_its0 as $get_store_its00) {

            echo $sales_quantity;

            //	echo "<br>";



            if ($sales_quantity > 0) {

                if ($get_store_its00->remaining > 0) {

                    if ($get_store_its00->remaining >= $sales_quantity) {

                        //echo "if";

                        //echo "<br>";

                        //insertinsolditems();

                        $this->insertinsolditems($get_store_its00, $sales_quantity, $userid,$inv);



                        //var_dump($get_store_its00)."<br/>";

                        //echo $sales_quantity."<br/>";



                        $sales_quantity = 0;



                        break;

                    } else {

                        //echo "else";

                        //echo "<br>";

                        $sales_quantity = $sales_quantity - $get_store_its00->remaining;

                        //var_dump($get_store_its00)."<br/>";

                        //echo $get_store_its00->remaining."<br/>";



                        $this->insertinsolditems($get_store_its00, $get_store_its00->remaining, $userid,$inv);



                        //insertinsolditems();

                    }

                }

            }

        }



        //die('trs');

    }



    function insertinsolditems($data, $quantity, $userid = null,$inv=null) {

        $insertsoled['store_item_id'] = $data->store_item_id;

        $insertsoled['store_id'] = $data->store_id;

        $insertsoled['item_id'] = $data->item_id;

        $insertsoled['supplier_id'] = $data->supplier_id;

        $insertsoled['soled_quantity'] = $quantity;

        $insertsoled['recipt_id'] = $inv;

        $insertsoled['userid'] = $userid;

        return $this->db->insert('store_soled_items', $insertsoled);

    }



    function dogetsaleItems($id = null) {



        if (post('product')['ids']) {

            $count = 0;





            foreach (post('product')['ids'] as $key => $sale) {



                //echo post('product')['itemid'][$key].','. post('product')['store_id'][$key].','.post('product')['quantity'][$key].','.post('userid');

                //$q=post('product')['quantity'][$key];

                //$q = 9;

                $get_store_its = $this->inventory->get_store_it(post('product')['store_id'][$key], post('product')['itemid'][$key]);



                $this->sold_onebyone(post('product')['itemid'][$key], post('product')['store_id'][$key], post('product')['quantity'][$key], post('userid'),$id);

                /*

                  $quantity=$get_store_its->squan-$get_store_its->soldquant;

                  if($quantity-$q > 0){

                  $fquantity=$q;

                  }else{

                  $fquantity=$quantity-$q;



                  }

                  $this->db->insert('store_soled_items',array('store_id'=>post('product')['store_id'][$key],'soled_quantity'=>$fquantity,'item_id'=>post('product')['itemid'][$key]));





                 */

                $sitem['store_id'] = post('product')['store_id'][$key];

                $sitem['item_id'] = post('product')['ids'][$key];

                $sitem['receipt_id'] = post('product')['invoice_id'][$key];

                $sitem['note'] = post('product')['note'][$key];

                $sitem['quantity'] = post('product')['quantity'][$key];









                $invoice2['confirm_status'] = 1;

                $this->db->where('inovice_product_id', post('product')['ids'][$key]);

                $this->db->update('bs_invoice_items', $invoice2);



                $count++;

            }









            //die();



            if ($count < post('countitem')) {

                $invoice['confirm_status'] = 0;

                $invoice['confirm_complete'] = (post('countitem') - $count);

            } else {

                $invoice['confirm_status'] = 1;

                $invoice['confirm_complete'] = (post('countitem') - $count);

            }





            $this->db->where('invoice_id', post('invoiceid'));

            $this->db->update('an_invoice', $invoice);



            redirect(base_url() . 'inventory/pending_sales_list?e=16');

        } else {

            //redirect(base_url() . 'inventory/pending_sales_list');

            redirect(base_url() . 'inventory/getSalesItems/' . $id . '/?e=17');

        }

    }



    //----------------------------------------------------------------------

    ///pending_Purcahse_list



    function pending_purchase_list($id = '') {

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);

        $this->_data['inside'] = 'pending_purchase_invoice';

        $this->load->view('common/main', $this->_data);



        //  $this->_data['inside2']='pending_purchase_invoice';

        // $this->_data['inside']='add-customer-form';

        //   $this->load->view('common/tabs', $this->_data);

    }



    function getPurcahseItems($id = null) {

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);

        $this->_data['inside'] = 'getPurcahseItems';

        $this->load->view('common/main', $this->_data);

    }



    function dogetPurcahseItems($id = null) {



        if (post('product')['ids']) {

            $count = 0;

            foreach (post('product')['ids'] as $key => $sale) {



                $sitem['store_id'] = post('product')['store_id'][$key];

                $sitem['item_id'] = post('product')['itemid'][$key];

                $sitem['purchase_id'] = post('product')['purchase_id'][$key];

                $sitem['note'] = post('product')['note'][$key];

                $sitem['quantity'] = post('product')['quantity'][$key];

                $sitem['supplier_id'] = post('product')['supplier_id'][$key];





                //print_r($sitem);

                $sitems[] = $this->inventory->inserData('bs_store_items', $sitem);



                //$pitems['confirm_status'] = 1;

                //$this->inventory->inserData('bs_store_items', $pitems);







                $invoice2['confirm_status'] = 1;

                $this->db->where('inovice_product_id', post('product')['ids'][$key]);

                $this->db->update('bs_purchase_items', $invoice2);



                $count++;

            }



            //echo '<hr/>purchaseid:'. post('purchaseid');

            //$invoice['status'] = 1;

            //$invoice['purchases_status'] = 1;

            //$invoice['purchase_status'] = 1;

            //echo 'c'.$count."<br/>";

            //echo post('countitem');

            if ($count < post('countitem')) {

                $invoice['confirm_status'] = 0;

                $invoice['confirm_complete'] = (post('countitem') - $count);

            } else {

                $invoice['confirm_status'] = 1;

                $invoice['confirm_complete'] = (post('countitem') - $count);

            }



            //var_dump($invoice);

            $this->db->where('purchase_id', post('purchaseid'));

            $this->db->update('an_purchase', $invoice);

            redirect(base_url() . 'inventory/pending_purchase_list?e=15');

        } else {

            redirect(base_url() . 'inventory/getPurcahseItems/' . $id . '/?e=17');

        }

    }



    //----------------------------------------------------------------------

    ///pending_Purcahse_list



    function done_transfer() {



        $this->_data['inside'] = 'done_transfer';

        //pending_sales_list

        $this->load->view('common/main', $this->_data);

    }



    function pending_transfer() {



        $this->_data['inside'] = 'pending_transfer';

        //pending_sales_list

        $this->load->view('common/main', $this->_data);

    }



    function save_transfer() {



        if (post('product')['ids']) {

            $count = 0;

            foreach (post('product')['ids'] as $key => $sale) {



                $sitem['store_id'] = post('tstore_id');

                $sitem['item_id'] = post('product')['ids'][$key];

                $sitem['note'] = post('product')['note'][$key];

                $sitem['quantity'] = post('product')['quantity'][$key];

                $sitem['transfer'] = '1';

                $sitem['fromstore'] = post('fstore_id');







                $q = post('product')['quantity'][$key];



                //echo post('product')['ids'][$key];

                //$get_store_its = $this->inventory->get_store_it(post('fstore_id'), post('product')['ids'][$key]);

                $get_store_its = $this->inventory->get_store_it(post('product')['store_id'][$key], post('product')['itemid'][$key]);





                //die(post('product')['ids'][$key].":".post('fstore_id'));

                $this->sold_onebyone(post('product')['ids'][$key], post('fstore_id'), post('product')['quantity'][$key]);



                //$quantity=0;

                //echo $q;



                $quantity = $get_store_its->squan - $get_store_its->soldquant;

                $fquantity = $q;





                //$this->db->insert('store_soled_items', array('store_id' => post('fstore_id'), 'soled_quantity' => $fquantity, 'item_id' => post('product')['ids'][$key]));





                $this->db->insert('bs_store_items', array('store_id' => post('tstore_id'), 'quantity' => $fquantity, 'item_id' => post('product')['ids'][$key], 'transfer' => '1', 'fromstore' => post('fstore_id')));











                /*

                  $invoice2['confirm_status'] = 1;

                  $this->db->where('inovice_product_id', post('product')['inovice_product_id'][$key]);

                  $this->db->update('bs_purchase_items', $invoice2);



                  $count++;

                 *

                 */

            }





            /* if($count < post('countitem')){

              $invoice['confirm_status'] = 0;

              $invoice['confirm_complete'] = (post('countitem')-$count);



              }else{

              $invoice['confirm_status'] = 1;

              $invoice['confirm_complete'] = (post('countitem')-$count);



              } */



            //var_dump($invoice);

            //$this->db->where('purchase_id', post('purchaseid'));

            //$this->db->update('an_purchase', $invoice);

            redirect(base_url() . 'inventory/done_transfer?e=15');

        } else {

            redirect(base_url() . 'inventory/pending_transfer/' . $id . '/?e=17');

        }

    }



    function getallitems($par = null, $storevalue = null) {





        //$this->_data['par'] = $par;

        if ($par == 'p') {

            $this->_data['par'] = $par;

        } else {

            $this->_data['par'] = '';

        }





        if ($par != '' && $storevalue == '') {

            $this->_data['storevalue'] = $par;

        } elseif ($par != '' && $storevalue != '') {

            $this->_data['storevalue'] = $storevalue;

        }



        //echo $this->_data['par'];

        //die();

        $this->load->view('getallitems', $this->_data);

    }



    function additem_wi() {



    }

    function views($id){

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);

        $this->_data['inside'] = 'sviews';

        $this->load->view('common/main', $this->_data);



    }



    function printed($id){

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);

        //$this->_data['inside'] = 'sviews';

        $this->load->view('printed', $this->_data);



    }

    function views2($id){

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);

        $this->_data['inside'] = 'sviews2';

        $this->load->view('common/main', $this->_data);



    }



    function printed2($id){

        $this->_data['id'] = $id;

        //$this->load->view('purchase_invoice', $this->_data);

        //$this->_data['inside'] = 'sviews';

        $this->load->view('printed2', $this->_data);



    }





}
