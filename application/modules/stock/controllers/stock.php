<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Stock extends CI_Controller {
    /*
     * Properties
     */

    private $_data = array();
    private $remin = 0;

    public function __construct() {
        parent::__construct();

        //load Home Model
        //echo get_set_value('site_lang');

        $this->load->model('stock_model', 'stock');
        //$this->load->model('inventory2_model', 'inventory2');
        $this->lang->load('main', get_set_value('site_lang'));
        //$this->_data['udata'] = userinfo_permission();
    }

    function stockad() {

        $this->_data['categories'] = $this->stock->getCategories();
        //echo "<pre>";
        //print_r($this->_data['categories']);
        //$this->load->view('add-invoice-purchase', $this->_data);
        //$this->_data['pdata'] = $this->purchase_management->getpurchase($id);
        $this->_data['inside'] = 'add-stockitem';
        $this->load->view('common/main', $this->_data);
    }

    function adjust_item() {
        $this->_data['inside'] = 'adjust_item';
        $this->load->view('common/main', $this->_data);
    }

    function submit_item_data() {
        $postData = $this->input->post();
        $itemData = $postData['product'];
        $totaoproducts = count($postData['product']['ids']);
        for ($a = 0; $a < $totaoproducts; $a++) {
            $store_id = $postData['frmStoreid'];
            $qty = $itemData['quantity'][$a];
            $id = $itemData['ids'][$a];
            $sql = $this->db->get_where('store_value', array('store_id' => $store_id, 'item_id' => $id));
            if ($sql->num_rows() > 0) {
                $query = "update store_value set totalq = $qty where item_id = $id and store_id = $store_id";
                $this->db->query($query);
            } else {
                $this->db->insert('store_value', array('totalq' => $qty, 'store_id' => $store_id, 'item_id' => $id));
            }
        }
        echo true;
    }

    function adjust_stock() {
        $postData = $this->input->post();
//        echo '<pre>';
//        print_r($postData);
//        exit();
        $itemData = $postData['items'];
        $totaoproducts = count($postData['items']['productId']);
        for ($a = 0; $a < $totaoproducts; $a++) {
            $store_id = $itemData['store_id'][$a];
            $qty = $itemData['quantity'][$a];
            $id = $itemData['productId'][$a];
            $sql = $this->db->get_where('store_value', array('store_id' => $store_id, 'item_id' => $id));
            if ($sql->num_rows() > 0) {
                $query = "update store_value set totalq = $qty where item_id = $id and store_id = $store_id";
                $this->db->query($query);
            } else {
                $this->db->insert('store_value', array('totalq' => $qty, 'store_id' => $store_id, 'item_id' => $id));
            }
        }
        redirect(base_url() . 'stock/stockad' . "/?e=11");
    }

    function stockitems($id = "") {


        /* $this->_data['products']	=	$this->inventory->product_list();



          echo '<pre>'; print_r($this->_data['products']); */

        // Load Home View
        //$this->_data['categories'] = $this->stock->getCategories();
        //$this->load->view('items', $this->_data);

        $this->_data['categories'] = $this->purchase_management->getCategories();
        //$this->_data['inside'] = 'add-invoice-purchase';
        //$this->_data['inside2'] = 'stockitems';

        $this->_data['inside'] = 'add-stockitem';





        $this->load->view('common/main', $this->_data);
        //$this->load->view('common/tabs', $this->_data);
    }

}
