<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quotation_model extends CI_Model {
    /*
     * Properties
     */

//----------------------------------------------------------------------

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();

        //Load Table Names from Config
    }

//----------------------------------------------------------------------	
    function inserData($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    function update_getpending($id, $data) {
        $this->db->where('quotation_id',$id);
        $this->db->update('an_quotation', $data);
        //return $this->db->insert_id();
        return TRUE;
    }
    function update_items($id, $data) {
        $this->db->where('quotation_product_id',$id);
        $this->db->update('bs_quotation_items', $data);
        //return $this->db->insert_id();
        return TRUE;
    }

    function getSaleInvocies_new($id = '') {
        if ($id) {
            $subq = "WHERE u.`userid` = '" . $id . "'";
        }
        /*
          $sql = "SELECT
          u.`fullname` AS customername,
          u.`userid`,
          bsi.*
          FROM
          `an_invoice` AS bsi
          INNER JOIN `bs_users` AS u
          ON u.`userid` = bsi.`customer_id`
          " . $subq . ""; */
        $sql = "SELECT 
               i.`customer_id`,
               i.`quotation_id` AS quotation_id,
               i.`sales_amount`,
               i.`quotation_total_amount`,
               c.*,
               pamount,
               bii.quotation_item_discount_type AS discount_type,
               i.`quotation_date` AS i_date,
               SUM(pamount) AS amount,
               SUM(quotation_item_discount) AS iidiscount,
               SUM(quotation_item_price) AS iiprice,
               SUM(quotation_item_price_purchase) AS iiprice_purchase,
               sales_amount - pamount AS remaining,
               GROUP_CONCAT(ds.bs_user_id) AS cols,
               SUM(bs_sales_commession) AS salescommession,
               c2.fullname as ofullname
             FROM
               `an_quotation` AS i 
               
               LEFT JOIN `bs_users` AS c 
                 ON c.`userid` = i.`customer_id` 
                 
               LEFT JOIN `bs_users` AS c2 
                 ON c2.`userid` = i.`userid` 
                 
                 
               LEFT JOIN 
                 (SELECT 
                   SUM(ip.`payment_amount`) AS pamount,
                   ip.`invoice_id` 
                 FROM
                   `invoice_payments` AS ip 
                   INNER JOIN `an_quotation` AS ii 
                     ON ii.`quotation_id` = ip.`invoice_id` 
                   INNER JOIN `bs_users` AS c 
                     ON c.`userid` = ii.`customer_id` 
                 GROUP BY ip.`invoice_id`) d 
                 ON d.invoice_id = i.`quotation_id` 
               LEFT JOIN bs_quotation_items AS bii 
                 ON bii.quotation_id = i.quotation_id 
               LEFT JOIN bs_sales_quotation_persons AS spi 
                 ON spi.quotation_id = i.quotation_id 
               LEFT JOIN bs_sales AS ds 
                 ON ds.bs_sales_id = spi.sales_id 
                where i.quotation_status='1'
                
             GROUP BY i.`quotation_id` ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function getpending($id = '') {
        if ($id) {
            $subq = "WHERE u.`userid` = '" . $id . "'";
        }
        /*
          $sql = "SELECT
          u.`fullname` AS customername,
          u.`userid`,
          bsi.*
          FROM
          `an_invoice` AS bsi
          INNER JOIN `bs_users` AS u
          ON u.`userid` = bsi.`customer_id`
          " . $subq . ""; */
        $sql = "SELECT 
               i.`customer_id`,
               i.`quotation_id` AS quotation_id,
               i.`sales_amount`,
               i.`quotation_total_amount`,
               c.*,
               pamount,
               i.quotation_totalDiscount_type AS discount_type,
               i.`quotation_date` AS i_date,
               SUM(pamount) AS amount,
               quotation_totalDiscount AS iidiscount,
               SUM(quotation_item_price) AS iiprice,
               SUM(quotation_item_price_purchase) AS iiprice_purchase,
               sales_amount - pamount AS remaining,
               GROUP_CONCAT(ds.bs_user_id) AS cols,
               SUM(bs_sales_commession) AS salescommession 
             FROM
               `an_quotation` AS i 
               INNER JOIN `bs_users` AS c 
                 ON c.`userid` = i.`customer_id` 
               LEFT JOIN 
                 (SELECT 
                   SUM(ip.`payment_amount`) AS pamount,
                   ip.`invoice_id` 
                 FROM
                   `invoice_payments` AS ip 
                   INNER JOIN `an_quotation` AS ii 
                     ON ii.`quotation_id` = ip.`invoice_id` 
                   INNER JOIN `bs_users` AS c 
                     ON c.`userid` = ii.`customer_id` 
                 GROUP BY ip.`invoice_id`) d 
                 ON d.invoice_id = i.`quotation_id` 
               LEFT JOIN bs_quotation_items AS bii 
                 ON bii.quotation_id = i.quotation_id 
               LEFT JOIN bs_sales_quotation_persons AS spi 
                 ON spi.quotation_id = i.quotation_id 
               LEFT JOIN bs_sales AS ds 
                 ON ds.bs_sales_id = spi.sales_id 
                 
                 where i.quotation_status='0' and i.contracts='0'
             GROUP BY i.`quotation_id` ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function contracts_data($id = '') {
        if ($id) {
            $subq = "WHERE u.`userid` = '" . $id . "'";
        }
        /*
          $sql = "SELECT
          u.`fullname` AS customername,
          u.`userid`,
          bsi.*
          FROM
          `an_invoice` AS bsi
          INNER JOIN `bs_users` AS u
          ON u.`userid` = bsi.`customer_id`
          " . $subq . ""; */
        $sql = "SELECT 
               i.`customer_id`,
               i.`quotation_id` AS quotation_id,
               i.`sales_amount`,
               i.`quotation_total_amount`,
               c.*,
               pamount,
               i.quotation_totalDiscount_type AS discount_type,
               i.`quotation_date` AS i_date,
               SUM(pamount) AS amount,
               quotation_totalDiscount AS iidiscount,
               SUM(quotation_item_price) AS iiprice,
               SUM(quotation_item_price_purchase) AS iiprice_purchase,
               sales_amount - pamount AS remaining,
               GROUP_CONCAT(ds.bs_user_id) AS cols,
               SUM(bs_sales_commession) AS salescommession 
             FROM
               `an_quotation` AS i 
               INNER JOIN `bs_users` AS c 
                 ON c.`userid` = i.`customer_id` 
               LEFT JOIN 
                 (SELECT 
                   SUM(ip.`payment_amount`) AS pamount,
                   ip.`invoice_id` 
                 FROM
                   `invoice_payments` AS ip 
                   INNER JOIN `an_quotation` AS ii 
                     ON ii.`quotation_id` = ip.`invoice_id` 
                   INNER JOIN `bs_users` AS c 
                     ON c.`userid` = ii.`customer_id` 
                 GROUP BY ip.`invoice_id`) d 
                 ON d.invoice_id = i.`quotation_id` 
               LEFT JOIN bs_quotation_items AS bii 
                 ON bii.quotation_id = i.quotation_id 
               LEFT JOIN bs_sales_quotation_persons AS spi 
                 ON spi.quotation_id = i.quotation_id 
               LEFT JOIN bs_sales AS ds 
                 ON ds.bs_sales_id = spi.sales_id 
                 
                 where  i.contracts='1'
             GROUP BY i.`quotation_id` ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function product_list($product_id) {
        if ($product_id != "") {
            $subq = " WHERE bs_item.`itemid` =" . $product_id . "";
        } else {
            $subq = "";
        }
        //$ownerid = ownerid();
        $query = $this->db->query("SELECT 
			  `bs_category`.`catname`,
			  `bs_item`.`storeid`,
			  `bs_suppliers`.`suppliername`,
			  `bs_store`.`storename`,
			  `bs_item`.`quotationid`,
			  `bs_item`.`quotationname`,
			  `bs_item`.`quotationname`,
			  `bs_item`.`quotationtype`,
			  `bs_item`.`notes`,
			  IF(
				bs_item.quotationtype = 'P',
				`bs_item`.`serialnumber`,
				'-----'
			  ) AS `serialnumber`,
			  IF(
				bs_item.itemtype = 'P',
				CONCAT(`bs_item`.`purchase_price`),
				'-----'
			  ) AS `purchase_price`,
			  IF(
				bs_item.itemtype = 'P',
				CONCAT(`bs_item`.`sale_price`),
				'-----'
			  ) AS `sale_price`,
			  IF(
				bs_item.itemtype = 'P',
				`bs_item`.`min_sale_price`,
				'-----'
			  ) AS `min_sale_price`,
			  IF(
				bs_item.itemtype = 'P',
				`bs_item`.`point_of_re_order`,
				'-----'
			  ) AS `point_of_re_order`,
			  IF(
				bs_item.itemtype = 'P',
				`bs_item`.`quantity`,
				'-----'
			  ) AS `quantity`,
			  `bs_item`.`product_picture` 
			FROM
			  (`bs_item`) 
			  JOIN `bs_category` 
				ON `bs_item`.`categoryid` = `bs_category`.`catid` 
			  JOIN `bs_store` 
				ON `bs_item`.`storeid` = `bs_store`.`storeid` 
			  JOIN `bs_suppliers` 
				ON `bs_item`.`suppliderid` = `bs_suppliers`.`supplierid` 
				" . $subq . " 
			ORDER BY `bs_item`.`itemname` ASC ");
        //$this->db->last_query();
        if ($product_id != "") {
            return $query->row();
        } else {
            return $query->result();
        }
    }

    function getSalesPersonsData() {
        $sql = "SELECT bsales.`bs_sales_id`,bu.`fullname`,bu.`userid`,bu.`office_number`,bu.`phone_number`,bu.`employeecode`,bsales.`bs_sales_commession`,bsales.`bs_sales_commession_type` FROM `bs_sales` bsales
                INNER JOIN `bs_users` AS bu ON bu.`userid` = bsales.`bs_user_id`";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function getBankAccounts() {
        $sql = "SELECT c.`account_number` FROM `bs_company` AS c";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function getSaleInvocies($id = '', $re = null) {
        if ($id) {
            $subq = "WHERE u.`userid` = '" . $id . "'";
        }
        if ($re) {
            $subq = "WHERE bsi.`return_sales` = '" . $re . "'";
        }

        $sql = "SELECT 
			  u.`fullname` AS customername,
			  u.*,
			  bsi.* 
			FROM
			  `bs_sales_invoice_quotation` AS bsi 
			  INNER JOIN `bs_users` AS u 
				ON u.`userid` = bsi.`customerid`
				" . $subq . "";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function users_list() {
        $permission_type_id = $this->input->post('permission_type_id');
        $member_type = get_member_type();
        $query = "SELECT 
				  a.userid,
				  a.`fullname`,
				  a.`username`,
				  IF(a.`employeecode`='','-----',a.`employeecode`) AS employeecode,
				  IF(a.`office_number`='','-----',a.`office_number`) AS office_number,
				  IF(a.`fax_number`='','-----',a.`fax_number`) AS fax_number,
				  IF(a.`phone_number`='','-----',a.`phone_number`) AS phone_number,
				  IF(a.`status`='A','Active','Deactive') AS `status`,  
				  b.`permissionhead`,
				  c.`fullname` AS ownername 
				FROM
				  bs_users AS a,
				  bs_users As c,
				  bs_permission_type AS b WHERE a.`member_type` = b.`permission_type_id` AND b.`permission_type_id`=6 AND a.ownerid=c.userid AND ";
        if ($member_type != '5') {
            $query .= " (a.ownerid='" . $this->session->userdata('bs_ownerid') . "' OR a.ownerid='" . $this->session->userdata('bs_userid') . "') AND ";
        }

        $query .= " a.userid > 0 Order by a.fullname ASC";

        $q = $this->db->query($query);
        return $q->result();
    }
	/*
    function p_invoices($id = null) {

        $this->db->select('bs_invoice_quotations.*,bs_quotation.*');
        $this->db->where('bs_invoice_quotations.invocie_id', $id);
        $this->db->join('bs_quotation', 'bs_quotation.quotationid=bs_invoice_quotations.invoice_quotation_id', 'LEFT');
        //$this->db->order_by();
        $Q = $this->db->get('bs_invoice_quotations');
        return $Q->result();
    }
	*/
    public function add_new_customer($employeecode = null) {
        $data = $this->input->post();
        $userid = $this->input->post('userid');

        $data['employeecode'] = $employeecode;

        $data['upassword'] = md5($this->input->post('upassword'));
        unset($data['sub_mit'], $data['sub_reset'], $data['userid'], $data['confpassword']);

        if ($userid != '') {
            $this->db->where('userid', $userid);
            $this->db->update($this->config->quotation('table_users'), $data);
            do_redirect('add_invoice?e=10');
        } else {
            $this->db->insert($this->config->quotation('table_users'), $data);
            do_redirect('add_invoice?e=10');
        }
    }

    function getCategories() {
        $sql = "SELECT * FROM `bs_category`";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

//----------------------------------------------------------------------

    function invoice_data($id) {

        $sql = "SELECT 
                i.*,
                qi.*,
                qii.*,
                i.`quotation_id` AS quotation_id,
                c.fullname,
                c.email_address,
                c.phone_number,
                c.userid,
                c.buywithtotal
              FROM
                `an_quotation` AS i 
            INNER JOIN `bs_users` AS c 
                      ON c.`userid` = i.`customer_id` 
            LEFT JOIN `bs_quotation_items` AS qi 
                      ON qi.`quotation_id` = i.`quotation_id` 
            INNER JOIN `bs_item` AS qii 
                      ON qii.`itemid` = qi.`quotation_item_id` 
              WHERE i.quotation_id='$id'    
              ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function invoice_data2($id) {

        $sql = "SELECT 
                i.*,
                qi.*,
                qii.*,
                i.`quotation_id` AS quotation_id,
                c.fullname,
                c.buywithtotal
              FROM
                `an_quotation` AS i 
            INNER JOIN `bs_users` AS c 
                      ON c.`userid` = i.`customer_id` 
            LEFT JOIN `bs_quotation_items` AS qi 
                      ON qi.`quotation_id` = i.`quotation_id` 
            INNER JOIN `bs_item` AS qii 
                      ON qii.`itemid` = qi.`quotation_item_id` 
              WHERE i.quotation_id='$id'    
              ";
        $query = $this->db->query($sql);
        return $query->result();
    }


	
	function p_invoice($id = null) {

        $this->db->select('an_quotation.*');
        $this->db->select('bs_users.*');
        $this->db->select('bs_company.*');
        $this->db->select('company_logos.*');
        $this->db->where('an_quotation.quotation_id', $id);
        $this->db->join('bs_company','bs_company.companyid=an_quotation.companyid', 'LEFT');
        $this->db->join('bs_users','bs_users.userid=an_quotation.customer_id', 'LEFT');
        $this->db->join('company_logos','company_logos.company_id=bs_company.companyid', 'LEFT');
        //$this->db->order_by();
        $Q = $this->db->get('an_quotation');
        return $Q->row();
    }
    function p_invoices($id = null) {

        $this->db->select('bs_quotation_items.*,bs_item.*');
        $this->db->where('bs_quotation_items.quotation_id', $id);
        $this->db->join('bs_item','bs_item.itemid=bs_quotation_items.quotation_item_id', 'LEFT');
        //$this->db->order_by();
        $Q = $this->db->get('bs_quotation_items');
        return $Q->result();
    }
	
}
