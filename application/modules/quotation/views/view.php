
<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" type="text/css"/>
<div class="row head">
    <div class="g16">
        <h4>

            <div class="title"> <span><?php breadcramb(); ?> >> <?php echo lang('view_title')?></span> </div>


        </h4>
        <?php error_hander($this->input->get('e')); ?>
    </div>
</div>
<div class="row ">
            <div id="wizard" class="box nav-box wizard g16">
                <ul class="nav">
                    <li data-nav="#Customer" class="sel" class="Customer"><?php echo lang('Customer')?></li>
                    <li data-nav="#tproduct" class="tproduct"><?php echo lang('tproduct')?><span class="arrow">(</span></li>



                </ul>
        <div id="wizard-body" class="nav-cont vt">

        <div id="Customer" class="nav-item show pad">
            <div class="g16 form-group" id="customer">
                <?php //if (get_member_type() == '1' OR get_member_type() == '5'): ?>
                    <div class="g4 form-group">
                        <label class="text-warning"><?php echo lang('Company-Name') ?> :</label>
                        <br>
                        <div  class="ui-select" style="width:100%">
                            <div class="styled-select " style="width:100%">
                                <?php company_dropbox('companyid', $invoice[0]->companyid, $user->companyid); ?>
                            </div>
                        </div>
                    </div>
                    <div class="g4 form-group">
                        <div class="field-box">
                            <label class="text-warning"><?php echo lang('Branch-Name') ?> :</label>
                            <br>
                            <div  class="ui-select" style="width:100%">
                                <div class="styled-select " style="width:100%">
                                    <?php company_branch_dropbox('branchid', $invoice[0]->branchid, $user->branchid, $user->companyid); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?PHP //endif; ?>
                <div class="g2 form-group">
                    <input id="customerid-val" name="customerid-val" type="hidden" value=''/>
                    <input id="customerid" name="customerid" type="hidden" value=''/>
                    <label class="text-warning"><?php echo lang('Customer') ?> :</label>
                    <br>
                    <input id="customername" disabled="disabled" name="customername" value="<?php echo $invoice[0]->fullname ?>" type="text" class="form-control"/>
                </div>
                <div class="g2 form-group">
                    <label class="text-warning"></label>

                    
                </div>


                <script>

                    $(document).ready(function () {



                        $(".get_balance").load(config.BASE_URL + "sales_management/get_balance/" +<?php echo $invoice[0]->customer_id ?>);


                        $.ajax({
                            url: config.BASE_URL + "ajax/branch_list",
                            type: 'post',
                            data: {companyid: "<?php echo $invoice[0]->companyid ?>", branch:<?php echo $invoice[0]->branchid ?>},
                            cache: false,
                            //dataType:"json",
                            success: function (data)
                            {
                                var response = $.parseJSON(data);

                                $('#branchid').html(response.dropdown);
                                $('#storeid').html(response.store);
                                $('#customerid').html(response.customer);
                            }
                        });





                    });

                </script>
                <div class="g16 get_balance">
                    <div class="g3 green"><?php echo lang('balance1') ?> 0</div>
                    <div class="g3 green"><?php echo lang('balance2') ?> 0</div>
                    <div class="g3 red"><?php echo lang('balance4') ?> 0</div>
                    <div class="g3 red"><?php echo lang('balance3') ?> 0</div>
                </div>


            </div>
            <br clear="all"/>
        </div>

        <div id="tproduct" class="nav-item pad-m">
            <!-- invoice table -->
            <table class="table table-bordered invoice-table mb20">
                <thead class="thead">
                <th>#</th>
                <th><?php echo lang('Image') ?></th>
                <th><?php echo lang('Product-Name') ?></th>
                <th><?php echo lang('Discription') ?></th>
                <th><?php echo lang('sforone') ?></th>
                <th><?php echo lang('Quantity') ?></th>
                <th><?php echo lang('Total') ?></th>

                </tr>
                </thead>
                <tbody>
                    <?php $get_product = get_quotation_items($id) ?>
                    <?php if ($get_product): ?>
                        <?php $count = 1 ?>
                        <?php $totalp = 0 ?>
                        <?php foreach ($get_product as $product): ?>
                            <tr id="tr_<?php echo $product->itemid ?>">
                                <td ><?php echo $product->itemid ?></td>
                                <td ><img src="<?php echo base_url() ?>uploads/item/<?php echo $product->product_picture ?>" width="50" height="50" /></td>



                                <td style="text-align:center"> <?php echo _s($product->itemname,get_set_value('site_lang')) ?> </td>
                                <td id="description" contentEditable="true"><?php echo $product->notes ?> </td>


                                <td id="oneitem<?php echo $product->itemid ?>" style="text-align:center" contentEditable="true"><?php echo $product->iip ?></td>



                                <td id="quantity_tp<?php echo $product->itemid ?>" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup=""><?php echo $product->quotation_item_quantity ?> </td>

                                <td id="totalAmount" style="text-align:center" contentEditable="true" ><?php echo $product->iip ?></td>

                            </tr>
                            <?php //$totalp+=$product->iip; ?>
                        <?php endforeach; ?>

                    <?php else:?>        
                            <tr><td colspan="7"><?php echo lang('no-data')?></td></tr>
                    <?php endif; ?>
                    <tr  class="grand-total">



                        <td colspan="1" >

                        </td> 
                        <td></td>

                        <td><div id="totalmin" style="display: none"></div></td>
                        <td>   


                        </td>

                        <td></td>                  
                        <td><?php echo lang('discount')?></td>
                        <td><strong id="netTotal"><?php echo $invoice[0]->quotation_totalDiscount; ?></strong></td>   
                       
                        

                    </tr>
                    <tr  class="grand-total">



                        <td colspan="1" >

                        </td> 
                        <td></td>

                        <td><div id="totalmin" style="display: none"></div></td>
                        <td>   


                        </td>

                        <td></td>
 
                         <td><?php echo lang('Total-Price') ?></td>
                        <td><strong id="netTotal"><?php echo $invoice[0]->quotation_total_amount; ?></strong></td>    
                        
                        

                    </tr>
                    <?PHP $get_sales=get_quotation_sales($id);?>
                    <?php if($get_sales):?>
                        <?php foreach ($get_sales as $get_sale): ?>
                           <tr  class="">



                                <td colspan="1" >

                                </td> 
                                <td><?php echo lang('salesperson')?></td>
                                <td><?php echo $get_sale->fullname?></td>
                                <td><?php echo lang('Payment-amount')?></td>
                                <td><?php echo $get_sale->amount?></td>
                                <td></td>
                                <td></td>    



                            </tr>
                        <?php endforeach;?>
                    <?php endif;?>
                </tbody>
            </table>

            <!-- #end invoice table -->
        </div>



        <br clear="all"/>
        <a href="<?php echo base_url() ?>quotation/printed/<?php echo $id ?>" class="btn btn-default"><i class="ion ion-printer"></i><?php echo lang('Print') ?></a>
    </div>    


</div>    
</div>    





<script type="text/javascript" src="<?php echo base_url(); ?>js/function.js" ></script> 


<!--------->
