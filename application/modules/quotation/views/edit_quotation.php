<?php $row = rand(33, 850) ?>
<style>
    .ui-autocomplete {
        height: 120px !important;
        overflow: scroll !important;
    }
</style>
<script type="text/javascript">
    function swapme2p() {
        pid = $("#productid").val();
        pname = $("#productname2").val();
        $("#productname").val(pname);
        $("#productid").val(pid);
    }
    function swapmeu() {

        pid = $("#customerid").val();
        pname = $("#customername2").val();
        $("#customername").val(pname);
        $("#customerid").val(pid);
        $("#customerid-val").val(pname);
    }
    $(document).ready(function () {
        //	alert('asd');

        $("#category_id").change(function () {
            val = $(this).val();
            //alert(val);
            $("#productname").trigger('keydown');
        });
        setInterval('checkData()', 1000);
        storeId = $("#store_id").val();


        $("#productname").autocomplete({
            source: function (request, response) {
                $.getJSON("<?php echo base_url(); ?>ajax/getAutoSearchProducts", {category: $('#category_id').val(), term: $('#productname').val()},
                response);
            },
            minLength: 0,
            select: function (event, ui) {
                //action
                $("#productname").val(ui.item.prod);
                $("#productname2").val(ui.item.prod);
                $("#productid").val(ui.item.id);
                getProductData(ui.item.id);
                console.log(ui);
                //swapme();
                setTimeout('swapme2p()', 500);
            }
        });

        $("#productname").focus(function () {
            $("#productname").trigger('keyup');
        });
        var ac_config = {
            source: "<?php echo base_url(); ?>ajax/getAutoSearchCustomer",
            select: function (event, ui) {
                $("#customerid").val(ui.item.id);
                $("#customername").val(ui.item.cus);
                $("#customername2").val(ui.item.cus);
                $("#customer_ftotal").val(ui.item.buywithtotal);
                console.log(ui);
                //swapme();
                setTimeout('swapmeu()', 500);
                view_balance_customer(ui.item.id);

            },
            minLength: 1
        };
        $("#customername").autocomplete(ac_config);



        $('select[name=store_id]').change(function () {
            var val = $(this).val();
            var pid = $("#productid").val();
            if (val != 0) {
                $.ajax({
                    url: "<?php echo base_url() ?>sales_management/get_store_quantity",
                    type: "POST",
                    data: {val: val, pid: pid},
                    success: function (e) {

                        $('#storequantity').val(e);
                    }
                });
            } else {
                $('#storequantity').val(0);
            }

        });


    });
    function checkData() {
        plen = allProductsData.length;
        if (plen > 0) {
            $('#customername').attr("disabled", true);
        }
        else {
            $('#customername').attr("disabled", false);
        }
    }





    $(document).ready(function () {
        $('#Alternate_Price').click(function () {
            if ($("#Alternate_Price:checked").val() == 1) {

                $('#Alternate_Price_input').css('display', 'block');
                //$('#item_total_price').css('display', 'block');
                //$('#item_total_price').attr('disabled', true);
                $('.item_total_price0').attr('id', 'item_total_price2');
                $('.item_total_price0').attr('name', 'item_total_price2');
                $('.item_total_price_old').attr('id', 'item_total_price');
                $('.item_total_price_old').attr('name', 'item_total_price');
            } else {
                $('#Alternate_Price_input').css('display', 'none');
                //$('#item_total_price').removeAttr('disabled');

                $('.item_total_price0').attr('id', 'item_total_price');
                $('.item_total_price0').attr('name', 'item_total_price');
                $('.item_total_price_old').attr('id', 'item_total_price_old');
                $('.item_total_price_old').attr('name', 'item_total_price_old');
            }

            $("#item_total_price").change(function () {
                //$(this).val()
                $('#total_price').val($(this).val());
                //alert($(this).val());

            });
        });
        $('#is_payment').click(function () {
            if ($("#is_payment:checked").val() == 1) {
                $('.payment_div').css('display', 'block');
            }
        });
        $('#customerType').change(function () {
            if ($(this).val() == 'newcustomer') {

                $('#newcustomer').css('display', 'block');
                $('#customer').css('display', 'none');
                //$('.payment_div').css('display', 'block');

            } else {
                $('#customer').css('display', 'block');
                $('#newcustomer').css('display', 'none');
            }
        });



        $('.go_to_nexttabs').click(function () {
            //$( "input[value='Hot Fuzz']" ).next().text( "Hot Fuzz" );

            $("div[aria-labelledby|='tab_item-0']").css("display", "none");
            $("li[aria-controls|='tab_item-0']").removeClass("resp-tab-item resp-tab-active");
            $("li[aria-controls|='tab_item-0']").addClass("resp-tab-item");
            //$( "h2[aria-controls|='tab_item-0']" ).css( "display", "none" );

            $("div[aria-labelledby|='tab_item-1']").css("display", "block");
            $("li[aria-controls|='tab_item-1']").addClass("resp-tab-item resp-tab-active");
            //$( "h2[aria-controls|='tab_item-0']" ).css( "display", "block" );


        });
        $('.go_to_nexttabs2').click(function () {
            //$( "input[value='Hot Fuzz']" ).next().text( "Hot Fuzz" );

            $("div[aria-labelledby|='tab_item-1']").css("display", "none");
            $("li[aria-controls|='tab_item-1']").removeClass("resp-tab-item resp-tab-active");
            $("li[aria-controls|='tab_item-1']").addClass("resp-tab-item");
            //$( "h2[aria-controls|='tab_item-0']" ).css( "display", "none" );

            $("div[aria-labelledby|='tab_item-2']").css("display", "block");
            $("li[aria-controls|='tab_item-2']").addClass("resp-tab-item resp-tab-active");
            //$( "h2[aria-controls|='tab_item-0']" ).css( "display", "block" );


        });
        $('.go_to_nexttabs3').click(function () {
            //$( "input[value='Hot Fuzz']" ).next().text( "Hot Fuzz" );

            $("div[aria-labelledby|='tab_item-2']").css("display", "none");
            $("li[aria-controls|='tab_item-2']").removeClass("resp-tab-item resp-tab-active");
            $("li[aria-controls|='tab_item-2']").addClass("resp-tab-item");
            //$( "h2[aria-controls|='tab_item-0']" ).css( "display", "none" );

            $("div[aria-labelledby|='tab_item-3']").css("display", "block");
            $("li[aria-controls|='tab_item-3']").addClass("resp-tab-item resp-tab-active");
            //$( "h2[aria-controls|='tab_item-0']" ).css( "display", "block" );


        });



        $('#payment_amount').change(function () {
            //$("#remin_view_all_total").html($("#remin_view_all_total").html()+$("#payment_amount").html());
            $("#remin_view_all_total").html(parseInt($("#remin_view_all_total").html()) - parseInt($('#payment_amount').val()));

            $("#recievedamount").val(parseInt($("#payment_amount").val()));
            $("#totalrecievedamount").val(parseInt($('#payment_amount').val()));
            //alert($("#howpay").val());
            ppshow = parseInt($("#get_my_allaccount").html()) - parseInt($('#payment_amount').val());
            ppshow2 = parseInt($("#view_all_total").html()) - parseInt($('#payment_amount').val());
            if ($("#howpay").val() == "1") {

                if (parseInt($("#remin_view_all_total").html()) < 0) {
                    $("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) - parseInt($('#view_all_total').html()));
                    $("#recievedamount").val(parseInt($('#view_all_total').html()));
                    $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                    $("#remin_view_all_total").text("0");
                } else {
                    $("#get_my_allaccount").text(ppshow);
                }

            } else {

                if (parseInt($("#remin_view_all_total").html()) < 0) {

                    //alert('if');
                    $("#increse_amount_span").text(Math.abs(ppshow2));
                    $("#recievedamount").val(parseInt($('#view_all_total').html()));
                    $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                    $("#remin_view_all_total").text("0");
                    $("#increse_amount").show();
                } else {
                    //alert('else');
                    //$("#increse_amount_span").text(Math.abs(ppshow2));
                    $("#remin_view_all_total").text(ppshow2);

                }
            }
        });







    });
    function rest_payment() {

        $('#get_my_allaccount').html("0");
        $('#howpay').val("");
        $('#recievedamount').val("");
        $('#remin_view_all_total').html($('#view_all_total').html());
        $('#increse_amount').hide("");
        $('#payment_amount').val("");
        $("#add_new_payment_btn").show();
    }
    function  do_payment_amountt(e, id) {
        //$("#remin_view_all_total").html($("#remin_view_all_total").html()+$("#payment_amount").html());
        //alert(e);
        $("#remin_view_all_total").html(parseInt($("#remin_view_all_total").html()) - parseInt(e));
        $("#recievedamount").val(parseInt($("#remin_view_all_total").html()));
        $("#totalrecievedamount").val(parseInt($("#totalrecievedamount").val()) + parseInt(e));

        var remin_view = parseInt($("#remin_view_all_total").html());
        var dopshow = parseInt($("#get_my_allaccount").html()) - parseInt(e);
        var dopshow2 = remin_view - parseInt(e);

        if ($("#howpay").val() == "1") {


            //alert(remin_view);
            if (remin_view < 0) {
                $("#get_my_allaccount").text(Math.abs(dopshow));
                $("#recievedamount").val(parseInt($('#view_all_total').html()));
                $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                $("#remin_view_all_total").text("0");
                $("#payment_amountt_" + id).val(Math.abs(parseInt($('#increse_amount_span').html()) - parseInt($("#payment_amountt_" + id).val())));

                $("#add_new_payment_btn").remove();

            } else {
                $("#get_my_allaccount").text(dopshow);
            }


        } else {

            //alert(remin_view);
            //$("#remin_view_all_total").text();
            if (remin_view < 0) {
                //alert(dopshow2);
                $("#increse_amount_span").text(Math.abs(remin_view));
                $("#increse_amount").show();
                $("#recievedamount").val(parseInt($('#view_all_total').html()));
                $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                $("#remin_view_all_total").text("0");
                $("#payment_amountt_" + id).val(Math.abs(parseInt($('#increse_amount_span').html()) - parseInt($("#payment_amountt_" + id).val())));

                $("#add_new_payment_btn").hide();

            } else {
                /*
                 $("#recievedamount").val(parseInt($('#view_all_total').html()));
                 $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                 $("#remin_view_all_total").text("0");
                 */

            }
        }



    }

</script>


<script>

    $(document).ready(function () {



        $(".get_balance").load(config.BASE_URL + "sales_management/get_balance/" + <?php echo $invoice[0]->customer_id ?>);


        $.ajax({
            url: config.BASE_URL + "ajax/branch_list",
            type: 'post',
            data: {companyid: "<?php echo $invoice[0]->companyid ?>", branch: "<?php echo $invoice[0]->branchid ?>"},
            cache: false,
            //dataType:"json",
            success: function (data)
            {
                var response = $.parseJSON(data);

                $('#branchid').html(response.dropdown);
                $('#storeid').html(response.store);
                $('#customerid').html(response.customer);
            }
        });





    });

</script>
<div class="table-wrapper users-table section" style="margin-top:0px" >
    <div class="row head">
        <div class="">
            <h4>
                <div class="title"> <span>
                        <?php breadcramb(); ?>
                    </span> </div>
            </h4>
            <?php error_hander($this->input->get('e')); ?>
        </div>
    </div>
    <style>

    </style>
    <div class="row">
        <div class="" style=""> 

<!--<script src="http://www.unverse.net/whizzery/whizzywig.js"></script>--> 

            <!--Section-->

            <div id="main-content" class="main_content" > 
              <!--<div class="title"> <span><?php breadcramb(); ?></span> </div>-->
                <form action="<?php echo base_url() ?>quotation/edit_quotation/<?php echo $quotation ?>" method="POST" id="frm_invoice" name="frm_invoice" autocomplete="off">
                    <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
                    <input type="hidden" name="userid" id="userid" value="<?php echo $udata->userid; ?>" />
                    <input type="hidden"  name="invoiceData" id="invoiceData" value=""/>
                    <input type="hidden"  name="purchase" id="invoiceData" value="0"/>
                    <input type="hidden"  name="quotation" id="quotation" value="<?php echo $quotation ?>"/>
                    <!--<input type="hidden"  name="quotation_foruid" id="quotation_foruid" value="<?php echo $quotation_foruid ?>"/>-->

                    <div class="invoice_raw_product_items">

                        <?php if ($invoice): ?>
                            <?php $count = 1 ?>
                            <?php foreach ($invoice as $product): ?>

                                <div id='invoice_input_<?php echo $row + $product->quotation_product_id ?>'>
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='productId<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[productId][]' value='<?php echo $product->quotation_product_id ?>' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='quotation_item_id<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[quotation_item_id][]' value='<?php echo $product->quotation_item_id ?>' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='productQuantity<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[quantity][]' value='<?php echo $product->quotation_item_quantity ?>' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='productTotal<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[productTotal][]' value='<?php echo $product->quotation_item_price ?>' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='minimum<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[in_minimum][]' value='<?php echo $product->min_sale_price ?>'>
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='productPerPrice<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[productperPrice][]' value='<?php echo $product->sale_price ?>' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='productDiscount<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[productDiscount][]' value='<?php echo $product->quotation_item_discount ?>' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='productDiscountType<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[productDiscountType][]' value='<?php echo $product->quotation_item_discount_type ?>' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='productDiscription<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[productDiscription][]' value='<?php echo $product->quotation_item_notes ?>' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='productNotes<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[productNotes][]' value='<?php echo $product->quotation_item_notes ?>' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='supplier<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[supplier_id][]' value='' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='unit_price<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[unit_price][]' value='' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='store_id<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[store_id][]' value='' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='supplier_id<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[supplier_id][]' value='' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='item_id<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[item_id][]' value='<?php echo $product->quotation_item_id ?>' />
                                    <input type='hidden' class='pi<?php echo $row + $product->quotation_product_id ?>' id='item_id<?php echo $row + $product->quotation_product_id ?>'  name='updateitems[item_id][]' value='<?php echo $product->invoice_itype ?>' />

                                </div>
                                    
                                <?php $totlamin+=$product->min_sale_price?>
                                
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>



                    <div id="wizard" class="box nav-box wizard g16">
                        <ul class="nav">
                            <li data-nav="#Customer" class="sel Customer"><?php echo lang('tclient') ?> </li>
                            <li data-nav="#tproduct" class="tproduct"> <?php echo lang('tproduct') ?><span class="arrow">(</span></li>
                            <!--<li data-nav="#tpay"><?php echo lang('tpay') ?><span class="arrow">(</span></li>-->
                            <!--<li data-nav="#texpences"> <?php echo lang('texpences') ?><span class="arrow">(</span></li>-->
                            <li data-nav="#tbill" class="tbill"> <?php echo lang('tbill') ?><span class="arrow">(</span></li>



                        </ul>



                        <div id="wizard-body" class="nav-cont vt">
                            <!--First tabs-->
                            <div id="Customer" class="nav-item show pad">
                                <div class=" form-group">
                                    <div class=" form-group" id="customer">

                                        <div class="g4 form-group">
                                            <label class="text-warning g8"><?php echo lang('Customer') ?></label>
                                            <div class="g8" id="viewCustomer"><?php echo $invoice[0]->fullname ?></div>

                                        </div>

                                        <div class="g4 form-group">
                                            <label class="g8 text-warning"><?php echo lang('Email-Address') ?></label>
                                            <div class="g8" id="viewemail_address"><?php echo $invoice[0]->email_address ?></div>

                                        </div>

                                        <div class="g4 form-group">
                                            <label class="g8 text-warning"><?php echo lang('Phone') ?></label>
                                            <div class="g8" id="viewphone_number"><?php echo $invoice[0]->phone_number ?></div>

                                        </div>
                                        <br clear="all"/>
                                        <br clear="all"/>
                                        <?php //if (get_member_type() == '1' OR get_member_type() == '5'): ?>
                                        <div class="g4 form-group">
                                            <label class="text-warning"><?php echo lang('Company-Name') ?> :</label>
                                            <br>
                                            <div  class="ui-select" style="width:100%">
                                                <div class="styled-select " style="width:100%">
                                                    <?php company_dropbox('companyid', $invoice[0]->companyid); ?>
                                                    <span class="arrow arrowselectbox">&amp;</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="g4 form-group">
                                            <div class="field-box" >
                                                <label class="text-warning"><?php echo lang('Branch-Name') ?> :</label>
                                                <br>
                                                <div  class="ui-select" style="width:100%">
                                                    <div class="styled-select " style="width:100%">
                                                        <?php company_branch_dropbox('branchid', $invoice[0]->branchid, $invoice[0]->companyid); ?>
                                                        <span class="arrow arrowselectbox">&amp;</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?PHP //endif; ?>
                                        <div class="g5 form-group">
                                            <input id="customerid-val" name="customerid-val" type="hidden" value=''/>
                                            <input id="customerid" name="customerid" type="hidden" value='<?php echo $invoice[0]->customer_id ?>'/>
                                            <input id="customer_ftotal" name="customer_ftotal" type="hidden" value='0'/>
                                            <input id="customername2" name="customername2" type="hidden" value=''/>
                                            <label class="text-warning"><?php echo lang('Customer') ?> :</label>
                                            <br>
                                            <input id="customername" disabled="disabled" name="customername" value="<?php echo $invoice[0]->fullname ?>" type="text" class="g10 form-control"/>

                                            <a onClick="show_dialog2('#demo-modal', '<?php echo base_url(); ?>customers/getallnewcustomer')" href="javascript:void" data-toggle="" class="g2"><i class="icon-th-list left" style="font-size:20px;color:black"></i></a> 
                                            <a class="g2" onClick="show_dialog2('#demo-modal', '<?php echo base_url(); ?>customers/getAddnewcustomer')" href="javascript:void" style="cursor:pointer;"><img  src="<?php echo base_url(); ?>images/internal/add_customer.png" border="0" /></a> 
                                        </div>
                                        <div class="g1 form-group">
                                            <label class="text-warning"></label>
                                            <br>   


                                        </div>
                                        <br clear="all"/>
                                        <div class=" get_balance">
                                            <div class="g3 tag green"><?php echo lang('balance1') ?> 0</div>
                                            <div class="g3 tag green"><?php echo lang('balance2') ?> 0</div>
                                            <div class="g3 tag red"><?php echo lang('balance4') ?> 0</div>
                                            <div class="g3 tag red"><?php echo lang('balance3') ?> 0</div>
                                        </div>

                                        <div class="main_viewmess" style="display: none;clear: both;color: red;text-align: center;">
                                            <span><?php echo lang('mess_blacklist') ?> </span>
                                            <span class="viewmess"></span>
                                        </div>
                                    </div>
                                    <!--New Customer-->
                                    <div class=" form-control invoice" id="newcustomer" style="display:none;">
                                        <div class="g3 form-control">
                                            <label class="text-warning"><?php echo lang('Customer') ?> :</label>
                                            <br>
                                            <input id="customername" name="customername" type="text" class="form-control"/>
                                        </div>
                                        <div class="g3 form-control">
                                            <label class="text-warning">Mobile  No :</label>
                                            <br>
                                            <input name="phone_number" id="phone_number" type="text"  class="form-control"/>
                                        </div>
                                        <div class="g3 form-control">
                                            <label class="text-warning">Email :</label>
                                            <br>
                                            <input name="office_number" id="office_number" type="text"  class="form-control"/>
                                        </div>
                                        <div class="g3 form-control">
                                            <label class="text-warning">Address :</label>
                                            <br>
                                            <input name="product_notes" id="product_notes" type="text"  class="form-control"/>
                                        </div>
                                    </div>
                                    <br clear="all"/>
                                    <br clear="all"/>
                                    <br clear="all"/>
                                                                    <div class="nav-act btn-m">
                                    <!--<button class="mini prev">Back</button>-->
                                    <a href="javascript:void" style="padding-right: 20px;padding-left: 20px;"class="green next flt-r tag" onclick="gotonext('tproduct','Customer')"><?php echo lang('next')?>  >>  </a>
                                </div>
                                    <!--<a href="javascript:void" class="go_to_nexttabs reset_btn btn-glow primary" style="margin-top: 10px;">Next &gt;&gt;</a> <br clear="all">-->
                                </div>
                            </div>
                            <!--SECOND TAB-->
                            <div id="tproduct" class="nav-item pad-m">
                                <div class=" form-group">
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Category') ?> :</label>
                                        <br>
                                        <div class="ui-select" style="width: 100%;">
                                            <select name="category_id" id="category_id">
                                                <option><?php echo lang('choose') ?></option>
                                                <?php
                                                if (isset($categories) && !empty($categories)) {
                                                    foreach ($categories as $categ) {
                                                        ?>
                                                        <option value="<?php echo $categ->catid; ?>"><?php echo _s($categ->catname,get_set_value('site_lang')); ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <span class="arrow arrowselectbox">&amp;</span>
                                        </div>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Product-Name') ?> :</label>
                                        <br>
                                        <input  type="hidden" name="productid"  id="productid" class="formControl"/>
                                        <input type="text" name="productname"  id="productname" class="g14 form-control "/>
                                        <!--<a onClick="show_dialog2('#demo-modal', '<?php echo base_url(); ?>inventory/getallitems')" href="javascript:void" data-toggle="" class="g2"><i class="icon-th-list left" style="font-size:20px;color:black"></i></a>--> 
                                        <a onClick="show_dialog2('#demo-modal', '<?php echo base_url(); ?>inventory/getallitems/' + $('#store_id').val(), $('#store_id').val());" href="javascript:void" data-toggle="" class="g2" ><i class="icon-th-list left" style="font-size:20px;color:black"></i></a> 
                                        <input type="hidden" name="productname2"  id="productname2" class="form-control "/>
                                        <input type="hidden" name="price_purchase"  id="price_purchase" class="form-control " value=""/>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Serial-No') ?> :</label>
                                        <br>
                                        <input name="serialnumber" id="serialnumber" type="text"  class=" currentProduct form-control"/>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Store') ?> :</label>
                                        <br>
                                        <div class="ui-select" style="width: 100%;">
                                            <?php store_dropbox('store_id', '', ''); ?>
                                            <span class="arrow arrowselectbox">&amp;</span>
                                        </div>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Quantity') ?> :</label>
                                        <br>
                                        <!--<input id="quantity" name="quantity" type="text" onchange="calculatePrice()"  class=" currentProduct form-control"/>-->
                                        <input id="quantity" name="quantity" type="text" onchange=""  class=" currentProduct form-control"/>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"> <?php echo lang('store_quantity') ?></label>
                                        <br>
                                        <input name="" id="storequantity" type="text" style="color:green;font-size: 16px;text-align: center" value="<?php //echo get_store_quantity(3);       ?>" disabled="disabled" class=" currentProduct form-control"/>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning" id="fortaotal"><?php echo lang('sforone') ?> :</label>
                                        <br>
                                        <input name="item_total_price" id="item_total_price" type="text"  class="item_total_price0 currentProduct form-control"/>
                                    </div>
                                    <div class="g2 form-group">
                                        <label class="text-warning"><?php echo lang('Alternate-Price') ?> :</label>
                                        <br>
                                        <input name="Alternate_Price" id="Alternate_Price" type="checkbox" value="1"  class="" style="width:10%"/>
                                    </div>
                                    <div class="g2 form-group"> <br>
                                        <div  id="Alternate_Price_input" style="display: none">
                                            <input name="item_total_price_old" id="item_total_price_old" type="text"  class="item_total_price_old currentProduct form-control"/>
                                        </div>
                                    </div>
                                    <br clear="all">
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Discount') ?> :</label>
                                        <br>
                                        <input id="item_discount" name="item_discount--" onkeyup="calculateDis()" type="text"  class="form-control"/>
                                    </div>
                                    <div class="g4 form-group"> <br>
                                        &nbsp; %
                                        <input type="radio" onchange="calculateDis()" name="type" id="type1" value="1"  class="currentProduct" />
                                        RO
                                        <input type="radio" onchange="calculateDis()" name="type" id="type2" value="2" class="currentProduct"/>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('afterDiscount') ?> :</label>
                                        <br>
                                        <input id="afterDiscount" name="item_discount" onchange="" type="text"  class="form-control"/>
                                    </div>
                                    <div class="g4 form-group">
                                        <label class="text-warning"><?php echo lang('Minimum') ?> :</label>
                                        <br>
                                        <input name="min_price" id="min_price" type="text"  class="currentProduct form-control" disabled="disabled"/>
                                        <input type="hidden" id="total_price" name="total_price"  value="" />
                                        <input type="hidden" id="price_product" name="price_product" />
                                        <input type="hidden" id="store_id" name="store_id" />
                                    </div>
                                    <div class="g8 form-group">
                                        <label class="text-warning"><?php echo lang('Discription') ?> :</label>
                                        <br>
                                        <textarea cols="100" style="height: 55px;"  rows="10"  name="product_comment" id="product_comment" class="dis_txtarea currentProduct form-control" ></textarea>
                                    </div>
                                    <div class="g8 form-group">
                                        <label class="text-warning"><?php echo lang('Notes') ?> :</label>
                                        <br>
                                        <textarea name="product_notes" style="height: 55px;"  id="product_notes" cols="100" rows="10" class="dis_txtarea currentProduct form-control" ></textarea>
                                    </div>
                                    <input type="hidden" id="product_picture" name="product_picture" />
                                    <input type="hidden" id="storeid_p" name="storeid_p" />
                                    <br  clear="all"/>
                                    <br  clear="all"/>
                                    <div class=" form-group">
                                        <div class="" onclick="addItem_edit()" style="border-bottom: 1px solid green;margin-bottom: -1px;position: relative;"> 
                                            <div style="border-width: 1px 1px 0px 1px;width: 113px;cursor: pointer;border-color: green;border-style: solid;clear: both;padding: 7px;">
                                                <i class="icon-addtocart left" style="font-size:20px;"></i>
                                                <?php echo lang('add-product') ?>
                                                <br clear="all"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group product_list">
                                        <table width="100%" align="left" id="product_list" >
                                            <thead>
                                                <tr>

                                                    <th></th>
                                                    <th><?php echo lang('Image') ?></th>
                                                    <th><?php echo lang('Product-Name') ?></th>
                                                    <th><?php echo lang('Discription') ?></th>
                                                    <th><?php echo lang('sforone') ?></th>
                                                    <th><?php echo lang('Quantity') ?></th>
                                                    <th><?php echo lang('Total') ?></th>
                                                    <th><i class="icon-remove-sign" style="font-size:20px;color: red;"></i></th>
                                                    <th><i class="nav-icon icon-settingstwo-gearalt" style="font-size:20px;"></i></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if ($invoice): ?>
                                                    <?php $count = 1 ?>
                                                    <?php foreach ($invoice as $product): ?>

                                                        <tr id="tr_<?php echo $row+$product->quotation_product_id ?>">
                                                            <td ></td>
                                                            <td ><a href="<?php echo base_url() ?>uploads/item/<?php echo $product->storeid ?>/<?php echo $product->product_picture ?>" class="aimg"><img src="<?php echo base_url() ?>uploads/item/<?php echo $product->storeid ?>/<?php echo $product->product_picture ?>" width="32" height="32" /></a></td>



                                                            <td style="text-align:center"> <?php echo _s($product->itemname, get_set_value('site_lang')) ?> </td>
                                                            <td id="description" contentEditable="true"><?php echo $product->notes ?> </td>


                        <!--                                                            <td id="oneitem<?php echo $product->itemid ?>" style="text-align:center" contentEditable="true"><?php echo $product->purchase_price ?></td>

                                                                                    <td id="quantity_tp<?php echo $product->itemid ?>" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculatePrice2(this,<?php echo $product->itemid ?>)"><?php echo $product->quotation_item_quantity ?> </td>

                                                                                    <td id="totalAmount" style="text-align:center" contentEditable="true" ><?php echo $product->quotation_item_price ?></td>-->



                                                            <td id="oneitem<?php echo $row+$product->quotation_product_id ?>" style="text-align:center" onkeyup="calculate_pre_Price(this, '<?php echo $row+$product->quotation_product_id ?>')" contentEditable="true"><?php echo $product->quotation_item_price ?></td>

                                                            <td id="quantity_tp<?php echo $row+$product->quotation_product_id ?>" class="quantity_tp" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculatePrice2(this, '<?php echo $row+$product->quotation_product_id ?>')" ><?php echo $product->quotation_item_quantity ?> </td>

                                                            <td id="totalAmount<?php echo $row+$product->quotation_product_id ?>" style="text-align:center"   class="ttamm thth"><?php echo $product->quotation_item_price * $product->quotation_item_quantity ?></td>    




                                                            <td style="text-align:center;cursor:pointer;" ><img src="<?php echo base_url() ?>images/internal/delete_small.png" width="7" height="8" border="0"   onclick="confirmRemove2('<?php echo $row+$product->quotation_product_id ?>','<?php echo $product->quotation_product_id?>')"/></td>
                                                            <td align="center" valign="middle" style="text-align:center"><input class="mainten" name="editmainten" id="editmainten" type="checkbox" value="" onchange="addMaintenance('<?php echo $row+$product->quotation_product_id ?>', this)" /></td>
                                                        </tr>

                                                        <?php $quan+=$product->quotation_item_quantity ?>
                                                        <?php $total+=$product->quotation_item_price*$product->quotation_item_quantity?>
                                                    <?php endforeach; ?>


                                                <?php endif; ?>
                                                <tr  class="grand-total" style="height: 50px;border-bottom: 1px solid #EFEFEF;">



                                                    <td colspan="5" bgcolor="#FFFFFF" >

                                                    </td> 


                                                    <td><?php echo lang('Total-Price') ?></td>
                                                    <td><strong id="netTotal"><?php echo $total;//echo $invoice[0]->quotation_total_amount ?></strong></td>    
                                                    <td><input type="hidden" name="totalnet_before" id="totalnet_before" value="<?php echo $total;//echo $invoice[0]->quotation_total_amount ?><"/></td>
                                                    <td></td>

                                                </tr>
                                                <tr  class="" style="background: #CCCCCC">




                                                    <td colspan="1" bgcolor="" >

                                                    </td> 
                                                    <td></td>


                                                    <td><div id="totalmin" style="display: none"><?php echo $totlamin?></div></td>
                                                    <td>   
                                                        <div class="col-md-1"> 
                                                            <?php //echo lang('Discount') ?>
                                                        </div>
                                                        <div class="col-md-3"> 
                                                            <input id="totalDiscount" name="totalDiscount" onkeyup="calculateNetTotal()" type="text" onchange=""  class="formtxtfield_small form-control" style="width:100px;"/>
                                                            <br clear="all"/>

                                                        </div>
                                                        <div class="col-md-4">
                                                            &nbsp; %
                                                            <input type="radio" onclick="calculateNetTotal()" name="type_ds" id="type_ds" value="1"  class="currentProduct">
                                                            RO
                                                            <input type="radio" onclick="calculateNetTotal()" name="type_ds" id="type_ds" value="2" class="currentProduct">
                                                        </div>
                                                    </td>







                                                    <td> </td>

                                                    <td style="text-align: center"><?php echo lang('afterDiscount') ?></td>
                                                    <td style="text-align: center">
                                                        <strong id="fnetTotal"></strong>
                                                        <input type="hidden" name="totalnet" id="totalnet" value="<?php echo $total;//echo $invoice[0]->quotation_total_amount ?><"/>
                                                    </td>    
                                                    <td><input type="hidden" name="totalnet_before" id="totalnet_before" value="<?php echo $total;//echo $invoice[0]->quotation_total_amount ?>"/></td>
                                                    <td></td>


                                                </tr>






                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="" id="product_maintenance"></div>


                                    <div class="g8 form-group">
                                        <label class="text-warning"><?php echo lang('Sales-Responsable') ?> :</label>
                                        <br>
                                        <div  class="ui-select">
                                            <div class="" >
                                                <select name="sale[sales_list][]" id="sales_list" onchange="showCode(this.value)">
                                                    <?php
                                                    if (!empty($sales)) {
                                                        $html = '';
                                                        foreach ($sales as $saler) {
                                                            $html .= '<input type="hidden"  id="salesCode' . $saler->bs_sales_id . '" name="salesCode' . $saler->bs_sales_id . '"  value="' . $saler->userid . '"/>';
                                                            $html .= '<input type="hidden"  id="salesName' . $saler->bs_sales_id . '" name="salesName' . $saler->bs_sales_id . '"  value="' . $saler->fullname . '"/>';
                                                            $html .= '<input type="hidden"  id="salescomission' . $saler->bs_sales_id . '" name="salescomission' . $saler->bs_sales_id . '"  value="' . $saler->bs_sales_commession . '"/>';
                                                            $html .= '<input type="hidden"  id="salescomissionType' . $saler->bs_sales_id . '" name="salescomissionType' . $saler->bs_sales_id . '"  value="' . $saler->bs_sales_commession_type . '"/>';
                                                            echo '<option value="' . $saler->bs_sales_id . '">' . $saler->fullname . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <span class="arrow arrowselectbox">&amp;</span>


                                            </div>
                                        </div>
                                        <!--<a id="osx2" href='#' class='osx2'><img src="<?php echo base_url(); ?>images/internal/add_customer.png" width="20" height="20" border="0" /></a>-->
                                        <?PHP echo $html; ?>
                                    </div>
                                    <div class="g8 form-group">
                                        <label class="text-warning"><?php echo lang('Code') ?> : (<a href="javascript:void(0)" onclick="addSales()"><?php echo lang('Add-Sales') ?></a>)</label>
                                        <br>
                                        <input name="sale[sale_code][]" id="sale_code" type="text"  class="form-control"/>

                                    </div>
                                    <div class=" form-group" id="sales_persons"></div>
                                    <div id="fff">


                                        <?php $gspersons = get_sales_items($quotation, 'bs_sales_quotation_persons'); ?>

                                        <?php if ($gspersons): ?>

                                            <?php foreach ($gspersons as $gsperson): ?>
                                                <div class="raw" id="purchase_div<?php echo $gsperson->bs_sales_id ?>">
                                                    <div class="form_title"><?php echo lang('Sales-Name') ?>: </div>
                                                    <div class="form_field form-group" id="total_purchase_price"><?php echo $gsperson->fullname ?></div>
                                                    <div class="form_title"><?php echo lang('Sales-Amount') ?>: </div>
                                                    <div class="form_field form-group" id="total_purchase_price"><?php echo $gsperson->amount ?> RO</div></div>
                                                <input type='hidden' class='salerId' id='salerId<?php echo $gsperson->bs_sales_id ?>'  name='salers[salerId][]' value='<?php echo $gsperson->bs_sales_id ?>'>


                                            <?php endforeach; ?>

                                        <?php endif; ?>

                                    </div>

                                    <br clear="all">
                                    <!--<a href="javascript:void" class="go_to_nexttabs2 reset_btn btn-glow primary" style="margin-top: 10px;">Next &gt;&gt;</a>-->
                                    <br clear="all">
                                </div>
                                <div class="nav-act btn-m">
                                    <!--<button class="">Back</button>-->
                                    <a href="javascript:void" style="" class="gotobutn mini prev tag" onclick="gotonext('Customer','tproduct')"><< <?php echo lang('back')?>  </a>
                                    <a href="javascript:void" style="" class="gotobutn green next  tag" onclick="gotonext('tbill','tproduct')"><?php echo lang('next')?>  >>  </a>
                                </div>

                                <!--aria-controls="tab_item-1"--> 
                                <!--aria-labelledby="tab_item-1"--> 
                            </div>
                            <!--Third tabs-->




                            <!--Four tabs-->
                            <!--                            <div id="tbill" class="nav-item pad-m">
                                                            <div class="">
                                                                <div class="g5" id="" style="">
                                                                    <div class="col-lg-4">
                            <?php echo lang('option_2') ?>
                                                                    </div>
                                                                    <div class=" col-lg-1">
                                                                        <input name="print_option_pt"  type="checkbox"  value='1'  class=" form-control" style="width: 20px;height: 20px;"/>
                                                                    </div>
                                                                </div>
                                                                <div class="g5" id="" style="">
                                                                    <div class="col-lg-4">
                            <?php echo lang('option_3') ?>
                                                                    </div>
                                                                    <div class=" col-lg-1">
                                                                        <input name="print_option_desc"  type="checkbox"  value='1'  class=" form-control" style="width: 20px;height: 20px;"/>
                                                                    </div>
                                                                </div>
                                                                <div class="g5" id="" style="">
                                                                    <div class="col-lg-4">
                            <?php echo lang('option_4') ?>
                                                                    </div>
                                                                    <div class=" col-lg-1">
                                                                        <input name="print_option_note"  type="checkbox"  value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                                                    </div>
                                                                </div>
                                                                <div class="g5" id="" style="">
                                                                    <div class="col-lg-4">
                            <?php echo lang('option_5') ?>
                                                                    </div>
                                                                    <div class=" col-lg-1">
                                                                        <input name="print_option_netprice"  type="checkbox"  value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                                                    </div>
                                                                </div>
                                                                <div class="g5" id="" style="">
                                                                    <div class="col-lg-4">
                            <?php echo lang('option_6') ?>
                                                                    </div>
                                                                    <div class=" col-lg-1">
                                                                        <input name="print_option_dicount"  type="checkbox"   value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                                                    </div>
                                                                </div>
                                                                <div class="g5" id="" style="">
                                                                    <div class="col-lg-4">
                            <?php echo lang('option_7') ?>
                                                                    </div>
                                                                    <div class=" col-lg-1">
                                                                        <input name="print_option_total"  type="checkbox" value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                                                    </div>
                                                                </div>
                                                                <div class="g5" id="" style="">
                                                                    <div class="col-lg-4">
                            <?php echo lang('option_8') ?>
                                                                    </div>
                                                                    <div class=" col-lg-1">
                                                                        <input name="print_option_pay"  type="checkbox" value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                                                    </div>
                                                                </div>
                                                                <div class="g5" id="" style="">
                                                                    <div class="col-lg-4">
                            <?php echo lang('option_9') ?>
                                                                    </div>
                                                                    <div class=" col-lg-1">
                                                                        <input name="print_option_total_pay"  type="checkbox" value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                                                    </div>
                                                                </div>
                            
                            <?php $get_setting_option = unserialize(get_setting_option()->options_print); ?>
                            <?php if ($get_setting_option): ?>
                                <?php $count = 0 ?>
                                <?php foreach ($get_setting_option['name'] as $get_setting_option_row): ?>
                                                                                <div class="g5" id="" style="">
                                                                                    <div class="col-lg-4">
                                    <?php echo $get_setting_option['name'][$count] ?>
                                                                                    </div>
                                                                                    <div class=" col-lg-1">
                                                                                        <input name="options_print_pages[name][]"  type="checkbox" value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                                                                        <input name="options_print_pages[text][]"  type="hidden" value='<?php echo $get_setting_option['text'][$count] ?>'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                                                                    </div>
                                                                                </div>
                                <?php endforeach; ?>
                                
                            <?php endif; ?>
                            
                            
                                                            </div>
                                                            <br clear="all"/>
                                                            <div class="g2" align="left"> 
                                                                <input name="" type="submit" class="submit_btn btn-glow primary " value="Add"  onclick=""/>
                                                                <input name="" type="button" class="submit wide flt-r mod-tg" value="اعتماد العرض"  onclick="addInvoice()"/>
                                                                <input name="" type="reset" class="reset_btn btn-glow primary" value="Reset" />
                                                            </div>
                                                        </div>-->


                            <div id="tbill" class="nav-item pad-m">
                                <div class="">
                                    <div class="g4" id="" style="">
                                        <label>
                                            Contracts <?php //echo lang('option_1')?>
                                        </label><br clear="all">
                                        <div class=" col-lg-1">
                                            <input name="contracts"  type="checkbox"  value='1'  class=" form-control"/>
                                        </div>
                                    </div>
                                    <div class="g4" id="" style="">
                                        <label>
                                            Payment <?php //echo lang('option_1') ?>
                                        </label>
                                        <div class=" col-lg-1">
                                            <input name="Payment"  type="text"  value='<?php echo $invoice[0]->Payment ?>'  class=" form-control"/>
                                        </div>
                                    </div>
                                    <div class="g4" id="" style="">
                                        <label>
                                            Design  <?php //echo lang('option_1') ?>
                                        </label>
                                        <div class=" col-lg-1">
                                            <input name="Design"  type="text"  value='<?php echo $invoice[0]->Design ?>'  class=" form-control"/>
                                        </div>
                                    </div>
                                    <div class="g4" id="" style="">
                                        <label>
                                            Completion  <?php //echo lang('option_1') ?>
                                        </label>
                                        <div class=" col-lg-1">
                                            <input name="Completion"  type="text"  value='<?php echo $invoice[0]->Completion ?>'  class=" form-control"/>
                                        </div>
                                    </div>
                                    <div class="g4" id="" style="">
                                        <label>
                                            Validity  <?php //echo lang('option_1') ?>
                                        </label>
                                        <div class=" col-lg-1">
                                            <input name="Validity"  type="text"  value='<?php echo $invoice[0]->Validity ?>'  class=" form-control"/>
                                        </div>
                                    </div>
                                   
                                    <div class="g10" id="" style="">
                                        <label>
                                            Note  <?php //echo lang('option_1') ?>
                                        </label><br clear="all"/>
                                        <div class=" col-lg-1">
                                            <textarea class="ckeditor" name="qnotes"> 
                                                <?php echo $invoice[0]->qnotes ?>
                                               
                                            </textarea>
                                            <!--<input name="Note"  type="text"  value=''  class=" form-control"/>-->
                                        </div>
                                    </div>





                                </div>
                                <br clear="all"/>
                                <br clear="all"/>
                                <br clear="all"/>
                                <!--<input name="" type="button" class="g3 submit wide flt-r mod-tg" value="حفظ<?php echo lang('save') ?>"  onclick="addInvoice()"/>-->
                                
                                <div class="nav-act btn-m">
                                    <!--<button class="">Back</button>-->
                                    <a href="javascript:void" style="" class="gotobutn mini prev  tag" onclick="gotonext('tproduct','tbill')"><< <?php echo lang('back')?>  </a>
                                    <button name="" type="submit" class="submit_btn btn-glow primary"   /><?php echo lang('save')?></button>
                                </div>
                            </div>


                        </div>
                    </div>

                    <!--end raw---> 

                    <!--end raw---> 
                    <!--end raw---> 

                    <!--                    <br clear="all"/>
                                        <div class="raw" align="left"> 
                                          <input name="" type="submit" class="submit_btn btn-glow primary " value="Add"  onclick=""/>
                                            <input name="" type="button" class="submit_btn btn-glow primary" value="اضافة الفاتوره"  onclick="addInvoice()"/>
                                            <input name="" type="reset" class="reset_btn btn-glow primary" value="Reset" />
                                        </div>-->
                </form>

                <!------> 

            </div>

            <!-- END PAGE --> 

        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    var allProductsData = new Array();
    var allSalesData = new Array();
    totalAmountexComision = '';
    totalnetAmountexComision = '';
    $(document).keypress(function (e) {
        if (e.which == 13) {
            //   alert('You pressed enter!');
            if ($(".currentProduct").is(":focus")) {
                //  alert('You pressed enter!');
                if ($("#productid").val() != "")
                {
                    addItem();
                }
            }
            else {
                //alert('not');
            }
        }
    });</script> 
<script type="text/javascript">
    netTotals = new Array(); /// for keep rocrds net total for each produc
    Totalsmin = new Array(); /// for keep rocrds net total for each produc
    Totalsquan = new Array(); /// for keep rocrds net total for each produc
    rowCounter = 0; /// counter for table products

    netCouner = 0; // counter for net totals
    tbHtml = ''; // for adding new row in the table 

    editArray = new Array(); // edit any product fromm tab;e 



    total_value = 0; // total cacluate value



    productData = {}; // array for current working product



    counter = 0; // counter for products in the hiddden

</script> 
<script type="text/javascript">
    $(function () {
        /*$(".from_date").datepicker({
         defaultDate: "today",
         changeMonth: true,
         numberOfMonths: 1,
         });
         $(".payment_date").datepicker({
         defaultDate: "today",
         changeMonth: true,
         numberOfMonths: 1,
         });
         $(".dpayment_date input").datepicker({
         defaultDate: "today",
         changeMonth: true,
         numberOfMonths: 1,
         });
         */
        $("#is_payment").change(function () {


            ckPayment = $(this).is(":checked");
            //alert(ckPayment);
            if (ckPayment) {
                if (netTotals.length > 0) {
                    $(".payment_div").show();
                }
            }
            else {
                $(".payment_div").hide();
            }
            //alert(ckPayment);

        });
    });</script> 



<script type="text/javascript">
    $(function () {
        $('#expense_date').datepicker({dateFormat: 'yy-mm-dd'});
        $(".expense_period").click(function () {
            //alert('asdasd');

            //checked_status = $(this).is(':checked');
            expense_period_val = $(this).val();

            //alert(checked_status);
            if (expense_period_val == 'fixed') {

                $("#period_parent").show();
            }
            else {
                $("#period_parent").hide();
            }
        });
    });
    $(document).ready(function () {
        //alert('ready');
        var ac_config = {
            source: "<?php echo base_url(); ?>outcome/getAutoSearch",
            select: function (event, ui) {
                $("#expense_title").val(ui.item.cus);
                $("#expense_id").val(ui.item.cus);
                console.log(ui);
                //swapme();
                setTimeout('swapgeneral()', 500);
            },
            minLength: 1
        };

        $("#expense_title").autocomplete(ac_config);


    });



</script>


<!--<script type="text/javascript" src="<?php echo base_url(); ?>js/function.js" ></script>--> 
<!-- End Section--> 
<!--footer--> 





<!----->





