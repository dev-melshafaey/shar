<br/>
<div id="pad-wrapper">

    <div class="table-wrapper users-table ">
        <form action="<?php //echo form_action_url('delete_customers');    ?>" id="listing" method="post" autocomplete="off">
            <!--            <div class="row head">
                            <div class="">
                                <h4>
            
                                    <div class="title"> <span><?php breadcramb(); ?></span> >> <?php echo lang('vall') ?> </div>
            
            
                                </h4>
                                
                            </div>
                        </div>-->



            <div class="">
                <div class="">
                    <div class="table-wrapper users-table ">

                        <?php error_hander($this->input->get('e')); ?>

                        <div class="">
                            <div class="">
                                <?php //action_buttons('company_management/add', $cnt); ?>    
                                <table id="new_data_table">
                                    <thead CLASS="thead">
                                        <tr>
                                            <th tabindex="0" rowspan="1" colspan="1">
                                            </th>
                                            <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('Company-Name') ?>
                                            </th>
                                            <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('Commercial-record') ?>
                                            </th>
                                            <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('Partners') ?>
                                            </th>
                                            <th tabindex="0" rowspan="1" colspan="1">
                                            </th>
                                        </tr>
                                    </thead>


                                    <tbody>

                                        <?php
                                        if (!empty($all_companies)) {
                                            foreach ($all_companies as $company) {
                                                $com = json_decode($company->company_name, TRUE);
                                                $en = $com['en'];
                                                $ar = $com['ar'];
                                                ?>
                                                <tr>
                                                    <td> </td>
                                                    <td> <div class="img">
                                                            <img src="<?php echo base_url(); ?>server/php/files/thumbnail/<?php echo $company->logo_name; ?>" alt="pic" />
                                                        </div>
                                                        <a href="#" class="name"><?php echo $en; ?>   |   <?php echo $ar; ?></a></td>
                                                    <td><?php echo $company->company_taderecord; ?></td>
                                                    <td>(<?php echo $company->partners; ?>)</td>
                                                    <td class="center"> <a href="<?php echo base_url(); ?>company_management/add/<?php echo $company->companyid; ?>" class="btn btn-warning btn-sm"><i class="icon-edit"></i> </a>
                                                        <a class="btn btn-danger btn-sm" onclick="deleteC('<?php echo $company->companyid; ?>', this)"><i class="icon-remove"></i> </a></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>  
                </div>           
            </div>           
    </div>           
</div>           

<script type="application/javascript">

    function deleteC(fielId,obj){

    BootstrapDialog.show({
    message: 'Are You Sure You want to delete!',
    buttons: [{
    label: 'Yes',
    cssClass: 'btn-primary',
    action: function(dialogItself){

    BootstrapDialog.show({
    message: 'Please Wait....'
    });

    deleteCompany(fielId,obj);
    dialogItself.close();
    }
    }, {
    label: 'No',
    action: function(dialogItself){
    dialogItself.close();
    }
    }]
    });

    }   
</script>