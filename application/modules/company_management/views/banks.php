<?php $this->load->view('common/meta');?>
<!--body with bg-->

	
<div class="body">
<header>
  <?php $this->load->view('common/header');?>
  
    <link href="<?php echo base_url(); ?>css/lib/jquery.dataTables.css" type="text/css" rel="stylesheet" />
    
    <!-- this page specific styles -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/compiled/datatables.css" type="text/css" media="screen" />
</header>
<?php $this->load->view('common/left-navigations'); ?>
    <div class="content">
        
        

        
        
        <!-- settings changer -->
        <div class="skins-nav">
            <a href="#" class="skin first_nav selected">
                <span class="icon"></span><span class="text">Default</span>
            </a>
            <a href="#" class="skin second_nav" data-file="css/compiled/skins/dark.css">
                <span class="icon"></span><span class="text">Dark skin</span>
            </a>
         </div>   
        <div id="pad-wrapper">
             
		<div class="table-wrapper users-table section">
                    <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">
                <div class="row head">
                    <div class="col-md-12">
                        <h4>
                            
                            <div class="title"> <span><?php breadcramb(); ?></span> </div>
  
                            
                        </h4>
                        <?php error_hander($this->input->get('e')); ?>
                    </div>
                </div>



                            <div class="row">
                <div class="col-md-12">

                    <table id="example">
                        <thead>
	                    <tr>
	                        <!-- <td width="1%"><label for="checkbox"></label></td> -->
	                        <td width="11%">Company Name</td>
	                        <td width="11%">Bank Name</td>
	                        <td width="12%" >Account Number</td>
	                        <td width="8%" >Payment Type</td>
	                        <td width="8%" >Debit</td>
	                        <td width="8%" >Credit</td>
	                        <td width="8%" >Total</td>
	                        
	                    </tr>
                        </thead>        
	                    <?php if(!empty($all_transactions)):?>
		                    <?php foreach ($all_transactions as $transaction):?>
		                   	<?php 
		                   	    $com = json_decode($transaction->company_name,TRUE);
								$en = $com['en'];
								$ar = $com['ar'];
								
								$total	=	($transaction->t_credit - $transaction->t_debit);
								
								$grand_total	+=	$total;
		                   	?>
			                    <tr>
			                      <td width="11%" ><a href="<?php echo base_url();?>company_management/transactions/<?php echo $transaction->companyid;?>"><?php echo $en;?></a></td>
			                      <td width="11%" ><a href="#_"><?php echo $transaction->bankname;?></a></td>
			                      <td><a href="<?php echo base_url();?>company_management/transactions/<?php echo $transaction->companyid;?>"><?php echo $transaction->account_number;?></a></td>
			                      <td><?php echo $transaction->payment_type;?></td>
			                      <td width="11%" ><?php echo $transaction->t_debit;?></td>
			                      <td width="11%" ><?php echo $transaction->t_credit;?></td>
			                     <!--  <td><span  class="blue"><?php echo date('d M Y',strtotime($transaction->transaction_date));;?></span></td> -->
			                     <td width="11%" ><?php echo (isset($total) ? $total.'.00' : '00.00');?></td>
			                    </tr>
		                    <?php endforeach;?>
	                    <?php endif;?>
                                            
	                    <tr>
	                    	<td width="11%"><strong>Total Value</strong></td>
	                    	<td width="11%"></td>
	                    	<td width="11%"></td>
	                    	<td width="11%"></td>
	                    	<td width="11%"></td>
	                    	<td width="11%"></td>
	                      	<td width="11%"><?php echo (isset($grand_total) ? $grand_total.'.00' : '00.00');?></td>
	                    </tr>
	                   
	                </table>
	            </div>
	</div>
	
	<!--end of raw-->
	</div>
<?php action_buttons('banks/add',$total_record); ?>

</form>
<!-- END PAGE -->  
</div>
      <!-- END PAGE -->  
</div>
</div>

<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
