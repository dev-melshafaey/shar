<?php $this->load->view('common/meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
      <div class="title">
       <span class="subtitle">Add new branch</span>
       </div>

	<form action="<?php echo current_url();?>" method="post" id="frm_bank_branch" name="frm_bank_branch" enctype="multipart/form-data">
		<div class="form" id="form-refresh">
		<div class="raw">
		<div class="form_title">Branch Name</div>
		<div class="form_field">
		<input name="branchname" id="branchname" type="text"  class="formtxtfield" value="<?php echo (isset($single_branch_detail) ? $single_branch_detail->branchname : NULL);?>"/>
		</div>
		</div>
		 <div class="raw form-group">
            <div class="form_title">Country</div>
            <div class="form_field">
              <div class="dropmenu">
                <div class="styled-select">
                  <?php country_dropbox('countryid',$user->countryid); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="raw form-group">
            <div class="form_title">City</div>
            <div class="form_field">
              <div class="dropmenu">
                <div class="styled-select">
                  <?php city_dropbox('cityid',$user->cityid); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="raw">
			<div class="form_title">Branch Code</div>
			<div class="form_field">
			 <input name="branch_code" id="branch_code" type="text"  readonly class="formtxtfield" value="<?php echo (isset($single_branch_detail) ? $single_branch_detail->branch_code : NULL);?>"/>
			 
			</div>
		  </div>
		  <div class="raw">
			<div class="form_title">Branch Address</div>
			<div class="form_field">
			 <textarea name="branchaddress" id="branchaddress" cols="" rows="" class="formareafield"><?php echo (isset($single_branch_detail) ? $single_branch_detail->branchaddress : NULL);?></textarea>
			</div>
		  </div>

		<div class="raw">
			<div class="form_title">Contact Person </div>
			<div class="form_field">
				<input name="contactperson" id="contactperson" type="text"  class="formtxtfield" value="<?php echo (isset($single_branch_detail) ? $single_branch_detail->contactperson : NULL);?>"/>
			</div>
		</div>
		
		<div class="raw">
			<div class="form_title">Phone Number</div>
			<div class="form_field">
				<input name="phone_number" id="phone_number" type="text"  class="formtxtfield" value="<?php echo (isset($single_branch_detail) ? $single_branch_detail->phone_number : NULL);?>"/>
			</div>
		</div>
		<div class="raw">
			<div class="form_title">Fax Number</div>
			<div class="form_field">
				<input name="fax_number" id="fax_number" type="text"  class="formtxtfield" value="<?php echo (isset($single_branch_detail) ? $single_branch_detail->fax_number : NULL);?>"/>
			</div>
		</div>
		<div class="raw">
			<div class="form_title">Mobile Number</div>
			<div class="form_field">
				<input name="mobile_number" id="mobile_number" type="text"  class="formtxtfield" value="<?php echo (isset($single_branch_detail) ? $single_branch_detail->mobile_number : NULL);?>"/>
			</div>
		</div>

		<div class="raw" align="center">
			<input type="hidden" id="set_branch_code" name="set_branch_code" value="<?php echo $company_name;?>">
			<input name="submit" type="submit" class="submit_btn" value="submit" />
			<input name="branchid" type="hidden"  value="<?php echo (isset($single_branch_detail) ? $single_branch_detail->branchid : NULL);?>" />
		</div>
		<!--end of raw-->
		</div>
	</form>
<!-- END PAGE -->  
</div>
      <!-- END PAGE -->  
   </div>
</section>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
