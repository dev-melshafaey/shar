

<script src="<?php echo base_url() ?>js/uploading/jquery.uploadify.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>js/uploading/uploadify.css">



<script>

    function remove_logo(fid,cid) {

        $.ajax({
            url: config.BASE_URL + "company_management/deletelogo",
            type: 'post',
            data: {fid: fid},
            cache: false,
            //dataType:"json",
            success: function (data)
            {
                //var response = $.parseJSON(data);

                $("#getfiles_logo").load(config.BASE_URL + "company_management/getfiles/" + cid);
                //$('#uploadform').html('<script>$("#file_upload").uploadify({"formData": {"company_id_up": "<?php echo get_last_add_comapny() + 1; ?>","company_id_prev": "<?php echo $company->companyid; ?>","mustafa": "1","timestamp": "<?php echo $timestamp; ?>","token": "<?php echo md5('unique_salt' . $timestamp); ?>"},"swf": "<?php echo base_url() ?>js/uploading/uploadify.swf","uploader": "<?php echo base_url() ?>company_management/uploadify/" + cid,"itemTemplate": "<div id="${fileID}" class="uploadify-queue-item">\<div class="cancel">\<a href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')">X</a>\</div>\<span class="fileName">${fileName} (${fileSize})</span><span class="data"></span>\</div>","onUploadComplete": function (file) {$("#getfiles_logo").load(config.BASE_URL + "company_management/getfiles/" + $("#company_id").val());$("#uploadform").html(" ");},"onUploadError": function (file, errorCode, errorMsg, errorString) {alert("The file " + file.name + " could not be uploaded: " + errorString);}});<\/\script>\n\<form><div id="queue"></div><input id="file_upload" name="file_upload" type="file" multiple="true"></form>');
                //$('#storeid').html(response.store);
                //$('#customerid').html(response.customer);
            }
        });
    }
</script>

<style>
    .btn-danger {
        color: #fff;
        background-color: #d9534f;
        border-color: #d43f3a;
    }
    .btn {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }
    .table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #f9f9f9;
    }
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
</style>

<br/>
<?php error_hander($this->input->get('e')); ?>
<div id="pad-wrapper">
    <div class="">
        <div class=" col-xs-12">
            <input type="hidden" name="company_idu" id="company_idu" value=""/>
            <!--<div id="company_idu2"></div>-->
            <div id="fuelux-wizard" class="wizard ">
                <ul class="wizard-steps">
                    <li data-target="#step1" class="active">
                        <span class="step">1</span>
                        <span class="title"><?php echo lang('General-information') ?></span>
                    </li>
                    <!--                           <li data-target="#step2">
                                                   <span class="step">2</span>
                                                   <span class="title">Partners <br> information</span>
                                               </li>-->
                    <li data-target="#step3">
                        <span class="step">2</span>
                        <span class="title"><?php echo lang('Partners-information') ?></span>
                    </li>
                    <li data-target="#step4">
                        <span class="step">3</span>
                        <span class="title"><?php echo lang('File-Upload') ?></span>
                    </li>
                </ul>                            
            </div>
            <div class="step-content mycontent">
                <?php
                $companyname = json_decode($company->company_name);
                ?>			
                <div class="step-pane active" id="step1">
                    <div class=" form-wrapper">
                        <div class="">
                            <br clear="all"/>
                            <br clear="all"/>
                            <input type="hidden" id="company_id" name="compnay_id" value="<?php echo $company->companyid; ?>" />
                            <form  id="form1" class="form-horizontal frm_company sample-form" >

                                <div class="g4">
                                    <label class=""><?php echo lang('Company-Name') ?> (بالعربية):</label>
                                    <div class="">	
                                        <input class="validate[required] form-control" name="company_name_ar" id="company_name_ar" value="<?php echo $companyname->ar; ?>" type="text"  title="Please Company Name In Arabic" />
                                    </div>
                                </div>
                                <div class="g4">

                                    <label class=""><?php echo lang('Company-Name') ?> (English) :</label>
                                    <div class="">
                                        <input name="company_name_en" id="company_name_en" value="<?php echo $companyname->en; ?>" class="validate[required]  valid form-control" type="text"   title="Please Company Name In English"/>
                                    </div>
                                </div>
                                <!--                                <div class="g4">
                                                                    <label class="text-warning"><?php echo lang('Bank') ?> </label>
                                                                    <div class="">
                                                                        <div class="ui-select" style="width:100%">
                                                                            <div class="">
                                <?php echo get_banks('bankid', $company->bankid, $mesage = ""); ?>
                                                                                 <span class="arrow arrowselectbox">&amp;</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                <!--                                <div class="g4">
                                
                                                                    <label class=""><?php echo lang('Account-Number') ?>:</label>
                                                                    <div class="">
                                                                        <input name="account_number" id="account_number" value="<?php echo $company->account_number; ?>"  class="validate[required]  valid form-control" type="text" title="Please Account Number needed" />
                                                                    </div>
                                                                </div>-->


                                <div class="g4">
                                    <label class="col-lg-3">رقم السجل التجاري</label>
                                    <div class="col-lg-4">
                                        <input name="company_taderecord" id="company_taderecord" value="<?php echo $company->company_taderecord; ?>"  class="validate[required,custom[number]] form-control" title="Please company_taderecord needed" type="text" />
                                    </div>
                                </div>
                                <div class="g2">

                                    <label class="col-lg-3">الصندوق البريدي</label>
                                    <div class="col-lg-4">
                                        <input name="company_pobox" id="company_pobox" type="text" value="<?php echo $company->company_pobox; ?>"  class="validate[required,custom[number]] form-control" title="Please company_pobox needed" />
                                    </div>
                                </div>
                                <div class="g2">

                                    <label class="col-lg-3">الرمز البريدي</label>
                                    <div class="col-lg-4">
                                        <input name="company_pcode" id="company_pcode" type="text" value="<?php echo $company->company_pcode; ?>"  class="validate[required,custom[number]] form-control" title="Please company_pcode needed" />
                                    </div>
                                </div>



                                <br clear="all"/>
                                <div class="g4">

                                    <label class=""><?php echo lang('phone-Number') ?>:</label>
                                    <div class="">
                                        <input name="phone_number" id="phone_number" value="<?php echo $company->phone_number; ?>"  class="validate[required]  valid form-control" type="text" title="Please Phone needed"/>
                                    </div>
                                </div>
                                <div class="g4">
                                    <label class=""><?php echo lang('Mobile-Number') ?>:</label>
                                    <div class="">
                                        <input name="company_mobile" id="company_mobile" value="<?php echo $company->company_mobile; ?>"  class="validate[required]  valid form-control" type="text" />
                                    </div>
                                </div>
                                <div class="g4">
                                    <label class=""><?php echo lang('Fax-Number') ?>:</label>
                                    <div class="">

                                        <input name="company_fax" id="company_fax" value="<?php echo $company->company_fax; ?>"  class="validate[required]  valid form-control" type="text" />
                                    </div>
                                </div>
                                <div class="g4">

                                    <label class=""><?php echo lang('Email-Address') ?>:</label>
                                    <div class="">
                                        <input name="company_email" id="company_email" value="<?php echo $company->company_email; ?>"  class="validate[required,custom[email]] form-control" type="text" />
                                    </div>
                                </div>
                                <!--                                        <div class="g4">
                                                                            <label class="col-lg-3"><?php echo lang('Email-Address') ?>Website:</label>
                                                                            <div class="col-lg-4">
                                                                            <input name="company_website" id="company_website" value="<?php echo $company->company_website; ?>" class="validate[custom[url]] form-control" type="text" />
                                                                                </div>
                                                                       </div>--> 



                                <br clear="all"/>
                                <br clear="all"/>
                                <div class="g7">
                                    <label class="" ><?php echo lang('Address') ?>:</label>
                                    <div class="">
                                        <textarea name="company_address" id="company_address" s="4" class="form-control"><?php echo $company->about_company; ?></textarea>
                                    </div>
                                </div>
                                <div class="g7">
                                    <label class=""><?php echo lang('About-Company') ?>:</label>
                                    <div class="">
                                        <textarea name="about_company" id="about_company" s="4" class="form-control"><?php echo $company->about_company; ?></textarea>
                                    </div>
                                </div>

                                <br clear="all"/>

                                <div class="top-bar-new g16">
                                    <div class="item notifs ig5" style="float: right;width: 100%;">
                                        <div class="notifff">
                                            <span class="green icon icon-ok-sign" style="right:-1px !important;"></span>
                                            <p style="float: right;margin-right: 45px;"><strong>ملاحظة</strong>جميع البيانات الموجودة ستظهر في بيانات الفاتوره او في التقارير وغيرها من الطباعة من خلال البرنامج</p>
                                        </div>


                                    </div>
                                </div>
                                <!--                                        <div class="g4">
                                                                            <label class="col-lg-3">Facebook:</label>
                                                                            <div class="col-lg-4">
                                                                            <input name="company_facebook" id="company_facebook" type="text" value="<?php echo $company->company_facebook; ?>" class="validate[custom[url]] text-input form-control " />
                                                                                </div>
                                                                        </div>
                                                                        <div class="g4">
                                                                                 <label class="col-lg-3">Twitter:</label>
                                                                            <div class="col-lg-4">
                                                                           <input name="company_twitter" id="company_twitter" type="text" value="<?php echo $company->company_twitter; ?>"   class="validate[custom[url]] form-control" />
                                                                                </div>
                                                                        </div>
                                                                        <div class="g4">
                                                                                <label class="col-lg-3">Youtube:</label>
                                                                            <div class="col-lg-4">
                                                                            
                                                                            <input name="company_fax" id="company_fax" value="<?php echo $company->company_fax; ?>"  class="form-control validate[custom[url]]" type="text" />
                                                                                </div>
                                                                        </div>-->





                            </form>
                        </div>
                        <br clear="all"/>
                    </div>

                    <br clear="all"/><br clear="all"/>
                </div>
                <div class="step-pane" id="step2">
                    <br clear="all"    />
                    <div class=" form-wrapper">
                        <br clear="all"    />
                    </div>
                </div>
                <div class="step-pane" id="step3">
                    <div class=" form-wrapper">
                        <div class="col-md-10">
                            <form id="form2">

                                <?php
                                $parner_status = "";
                                $parnter_label = "No";

                                //echo "<pre>";
                                //print_r($companyPartners);

                                if (!empty($companyPartners)) {


                                    $partnersCount = count($companyPartners);
                                    if ($partnersCount > 0) {
                                        $parner_status = 'on';
                                        $parnter_label = "Yes";
                                    }
                                }
                                ?>
                                <br clear="all"/>
                                <br clear="all"/>
                                <div class="">
                                    <label style="width: 20%;"><?php echo lang('have_Partners') ?>:</label>
                                    <div id="partner_status"  class="slider-frame primary <?php echo $parner_status; ?>">
                                        <span data-on-text="Yes" data-off-text="No" class="slider-button"><?php echo $parnter_label; ?></span>
                                    </div>
                                </div>
                                <br clear="all"    />
                                <div  class="partner_div" style=" <?php
                                if ($parner_status = 'on') {
                                    echo "display:block";
                                } else {
                                    echo "display:none";
                                }
                                ?>">
                                    <br clear="all"    />    

                                    <?php
                                    if (!empty($companyPartners)) {
                                        $counter = 1;
                                        foreach ($companyPartners as $c => $cp) {
                                            //echo "<pre>";
                                            //print_r($cp);
                                            ?>
                                            <h3 class="partner_title"><?php echo lang('Parnter') ?> <?php echo $counter; ?></h3> <br clear="all"/>
                                            <div class="g4">
                                                <input type="hidden"  name="partner_id[]" id="partner_id" value="<?php echo $cp->partner_id; ?>"/>
                                                <label class=""><?php echo lang('Name') ?> :</label>
                                                <div class="">
                                                    <input class="validate[required] form-control" type="text" name="partnername[]" value="<?php echo $cp->partnername; ?>" placeholder="Name" />

                                                </div>
                                            </div>
                                            <div class="g4">        
                                                <label class=""><?php echo lang('phone-Number') ?>:</label>
                                                <div class="">
                                                    <input class="validate[required,custom[integer]] form-control" type="text" name="partnerphone[]" value="<?php echo $cp->parnterphone; ?>" placeholder="Phone" />

                                                </div>  
                                            </div>
                                            <div class="g4"> 
                                                <label class=""><?php echo lang('Share') ?> %:</label>
                                                <div class="">
                                                    <input class="validate[required,custom[integer]] form-control" type="text" name="Partnershare[]" value="<?php echo $cp->Partnershare; ?>" placeholder="Partner share" />

                                                </div>
                                            </div>

                                            <div class="g4">     
                                                <label class=""><?php echo lang('Email-Address') ?>:</label>
                                                <div class="">
                                                    <input class="validate[required,custom[email]] form-control" type="text" name="partneremail[]" value="<?php echo $cp->partneremail; ?>" placeholder="Parnter Email" />

                                                </div>
                                            </div>

                                            <div class="g4">
                                                <label class=""><?php echo lang('Total_Company_Profit') ?>:</label>
                                                <div class="">
                                                    <input class="form-control" type="text" name="companyProfit[]" placeholder="Textbox #1" />

                                                </div>
                                            </div>

                                            <div class="g4">
                                                <label class=""><?php echo lang('Today-Share') ?>:</label>
                                                <div class="">
                                                    <input class="form-control" type="text" name="todayShare[]" placeholder="Today's Share #1" />

                                                </div>  
                                            </div>
                                            <?php
                                            $counter++;

                                            if ($c > 0) {
                                                ?>
                                                <br clear="all" />
                                                <div class="col-lg-12 " style="padding-top:20px;">
                                                    <button type="button" class="btn btn-default btn-sm btn-danger  removeButton" onclick="removePartner(<?php echo $cp->partner_id; ?>)"><i class="icon-remove"></i> Remove</button>

                                                </div>
                                                <?php
                                            }
                                        }
                                    } else {
                                        ?>
                                        <br clear="all"       />
                                        <h3 class="partner_title"><?php echo lang('Parnter_n') ?> 1</h3>
                                        <br clear="all"       />
                                        <br clear="all"       />
                                        <div class="g4">

                                            <label class=""><?php echo lang('Name') ?> :</label>
                                            <div class="">
                                                <input class="validate[required] form-control" type="text" name="partnername[]" placeholder="<?php echo lang('Name') ?>" />

                                            </div>
                                        </div>

                                        <div class="g4">    

                                            <label class=""><?php echo lang('phone-Number') ?>:</label>
                                            <div class="">
                                                <input class="validate[required,custom[integer]] form-control" type="text" name="partnerphone[]" placeholder="<?php echo lang('phone-Number') ?>" />

                                            </div>  
                                        </div>
                                        <div class="g4"> 
                                            <label class=""><?php echo lang('Share') ?> %:</label>
                                            <div class="">
                                                <input class="validate[required,custom[integer]] form-control" type="text" name="Partnershare[]" placeholder="<?php echo lang('Share') ?>" />

                                            </div>
                                        </div>

                                        <div class="g4">     
                                            <label class=""><?php echo lang('Email-Address') ?>:</label>
                                            <div class="">
                                                <input class="validate[required,custom[email]] form-control" type="text" name="partneremail[]" placeholder="<?php echo lang('Email-Address') ?>" />

                                            </div>
                                        </div>
                                        <div class="g4">
                                            <label class="">><?php echo lang('Total_Company_Profit') ?>:</label>
                                            <div class="">
                                                <input class="form-control" type="text" name="companyProfit[]" placeholder="<?php echo lang('Total_Company_Profit') ?>  #1" />

                                            </div>
                                        </div>

                                        <div class="g4">
                                            <label class=""><?php echo lang('Today-Share') ?>:</label>
                                            <div class="">
                                                <input class="form-control" type="text" name="todayShare[]" placeholder="<?php echo lang('Today-Share') ?> #1" />

                                            </div>  
                                        </div>

                                        <?php
                                    }
                                    ?>




                                    <div class="hide" id="textboxTemplate">
                                        <br clear="all"    />
                                        <hr />
                                        <p></p><p></p><p></p>
                                        <h3 class="partner_title"></h3>
                                        <br clear="all"    />
                                        <div class="g4">

                                            <label class=""><?php echo lang('Name') ?> :</label>
                                            <div class="">
                                                <input class="validate[required] form-control" type="text" name="partnername[]" placeholder="<?php echo lang('Name') ?> " />

                                            </div>
                                        </div>
                                        <div class="g4">  
                                            <label class=""><?php echo lang('phone-Number') ?>:</label>
                                            <div class="">
                                                <input class="validate[required,custom[integer]] form-control" type="text" name="partnerphone[]" placeholder="<?php echo lang('phone-Number') ?>" />

                                            </div>  
                                        </div>

                                        <div class="g4"> 
                                            <label class=""><?php echo lang('Share') ?> %:</label>
                                            <div class="">
                                                <input class="validate[required,custom[integer]] form-control" type="text" name="Partnershare[]" placeholder="<?php echo lang('Share') ?> " />

                                            </div>
                                        </div>


                                        <div class="g4"> 
                                            <label class=""><?php echo lang('Email-Address') ?>:</label>
                                            <div class="">
                                                <input class="validate[required,custom[email]] form-control" type="text" name="partneremail[]" placeholder="<?php echo lang('Email-Address') ?>" />


                                            </div>
                                        </div>
                                        <div class="g4">
                                            <label class=""><?php echo lang('Total_Company_Profit') ?>:</label>
                                            <div class="">
                                                <input class="form-control" type="text" name="companyProfit[]" placeholder="<?php echo lang('Total_Company_Profit') ?> #1" />

                                            </div>
                                        </div>

                                        <div class="g4">
                                            <label class=""><?php echo lang('Today-Share') ?>:</label>
                                            <div class="">
                                                <input class="form-control" type="text" name="todayShare[]" placeholder="<?php echo lang('Today-Share') ?> #1" />

                                            </div>  
                                        </div>  

                                        <div class="g4">
                                            <div class=" " style="padding-top:20px;">
                                                <button type="button" class="btn btn-default btn-sm btn-danger addButton  removeButton"><i class="icon-remove"></i> Remove</button>

                                            </div>

                                        </div>


                                    </div>
                                </div>
                                <?php
                                if (!empty($companyPartners)) {
                                    $display_block = "block";
                                } else {
                                    $display_block = "none";
                                }
                                ?>
                                <br clear="all"    />
                                <div class="col-lg-4 partner_div" style=" display:<?php echo $display_block; ?>; padding:20px;">                                                      
                                    <button type="button" class="btn btn-default btn-sm btn-success addButton" data-template="textbox"><i class="icon-plus"></i> Add More</button>
                                </div>
                                <!--<div class="g4">
                                <label>Username:</label>
                                <input class="form-control" type="text" />
                            </div>
                            <div class="g4">
                                <label>Photo:</label>
                                <input type="file" />
                            </div>
                            <div class="g4">
                                <label>App name:</label>
                                <input class="form-control" type="text" />
                            </div>-->


                            </form>
                        </div>
                    </div>
                </div>
                <div class="step-pane" id="step4">
                    <div class="col-md-10">

                        <!--<div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                        <img data-src="holder.js/100%x100%" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                        <div>
                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="..."></span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                        </div>-->
                        <br clear="all"/>
                        <!--                        <form  id="c_logo" class="fileupload" action="#" method="POST" enctype="multipart/form-data"   data-upload-template-id="template-upload-2"
                                                       data-download-template-id="template-download-2">
                                                     The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload 
                                                    <div class=" fileupload-buttonbar">
                                                       
                                                        <div class="col-lg-7">
                                                             The fileinput-button span is used to style the file input field as button 
                                                            <span class="btn btn-success fileinput-button">
                                                                <i class="icon-plus"></i>
                                                                <span><?php echo lang('Add-files') ?>...</span>
                                                                <input id="addFiles2" type="file" name="files[]" multiple>
                                                            </span>
                                                            <button type="submit" class="btn btn-primary start">
                                                                <i class="glyphicon glyphicon-upload"></i>
                                                                <span>Start upload</span>
                                                            </button>
                                                            <button type="reset" class="btn btn-warning cancel">
                                                                <i class="glyphicon glyphicon-ban-circle"></i>
                                                                <span>Cancel upload</span>
                                                            </button>
                                                            <button type="button" class="btn btn-danger delete">
                                                                <i class="icon-remove"></i></i>
                                                                <span>Delete</span>
                                                            </button>
                                                             The global file processing state 
                                                            <span class="fileupload-process"></span>
                                                        </div>
                                                         The global progress state 
                                                        <div class="col-lg-5 fileupload-progress fade">
                                                             The global progress bar 
                                                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                                            </div>
                                                             The extended global progress state 
                                                            <div class="progress-extended">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                     The table listing the files available for upload/download 
                                                    <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                                             </form>-->

                        <h3><?php echo lang('Upload-Logo') ?></h3>   

                        <div id="getfiles_logo" style="margin-bottom: 50px;"><!-- ngRepeat: file in queue -->

                            <br clear="all"       />   
                        </div>
                        <br clear="all"       />
                        <div id="uploadform">
                            <form>
                                <div id="queue"></div>
                                <input id="file_upload" name="file_upload" type="file" multiple="true">
                            </form>

                        </div>    


                        <br clear="all"       />
                        <br clear="all"       />
                        <br clear="all"       />
                        <br clear="all"/>
                        <h3><?php echo lang('Upload-Documents') ?></h3>


                        <div id="getfiles_logo2"  style="margin-bottom: 50px;"><!-- ngRepeat: file in queue -->

                            <br clear="all"       />   
                        </div>
                        <br clear="all"/>
                        <form>
                            <div id="queue2"></div>
                            <input id="file_upload2" name="file_upload2" type="file" multiple="true">
                        </form>

                        <!--                        <form id="form3" class="fileupload" action="#" method="POST" enctype="multipart/form-data"   data-upload-template-id="template-upload"
                                                      data-download-template-id="template-download">
                                                     The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload 
                                                    <div class=" fileupload-buttonbar">
                                                        
                                                        <div class="col-lg-7">
                                                             The fileinput-button span is used to style the file input field as button 
                                                            <span class="btn btn-success fileinput-button">
                                                                <i class="icon-plus"></i>
                                                                <span><?php echo lang('Add-files') ?>...</span>
                                                                <input id="addFiles" type="file" name="files[]" multiple>
                                                            </span>
                                                            <button type="submit" class="btn btn-primary start">
                                                                <i class="glyphicon glyphicon-upload"></i>
                                                                <span>Start upload</span>
                                                            </button>
                                                            <button type="reset" class="btn btn-warning cancel">
                                                                <i class="glyphicon glyphicon-ban-circle"></i>
                                                                <span>Cancel upload</span>
                                                            </button>
                                                            <button type="button" class="btn btn-danger delete">
                                                                <i class="icon-remove"></i></i>
                                                                <span><?php echo lang('Delete') ?></span>
                                                            </button>
                                                             The global file processing state 
                                                            <span class="fileupload-process"></span>
                                                        </div>
                                                         The global progress state 
                                                        <div class="col-lg-5 fileupload-progress fade">
                                                             The global progress bar 
                                                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                                            </div>
                                                             The extended global progress state 
                                                            <div class="progress-extended">&nbsp;</div>
                                                        </div>
                                                    </div>-->
                        <!-- The table listing the files available for upload/download -->
                        <table role="presentation" class="table table-striped"><tbody class="files"><?php
                                if (!empty($companydocument)) {
                                    foreach ($companydocument as $document) {

                                        //echo "<pre>";
                                        //print_r($document);
                                        /* echo $tbleData = '<tr class="template-download fade in">
                                          <td>
                                          <span class="preview">

                                          <a data-gallery="" download="' . $document->document_image . '" title="' . $document->document_image . '" href="' . base_url() . 'server/php/files/' . $document->document_image . '"><img src="' . base_url() . 'server/php/files/thumbnail/' . $document->document_image . '"></a>

                                          </span>
                                          </td>
                                          <td>
                                          <div class="g4">
                                          <label class="col-lg-3">Title:</label>
                                          <div class="col-md-7">
                                          <input type="text" class="validate[required] form-control" value="' . $document->document_title . '" id="document_title[]" name="document_title[]">
                                          </div>
                                          </div>
                                          </td>
                                          <td>
                                          <div class="g4">
                                          <label class="col-lg-3">Expire date:</label>
                                          <div class="col-md-7">
                                          <input type="text" onclick="addDatePicker()" class="validate[required] form-control input-datepicker" value="' . $document->document_date . '" id="document_date[]" name="document_date[]">
                                          </div>
                                          </div>
                                          </td>

                                          <td>
                                          <p class="name">

                                          <a data-gallery="" download="' . $document->document_name . '" title="' . $document->document_name . '" href="' . base_url() . '"server/php/files/' . $document->document_name . '">' . $document->document_name . '</a>

                                          <input type="hidden" value="' . $document->document_name . '" id="document_name[]" name="document_logo[]">
                                          <input type="hidden" value="' . $document->document_id . '" id="document_id[]" name="document_id[]">
                                          </p>

                                          </td>
                                          <td>

                                          <button data-type="DELETE" class="btn btn-danger delete" onclick="deleteFile(' . $document->document_id . ')">
                                          <i class="glyphicon icon-remove"></i>
                                          <span>Delete</span>
                                          </button>
                                          <button style="display:none;" data-url="' . base_url() . 'server/php/?file=' . $document->document_image . '" data-type="DELETE" class="btn btn-danger delete">
                                          <i class="glyphicon icon-remove"></i>
                                          <span>Delete</span>
                                          </button>
                                          <input type="checkbox" class="toggle" value="1" name="delete">

                                          </td>
                                          </tr>';
                                         * 
                                         */
                                    }
                                }
                                ?></tbody></table>
                        </form>	
                    </div>
                </div>


                <br clear="all"/><br clear="all"/>
            </div>
            <div class="wizard-actions">
                <button type="button" disabled class="btn-glow primary btn-prev"> 
                    <i class="icon-chevron-right right"></i> <?php echo lang('Prev') ?>
                </button>
                <button type="button" class="btn-glow primary next" data-last="Finish">
                    <span> <?php echo lang('Next') ?> </span><i class="icon-chevron-left"></i>
                </button>
                <button type="button" class="btn-glow primary btn-next" data-last="Finish" style="display:none;">
                    <span> <?php echo lang('Next') ?> </span><i class="icon-chevron-left"></i>
                </button>
                <button type="button" class="btn-glow success btn-finish">
                    Setup your account!
                </button>
            </div>



            <br clear="all"/>
        </div>
    </div>
</div>




<script type="text/javascript">
    comp_id = 0;
    template_script = 'template-upload1';
    base = '<?php echo base_url(); ?>';
</script>
<script id="template-upload-2" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
    <td>
    <span class="preview"></span>
    </td>

    <td>
    <p class="size">Processing...</p>
    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
    </td>
    <td>
    {% if (!i && !o.options.autoUpload) { %}
    <button class="btn btn-primary start" disabled>
    <i class="glyphicon glyphicon-upload"></i>
    <span>Start</span>
    </button>
    {% } %}
    {% if (!i) { %}
    <button class="btn btn-warning cancel">
    <i class="glyphicon glyphicon-ban-circle"></i>
    <span>Cancel</span>
    </button>
    {% } %}
    </td>
    </tr>

    {% } %}
</script>

<script id="template-upload" type="text/x-tmpl">
    {% 

    for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
    <td>
    <span class="preview"></span>
    </td>
    if(FileUploadName == 'fileupload'){
    <td>
    <div class="g4">
    <label class="col-lg-3" >Title:</label>
    <div class="col-md-7">
    <input name="document_title[]" id="document_title[]" type="text" value=""  class="validate[required] form-control" />
    </div>
    </div>
    </td>

    <td>
    <div class="g4">
    <label class="col-lg-3" >Expire date:</label>
    <div class="col-md-7">
    <input name="document_date[]" id="document_date[]" type="text" value=""  class="validate[required] form-control input-datepicker" onclick="addDatePicker()" />
    </div>
    </div>
    </td>
    }
    <td>
    <p class="size">Processing...</p>
    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
    </td>
    <td>
    {% if (!i && !o.options.autoUpload) { %}
    <button class="btn btn-primary start" disabled>
    <i class="glyphicon glyphicon-upload"></i>
    <span>Start</span>
    </button>
    {% } %}
    {% if (!i) { %}
    <button class="btn btn-warning cancel">
    <i class="glyphicon icon-remove"></i>
    <span>Cancel</span>
    </button>
    {% } %}
    </td>
    </tr>
    $('.input-datepicker').datepicker().on('changeDate', function (ev) {
    $(this).datepicker('hide');
    })
    {% } %}
</script>

<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    <td>
    <span class="preview">
    {% if (file.thumbnailUrl) { %}
    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
    {% } %}
    </span>
    </td>
    <td>
    <div class="g4">
    <label class="col-lg-3" >Title:</label>
    <div class="col-md-7">
    <input name="document_title[]" id="document_title[]" type="text" value=""  class="validate[required] form-control" />
    </div>
    </div>
    </td>
    <td>
    <div class="g4">
    <label class="col-lg-3" >Expire date:</label>
    <div class="col-md-7">
    <input name="document_date[]" id="document_date[]" type="text" value=""  class="validate[required] form-control input-datepicker"  onclick="addDatePicker()"/>
    </div>
    </div>
    </td>

    <td>
    <p class="name">
    {% if (file.url) { %}
    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
    {% } else { %}
    <span>{%=file.name%}</span>
    {% } %}
    <input type="hidden" name="document_logo[]" id="document_logo[]" value="{%=file.name%}" />
    </p>
    {% if (file.error) { %}
    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
    {% } %}
    </td>
    <td>
    <span class="size">{%=o.formatFileSize(file.size)%}</span>
    </td>
    <td>
    {% if (file.deleteUrl) { %}
    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
    <i class="glyphicon icon-remove"></i>
    <span>Delete</span>
    </button>
    <input type="checkbox" name="delete" value="1" class="toggle">
    {% } else { %}
    <button class="btn btn-warning cancel">
    <i class="glyphicon icon-remove"></i>
    <span>Cancel</span>
    </button>
    {% } %}
    </td>
    </tr>
    {% } %}
</script>
<script id="template-download-2" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    <td>
    <span class="preview">
    {% if (file.thumbnailUrl) { %}
    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
    {% } %}
    </span>
    </td>
    <td>
    <p class="name">
    {% if (file.url) { %}
    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
    {% } else { %}
    <span>{%=file.name%}</span>
    {% } %}
    <input class="company_logo" type="hidden" name="company_logo[]" id="company_logo[]" value="{%=file.name%}" />

    </p>
    {% if (file.error) { %}
    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
    {% } %}
    </td>
    <td>
    <span class="size">{%=o.formatFileSize(file.size)%}</span>
    </td>
    <td>
    {% if (file.deleteUrl) { %}
    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
    <i class="glyphicon glyphicon-trash"></i>
    <span>Delete</span>
    </button>
    <input type="checkbox" name="delete" value="1" class="toggle">
    {% } else { %}
    <button class="btn btn-warning cancel">
    <i class="glyphicon glyphicon-ban-circle"></i>
    <span>Cancel</span>
    </button>
    {% } %}
    </td>
    </tr>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    <td>
    <span class="preview">
    {% if (file.thumbnailUrl) { %}
    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
    {% } %}
    </span>
    </td>
    <td>
    <div class="g4">
    <label class="col-lg-3" >Title:</label>
    <div class="col-md-7">
    <input name="document_title[]" id="document_title[]" type="text" value=""  class="validate[required] form-control" />
    </div>
    </div>
    </td>
    <td>
    <div class="g4">
    <label class="col-lg-3" >Expire date:</label>
    <div class="col-md-7">
    <input name="document_date[]" id="document_date[]" type="text" value=""  class="validate[required] form-control input-datepicker" onfocus="addDatePicker()"/>
    </div>
    </div>
    </td>

    <td>
    <p class="name">
    {% if (file.url) { %}
    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
    {% } else { %}
    <span>{%=file.name%}</span>
    {% } %}
    <input type="hidden" name="document_logo[]" id="document_logo[]" value="{%=file.name%}" />
    </p>
    {% if (file.error) { %}
    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
    {% } %}
    </td>
    <td>
    <span class="size">{%=o.formatFileSize(file.size)%}</span>
    </td>
    <td>
    {% if (file.deleteUrl) { %}
    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
    <i class="glyphicon glyphicon-trash"></i>
    <span>Delete</span>
    </button>
    <input type="checkbox" name="delete" value="1" class="toggle">
    {% } else { %}
    <button class="btn btn-warning cancel">
    <i class="glyphicon glyphicon-ban-circle"></i>
    <span>Cancel</span>
    </button>
    {% } %}
    </td>
    </tr>
    {% } %}
</script>

<!--footer-->


<script type="text/javascript">

<?php $timestamp = time(); ?>
    $(function () {

        $("#getfiles_logo").load(config.BASE_URL + "company_management/getfiles/<?php echo $company->companyid; ?>");

        var cid = $("#company_idu2").text();
        $('#file_upload').uploadify({
            'formData': {
                'company_id_up': '<?php echo get_last_add_comapny() + 1; ?>',
                'company_id_prev': '<?php echo $company->companyid; ?>',
                'mustafa': '1',
                'timestamp': '<?php echo $timestamp; ?>',
                'token': '<?php echo md5('unique_salt' . $timestamp); ?>'
            },
            'swf': '<?php echo base_url() ?>js/uploading/uploadify.swf',
            'uploader': '<?php echo base_url() ?>company_management/uploadify/' + cid,
            'itemTemplate': '<div id="${fileID}" class="uploadify-queue-item">\
                                <div class="cancel">\
                                    <a href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')">X</a>\
                                </div>\
                                <span class="fileName">${fileName} (${fileSize})</span><span class="data"></span>\
                            </div>',
            'onUploadComplete': function (file) {
                //alert('The file ' + file.name + ' finished processing.');

                $("#getfiles_logo").load(config.BASE_URL + "company_management/getfiles/" + $("#company_id").val());
                //$('#uploadform').html(' ');


            },
            'onUploadError': function (file, errorCode, errorMsg, errorString) {
                alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
            }
        });

        /**/





        $("#getfiles_logo2").load(config.BASE_URL + "company_management/getdocuments/<?php echo $company->companyid; ?>");

        $('#file_upload2').uploadify({
            'formData': {
                'company_id_up': '<?php echo get_last_add_comapny() + 1; ?>',
                'timestamp': '<?php echo $timestamp; ?>',
                'token': '<?php echo md5('unique_salt' . $timestamp); ?>'
            },
            'swf': '<?php echo base_url() ?>js/uploading/uploadify.swf',
            'uploader': '<?php echo base_url() ?>company_management/uploadify2',
            'itemTemplate': '<div id="${fileID}" class="uploadify-queue-item">\
                                <div class="cancel">\
                                    <a href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')">X</a>\
                                </div>\
                                <span class="fileName">${fileName} (${fileSize})</span><span class="data"></span>\
                            </div>',
            'onUploadComplete': function (file) {
                //alert('The file ' + file.name + ' finished processing.');

                $("#getfiles_logo2").load(config.BASE_URL + "company_management/getdocuments/" + $("#company_id").val());

            },
            'onUploadError': function (file, errorCode, errorMsg, errorString) {
                alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
            }
        });


    });
    function onUploadProgress() {

        alert('sdsd');

    }
</script>