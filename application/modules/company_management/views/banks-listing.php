<?php $this->load->view('common/meta');?>
<!--body with bg-->

	
<div class="body">
<header>
  <?php $this->load->view('common/header');?>
  
    <link href="<?php echo base_url(); ?>css/lib/jquery.dataTables.css" type="text/css" rel="stylesheet" />
    
    <!-- this page specific styles -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/compiled/datatables.css" type="text/css" media="screen" />
</header>
<?php $this->load->view('common/left-navigations'); ?>
    <div class="content">
        
        

        
        
        <!-- settings changer -->
        <div class="skins-nav">
            <a href="#" class="skin first_nav selected">
                <span class="icon"></span><span class="text">Default</span>
            </a>
            <a href="#" class="skin second_nav" data-file="css/compiled/skins/dark.css">
                <span class="icon"></span><span class="text">Dark skin</span>
            </a>
         </div>   
        <div id="pad-wrapper">
             
		<div class="table-wrapper users-table section">
                    <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">
                <div class="row head">
                    <div class="col-md-12">
                        <h4>
                            
                            <div class="title"> <span><?php breadcramb(); ?></span> </div>
  
                            
                        </h4>
                        <?php error_hander($this->input->get('e')); ?>
                    </div>
                </div>



                            <div class="row">
                <div class="col-md-12">

                    <table id="example">
                <thead>
                  <tr>
                    <th width="1%" id="no_filter"><label for="checkbox"></label></th>
                    <th width="11%">Company Name</th>
                    <th width="11%">Bank Name</th>
                    <td width="12%" >Account Number
                      </th>
                    <th width="8%" >Debit</th>
                    <th width="8%" >Credit</th>
                    <th width="8%" id="no_filter">Total</th>
                  </tr>
                </thead>
               
                <?php if(!empty($all_transactions)):?>
                <?php foreach ($all_transactions as $transaction):?>
                <?php $bank_info	=	$this->company->get_bank_info($transaction->bankid);?>
                <tr>
                  <td><input type="checkbox" name="ids[]" id="checkbox2" value="<?php echo $transaction->transactionid;?>"/></td>
                  <td width="11%" ><a href="#_"><?php echo $bank_info->bankname;?></a></td>
                  <td><?php echo $bank_info->account_number;?></td>
                  <td style="text-align:center; background-color:<?php echo ((isset($transaction->transaction_type) AND $transaction->transaction_type	==	'D') ? '#c4fbbb;' : '#fbf7bb;');?>"><?php echo $transaction->cheque_amount;?></td>
                  <td><span  class="blue"><!-- 10 September 2013 --><?php echo date('d M Y',strtotime($transaction->transaction_date));;?></span></td>
                  <td><?php echo $transaction->cheque_number;?></td>
                  <td><?php echo $transaction->fullname;?></td>
                </tr>
                <?php endforeach;?>
                <?php endif;?>
                <tr>
                  <td colspan="7" ><strong>Total Value</strong></td>
                </tr>
              </table>
            </div>
          </div>
          
          <!--end of raw--> 
        </div>
        <?php action_buttons('banks/add',$total_record); ?>
      </form>
      <!-- END PAGE --> 
    </div>
    <!-- END PAGE --> 
  </div>
  </div>

<!-- End Section--> 
<!--footer-->
<?php $this->load->view('common/footer');?>
