<?php $this->load->view('common/meta');?>
<script>

</script>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<?php $error	=	$this->session->flashdata('error');?>
<?php $success	=	$this->session->flashdata('success');?>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
      <?php if($error):?><div class="alert alert-danger"><?php echo (isset($error) ? $error : '');?></div><?php endif;?>
      <?php if($success):?><div class="alert alert-success"><?php echo (isset($success) ? $success : '');?></div><?php endif;?>
      <div class="title"> Transactions <?php echo (isset($company_name->en) ? '>'.$company_name->en : NULL); ?></div>

<form action="<?php echo base_url();?>company_management/bulk_delete_transaction" method="post" id="listing">
	<div class="form">
	
	<div class="raw">
	<div class="CSSTableGenerator" id="printdiv" >
	                <table width="100%" align="left" id="usertable">
	                 <thead>
	                    <tr>
	                        <th width="1%" id="no_filter"><label for="checkbox"></label></th>
	                        <th width="11%">TRANSACTION NO</th>
	                        <th width="12%" >PAYMENT TYPE</th>
	                        <th width="8%" >TRANSACTION AMOUT</th>
	                        <th width="8%" >Date of Import</th>
	                        <th width="8%" >COSTUMER</th>
	                        <th width="10%" >DEPIT</th>
	                        <th width="10%" >CRIDET</th>
	                        <th width="10%" >TOTAL</th>
	                        <th width="10%" >NOTES</th>
	                    </tr>
	                    </thead>
	                    <tfoot>
	                    <tr>
	                      <td><input type="checkbox" name="checkbox" id="checkboxall" /></td>
	                      <td><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
	                      <td><input type="text" name="textfield2" id="textfield2"  class="flex_feild"/></td>
	                      <td><input type="text" name="textfield9" id="textfield9"  class="flex_feild"/></td>
	                      <td><input type="text" name="textfield3" id="textfield3"  class="flex_feild"/></td>
	                      <td><input type="text" name="textfield7" id="textfield7"  class="flex_feild"/></td>
	                      <td><input type="text" name="textfield8" id="textfield8"  class="flex_feild"/></td>
	                      <td><input type="text" name="textfield8" id="textfield8"  class="flex_feild"/></td>
	                      <td><input type="text" name="textfield8" id="textfield8"  class="flex_feild"/></td>
	                      <td><input type="text" name="textfield8" id="textfield8"  class="flex_feild"/></td>
	                    </tr>
	                    </tfoot>
	                    <?php if(!empty($all_transactions)):?>
		                    <?php foreach ($all_transactions as $transaction):?>
		                    <?php $bank_info	=	$this->company->get_bank_info($transaction->bankid);?>
		                    <?php //echo ((isset($transaction->transaction_type) AND $transaction->transaction_type	==	'D') ? $transaction->cheque_amount : NULL);?>
		                    <?php 
		                    if((isset($transaction->transaction_type) AND $transaction->transaction_type	==	'D'))
		                    {
		                    	$debit_amount	+=	$transaction->cheque_amount;
		                    }
		                    else if((isset($transaction->transaction_type) AND $transaction->transaction_type	==	'C')) 
		                    {
		                    	$credit_amount	+=	$transaction->cheque_amount;
		                    }
		                    
		                    $total_amount	=	($credit_amount - $debit_amount);
		                    $grand_total	+=	$total_amount;
		                    ?>
			                    <tr>
			                      <td><input class="allcb" type="checkbox" name="ids[]" id="checkbox2" value="<?php echo $transaction->transactionid;?>"/></td>
			                        <td width="11%" ><a href="#_"><?php echo $transaction->transactionid;?></a></td>
			                         <td width="11%" ><a href="#_"><?php echo $transaction->payment_type;?></a></td>
			                         <td width="11%" ><a href="#_"><?php echo $transaction->cheque_amount;?></a></td>
			                         <td><span  class="blue"><!-- 10 September 2013 --><?php echo date('d M Y',strtotime($transaction->transaction_date));?></span></td>
			                         <td><?php echo $transaction->fullname;?></td>
			                         <td width="11%" ><a href="#_"><?php echo ((isset($transaction->transaction_type) AND $transaction->transaction_type	==	'D') ? $transaction->cheque_amount.'.00' : '00.00');?></a></td>
			                         <td width="11%" ><a href="#_"><?php echo ((isset($transaction->transaction_type) AND $transaction->transaction_type	==	'C') ? $transaction->cheque_amount.'.00' : '00.00');?></a></td>
			                         <td width="11%" ><?php echo $total_amount;?></td>
			                         <td width="11%" ><?php echo $transaction->notes;?></td>
			                         <!-- 
			                        <td width="11%" ><a href="#_"><?php echo $bank_info->bankname;?></a></td>
			                        <td><?php //echo $bank_info->account_number;?></td>
			                        <td style="text-align:center; background-color:<?php echo ((isset($transaction->transaction_type) AND $transaction->transaction_type	==	'D') ? '#c4fbbb;' : '#fbf7bb;');?>"><?php echo $transaction->cheque_amount;?></td>
			                        <td><span  class="blue"><?php echo date('d M Y',strtotime($transaction->transaction_date));?></span></td>
			                        <td><?php echo $transaction->cheque_number;?></td>
			                        <td><?php echo $transaction->fullname;?></td>
			                         --> 
			                    </tr>
		                    <?php endforeach;?>
	                    <?php endif;?>
	                    <?php $total_amount	=	($credit_amount - $debit_amount);?>
	                    <tr>
	                     <td width="11%"><strong>Total Value</strong></td>
	                     <td width="11%" ></td>
	                     <td width="11%" ></td>
	                     <td width="11%" ></td>
	                     <td width="11%" ></td>
	                     <td width="11%" ></td>
	                     <td width="11%" ></td>
	                     <td width="11%" ><?php echo (isset($debit_amount) ? $debit_amount.'.00' : '00.00');?></td>
	                     <td width="11%"><?php echo (isset($credit_amount) ? $credit_amount.'.00' : '00.00');?></td>
	                     <td width="11%" ><?php echo (isset($total_amount) ? $total_amount.'.00' : '00.00');?></td>
	                    </tr>
	                   
	                </table>
	            </div>
	</div>
	
	<!--end of raw-->
	</div>
	<input type="hidden" name="companyid" value="<?php echo (isset($companyid) ? $companyid : NULL);?>">
<?php action_buttons('banks/add',$total_record); ?>

</form>
<!-- END PAGE -->  
</div>
      <!-- END PAGE -->  
   </div>
</section>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
