<?php 
  		$com 	= json_decode($comp_detail->company_name,TRUE);
		$en 	= $com['en'];
		$ar 	= $com['ar'];
?>
<?php $this->load->view('common/company-meta');?>
<script type="text/javascript">
function delete_branch(branch_id)
{
	//show confirm message
	var answer = confirm("Are you sure you want to proceed?")
	if (answer)
	{
        //var str_data = $("#login-form").serialize();
        var str_data = 'branch_id='+branch_id;
        
		// Start AJAX request
		$.ajax({
		  url: config.BASE_URL + "company_management/delete_branch",
		  dataType: 'json',
		  type: 'post',
		  data: str_data,
		  cache: false,
		  success: function(data)
		  {
			  $( "#branches" ).fadeOut( "slow" );
			 
		  }
		});
	}
	else 
	{
		e.preventDefault();
	}
}
</script>
<!--body with bg-->

<div class="body">
<header>
  <?php $this->load->view('common/header');?>
  <?php $this->load->view('common/search');?>
  <nav>
    <?php $this->load->view('common/navigations');?>
  </nav>
</header>

<!--Section-->
<section>
  <div id="container" class="row-fluid"> 
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/left-navigations');?>
    <!-- END SIDEBAR --> 
    <!-- BEGIN PAGE -->
    <div id="printdiv">
      <div id="main-content" class="main_content">
        <div class="company_logo"><img src="<?php echo base_url();?>images/default_logo.png" width="170" height="76" /></div>
        <div class="company_name"><?php echo ($en ) ? $en  : NULL;?></div>
        <form action="" method="get">
          <div class="form">
            <div class="raw">
              <div class="form_title">Company Name (بالعربية)</div>
              <div class="form_field"> <strong><?php echo ($ar ) ? $ar  : NULL;?></strong></div>
            </div>
            <!--end of raw-->
            <div class="raw">
              <div class="form_title">Company Name (English)</div>
              <div class="form_field"><?php echo ($en ) ? $en  : NULL;?></div>
            </div>
            <div class="raw">
              <div class="form_title">Phone Number</div>
              <div class="form_field"> <?php echo ($comp_detail->company_phone ) ? $comp_detail->company_phone  : NULL;?> </div>
            </div>
            <div class="raw">
              <div class="form_title">Mobile Number</div>
              <div class="form_field"> <?php echo ($comp_detail->company_mobile ) ? $comp_detail->company_mobile  : NULL;?> </div>
            </div>
            <div class="raw">
              <div class="form_title">Fax Number</div>
              <div class="form_field"> <?php echo ($comp_detail->company_fax ) ? $comp_detail->company_fax  : NULL;?> </div>
            </div>
            <!--end of raw-->
            
            <?php $get_company_branches	=	$this->company->get_company_branches($comp_detail->companyid);?>
            <?php if (!empty($get_company_branches)):?>
            <div class="black_line"></div>
            <?php foreach ($get_company_branches as $branch):?>
            <div id="branches">
              <div class="raw_payments">
                <div class="form_title">Branch Name</div>
                <div class="form_field"><?php echo (isset($branch->branchname) ? $branch->branchname : NULL);?></div>
                <div class="form_field"><a href="<?php echo base_url();?>company_management/add_branch/<?php echo $comp_detail->companyid;?>/<?php echo $branch->branchid;?>">Edit</a> | <a href="#_" onclick="delete_branch('<?php echo $branch->branchid;?>');">Delete</a></div>
              </div>
              <div class="raw">
                <div class="form_title">Phone Number</div>
                <div class="form_field"> <?php echo (isset($branch->phone_number) ? $branch->phone_number : NULL);?> </div>
              </div>
              <div class="raw">
                <div class="form_title">Mobile Number</div>
                <div class="form_field"> <?php echo (isset($branch->mobile_number) ? $branch->mobile_number : NULL);?> </div>
              </div>
              <div class="raw">
                <div class="form_title">Fax Number</div>
                <div class="form_field"> <?php echo (isset($branch->fax_number) ? $branch->fax_number : NULL);?> </div>
              </div>
            </div>
            <?php endforeach;?>
            <div class="black_line"></div>
            <?php endif;?>
            
            <!--end of raw-->
            <div class="raw">
              <div class="form_title">Email Address</div>
              <div class="form_field"> <a href="mailto:email@mail.com" target="_blank"><?php echo ($comp_detail->company_email ) ? $comp_detail->company_email  : NULL;?></a> </div>
            </div>
            <!--end of raw-->
            <div class="raw">
              <div class="form_title">Website</div>
              <div class="form_field"> <a href="http://durar-it.com" target="_blank"><?php echo ($comp_detail->company_website ) ? $comp_detail->company_website  : NULL;?></a></div>
            </div>
            <!--end of raw-->
            <div class="raw">
              <div class="form_title">P.O.Box</div>
              <div class="form_field"> <?php echo ($comp_detail->company_pobox ) ? $comp_detail->company_pobox  : NULL;?> </div>
            </div>
            <!--end of raw-->
            <div class="raw">
              <div class="form_title">P.C</div>
              <div class="form_field"> <?php echo ($comp_detail->company_pc ) ? $comp_detail->company_pc  : NULL;?> </div>
            </div>
            <!--end of raw-->
            <div class="raw">
              <div class="form_title">Address</div>
              <div class="form_field"> <?php echo ($comp_detail->company_address ) ? $comp_detail->company_address  : NULL;?> </div>
            </div>
            <!--end of raw-->
            <div class="raw">
              <div class="form_title">About Company</div>
              <div class="form_field"> <?php echo ($comp_detail->about_company ) ? $comp_detail->about_company  : NULL;?> </div>
            </div>
            <!--end of raw-->
            <div class="raw">
              <div class="form_title">Social Media</div>
              <div class="form_field">
                <div class="social_raw"> <img src="<?php echo base_url();?>images/face_icon.png" width="15" height="15" /><a href="#"><?php echo ($comp_detail->company_facebook ) ? $comp_detail->company_facebook  : NULL;?> </a></div>
                <div class="social_raw"> <img src="<?php echo base_url();?>images/twitter_icon.png" width="15" height="15" /><a href="#"><?php echo ($comp_detail->company_twitter ) ? $comp_detail->company_twitter  : NULL;?></a></div>
                <div class="social_raw"> <img src="<?php echo base_url();?>images/youtube_icon.png" width="15" height="15" /><a href="#"><?php echo ($comp_detail->company_youtube ) ? $comp_detail->company_youtube  : NULL;?></a></div>
              </div>
            </div>
            <!--end of raw--> 
            
          </div>
          <input type="submit" class="print_icon" value=""/>
          <a href="add_companyprofile.html">
          <div class="edit_icon" ></div>
          </a>
        </form>
      </div>
    </div>
    <!-- END PAGE --> 
  </div>
</section>
<!-- End Section--> 
<!--footer-->
<?php $this->load->view('common/footer');?>
