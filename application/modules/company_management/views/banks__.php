<?php $this->load->view('common/company-meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
      <div class="title">Banks</div>

<form action="" method="get">
<div class="form">

<div class="raw">
<div class="CSSTableGenerator"  >
                <table width="100%" align="left" >
                    <tr>
                      <td width="1%"><label for="checkbox"></label></td>
                        <td width="11%">Bank Name</td>
                        <td width="12%" >Account Number</td>
                        <td width="8%" >Last Import</td>
                        <td width="8%" >Date of Import</td>
                        <td width="8%" >Cheque Number</td>
                        <td width="10%" >Customer  Name</td>
                        <!-- 
                        <td width="8%" >Last Export</td>
                        <td width="8%" >Date of Export</td>
                        <td width="10%" >Cheque Number</td>
                        <td width="8%" >Customer  Name</td>
                        <td width="8%" >Value</td>
                         -->
                    </tr>
                    <tr>
                      <td  style="background-color:#afe2ee"><input type="checkbox" name="checkbox" id="checkbox" /></td>
                      <td width="11%"  style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield2" id="textfield2"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield9" id="textfield9"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield3" id="textfield3"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield7" id="textfield7"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield8" id="textfield8"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield4" id="textfield4"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield7" id="textfield7"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield8" id="textfield8"  class="flex_feild"/></td>
                      <td style="background-color:#afe2ee"><input type="text" name="textfield6" id="textfield6"  class="flex_feild"/></td>
                    </tr>
                    <tr>
                      <td><input type="checkbox" name="checkbox2" id="checkbox2" /></td>
                        <td width="11%" ><a href="bank.html">Dhofar Bank</a></td>
                      <td>25555 25555 6</td>
                        <td style="text-align:center; background-color:#c4fbbb;">205 RO.</td>
                        <td><span  class="blue">10 September 2013</span></td>
                        <td>255666542</td>
                        <td>Abou Almageed Almafrragi</td>
                        <td  style="text-align:center; background-color:#fbf7bb;">25 RO.</td>
                        <td><span  class="blue">10 September 2013</span></td>
                        <td>255666542</td>
                        <td>Abou Almageed Almafrragi</td>
                        <td style="text-align:center">25 RO.</td>
                    </tr>
                    <tr>
                      <td ><input type="checkbox" name="checkbox5" id="checkbox6" /></td>
                      <td width="11%" ><a href="bank.html">Muscat Bank</a></td>
                      <td>2555  174414</td>
                     <td style="text-align:center; background-color:#c4fbbb;">2205 RO.</td>
                     <td><span  class="blue">10 September 2013</span></td>
                     <td>242452445</td>
                     <td>Abou Almageed Almafrragi</td>
                     <td  style="text-align:center; background-color:#fbf7bb;">2205 RO.</td>
                     <td><span  class="blue">10 September 2013</span></td>
                     <td>242452445</td>
                     <td>Abou Almageed Almafrragi</td>
                     <td style="text-align:center">2205 RO.</td>
                    </tr>
                    <tr>
                      <td ><input type="checkbox" name="checkbox5" id="checkbox8" /></td>
                      <td width="11%" ><a href="bank.html">National Bank</a></td>
                      <td>82425 2452</td>
                      <td style="text-align:center; background-color:#c4fbbb;">2005 RO.</td>
                      <td><span  class="blue">10 September 2013</span></td>
                      <td>524245424</td>
                      <td>Abou Almageed Almafrragi</td>
                      <td  style="text-align:center; background-color:#fbf7bb;">2005 RO.</td>
                      <td><span  class="blue">10 September 2013</span></td>
                      <td>524245424</td>
                      <td>Abou Almageed Almafrragi</td>
                      <td style="text-align:center">2005 RO.</td>
                    </tr>
                    <tr>
                      <td ><input type="checkbox" name="checkbox5" id="checkbox7" /></td>
                      <td width="11%" ><a href="bank.html">HSBC Bank</a></td>
                      <td>524 54555 101</td>
                     <td style="text-align:center; background-color:#c4fbbb;">525 RO.</td>
                     <td><span  class="blue">10 September 2013</span></td>
                     <td>525711121</td>
                     <td>Abou Almageed Almafrragi</td>
                     <td  style="text-align:center; background-color:#fbf7bb;">5 RO.</td>
                     <td><span  class="blue">10 September 2013</span></td>
                     <td>525711121</td>
                     <td>Abou Almageed Almafrragi</td>
                     <td style="text-align:center">5 RO.</td>
                    </tr>
                    <tr>
                      <td ><input type="checkbox" name="checkbox5" id="checkbox10" /></td>
                      <td width="11%" ><a href="bank.html">Emarate Bank</a></td>
                      <td>254 42888 10</td>
                      <td style="text-align:center; background-color:#c4fbbb;">25 RO.</td>
                      <td><span  class="blue">10 September 2013</span></td>
                      <td>245241744</td>
                      <td>Abou Almageed Almafrragi</td>
                      <td  style="text-align:center; background-color:#fbf7bb;">25 RO.</td>
                      <td><span  class="blue">10 September 2013</span></td>
                      <td>245241744</td>
                      <td>Abou Almageed Almafrragi</td>
                      <td style="text-align:center">25 RO.</td>
                    </tr>
                    <tr>
                      <td colspan="11" ><strong>Total Value</strong></td>
                      <td style="background-color:#335a85; text-align:center; color:#FFF;">5150 RO.</td>
                    </tr>
                   
                </table>
            </div>
</div>

<!--end of raw-->
</div>
<input type="submit" class="print_icon" value=""/>
<input type="submit" class="delete_icon" value=""/>
<input type="submit" class="edit_icon" value=""/>
<a href="<?php echo base_url();?>company_management/banks/add"><div class="add_icon"></div></a>

</form>
<!-- END PAGE -->  
</div>
      <!-- END PAGE -->  
   </div>
</section>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
