<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['main']= 'صفحة العملاء';
$lang['Customer-Name']= 'العميل';
$lang['Number-of-Invoices']= 'الفواتير';
$lang['Mobile-Number']= 'رقم الجوال';
$lang['Type']= 'النوع';
$lang['Code']= 'الكود';
$lang['Debit']= 'الدائن';
$lang['Credit']= 'المدين';
$lang['Total']= 'الاجمالي المعاملات';
$lang['Status']= 'الحالة';
$lang['Notes']= 'ملاحظات';
$lang['Responsable-Phone']= 'جوال المسؤل';
$lang['Responsable-Name']= 'اسم المسؤل';
$lang['Address']= 'العنوان';
$lang['Email-Address']= 'البريد الالكتروني';
$lang['Fax-Number']= 'الفاكس';
$lang['Contact-Number']= 'رقم اتصال اخر';


$lang['Invoice-Number']= 'رقم الفاتوره';


$lang['Total-Price']= 'اجمالي السعر';

$lang['Net-Price']= 'السعر الصافي';

$lang['discountamount']= 'الخصم';
$lang['Customer']= 'العميل';
$lang['Date']= 'التاريخ';
$lang['Status']= 'الحالة';
$lang['Payments']= 'الدفع';
$lang['Incomplete']= 'غير مكتمل';
$lang['Complete']= 'تم';
$lang['Extra']= 'اضافي';
$lang['Cash']= 'كاش';
$lang['Bank']= 'البنك';
$lang['choose']= 'اختر';
/*Form*/

$lang['add-edit']= 'اضافة جديد & تحديث حالي'.' عميل';
$lang['mess1']= 'يرجى فهم بوضوح جميع البيانات قبل الادخال';
$lang['Add']= 'اضافة';
$lang['Reset']= 'اعاده تعيين';
$lang['Branch-Name']= 'الفرع';
$lang['RePassword']= 'تاكيد كلمة المرور';
$lang['Password']= 'كلمة المرور';
$lang['User-Name']= 'اسم المستخدم';
$lang['Add-Account']= 'اضافة عضويه';
$lang['Company-Name']= 'الشركة';
$lang['Partners']= 'الشركاء';
$lang['Account-Number']= 'رقم الحساب';



$lang['General-information']= 'البيانات <br> العامة';
$lang['Partners-information']= 'بيانات <br>الشركاء ';
$lang['File-Upload']= 'الملفات';
$lang['phone-Number']= 'رقم الهاتف';
$lang['About-Company']= 'عن الشركة';
$lang['have_Partners']= 'هل لديك شركاء';
$lang['Parnter']= 'الشركاء';
$lang['Name']= 'الاسم بالكامل';
$lang['Share']= 'نسبة المشاركة';
$lang['Total_Company_Profit']= 'اجمالي الربح';
$lang['Today-Share']= 'الربح اليومي';
$lang['Parnter_n']= 'الشريك';
$lang['Upload-Logo']= 'تحميل لوجو الشركة';
$lang['Add-files']= 'اضافة ملفات';
$lang['Upload-Documents']= 'تحميل مستندات';
$lang['Delete']= 'حذف';
$lang['Prev']= 'السابق';
$lang['Next']= 'التالي';


$lang['Edit']= 'تعديل';
$lang['Delete']= 'حذف';
$lang['vall']= 'عرض الكل';
$lang['Commercial-record']= 'رقم السجل التجاري';



/**/


$lang['show_datatable']= ' عرض  من _START_   الي   _END_  سجلات '.' الاجمالي _TOTAL_ ';
$lang['Previous']= 'السابق';
$lang['First']= 'الاول';
$lang['Last']= 'الاخير';
$lang['Next']= 'التالي';
$lang['Search']= 'البحث';
$lang['show_bylist']= 'عرض_MENU_ سجلات';
