<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['main']= 'Customers';
$lang['Customer-Name']= 'Customer Name';
$lang['Number-of-Invoices']= 'Number of Invoices';
$lang['Mobile-Number']= 'Mobile Number';
$lang['Type']= 'Type';
$lang['Code']= 'Code';
$lang['Debit']= 'Debit';
$lang['Credit']= 'Credit';
$lang['Total']= 'Total';
$lang['Status']= 'Status';
$lang['Notes']= 'Notes';
$lang['Responsable-Phone']= 'Responsable Phone';
$lang['Responsable-Name']= 'Responsable Name';
$lang['Address']= 'Address';
$lang['Email-Address']= 'Email-Address';
$lang['Fax-Number']= 'Fax-Number';
$lang['Contact-Number']= 'Contact-Number';


$lang['Invoice-Number']= 'Invoice Number';
$lang['Total-Price']= 'Total-Price';
$lang['Net-Price']= 'Net-Price';
$lang['discountamount']= 'Discount amount';
$lang['Customer']= 'Customer';
$lang['Date']= 'Date';
$lang['Status']= 'Status';
$lang['Payments']= 'Payments';
$lang['Incomplete']= 'Incomplete';
$lang['Complete']= 'Complete';
$lang['Extra']= 'Extra';
$lang['Cash']= 'Cash';
$lang['Bank']= 'Bank';
$lang['choose']= 'Choose';
/*Form*/

$lang['add-edit']= 'Add & Edit Customer';
$lang['mess1']= 'Please Understand Clearly All The Data Before Entering';
$lang['Add']= 'Add';
$lang['Reset']= 'Reset';
$lang['Branch-Name']= 'Branch Name';
$lang['RePassword']= 'RePassword';
$lang['Password']= 'Password';
$lang['User-Name']= 'User Name';
$lang['Add-Account']= 'Add Account';
$lang['Company-Name']= 'Company-Name';


$lang['General-information']= 'General <br> information';
$lang['Partners-information']= 'Partners <br> information';
$lang['File-Upload']= 'File <br> Upload';
$lang['About-Company']= 'About Company';
$lang['Parnter']= 'Parnters';
$lang['have_Partners']= 'Do you Have Partners';
$lang['Parnter']= 'Parnter';
$lang['Name']= 'Name';
$lang['Share']= 'Share';
$lang['Total_Company_Profit']= 'Total Company Profit';
$lang['Today-Share']= 'Today s Share';
$lang['Parnter_n']= 'Parnter';
$lang['Upload-Logo']= 'Upload Logo';
$lang['Add-files']= 'Add files';
$lang['Upload-Documents']= 'Upload Documents';
$lang['Delete']= 'Delete';
$lang['Prev']= 'Prev';
$lang['Next']= 'Next';

$lang['Edit']= 'Edit';
$lang['Delete']= 'Delete';
$lang['vall']= 'View all';


/**/

$lang['show_datatable']= 'Showing _START_ to _END_ of _TOTAL_ entries';
$lang['Previous']= 'Previous';
$lang['First']= 'First';
$lang['Last']= 'Last';
$lang['Next']= 'Next';
$lang['Search']= 'Search';
$lang['show_bylist']= 'Show _MENU_ entries';
