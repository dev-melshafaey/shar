<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Company_management_model extends CI_Model {
    /*
     * Properties
     */

    private $_table_users;
    private $_table_banks;
    private $_table_modules;
    private $_table_permissions;
    private $_table_companies;
    private $_table_company_branches;
    private $_table_users_bank_transactions;

//----------------------------------------------------------------------

    /*
     * Constructor
     */
    function __construct() {
        parent::__construct();

        //Load Table Names from Config
        $this->_table_users = $this->config->item('table_users');
        $this->_table_banks = $this->config->item('table_banks');
        $this->_table_modules = $this->config->item('table_modules');
        $this->_table_permissions = $this->config->item('table_permissions');
        $this->_table_companies = $this->config->item('table_companies');
        $this->_table_company_branches = $this->config->item('table_company_branches');
        $this->_table_users_bank_transactions = $this->config->item('table_users_bank_transactions');
    }

//----------------------------------------------------------------------

    /*
     * Add New Company
     * @param $data array
     * return TRUE
     */
    function add_company($data) {
        $this->db->insert($this->_table_companies, $data);
        return $this->db->insert_id();
        //return TRUE;
    }
    function inserData($table,$data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
        //return TRUE;
    }
    function get_files($id){
           $sql = "SELECT 
                company_logos.*
              FROM
                `company_logos` 

                  
                 WHERE company_logos.company_id='$id'    
              ";
        $query = $this->db->query($sql);
        return $query->result(); 
        
    }
    function getdocuments($id){
           $sql = "SELECT 
                compnay_documents.*
              FROM
                `compnay_documents` 

                  
                 WHERE compnay_documents.company_id='$id'    
              ";
        $query = $this->db->query($sql);
        return $query->result(); 
        
    }

    function update_company($data, $companyid) {
        $this->db->where('companyid', $companyid);
        $this->db->update($this->_table_companies, $data);
        return TRUE;
    }

//----------------------------------------------------------------------

    /*
     * Add New Company
     * @param $data array
     * return TRUE
     */
    function add_branch($data) {
        $this->db->insert($this->_table_company_branches, $data);

        return TRUE;
    }

    function update_branch($data, $branch_id) {
        $this->db->where('branchid', $branch_id);
        $this->db->update($this->_table_company_branches, $data);

        return TRUE;
    }

//----------------------------------------------------------------------

    /*
     * Get all Companies
     * return Object
     */

    function get_all_companies() {

        $sql = "SELECT 
            c.*,
            COUNT(cp.`company_id`) AS partners,
            cl.`logo_name` 
          FROM
            `bs_company` AS c 
            LEFT JOIN `company_partners` AS cp 
              ON cp.`company_id` = c.`companyid`
            LEFT JOIN `company_logos` cl
              ON cl.`company_id` = c.`companyid` 
              WHERE c.`is_delete` = 0
              GROUP BY c.`companyid`";
        $q = $this->db->query($sql);
        //return $q->row();
        if ($q->num_rows() > 0) {
            $customers = $q->result();
        } else {
            return '0';
        }
        return $customers;
    }

    /* 	public function get_all_companies()
      {
      $membertype = get_member_type();

      if($membertype!=5)
      {
      $this->db->where('ownerid',ownerid());
      }

      $query = $this->db->get($this->_table_companies);

      if($query->num_rows() > 0)
      {
      return $query->result();
      }
      } */

//----------------------------------------------------------------------

    /*
     * Get all Company Branches
     * @param $company_id integer
     * return Object
     */
    public function get_company_branches($company_id) {
        $this->db->where('companyid', $company_id);
        $query = $this->db->get($this->_table_company_branches);

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

//----------------------------------------------------------------------

    /*
     * Get all Companies
     * return Object
     */
    public function get_company_detail($id) {
        $this->db->where('companyid', $id);
        $query = $this->db->get($this->_table_companies);

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

//----------------------------------------------------------------------

    /*
     * Get all banks
     * return Object
     */
    public function get_banks_list() {
        $query = $this->db->get($this->_table_banks);

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

//----------------------------------------------------------------------

    /*
     * Add Transaction Data
     * @param $data array
     * return TRUE
     */
    function add_transaction($data) {
        $this->db->insert($this->_table_users_bank_transactions, $data);

        return TRUE;
    }

//----------------------------------------------------------------------

    /*
     * Add New Bank
     * @param $data array
     * return TRUE
     */
    function add_bank($data) {
        $this->db->insert($this->_table_banks, $data);

        return TRUE;
    }

//----------------------------------------------------------------------

    /*
     * Get All Transactions
     * return OBJECT
     */
    function get_all_transactions($ownerid) {
        $this->db->where('ownerid', $ownerid);
        $query = $this->db->get($this->_table_users_bank_transactions);

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

//----------------------------------------------------------------------

    /*
     * Get All Transactions
     * return OBJECT
     */
    function get_all_transactions_company($company_id) {
        $this->db->where('companyid', $company_id);
        $query = $this->db->get($this->_table_users_bank_transactions);

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function getAllcompnayPartners($company_id) {
        //111
        $w = array('company_id' => $company_id, 'is_delete' => 0);
        $this->db->where($w);
        $query = $this->db->get('company_partners');

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function getAllDocuments($company_id) {
        $sql = "    SELECT * FROM `compnay_documents` AS cd WHERE cd.`company_id` = '" . $company_id . "' AND cd.`is_delete` = 0";
        $q = $this->db->query($sql);
        return $q->result();
    }

//----------------------------------------------------------------------

    /*
     * Get Single Bank Info
     * return OBJECT
     */
    function get_bank_info() {
        $this->db->select('bankname');

        $query = $this->db->get($this->_table_banks);

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

//----------------------------------------------------------------------

    /*
     * Get Single Bank Info
     * return OBJECT
     */
    function get_single_branch($branch_id) {
        $this->db->where('branchid', $branch_id);

        $query = $this->db->get($this->_table_company_branches);

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

//----------------------------------------------------------------------

    /*
     * Delete Bulk Record
     * @param $id integer
     * return TRUE
     */
    function delete_company($id) {
        $this->db->where_in('companyid', $id);
        $this->db->delete($this->_table_companies);

        return true;
    }

//----------------------------------------------------------------------

    /*
     * Delete Bulk Record
     * @param $id integer
     * return TRUE
     */
    function delete_transaction($id) {
        $this->db->where_in('transactionid', $id);
        $this->db->delete($this->_table_users_bank_transactions);

        return true;
    }

//----------------------------------------------------------------------

    /*
     * Delete Bulk Record
     * @param $id integer
     * return TRUE
     */
    function delete_branch($id) {
        $this->db->where('branchid', $id);
        $this->db->delete($this->_table_company_branches);

        return true;
    }

//----------------------------------------------------------------------
    /**
     * 
     */
    function get_all_owner_companies($ownerid) {
        //$this->db->where('ownerid', $ownerid);
        $membertype = get_member_type();

        if ($memtype != 5) {
            $this->db->where('ownerid', ownerid());
        }

        $query = $this->db->get($this->_table_companies);


        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

//----------------------------------------------------------------------
    /**
     * 
     */
    function single_company_transactions($companyid) {
        $this->db->where('companyid', $companyid);
        $query = $this->db->get($this->_table_users_bank_transactions);

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
    }

//----------------------------------------------------------------------

    /*
     * Get Compnay Name
     * @param $companyid integer
     * return OBJECT
     */
    public function get_company_name($companyid) {
        $this->db->select('company_name');
        $this->db->where('companyid', $companyid);

        $query = $this->db->get($this->_table_companies);

        if ($query->num_rows() > 0) {
            return $query->row()->company_name;
        }
    }

    /**
     * 
     * 
     */
    public function get_company_transactions() {
        $ownerid = ownerid();
        $this->db->select("
		    bs_bank.bankname
		    , bs_users_bank_transaction.transaction_type
		    , bs_company.company_name
		    , bs_company.account_number
		    , bs_users_bank_transaction.cheque_amount
		    , bs_bank`.`bankid
		    , bs_users_bank_transaction.transactionid
		    , bs_company.companyid
		    , bs_users_bank_transaction.transaction_date
		    , bs_users_bank_transaction.payment_type,
		      (SELECT SUM(`cheque_amount`) FROM bs_users_bank_transaction WHERE transaction_type = 'D' AND ownerid=bs_users_bank_transaction.`ownerid` AND companyid=bs_company.`companyid` )  AS `t_debit` ,
  (SELECT SUM(`cheque_amount`) FROM bs_users_bank_transaction WHERE transaction_type = 'C' AND ownerid=bs_users_bank_transaction.`ownerid` AND companyid=bs_company.`companyid`)AS `t_credit` 
		");
        $this->db->from($this->_table_companies);
        $this->db->join('bs_bank', 'bs_bank.bankid = bs_company.bankid');
        $this->db->join('bs_users_bank_transaction', 'bs_users_bank_transaction.companyid = bs_company.companyid');
        $this->db->where('bs_users_bank_transaction.ownerid', $ownerid);
        $this->db->group_by('bs_company.companyid');
        $this->db->order_by("bs_company.company_name", "ASC");
        $query = $this->db->get();

        return $query->result();

        //echo $this->db->last_query();
    }

//----------------------------------------------------------------------
}

?>