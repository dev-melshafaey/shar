<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Company_management extends CI_Controller {
    /*
     * Properties
     */

    private $_data = array();

//----------------------------------------------------------------------
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        //load Home Model
        $this->load->model('company_management_model', 'company');
        $this->lang->load('main', get_set_value('site_lang'));
        $this->_data['header_links'] = 'company_management';
        $this->_data['udata'] = userinfo_permission();
    }

//----------------------------------------------------------------------

    /*
     * Company Page
     */
    public function index($action = NULL) {
        $this->_data['all_companies'] = $this->company->get_all_companies();

        //echo "<pre>";
        //print_r($this->_data['all_companies']);
        if (!empty($this->_data['all_companies'])) {
            $this->_data['total_record'] = count($this->_data['all_companies']);
        } else {
            $this->_data['total_record'] = '0';
        }

        // Load All Companies List
        //$this->load->view('company-list', $this->_data);
        $this->_data['inside'] = 'company-list';
        $this->load->view('common/main', $this->_data);
    }

    function viewPartners() {
        
    }

//------------------------------------------------------------------------

    /**
     * Add New Company
     */
    public function add($companyid = '') {


        if ($this->input->post('submit_company')) {
            // Upload a selected file and get name
            $companyid = $this->input->post('companyid');
            $file_name = $this->file_uploads('company_logo');


            $data = $this->input->post();
            $data['ownerid'] = ownerid();
            if ($file_name != '') {
                $data['company_logo'] = $file_name;
            }
            $data['company_name'] = json_encode(array('en' => $this->input->post('company_name_en'), 'ar' => $this->input->post('company_name_ar')));
            unset($data['submit_company']);
            unset($data['company_name_en']);
            unset($data['company_name_ar']);

            // insert data into database
            if ($companyid != '') {
                $this->company->update_company($data, $companyid);
            } else {
                $this->company->add_company($data);
            }
            redirect(base_url() . 'company_management');
            exit();
        } else {
            // Load Home View
            if ($companyid != '') {
                $this->_data['company'] = $this->company->get_company_detail($companyid);
                $this->_data['companyPartners'] = $this->company->getAllcompnayPartners($companyid);
                $this->_data['companydocument'] = $this->company->getAllDocuments($companyid);
                //echo "<pre>";
                //print_r($this->_data['companyPartners']);
            }



            //$this->load->view('company-form', $this->_data);
            $this->_data['inside'] = 'company-form';
            $this->load->view('common/main', $this->_data);
        }
    }

    //------------------------------------------------------------------------

    /**
     * Company Detail Pge
     */
    public function view($company_id) {
        $this->_data['comp_detail'] = $this->company->get_company_detail($company_id);

        // Load Home View
        $this->load->view('company-management', $this->_data);
    }

//----------------------------------------------------------------------

    /*
     * Banks Page
     */
    public function banks($action = NULL) {

        //echo '<pre>'; print_r($this->company->get_company_transactions());exit();

        if (isset($action)) {
            if ($action == 'add' OR $action == 'edit') {
                if ($this->input->post('submit_transaction')) {
                    // Upload a selected file and get name
                    //$file_name	=	$this->file_uploads('transaction_attachment');

                    $data = $this->input->post();
                    $data['companyid'] = $this->input->post('companyid');

                    $data['ownerid'] = ownerid();
                    //$data['transaction_attachment']		=	$file_name;
                    unset($data['submit_transaction']);
                    unset($data['company_id']);

                    // insert data into database
                    $this->company->add_transaction($data);
                    redirect(base_url() . 'company_management/transactions');
                    exit();
                } else {
                    // Get all banks List from Database
                    $this->_data['all_banks'] = $this->company->get_banks_list();
                    $this->_data['all_companies'] = $this->company->get_all_owner_companies(ownerid());

                    //print_r($this->_data['all_companies']);
                    // Load Home View
                    $this->load->view('bank-form', $this->_data);
                }
            }
        } else {
            /* $this->_data['all_transactions']	=	$this->company->get_all_transactions(ownerid());

              if(!empty($this->_data['all_transactions']))
              {
              $this->_data['total_record']		=	count($this->_data['all_transactions']);
              }
              else
              {
              $this->_data['total_record']		=	'0';
              } */


            $this->_data['all_transactions'] = $this->company->get_company_transactions();

            if (!empty($this->_data['all_transactions'])) {
                $this->_data['total_record'] = count($this->_data['all_transactions']);
            } else {
                $this->_data['total_record'] = '0';
            }


            // Load Home View
            $this->load->view('banks', $this->_data);
        }
    }

//----------------------------------------------------------------------

    /*
     * Reports Page
     */
    public function banks_listing() {
        // Load Home View
        $this->load->view('banks-listing', $this->_data);
    }

//----------------------------------------------------------------------

    /*
     * Reports Page
     */
    public function transactions($company_id = NULL) {
        if (isset($company_id)) {
            $this->_data['companyid'] = $company_id;
        } else {
            $this->_data['companyid'] = NULL;
        }

        if (!isset($company_id)) {
            // Get all banks List from Database
            $this->_data['all_banks'] = $this->company->get_banks_list();
            $this->_data['all_companies'] = $this->company->get_all_owner_companies(ownerid());
            $this->_data['all_transactions'] = $this->company->get_all_transactions(ownerid());

            if (!empty($this->_data['all_transactions'])) {
                $this->_data['total_record'] = count($this->_data['all_transactions']);
            } else {
                $this->_data['total_record'] = '0';
            }

            // Load Home View
            $this->load->view('Transactions', $this->_data);
        } else {
            $this->_data['company_name'] = $this->company->get_company_name($company_id);
            $this->_data['company_name'] = json_decode($this->_data['company_name']);

            // Get all banks List from Database
            $this->_data['all_banks'] = $this->company->get_banks_list();
            $this->_data['all_companies'] = $this->company->get_all_owner_companies(ownerid());
            $this->_data['all_transactions'] = $this->company->get_all_transactions_company($company_id);

            if (!empty($this->_data['all_transactions'])) {
                $this->_data['total_record'] = count($this->_data['all_transactions']);
            } else {
                $this->_data['total_record'] = '0';
            }
            // Load Home View
            $this->load->view('Transactions', $this->_data);
        }
    }

//----------------------------------------------------------------------

    /*
     * Reports Page
     */
    public function reports() {
        // Load Home View
        $this->load->view('reports', $this->_data);
    }

//------------------------------------------------------------------------

    /**
     * Add New Bank
     */
    public function add_bank() {
        $bankname = $this->input->post('bankname');
        $account_number = $this->input->post('account_number');

        $data = array(
            'bankname' => $bankname,
            'account_number' => $account_number
        );

        $userdata = $this->company->add_bank($data);

        echo $return = json_encode(array('success' => 'Bank has been added.'));
    }

//-----------------------------------------------------------------------
    /**
     * 
     * User Upload Images
     */
    function add_branch($company_id, $branchid = NULL) {

        if ($this->input->post('submit')) {
            // Upload a selected file and get name
            $branch_id = $this->input->post('branchid');

            $data = $this->input->post();
            $data['companyid'] = $company_id;

            unset($data['submit']);
            unset($data['set_branch_code']);


            // insert data into database
            if ($branch_id != '') {
                unset($data['branchid']);
                $this->company->update_branch($data, $branch_id);
            } else {
                unset($data['branchid']);

                $this->company->add_branch($data);
            }

            redirect(base_url() . 'company_management');
            exit();
        } else {

            if (isset($company_id) AND isset($branchid)) {
                $this->_data['single_branch_detail'] = $this->company->get_single_branch($branchid);
            }

            $company_name = $this->company->get_company_name($company_id);
            $company_name = json_decode($company_name);
            //print_r($company_name);

            $this->_data['company_name'] = strtoupper(substr($company_name->en, 0, 4));

            $this->load->view('add-branch-form', $this->_data);
        }
    }

//-----------------------------------------------------------------------
    /**
     * 
     * User Upload Images
     */
    function file_uploads($input_field) {
        $config['upload_path'] = './bank-uploads/';
        $config['allowed_types'] = '*';
        $config['max_size'] = '5000';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($input_field)) {
            $error = array('error' => $this->upload->display_errors());

            return $error = '';
        } else {
            $image_data = $this->upload->data();
            return $image_name = $image_data['file_name'];
        }
    }

//-----------------------------------------------------------------------

    /**
     * Delete Bulk Data
     */
    public function delete_branch() {
        $branch_id = $this->input->post('branch_id');

        $this->company->delete_branch($branch_id);

        echo $return = json_encode(array('success' => 'Branch has Been Delete.'));
    }

//-----------------------------------------------------------------------

    /**
     * Delete Bulk Data
     */
    function bulk_delete() {
        $all_ids = $this->input->post('ids');


        if (!empty($all_ids)) {

            $this->company->delete_record($all_ids);

            $this->session->set_flashdata('success', 'Record has been deleted.');

            redirect(base_url() . 'company_management/banks');
            exit();
        } else {
            $this->session->set_flashdata('error', 'There is No record Selected.');

            redirect(base_url() . 'company_management/banks');
            exit();
        }
    }

//-----------------------------------------------------------------------

    /**
     * Delete Bulk Data
     */
    /* function bulk_delete()
      {
      $all_ids = $this->input->post('ids');


      if(!empty($all_ids))
      {

      $this->company->delete_record($all_ids);

      $this->session->set_flashdata('success', 'Record has been deleted.');

      redirect(base_url().'company_management/banks');
      exit();
      }
      else
      {
      $this->session->set_flashdata('error', 'There is No record Selected.');

      redirect(base_url().'company_management/banks');
      exit();
      }

      } */
//-----------------------------------------------------------------------

    /**
     * Delete Bulk Data
     */
    function bulk_delete_company() {
        $all_ids = $this->input->post('ids');


        if (!empty($all_ids)) {

            $this->company->delete_company($all_ids);

            $this->session->set_flashdata('success', 'Record has been deleted.');

            redirect(base_url() . 'company_management');
            exit();
        } else {
            $this->session->set_flashdata('error', 'There is No record Selected.');

            redirect(base_url() . 'company_management');
            exit();
        }
    }

//-----------------------------------------------------------------------

    /**
     * Delete Bulk Data
     */
    function bulk_delete_transaction() {
        $all_ids = $this->input->post('ids');
        $companyid = $this->input->post('companyid');

        if (!empty($all_ids)) {

            $this->company->delete_transaction($all_ids);

            $this->session->set_flashdata('success', 'Record has been deleted.');

            if (isset($companyid)) {
                redirect(base_url() . 'company_management/transactions/' . $companyid);
                exit();
            } else {
                redirect(base_url() . 'company_management/transactions/');
                exit();
            }
        } else {
            $this->session->set_flashdata('error', 'There is No record Selected.');

            redirect(base_url() . 'company_management/transactions');
            exit();
        }
    }

    function uploadify() {

        $targetFolder = $this->config->item('bath'); // Relative to the root

        $verifyToken = md5('unique_salt' . $_POST['timestamp']);

        if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
            $targetFile = rtrim($targetPath, '/') . '/' . $_FILES['Filedata']['name'];

            // Validate the file type
            $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // File extensions
            $fileParts = pathinfo($_FILES['Filedata']['name']);

            if (in_array($fileParts['extension'], $fileTypes)) {

                //$file=move_uploaded_file($tempFile,$targetFile);
                $path = APPPATH . '../uploads/company_logo/';
                $file_name = upload_file('Filedata', $path, true, 50, 50);

                if(post('company_id_prev')!=null){
                    $sdata['company_id'] = post('company_id_prev');
                }else{
                    $sdata['company_id'] = post('company_id_up');
                }
                $sdata['logo_name'] = $file_name;

                //var_dump($sdata);
                $this->company->inserData('company_logos', $sdata);


                //print_r($file_name);
                //echo $tempFile."<br/>";
                //echo $targetFile;

                //echo '1';
            } else {
                echo '<script>Invalid file type</script>';
            }
        }
    }

    function uploadify2() {

        $targetFolder = '/demo/uploads'; // Relative to the root

        $verifyToken = md5('unique_salt' . $_POST['timestamp']);

        if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
            $targetFile = rtrim($targetPath, '/') . '/' . $_FILES['Filedata']['name'];

            // Validate the file type
            $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // File extensions
            $fileParts = pathinfo($_FILES['Filedata']['name']);

            if (in_array($fileParts['extension'], $fileTypes)) {

                //$file=move_uploaded_file($tempFile,$targetFile);
                $path = APPPATH . '../uploads/compnay_documents/';
                $file_name = upload_file('Filedata', $path, true, 50, 50);

                $sdata['document_date'] = post('document_date');
                $sdata['document_title'] = post('document_title');
                $sdata['company_id'] = post('company_id_up');
                $sdata['document_image'] = $file_name;

                //var_dump($sdata);
                $this->company->inserData('compnay_documents', $sdata);


                //print_r($file_name);
                //echo $tempFile."<br/>";
                //echo $targetFile;

                echo '1';
            } else {
                echo '<script>Invalid file type</script>';
            }
        }
    }

    function getfiles($id) {
        $invoice = $this->company->get_files($id);
        if ($invoice) {
            foreach ($invoice as $d_invoice) {
               // '.$d_invoice->company_id.'
                echo '<div class="g1" style="height: 50px;">
                         <a data-ng-href="" title="" data-gallery="" href="">
                            <img src="' . base_url() . "/uploads/company_logo/" . $d_invoice->logo_name . '" alt=""  style="width:100%;height:100%"></a>
                            <button  type="button"  onclick="remove_logo('.$d_invoice->logo_id.','.$id.')" >
                            <i class="icon-trash"></i>
                            <span>Delete</span>
                        </button>   
                        <br clear="all"/>
                       </div>';    
               
            }
            echo ' <br clear="all"       />';
        }
    }
    function deletelogo(){
        $fid=$_POST['fid'];
        $this->db->where('logo_id',$fid);
        $this->db->delete('company_logos');
        return true;
    }

    function getdocuments($id) {
        $invoice = $this->company->getdocuments($id);
        if ($invoice) {
            foreach ($invoice as $d_invoice) {
               // '.$d_invoice->company_id.'
                echo '<div class="g1" style=" height:50px">
                         <a data-ng-href="" title="" data-gallery="" href="">
                            <img src="' . base_url() . "/uploads/compnay_documents/" . $d_invoice->document_image . '" alt="" style="width:100%;height:100%"></a>
                            <button  type="button"  data-ng-click="file.$destroy()" >
                            <i class="icon-trash"></i>
                            <span>Delete</span>
                        </button>   
                        <br clear="all"/>
                       </div>';    
                
                //class="btn btn-danger destroy ng-scope"
               
            }
            echo ' <br clear="all"       />';
        }
    }

//----------------------------------------------------------------------
}

?>