<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Branches_model extends CI_Model {
    /*
     * Properties
     */

    private $_table_branch;

//----------------------------------------------------------------------

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();

        //Load Table Names from Config
        $this->_table_branch = 'bs_company_branch';
        $this->_table_users = $this->config->item('table_users');
        $this->_table_banks = $this->config->item('table_banks');
        $this->_table_customers = $this->config->item('table_customers');
        $this->_table_modules = $this->config->item('table_modules');
        $this->_table_permissions = $this->config->item('table_permissions');
        $this->_table_companies = $this->config->item('table_companies');
        $this->_table_company_branches = $this->config->item('table_company_branches');
        $this->_table_users_bank_transactions = $this->config->item('table_users_bank_transactions');
    }

//----------------------------------------------------------------------

    /*
     * Update Branches
     * @param $data array
     * @param $branch_id
     * return TRUE
     * Created M.Ahmed
     */
    function update_branch($data, $branch_id) {
        $this->db->where('branchid', $branch_id);
        $this->db->update("bs_company_branch", $data);

        return TRUE;
    }

    /**
     * 
     * Enter description here ...
     * Created by M.Ahmed
     */
    public function add_branch($data,$branch_id = '') {
        //$data = $this->input->post();
        $branch = $data['branch_id'];

        unset($data['sub_mit'], $data['sub_reset'], $data['branch_id']);

        if ($branch_id != '') {
            $this->db->where('branch_id', $branch);
            $this->db->update($this->_table_branch, $data);
            do_redirect('options?e=10');
        } else {
            $data['date_of_creation'] = date('Y-m-d H:i:s');
            $this->db->insert($this->_table_branch, $data);
            do_redirect('options?e=10');
        }
    }

//----------------------------------------------------------------------
    // Created By M.Ahmed

    function getBranchesListsWithFilter($from = '', $to = '') {
        $sql = "SELECT 
				  b.`branch_id`,
				  b.`branch_name`,
				  b.`branch_address`,
				  b.`contact_person`,
				  b.`phone_number`,
				  b.`fax_number`,
				  b.`branch_email`,
				  b.`branch_website`,
				  b.`region_id`,
				  b.`wilayat_id`,
				  b.`branch_code`,
				  b.`status`,
				  r.`region_arabic_name`,
		 		  r.`region_name`,
				  w.`wilayat_arabic_name`,
				  w.`wilayat_name` 
				FROM
				  an_branch AS b 
				  INNER JOIN an_regions AS r 
					ON (b.`region_id` = r.`region_id`) 
				  INNER JOIN an_wilayats AS w 
					ON (w.`wilayat_id` = b.`wilayat_id`) 
				  WHERE b.date_of_creation  BETWEEN '" . $from . "' AND '" . $to . "' ";
        $q = $this->db->query($sql);
        return $q->result();
    }

    // Created By M.Ahmed

    function getBranchesLists() {
        /* $sql = "SELECT 
          b.`branch_id`,
          b.`branch_name`,
          b.`branch_address`,
          b.`contact_person`,
          b.`phone_number`,
          b.`fax_number`,
          b.`branch_email`,
          b.`branch_website`,
          b.`region_id`,
          b.`wilayat_id`,
          b.`branch_code`,
          b.`status`,
          r.`region_arabic_name`,
          r.`region_name`,
          w.`wilayat_arabic_name`,
          w.`wilayat_name`
          FROM
          an_branch AS b
          INNER JOIN an_regions AS r
          ON (b.`region_id` = r.`region_id`)
          INNER JOIN an_wilayats AS w
          ON (w.`wilayat_id` = b.`wilayat_id`) ";
         */
        $sql = "SELECT * FROM bs_company_branch AS bcb GROUP BY bcb.`branchid` ";
        $q = $this->db->query($sql);
        return $q->result();
    }

    // Created by M.Ahmed

    function get_branch_detail($branch_id = '') {
        $sql = "SELECT 
				  * 
				FROM
				  bs_company_branch 
				WHERE bs_company_branch.`branchid` = '$branch_id' ";
        $q = $this->db->query($sql);
        return $q->row();
    }

}

?>