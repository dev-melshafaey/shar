
<div id="main-content" class="">



    <!--<div class="notion title alert alert-info"> <?php breadcramb(); ?> </div>-->





    <div class="notion  title alert alert-info">* <?php echo lang('mess1') ?></div>
    <?php error_hander($this->input->get('e')); ?>


    
    <form action="<?php echo base_url(); ?>branches/add_branches/<?php echo (isset($branch->branchid)) ? $branch->branchid : ''; ?>" method="post" class="sample-form" id="frm_branch" name="frm_branch" autocomplete="off">

        <input type="hidden" name="branchid" id="branchid" value="<?php echo (isset($branch->branchid)) ? $branch->branchid : ''; ?>" />
        <div class="form mycontent">
            <div class="g4">
                <label><?php echo lang("Company-Name")?></label>
                <div class="">
                    <div class="dropmenu">
                        <div class="styled-select">
                            <?php company_dropbox('companyid', $branch->companyid); ?>
                            <span class="arrow arrowselectbox">&amp;</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="g4">
                <label><?php echo lang("Branch-Name-ar")?></label>
                <div class="">
                    <input name="branchname[arabic]" id="branch_name" value="<?php echo _s($branch->branchname,"arabic"); ?>" type="text"  class="required  valid formtxtfield"/>
                </div>
            </div>                    
            <div class="g4">
                <label><?php echo lang("Branch-Name-en")?></label>
                <div class="">
                    <input name="branchname[english]" id="branch_name" value="<?php echo _s($branch->branchname,"english"); ?>" type="text"  class="required  valid formtxtfield"/>
                </div>
            </div>                    

            <div class="g4">
                <label><?php echo lang("contactperson")?></label>
                <div class="">
                    <input name="contactperson" id="contactperson" value="<?php echo isset($branch->contactperson) ? $branch->contactperson : ''; ?>" type="text"  class="formtxtfield"/>
                    <!--<input type="hidden" name="contact_person_id" id="contact_person_id" value="" />-->
                </div>
            </div>
<br clear="all"/>
            <div class="g4">
                <label><?php echo lang("Phone-Number")?></label>
                <div class="">
                    <input name="phone_number" id="phone_number" value="<?php echo isset($branch->phone_number) ? $branch->phone_number : ''; ?>" type="text"  class="formtxtfield"/>
                </div>
            </div>
            
            <div class="g4">
                <label><?php echo lang("Fax-Number")?></label>
                <div class="">
                    <input name="fax_number" id="fax_number" value="<?php echo isset($branch->fax_number) ? $branch->fax_number : ''; ?>" type="text"  class="formtxtfield"/>
                </div>
            </div>

            <div class="g4">
                <label><?php echo lang("Email-Address")?></label>
                <div class="">
                    <input name="branch_email" id="branch_email" value="<?php echo isset($branch->branch_email) ? $branch->branch_email : ''; ?>" type="text"  class="formtxtfield"/>
                </div>
            </div>

            <div class="g4">
                <label><?php echo lang("Branch-website")?></label>
                <div class="">
                    <input name="branch_website" id="branch_website" value="<?php echo isset($branch->branch_website) ? $branch->branch_website : ''; ?>" type="text"  class="formtxtfield"/>
                </div>
            </div>
<br clear="all"/>
            <div class="g4">
                <label><?php echo lang("Branch-code")?></label>
                <div class="">
                    <input name="branch_code" id="branch_code" value="<?php echo isset($branch->branch_code) ? $branch->branch_code : ''; ?>" type="text"  class="formtxtfield"/>
                </div>
            </div>

        
            <!--            <div class="g4">
                            <label>Region</label>
                            <div class="">
                                <div class="dropmenu">
                                    <div class="styled-select">
            <?php //region_dropbox('region_id',isset($branch->region_id)?$branch->region_id:'','english'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
            
            
            
            
            
                        <div class="g4">
                            <label>Wilayat</label>
                            <div class="">
                                <div class="dropmenu">
                                    <div class="styled-select" id="div_wilayat_show">
            <?php
            //wilayat_dropbox('wilayat_id',isset($branch->wilayat_id)?$branch->wilayat_id:'',$language='english');
            ?>
                                    </div>
                                </div>
                            </div>
                        </div> -->

            <div class="g4">
                <label><?php echo lang("Status")?></label>
                <div class="">
                    <div class="dropmenu">
                        <div class="styled-select">
                            <?php get_statusdropdown(isset($branch->status) ? $branch->status : '', 'status', 'status'); ?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="g5">
                <label><?php echo lang("Address")?></label><br clear="all"/>
                <div class="">
                    <textarea id="branchaddress" name="branchaddress" cols="" rows="" class="ckeditor formareafield"><?php echo isset($branch->branchaddress) ? $branch->branchaddress : ''; ?></textarea>
                </div>
            </div>


            <br clear="all"/>
            <div class="raw-group">
                <?php
                $btn_text = isset($branch->branchid) ? lang("Add") : lang("Add");
                ?>
                <input name="sub_mit" id="sub_mit" type="submit" class="submit_btn green flt-r g3" value="<?php echo $btn_text;  ?>" />
                <?php if (!isset($branch->branch_id)) { ?>
                    <input name="sub_reset" type="reset" class="btn-flat primary reset_btn gray flt-r g3" value="<?php echo lang("Reset")?>" />
                <?php } ?>
            </div>   





            <!--end of raw--> 
        </div>
    </form>
</div>

<!-- End Section-->
