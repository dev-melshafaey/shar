
<div id="main-content" class="main_content">
<!--<div class="title"> <span><?php breadcramb(); ?></span> </div>-->
    <?php error_hander($this->input->get('e')); ?>
    <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">
        <div class="form">
            <div class="" id="printdiv" >
                <div style=" margin-left: 12px; margin-top: 4px;" class="fdirection">
                    <div class="g3">
                    <label class="" style="">From Date </label>
                    <input type="text" id="from_date_filter" value="" class="right formtxtfield"  style=""/> 
                    </div>
                    <div class="g3">
                    <label class=""  style="">To Date </label>
                    <input type="text" id="to_date_filter" value="" class="right formtxtfield"  style=""/>
                    </div>
                    <div class="g2">
                        <br/>
                    <input type="button" onclick="updateBranchesDataTableDiv();" value="Search" class="btn-flat primary ">
                    </div>
                    <br clear="all"/>
                </div>
                <table width="100%" align="left" id="new_data_table">
                    <thead class="thead">
                        <tr>

                            <th id="no_filter"><label for="checkbox"></label></th>
                            <th><?php echo lang("Branch-Name")?></th>
                            <th ><?php echo lang("Phone-Number")?></th>
                            <th ><?php echo lang("Email-Address")?></th>
                            <th ><?php echo lang("Branch-website")?></th>
                            <th ><?php echo lang("Branch-code")?></th>
                            <th ><?php echo lang("Status")?></th>
                            <!--<th width="5%">Region</th>-->
                            <!--<th width="9%">wilayat</th>-->
                            <th  id="no_filter">&nbsp;</th>
                        </tr>
                    </thead>

                    <?php
                    $cnt = 0;
                    foreach ($this->branches->getBranchesLists() as $branchdata) {
                        $cnt++;
                        ?>
                        <tr>
                            <td ><input type="checkbox" style="" class="allcb" name="ids[]" id="u_<?php echo $branchdata->branchid; ?>" value="<?php echo $branchdata->branchid; ?>" /></td>
                            <td><?php echo _s($branchdata->branchname,  get_set_value('site_lang')); ?></td>
                            <td><?php echo $branchdata->contactperson; ?></td>
                            <td><?php echo $branchdata->branch_email; ?></td>
                            <td><?php echo ($branchdata->branch_website); ?></td>
                            <td><?php echo $branchdata->branch_code; ?></td>
                            <td><?php echo ($branchdata->status == 'A') ? 'Active' : 'Not Active'; ?></td>
                            <!--<td><?php echo ucfirst($branchdata->region_name); ?></td>-->
                            <!--<td><?php echo ucfirst($branchdata->wilayat_name); ?></td>-->
                            <td>
                                <?php edit_button('add_branches/' . $branchdata->branchid); ?> 
                                <!--<a id="<?php echo $branchdata->branch_id; ?>" href='#' class='basic'><img src="<?php echo base_url(); ?>images/internal/i_info.png" width="28" height="18" border="0" /></a>-->          
                                <!-- modal content -->


                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
        <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>

        <?php //action_buttons('addnewcustomer',$cnt); ?>
 <!--<input type="submit" class="send_icon" value=""/>-->
    </form>
</div>
<!-- END PAGE -->  


<!-- End Section-->
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">-->
<script>
    $(function () {
        $("#from_date_filter").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            numberOfMonths: 1,
            onClose: function (selectedDate) {
                $("#to_date_filter").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#to_date_filter").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            numberOfMonths: 1,
            onClose: function (selectedDate) {
                $("#from_date_filter").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
</script>
<!--footer-->

<div id="basic-modal-content" class="dig<?php echo $branchdata->branch_id; ?>" style="display: none">
    <h3><?php echo $branchdata->branch_name; ?><br />
        Code : #<?php echo $branchdata->branch_code ?></h3>

    <code> <span class="pop_title">Branch Address : </span>
        <div class="pop_txt"><?php echo $branchdata->branch_address; ?></div>
    </code> 
    <!--line--> 
    <code> <span class="pop_title">Status : </span>
        <div class="pop_txt"><?php echo ($branchdata->status == 'A') ? 'Active' : 'Not Active'; ?></div>
    </code> 
    <!--line--> 
    <code> <span class="pop_title">Contact person : </span>
        <div class="pop_txt"><?php echo $branchdata->contact_person; ?></div>
    </code>
    <!--line--> 
    <code> <span class="pop_title">Phone number : </span>
        <div class="pop_txt"><?php echo $branchdata->phone_number; ?></div>
    </code> 
    <!--line--> 
    <code> <span class="pop_title">FAX : </span>
        <div class="pop_txt"><?php echo $branchdata->fax_number; ?></div>
    </code> 
    <!--line--> 
    <code> <span class="pop_title">Email : </span>
        <div class="pop_txt"><?php echo $branchdata->branch_email; ?></div>
    </code> 
    <code> <span class="pop_title">Branch Website : </span>
        <div class="pop_txt"><?php echo($branchdata->branch_website); ?></div>
    </code>
    <code> <span class="pop_title">Region : </span>
        <div class="pop_txt"><?php echo($branchdata->region_name); ?></div>
    </code>
    <code> <span class="pop_title">Wilayat Name : </span>
        <div class="pop_txt"><?php echo($branchdata->wilayat_name); ?></div>
    </code>
    <!--line-->  
    <?php
    //get_permission_text();
    ?>
</ul>
</div>


