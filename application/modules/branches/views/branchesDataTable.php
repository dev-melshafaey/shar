<div style="float: left; margin-left: 12px; margin-top: 4px;">
                From Date 
                <input type="text" id="from_date_filter" value="<?php echo $from_date_filter;?>"> 
                To Date 
                <input type="text" id="to_date_filter" value="<?php echo $to_date_filter;?>" >
                <input type="button" onclick="updateBranchesDataTableDiv();" value="Search">
            </div>
            <table width="100%" align="left" id="usertable">
              <thead>
              <tr>

                <th width="1%" id="no_filter"><label for="checkbox"></label></th>
                <th width="20%">Branch name</th>
                <th width="20%">Phone number</th>
                <th width="12%" >Branch email</th>
                <th width="12%">Branch website</th>
                <th width="5%">Branch code</th>
                <th width="5%">Status</th>
                <th width="5%">Region</th>
                <th width="9%">wilayat</th>
                <th width="7%" id="no_filter">&nbsp;</th>
              </tr>
              </thead>
              <tfoot>
              <tr>
                <td  style="background-color:#afe2ee"><input type="checkbox" name="checkbox" id="checkboxall" /></td>
                <td  style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield2" id="textfield2"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield4" id="textfield4"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"></td>
                <td style="background-color:#afe2ee"></td>
                <td  style="background-color:#afe2ee"><input type="text" name="textfield5" id="textfield5"  class="flex_feild"/></td>
                <td style="background-color:#afe2ee"></td>
              </tr>
              </tfoot>
              <?php
			  	$cnt = 0;
              	foreach($this->branches->getBranchesListsWithFilter($from_date_filter,$to_date_filter) as $branchdata) {
					$cnt++;
			  ?>
              <tr>
                <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $branchdata->branch_id; ?>" value="<?php echo $branchdata->branch_id; ?>" /></td>
                <td><?php echo ucfirst($branchdata->branch_name); ?></td>
                <td><?php echo $branchdata->branch_code; ?></td>
                <td><?php echo $branchdata->branch_email; ?></td>
               	<td><?php echo ($branchdata->branch_website); ?></td>
               	<td><?php echo $branchdata->branch_code; ?></td>
               	<td><?php echo ($branchdata->status=='A')?'Active':'Not Active'; ?></td>
                <td><?php echo ucfirst($branchdata->region_name); ?></td>
                <td><?php echo ucfirst($branchdata->wilayat_name); ?></td>
                <td><?php edit_button('add_branches/'.$branchdata->branch_id); ?> <a id="<?php echo $branchdata->branch_id; ?>" href='#' class='basic'><img src="<?php echo base_url(); ?>images/internal/i_info.png" width="28" height="18" border="0" /></a>          
                  <!-- modal content -->
                  
                  <div id="basic-modal-content" class="dig<?php echo $branchdata->branch_id; ?>">
                    <h3><?php echo $branchdata->branch_name; ?><br />
                      Code : #<?php echo $branchdata->branch_code ?></h3>
                    
                    <code> <span class="pop_title">Branch Address : </span>
                    <div class="pop_txt"><?php echo $branchdata->branch_address; ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Status : </span>
                    <div class="pop_txt"><?php echo ($branchdata->status=='A')?'Active':'Not Active'; ?></div>
                    </code> 
                    <!--line--> 
                    <code> <span class="pop_title">Contact person : </span>
                    <div class="pop_txt"><?php echo $branchdata->contact_person; ?></div>
                    </code>
                    <!--line--> 
                    <code> <span class="pop_title">Phone number : </span>
                    <div class="pop_txt"><?php echo $branchdata->phone_number; ?></div>
                    </code> 
                   <!--line--> 
                    <code> <span class="pop_title">FAX : </span>
                    <div class="pop_txt"><?php echo $branchdata->fax_number; ?></div>
                    </code> 
                     <!--line--> 
                    <code> <span class="pop_title">Email : </span>
                    <div class="pop_txt"><?php echo $branchdata->branch_email; ?></div>
                    </code> 
                    <code> <span class="pop_title">Branch Website : </span>
                    <div class="pop_txt"><?php echo($branchdata->branch_website); ?></div>
                    </code>
                   <code> <span class="pop_title">Region : </span>
                    <div class="pop_txt"><?php echo($branchdata->region_name); ?></div>
                    </code>
                    <code> <span class="pop_title">Wilayat Name : </span>
                    <div class="pop_txt"><?php echo($branchdata->wilayat_name); ?></div>
                    </code>
                    <!--line-->  
                    	<?php
							//get_permission_text();
						?>
                      </ul>
                    </div>
                    </code>
                    </div></td>
              </tr>
              <?php } ?>
            </table>
         <script>
$(document).ready(function(){	

$( "#from_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to_date_filter" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to_date_filter" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
dateFormat: 'yy-mm-dd',
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from_date_filter" ).datepicker( "option", "maxDate", selectedDate );
}
});


	if($('#usertable').length > 0)
	{
		 $('#usertable tfoot td').each( function () {
			var title = $('#usertable thead th').eq( $(this).index() ).text();
			var attr_id = $('#usertable thead th').eq( $(this).index() ).attr('id');
			$(this).html( '<input class="'+attr_id+' searchfilter" type="text" placeholder="'+title+'" />' );		
		 });
		
		
		
		var user_table = $('#usertable').DataTable();	
		user_table.columns().eq(0).each( function ( colIdx ) {		
			$( 'input', user_table.column( colIdx ).footer() ).on( 'keyup change', function () {
				user_table
					.column( colIdx )
					.search( this.value )
					.draw();
			} );
			
		} );
		$('.no_filter, #usertable_filter').hide();
		
		$('.searchfilter').keyup(function(){
			console.log($(this).val());
			$('#usertable td').removeHighlight().highlight($(this).val());
		});
	}


	$('.basic').click(function() {
	
		var id = $(this).attr('id');
			$('.dig'+id).modal();

		return false;
	});
	
});
		
		 </script>