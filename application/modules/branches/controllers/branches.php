<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Branches extends CI_Controller {
    /*
     * Properties
     */

    private $_data = array();

//----------------------------------------------------------------------
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        //load Home Model
        $this->load->model('branches_model', 'branches');
        $this->load->model('users/users_model', 'users');
        $this->lang->load('main', get_set_value('site_lang'));
        $this->_data['udata'] = userinfo_permission();
    }

//----------------------------------------------------------------------

    public function options() {
        // Load Home View
        //$this->branches->update_branch($data,$branch_id);
        //$this->load->view('branches',	$this->_data);
        $this->_data['inside2'] = 'branches';
        $this->_data['inside'] = 'add-branch-form';
        $this->load->view('common/tabs', $this->_data);
    }

    public function optionsDataTable() {
        // Load Home View
        $this->_data['from_date_filter'] = $this->input->post('from_date_filter');
        $this->_data['to_date_filter'] = $this->input->post('to_date_filter');
        //$this->branches->update_branch($data,$branch_id);
        $this->load->view('branchesDataTable', $this->_data);
    }

//----------------------------------------------------------------------

    /*
     * Add branches
     */
    public function add_branches($branch_id = NULL) {

        if ($this->input->post('sub_mit')) {
            $data = $this->input->post();
            unset($data['sub_mit']);
            unset($data['contact_person_id']);
            unset($data['sub_reset']);
            $data['branchname']=serialize(post('branchname'));
            $branch_id = $this->input->post('branchid');
            // insert data into database
            if ($branch_id != '') {
                $this->branches->update_branch($data,$branch_id);
                redirect(base_url() . 'branches/options/?e=11');
                exit();
            } else {
                $this->branches->add_branch($data);
                redirect(base_url() . 'branches/options/?e=10');
                exit();
            }
        } else {
            if ($branch_id != '') {
                $this->_data['branch'] = $this->branches->get_branch_detail($branch_id);
            } else {
                $this->_data['branch_id'] = '';
            }
            //$this->load->view('add-branch-form',	$this->_data);
            $this->_data['inside'] = 'add-branch-form';
            $this->_data['inside2'] = 'branches';
            $this->load->view('common/tabs', $this->_data);
        }
    }

    //----------------------------------------------------------------------
    /**
     * 
     * Enter description here ...
     * Created by M.Ahmed
     */
    /*     * *
     * 
     */
    public function delete_branch() {
        $all_ids = $this->input->post('ids');


        if (!empty($all_ids)) {

            $this->branches->delete_branches($all_ids);

            $this->session->set_flashdata('success', 'Record has been deleted.');

            redirect(base_url() . 'branches/options');
            exit();
        } else {
            $this->session->set_flashdata('error', 'There is No record Selected.');

            redirect(base_url() . 'branches/options');
            exit();
        }
    }

//----------------------------------------------------------------------
}

?>