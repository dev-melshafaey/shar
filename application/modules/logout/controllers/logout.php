<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class logout extends CI_Controller
{
	/*
	* Properties
	*/

    private $_data	=	array();

//----------------------------------------------------------------------
	/*
	* Constructor
	*/
   public function __construct()
   {
		parent::__construct();
		$this->session->sess_destroy();		
		redirect(base_url().'login');
   }

   
}
?>