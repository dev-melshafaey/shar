<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Logout_model extends CI_Model {
	
	/*
	* Properties
	*/
	
	private $_table_users;
	private $_table_banks;
	private $_table_modules;
	private $_table_permissions;
	private $_table_companies;
	private $_table_company_branches;
	private $_table_users_bank_transactions;

//----------------------------------------------------------------------
    
	/*
	* Constructor
	*/
	function __construct()
    {
        parent::__construct();
		
		//Load Table Names from Config
		$this->_table_users						=	$this->config->item('table_users');
		$this->_table_banks						=	$this->config->item('table_banks');
		$this->_table_modules					=	$this->config->item('table_modules');
		$this->_table_permissions				=	$this->config->item('table_permissions');
		$this->_table_companies					=	$this->config->item('table_companies');
		$this->_table_company_branches			=	$this->config->item('table_company_branches');
		$this->_table_users_bank_transactions	=	$this->config->item('table_users_bank_transactions');
    }
//----------------------------------------------------------------------

	/*
	* Get all banks
	* return Object
	*/
	public function get_banks_list()
	{
		$query = $this->db->get($this->_table_banks);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* Add Transaction Data
	* @param $data array
	* return TRUE
	*/
	function add_transaction($data)
	{
		$this->db->insert($this->_table_users_bank_transactions, $data);
		 
		return TRUE;
	}
	
//----------------------------------------------------------------------
}
?>