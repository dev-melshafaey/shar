<?php $this->load->view('common/company-meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
      <div class="title">Reports</div>
      <div class="invoice_raw">
<div class="invoice_no2">
<div class="left_small2"><strong>From :</strong></div>
<div class="left_div">
<input name="" type="text"  class="formtxtfield"/>
<div class="date_icon"><a href="#"><img src="<?php echo base_url();?>images/internal/date_icon.png" width="22" height="24" border="0" /></a></div>
</div>
</div>
<div class="invoice_no2">
  <div class="left_small2"><strong>To :</strong></div> 
  <input name="" type="text"  class="formtxtfield"/>
<div class="date_icon"><a href="#"><img src="<?php echo base_url();?>images/internal/date_icon.png" width="22" height="24" border="0" /></a></div></div>
<div class="invoice_no2">
  <span>
<input name="" type="submit"  class="show_btn" value="Show"/></span> 
</div>
</div>
      <form action="" method="get">
  <div class="form">
  <div class="raw">
<div class="raw_payments">
<div class="form_title">Customers</div>
<div class="report_close"><img src="<?php echo base_url();?>images/internal/close_report.png" width="16" height="16"  onclick="$(this).parent().parent().parent().remove();" /></div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Companies Customer</div>
<div class="form_field">550 Customer</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Individuals Customer</div>
<div class="form_field">550 Customer</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Customer</div>
<div class="form_field">1100 Customer</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Paid</div>
<div class="form_field">400 RO.</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Rest Money</div>
<div class="form_field">300 RO.</div>
</div>
</div>
<div class="raw">
<div class="raw_payments">
<div class="form_title">Sales</div>
<div class="report_close"><img src="<?php echo base_url();?>images/internal/close_report.png" width="16" height="16"  onclick="$(this).parent().parent().parent().remove();" /></div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Sales Representative</div>
<div class="form_field">
25 Sales
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Salleries <span class="light_txt">(Net Salleries)</span></div>
<div class="form_field">4800 RO.</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Comissions</div>
<div class="form_field">1100 RO.</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Salleries </div>
<div class="form_field">5900 RO.</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Sallery Avarage</div>
<div class="form_field">
300 RO.
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Comission Avarage</div>
<div class="form_field">
10%
</div>
</div>
</div>
<div class="raw">
<div class="raw_payments">
<div class="form_title">Sales Invoices</div>
<div class="report_close"><img src="<?php echo base_url();?>images/internal/close_report.png" width="16" height="16"  onclick="$(this).parent().parent().parent().remove();"  /></div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Sales Invoices</div>
<div class="form_field">263 Invoice</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Customers</div>
<div class="form_field">
15 Customer
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Products</div>
<div class="form_field">
25 Product
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Prices</div>
<div class="form_field">
2000 RO.
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Net Prices</div>
<div class="form_field">
1500  RO.</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Cash</div>
<div class="form_field">
500 RO.
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Cheques</div>
<div class="form_field">
1000 RO.
</div>
</div>
<!--end of raw-->
</div>
<div class="raw">
<div class="raw_payments">
<div class="form_title">Purchase Invoices</div>
<div class="report_close"><img src="<?php echo base_url();?>images/internal/close_report.png" width="16" height="16"   onclick="$(this).parent().parent().parent().remove();" /></div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Purchase Invoices</div>
<div class="form_field">263 Invoice</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Suppliers</div>
<div class="form_field">
15 Suppliers
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Products</div>
<div class="form_field">
25 Product
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Prices</div>
<div class="form_field">
2000 RO.
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Net Prices</div>
<div class="form_field">
1500  RO.</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Cash</div>
<div class="form_field">
500 RO.
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Cheques</div>
<div class="form_field">
1000 RO.
</div>
</div>
</div>
<div class="raw">
<div class="raw_payments">
<div class="form_title">Banks</div>
<div class="report_close"><img src="<?php echo base_url();?>images/internal/close_report.png" width="16" height="16"   onclick="$(this).parent().parent().parent().remove();" /></div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title"> Total Banks</div>
<div class="form_field">
4 Banks
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title"> Muscat Bank</div>
<div class="form_field">
2563 RO.</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title"> Dhofar Bank</div>
<div class="form_field">
2563 RO.</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title"> HSBC Bank</div>
<div class="form_field">
2563 RO.</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title"> National  Omani Bank</div>
<div class="form_field">
2563 RO.</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Account</div>
<div class="form_field">
9563 RO.</div>
</div>
</div>
<!--end of raw-->
<div class="raw">

<div class="raw_payments">
<div class="form_title">Qutations</div>
<div class="report_close"><img src="<?php echo base_url();?>images/internal/close_report.png" width="18" height="18"  onclick="$(this).parent().parent().parent().remove();"  /></div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Qutations</div>
<div class="form_field">550 Qutation</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Waiting Qutations</div>
<div class="form_field">400 Qutation</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Accepted Qutations</div>
<div class="form_field">150 Qutation</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Avarage of Qutation</div>
<div class="form_field">1510 RO.</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Price</div>
<div class="form_field">35000 RO.</div>
</div>
</div>
<div class="raw">
<div class="raw_payments">
<div class="form_title">Inventory</div>
<div class="report_close"><img src="<?php echo base_url();?>images/internal/close_report.png" width="18" height="18"  onclick="$(this).parent().parent().parent().remove();"  /></div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Categories</div>
<div class="form_field">
25 Category
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Items </div>
<div class="form_field">100 Item</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Suppliers</div>
<div class="form_field">11 Supplier</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Customers </div>
<div class="form_field">100 Customer</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Services</div>
<div class="form_field">
52 Service
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Products</div>
<div class="form_field">
52 Product
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Stores</div>
<div class="form_field">
10 Stores
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Avarage of Capasity</div>
<div class="form_field">
1500 KG. - 1200 Unit
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Point of Re-Order</div>
<div class="form_field">
10 Products
</div>
</div>
</div>
<div class="raw">
<div class="raw_payments">
<div class="form_title">Expenses</div>
<div class="report_close"><img src="<?php echo base_url();?>images/internal/close_report.png" width="18" height="18"  onclick="$(this).parent().parent().parent().remove();"  /></div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Water</div>
<div class="form_field">263 RO.</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Salaries</div>
<div class="form_field">
15000 RO.
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Rents</div>
<div class="form_field">
2550 RO.
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Petrol</div>
<div class="form_field">
2000 RO.
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Orginal</div>
<div class="form_field">
1500  RO.</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Maintance</div>
<div class="form_field">
500 RO.
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Electracity</div>
<div class="form_field">
1000 RO.
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Cost</div>
<div class="form_field">
85320 RO.
</div>
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="raw_payments">
<div class="form_title">Payments</div>
<div class="report_close"><img src="<?php echo base_url();?>images/internal/close_report.png" width="18" height="18"  onclick="$(this).parent().parent().parent().remove();"  /></div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Payments</div>
<div class="form_field">26 Payment</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Customers</div>
<div class="form_field">
19 Customer
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Paid</div>
<div class="form_field">
2550 RO.
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total On Credit</div>
<div class="form_field">
2550 RO.
</div>
</div>
</div>
<div class="raw">
<div class="raw_payments">
<div class="form_title">Users</div>
<div class="report_close"><img src="<?php echo base_url();?>images/internal/close_report.png" width="18" height="18"  onclick="$(this).parent().parent().parent().remove();"  /></div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title"> Sales</div>
<div class="form_field">
20 Sales
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title"> Data Entries</div>
<div class="form_field">
10  Data Entry</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title"> Super Admins</div>
<div class="form_field">
1 Admin
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Admins</div>
<div class="form_field">
11 Admin
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Total Users</div>
<div class="form_field">
42 Person
</div>
</div>
</div>
</div>
<div class="raw">
<input type="submit" class="print_icon" value=""/>
<a href="#">
<div class="send_icon" ></div>
</a>
<a href="#">
<div class="pdf_icon" ></div>
</a>
<a href="#">
<div class="word_icon" ></div>
</a>
</div>
</form>

</div>
      <!-- END PAGE -->  
   </div>
</section>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
