<?php $this->load->view('common/company-meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
        <div class="company_logo"><img src="<?php echo base_url();?>images/default_logo.png" width="170" height="76" /></div>
        <div class="company_name">Company Name</div>
      <form action="" method="get">
  <div class="form">

<div class="raw">
<div class="form_title">Company Name (بالعربية)</div>
<div class="form_field">
  <strong>إسم الشركة بالعربية </strong></div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Company Name (English)</div>
<div class="form_field">
Company Name
</div>
</div>
<!--end of raw-->
<div class="black_line"></div>
<div class="raw_payments">
<div class="form_title">Main Branch</div>
<div class="form_field">Main Branch</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Phone Number</div>
<div class="form_field">
222 2222 2222 
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Mobile Number</div>
<div class="form_field">
2222 2222 2222
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Fax Number</div>
<div class="form_field">
2222 2222 22222
</div>
</div>
<div class="raw_payments">
<div class="form_title">Alkhwier Branch</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Phone Number</div>
<div class="form_field">
222 2222 2222 
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Mobile Number</div>
<div class="form_field">
2222 2222 2222
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Fax Number</div>
<div class="form_field">
2222 2222 22222
</div>
</div>
<div class="black_line"></div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Email Address</div>
<div class="form_field">
<a href="mailto:email@mail.com" target="_blank">Email@mail.com</a>
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Website</div>
<div class="form_field">
  <a href="http://durar-it.com" target="_blank">www.durar-it.com</a></div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">P.O.Box</div>
<div class="form_field">
  523
  </div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">P.C</div>
<div class="form_field">
121
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Address</div>
<div class="form_field">
Alkhod, Pizza HUT, Muscat
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">About Company</div>
<div class="form_field">
Our Location in the Sultanate of Oman, offer Website Development Services, Web Design, Commercial sites in the search engines, e-commerce services, maintenance and follow-up, consulting, training and offer these services to all companies large or small-sized enterprises in all over the world. Dürer solutions smart you connect your requirement ofInternet solutions to serve your work as soon as possible with listening to provide better ways of technical support
</div>
</div>
<!--end of raw-->
<div class="raw">
<div class="form_title">Social Media</div>
<div class="form_field">
<div class="social_raw">
<img src="<?php echo base_url();?>images/face_icon.png" width="15" height="15" /><a href="#">www.facebook.com
</a></div>
<div class="social_raw">
<img src="<?php echo base_url();?>images/twitter_icon.png" width="15" height="15" /><a href="#">www.twitter.com</a></div>
<div class="social_raw">
<img src="<?php echo base_url();?>images/youtube_icon.png" width="15" height="15" /><a href="#">www.youtube.com </a></div>
</div>
</div>
<!--end of raw-->

</div>

<input type="submit" class="print_icon" value=""/>
<a href="add_companyprofile.html">
<div class="edit_icon" ></div>
</a>
</form>

</div>
      <!-- END PAGE -->  
   </div>
</section>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
