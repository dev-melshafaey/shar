<?php $this->load->view('common/company-meta');?>
<!--body with bg-->
<div class="body">
<header>
	<?php $this->load->view('common/header');?>
	<?php $this->load->view('common/search');?>
	<nav>
		<?php $this->load->view('common/navigations');?>
	</nav>
</header>

<!--Section-->
<section>
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/left-navigations');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" class="main_content">
      <div class="title">
       <span><a href="banks.html">Banks</a></span>
       <span class="subtitle">New Bank Transaction</span>
       </div>

	<form action="<?php echo current_url();?>" method="post" enctype="multipart/form-data">
		<div class="form">
		  <div class="raw">
		<div class="form_title">Full Name</div>
		<div class="form_field">
		<input name="fullname" type="text"  class="formtxtfield"/>
		</div>
		  </div>
		<div class="raw">
		<div class="form_title">Bank Name</div>
		<div class="form_field">
		  <div class="dropmenu">
			<div class="styled-select">
			  <select name="bankid">
				<option>-- Select Bank --</option>
				<?php if(!empty($all_banks)):?>
					<?php foreach($all_banks as $banks):?>
						<option value="<?php echo $banks->bankid;?>"><?php echo $banks->bankname;?></option>
					<?php endforeach;?>
				<?php endif;?>
			  </select>
			</div>
		  </div>
		</div>
		<div class="left_div2">
		<div id='osx2-modal'>
			<div class="invoice_add_customer"><a href='#' class='osx2'><img src="<?php echo base_url();?>images/internal/add_customer.png" width="20" height="20" border="0" /></a>		    </div>
		</div>
		</div>
		<div id="osx2-modal-content">
					<div id="osx2-modal-title">Add New Bank</div>
					<div class="close"><a href="#" class="simplemodal-close">x</a></div>
				<div id="osx2-modal-data">
		<div class="raw">
		<div class="form_title">Bank  Name</div>
		<div class="form_field">
		<input name="" type="text"  class="formtxtfield"/>
		</div>
		</div>
		<div class="raw">
		<div class="form_title">Account Number</div>
		<div class="form_field">
		<input name="" type="text"  class="formtxtfield"/>
		</div>
		</div>
							<div class="raw" align="center">
							<input name="" type="submit" class="submit_btn" value="Submit" />
							<input name="" type="reset" class="reset_btn" value="Reset" />
							</div>
		<!--end of raw-->

		</div>
				</div>
		  </div>
		  <div class="raw">
		<div class="form_title">Cheque Number</div>
		<div class="form_field">
		<input name="cheque_number" type="text"  class="formtxtfield"/>
		</div>
		  </div>

		<div class="raw">
		<div class="form_title">Value </div>
		<div class="form_field">
			<input name="cheque_amount" type="text"  class="formtxtfield_small"/>
			<span>RO.</span>
		</div>
		</div>
		<div class="raw">
		<div class="form_title">Attachments </div>
		<div class="form_field">
		 <div id="FileUpload">
		 
			<input type="file" name="transaction_attachment" size="24" id="BrowserHidden" onchange="getElementById('FileField').value = getElementById('BrowserHidden').value;" />
		 
			<div id="BrowserVisible"><input type="text" id="FileField" /></div>
		</div>
		</div>
		</div>
		<div class="raw" align="center">
			<input name="submit" type="submit" class="submit_btn" value="Add" />
			<!-- <input name="id" type="hidden"  value="" /> -->
			<input name="" type="reset" class="reset_btn" value="Reset" />
		</div>
		<!--end of raw-->
		</div>
	</form>
<!-- END PAGE -->  
</div>
      <!-- END PAGE -->  
   </div>
</section>
<!-- End Section-->
<!--footer-->
<?php $this->load->view('common/footer');?>
