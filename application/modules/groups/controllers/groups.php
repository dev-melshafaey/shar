<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Groups extends CI_Controller {
    /*
     * Properties
     */

    private $_data = array();

//----------------------------------------------------------------------
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        //load Home Model
        $this->load->model('groups_model', 'groups');
        $this->load->model('users/users_model', 'users');
        $this->lang->load('main', get_set_value('site_lang'));
        $this->_data['udata'] = userinfo_permission();
    }

//----------------------------------------------------------------------

    /*
     * Expensis Listing Page
     */
    public function index() {
        // Load Home View
        //$this->load->view('expenses', $this->_data);
        $this->_data['inside2'] = 'groups';
        $this->_data['inside'] = 'add-groups';
        $this->load->view('common/tabs', $this->_data);
    }

    public function viewall() {

        $this->index();
    }

    function category() {


        $this->_data['inside2'] = 'category';
        $this->_data['inside'] = 'add-expenses-cat';
        $this->load->view('common/tabs', $this->_data);
    }

//----------------------------------------------------------------------

    /*
     * Add New Expensis Form
     */
    function add_new($id = null) {
        if ($id) {
            if ($this->input->post()) {
                $postData = $this->input->post();


                //$data['permissionhead'] = serialize($postData['unit_title']);
                $data['permissionhead'] = $postData['permissionhead'];




                $this->db->where('permission_type_id', $id);
                $this->db->update('bs_permission_type', $data);
                redirect('groups/viewall/' . $id . '/?e=11');


                //print_r($postData);
            }
            //$this->load->view('add-expenses', $this->_data);

            if ($id) {
                $this->_data['hd'] = $this->groups->get_item($id);
                $this->_data['id'] = $id;
            }
            //$this->load->view('add-expenses', $this->_data);
            $this->_data['inside2'] = 'groups';
            $this->_data['inside'] = 'add-groups';
            $this->load->view('common/tabs', $this->_data);
        } else {
            $postData = $this->input->post();


            //$data['unit_title'] = serialize($postData['unit_title']);
            $data['permissionhead'] = $postData['permissionhead'];




            $this->db->insert('bs_permission_type', $data);
            redirect('groups/viewall/?e=10');
        }
    }

    function getAutoSearch() {
        // Data could be pulled from a DB or other source


        $term = trim(strip_tags($_GET['term']));
        $customers = $this->outcome->getExpenseByTitle($term);
        // Cleaning up the term
        // Rudimentary search
        foreach ($customers as $eachloc) {
            // Add the necessary "value" and "label" fields and append to result set
            $eachloc['value'] = $eachloc['id'];
            $eachloc['label'] = "{$eachloc['cus']}";
            $matches[] = $eachloc;
        }
        // Truncate, encode and return the results
        $matches = array_slice($matches, 0, 5);
        print json_encode($matches);
    }

//----------------------------------------------------------------------
}

?>