<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Setting extends CI_Controller {

    /*

     * Properties

     */



    private $_data = array();



//----------------------------------------------------------------------

    /*

     * Constructor

     */

    public function __construct() {

        parent::__construct();



        //load Home Model

        //echo get_set_value('site_lang');

        $this->load->model('setting_model', 'setting');

        $this->lang->load('main', get_set_value('site_lang'));

        //$this->_data['udata'] = userinfo_permission();

    }



//----------------------------------------------------------------------



    /*

     * Home Page

     */

    public function index() {

        // Load Home View

        //$this->load->view('users',$this->_data);

    }



    public function location($location) {

        if ($location != '') {

            $this->_data['hd'] = $this->setting->get_loc_detail($location);

        }

        //$this->load->view('location', $this->_data);

                $this->_data['inside'] = 'location';

        $this->load->view('common/main', $this->_data);

    }



    public function financial_type($ftypeid) {

        if ($ftypeid != '') {

            $this->_data['hd'] = $this->setting->get_type_detail($ftypeid);

        }

        $this->load->view('financial_type', $this->_data);

    }



    public function area($areaid = '') {

        if ($areaid != '') {



            $this->_data['hd'] = $this->setting->get_area_detail($areaid);

        }

        $this->_data['inside'] = 'area';

        $this->load->view('common/main', $this->_data);

        //$this->load->view('area', $this->_data);

    }



    public function banks($bankid = '') {

        if ($bankid != '') {

            $this->_data['hd'] = $this->setting->get_bank_detail($bankid);

        }

        $this->load->view('banks', $this->_data);

    }



    public function delete_loc() {

        $this->setting->delete_locations();

    }



    public function delete_bank() {

        $this->setting->delete_banks();

    }



    public function delete_financial() {

        $this->setting->delete_financials();

    }



    public function delete_area() {

        $this->setting->delete_areas();

    }



    public function add_new() {

        $this->setting->new_location();

    }



    public function add_new_bank() {

        $this->setting->new_bank();

    }



    public function add_new_area() {

        $this->setting->new_area();

    }



    public function add_new_financial() {

        $this->setting->new_financial();

    }



    //Country Section /////////////////////////////////////////Written By HeartDisk		

    public function country($countryid = '') {

        if ($countryid != '') {

            $this->_data['hd'] = $this->setting->get_country_detail($countryid);

        }



        //$this->load->view('country', $this->_data);

        $this->_data['inside'] = 'country';

        $this->load->view('common/main', $this->_data);

    }



    public function add_new_country() {

        $this->setting->new_country();

    }



    public function delete_country() {

        $this->setting->delete_countries();

    }



    //City Section /////////////////////////////////////////Written By HeartDisk		

    public function city($cityid = '') {

        if ($cityid != '') {

            $this->_data['hd'] = $this->setting->get_city_detail($cityid);

        }



        //$this->load->view('city', $this->_data);

        $this->_data['inside'] = 'city';

        $this->load->view('common/main', $this->_data);

    }



    public function add_new_city() {

        $this->setting->new_city();

    }



    public function delete_city() {

        $this->setting->delete_cities();

    }

    function changelang($lang = 'arabic'){
       //echo "<pre>";
        //print_r($_SERVER);
        //exit;
        //echo $_SERVER['HTTP_REFERER'];
        //exit;
        $sitelog = $this->session->userdata('site_lang');
        //echo "<pre>";
        //print_r($sitelog);
        if($sitelog !=""){
            if($sitelog == 'english'){
                $this->session->set_userdata('site_lang','arabic');
            }
            else{
                $this->session->set_userdata('site_lang','english');
            }
        }
        else{
            $this->session->set_userdata('site_lang','arabic');
        }
        if($_SERVER['HTTP_REFERER'] !=""){
            redirect($_SERVER['HTTP_REFERER']);
        }
        else{
            redirect(base_url());
        }


    }
    function general_setting($langu=null){

        //var_dump($_POST);

		if($langu){

			if($langu=='english' || $langu=='arabic'){

			

				//

				$clang=array('site_lang'=>$langu);

				

				

				$this->session->set_userdata($clang);



				$print= serialize(post('options_print'));

				$data=array('site_lang'=>$this->session->userdata($langu),'printtext1'=>  post('printtext1'),'options_print'=>  $print);

				$this->setting->update_general_setting($data);

				$this->config->set_item('language', get_set_value('site_lang'));

				//var_dump($clang);

				//echo $this->session->userdata('site_lang');

				//die(get_set_value('site_lang'));

				

			}else{

				//die($langu);

				/*$lang=array('site_lang'=>'arabic');

				//die(var_dump($lang));

				

				$this->session->set_userdata($lang);



				$print= serialize(post('options_print'));

				$data=array('site_lang'=>$this->session->userdata('arabic'),'printtext1'=>  post('printtext1'),'options_print'=>  $print);

				$this->setting->update_general_setting($data);

				$this->config->set_item('language', get_set_value('site_lang'));*/

				

				//echo 'else';

			}

			//die('get');

			

		}else{

			if ($_POST) {

				if(post('ajax')!=null){

					//die(var_dump($_POST));

                                        if($this->input->post('site_lang')){$chagelang=$this->input->post('site_lang');}else{$chagelang="arabic";}
					$langg=array('site_lang'=>$chagelang);

					//die(var_dump($lang));

					

					$this->session->set_userdata($langg);


                    $is_dharam_static = post('is_dharam_static');
                    $dhrarm_rate = post('dhrarm_rate');
					$print= serialize(post('options_print'));

					$data=array('site_lang'=>$this->session->userdata($langg),'printtext1'=>  post('printtext1'),'options_print'=>  $print,'is_dharam_static'=>$is_dharam_static,'dharm_value'=>$dhrarm_rate);
             //       echo "<pre>";
               //     print_r($data);
                 //   exit;


					$this->setting->update_general_setting($data);

					$this->config->set_item('language', get_set_value('site_lang'));



					

				   

					

					//echo get_set_value('site_lang');

					//do_redirect('setting/general_setting/?e=11');

					//echo 'ajax';

				}else{

                    $p = $this->input->post();
                 //   echo "<pre>";
                    //print_r($p);
                   // $p['site_lang'];


                                        if($this->input->post('site_lang')){$chagelang=$this->input->post('site_lang');}else{$chagelang="arabic";}
					

					$langg2=array('site_lang'=>$chagelang);



                    $is_dharam_static = post('is_dharam_static');
                    $dhrarm_rate = post('dhrarm_rate');

					$this->session->set_userdata($langg2);



					$print= serialize(post('options_print'));

					$data=array('site_lang'=>$this->session->userdata($langg2),'options_print'=>  $print,'is_dharam_static'=>$is_dharam_static,'dharm_value'=>$dhrarm_rate);

                              // echo "<pre>";
                      //   print_r($data);
                       //exit;



                    $this->setting->update_general_setting($data);

					$this->config->set_item('language',get_set_value('site_lang'));



					//echo get_set_value('site_lang');

                                        //echo 'post';

					//do_redirect('setting/general_setting/?e=11');

				}

				

			}

        }

        //echo $this->session->userdata('site_lang');

        //$this->load->view('general_setting', $this->_data); 

        $this->_data['fdata']=$this->setting->get_general_setting();      

		//echo "<pre>";
		//print_r($this->_data['fdata']);
		
        $this->_data['inside'] = 'general_setting';

        $this->load->view('common/main', $this->_data);

        

    }



//----------------------------------------------------------------------

}



