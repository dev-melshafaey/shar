
      <div id="main-content" class="main_content">
                          <div class="row head">
                    <div class="col-md-12">
                        <h4>
                            
                            <div class="title"> <span><?php breadcramb(); ?></span>  </div>
  
                            
                        </h4>
                        <?php error_hander($this->input->get('e')); ?>
                    </div>
                </div>
      <form action="<?php echo form_action_url('add_new'); ?>" method="post" id="frm_location" name="frm_location" autocomplete="off">
        <input type="hidden" name="locationid" id="locationid" value="<?php echo $hd->locationid; ?>" />
        <div class="form">
          <div class="raw form-group">
            <div class="form_title">Location Name</div>
            <div class="form_field">
              <input name="location_name" id="location_name" value="<?php echo $hd->location_name; ?>" type="text"  class="formtxtfield"/>
            </div>
          </div>
          <div class="raw">
            <div class="form_title">Status</div>
            <div class="form_field">
              <div class="dropmenu">
                <div class="styled-select">
                  <?php get_statusdropdown($hd->location_status,'location_status'); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="raw" align="center">
            <input name="sub_mit" id="sub_mit" type="submit" class="submit_btn" value="Add" />
            <input name="sub_reset" type="reset" class="reset_btn" value="Reset" />
          </div>
          <!--end of raw--> 
        </div>
      </form>
      <div class="form">
        <div class="CSSTableGenerator" >
            <?php action_buttons('location',$cnt); ?>
          <table width="100%" align="left" id="new_data_table">
          <form action="<?php echo form_action_url('delete_loc'); ?>" id="listing" method="post" autocomplete="off">
              <thead>
              <tr>
              <th width="1%"><label for="checkbox"></label>
                All</th>
              <th width="20%">Location</th>
              <th width="5%" >Status</th>
              <th width="10%" >Action</th>
            </tr>
            </thead>
            <?php 
			$cnt = 0;
			foreach($this->setting->locationlist() as $cat) { 
				$cnt++;
			?>
            <tr>
              <td ><input type="checkbox" class="allcb" name="u[]" id="u_<?php echo $cat->locationid; ?>" value="<?php echo $cat->locationid; ?>" /></td>
              <td ><?php echo $cat->location_name; ?></td>
              <td><?php echo $cat->location_status; ?></td>
              <td><?php edit_button('location/'.$cat->locationid); ?></td>
            </tr>
            <?php } ?>
            </form>
          </table>
        </div>
      </div>
      <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>
      
      
    </div>
    
    <!-- END PAGE --> 
  

<!-- End Section--> 
<!--footer-->

