<?php $this->load->view('common/meta');?>
<!--body with bg-->

<div class="body">
<header>
  <?php $this->load->view('common/header');?>
  <?php $this->load->view('common/search');?>
  <nav>
    <?php $this->load->view('common/navigations');?>
  </nav>
</header>

<!--Section-->
<section>
  <div id="container" class="row-fluid"> 
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/left-navigations');?>
    <!-- END SIDEBAR --> 
    <!-- BEGIN PAGE -->
    <div id="main-content" class="main_content">
    <?php error_hander($this->input->get('e')); ?>
      <div class="title"> <span>
        <?php breadcramb(); ?>
        </span> </div>
      <form action="<?php echo form_action_url('add_new_bank'); ?>" method="post" id="frm_bank" name="frm_bank" autocomplete="off">
        <input type="hidden" name="bankid" id="bankid" value="<?php echo $hd->bankid; ?>" />
        <div class="form">
          <div class="raw form-group">
            <div class="form_title">Bank Name</div>
            <div class="form_field">
              <input name="bankname" id="bankname" value="<?php echo $hd->bankname; ?>" type="text"  class="formtxtfield"/>
            </div>
          </div>
          <div class="raw">
            <div class="form_title">Status</div>
            <div class="form_field">
              <div class="dropmenu">
                <div class="styled-select">
                  <?php get_statusdropdown($hd->bankstatus,'bankstatus'); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="raw" align="center">
            <input name="sub_mit" id="sub_mit" type="submit" class="submit_btn" value="Add" />
            <input name="sub_reset" type="reset" class="reset_btn" value="Reset" />
          </div>
          <!--end of raw--> 
        </div>
      </form>
      <div class="form">
        <div class="CSSTableGenerator" >
          <table width="100%" align="left" id="printdiv">
          <form action="<?php echo form_action_url('delete_bank'); ?>" id="listing" method="post" autocomplete="off">
            <tr>
              <td width="1%"><label for="checkbox"></label>
                All</td>
              <td width="20%">Bank</td>
              <td width="5%" >Status</td>
              <td width="10%" >Action</td>
            </tr>
            <tr style="background:#446F9C;" >
              <td style="background-color:#afe2ee"><input type="checkbox" name="checkbox" id="checkbox" /></td>
              <td style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
              <td style="background-color:#afe2ee"></td>
              <td style="background-color:#afe2ee"></td>
            </tr>
            <?php 
			$cnt = 0;
			foreach($this->setting->banklist() as $cat) { 
				$cnt++;
			?>
            <tr>
              <td ><input type="checkbox" class="allcb" name="u[]" id="u_<?php echo $cat->bankid; ?>" value="<?php echo $cat->bankid; ?>" /></td>
              <td ><?php echo $cat->bankname; ?></td>
              <td><?php echo $cat->bankstatus; ?></td>
              <td><?php edit_button('banks/'.$cat->bankid); ?></td>
            </tr>
            <?php } ?>
            </form>
          </table>
        </div>
      </div>
      <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>
      <?php action_buttons('banks',$cnt); ?>
      
    </div>
    
    <!-- END PAGE --> 
  </div>
</section>
<!-- End Section--> 
<!--footer-->
<?php $this->load->view('common/footer');?>
