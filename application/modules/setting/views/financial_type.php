<?php $this->load->view('common/meta');?>
<!--body with bg-->

<div class="body">
<header>
  <?php $this->load->view('common/header');?>
  <?php $this->load->view('common/search');?>
  <nav>
    <?php $this->load->view('common/navigations');?>
  </nav>
</header>

<!--Section-->
<section>
  <div id="container" class="row-fluid"> 
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/left-navigations');?>
    <!-- END SIDEBAR --> 
    <!-- BEGIN PAGE -->
    <div id="main-content" class="main_content">
    <?php error_hander($this->input->get('e')); ?>
      <div class="title"> <span>
        <?php breadcramb(); ?>
        </span> </div>
      <form action="<?php echo form_action_url('add_new_financial'); ?>" method="post" id="frm_financial" name="frm_financial" autocomplete="off">
        <input type="hidden" name="ftypeid" id="ftypeid" value="<?php echo $hd->ftypeid; ?>" />
        <div class="form">
          <div class="raw form-group">
            <div class="form_title">Financial Type</div>
            <div class="form_field">
              <input name="financial" id="financial" value="<?php echo $hd->financial; ?>" type="text"  class="formtxtfield"/>
            </div>
          </div>
          <div class="raw">
            <div class="form_title">Status</div>
            <div class="form_field">
              <div class="dropmenu">
                <div class="styled-select">
                  <?php get_statusdropdown($hd->financial_status,'financial_status'); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="raw" align="center">
            <input name="sub_mit" id="sub_mit" type="submit" class="submit_btn" value="Add" />
            <input name="sub_reset" type="reset" class="reset_btn" value="Reset" />
          </div>
          <!--end of raw--> 
        </div>
      </form>
      <div class="form">
        <div class="CSSTableGenerator" >
          <table width="100%" align="left" id="printdiv">
          <form action="<?php echo form_action_url('delete_financial'); ?>" id="listing" method="post" autocomplete="off">
            <tr>
              <td width="1%"><label for="checkbox"></label>
                All</td>
              <td width="20%">Financial Type</td>
              <td width="5%" >Status</td>
              <td width="10%" >Action</td>
            </tr>
            <tr style="background:#446F9C;" >
              <td style="background-color:#afe2ee"><input type="checkbox" name="checkbox" id="checkbox" /></td>
              <td style="background-color:#afe2ee"><input type="text" name="textfield" id="textfield"  class="flex_feild"/></td>
              <td style="background-color:#afe2ee"></td>
              <td style="background-color:#afe2ee"></td>
            </tr>
            <?php 
			$cnt = 0;
			foreach($this->setting->financiallist() as $cat) { 
				$cnt++;
			?>
            <tr>
              <td ><input type="checkbox" class="allcb" name="u[]" id="u_<?php echo $cat->ftypeid; ?>" value="<?php echo $cat->ftypeid; ?>" /></td>
              <td ><?php echo $cat->financial; ?></td>
              <td><?php echo $cat->financial_status; ?></td>
              <td><?php edit_button('financial_type/'.$cat->ftypeid); ?></td>
            </tr>
            <?php } ?>
            </form>
          </table>
        </div>
      </div>
      <div id="tnt_pagination" style="display:none;"> <span class="disabled_tnt_pagination">Prev</span><a href="#1">1</a><a href="#2">2</a><a href="#3">3</a><span class="active_tnt_link">4</span><a href="#5">5</a><a href="#6">6</a><a href="#7">7</a><a href="#8">8</a><a href="#9">9</a><a href="#10">10</a><a href="#forwaed">Next</a></div>
      <?php action_buttons('financial_type',$cnt); ?>
      
    </div>
    
    <!-- END PAGE --> 
  </div>
</section>
<!-- End Section--> 
<!--footer-->
<?php $this->load->view('common/footer');?>
