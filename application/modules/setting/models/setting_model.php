<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setting_model extends CI_Model {
    /*
     * Properties
     */

//----------------------------------------------------------------------

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();

        //Load Table Names from Config
    }

    /////////////////////COUNTRY SECTION/////////////////////////////
    public function new_country() {
        $data = $this->input->post();
        $countryid = $this->input->post('countryid');
        unset($data['sub_mit'], $data['sub_reset'], $data['countryid']);
        if ($countryid != '') {
            $this->db->where('countryid', $countryid);
            $this->db->update($this->config->item('table_country'), $data);
            do_redirect('country?e=11');
        } else {
            $this->db->insert($this->config->item('table_country'), $data);
            do_redirect('country?e=10');
        }
    }

    public function country_list() {
        $q = $this->db->query("SELECT countryid, countryname,countrycode,IF(countrystatus='A','Active','Deactive') AS `countrystatus` FROM " . $this->config->item('table_country') . " Order by countryname ASC");
        return $q->result();
    }

    public function get_country_detail($countryid) {
        $this->db->where('countryid', $countryid);
        $query = $this->db->get($this->config->item('table_country'));
        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function get_general_setting($countryid) {
        //$this->db->where('countryid', $countryid);
        $query = $this->db->get('setting');
        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function delete_countries() {
        $u = $this->input->post('u');
        if ($u != '') {
            $this->db->where_in('countryid', $u);
            $this->db->delete($this->config->item('table_country'));
        }
        do_redirect('country?e=12');
    }

    //////////////////////////////////////////////////////////////////
    /////////////////////CITY SECTION/////////////////////////////
    public function new_city() {
        $data = $this->input->post();
        $cityid = $this->input->post('cityid');
        unset($data['sub_mit'], $data['sub_reset'], $data['cityid']);
        if ($cityid != '') {
            $this->db->where('cityid', $cityid);
            $this->db->update($this->config->item('table_city'), $data);
            do_redirect('city?e=11');
        } else {
            $this->db->insert($this->config->item('table_city'), $data);
            do_redirect('city?e=10');
        }
    }

    public function city_list() {
        $q = $this->db->query("SELECT a.cityid,a.cityname,a.citycode,IF(a.citystatus='A','Active','Deactive') AS `citystatus`,b.countryname,b.countrycode FROM " . $this->config->item('table_city') . " as a, " . $this->config->item('table_country') . " as b WHERE a.countryid=b.countryid  Order by countryname ASC");
        return $q->result();
    }

    public function get_city_detail($cityid) {
        $this->db->where('cityid', $cityid);
        $query = $this->db->get($this->config->item('table_city'));
        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function delete_cities() {
        $u = $this->input->post('u');
        if ($u != '') {
            $this->db->where_in('cityid', $u);
            $this->db->delete($this->config->item('table_city'));
        }
        do_redirect('city?e=12');
    }

    //////////////////////////////////////////////////////////////////

    public function new_location() {
        $data = $this->input->post();
        $locationid = $this->input->post('locationid');
        unset($data['sub_mit'], $data['sub_reset'], $data['locationid']);
        if ($locationid != '') {
            $this->db->where('locationid', $locationid);
            $this->db->update($this->config->item('table_location'), $data);
            do_redirect('location?e=11');
        } else {
            $this->db->insert($this->config->item('table_location'), $data);
            do_redirect('location?e=10');
        }
    }

    public function new_financial() {
        $data = $this->input->post();
        $ftypeid = $this->input->post('ftypeid');
        unset($data['sub_mit'], $data['sub_reset'], $data['ftypeid']);
        if ($ftypeid != '') {
            $this->db->where('ftypeid', $ftypeid);
            $this->db->update($this->config->item('table_financial'), $data);
            do_redirect('financial_type?e=11');
        } else {
            $this->db->insert($this->config->item('table_financial'), $data);
            do_redirect('financial_type?e=10');
        }
    }

    function update_general_setting($data) {
        $this->db->where('is_default', '1');
        $this->db->update('setting', $data);
        return true;
    }

    public function new_area() {
        $data = $this->input->post();
        $areaid = $this->input->post('areaid');
        unset($data['sub_mit'], $data['sub_reset'], $data['areaid']);
        if ($areaid != '') {
            $this->db->where('areaid', $areaid);
            $this->db->update($this->config->item('table_area'), $data);
            do_redirect('area?e=11');
        } else {
            $this->db->insert($this->config->item('table_area'), $data);
            do_redirect('area?e=10');
        }
    }

    public function new_bank() {
        $data = $this->input->post();
        $bankid = $this->input->post('bankid');
        unset($data['sub_mit'], $data['sub_reset'], $data['bankid']);
        if ($bankid != '') {
            $this->db->where('bankid', $bankid);
            $this->db->update($this->config->item('table_banks'), $data);
            do_redirect('banks?e=11');
        } else {
            $this->db->insert($this->config->item('table_banks'), $data);
            do_redirect('banks?e=10');
        }
    }

    public function locationlist() {
        $query = "SELECT locationid, location_name,	IF(location_status='A','Active','Deactive') AS `location_status` FROM " . $this->config->item('table_location') . " Order by location_name ASC";
        $q = $this->db->query($query);
        return $q->result();
    }

    public function banklist() {
        $query = "SELECT bankid, bankname,IF(bankstatus='A','Active','Deactive') AS `bankstatus` FROM " . $this->config->item('table_banks') . " Order by bankname ASC";
        $q = $this->db->query($query);
        return $q->result();
    }

    public function financiallist() {
        $query = "SELECT ftypeid, financial,IF(financial_status='A','Active','Deactive') AS `financial_status` FROM " . $this->config->item('table_financial') . " Order by financial ASC";
        $q = $this->db->query($query);
        return $q->result();
    }

    public function arealist() {
        $q = $this->db->query("SELECT a.`location_name`,b.`areaid`,b.`areaname`,IF(b.`areastatus`='A','Active','Deactive') AS `areastatus` FROM bs_location AS a, bs_area AS b WHERE a.`locationid`=b.`locationid` ORDER BY b.`areaname` ASC;");
        return $q->result();
    }

    public function get_loc_detail($locid) {
        $this->db->where('locationid', $locid);
        $query = $this->db->get($this->config->item('table_location'));

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function get_type_detail($locid) {
        $this->db->where('ftypeid', $locid);
        $query = $this->db->get($this->config->item('table_financial'));

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function get_area_detail($locid) {

        $this->db->where('areaid', $locid);
        $query = $this->db->get($this->config->item('table_area'));

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function get_bank_detail($bankid) {

        $this->db->where('bankid', $bankid);
        $query = $this->db->get($this->config->item('table_banks'));

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function delete_areas() {
        $u = $this->input->post('u');
        if ($u != '') {
            $this->db->where_in('areaid', $u);
            $this->db->delete($this->config->item('table_area'));
        }
        do_redirect('area?e=12');
    }

    public function delete_banks() {
        $u = $this->input->post('u');
        if ($u != '') {
            $this->db->where_in('bankid', $u);
            $this->db->delete($this->config->item('table_banks'));
        }
        do_redirect('banks?e=12');
    }

    public function delete_locations() {
        $u = $this->input->post('u');
        if ($u != '') {
            $this->db->where_in('locationid', $u);
            $this->db->delete($this->config->item('table_location'));
        }
        do_redirect('location?e=12');
    }

    public function delete_financials() {
        $u = $this->input->post('u');
        if ($u != '') {
            $this->db->where_in('ftypeid', $u);
            $this->db->delete($this->config->item('table_financial'));
        }
        do_redirect('financial_type?e=12');
    }

//----------------------------------------------------------------------	
}

?>