
<!--<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" type="text/css"/>-->
<!--<div class=" head">
    <div class="">
        <h4>

            <div class="title"> <span><?php breadcramb(); ?> >> عرض تفاصيل فاتوره</span> </div>


        </h4>
<?php error_hander($this->input->get('e')); ?>
    </div>
</div>-->



<div class="row">
    <div id="wizard" class="box nav-box wizard g16">
        <ul class="nav">
            <li data-nav="#Customer" class="sel"><?php echo lang('Customer') ?> </li>
            <li data-nav="#tproduct"><?php echo lang('tproduct') ?><span class="arrow">(</span></li>
            <li data-nav="#tpay"><?php echo lang('amountpay') ?><span class="arrow">(</span></li>
            <li data-nav="#texpences"> <?php echo lang('texpences') ?><span class="arrow">(</span></li>
            <li data-nav="#tbill" class="tbill"> <?php echo lang('tbill') ?><span class="arrow">(</span></li>
            <li data-nav="#Paymentdoc"> <?php echo lang('Payment-doc') ?><span class="arrow">(</span></li>
            <li data-nav="#payno"> <?php echo lang('Payment-note') ?><span class="arrow">(</span></li>
            <li data-nav="#sms"> Sms<span class="arrow">(</span></li>


        </ul>



        <div id="wizard-body" class="nav-cont vt">

            <div id="Customer" class="nav-item show pad">
                <div class=" form-group" id="customer">
                    <?php //if (get_member_type() == '1' OR get_member_type() == '5'): ?> 

                    <div class="g4 form-group">
                        <label class="text-warning"><?php echo lang('fullname') ?></label>
                        <div><?php echo $invoice[0]->fullname ?></div>

                    </div>

                    <div class="g4 form-group">
                        <label class="text-warning"><?php echo lang('Email-Address') ?></label>
                        <div><?php echo $invoice[0]->email_address ?></div>

                    </div>

                    <div class="g4 form-group">
                        <label class="text-warning"><?php echo lang('Phone') ?></label>
                        <div><?php echo $invoice[0]->phone_number ?></div>

                    </div>
                    <br clear="all"/>
                    <br clear="all"/>
                    <div class="g4 form-group">
                        <label class="text-warning"><?php echo lang('Company-Name') ?> :</label>
                        <br>
                        <div  class="ui-select" style="width:100%">
                            <div class="styled-select " style="width:100%">
                                <?php company_dropbox('companyid', $invoice[0]->companyid, $user->companyid); ?>
                                <span class="arrow arrowselectbox">&amp;</span>
                            </div>
                        </div>
                    </div>
                    <div class="g4 form-group">
                        <div class="field-box">
                            <label class="text-warning"><?php echo lang('Branch-Name') ?> :</label>
                            <br>
                            <div  class="ui-select" style="width:100%">
                                <div class="styled-select " style="width:100%">
                                    <?php company_branch_dropbox('branchid', $invoice[0]->branchid, $user->branchid, $user->companyid); ?>
                                    <span class="arrow arrowselectbox">&amp;</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?PHP //endif; ?>
                    <div class="g3 form-group">
                        <input id="customerid-val" name="customerid-val" type="hidden" value=''/>
                        <input id="customerid" name="customerid" type="hidden" value=''/>
                        <label class="text-warning"><?php echo lang('Customer') ?> :</label>
                        <br>
                        <input id="customername" disabled="disabled" name="customername" value="<?php echo $invoice[0]->fullname ?>" type="text" class="form-control"/>
                    </div>

                    <div class="g3 form-group">
                        <label class="text-warning"><?php echo lang('Date') ?> :</label>
                        <br>
                        <input id=""  name="" value="<?php echo $invoice[0]->invoice_date ?>" type="text" class="datapic_input form-control"/>
                    </div>

                    <br clear="all"/>


                    <script>

                        $(document).ready(function () {



                            $(".get_balance").load(config.BASE_URL + "sales_management/get_balance/" +<?php echo $invoice[0]->customer_id ?>);


                            $.ajax({
                                url: config.BASE_URL + "ajax/branch_list",
                                type: 'post',
                                data: {companyid: "<?php echo $invoice[0]->companyid ?>", branch:<?php echo $invoice[0]->branchid ?>},
                                cache: false,
                                //dataType:"json",
                                success: function (data)
                                {
                                    var response = $.parseJSON(data);

                                    $('#branchid').html(response.dropdown);
                                    $('#storeid').html(response.store);
                                    $('#customerid').html(response.customer);
                                }
                            });





                        });

                    </script>
                    <div class=" get_balance">
                        <div class="g3 green tag"><?php echo lang('balance1') ?> 0</div>
                        <div class="g3 green tag"><?php echo lang('balance2') ?> 0</div>
                        <div class="g3 red tag"><?php echo lang('balance4') ?> 0</div>
                        <div class="g3 red tag"><?php echo lang('balance3') ?> 0</div>
                    </div>
                    <br clear="all"/>








                </div>
                <br clear="all"/>
            </div>

            <div id="tproduct" class="nav-item pad-m">
                <!-- invoice table -->
                <table class="table table-bordered invoice-table mb20 tablesorter" id="table">
                    <thead class="thead">
                    <th><?php echo lang('barcodenumber') ?></th>
                    <th><?php echo lang('Shelf') ?></th>
                    <th><?php echo lang('Image') ?></th>
                    <th><?php echo lang('Product-Name') ?></th>
                    <th><?php echo lang('Discription') ?></th>
                    <th><?php echo lang('sforone') ?></th>
                    <th><?php echo lang('Quantity') ?></th>
                    <th><?php echo lang('Total') ?></th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php $get_product = get_invoice_items($id) ?>
                        <?php if ($get_product): ?>
                            <?php $count = 1 ?>
                            <?php $totalp = 0 ?>
                            <?php foreach ($get_product as $product): ?>
                                <tr id="tr_<?php echo $product->itemid ?>">
                                    <td ><?php echo $product->barcodenumber ?></td>
                                    <td ><?php echo $product->shelf ?></td>
                                    <td ><a href="<?php echo base_url() ?>uploads/item/<?php echo $product->storeid ?>/<?php echo $product->product_picture ?>" class="aimg"><img src="<?php echo base_url() ?>uploads/item/<?php echo $product->storeid ?>/<?php echo $product->product_picture ?>" width="32" height="32" /></a></td>



                                    <td style="text-align:center"> <?php echo _s($product->itemname, get_set_value('site_lang')) ?> </td>
                                    <td id="description" contentEditable="true"><?php echo $product->notes ?> </td>


                                    <td id="oneitem<?php echo $product->itemid ?>" style="text-align:center" contentEditable="true"><?php echo $product->iip ?></td>



                                    <td id="quantity_tp<?php echo $product->itemid ?>" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup=""><?php echo $product->invoice_item_quantity ?> </td>

                                    <td id="totalAmount" style="text-align:center" contentEditable="true" ><?php echo $product->iip ?></td>

                                </tr>
                                <?php //$totalp+=$product->iip; ?>
                                <?php $totaldi+=$product->invoice_item_discount * $product->invoice_item_quantity; ?>
                            <?php endforeach; ?>

                        <?php else: ?>        
                            <tr><td colspan="7"><?php echo lang('no-data') ?></td></tr>
                        <?php endif; ?>
                        <tr  class="grand-total">



                            <td colspan="1" >

                            </td> 
                            <td></td>
                            <td></td>

                            <td><div id="totalmin" style="display: none"></div></td>
                            <td>   


                            </td>

                            <td></td>                  
                            <td><?php echo lang('discount') ?></td>
                            <td><strong id="netTotal"><?php echo $invoice[0]->invoice_totalDiscount + $totaldi; ?></strong></td>   



                        </tr>
                        <tr  class="grand-total">



                            <td colspan="1" >

                            </td> 
                            <td></td>
                            <td></td>

                            <td><div id="totalmin" style="display: none"></div></td>
                            <td>   


                            </td>

                            <td></td>

                            <td><?php echo lang('Total-Price') ?></td>
                            <td><strong id="netTotal"><?php echo $invoice[0]->invoice_total_amount; ?></strong></td>    



                        </tr>
                        <?PHP $get_sales = get_invoice_sales($id); ?>
                        <?php if ($get_sales): ?>
                            <?php foreach ($get_sales as $get_sale): ?>
                                <tr  class="">



                                    <td colspan="1" >

                                    </td> 
                                    <td><?php echo lang('Sales-Name') ?></td>
                                    <td><?php echo $get_sale->fullname ?></td>
                                    <td><?php echo lang('Payment-amount') ?></td>
                                    <td><?php echo $get_sale->amount ?></td>
                                    <td></td>
                                    <td></td>    



                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>

                <!-- #end invoice table -->
            </div>

            <div id="tpay" class="nav-item pad-m">


                <table class="table table-bordered invoice-table mb20 tablesorter" id="table">
                    <thead class="thead">
                        <tr style="">
                            <th>#</th>
                            <th>كيفية الدفع<?php //echo lang('Product-Name')        ?></th>
                            <th>نوع الدفع<?php //echo lang('Product-Name')        ?></th>
                            <th>تاريخ الدفع<?php //echo lang('Total')        ?></th>
                            <th>المبلغ<?php //echo lang('Total')        ?></th>
                            <th>نوع التعامل<?php //echo lang('Total')        ?></th>
                            <th>رقم الايصال او الشيك او الحساب<?php //echo lang('Quantity')        ?></th>
                            <th>البنك<?php //echo lang('Total')        ?></th>
                            <th> مستندات<?php //echo lang('Total')        ?></th>
                            <th> ملاحظات<?php //echo lang('Total')        ?></th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $gpinvoices = get_payment_invoice($id); ?>

                        <?php if ($gpinvoices): ?>

                            <?php foreach ($gpinvoices as $gpinvoice): ?>
                                <tr>

                                    <td></td>
                                    <td><?php echo ($gpinvoice->howpay == '1') ? lang('frombalance') : lang('outbalance'); ?></td>
                                    <td><?php echo ($gpinvoice->iptype == '1') ? lang('Cash') : lang('Bank'); ?></td>
                                    <td><?php echo $gpinvoice->ipcreated; ?></td>
                                    <td><?php echo $gpinvoice->ipamount; ?></td>
                                    <td><?php echo $gpinvoice->ipmethod; ?></td>
                                    <td><?php echo ($gpinvoice->icash == null) ? '' : $gpinvoice->cchequenum; ?></td>
                                    <td><?php echo ($gpinvoice->icash == null) ? '' : $gpinvoice->bank_name . "-" . $gpinvoice->account_name; ?></td>
                                    <td><?php echo ($gpinvoice->icash == null) ? '' : $gpinvoice->cnotes; ?></td>
                                    <td><?php echo ($gpinvoice->icash) ? '' : $gpinvoice->cheque_number; ?></td>

                                </tr>
                            <?php endforeach; ?>    
                        <?php else: ?>        
                            <tr><td colspan="10"><?php echo lang('no-data') ?></td></tr>
                        <?php endif; ?>            
                    </tbody>
                </table>

                <div class='more_add_new_payment_main'> </div>
                <br clear="all"/>
                <a href="javascript:void" onclick='add_new_payment()'  class="btn btn-default"><i class="icon-plus"></i>المزيد<?php //echo lang('Print')       ?></a>

            </div>

            <div id="texpences" class="nav-item pad-m">

                <table class="table table-bordered invoice-table mb20 tablesorter" id="table">
                    <thead class="thead">
                        <tr style="">
                            <th>#</th>
                            <th><?php echo lang('ex_title') ?></th>
                            <th><?php echo lang('ex_category') ?></th>
                            <th><?php echo lang('ex_type') ?></th>
                            <th><?php echo lang('clearance_date') ?></th>
                            <th><?php echo lang('ex_value') ?></th>
                            <th><?php echo lang('Bank') ?></th>
                            <th> <?php echo lang('Notes') ?></th>
                        </tr>
                    </thead>
                    <?php $exinvoices = get_expences_invoice($id, 'sales'); ?>

                    <?php if ($exinvoices): ?>

                        <?php foreach ($exinvoices as $exinvoice): ?>
                            <tr>

                                <td>ex #<?php echo $exinvoice->expense_id ?></td>
                                <td><?php echo $exinvoice->expense_title ?></td>
                                <td><?php echo $exinvoice->cexpense_title ?></td>
                                <td><?php echo $exinvoice->expense_type ?></td>
                                <td><?php echo $exinvoice->expense_date ?></td>
                                <td><?php echo $exinvoice->value ?></td>
                                <td><?php echo $exinvoice->bank_name . "-" . $exinvoice->account_name; ?></td>
                                <td><?php echo $exinvoice->Notes ?></td>



                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>        
                        <tr><td colspan="8"><?php echo lang('no-data') ?></td></tr>
                    <?php endif; ?>  
                    <tbody>

                    </tbody>
                </table>

                <form  action="<?php echo base_url() ?>sales_management/add_expences" method="post">
                    <div class='more_add_new_expences_main'> </div>

                    <input type="hidden" name="invoice_id" value="<?php echo $id ?>" class="btn-flat primary"/>
                    <input type="submit" name="" value="<?php echo lang('save') ?>" class="btn-flat primary left g3 green"/>
                </form>
                <br clear="all"/>
                <a href="javascript:void" onclick='add_new_expences()' class="btn btn-default" style=""><i class="icon-plus"></i><?php echo lang('more') ?></a> 
                <br clear="all"/>
            </div>



            <!--Four tabs-->
            <div id="tbill" class="nav-item pad-m">
                <form action="<?php echo base_url() ?>sales_management/updateInvoice/<?php echo $id ?>" method="post">
                    <div class="">
                        <!--                <div class="g5 divsale_direct_store" id="" style="">
                                            <div class="col-lg-4">
                        <?php echo lang('option_1') ?>
                                            </div>
                                            <div class=" col-lg-1">
                                                <input name="sale_direct_store"  type="checkbox"  value='1'  class=" form-control" style="width: 20px;height: 20px;"/>
                                            </div>
                                        </div>-->
                        <!--                                    <div class="g5" id="" style="">
                                                                <div class="col-lg-4">
                        <?php echo lang('option_2') ?>
                                                                </div>
                                                                <div class=" col-lg-1">
                                                                    <input name="print_option_pt"  type="checkbox"  value='1'  class=" form-control" style="width: 20px;height: 20px;"/>
                                                                </div>
                                                            </div>-->
                        <div class="g5" id="" style="">
                            <div class="col-lg-4">
                                <?php echo lang('option_3') ?>
                            </div>
                            <div class=" col-lg-1">
                                <input name="print_option_desc" id="print_option_desc" <?php echo DoSelect('1', $invoice[0]->print_option_desc, 'checked') ?>  type="checkbox"  value='1'  class=" form-control" style="width: 20px;height: 20px;"/>
                            </div>
                        </div>
                        <div class="g5" id="" style="">
                            <div class="col-lg-4">
                                <?php echo lang('option_4') ?>
                            </div>
                            <div class=" col-lg-1">
                                <input name="print_option_note" id="print_option_note" <?php echo DoSelect('1', $invoice[0]->print_option_note, 'checked') ?> type="checkbox"  value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                            </div>
                        </div>
                        <div class="g5" id="" style="">
                            <div class="col-lg-4">
                                <?php echo lang('option_5') ?>
                            </div>
                            <div class=" col-lg-1">
                                <input name="print_option_netprice" id="print_option_netprice" <?php echo DoSelect('1', $invoice[0]->print_option_netprice, 'checked') ?> type="checkbox"  value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                            </div>
                        </div>
                        <div class="g5" id="" style="">
                            <div class="col-lg-4">
                                <?php echo lang('option_6') ?>
                            </div>
                            <div class=" col-lg-1">
                                <input name="print_option_dicount" id="print_option_dicount" <?php echo DoSelect('1', $invoice[0]->print_option_dicount, 'checked') ?>  type="checkbox"   value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                            </div>
                        </div>
                        <div class="g5" id="" style="">
                            <div class="col-lg-4">
                                <?php echo lang('option_7') ?>
                            </div>
                            <div class=" col-lg-1">
                                <input name="print_option_total" id="print_option_total" <?php echo DoSelect('1', $invoice[0]->print_option_total, 'checked') ?>  type="checkbox" value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                            </div>
                        </div>
                        <div class="g5" id="" style="">
                            <div class="col-lg-4">
                                <?php echo lang('option_8') ?>
                            </div>
                            <div class=" col-lg-1">
                                <input name="print_option_pay" id="print_option_pay" <?php echo DoSelect('1', $invoice[0]->print_option_pay, 'checked') ?>   type="checkbox" value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                            </div>
                        </div>
                        <div class="g5" id="" style="">
                            <div class="col-lg-4">
                                <?php echo lang('option_9') ?>
                            </div>
                            <div class=" col-lg-1">
                                <input name="print_option_total_pay" id="print_option_total_pay" <?php echo DoSelect('1', $invoice[0]->print_option_total_pay, 'checked') ?>  type="checkbox" value='1'  class=" form-control"  style="width: 20px;height: 20px;"/>
                            </div>
                        </div>

                        <?php $get_setting_option = unserialize(get_setting_option()->options_print); ?>



                        <?php //$get_setting_option = unserialize($invoice[0]->options_print_pages); ?>
                        <?php //var_dump($get_setting_option)?>
                        <?php if ($get_setting_option['name']): ?>
                            <?php $count = 0 ?>
                            <?php foreach ($get_setting_option['name'] as $get_setting_option_row): ?>
                                <div class="g5" id="" style="">
                                    <div class="col-lg-4">
                                        <?php echo $get_setting_option['name'][$count] ?>
                                    </div>
                                    <div class=" col-lg-1">
                                        <input name="options_print_pages[name][]"    type="checkbox" value='<?php echo $get_setting_option['name'][$count] ?>'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                        <input name="options_print_pages[text][]"  type="hidden" value='<?php echo $get_setting_option['text'][$count] ?>'  class=" form-control"  style="width: 20px;height: 20px;"/>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                        <?php endif; ?>


                    </div>
                    <br clear="all"/>
                    <div class="g2" align="left"> 
                        <!--<input name="" type="submit" class="submit_btn btn-glow primary " value="Add"  onclick=""/>-->
                        <!--<input name="" type="button" class="submit wide flt-r mod-tg" value="اضافة الفاتوره"  onclick="addInvoice()"/>-->
                        <!--<input name="" type="reset" class="reset_btn btn-glow primary" value="Reset" />-->
                    </div>
                    <br clear="all"/>
                    <br clear="all"/>
                    <br clear="all"/>
                    <br clear="all"/>
                    <div class="nav-act btn-m">
                        <!--<button class="">Back</button>-->
                        <!--<a href="javascript:void" style="" class="gotobutn mini prev  tag" onclick="gotonext('texpences', 'tbill')"><< <?php echo lang('back') ?>  </a>-->
                        <button name="" type="submit" class="submit_btn btn-glow primary"   /><?php echo lang('update') ?></button>
                    </div>
                </form>
            </div>





            <div id="Paymentdoc" class="nav-item pad-m">

                <table class="table table-bordered invoice-table mb20 tablesorter" id="table">
                    <thead class="thead">
                        <tr style="">
                            <th>#</th>
                            <th><?php echo lang('files') ?></th>
                            <th><?php echo lang('Date') ?></th>
                        </tr>
                    </thead>

                    <tbody id="getfiles">
                        <?php $d_invoices = $this->sales_management->get_files($id); ?>
                        <?php if ($d_invoices): ?>
                            <?php foreach ($d_invoices as $d_invoice): ?>

                                <tr>
                                    <td><?php echo $d_invoice->ido_id ?></td>
                                    <td><a href="<?php echo base_url() ?>/uploads/invoicefiles/<?php echo $d_invoice->ido_file ?>"><?php echo $d_invoice->ido_file ?></td>
                                    <td><?php echo $d_invoice->ido_date ?></td>
                                </tr>

                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="4"><?php echo lang('no-data') ?></td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>

            <!-- <form  action="<?php echo base_url() ?>sales_management/add_files" method="post" enctype="multipart/form-data">
                        <div class='more_add_new_files_main'> </div>

             </form>            -->


            <script src="<?php echo base_url() ?>js/uploading/jquery.uploadify.min.js" type="text/javascript"></script>
            <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>js/uploading/uploadify.css">

                <form>
                    <div id="queue"></div>
                    <input id="file_upload" name="file_upload" type="file" multiple="true">
                </form>

                <script type="text/javascript">
                            <?php $timestamp = time(); ?>
                                        $(function () {
                                            $('#file_upload').uploadify({
                                                'formData': {
                                                    'invoi_id': '<?php echo $id; ?>',
                                                    'timestamp': '<?php echo $timestamp; ?>',
                                                    'token': '<?php echo md5('unique_salt' . $timestamp); ?>'
                                                },
                                                'swf': '<?php echo base_url() ?>js/uploading/uploadify.swf',
                                                'uploader': '<?php echo base_url() ?>sales_management/uploadify',
                                                'itemTemplate': '<div id="${fileID}" class="uploadify-queue-item">\
                                                                <div class="cancel">\
                                                                    <a href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')">X</a>\
                                                                </div>\
                                                                <span class="fileName">${fileName} (${fileSize})</span><span class="data"></span>\
                                                            </div>',
                                                'onUploadComplete': function (file) {
                                                    //alert('The file ' + file.name + ' finished processing.');
                    
                                                    $("#getfiles").load(config.BASE_URL + "sales_management/getfiles/<?php echo $id ?>");
                    
                                                },
                                                'onUploadError': function (file, errorCode, errorMsg, errorString) {
                                                    alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
                                                }
                                            });
                    
                    
                                        });
                                        function onUploadProgress() {
                    
                                            alert('sdsd');
                    
                                        }
                </script>


                <br clear="all"/>
                <!--<a href="javascript:void" onclick='add_new_files()'class="btn btn-default"><i class="icon-plus"></i>المزيد<?php //echo lang('Print')       ?></a>-->
            </div>

            <div id="payno" class="nav-item pad-m">

                <table class="table table-bordered invoice-table mb20 tablesorter" id="table">
                    <thead class="thead">
                        <tr style="">
                            <th>#</th>
                            <th> <?php echo lang('title')        ?></th>
                            <th> <?php echo lang('Notes')        ?></th>
                            <th><?php echo lang('Date')        ?></th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php if ($invoice[0]->in_id): ?>
                            <?php foreach ($invoice as $n_invoice): ?>
                                <tr>
                                    <td><?php echo $n_invoice->in_id ?></td>
                                    <td><?php echo $n_invoice->in_title ?></td>
                                    <td><?php echo $n_invoice->in_notes ?></td>
                                    <td><?php echo $n_invoice->in_date ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="4"><?php echo lang('no-data') ?></td>
                            </tr>
                        <?php endif; ?>

                    </tbody>
                </table>
                <form  action="<?php echo base_url() ?>sales_management/add_note" method="post">
                    <div class='more_add_new_notes_main'> </div>
                    <input type="hidden" name="invoice_id" value="<?php echo $id ?>" class="btn-flat primary"/>
                    <input type="submit" name="" value="<?php echo lang('save')?>" class="btn-flat primary left g3 green"/>
                </form>
                <br clear="all"/>
                <a href="javascript:void" onclick='add_new_notes()'class="btn btn-default"><i class="icon-plus"></i><?php echo lang('more')       ?></a>
            </div>

            <div id="sms" class="nav-item pad-m">

                <table class="table table-bordered invoice-table mb20 tablesorter" id="table">
                       <thead class="thead">
                        <tr style="">
                            <th>#</th>
                            <th><?php echo lang('title')        ?></th>
                            <th><?php echo lang('message')        ?></th>
                            <th> <?php echo lang('nummessage')        ?></th>

                        </tr>
                    </thead>

                    <tbody>
                        <?php //$total = 0 ?>
                        <?php //foreach ($p_invoices as $p_invoice): ?>
                           <!-- <tr>
                                <td><?php echo $p_invoice->itemid ?></td>
                                <td><?php echo $p_invoice->itemname ?></td>
                                <td><?php echo $p_invoice->invoice_item_quantity ?></td>
                                <td><?php echo $p_invoice->invoice_item_price ?>   OM</td>
    
                        <?php //$total+=$p_invoice->invoice_item_price ?>
                            </tr>-->
                        <?php //endforeach; ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                        </tr>
                    </tbody>
                </table>
                <a href="#" class="btn btn-default"><i class="ion-email"></i><?php echo lang('more')       ?></a>
            </div>



        </div>    


    </div>    

</div>
<br clear="all"/>
<br clear="all"/>
<a href="<?php echo base_url() ?>sales_management/printed/<?php echo $id ?>" class="btn btn-default"><i class="icon-print"></i><?php echo lang('Print') ?></a>
<br clear="all"/>
<br clear="all"/>

<!--<script type="text/javascript" src="<?php echo base_url(); ?>js/function.js" ></script>--> 


<!--------->

<div class='hide_form_payment' style="display:none">

<!-- <script type="text/javascript" src="jquery/ui/ui.core.js"></script>
 <script type="text/javascript" src="jquery/ui/ui.datepicker.js"></script>-->
    <script>
        //$('.datapic_input').datepicker({dateFormat: 'yy-mm-dd'});
        //$(".datapic_input_inside").datepick({dateFormat: 'yyyy-mm-dd'});
    </script>


    <div class=" form-group" style="" id="">
        <label class="text-warning"><?php echo lang('howpay') ?> :</label><br>
        <div class="ui-select"  style="width:100%">                       
            <select class="validate[required]" name="payment[howpay][]" onchange="">
                <option value="" selected="selected" ><?php echo lang('choose') ?> </option>
                <option value="1"><?php echo lang('frombalance') ?></option>
                <option value="2"><?php echo lang('outbalance') ?></option>

            </select>                            
        </div>

    </div>
    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('TypePayment') ?> </label><br>
        <div class="ui-select styled-select2"  style="width:100%">
            <select name="payment[p_type][]" class="" onchange="" payment='5' class='1'>
                <option value="" selected="selected" ><?php echo lang('choose') ?> </option>
                <option value="1"><?php echo lang('Cash') ?></option>
                <option value="2"><?php echo lang('Bank') ?></option>
            </select>
        </div>

    </div>
    <div class="g4 form-group ">
        <label class="text-warning"><?php echo lang('Payment-date') ?></label><br>

        <div class="viewdate"></div>

    </div>
    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('Payment-amount') ?></label><br>

        <input name="payment[p_amount][]"  type="text" id="payment_amountt" onchange="do_payment_amountt(this.value)" value=""  class=" form-control"/>

    </div>



    <div class="g4 form-group" id="div_banks" style="display:none;">
        <label class=""><?php echo lang('Bank'); ?> </label>


        <div class="ui-select styled-select3" style="width:100%">
            <?php company_bank_dropbox('payment[p_bank][]', '', 'english', '', 'arr_bank'); ?>
        </div>


    </div>

    <div class="g4 form-group" style="display:none" id="div_accounts">
        <label class=""><?php echo lang('Accounts') ?> </label>


        <div class="ui-select" id="div_accounts_responce" style="width:100%">

        </div>


    </div>

    <div class="g4 form-group" style="display:none" id="div_TypePayment2">
        <label class="text-warning"><?php echo lang('TypePayment2') ?> :</label><br>
        <div class="ui-select"  style="width:100%">                       
            <select class="" name="payment[p_type2][]" onchange="showPaymentLabel(this.value, 0)">
                <option value="" selected="selected" ><?php echo lang('choose') ?> </option>
                <option value="cheque"><?php echo lang('Cheque') ?></option>
                <option value="transfer"><?php echo lang('Transfer') ?></option>
                <option value="deposite"><?php echo lang('Deposite') ?></option>
                <option value="withdraw"><?php echo lang('withdraw') ?></option>
                <option value="later"><?php echo lang('later') ?></option>
            </select>                            
        </div>

    </div>

    <div class="g4 form-group" style="display: none" id="div_pdoc">
        <label class="text-warning"><?php echo lang('Payment-doc') ?> :</label><br>
        <input name="payment[p_doc][]"  type="file"  class=" form-control"/>
    </div>   

    <div class="g4 form-group" style="display: none" id="div_pnumper">
        <label class="text-warning"><?php echo lang('Number') ?> :</label><br>
        <input name="payment[p_numper][]"  type="text"  class=" form-control"/>
    </div>   

    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('Payment-note') ?> :</label><br><textarea name='payment[p_note][]' class=" form-control"></textarea>
    </div>



</div>

<div class="hide_form_notes" style="display:none">

    <div class="g4 form-group" style="" id="div_pnumper">
        <label class="text-warning">عنوان<?php //echo lang('Number')     ?> :</label><br>
        <input name="notes[title][]"  type="text"  class=" form-control"/>
    </div>   

    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('Payment-note') ?> :</label><br><textarea name='notes[note][]' class=" form-control"></textarea>
    </div> 

</div>





<div class='hide_form_expences' style="display:none"> 
    <hr/>
    <script>
        //$('.datapic_input').datepicker({dateFormat: 'yy-mm-dd'});
        //$(".datapic_input_inside2").datepick({dateFormat: 'yyyy-mm-dd'});
    </script>


    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('expense_title') ?></label>
        <div class="">
            <input name="expense[expense_title][]" id="" type="text"  value="" class="form-control"/>
        </div>
    </div>

    <input type="hidden" name="type" id="type" value="indirect" />  


    <div class="g4 form-group">
        <label class="text-warning"><?php echo lang('value') ?></label>
        <div class="">
            <input name="expense[value][]" value="" id="value" type="text"  class="form-control"/>

        </div>
    </div>

    <div class="g3 form-group" id="div_banks">
        <label class="text-warning"><?php echo lang('cat') ?> </label>
        <div class="">
            <div class="ui-select" style="width:100%">
                <div class="">
                    <?php expense_charges2('expense[expense_charges_id][]', $exdata->expense_charges_id, 'english', ''); ?>
                    <span class="arrow arrowselectbox">&amp;</span>
                </div>
            </div>
        </div>
    </div>

    <div class="g4 form-group">
        <label class="col-lg-12 text-warning"><?php echo lang('clearanceDate') ?></label>
        <div class="viewdate_ex"></div>

    </div>



    <input type="hidden" id="expense_type1" class="expense_period" value="direct" name="expense[expense_type][]">



    <br clear="all"    />





    <div class="g4 form-group" id="div_banks">
        <label class="text-warning"><?php echo lang('Bank') ?> </label>
        <div class="">
            <div class="ui-select" style="width:100%">
                <div class="styled-select3" style="width:100%">
                    <?php company_bank_dropbox('expense[bank_id_ex][]', '', 'english', ' ', 'bank_id_ex'); ?>
                    <span class="arrow arrowselectbox">&amp;</span>
                </div>
            </div>
        </div>
    </div>

    <div class="g4 form-group" style="" id="div_accounts">
        <label class="text-warning"><?php echo lang('Accounts') ?> </label>  
        <div class="styled-select" id="div_accounts_responce_ex" style="width:100%">

            <select id="div_accounts_responce" class="div_accounts_responce_ex required  valid " name="account_id" onchange="loadtranserBankaccounts();" >
            </select>
            <span class="arrow arrowselectbox">&amp;</span>
        </div>
    </div>

    <div class="g4 form-group" id="div_checkque" style="display:none;">
        <label class="text-warning">Deposite Recipt </label>
        <div class="">
            <div class="ui-select" >
                <div class="styled-select">
                    <input type="expense[file][]"  class="form-control" name="deposite_recipt" id="deposite_recipt" />
                </div>
            </div>
        </div>
    </div>




    <!--                            <div class="g4 form-group" style="display:none" id="div_tr_accounts">
                                    <label class="text-warning">Accounts </label>
                                    <div class="">
                                        <div class="ui-select" >
                                            <div class="styled-select" id="div_tr_accounts_responce">
    
                                            </div>
                                        </div>
                                    </div>
                                </div>  -->
    <br clear="all"/>
    <div class="g14 form-group">
        <label class="text-warning"><?php echo lang('Notes') ?></label>
        <div class="">
            <textarea name="expense[Notes][]" cols="" rows="" class="form-control"></textarea>

        </div>
    </div> 
</div>



<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->

<!--<link rel="stylesheet" type="text/css" href="http://keith-wood.name/css/jquery.datepick.css"> 
<script type="text/javascript" src="http://keith-wood.name/js/jquery.plugin.js"></script> 
<script type="text/javascript" src="http://keith-wood.name/js/jquery.datepick.js"></script>-->

<script type="text/javascript">
    $(function () {


        $(".expense_period").click(function () {
            //alert('asdasd');

            //checked_status = $(this).is(':checked');
            expense_period_val = $(this).val();

            //alert(checked_status);
            if (expense_period_val == 'fixed') {

                $("#period_parent").show();
            }
            else {
                $("#period_parent").hide();
            }
        });
    });
    $(document).ready(function () {
        //alert('ready');
        var ac_config = {
            source: "<?php echo base_url(); ?>outcome/getAutoSearch",
            select: function (event, ui) {
                $("#expense_title").val(ui.item.cus);
                $("#expense_id").val(ui.item.cus);
                console.log(ui);
                //swapme();
                setTimeout('swapgeneral()', 500);
            },
            minLength: 1
        };

        $("#expense_title").autocomplete(ac_config);


    });



</script>