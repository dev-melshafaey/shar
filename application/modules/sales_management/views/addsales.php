<link href="<?php echo base_url()?>js/2select/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url()?>js/2select/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
    });
</script>

<div class="row form-wrapper">
  <div class="col-md-12">
    <h4>
      <div class="title"> <span><?php echo lang('addsales') ?></span> </div>
    </h4>
    <div class="col-md-12 form-group mycontent" style="padding:5px 7px !important;">
      <div class="col-md-12">
        <?php error_hander($this->input->get('e')); ?>
        <br clear="all">
      </div>
      <form action="<?php echo base_url(); ?>sales_management/addsales" method="post" id="form1" class="" name="frm_customer" autocomplete="off">
        <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
        <input type="hidden" name="userid" id="userid" value="<?php echo $sdata->bs_sales_id; ?>" />
        <input type="hidden" name="member_type" id="member_type" value="6">
        <div class="col-md-3">
          <label><?php echo lang('userlist') ?> :</label>
          <div class="styled-select"  style="width: 100%">
            <select name="bs_user_id">
              <?php if($users):?>
              <?php foreach($users as $user):?>
              <option value='<?php echo $user->userid?>' <?php echo DoSelect($user->userid,$sdata->bs_user_id )?>><?php echo $user->fullname?></option>
              <?php endforeach;?>
              <?php endif;?>
            </select>
          </div>
        </div>
        <!------------------>
        <div class="col-md-3">
          <label><?php echo lang('fromwhat') ?> :</label>
          <div class="styled-select"   style="width: 100%">
            <select name='bs_sales_commession_how' class="validate[required]">
              <option value='1' <?php echo DoSelect(1,$sdata->bs_sales_commession_how )?>><?php echo lang('fromtotal')?></option>
              <option value='0' <?php echo DoSelect(0,$sdata->bs_sales_commession_how )?>><?php echo lang('fromnetprice')?></option>
            </select>
          </div>
        </div>
        <!------------------->
        <div class="col-md-3">
          <label><?php echo lang('typecommession') ?> :</label>
          <div class="styled-select"  style="width: 100%">
            <select name='bs_sales_commession_type' class="validate[required]">
              <option value='1' <?php echo DoSelect(1,$sdata->bs_sales_commession_type)?>><?php echo lang('percent')?> </option>
              <option value='0' <?php echo DoSelect(0,$sdata->bs_sales_commession_type)?>><?php echo lang('amount')?> </option>
            </select>
          </div>
        </div>
        <!------------------->
        <div class="col-md-3 ">
          <label><?php echo lang('commession') ?> :</label>
          <input name="bs_sales_commession" id="bs_sales_commession" value="<?php echo $sdata->bs_sales_commession; ?>" type="text"  class="validate[required] form-control"/>
        </div>
        <br clear="all">
        <div class="col-md-12" style="margin:5px 0px;">
          <input name="sub_mit" id="sub_mit" type="submit" class="btn-glow primary" value="<?php echo lang('Add') ?>" />
          <input name="sub_reset" type="reset" class="btn-glow primary" value="<?php echo lang('Reset') ?>" />
          <br clear="all">
        </div>
      </form>
      <br clear="all">
    </div>
  </div>
  <!-- END PAGE --> 
</div>
