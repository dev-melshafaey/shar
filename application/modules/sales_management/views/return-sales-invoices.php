

<div class="table-wrapper users-table">
    <form action="<?php echo form_action_url('delete_customers'); ?>" id="listing" method="post" autocomplete="off">


        <?php error_hander($this->input->get('e')); ?>



        <div class="row">
            <div class="">

                <table id="new_data_table" style="text-align: center">
                    <thead class="thead">
                        <tr>
                            <th id="no_filter"><label for="checkbox"></label></th>
                            <th ><?php echo lang('Invoice-Number') ?></th>
                            <th ><?php echo lang('offer-Number') ?></th>
                            <th style="width:250px"><?php echo lang('Customer') ?></th>
                            <th ><?php echo lang('Phone') ?></th>
                            <th ><?php echo lang('Total-Price') ?></th>
                            <th ><?php echo lang('discountamount') ?></th>
                            <th ><?php echo lang('typepay') ?></th>
                            <th ><?php echo lang('amountpay') ?></th>
                            <th ><?php echo lang('remain') ?></th>
                            <th class="divNetPrice"><?php echo lang('NetPrice') ?></th>
                            <!--<th ><?php echo lang('saleprice') ?></th>-->
                            <th ><?php echo lang('expences') ?></th>
                            <!--<th ><?php echo lang('Statusinvoice') ?></th>-->
                            <th ><?php echo lang('salesperson') ?></th>
                            <th ><?php echo lang('salesperson_percent') ?></th>
                            <th ><?php echo lang('Date') ?></th>
                            <th id="no_filter" width="7%">&nbsp;</th>
                        </tr>
                    </thead>

                    <?php
                    $cnt = 0;
                    //print_r($sale_invoices[0]->fullname);
                    foreach ($sale_invoices as $sdata) {
                        $cnt++;
                        ?>
                        <tr>
                            <td ><input type="checkbox" class="allcb" name="ids[]" id="u_<?php echo $sdata->in_id ?>" value="<?php echo $sdata->in_id; ?>" /></td>
                            <td>in#<?php echo $sdata->in_id; ?></td>
                            <td><?php if ($sdata->qid): ?><a href="<?php echo base_url() . "quotation/view/" . $sdata->qid; ?>">of#<?php echo $sdata->qid; ?></a><?php endif; ?></td>
                            <td style="width:250px;text-align: right"><a href="<?php echo base_url(); ?>customers/statics/<?php echo $sdata->userid; ?>"><?php echo $sdata->fullname; ?></a></td>
                            <td><?php echo $sdata->phone_number ?></td>
                            <td>
                                <?php //echo $sdata->discount_type?>
                                <?php //if($sdata->discount_type=='1'):?>
                                <?php //echo $sdata->iiprice-(($sdata->iiprice*$sdata->iidiscount)/100)?>
                                
                                <?php //else:?>
                                <?php //echo $sdata->iiprice-$sdata->iidiscount; ?>
                                <?php //endif;?>
                                <?php //echo $sdata->sum_invoicediscount?>
                                <?php echo $sdata->sum_invoiceprice+$sdata->sum_invoicediscount ?>
                            </td>
                            <td>
                                <?php //if ($sdata->sum_invoicediscount): ?>
                                <?php //echo ($sdata->sum_invoicediscount==0.000) ?  $sdata->alldiscount : $sdata->sum_invoicediscount ;?>
                                <?php //echo $sdata->sum_invoicediscount?>
                                <?php echo $sdata->alldiscount?>
                                    
                                <?php //else: ?>
                                    
                                <?php //endif; ?>

                            </td>

                            <td><?php if($sdata->payment_type && $sdata->payment_type=='2' ): echo lang('Cash');
                                      elseif($sdata->payment_type && $sdata->payment_type=='1'):  echo lang('Bank');
                                      else: echo '---';
                                      endif;
                                ?></td>


                            <td class="green"><?php echo ($sdata->pamount) ? $sdata->pamount : '0'; ?></td>

                            <!--<td class="red"><?php echo ($sdata->remaining - $sdata->iidiscount) ? $sdata->remaining - $sdata->iidiscount : '0'; ?></td>-->
                            <td class="red">
                                                <?php //echo $sdata->remaining; ?>
                                                <?php if ($sdata->remaining==NULL) : ?>
                                                    <?php if ($sdata->invoice_total_amount - $sdata->sales_amount!=0) : 
                                                        //echo $sdata->invoice_total_amount - $sdata->sales_amount;
                                                     else:
                                                        echo $sdata->invoice_total_amount;
                                                      endif;
                                                elseif($sdata->remaining!=''): 
                                                    echo $sdata->remaining+$sdata->sum_invoicediscount;
                                                elseif($sdata->remaining=='0.000'): 
                                                    echo $sdata->invoice_total_amount - $sdata->sales_amount;
                                                endif; ?>
                            </td>

                            <td class="divNetPrice">
                                <?php //echo $sdata->sum_invoiceprice-($sdata->sum_invoicediscount+$sdata->alldiscount) ?>
                                <?php //echo $sdata->sum_invoiceprice+$sdata->sum_invoicediscount?>
                                <?php echo ($sdata->sum_invoiceprice)-$sdata->sumipurchase?>
                            </td>

                            <td ><?php echo $sdata->sumexvalue; ?></td>

                            <td><?php $salesvalue = explode(',', $sdata->cols); ?>
                                <?php if ($salesvalue): ?>
                                    <?php $count = array_unique($salesvalue) ?>
                                    <?php foreach ($salesvalue as $skey => $svalue): ?>
                                        <?php if ($count[$skey]): ?>
                                            <a onclike="<?php echo $count[$skey] ?>" data-toggle="modal" href="#myModal"><?php echo $count[$skey] ?></a><br/>
                                        <?php endif; ?>    
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </td>
                            <td><?php echo $sdata->salescommession; ?></td>
                            
                            <td><?php echo date('d/m/Y', strtotime($sdata->i_date)); ?></td>





                            <td>
                                <a href="<?php echo base_url() ?>sales_management/editinvoice/<?php echo $sdata->in_id ?>"><i class="icon-pencil"></i></a> 
                                <a href="<?php echo base_url() ?>sales_management/viewp/<?php echo $sdata->in_id ?>"><i class="icon-search"></i></a> 
                                <a target="_new" href="<?php echo base_url() ?>sales_management/printed/<?php echo $sdata->in_id ?>"><i class="icon-print"></i></a> 

                                <?php //edit_button('addnewcustomer/'.$userdata->userid);   ?>
    <!--<a id="<?php //echo $userdata->userid;    ?>" href='#' class='basic'><img src="<?php echo base_url(); ?>images/internal/i_info.png" width="28" height="18" border="0" /></a>--> 

                                <!-- modal content -->

                                <div id="basic-modal-content" class="dig<?php echo $userdata->userid; ?>" style="display: none">
                                    <h3><?php echo $userdata->fullname; ?><br />
                                        Code : #<?php echo $userdata->employeecode; ?></h3>

                                    <code> <span class="pop_title"><?php echo lang('Mobile-Number'); ?> </span>
                                        <div class="pop_txt"><?php echo $userdata->phone_number; ?></div>
                                    </code> 
                                    <!--line--> 
                                    <code> <span class="pop_title"><?php echo lang('Contact-Number'); ?>  </span>
                                        <div class="pop_txt"><?php echo $userdata->office_number; ?></div>
                                    </code> 
                                    <!--line--> 
                                    <code> <span class="pop_title"><?php echo lang('Fax-Number'); ?> </span>
                                        <div class="pop_txt"><?php echo $userdata->fax_number; ?></div>
                                    </code>
                                    <!--line--> 
                                    <code> <span class="pop_title"><?php echo lang('Email-Address'); ?> </span>
                                        <div class="pop_txt"><?php echo $userdata->email_address; ?></div>
                                    </code> 
                                    <!--line--> 
                                    <code> <span class="pop_title"><?php echo lang('Address'); ?></span>
                                        <div class="pop_txt"><?php echo $userdata->address; ?></div>
                                    </code> 
                                    <!--line--> 
                                    <code> <span class="pop_title"><?php echo lang('Type'); ?></span>
                                        <div class="pop_txt"><?php echo $userdata->permissionhead; ?></div>
                                    </code> 
                                    <code> <span class="pop_title"><?php echo lang('Responsable-Name'); ?></span>
                                        <div class="pop_txt"><?php echo($userdata->responsable_name); ?></div>
                                    </code>
                                    <code> <span class="pop_title"><?php echo lang('Responsable-Phone'); ?></span>
                                        <div class="pop_txt"><?php echo($userdata->responsable_phone); ?></div>
                                    </code>
                                    <code> <span class="pop_title"><?php echo lang('Notes'); ?></span>
                                        <div class="pop_txt"><?php echo($userdata->notes); ?></div>
                                    </code>
                                    <!--line--> 
                                    <code> <span class="pop_title"><?php echo lang('Status'); ?></span>
                                        <div class="pop_txt"><?php echo($userdata->status); ?></div>
                                    </code> 
                                <?php } ?>
                </table>
            </div>
        </div>
</div>
<script>
    $(document).ready(function () {
            searchabout_permession('divNetPrice');
            //searchabout_permession('divsale_direct_store');
    });

</script>