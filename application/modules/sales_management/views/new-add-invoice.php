<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>POS System</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>pos_assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Reset -->
    <link href="<?php echo base_url(); ?>pos_assets/css/normalize.css" rel="stylesheet">
    <!-- Custom -->
    <link href="<?php echo base_url(); ?>pos_assets/css/style.css" rel="stylesheet">
    <!-- Arabic -->
    <link href="<?php echo base_url(); ?>pos_assets/css/ar.css" rel="stylesheet">
    <!-- Select -->
    <link href="<?php echo base_url(); ?>pos_assets/css/bootstrap-select.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>pos_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="<?php echo base_url(); ?>pos_assets/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>pos_assets/css/DateTimePicker.css" rel="stylesheet" type="text/css" />  
    <style>
    .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9 {float: right;}  
        .panel-heading {text-align: right;}
        .bootstrap-select:not([class*="span"]):not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {width: 100%;}
        .bootstrap-select.btn-group .btn .filter-option {text-align: right;}
        .bootstrap-select.btn-group .btn .caret {left: 12px; right: 0;}
        .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9 {float: right;}
        .dropdown-menu {text-align: right !important;}
        .green_gate {background: #5cb85c; border: none; color: #fff; margin-bottom: 1em;}
        .red_gate {background: #d9534f; border: none; color: #fff; margin-bottom: 1em;}
        .orange_gate {background: #f4a034; border: none; color: #fff; margin-bottom: 1em;}
        .yellow_gate {background: #c4c641; border: none; color: #fff; margin-bottom: 1em;}
        .form-control::-moz-placeholder {color: #fff;}
      /*  .btn {white-space: pre-wrap;} */
    </style>
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <header>
  
  <div class="container">
  	<div class="row">
    	<div class="col-sm-12"><img src="<?php echo base_url(); ?>pos_assets/images/logo.png" alt="Business Solutions" class="img-responsive bslogo"></div>
    </div>
  </div>
  
  </header>
<section>
<div class="container">
	<div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12">
<!-- Row start -->
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <i class="icon-calendar"></i>
          <h3 class="panel-title">العملاء </h3>
        </div>
          
        
      
        <div class="panel-body">
          <form class="form-horizontal row-border" action="#">
              <div class="col-sm-6">
           <div class="panel-body">
              <div class="form-group">
              <label class="col-md-2 control-label">الشركة</label>
              <div class="col-md-10">
                        <select class="selectpicker" style="display: none;">
                          <option>special home | البيت المميز لبيع الأصباغ ومواد البناء وأعمال الديكور</option>
                          <option>special home | البيت المميز لبيع الأصباغ ومواد البناء وأعمال الديكور</option>
                        </select>
               </div>
          </div>
               <div class="form-group">
              <label class="col-md-2 control-label">الفرع</label>
              <div class="col-md-10">
                        <select class="selectpicker" style="display: none;">
                          <option>فرع شناص</option>
                          <option>فرع العقر</option>
                        </select>
               </div>
          </div>
            </div>
        </div>
            <div class="col-sm-6">
          <div class="panel-body">
              <div class="form-group">
              <label class="col-md-2 control-label">العميل</label>
              <div class="col-md-10">
                <div class="row">  
                    
                    <div class="col-xs-2"><a href=""><i class="fa fa-plus-circle fa-2x"></i></a></div>
                    <div class="col-xs-2"><a href=""><i class="fa fa-th-list fa-2x"></i></a></div>
                    <div class="col-xs-8">
                        <input type="text" name="regular" class="form-control">
                    </div>
                </div>
              </div>
          </div>
              <div class="form-group">
              <label class="col-md-2 control-label"></label>
              <div class="col-md-10">
                <div class="row">
                  <div class="col-xs-7">
                    <input type="text" class="form-control red_gate" placeholder=" المبالغ المستحقة علي العميل 0.000">
                  </div>
                  <div class="col-xs-5">
                    <input type="text" class="form-control green_gate" placeholder="الرصيد 0.000">
                  </div>
                </div>
              </div>
            </div>
            </div>
        </div>
        
          </form>
        </div> 
      </div>
    </div>
  </div>
  <!-- Row end -->
<!-- Row start -->
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <i class="icon-calendar"></i>
          <h3 class="panel-title">المنتجات</h3>
        </div>
          
        
      
        <div class="panel-body">
          <form class="form-horizontal row-border" action="#">
              <div class="col-sm-6">
           <div class="panel-body">
              
                              <div class="row">
<div class="col-sm-6">
    <div class="form-group">
              <label class="col-sm-2 control-label">المخزن</label>
              <div class="col-sm-10">
                        <select class="selectpicker" style="display: none;">
                          <option>اختر</option>
                          <option>العقر</option>
                          <option>شناص</option>
                        </select>
               </div>
                                  </div>   </div>
<div class="col-sm-6">
    <div class="form-group">
              <label class="col-sm-2 control-label">القسم</label>
              <div class="col-sm-10">
                        <select class="selectpicker" style="display: none;">
                          <option>اختر</option>
                          <option>العقر</option>
                          <option>شناص</option>
                        </select>
               </div>
                                  </div>
                  </div>
                  
          </div>
               
                              <div class="row">
<div class="col-sm-6">
    <div class="form-group">
              <label class="col-sm-3 control-label" style="padding-right:0; padding-left:0;">سعر المنتج</label>
              <div class="col-sm-9">
                        <input type="text" class="form-control">
               </div>
    </div>
                                  </div>
<div class="col-sm-6">
    <div class="form-group">
              <label class="col-sm-2 control-label">صورة</label>
              <div class="col-sm-10">
                        <img src="<?php echo base_url(); ?>pos_assets/images/imgholder.png" width="64px" height="64px" style="float:right">
               </div>
                                  </div>
                  </div>
                  
          </div>
                <div class="form-group">
                              <div class="row">
<div class="col-sm-6">
              <label class="col-sm-3 control-label" style="padding-right:0; padding-left:0;">سعر بديل</label>
              <div class="col-sm-9">
                        <div class="checkbox" style="text-align:right;">
              <label>
                <input type="checkbox"> 
              </label>
            </div>
               </div>
                                  </div>
<div class="col-sm-6">
              </div>
                  </div>
                  
          </div>
               <div class="form-group">
                              <div class="row">
<div class="col-sm-6">
              <label class="col-sm-3 control-label" style="padding-right:0; padding-left:0;"></label>
              <div class="col-sm-9">
                        <input type="text" class="form-control">
               </div>
                                  </div>
<div class="col-sm-6">
              </div>
                  </div>
                  
          </div> 
            </div>
        </div>
            <div class="col-sm-6">
          <div class="panel-body">
              <div class="form-group">
                  <label class="col-md-4 control-label">الكمية</label>
                  <div class="col-md-8">
                    <input type="text" name="regular" class="form-control">
                    <i class="icon-pencil input-icon"></i>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-md-4 control-label"> الكمية المتوفره في المخزن</label>
                  <div class="col-md-8">
                    <input type="text" name="regular" class="form-control">
                    <i class="icon-pencil input-icon"></i>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-md-4 control-label"> الرف </label>
                  <div class="col-md-8">
                    <input type="text" name="regular" class="form-control">
                    <i class="icon-pencil input-icon"></i>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-md-4 control-label">الحد الادني</label>
                  <div class="col-md-8">
                    <input type="text" name="regular" class="form-control">
                    <i class="icon-pencil input-icon"></i>
                  </div>
              </div>
              
            </div>
        </div>
        
          </form>
            
            <div class="col-sm-12">
                
    <div class="row col-sm-12 col-md-offset-2 custyle">
        <div class="table-responsive">
    <table class="table table-striped custab">
    <thead>
    <a href="#" class="btn btn-warning pull-right">أضافة المنتج <i class="fa fa-plus"></i></a>
        <tr>
            <th>رقم سري</th>
            <th>الصوره</th>
            <th>اسم المنتج</th>
            <th>الوصف</th>
            <th>سعر المنتج</th>
            <th>الكمية</th>
            <th>الاجمالي</th>
            <th class="text-center">الإجراءات</th>
            <th>حذف جميع</th>
        </tr>
    </thead>
            <tr>
                <td>1</td>
                <td><img src="<?php echo base_url(); ?>pos_assets/images/imgholder.png" width="64px" height="64px"></td>
                <td> MATT EMULSION  (W1) D </td>
                <td> MATT EMULSION </td>
                <td>17.500</td>
                <td>3</td>
                <td>17.500</td>
                <td class="text-center"><a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
                <td><input type="checkbox"></td>
            </tr>
            <tr>
                <td>2</td>
                <td><img src="<?php echo base_url(); ?>pos_assets/images/imgholder.png" width="64px" height="64px"></td>
                <td> MATT EMULSION  (W1) D </td>
                <td> MATT EMULSION </td>
                <td>13.500</td>
                <td>1</td>
                <td>13.500</td>
                <td class="text-center"><a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
                <td><input type="checkbox"></td>
            </tr>
            <tr>
                <td>3</td>
                <td><img src="<?php echo base_url(); ?>pos_assets/images/imgholder.png" width="64px" height="64px"></td>
                <td> MATT EMULSION  (W1) D </td>
                <td> MATT EMULSION </td>
                <td>11.500</td>
                <td>7</td>
                <td>11.500</td>
                <td class="text-center"><a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
                <td><input type="checkbox"></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><input type="text" class="form-control"> <input type="radio"> RO <input type="radio"> %</td>
                <td></td>
                <td>بعد التخفيض</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </table></div>
    </div>

            </div> <!-- card -->
            
            <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-md-2 control-label">التسويق</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" name="regular">
                    <i class="icon-pencil input-icon"></i>
                  </div>
              </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-md-4 control-label">(كود :  (اضافة مسوق </label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" name="regular">
                    <i class="icon-pencil input-icon"></i>
                  </div>
              </div>
            </div>
            
            
  </div> 
          
      </div>
        
    </div>
  </div>
  <!-- Row end -->
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <i class="icon-calendar"></i>
          <h3 class="panel-title">الدفع </h3>
        </div>
          
        
      
        <div class="panel-body">
          <form action="#" class="form-horizontal row-border">
              <div class="col-sm-12">
           <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                <div class="row">
                  <div class="col-sm-3 col-xs-12">
                    <input type="text" placeholder=" تهيئة الحساب 0.000" class="form-control red_gate">
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <input type="text" placeholder="الرصيد 0.000" class="form-control yellow_gate">
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <input type="text" placeholder="المتبقي 0.000" class="form-control orange_gate">
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <input type="text" placeholder=" الاجمالي 0.000" class="form-control green_gate">
                  </div>
                </div>
              </div>
            </div>
               
               <div class="form-group">
              <div class="col-sm-12">
                <div class="row">
                  
                              <div class="row">
<div class="col-sm-6">
    <div class="form-group">
              <label class="col-sm-2 control-label">كيفية الدفع</label>
              <div class="col-sm-10">
                        <select class="selectpicker" style="display: none;">
                          <option>الدفع نقدي</option>
                          <option>من رصيدي </option>
                          <option>Later</option>
                        </select>
               </div>
                        </div>           </div>
<div class="col-sm-6">
     <div class="form-group">
              <label class="col-sm-2 control-label">نوع الدفع</label>
              <div class="col-sm-10">
                        <select class="selectpicker" style="display: none;">
                          <option>اختر</option>
                          <option>كاش</option>
                          <option>بنك</option>
                        </select>
               </div>
                                  </div>
                  </div>
                      
          </div>
                    
                    <div class="form-group">
                              <div class="row">
                                  <div class="col-sm-6">
                                      <label class="col-sm-2 control-label">تاريخ الدفع</label>
                                          <div class="col-sm-10">
                <input type="text" class="form-control" data-field="date" readonly placeholder="التاريخ المحدد" />
                <div id="dtBox"></div>
                                           </div>
                                  </div>
                                  
                                  <div class="col-sm-6">
                                      <label class="col-sm-2 control-label">المبلغ</label>
                                          <div class="col-sm-10">
                                                    <input type="text" class="form-control">
                                           </div>
                                  </div>
                                </div>
                      
                    </div>
                    
                    <div class="form-group">
                              <div class="row">
                                  <div class="col-sm-6">
                                      <label class="col-sm-2 control-label">اسم بنك</label>
                                          <div class="col-sm-10">
                                                    <input type="text" class="form-control">
                                           </div>
                                  </div>
                                  
                                  <div class="col-sm-6">
                                      <label class="col-sm-2 control-label">شك هاتف</label>
                                          <div class="col-sm-10">
                                                    <input type="text" class="form-control">
                                           </div>
                                  </div>
                                </div>
                      
                    </div>
                
                </div>
              </div>
            </div>
               
               
               
              
            </div>
        </div>
            
        
          </form>
        </div> 
      </div>
    </div>
  </div>
            

        </div>
    </div>
</div>
</section>  
  

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url(); ?>pos_assets/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>pos_assets/js/bootstrap.min.js"></script>
    <!-- Select Picker -->
    <script src="<?php echo base_url(); ?>pos_assets/js/bootstrap-select.js"></script>
    <!-- Owl Carousel -->
    <script src="<?php echo base_url(); ?>pos_assets/js/owl.carousel.min.js"></script>
    <script>
		$(document).ready(function () {
			var carousel = $("#owl-demo");
		  	carousel.owlCarousel({
			navigation:true,
			navigationText: [
			  "<i class='icon-chevron-left icon-white'><</i>",
			  "<i class='icon-chevron-right icon-white'>></i>"
			  ],
		  });
		});
    </script>
    <!-- Double click -->
    <script>
		var oriVal;
		$("#eVal1").on('dblclick', 'span', function () {
			oriVal = $(this).text();
			$(this).text("");
			$("<input type='text' class='thVal'>").appendTo(this).focus();
		});
		$("#eVal1").on('focusout', 'span > input', function () {
			var $this = $(this);
			$this.parent().text($this.val() || oriVal);
			$this.remove(); // Don't just hide, remove the element.
		});
		
		var oriVal;
		$("#eVal2").on('dblclick', 'span', function () {
			oriVal = $(this).text();
			$(this).text("");
			$("<input type='text' class='thVal'>").appendTo(this).focus();
		});
		$("#eVal2").on('focusout', 'span > input', function () {
			var $this = $(this);
			$this.parent().text($this.val() || oriVal);
			$this.remove(); // Don't just hide, remove the element.
		});
		
		var oriVal;
		$("#eVal3").on('dblclick', 'span', function () {
			oriVal = $(this).text();
			$(this).text("");
			$("<input type='text' class='thVal'>").appendTo(this).focus();
		});
		$("#eVal3").on('focusout', 'span > input', function () {
			var $this = $(this);
			$this.parent().text($this.val() || oriVal);
			$this.remove(); // Don't just hide, remove the element.
		});
		
		var oriVal;
		$("#eVal4").on('dblclick', 'span', function () {
			oriVal = $(this).text();
			$(this).text("");
			$("<input type='text' class='thVal'>").appendTo(this).focus();
		});
		$("#eVal4").on('focusout', 'span > input', function () {
			var $this = $(this);
			$this.parent().text($this.val() || oriVal);
			$this.remove(); // Don't just hide, remove the element.
		});
	</script>
    <!-- C Slider -->
    <script src="js/jquery.cslide.js"></script>
    <script>
		$(document).ready(function(){
			$("#cslide-slides").cslide();
		});
	</script>
      
    <!-- datepicker -->
    <script type="text/javascript" src="<?php echo base_url(); ?>pos_assets/js/DateTimePicker.js"></script>
    <script type="text/javascript">
      $(document).ready(function()
      {
        $("#dtBox").DateTimePicker();
      });
    </script>
   
  </body>
</html>