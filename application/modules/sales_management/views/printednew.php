<div id="dividprint">
<table bgcolor="#f6f6f6" id="invoicediv" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="border: 1px solid #ccc; padding:5px 10px 5px 10px; font-family:Arial, Gadget, sans-serif; margin-top:10px; text-align:center; font-size:30px; ">
    <tr>
        <td colspan="3">INVOICE - فــاتــورة</td>
    </tr>
    <tr style="font-size: 12px;">
        <td  valign="middle" bgcolor="#f6f6f6">Invoice No </td>
        <td style="font-weight:bold; text-align:center;">" <?php echo $invoice->invoice_id ?> "</td>
        <td dir="rtl" valign="middle" bgcolor="#f6f6f6">رقم الفاتورة</td>
    </tr>
    <?php $total = 0 ?>                <?php $count = 1 ?>                <?php foreach ($p_invoices as $p_invoice): ?>
        <tr>
            <td bgcolor="#d9d9d9" class="num"><?php echo $count ?></td>
            <td  style="text-align:left;"><?php   echo _s($p_invoice->itemname,'arabic')."|"._s($p_invoice->itemname,'english'); ?></td>
            <?php if ($invoice->print_option_desc == 1): ?>
                <td  style="text-align:left;"><?php echo $p_invoice->invoice_item_notes ?></td>
            <?php endif; ?>
            <td class="num"><?php echo $p_invoice->invoice_item_quantity ?></td>
            <td class="num"><?php echo ($p_invoice->invoice_item_price + $p_invoice->invoice_item_discount) ?> </td>
            <td class="num"><?php echo ($p_invoice->invoice_item_price + $p_invoice->invoice_item_discount) * $p_invoice->invoice_item_quantity ?> </td>
        </tr>
        <?php //$total1+=($p_invoice->invoice_item_price) * $p_invoice->invoice_item_quantity ?>                    <?php $total+=($p_invoice->invoice_item_price + $p_invoice->invoice_item_discount) * $p_invoice->invoice_item_quantity ?>                    <?php $totaldiscount+=$p_invoice->invoice_item_discount * $p_invoice->invoice_item_quantity ?>                    <?php $count++; ?>                <?php endforeach; ?>                <?php if ($invoice->print_option_total == 1): ?>
        <tr>
            <td bgcolor="#d9d9d9"  class="num">#</td>
            <td style="text-align:left;">&nbsp;                            المبلغ /Total Amount                        </td>
            <?php if ($invoice->print_option_desc == 1): ?>
                <td colspan="5" style="text-align:left;">&nbsp;</td>
            <?php else: ?>
                <td colspan="2" style="text-align:left;">&nbsp;</td>
            <?php endif; ?>
            <td class="num"><?php echo ($total) ?>  </td>
        </tr>
    <?php endif; ?>                <!--                <?php if ($totaldiscount && $invoice->invoice_totalDiscount == '0.000'): ?>        <tr>            <td bgcolor="#d9d9d9"  class="num">#</td>            <td width="26%" style="text-align:left; padding-left:15px;">&nbsp;                الخصم / Discount                </td>                    <?php if ($invoice->print_option_desc == 1): ?><td colspan="3" style="text-align:left; padding-left:15px;">&nbsp;</td> <?php else: ?><td colspan="2" style="text-align:left; padding-left:15px;">&nbsp;</td> <?php endif; ?>                                    <td class="num"><?php echo $totaldiscount ?>  </td>        </tr>                <?php else: ?>                        <tr>            <td bgcolor="#d9d9d9"  class="num">#</td>            <td width="26%" style="text-align:left; padding-left:15px;">&nbsp;                الخصم / Discount                </td>                    <?php if ($invoice->print_option_desc == 1): ?><td colspan="3" style="text-align:left; padding-left:15px;">&nbsp;</td> <?php else: ?><td colspan="2" style="text-align:left; padding-left:15px;">&nbsp;</td> <?php endif; ?>                                    <td class="num"><?php echo $invoice->invoice_totalDiscount + $totaldiscount ?>  </td>        </tr>                <?php endif; ?>                -->                <?php if ($invoice->print_option_dicount == 1):   $disc = $invoice->invoice_totalDiscount+$totaldiscount; if($disc>0){ ?>
        <tr>
            <td bgcolor="#d9d9d9"  class="num">#</td>
            <td style="text-align:left;">&nbsp;                            الخصم / Discount                        </td>
            <?php if ($invoice->print_option_desc == 1): ?>
                <td colspan="5" style="text-align:left;">&nbsp;</td>
            <?php else: ?>
                <td colspan="2" style="text-align:left;">&nbsp;</td>
            <?php endif; ?>
            <td class="num"><?php echo $invoice->invoice_totalDiscount+$totaldiscount ?>  </td>
        </tr>
    <?php  } ?>                            <?php endif; ?>                <?php if ($invoice->print_option_netprice == 1): ?>
        <tr>
            <td bgcolor="#d9d9d9"  class="num">#</td>
            <td style="text-align:left;">&nbsp;                            الاجمالى / Total Price                        </td>
            <?php if ($invoice->print_option_desc == 1): ?>
                <td colspan="5" style="text-align:left;">&nbsp;</td>
            <?php else: ?>
                <td colspan="2" style="text-align:left;">&nbsp;</td>
            <?php endif; ?>
            <td class="num"><?php echo ($total - ($invoice->invoice_totalDiscount+$totaldiscount)) ?>  </td>
        </tr>
    <?php endif; ?>
</table>
    </div>
<script>
    window.print();
    function printdiv(printdivname)
    {
        var headstr = "<html><head><title>Print</title></head><body>";
        var footstr = "</body>";
        var newstr = document.getElementById(printdivname).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr+newstr+footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }

//printdiv('dividprint');
</script>