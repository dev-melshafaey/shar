<?php $this->load->view('common/meta');?>
<!--body with bg-->

<div class="body">
<header>
  <?php $this->load->view('common/header');?>
  <?php $this->load->view('common/search');?>
  <nav>
    <?php $this->load->view('common/navigations');?>
  </nav>
</header>

<!--Section-->
<section>
  <div id="container" class="row-fluid"> 
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/left-navigations');?>
    <!-- END SIDEBAR --> 
    <!-- BEGIN PAGE -->
    <div id="main-content" class="main_content">
      <div class="title"> <span><?php breadcrumb(); ?></span> </div>
      <form action="" method="post" id="frm_invoice" name="frm_invoice" autocomplete="off">
        <input type="hidden" name="ownerid" id="ownerid" value="<?php echo ownerid(); ?>" />
         <input type="hidden" name="userid" id="userid" value="<?php echo $this->session->userdata('bs_userid'); ?>" />
        <div class="invoice">
          <Div class="invoice_raw">
	     <?php if(get_member_type() == '1' OR get_member_type() == '5'):?>
	          <div class="raw form-group">
	            <div class="form_title">Company Name</div>
	            <div class="form_field form-group">
	              <div class="dropmenu">
	                <div class="styled-select">
	                  <?php company_dropbox('companyid',$user->companyid); ?>
	                </div>
	              </div>
	            </div>
	          </div>
	           <div class="raw form-group">
	            <div class="form_title">Branch Name</div>
	            <div class="form_field form-group">
	              <div class="dropmenu">
	                <div class="styled-select">
	                 <?php company_branch_dropbox('branchid',$user->branchid,$user->companyid); ?>
	                </div>
	              </div>
	            </div>
	          </div>
	        <div class="raw form-group">
            <div class="form_title">Store</div>
            <div class="form_field">
              <div class="dropmenu">
                <div class="styled-select">
                  <?php store_dropbox('storeid',$hd->storeid,$hd->companyid); ?>
                </div>
              </div>
            </div>
          </div>
	       <?php endif;?>
            <div class="raw invoice_customer2">
              <div class="left_div_txt">Invoice Number : </div>
              <div class="left_div form-group">
                <input name="invoice_no" id="invoice_no" type="text"  class="formtxtfield_small"/>
              </div>
            </div>
          </Div>
          <Div class="invoice_raw">
            <div class="invoice_customer2">
              <div class="left_div_txt">Customer  : </div>
              <div class="left_div">
                <div class="dropmenu">
                  <div class="styled-select">
                    <select name="">
                      <option selected="selected" >Ahmed mohamed</option>
                      <option>Mohamed Ahmed</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="left_div">
                <div id='osx-modal'>
                  <div class="invoice_add_customer"><a href='#' class='osx'><img src="<?php echo base_url();?>images/internal/add_customer.png" width="20" height="20" border="0" /></a> </div>
                </div>
              </div>
            </div>
          </Div>
          <div id="osx-modal-content">
            <div id="osx-modal-title">Add New Customer</div>
            <div class="close"><a href="#" class="simplemodal-close">x</a></div>
            <div id="osx-modal-data">
              <div class="raw">
                <div class="form_title">Full Name</div>
                <div class="form_field form-group">
                  <div class="left_div">
                    <input name="" type="text"  class="formtxtfield"/>
                  </div>
                </div>
                <div class="star">*</div>
              </div>
              <div class="raw">
                <div class="form_title">Phone Number</div>
                <div class="form_field form-group">
                  <div class="left_div">
                    <input name="" type="text"  class="formtxtfield"/>
                  </div>
                </div>
              </div>
              <div class="raw">
                <div class="form_title">Office Number</div>
                <div class="form_field form-group">
                  <div class="left_div">
                    <input name="" type="text"  class="formtxtfield"/>
                  </div>
                </div>
              </div>
              <div class="raw">
                <div class="form_title">Fax Number</div>
                <div class="form_field form-group">
                  <div class="left_div">
                    <input name="" type="text"  class="formtxtfield"/>
                  </div>
                </div>
              </div>
              <div class="raw">
                <div class="form_title">Email Address</div>
                <div class="form_field form-group">
                  <div class="left_div">
                    <input name="" type="text"  class="formtxtfield"/>
                  </div>
                </div>
              </div>
              <div class="raw">
                <div class="form_title">Type</div>
                <div class="form_field form-group">
                  <div class="left_div"> 
                    <!--dropmenu-->
                    <div class="dropmenu">
                      <div class="styled-select">
                        <select name="">
                          <option selected="selected" >Individual</option>
                          <option>Company</option>
                        </select>
                      </div>
                    </div>
                    <!--end of dropmenu--> 
                  </div>
                </div>
              </div>
              <div class="raw">
                <div class="form_title">Address</div>
                <div class="form_field form-group">
                  <textarea name="" cols="" rows="" class="formareafield"></textarea>
                </div>
              </div>
              <div class="raw">
                <div class="form_title">Code</div>
                <div class="form_field form-group">
                  <input name="" type="text"  class="formtxtfield_small"/>
                </div>
              </div>
              <div class="raw">
                <div class="form_title">Responsable Name</div>
                <div class="form_field form-group">
                  <input name="" type="text"  class="formtxtfield"/>
                </div>
              </div>
              <div class="raw">
                <div class="form_title">Phone  Number</div>
                <div class="form_field form-group">
                  <input name="" type="text"  class="formtxtfield"/>
                </div>
              </div>
              <div class="raw" align="center"> 
                <!--     <input name="" type="submit" class="submit_btn" value="Submit" />-->
                <input name="" type="submit" class="add_btn" value="Add" />
                <input name="" type="reset" class="reset_btn" value="Reset" />
              </div>
              <!--end of raw--> 
            </div>
          </div>
          <!--end raw--->
          <div class="invoice_raw">
            <div class="invoice_no2">
              <div class="left_div_txt">Phone  No :</div>
              <div class="left_div">
                <input name="" type="text"  class="formtxtfield"/>
              </div>
            </div>
            <div class="invoice_no2">
              <div class="left_div_txt">Office  No :</div>
              <input name="" type="text"  class="formtxtfield"/>
            </div>
          </div>
          <div class="invoice_raw">
            <div class="invoice_no2">
              <div class="left_div_txt"> Fax  No :</div>
              <div class="left_div">
                <input name="" type="text"  class="formtxtfield"/>
              </div>
            </div>
          </div>
          <div class="invoice_raw">
            <div class="invoice_no2">
              <div class="left_div_txt"> Type :</div>
              <div class="dropmenu_small">
                <div class="styled-select2">
                  <select name="">
                    <option selected="selected" >Company </option>
                    <option>Individual</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <!--end raw---><br />
          &nbsp;
          <hr />
          <div class="raw_payments">
            <div class="invoice_no2">
              <div class="left_txt_small">Product Name :</div>
              <div class="left_div">
                <div class="dropmenu">
                  <div class="styled-select">
                    <select name="">
                      <option selected="selected" >Select product </option>
                      <option>product 1</option>
                      <option >product 2</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="invoice_no2">
              <div class="left_txt_small">Quantity :</div>
              <div class="left_small2">
                <input name="" type="text"  class="formtxtfield_small"/>
              </div>
            </div>
            <div class="invoice_no2">
              <div class="left_txt_small" > Serial  No :</div>
              <div class="left_small2">
                <input name="" type="text"  class="formtxtfield_small"/>
              </div>
            </div>
            <div class="invoice_no2">
              <div class="left_txt_small" > Total :</div>
              <div class="left_small2">
                <input name="" type="text"  class="formtxtfield_small"/>
                <span class="left_div2">&nbsp; RO.</span></div>
            </div>
            <div class="invoice_no2">
              <div class="left_txt_small" > Discount :</div>
              <div class="left_small2">
                <input name="" type="text"  class="formtxtfield_small"/>
                <span class="left_div2">&nbsp; %</span></div>
            </div>
          </div>
          <!--endraw-->
          
          <div class="raw_payments">
            <div class="invoice_dis"> <span>Discription</span>
              <textarea cols="10" rows="3" name="comment" id="comment" class="dis_txtarea" ></textarea>
            </div>
          </div>
          
          <!--end raw--->
          <div class="invoice_raw">
            <div class="invoice_dis"> <span>Notes</span>
              <textarea name="textarea" cols="10" rows="3" class="dis_txtarea" ></textarea>
            </div>
          </div>
          <!--end raw--->
          <div class="invoice_raw">
            <div class="invoice_table" > <a href="#'">
              <div class="add_item"> </div>
              </a> &nbsp;
              <hr />
              <table width="100%" align="left" >
                <tr>
                  <td width="1%">#
                    <label for="checkbox"></label></td>
                  <td width="5%">Image</td>
                  <td width="22%" >Product Name</td>
                  <td width="7%" >Quantity</td>
                  <td width="20%" >Discription</td>
                  <td width="13%">Total</td>
                  <td width="13%">Discount</td>
                  <td width="13%">Net</td>
                  <td width="1%"><img src="<?php echo base_url();?>images/internal/wrong.png" width="26" height="26" border="0" /></td>
                  <td width="2%"><img src="<?php echo base_url();?>images/internal/maintenance.png" width="26" height="26" /></td>
                </tr>
                <tr>
                  <td >1</td>
                  <td ><img src="<?php echo base_url();?>images/internal/product_icon.jpg" width="50" height="50" /></td>
                  <td style="text-align:center"> Filter And Purefy System </td>
                  <td align="center" valign="middle" style="text-align:center">100 </td>
                  <td>That is the discraption you have entered </td>
                  <td style="text-align:center">250  RO.</td>
                  <td style="text-align:center">10 %</td>
                  <td style="text-align:center">225 RO.</td>
                  <td style="text-align:center" ><img src="<?php echo base_url();?>images/internal/delete_small.png" width="7" height="8" border="0"   onclick="$(this).parent().parent().remove();"/></td>
                  <td align="center" valign="middle" style="text-align:center"><input name="" type="checkbox" value="" /></td>
                </tr>
                <tr>
                  <td colspan="7" bgcolor="#FFFFFF" ><strong>Total Price</strong></td>
                  <td bgcolor="#FFFFFF" style="text-align:center"><strong>365 RO.</strong></td>
                  <td colspan="2" rowspan="5" align="center" valign="middle" bgcolor="#FFFFFF" style="text-align:center">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="7" bgcolor="#FFFFFF" >Discount</td>
                  <td bgcolor="#FFFFFF" style="text-align:center"><input name="" type="text"  class="formtxtfield_small" style="width:50px;"/>
                    <span> RO. </span></td>
                </tr>
                <tr>
                  <td colspan="7" bgcolor="#FFFFFF" ><strong>Net Price</strong></td>
                  <td bgcolor="#FFFFFF" style="text-align:center"><strong>329 RO.</strong></td>
                </tr>
                <tr>
                  <td colspan="7" bgcolor="#FFFFFF" >Recieved</td>
                  <td bgcolor="#FFFFFF"  style="text-align:center"><div class="blue_txt">
                      <input name="" type="text"  class="formtxtfield_small" style="width:50px;"/>
                    </div>
                    <span> RO. </span></td>
                </tr>
                <tr>
                  <td colspan="7" bgcolor="#FFFFFF" >Rest of Amount</td>
                  <td bgcolor="#FFFFFF"  style="text-align:center"><div class="blue_txt">11 RO.</div></td>
                </tr>
              </table>
            </div>
          </div>
          <!--end raw--->
          <Div class="raw">
            <div class="mant_title">Products Maintenance</div>
            <div class="invoice_raw">
              <div class="invoice_mant"><strong>Filter 55R</strong></div>
              <div class="invoice_mant"><strong>Every</strong>
                <div class="formtxtfield_filter_small">
                  <input name="" type="text"  class="filter_field_small"/>
                  <div class="dropmenu_filter">
                    <div class="styled-select_filter">
                      <select name="">
                        <option selected="selected" >Days </option>
                        <option>Weeks</option>
                        <option>Months</option>
                        <option >Years</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="invoice_mant"><strong>From : </strong>
                <input name="" type="text"  class="formtxtfield" style="width:110px;margin-top:-5px;"/>
                <div class="date_icon" style="margin-top:-5px;"><a href="#"><img src="<?php echo base_url();?>images/internal/date_icon.png" width="22" height="24" border="0" /></a></div>
              </div>
              <div class="invoice_mant"><strong>Maintenance Period  : </strong>
                <div class="formtxtfield_filter_small">
                  <input name="" type="text"  class="filter_field_small"/>
                  <div class="dropmenu_filter">
                    <div class="styled-select_filter">
                      <select name="">
                        <option selected="selected" >Days </option>
                        <option>Weeks</option>
                        <option>Months</option>
                        <option >Years</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Div>
          <!--end raw---> 
          
          <!--end raw---> 
          <!--end raw---> 
        </div>
        <div class="form">
          <div class="raw_payments">
            <div class="form_title">
              <input name="" type="checkbox" value="" />
              Payments</div>
            <div class="form_field form-group">
              <input name="input2" type="text"  class="formtxtfield_small"/>
              <span>RO.</span> </div>
            <div class="form_field form-group">
              <input name="input2" type="text"  class="formtxtfield_small"/>
              <span>%</span> </div>
          </div>
          <div class="raw_payments">
            <div class="form_title">Type </div>
            <div class="form_field form-group">
              <div class="dropmenu">
                <div class="styled-select">
                  <select name="">
                    <option selected="selected" >Select Payment Method </option>
                    <option>Cash</option>
                    <option >Cheque</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="form_title2" style="width:60px;">Bank </div>
            <div class="form_field form-group">
              <input name="input2" type="text"  class="formtxtfield"/>
            </div>
          </div>
          <div class="raw_payments">
            <div class="form_title">Cheque Number </div>
            <div class="form_field form-group">
              <input name="input2" type="text"  class="formtxtfield_small"/>
            </div>
            <div class="form_title2"style="width:60px;">Period </div>
            <div class="form_field form-group">
              <div class="formtxtfield_filter_small2">
                <input name="" type="text"  class="filter_field_small"/>
                <div class="dropmenu_filter">
                  <div class="styled-select_filter">
                    <select name="">
                      <option selected="selected" >Days </option>
                      <option>Weeks</option>
                      <option>Months</option>
                      <option >Years</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="form_title2">Payment  Numbers </div>
            <div class="form_field form-group">
              <input name="input2" type="text"  class="formtxtfield_small"/>
            </div>
          </div>
          <div class="raw">
            <div class="form_title">
              <input name="" type="checkbox" value="" />
              From Customer Account</div>
            <div class="rest_money_min">- 25 RO.</div>
            <div class="rest_money_plus">+ 25 RO.</div>
          </div>
          <div class="raw">
            <div class="form_title">
              <input name="" type="checkbox" value="" />
              Pay The Rest </div>
            <div class="form_field form-group">
              <input name="input2" type="text"  class="formtxtfield_small"/>
              <span>RO.</span> </div>
          </div>
          <div class="raw">
            <div class="form_title">Total Profit</div>
            <div class="form_field form-group"> 130 RO. </div>
          </div>
          <div class="raw">
            <div class="form_title">Sales Responsable</div>
            <div class="form_field form-group">
              <div class="dropmenu">
                <div class="styled-select">
                  <select name="">
                    <option selected="selected" >Ahmed mohamed</option>
                    <option>Mohamed Ahmed</option>
                  </select>
                </div>
              </div>
              <div class="left_div">
                <div id='osx2-modal'>
                  <div class="invoice_add_customer"><a href='#' class='osx2'><img src="<?php echo base_url();?>images/internal/add_customer.png" width="20" height="20" border="0" /></a> </div>
                </div>
              </div>
              <div id="osx2-modal-content">
                <div id="osx2-modal-title">Add New Supplier</div>
                <div class="close"><a href="#" class="simplemodal-close">x</a></div>
                <div id="osx2-modal-data">
                  <div class="raw">
                    <div class="form_title">Supplier Name</div>
                    <div class="form_field form-group">
                      <input name="" type="text"  class="formtxtfield"/>
                    </div>
                  </div>
                  <div class="raw">
                    <div class="form_title">Phone  Number</div>
                    <div class="form_field form-group">
                      <input name="" type="text"  class="formtxtfield"/>
                    </div>
                  </div>
                  <div class="raw">
                    <div class="form_title">Fax  Number</div>
                    <div class="form_field form-group">
                      <input name="" type="text"  class="formtxtfield"/>
                    </div>
                  </div>
                  <div class="raw">
                    <div class="form_title">Office Number</div>
                    <div class="form_field form-group">
                      <input name="" type="text"  class="formtxtfield"/>
                    </div>
                  </div>
                  <div class="raw">
                    <div class="form_title">Products</div>
                    <div class="form_field form-group">
                      <div class="dropmenu">
                        <div class="styled-select">
                          <select name="select">
                            <option selected="selected" >Current Products</option>
                            <option>Supplier 1</option>
                            <option >Supplier 2</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="left_div">
                      <div id='osx-modal'>
                        <div class="invoice_add_customer"><a href="add_products.html" ><img src="<?php echo base_url();?>images/internal/add_customer.png" width="20" height="20" border="0" /></a> </div>
                      </div>
                    </div>
                  </div>
                  <div class="raw">
                    <div class="form_title">Notes</div>
                    <div class="form_field form-group">
                      <textarea name="" cols="" rows="" class="formareafield"></textarea>
                    </div>
                  </div>
                  <div class="raw" align="center">
                    <input name="" type="submit" class="submit_btn" value="Submit" />
                    <input name="" type="reset" class="reset_btn" value="Reset" />
                  </div>
                  <!--end of raw--> 
                  
                </div>
              </div>
            </div>
            <div class="form_title2">Code</div>
            <div class="form_field form-group">
              <input name="" type="text"  class="formtxtfield_small"/>
            </div>
          </div>
          <div class="raw">
            <div class="form_title">Purchase Price </div>
            <div class="form_field form-group">120 RO.</div>
          </div>
          <div class="raw">
            <div class="form_title">Net Profit</div>
            <div class="form_field form-group"> 50 RO. </div>
          </div>
          <div class="raw" align="center">
            <input name="" type="submit" class="submit_btn" value="Add" />
            <input name="" type="reset" class="reset_btn" value="Reset" />
          </div>
        </div>
      </form>
    </div>
    <!-- END PAGE --> 
  </div>
</section>
<!-- End Section--> 
<!--footer-->
<?php $this->load->view('common/footer');?>
