<style>
    *{
        font-size: 12px !important;
    }
    .style-three {
        border: 0;
        border-bottom: 2px solid #000;
        background: #FFF;
    }
    .style-two {
        border: 0;
        border-bottom: 2px dashed #000;
        background: #FFF;
    }
    #invoicediv tr td { padding: 5px 10px;}
</style>
<div id="dividprint">
    <h1 style="text-align: center;"><img src="<?php echo base_url(); ?>img/logo_grey_sm.png" width="100"></h1>
    <hr class="style-three">
    <h1 style="text-align: center; " ><span style="font-size: 17px !important;">Invoice No <?php echo $invoice->invoice_id; ?> رقم الفاتورة  </span><br> <?php echo date('d/m/Y h:i:s', strtotime($invoice->invoice_date)); ?> <br> Refrence Number : <?php echo $invoice->fullname; ?></h1>
    <table bgcolor="#FFF"  id="invoicediv" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style=" padding:5px 10px 5px 10px; font-family:Arial, Gadget, sans-serif; margin-top:10px; text-align:center; font-size:30px;   ">
        <tr style="display: none;">
            <td colspan="4"><img src="<?php echo base_url(); ?>barcode_image/orders/<?php echo $invoice->invoice_id; ?>.gif"></td>
        </tr>
        <tr><th style="text-align: left">الإسم<br>Name</th><th>الكمية<br>Qty</th><th>القيمة<br>Price</th><th>الحجم<br>Size</th></tr><hr class="style-three">
        <tr>
            <td colspan="4"><hr class="style-three"></td>
        </tr>
        <?php $total = 0 ?>                <?php $count = 1 ?>                <?php
        foreach ($p_invoices as $p_invoice):

            $moreItems = $this->sales_management->getMoreItems($p_invoice->inovice_product_id);
            //echo "<pre>";
            // print_r($moreItems);
            ?>
            <tr>
                <td  style="text-align:left;font-size: 15px ! important;"><?php
                    echo _s($p_invoice->itemname, 'arabic') . "<br>" . _s($p_invoice->itemname, 'english');
                    if ($moreItems) {
                        $mItems = array();
                        ?> (<?php
                        foreach ($moreItems as $mi) {
                            $mItems[] = $mi->item_eng_name . ' |' . $mi->item_ar_name;
                        } if (!empty($mItems)) {
                            echo implode(',', $mItems);
                        }
                        ?>) <?php } ?></td>
                <?php if ($invoice->print_option_desc == 1): ?>
                    <td  style="text-align:left;"><?php echo $p_invoice->invoice_item_notes ?></td>
                <?php endif; ?>
                <td class="num"><?php echo $p_invoice->invoice_item_quantity ?></td>
             <!--   <td class="num"><?php // echo ($p_invoice->invoice_item_price + $p_invoice->invoice_item_discount)      ?> </td> -->
                <td class="num"><?php echo getAmountFormat(($p_invoice->invoice_item_price + $p_invoice->invoice_item_discount) * $p_invoice->invoice_item_quantity); ?> </td>
                <td ><strong><?php
                        if ($p_invoice->invoice_item_price == $p_invoice->large_price) {
                            echo "L";
                        } elseif ($p_invoice->invoice_item_price == $p_invoice->medium_price) {
                            echo "M";
                        }if ($p_invoice->invoice_item_price == $p_invoice->small_price) {
                            echo "S";
                        }
                        ?></strong></td>
            </tr>
            <tr>
                <td colspan="4"></td>
            </tr>
            <?php //$total1+=($p_invoice->invoice_item_price) * $p_invoice->invoice_item_quantity  ?>                    <?php $total+=($p_invoice->invoice_item_price + $p_invoice->invoice_item_discount) * $p_invoice->invoice_item_quantity ?>                    <?php $totaldiscount+=$p_invoice->invoice_item_discount * $p_invoice->invoice_item_quantity ?>                    <?php $count++; ?>                <?php endforeach; ?>                <?php if ($invoice->print_option_total == 1): ?>
        <!--    <tr>
                <td bgcolor="#d9d9d9"  class="num">#</td>
                <td style="text-align:left;">&nbsp;                            المبلغ /Total Amount                        </td>
            <?php if ($invoice->print_option_desc == 1): ?>
                                            <td colspan="5" style="text-align:left;">&nbsp;</td>
            <?php else: ?>
                                            <td colspan="2" style="text-align:left;">&nbsp;</td>
            <?php endif; ?>
                <td class="num"><?php echo ($total) ?>  </td>
            </tr> -->
        <?php endif; ?>                <!--                <?php if ($totaldiscount && $invoice->invoice_totalDiscount == '0.000'): ?>        <tr>            <td bgcolor="#d9d9d9"  class="num">#</td>            <td width="26%" style="text-align:left; padding-left:15px;">&nbsp;                الخصم / Discount                </td>                    <?php if ($invoice->print_option_desc == 1): ?><td colspan="3" style="text-align:left; padding-left:15px;">&nbsp;</td> <?php else: ?><td colspan="2" style="text-align:left; padding-left:15px;">&nbsp;</td> <?php endif; ?>                                    <td class="num"><?php echo $totaldiscount ?>  </td>        </tr>                <?php else: ?>                        <tr>            <td bgcolor="#d9d9d9"  class="num">#</td>            <td width="26%" style="text-align:left; padding-left:15px;">&nbsp;                الخصم / Discount                </td>                    <?php if ($invoice->print_option_desc == 1): ?><td colspan="3" style="text-align:left; padding-left:15px;">&nbsp;</td> <?php else: ?><td colspan="2" style="text-align:left; padding-left:15px;">&nbsp;</td> <?php endif; ?>                                    <td class="num"><?php echo $invoice->invoice_totalDiscount + $totaldiscount ?>  </td>        </tr>                <?php endif; ?>                -->                <?php
        if ($invoice->print_option_dicount == 1): $disc = $invoice->invoice_totalDiscount + $totaldiscount;
            if ($disc > 0) {
                ?>
                <tr>
                    
                    <td style="text-align:left;">&nbsp;                            الخصم / Discount                        </td>
                    <td></td>
                    <td class="num" style="font-size: 18px ! important;"><?php echo $invoice->invoice_totalDiscount + $totaldiscount ?>  </td>
                </tr>
            <?php } ?>                            <?php endif; ?>                <?php if ($invoice->print_option_netprice == 1): ?>
            <tr>
                <td colspan="4"><hr class="style-three"></td>
            </tr>
            <!-- coupon -->
            <tr>
                <?php if ($invoice->coupon_value): ?>
                   
                    <td style="text-align:left;">&nbsp; Coupon / الكوبون</td>
                    <td></td>
                    <td class="num" style="font-size: 22px ! important;"><?php echo getAmountFormat($invoice->coupon_value); ?>  </td>
                <?php endif; ?>

            </tr>
            <!-- coupon -->

            <tr>
               
                <td style="text-align:left;">&nbsp;                            الاجمالى / Total Price                        </td>

                <td style="text-align:left;">&nbsp;</td>

                <td class="num" style="font-size: 22px ! important;"><?php echo getAmountFormat($invoice->invoice_total_amount); ?>  </td>
            </tr>
        <?php endif; ?>
    </table>
    <h2 style="text-align: center;"> شكرا لك لتناول الطعام معنا<br> Thank you for Dining with us  </h2>
</div>
<script>
    //   window.print();
    url = '<?php echo base_url(); ?>pos/add';

    function printdiv(printdivname)
    {
        var headstr = "<html><head><title>Print</title></head><body>";
        var footstr = "</body>";
        var newstr = document.getElementById(printdivname).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        //document.location.href = 'pos/add';\
        //window.location.assign = '<?php echo base_url ?>pos/add';
        //window.onafterprint = '<?php echo base_url ?>pos/add';
        setTimeout(redirectPage, '1000');
        return false;
    }

    printdiv('dividprint');

    function redirectPage() {
        window.top.location = url;
    }
</script>