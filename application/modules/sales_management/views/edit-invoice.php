<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" type="text/css"/>
<div class="row head">
    <div class="">
        <h4>
            <div class="title"> <span><?php breadcramb(); ?> >> فاتوره مرتجعه</span> </div>
        </h4>
        <?php error_hander($this->input->get('e')); ?>
    </div>
</div>
<div class="box nav-box  g16">



    <div>


        <form action="<?php echo base_url(); ?>sales_management/update_cancel_invoice/<?php echo $id ?>" method="post">
            <input type="hidden"  id="invoice_id" name="invocie_id" value="<?php echo $id; ?>"/>    
            <div class="invoice_raw_product_items"></div>	                                    
            <br clear="all"/>
            <table  id="table" class="table table-bordered invoice-table mb20">
                <thead>
                <th>#</th>
                <th><?php echo lang('Image') ?></th>
                <th><?php echo lang('Product-Name') ?></th>
                <th><?php echo lang('Discription') ?></th>
                <th><?php echo lang('sforone') ?></th>
                <th><?php echo lang('Quantity') ?></th>
                <th><?php echo lang('Total') ?></th>
                </tr>
                </thead>
                <tbody>
                    <?php $get_product = get_invoice_items($id) ?>
                    <?php if ($get_product): ?>
                        <?php $count = 1 ?>
                        <?php $totalp = 0 ?>
                        <?php foreach ($get_product as $product):  //echo "<pre>"; print_r($product); ?>
                            <tr id="tr_<?php echo $product->inovice_product_id ?>">
                                <td ><?php echo $product->itemid ?></td>
                                <td ><img src="<?php echo base_url() ?>uploads/item/<?php echo $product->store ?>/<?php echo $product->product_picture ?>" width="50" height="50" /></td>
                                <td style="text-align:center"> <?php echo _s($product->itemname, get_set_value('site_lang')) ?> </td>
                                <td id="description" contentEditable="true"><?php echo $product->notes ?> </td>
                                <td id="oneitem<?php echo $product->inovice_product_id ?>" style="text-align:center" contentEditable="true"><?php echo $product->iip ?></td>
                                <td id="quantity_tp<?php echo $product->inovice_product_id ?>" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculatePriceOrder('<?php echo $product->inovice_product_id ?>')"><?php echo $product->invoice_item_quantity ?> </td>
                                <td class="ttamm thth" id="totalAmount<?php echo $product->inovice_product_id ?>" style="text-align:center" contentEditable="true" ><?php echo getAmountFormat($product->iip * $product->invoice_item_quantity); ?></td>

                                <td><a href="javascript:void(0)"></a> <i class="icon-remove-sign" style="font-size: 20px; color: red;cursor: pointer;" onclick="delete_sales_req_item('<?php echo $product->inovice_product_id; ?>')"></i></td>

                        <input type="hidden" id="purchase_item_price<?php echo $product->inovice_product_id; ?>" name="purchase_item_price[<?php echo $product->inovice_product_id; ?>]" value="<?php echo $product->invoice_item_price; ?>">
                        <input class="piq" data-content="<?php echo $product->invoice_item_quantity; ?>" type="hidden" id="purchase_item_quantity<?php echo $product->inovice_product_id; ?>" name="purchase_item_quantity[<?php echo $product->inovice_product_id; ?>]" value="<?php echo $product->invoice_item_quantity; ?>">
                        <input type="hidden" name="invoice_payment" value="<?php echo $product->inv_payment_id_all ?>"/>
                        <input type="hidden" name="payment" value="<?php echo $product->payment_id_all ?>"/>
                        <input type="hidden" name="totalpay" value="<?php echo $product->totalpay; ?>"/>
                        </tr>
                        <?php //$totalp+=$product->iip;  ?>
                    <?php endforeach; ?>
                <?php else: ?>        
                    <tr><td colspan="7"><?php echo lang('no-data') ?></td></tr>
                <?php endif; ?>
                <tr  class="grand-total-discount">
                    <td colspan="1" >
                    </td> 
                    <td></td>
                    <td><div id="totalmin" style="display: none"></div></td>
                    <td>   
                    </td>
                    <td></td>                  
                    <td><?php echo lang('discount') ?></td>
                    <td><strong id="netTotal-discount"><?php echo getAmountFormat($invoice[0]->invoice_totalDiscount); ?></strong></td>   
                </tr>

                <tr  class="grand-total">
                    <td colspan="1" >
                    </td> 
                    <td></td>
                    <td><div id="totalmin" style="display: none"></div></td>
                    <td>   
                    </td>
                    <td></td>
                    <td><?php echo lang('Total-Price') ?></td>
                    <td><strong id="netTotal"><?php echo getAmountFormat($invoice[0]->invoice_total_amount); ?></strong></td>

                </tr>
                <?PHP $get_sales = get_invoice_sales($id); ?>
                <?php if ($get_sales): ?>
                    <?php foreach ($get_sales as $get_sale): ?>
                        <tr  class="">
                            <td colspan="1" >
                            </td> 
                            <td><?php echo lang('Sales-Name') ?></td>
                            <td><?php echo $get_sale->fullname ?></td>
                            <td><?php echo lang('Payment-amount') ?></td>
                            <td><?php echo getAmountFormat($get_sale->amount) ?></td>
                            <td></td>
                            <td></td>    
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>


                </tbody>
            </table>
            <input type="submit" class="btn-flat primary g3" value="تنفيذ" name=""/>
        </form>
    </div>

</div>
<script type="text/javascript">
    pcount = '<?php echo 2 ?>';


    function delete_sales_req_item(rowid) {
        message = 'هل أنت متأكد أنك تريد حذف؟';
        ret = confirm(message);
        if (ret) {
            abc = $("#invoice_input_" + rowid).html();
            if (abc) {
                confirmsalesEditRemove(rowid);
            }
            else {
                delete_payorder_record(rowid);
            }

        }
    }

    /*function calculatePriceOrder(ob) {
     alert(1);
     ttpriceaed = 0;
     quantity = $("#quantity_tp" + ob).text().trim();
     oneitem = $("#oneitem" + ob).text().trim();
     
     //totalLength = netTotals.length;
     //totalDiscount = $("#totalDiscount").val()a;
     //console.log(totalLength);
     // var editArray = geteditProductData(ob);
     //pProduct = $("#item_total_price").val();
     if (quantity > 0) {
     totalPrice = (quantity * oneitem);
     $("#tr_" + ob + " #totalAmount" + ob + "").text(totalPrice.toFixed(3));
     ttprice = 0;
     if ($('.thth').length > 0) {
     len = $('.thth').length;
     for (a = 0; a < len; a++) {
     tprice = $('.thth').eq(a).html();
     ttprice = parseFloat(ttprice) + parseFloat(tprice);
     //console.log(tprice + 'tprice');
     }
     }
     $("#netTotal").text(ttprice.toFixed(3));
     $(".netTotal").text(ttprice.toFixed(3));
     tAmount = oneitem * quantity;
     
     totalontme = aedontime * parseInt(quantity);
     // $(" #totalAedAmount"+ ob +"").text(totalontme.toFixed(3))
     //  alert(ob);
     // alert('iff');
     // $("#tr_" + ob + " #totalAmount" + ob + "").text(tAmount.toFixed(3));
     } else {
     // alert('else');
     $("#tr_" + ob + " #totalAmount" + ob + "").text(oneitem.toFixed(3));
     ttprice = 0;
     if ($('.thth').length > 0) {
     len = $('.thth').length;
     for (a = 0; a < len; a++) {
     tprice = $('.thth').eq(a).html();
     ttprice = parseFloat(ttprice.toFixed(3)) + parseFloat(tprice.toFixed(3));
     //console.log(tprice + 'tprice');
     }
     }
     //$("#totalAmount").text(oneitem);
     //  alert('net total');
     $("#netTotal").text(ttprice.toFixed(3));
     $(".netTotal").text(ttprice.toFixed(3));
     //$("#netTotal").text(oneitem);
     //$("#totalnet").val(oneitem);
     }
     // calculateaed();
     
     // $("#purchase_item_price"+obj).val();
     $("#productQuantity" + ob).val(quantity);
     
     }*/

    function delete_payorder_record(id) {
        $.ajax({
            url: config.BASE_URL + "sales_management/delete_sales_order",
            type: 'post',
            data: {sales_item_id: id},
            cache: false,
            success: function (data) {
                if (data) {
                    $("#tr_" + id).remove();
                }
            }
        });
    }

    function calculatePriceOrder(ob) {

        ttpriceaed = 0;
        quantity = $("#quantity_tp" + ob).text().trim();
        oneitem = $("#oneitem" + ob).text().trim();
        oneitemaed = $("#oneitemaed" + ob).text().trim();
        //totalLength = netTotals.length;
        //totalDiscount = $("#totalDiscount").val();
        //console.log(totalLength);
        // var editArray = geteditProductData(ob);
        //pProduct = $("#item_total_price").val();
        console.log(quantity + " = " + oneitem + " = " + oneitemaed);
        if (quantity > 0) {
            totalPrice = (quantity * oneitem);
            $("#tr_" + ob + " #totalAmount" + ob + "").text(totalPrice.toFixed(3));
            ttprice = 0;
            if ($('.thth').length > 0) {
                len = $('.thth').length;
                for (a = 0; a < len; a++) {
                    tprice = $('.thth').eq(a).html();
                    ttprice = parseFloat(ttprice) + parseFloat(tprice);
                    //console.log(tprice + 'tprice');
                }
            }
            $("#netTotal").text(ttprice.toFixed(3));
            $(".netTotal").text(ttprice.toFixed(3));
            tAmount = oneitem * quantity;
            aedontime = $("#oneitemaed" + ob + "").text();
            aedontime = parseFloat(aedontime).toFixed(3);
            totalontme = aedontime * parseInt(quantity);
            $(" #totalAedAmount" + ob + "").text(totalontme.toFixed(3))
            //  alert(ob);
            // alert('iff');
            // $("#tr_" + ob + " #totalAmount" + ob + "").text(tAmount.toFixed(3));
        } else {
            // alert('else');
            $("#tr_" + ob + " #totalAmount" + ob + "").text(oneitem.toFixed(3));
            ttprice = 0;
            if ($('.thth').length > 0) {
                len = $('.thth').length;
                for (a = 0; a < len; a++) {
                    tprice = $('.thth').eq(a).html();
                    ttprice = parseFloat(ttprice.toFixed(3)) + parseFloat(tprice.toFixed(3));
                    //console.log(tprice + 'tprice');
                }
            }
            //$("#totalAmount").text(oneitem);
            //  alert('net total');
            $("#netTotal").text(ttprice.toFixed(3));
            $(".netTotal").text(ttprice.toFixed(3));
            //$("#netTotal").text(oneitem);
            //$("#totalnet").val(oneitem);
        }
        // calculateaed();
        if ($('.aedcc').length > 0) {
            len = $('.aedcc').length;
            for (a = 0; a < len; a++) {
                tpriceaed = $('.aedcc').eq(a).html();
                //alert(tpriceaed);
                ttpriceaed = parseFloat(ttpriceaed) + parseFloat(tpriceaed);
                //console.log(tprice + 'tprice');
            }
        }
        // $("#purchase_item_price"+obj).val();
        $("#purchase_item_quantity" + ob).val(quantity);

    }
    function swapme2p() {

        pid = $("#productid").val();

        pname = $("#productname2").val();

        $("#productname").val(pname);

        $("#productid").val(pid);

    }
    function checkData() {

        plen = allProductsData.length;

        if (plen > 0) {

            $('#customername').attr("disabled", true);

        }

        else {

            $('#customername').attr("disabled", false);

        }

    }

    $(document).ready(function () {
        $('.piq').each(function(){
            this.value = $(this).attr('data-content');
        });

        $('#showdilaog').click(function () {
            show_dialog2('#demo-modal', '<?php echo base_url(); ?>inventory/getallitems', $('#store_id').val());
        });

        $("#category_id").change(function () {
            val = $(this).val();
            $("#productname").trigger('keydown');
        });

        setInterval('checkData()', 1000);

        storeId = $("#store_id").val();

        /*var ac_config = {
         
         source: "<?php echo base_url(); ?>ajax/getAutoSearchProducts?store_id=",
         
         select: function(event, ui){
         
         $("#productname").val(ui.item.id);
         
         $("#productid").val(ui.item.prod);
         
         getProductData(ui.item.id);
         
         console.log(ui);
         
         //swapme();
         
         setTimeout('swapme2()',500);
         
         },
         
         minLength:1
         
         };
         
         
         
         $("#productname").focusin(function(){
         
         //alert('asd');
         
         storeId = $("#store_id").val();
         
         
         
         });*/

        //$("#productname").autocomplete(ac_config);



        $("#productname").autocomplete({
            source: function (request, response) {

                $.getJSON("<?php echo base_url(); ?>ajax/getAutoSearchProducts", {category: $('#category_id').val(), term: $('#productname').val(), storeid: $('#store_id').val()},
                response);

            },
            minLength: 0,
            select: function (event, ui) {

                //action

                $("#productname").val(ui.item.prod);

                $("#productname2").val(ui.item.prod);

                $("#productid").val(ui.item.id);



                //alert('m');

                getProductData(ui.item.id, '', $('#store_id').val());

                //alert($('#store_id').val());

                console.log(ui);

                //swapme();

                setTimeout('swapme2p()', 500);

            }

        });





        $("#productname").focus(function () {

            $("#productname").trigger('keyup');

        });

        var ac_config = {
            source: "<?php echo base_url(); ?>ajax/getAutoSearchCustomer",
            select: function (event, ui) {

                $("#customerid").val(ui.item.id);

                $("#customername").val(ui.item.cus);

                $("#customername2").val(ui.item.cus);

                $("#customer_ftotal").val(ui.item.buywithtotal);



                $("#store_id").val(ui.item.storeid);

                $('#store_id option[value=' + ui.item.storeid + ']').attr('selected', 'selected');

                //alert(ui.item.storeid);

                console.log(ui);

                //swapme();

                setTimeout('swapmeu()', 500);

                if (ui.item.blacklist === "1") {



                    $(".viewmess").html(ui.item.reson_blacklist);

                    $(".get_balance").html("");

                    $(".main_viewmess").css("display", "block");

                    $(".submit_btn").hide();

                } else {

                    $.ajax({
                        url: "<?php echo base_url() ?>sales_management/get_balance",
                        type: "POST",
                        data: {val: ui.item.id},
                        success: function (e) {



                            $('.get_balance').html(e);

                        }

                    });

                    $(".main_viewmess").css("display", "none");

                    $(".submit_btn").show();

                }



            },
            minLength: 1

        };

        $("#customername").autocomplete(ac_config);



    });
    $(document).ready(function () {

        $('#Alternate_Price').click(function () {

            if ($("#Alternate_Price:checked").val() == 1) {



                $('#Alternate_Price_input').css('display', 'block');

                //$('#item_total_price').css('display', 'block');

                //$('#item_total_price').attr('disabled', true);

                $('.item_total_price0').attr('id', 'item_total_price2');

                $('.item_total_price0').attr('name', 'item_total_price2');

                $('.item_total_price_old').attr('id', 'item_total_price');

                $('.item_total_price_old').attr('name', 'item_total_price');

            } else {

                $('#Alternate_Price_input').css('display', 'none');

                //$('#item_total_price').removeAttr('disabled');



                $('.item_total_price0').attr('id', 'item_total_price');

                $('.item_total_price0').attr('name', 'item_total_price');

                $('.item_total_price_old').attr('id', 'item_total_price_old');

                $('.item_total_price_old').attr('name', 'item_total_price_old');

            }



            $("#item_total_price").change(function () {

                //$(this).val()

                $('#total_price').val($(this).val());

                //alert($(this).val());



            });

        });

        $('#is_payment').click(function () {

            if ($("#is_payment:checked").val() == 1) {

                $('.payment_div').css('display', 'block');

            }

        });

        $('#customerType').change(function () {

            if ($(this).val() == 'newcustomer') {



                $('#newcustomer').css('display', 'block');

                $('#customer').css('display', 'none');

                //$('.payment_div').css('display', 'block');



            } else {

                $('#customer').css('display', 'block');

                $('#newcustomer').css('display', 'none');

            }

        });















        $('#payment_amount').change(function () {

            //$("#remin_view_all_total").html($("#remin_view_all_total").html()+$("#payment_amount").html());



            $("#remin_view_all_total").html(parseInt($("#remin_view_all_total").html()) - parseInt($('#payment_amount').val()));

            $("#recievedamount").val(parseInt($("#payment_amount").val()));

            $("#totalrecievedamount").val(parseInt($('#payment_amount').val()));



            //alert($("#howpay").val());

            ppshow = parseInt($("#get_my_allaccount").html()) - parseInt($('#payment_amount').val());

            ppshow2 = parseInt($("#view_all_total").html()) - parseInt($('#payment_amount').val());





            if ($("#howpay").val() == "1") {



                if (parseInt($("#remin_view_all_total").html()) < 0) {



                    $("#increse_amount_span").text(Math.abs(ppshow2));

                    $("#increse_amount").css('display', 'block');

                    $("#remin_view_all_total").text("0");

                    $("#payment_amount").val($('#view_all_total').html());

                    if ($("#remin_view_all_total").text() > 0) {

                        $("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) - parseInt($('#view_all_total').html()));

                    }

                    /*
                     
                     $("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) - parseInt($('#view_all_total').html()));
                     
                     $("#recievedamount").val(parseInt($('#view_all_total').html()));
                     
                     $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                     
                     $("#remin_view_all_total").text("0");
                     
                     */

                } else {

                    $("#get_my_allaccount").text(ppshow);

                }



            } else {



                if (parseInt($("#remin_view_all_total").html()) < 0) {



                    //alert('if');

                    $("#increse_amount").css('display', 'block');

                    $("#increse_amount_span").text(Math.abs(ppshow2));

                    $("#remin_view_all_total").text("0");

                    $("#payment_amount").val($('#view_all_total').html());

                    //$("#get_my_allaccount").text(parseInt($("#get_my_allaccount").html()) - parseInt($('#view_all_total').html()));

                    /*
                     
                     $("#recievedamount").val(parseInt($('#view_all_total').html()));
                     
                     $("#totalrecievedamount").val(parseInt($('#view_all_total').html()));
                     
                     $("#remin_view_all_total").text("0");
                     
                     $("#increse_amount").show();
                     
                     */

                } else {

                    //alert('else');

                    //$("#increse_amount_span").text(Math.abs(ppshow2));

                    $("#remin_view_all_total").text(ppshow2);



                }

            }

        });







        $('select[name=store_id]').change(function () {

            var val = $(this).val();

            var pid = $("#productid").val();



            $.ajax({
                url: "<?php echo base_url() ?>sales_management/get_store_quantity",
                type: "POST",
                data: {val: val, pid: pid},
                success: function (e) {



                    $('#storequantity').val(e);

                    getProductData(pid, '', val);

                }

            });



        });







    });

</script>
<script type="text/javascript">

    var allProductsData = new Array();

    var allSalesData = new Array();

    totalAmountexComision = '';

    totalnetAmountexComision = '';

    $(document).keypress(function (e) {

        if (e.which == 13) {

            //   alert('You pressed enter!');

            if ($(".currentProduct").is(":focus")) {

                //  alert('You pressed enter!');

                if ($("#productid").val() != "")

                {

                    //addItem();
                    addItem_edit();
                }

            }

            else {

                //alert('not');

            }

        }

    });</script> 

<script type="text/javascript">

    netTotals = new Array(); /// for keep rocrds net total for each produc

    Totalsmin = new Array(); /// for keep rocrds net total for each produc

    Totalsquan = new Array(); /// for keep rocrds net total for each produc

    rowCounter = 0; /// counter for table products



    netCouner = 0; // counter for net totals

    tbHtml = ''; // for adding new row in the table 



    editArray = new Array(); // edit any product fromm tab;e 







    total_value = 0; // total cacluate value







    productData = {}; // array for current working product







    counter = 0; // counter for products in the hiddden



</script> 

<script type="text/javascript">

    $(function () {

        /*$(".from_date").datepicker({
         
         defaultDate: "today",
         
         changeMonth: true,
         
         numberOfMonths: 1,
         
         });
         
         $(".payment_date").datepicker({
         
         defaultDate: "today",
         
         changeMonth: true,
         
         numberOfMonths: 1,
         
         });
         
         $(".dpayment_date input").datepicker({
         
         defaultDate: "today",
         
         changeMonth: true,
         
         numberOfMonths: 1,
         
         });
         
         */

        $("#is_payment").change(function () {





            ckPayment = $(this).is(":checked");

            //alert(ckPayment);

            if (ckPayment) {

                if (netTotals.length > 0) {

                    $(".payment_div").show();

                }

            }

            else {

                $(".payment_div").hide();

            }

            //alert(ckPayment);



        });

    });</script> 







<script type="text/javascript">

    $(function () {

        $('#expense_date').datepicker({dateFormat: 'yy-mm-dd'});

        $(".expense_period").click(function () {

            //alert('asdasd');



            //checked_status = $(this).is(':checked');

            expense_period_val = $(this).val();



            //alert(checked_status);

            if (expense_period_val == 'fixed') {



                $("#period_parent").show();

            }

            else {

                $("#period_parent").hide();

            }

        });

    });

    $(document).ready(function () {

        //alert('ready');

        var ac_config = {
            source: "<?php echo base_url(); ?>outcome/getAutoSearch",
            select: function (event, ui) {

                $("#expense_title").val(ui.item.cus);

                $("#expense_id").val(ui.item.cus);

                console.log(ui);

                //swapme();

                setTimeout('swapgeneral()', 500);

            },
            minLength: 1

        };



        $("#expense_title").autocomplete(ac_config);





    });







</script>

