<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sales_management_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getMostSellingItem($type) {
        $d = date('Y-m-d');

        if ($type == 'm') {
            $sub = " WHERE MONTH(a.`invoice_date`) = MONTH('" . $d . "') ";
        } elseif ($type == 'd') {
            $sub = " WHERE DATE(a.`invoice_date`) = DATE('" . $d . "') ";
        } elseif ($type == 't') {

            $fdate = getSingelDate('from', $d);
            $todate = getSingelDate('to', $d);

            //$sub = " WHERE DATE(a.`invoice_date`) = DATE('".$d."')";
            $sub = " WHERE a.`invoice_date`>= '" . $fdate . "' AND a.`invoice_date`<= '" . $todate . "' ";
        } else {
            $sub = "";
        }
        /* $sql = "SELECT
          i.`itemname`,
          SUM(it.`invoice_item_quantity`) AS totalitems ,MONTH(a.`invoice_date`)
          FROM
          `bs_invoice_items` AS it
          INNER JOIN `bs_item` AS i
          ON i.`itemid` = it.`invoice_item_id`
          INNER JOIN `an_invoice` AS a
          ON a.`invoice_id` = it.`invoice_id`
          " . $sub . "
          GROUP BY it.`invoice_item_id`
          ORDER BY   SUM(it.`invoice_item_quantity`)  DESC "; */
        $sql = "SELECT bit.*,mi.`link_id`,itemname,ai.`item_ar_name`,MONTH(a.`invoice_date`),SUM(bit.`invoice_item_quantity`) AS totalitems,
IF(bit.`invoice_item_price` = bi.`large_price`, bi.`large_price`, 0) AS large ,
IF(bit.`invoice_item_price` = bi.`medium_price`, bi.`medium_price`, 0) AS `medium` ,
IF(bit.`invoice_item_price` = bi.`small_price`, bi.`small_price`, 0) AS small            
FROM `bs_invoice_items` `bit` 
INNER JOIN `bs_item` AS bi ON bi.`itemid` = bit.`invoice_item_id` 
INNER JOIN `an_invoice` AS a ON a.`invoice_id` = bit.`invoice_id` 
LEFT JOIN `more_items` mi ON bit.`inovice_product_id`  = mi.`invoice_item_id` 
LEFT JOIN `link_items` li ON  mi.`link_id`= li.`link_id`
LEFT JOIN `additional_items` ai ON li.`additional_item_id` = ai.`ad_item_id` $sub
GROUP BY bit.`invoice_item_id`,li.`additional_item_id` ORDER BY totalitems DESC";

        //echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getPaymentIdsForCancel($invoiceid) {

        $sql = "    SELECT 
      ip.`payment_id`      
    FROM
      `invoice_payments` AS ip 
      INNER JOIN `an_invoice` AS ii 
        ON ii.`invoice_id` = ip.`invoice_id` 
        WHERE ii.`invoice_id` = '" . $invoiceid . "'";

        $query = $this->db->query($sql);

        return $query->result();
    }

    function inserData($table, $data) {

        $this->db->insert($table, $data);

        return $this->db->insert_id();
    }

    function getOrderIdByInId($id) {
        $sql = "SELECT * FROM `orders` AS o WHERE o.`invoice_id` = '" . $id . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->row()->order_id;
        } else {
            return false;
        }
    }

    function updateData($table, $data, $id, $par = null) {

        if ($par == null) {

            $this->db->where('id', $id);
        } else {

            $this->db->where($par, $id);
        }

        $this->db->update($table, $data);

        //return $this->db->insert_id();

        return true;
    }

    function update($table, $data, $id) {



        $this->db->where('bs_sales_id', $id);

        $this->db->update($table, $data);
    }

    public function product_list($product_id) {

        if ($product_id != "") {

            $subq = " WHERE bs_item.`itemid` =" . $product_id . "";
        } else {

            $subq = "";
        }

        //$ownerid = ownerid();

        $query = $this->db->query("SELECT 

			  `bs_category`.`catname`,

			  `bs_item`.`storeid`,

			  `bs_suppliers`.`suppliername`,

			  `bs_store`.`storename`,

			  `bs_item`.`itemid`,

			  `bs_item`.`itemname`,

			  `bs_item`.`itemname`,

			  `bs_item`.`itemtype`,

			  `bs_item`.`notes`,

			  IF(

				bs_item.itemtype = 'P',

				`bs_item`.`serialnumber`,

				'-----'

			  ) AS `serialnumber`,

			  IF(

				bs_item.itemtype = 'P',

				CONCAT(`bs_item`.`purchase_price`),

				'-----'

			  ) AS `purchase_price`,

			  IF(

				bs_item.itemtype = 'P',

				CONCAT(`bs_item`.`sale_price`),

				'-----'

			  ) AS `sale_price`,

			  IF(

				bs_item.itemtype = 'P',

				`bs_item`.`min_sale_price`,

				'-----'

			  ) AS `min_sale_price`,

			  IF(

				bs_item.itemtype = 'P',

				`bs_item`.`point_of_re_order`,

				'-----'

			  ) AS `point_of_re_order`,

			  IF(

				bs_item.itemtype = 'P',

				`bs_item`.`quantity`,

				'-----'

			  ) AS `quantity`,

			  `bs_item`.`product_picture` 

			FROM

			  (`bs_item`) 

			  JOIN `bs_category` 

				ON `bs_item`.`categoryid` = `bs_category`.`catid` 

			  JOIN `bs_store` 

				ON `bs_item`.`storeid` = `bs_store`.`storeid` 

			  JOIN `bs_suppliers` 

				ON `bs_item`.`suppliderid` = `bs_suppliers`.`supplierid` 

				" . $subq . " 

			ORDER BY `bs_item`.`itemname` ASC ");

        //$this->db->last_query();

        if ($product_id != "") {

            return $query->row();
        } else {

            return $query->result();
        }
    }

    function getSalesPersonsData() {

        $sql = "SELECT bsales.`bs_sales_id`,bu.`fullname`,bu.`employeecode`,bu.`userid`,bsales.`bs_sales_commession`,bsales.`bs_sales_commession_type`,bsales.bs_sales_commession_how FROM `bs_sales` bsales

                INNER JOIN `bs_users` AS bu ON bu.`userid` = bsales.`bs_user_id`";

        $query = $this->db->query($sql);

        return $query->result();
    }

    function get_all_users() {

        $sql = "SELECT * FROM `bs_users` where member_type='3'";

        $query = $this->db->query($sql);

        return $query->result();
    }

    function get_sales($id = null) {

        $sql = "SELECT * FROM `bs_sales` where bs_sales_id= '$id' ";

        $query = $this->db->query($sql);

        return $query->row();
    }

    function getInvoiceDataById($invid) {

        $sql = "SELECT * FROM `an_invoice` AS i WHERE i.`invoice_id` = '" . $invid . "'";
        $query = $this->db->query($sql);

        return $query->row_array();
    }

    function getInvoiceItemsDataById($invId) {
        $sql = "SELECT * FROM `bs_invoice_items` AS it WHERE it.`invoice_id` = '" . $invId . "'";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function getPaymentDataById($paymentid) {
        $sql = "SELECT  * FROM `payments` AS p WHERE p.`id` = '" . $paymentid . "'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getSoldItemsByInvId($invid) {
        $sql = "SELECT * FROM `store_soled_items` AS iit WHERE iit.`recipt_id` = '" . $invid . "' ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getRawSoldItemsByInvId($invid) {
        $sql = "SELECT * FROM `raw_soled_items` AS iit WHERE iit.`recipt_id` = '" . $invid . "' ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getCoupon($invid) {
        $sql = "SELECT * FROM invoice_coupon INNER JOIN `coupon` ON `invoice_coupon`.`coupon_id` = `coupon`.`coupon_id` where invoice_id = $invid";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getItemIds($ids) {
        $item_ids = [];
        $item_price = [];
        $item_qty = [];
        foreach ($ids as $index => $value) {
            $sql = "select invoice_item_id,invoice_item_price from bs_invoice_items where inovice_product_id = $index";
            $query = $this->db->query($sql);
            $id = $query->row()->invoice_item_id;
            $price = $query->row()->invoice_item_price;
            array_push($item_ids, $id);
            array_push($item_price, $price);
            array_push($item_qty, $value);
        }
        return [$item_ids, $item_price, $item_qty];
    }

    function get_invoice_coupon($invid) {
        $sql = "select * from invoice_coupon where invoice_id = $invid";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getInvoicePaymentsById($id) {
        $sql = "SELECT * FROM invoice_payments AS ivp WHERE ivp.`invoice_id` = '" . $id . "'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getBankAccounts() {

        $sql = "SELECT c.`account_number` FROM `bs_company` AS c";

        $query = $this->db->query($sql);

        return $query->result();
    }
    
     function getSaleInvocies_cancelnew($id = '') {
        $subq2 = '';

        if ($id) {

            $subq2 = " AND i.`user_id` = '" . $id . "'";

            $date = date('Y-m-d');
            $responseDate = getFromTo($date);
            $dateArr = json_decode($responseDate);

            $from = $dateArr->from;
            $to = $dateArr->to;
            $subq2.= " AND i.`invoice_date` >= '" . $from . "' AND i.`invoice_date` <= '" . $to . "' ";

        }
        $get = $this->input->get();
  


        if(isset($get['uid']) && $get['uid']!=""){
            $subq2.= " AND i.`user_id` = '" .$get['uid']. "'";
        }
        if ($get['frm']!=null && $get['to']!=null) {
            $from  = getSingelDate('from',$get['frm']);
            $to  = getSingelDate('to',$get['to']);
            $subq2.= " AND i.`invoice_date` >= '" . $from . "' AND i.`invoice_date` <= '" . $to . "' ";
        }

        if(isset($get['rec']) && $get['rec']!=""){

            $subq2.=' AND (sum_invoiceprice- IFNULL(invoice_totalDiscount,0)) - IFNULL(pamount,0)>0';
        }


        if(isset($get['bid']) && $get['bid']!="")
            $subq2.= ' AND i.branchid = '.$get['bid'];

        if($subq2 ==''){
            $date = date('Y-m-d');
            $responseDate = getFromTo($date);
            $dateArr = json_decode($responseDate);
            $from = $dateArr->from;
            $to = $dateArr->to;
            $subq2= " AND o.create >= '" . $from . "' AND o.create <= '" . $to . "' ";
        }
        $this->db->query('SET SQL_BIG_SELECTS=1');

        $sql="SELECT

                i.`customer_id`,

                i.`invoice_id` AS in_id,

                i.`sales_amount`,
				i.refrence_id,

                i.`invoice_total_amount`,
                i.invoice_deal_type,
                i.`qid`,
                i.invoice_totalDiscount,
                c.*,

                ex.invoice_id,

                ex.value,

                pamount,

                i.`invoice_date` AS i_date,

                SUM(pamount) AS amount,



                GROUP_CONCAT(ds.bs_user_id) AS cols,

                SUM(amount) AS salescommession,

                SUM(ex.value) AS sumexvalue,

                suminvoicepurchase AS iipp,

                sum_invoiceprice AS sum_invoiceprice,

                sum_invoicediscount,

                invoice_totalDiscount AS alldiscount,

                sumipurchase,
				cancel_sales,
		(sum_invoiceprice-invoice_totalDiscount) - pamount AS remaining,
                d.payment_type,u.fullname as empname,
                o.create AS orddate

              FROM

                `an_invoice` AS i

                 INNER JOIN `orders` AS o
                 ON o.invoice_id = i.invoice_id

                LEFT JOIN `bs_users` AS c

                  ON c.`userid` = i.`customer_id`
                LEFT JOIN `bs_users` AS u
                  ON u.`userid` = i.`user_id`
                LEFT JOIN

                  (SELECT

                    SUM(ip.`payment_amount`) AS pamount,

                    ip.`invoice_id`,

                    ip.payment_type as payment_type

                  FROM

                    `invoice_payments` AS ip

                    INNER JOIN `an_invoice` AS ii

                      ON ii.`invoice_id` = ip.`invoice_id`

                    LEFT JOIN `bs_users` AS c

                      ON c.`userid` = ii.`customer_id`

                  GROUP BY ip.`invoice_id`) d

                  ON d.invoice_id = i.`invoice_id`



                LEFT JOIN

                  (SELECT

                    bii.*,

                    SUM(

                      bii.invoice_item_price_purchase

                    ) AS suminvoicepurchase,

                    SUM(

                      bii.invoice_item_price * bii.invoice_item_quantity

                    ) AS sum_invoiceprice,

                    SUM(

                      bii.invoice_item_discount * bii.invoice_item_quantity

                    ) AS sum_invoicediscount,

                  SUM(bs_item.purchase_price*bii.invoice_item_quantity) AS sumipurchase

                  FROM bs_invoice_items AS bii

                    LEFT JOIN bs_item ON bs_item.itemid = bii.invoice_item_id

                  GROUP BY bii.`invoice_id`) iii

                  ON iii.invoice_id = i.invoice_id



                LEFT JOIN bs_sales_invoice_persons AS spi

                  ON spi.invoice_id = i.invoice_id

                LEFT JOIN bs_sales AS ds

                  ON ds.bs_sales_id = spi.sales_id



                LEFT JOIN an_expenses AS ex

                  ON ex.sinvoice_id = i.invoice_id



              WHERE i.cancel_sales ='1' AND ( i.invoice_type = 'itemscharges' OR i.invoice_type = 'pos')  ".$subq2."

              GROUP BY i.`invoice_id`

              ORDER BY i.`invoice_id` DESC  ";
        //echo $sql ;
        $query = $this->db->query($sql);

        return $query->result();
        ///ANd i.cancel_sales = 0
    }

    function getSaleInvocies_new($id = '') {
        $subq2 = '';

        if ($id) {

            $subq2 = " AND i.`user_id` = '" . $id . "'";

            $date = date('Y-m-d');
            $responseDate = getFromTo($date);
            $dateArr = json_decode($responseDate);

            $from = $dateArr->from;
            $to = $dateArr->to;
            $subq2.= " AND i.`invoice_date` >= '" . $from . "' AND i.`invoice_date` <= '" . $to . "' ";
        }
        $get = $this->input->get();



        if (isset($get['uid']) && $get['uid'] != "") {
            $subq2.= " AND i.`user_id` = '" . $get['uid'] . "'";
        }

        if (isset($get['ptype']) && $get['ptype'] != "") {
            if ($get['ptype'] == '2') {
                $subq2.= " AND i.`invoice_deal_type` = 'card'";
            } elseif ($get['ptype'] == '1') {
                $subq2.= " AND i.`invoice_deal_type` != 'card'";
            }
        }
        if ($get['frm'] != null && $get['to'] != null) {
            $from = getSingelDate('from', $get['frm']);
            $to = getSingelDate('to', $get['to']);
            $subq2.= " AND i.`invoice_date` >= '" . $from . "' AND i.`invoice_date` <= '" . $to . "' ";
        }

        if (isset($get['rec']) && $get['rec'] != "") {

            $subq2.=' AND (sum_invoiceprice- IFNULL(invoice_totalDiscount,0)) - IFNULL(pamount,0)>0';
        }


        if (isset($get['bid']) && $get['bid'] != "")
            $subq2.= ' AND i.branchid = ' . $get['bid'];

        if ($subq2 == '') {
            $date = date('Y-m-d');
            $responseDate = getFromTo($date);
            $dateArr = json_decode($responseDate);
            
            $from = $dateArr->from;
            $to = $dateArr->to;
            $subq2 = " AND i.`invoice_date` >= '" . $from . "' AND i.`invoice_date` <= '" . $to . "' ";
        }


        $this->db->query('SET SQL_BIG_SELECTS=1');

        $sql = "SELECT

                i.`customer_id`,
                  i.coupon_value,

                i.`invoice_id` AS in_id,

                i.`sales_amount`,
				i.refrence_id,
                i.invoice_totalDiscount,
                i.`invoice_total_amount`,
                i.invoice_deal_type,
                i.`qid`,

                c.*,

                ex.invoice_id,

                ex.value,

                pamount,

                i.`invoice_date` AS i_date,

                SUM(pamount) AS amount,

	        

                GROUP_CONCAT(ds.bs_user_id) AS cols,

                SUM(amount) AS salescommession,

                SUM(ex.value) AS sumexvalue,

                suminvoicepurchase AS iipp,

                sum_invoiceprice AS sum_invoiceprice,

                sum_invoicediscount,

                invoice_totalDiscount AS alldiscount,

                sumipurchase,
				cancel_sales,
		(sum_invoiceprice-invoice_totalDiscount) - pamount AS remaining,
                d.payment_type,u.fullname as empname

              FROM

                `an_invoice` AS i 

                LEFT JOIN `bs_users` AS c 

                  ON c.`userid` = i.`customer_id` 
                LEFT JOIN `bs_users` AS u
                  ON u.`userid` = i.`user_id`
                LEFT JOIN 

                  (SELECT 

                    SUM(ip.`payment_amount`) AS pamount,

                    ip.`invoice_id`, 

                    ip.payment_type as payment_type

                  FROM

                    `invoice_payments` AS ip 

                    INNER JOIN `an_invoice` AS ii 

                      ON ii.`invoice_id` = ip.`invoice_id` 

                    LEFT JOIN `bs_users` AS c

                      ON c.`userid` = ii.`customer_id` 

                  GROUP BY ip.`invoice_id`) d 

                  ON d.invoice_id = i.`invoice_id` 

		  

                LEFT JOIN 

                  (SELECT 

                    bii.*,

                    SUM(

                      bii.invoice_item_price_purchase

                    ) AS suminvoicepurchase,

                    SUM(

                      bii.invoice_item_price * bii.invoice_item_quantity

                    ) AS sum_invoiceprice,

                    SUM(

                      bii.invoice_item_discount * bii.invoice_item_quantity

                    ) AS sum_invoicediscount,

                  SUM(bs_item.purchase_price*bii.invoice_item_quantity) AS sumipurchase 

                  FROM bs_invoice_items AS bii 

                    LEFT JOIN bs_item ON bs_item.itemid = bii.invoice_item_id 

                  GROUP BY bii.`invoice_id`) iii 

                  ON iii.invoice_id = i.invoice_id 

		  

                LEFT JOIN bs_sales_invoice_persons AS spi 

                  ON spi.invoice_id = i.invoice_id 

                LEFT JOIN bs_sales AS ds 

                  ON ds.bs_sales_id = spi.sales_id 

                  

                LEFT JOIN an_expenses AS ex 

                  ON ex.sinvoice_id = i.invoice_id 

    

              WHERE  ( i.invoice_type = 'itemscharges' OR i.invoice_type = 'pos')  " . $subq2 . "

              GROUP BY i.`invoice_id` 

              ORDER BY i.`invoice_id` DESC  ";
        // echo $sql ;
        $query = $this->db->query($sql);

        return $query->result();
        ///ANd i.cancel_sales = 0
    }
    
      function getBrnachDetails() {
        $sql = "SELECT * FROM `bs_company_branch` AS cb WHERE cb.`branchid` = '1'";
        $q = $this->db->query($sql);

        if ($q->num_rows() > 0) {

            return $q->row();
        } else {

            return false;
        }
    }

    function getInvoicedetailsByid($id) {

        //$bs_userid = $this->session->userdata('bs_userid');

        $sql = "SELECT
  adval,
  aditemsnames,
   aditemsnamesar,
    u.`fullname` AS refno,
  it.`inovice_product_id`,
  o.*,
  item.*,
  it.*
FROM
  `orders` AS o
  INNER JOIN `an_invoice` AS i
    ON i.`invoice_id` = o.`invoice_id`
  INNER JOIN `bs_invoice_items` AS it
    ON it.`invoice_id` = i.`invoice_id`
  LEFT JOIN
    (SELECT
      pai.pos_item_id,
      pai.`invoice_itempk_id`,
      GROUP_CONCAT(pai.`additional_item_val`) AS adval,
      GROUP_CONCAT(ai.`item_eng_name`) AS aditemsnames,
      GROUP_CONCAT(ai.`item_ar_name`) AS aditemsnamesar
    FROM
      `pos_additional_items` AS pai
      INNER JOIN `additional_items` AS ai
        ON ai.`ad_item_id` = pai.`additional_item_id`
    GROUP BY pai.`invoice_itempk_id`) AS poi
    ON poi.invoice_itempk_id = it.`inovice_product_id`
  INNER JOIN `bs_item` AS item
    ON item.`itemid` = it.`invoice_item_id`
      INNER JOIN `bs_users` AS u
    ON u.`userid` = i.`customer_id`
WHERE o.`invoice_id` = '$id'
ORDER BY o.`order_id` ASC ";
        //  echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function getOrderidByInvID($id) {
        $sql = "SELECT * FROM `orders` AS o WHERE o.`invoice_id` = '" . $id . "'";
        $query = $this->db->query($sql);

        return $query->row()->order_id;
    }

    function getUsersList() {
        $sql = "SELECT u.`userid`,u.`fullname` FROM `bs_users` AS u WHERE u.`member_type` !='6'";
        $query = $this->db->query($sql);

        return $query->result();
    }

    function getSaleInvocies($id = '', $re = null) {

        if ($id) {

            //$subq = "WHERE u.`userid` = '" . $id . "'";
        }

        if ($re) {

            //$subq = "WHERE bsi.`return_sales` = '" . $re . "'";
        }

        /*

          $sql = "SELECT

          u.`fullname` AS customername,

          u.*,

          bsi.*

          FROM

          `an_invoice` AS bsi

          INNER JOIN `bs_users` AS u ON u.`userid` = bsi.`customer_id`

          " . $subq . "   GROUP BY bsi.invoice_id";



          $query = $this->db->query($sql);

          return $query->result();

         *

         */
        $sql = "SET SQL_BIG_SELECTS=1";
        $query = $this->db->query($sql);
        $sql = "SELECT 

                i.`customer_id`,

                i.`invoice_id` AS in_id,

                i.`sales_amount`,

                i.`invoice_total_amount`,

                i.`qid`,

                c.*,

                ex.invoice_id,

                ex.value,

                pamount,

                i.`invoice_date` AS i_date,

                SUM(pamount) AS amount,

                sales_amount - pamount AS remaining,

                GROUP_CONCAT(ds.bs_user_id) AS cols,

                SUM(amount) AS salescommession,

                SUM(ex.value) AS sumexvalue,

                suminvoicepurchase AS iipp,

                sum_invoiceprice AS sum_invoiceprice,

                sum_invoicediscount,

                invoice_totalDiscount AS alldiscount,

                sumipurchase,

                payment_type

              FROM

                `an_invoice` AS i 

                LEFT JOIN `bs_users` AS c 

                  ON c.`userid` = i.`customer_id` 

                LEFT JOIN 

                  (SELECT 

                    SUM(ip.`payment_amount`) AS pamount,

                    ip.`invoice_id`, 

                    ip.payment_type as payment_type

                  FROM

                    `invoice_payments` AS ip 

                    INNER JOIN `an_invoice` AS ii 

                      ON ii.`invoice_id` = ip.`invoice_id` 

                    INNER JOIN `bs_users` AS c 

                      ON c.`userid` = ii.`customer_id` 

                  GROUP BY ip.`invoice_id`) d 

                  ON d.invoice_id = i.`invoice_id` 

                LEFT JOIN 

                  (SELECT 

                    bii.*,

                    SUM(

                      bii.invoice_item_price_purchase

                    ) AS suminvoicepurchase,

                    SUM(

                      bii.invoice_item_price * bii.invoice_item_quantity

                    ) AS sum_invoiceprice,

                    SUM(

                      bii.invoice_item_discount * bii.invoice_item_quantity

                    ) AS sum_invoicediscount,

                    bs_item.purchase_price*bii.invoice_item_quantity AS sumipurchase 

                  FROM bs_invoice_items AS bii 

                    LEFT JOIN bs_item ON bs_item.itemid = bii.invoice_item_id 

                  GROUP BY bii.`invoice_id`) iii 

                  ON iii.invoice_id = i.invoice_id 

                LEFT JOIN bs_sales_invoice_persons AS spi 

                  ON spi.invoice_id = i.invoice_id 

                LEFT JOIN bs_sales AS ds 

                  ON ds.bs_sales_id = spi.sales_id 

                LEFT JOIN an_expenses AS ex 

                  ON ex.invoice_id = i.invoice_id 

          where i.status='1' and  i.invoice_type = 'itemscharges' 

          GROUP BY i.`invoice_id`

          ORDER BY i.`invoice_id` DESC

            ";


        echo $sql;
        $query = $this->db->query($sql);

        return $query->result();
    }

    public function users_list() {

        $permission_type_id = $this->input->post('permission_type_id');

        $member_type = get_member_type();

        $query = "SELECT 

				  a.userid,

				  a.`fullname`,

				  a.`username`,

				  IF(a.`employeecode`='','-----',a.`employeecode`) AS employeecode,

				  IF(a.`office_number`='','-----',a.`office_number`) AS office_number,

				  IF(a.`fax_number`='','-----',a.`fax_number`) AS fax_number,

				  IF(a.`phone_number`='','-----',a.`phone_number`) AS phone_number,

				  IF(a.`status`='A','Active','Deactive') AS `status`,  

				  b.`permissionhead`,

				  c.`fullname` AS ownername 

				FROM

				  bs_users AS a,

				  bs_users As c,

				  bs_permission_type AS b WHERE a.`member_type` = b.`permission_type_id` AND b.`permission_type_id`=6 AND a.ownerid=c.userid AND ";

        if ($member_type != '5') {

            $query .= " (a.ownerid='" . $this->session->userdata('bs_ownerid') . "' OR a.ownerid='" . $this->session->userdata('bs_userid') . "') AND ";
        }



        $query .= " a.userid > 0 Order by a.fullname ASC";



        $q = $this->db->query($query);

        return $q->result();
    }

    function p_invoice($id = null) {

        /*

          $this->db->select('an_invoice.*');

          $this->db->select('bs_users.*');

          $this->db->select('bs_company.*');

          $this->db->select('company_logos.*');

          $this->db->where('an_invoice.invoice_id', $id);

          $this->db->join('bs_company','bs_company.companyid=an_invoice.companyid', 'LEFT');

          $this->db->join("bs_users","bs_users.userid='254' ", "LEFT");

          $this->db->join('company_logos','company_logos.company_id=bs_company.companyid', 'LEFT');

         */

        $sql = "SELECT `an_invoice`.*, `bs_users`.*, `bs_company`.*, `company_logos`.*,

		bs_users.email_address as uemail_address,bs_users.office_number as uoffice_number,bs_users.phone_number as uphone_number

		FROM (`an_invoice`) 

		LEFT JOIN `bs_company` ON `bs_company`.`companyid`=`an_invoice`.`companyid` 

		LEFT JOIN `bs_users` ON `bs_users`.`userid`= an_invoice.customer_id

		LEFT JOIN `company_logos` ON `company_logos`.`company_id`=`bs_company`.`companyid` WHERE `an_invoice`.`invoice_id` = '$id'";



        //$this->db->order_by();

        $Q = $this->db->query($sql);

        if ($Q->num_rows() > 0) {
            return $Q->row();
        } else {
            return false;
        }
    }

    function getMoreItems($invitemid) {

        $sql = "SELECT
  ai.*
FROM
  `more_items` AS mi
  LEFT JOIN `link_items` AS li
    ON li.`link_id` = mi.`link_id`
  LEFT JOIN `additional_items` AS ai
    ON ai.`ad_item_id` = li.`additional_item_id`
WHERE mi.`invoice_item_id` = '" . $invitemid . "'";


        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            return $Q->result();
        } else {
            return false;
        }
    }

    function p_invoices($id = null) {



        $this->db->select('bs_invoice_items.*,bs_item.*,units.*');

        $this->db->where('bs_invoice_items.invoice_id', $id);

        $this->db->join('bs_item', 'bs_item.itemid=bs_invoice_items.invoice_item_id', 'LEFT');
        $this->db->join('units', 'units.unit_id=bs_item.type_unit', 'LEFT');
        //$this->db->order_by();

        $Q = $this->db->get('bs_invoice_items');

        return $Q->result();
    }

    public function add_new_customer($employeecode = null) {

        $data = $this->input->post();

        $userid = $this->input->post('userid');



        $data['employeecode'] = $employeecode;



        $data['upassword'] = md5($this->input->post('upassword'));

        unset($data['sub_mit'], $data['sub_reset'], $data['userid'], $data['confpassword']);



        if ($userid != '') {

            $this->db->where('userid', $userid);

            $this->db->update($this->config->item('table_users'), $data);

            do_redirect('add_invoice?e=10');
        } else {

            $this->db->insert($this->config->item('table_users'), $data);

            do_redirect('add_invoice?e=10');
        }
    }

    function openbalance_old($id) {



        $this->db->select('payments.*');

        $this->db->where('payments.refrence_id', $id);

        $this->db->where('payments.refernce_type', 'opening');

        //$this->db->where('bs_users_bank_transaction.payment_type',"");
        //$this->db->join('bs_item','bs_item.itemid=bs_invoice_items.invoice_item_id','LEFT');
        //$this->db->order_by();

        $Q = $this->db->get('payments');



        return $Q->row()->payment_amount;
    }

    function openbalance($id) {

        /* $sql="

          SELECT SUM( p.`payment_amount` ) AS totalpayment, total_invoice_payment

          FROM  `payments` AS p

          LEFT JOIN (



          SELECT i.customer_id, SUM( ip.`payment_amount` ) AS total_invoice_payment

          FROM  `invoice_payments` AS ip

          INNER JOIN  `an_invoice` AS i ON i.`invoice_id` = ip.`invoice_id`

          WHERE i.`customer_id` =  '$id'

          ) AS ip ON ip.`customer_id` = p.`refrence_id`

          WHERE p.`refrence_id` =  '$id'

          AND p.`payment_type` =  'receipt'";

         */

        $sql = "

                SELECT 

                  SUM(p.`payment_amount`)  AS totalpayment,

                  SUM(totalcharges) AS totalcharges ,

                  SUM(p.`payment_amount`)-SUM(totalcharges) AS remain 

                FROM

                  `payments` AS p 

                  LEFT JOIN 

                    (SELECT 

                      ip.`payment_id`,

                          SUM(ip.`payment_amount`) AS totalcharges



                    FROM

                      `invoice_payments` AS ip 

                      INNER JOIN `an_invoice` AS i 

                        ON i.`invoice_id` = ip.`invoice_id` 

                    WHERE i.`customer_id` = '" . $id . "') AS ip 

                    ON ip.`payment_id` = p.`id` 

                    WHERE p.`refrence_id` = '" . $id . "' AND p.`payment_type` = 'receipt' ";



        //$sql="select sale_payments.*,bs_user.userid,bs_user.openbalance from sale_payments ";
        //$this->db->select('sale_payments.payment_amount,sale_payments.customer_id');
        //$this->db->where('sale_payments.customer_id', $id);
        //$this->db->join('bs_users','bs_users.userid=sale_payments.customer_id','left');
        //$q = $this->db->get('sale_payments');

        $total = 0;

        $q = $this->db->query($sql);



        if ($q->num_rows() > 0) {

            return $q->row();
        } else {

            return false;
        }
    }

    function get_invoice_total($id = null) {

        /* $sql = "SELECT 

          i.`customer_id`,

          i.`invoice_id` AS invoice_id,

          i.`sales_amount`,

          pamount,

          i.`invoice_date`,

          SUM(pamount) AS amount ,

          SUM(i.invoice_total_amount) AS invoice_total_amount

          FROM

          `an_invoice` AS i

          INNER JOIN `bs_users` AS c

          ON c.`userid` = i.`customer_id`

          LEFT JOIN

          (SELECT

          SUM(ip.`payment_amount`) AS pamount,

          ip.`invoice_id`

          FROM

          `invoice_payments` AS ip

          INNER JOIN `an_invoice` AS ii

          ON ii.`invoice_id` = ip.`invoice_id`

          INNER JOIN `bs_users` AS c

          ON c.`userid` = ii.`customer_id`

          WHERE c.`userid` = '$id'

          GROUP BY ip.`invoice_id`) d

          ON d.invoice_id = i.`invoice_id`

          WHERE i.customer_id = '$id'";

         */

        $sql = "SELECT 

                  SUM(p.`payment_amount`)  AS totalpayment,

                  SUM(totalcharges) AS totalcharges 

                FROM

                  `payments` AS p 

                  LEFT JOIN 

                    (SELECT 

                      ip.`payment_id`,

                          SUM(ip.`payment_amount`) AS totalcharges



                    FROM

                      `invoice_payments` AS ip 

                      INNER JOIN `an_invoice` AS i 

                        ON i.`invoice_id` = ip.`invoice_id` 

                    WHERE i.`customer_id` = '" . $id . "') AS ip 

                    ON ip.`payment_id` = p.`id` 

                    WHERE p.`refrence_id` = '" . $id . "' AND p.`payment_type` = 'receipt'";

        $q = $this->db->query($sql);



        if ($q->num_rows() > 0) {

            return $q->row();
        } else {

            return false;
        }
    }

    function get_calculation($id) {

        /*

          $sql="SELECT

          SUM(i.`sales_amount`) AS totalpayment,

          SUM(i.`sales_amount`) - pamount AS totalremaining

          FROM

          `an_invoice` AS i

          LEFT JOIN

          (SELECT

          (p.`payment_amount`) AS pamount,

          ip.`invoice_id`

          FROM

          `invoice_payments` AS ip

          INNER JOIN `an_invoice` AS ii

          ON ii.`invoice_id` = ip.`invoice_id`

          INNER JOIN `bs_users` AS c

          ON c.`userid` = '$id'

          INNER JOIN `payments` AS p

          ON p.`id` = ip.`payment_id`

          GROUP BY ii.`customer_id`,

          p.id) d

          ON d.invoice_id = i.`invoice_id`

          AND i.`invoice_status` = '0'

          WHERE i.`customer_id` = '$id' ";

         */

        /*

          $sql="SELECT

          sales,

          SUM(i.`sales_amount`) AS totalpayment,

          SUM(pamount) AS totalpaid



          FROM

          `an_invoice` AS i

          LEFT JOIN

          (SELECT

          (p.`payment_amount`) AS pamount,

          ip.`invoice_id`

          FROM

          `invoice_payments` AS ip

          INNER JOIN `an_invoice` AS ii

          ON ii.`invoice_id` = ip.`invoice_id`

          INNER JOIN `bs_users` AS c

          ON c.`userid` = '$id'

          INNER JOIN `payments` AS p

          ON p.`id` = ip.`payment_id`

          GROUP BY ii.`customer_id`,

          p.id) d

          ON d.invoice_id = i.`invoice_id`

          AND i.`invoice_status` = '0'

          LEFT JOIN

          (SELECT

          SUM(ii.`sales_amount`) AS sales,

          ii.`customer_id`

          FROM

          `an_invoice` AS ii

          GROUP BY ii.`customer_id`) AS id

          ON id.customer_id = i.`customer_id`

          WHERE i.`customer_id` = '$id' ";

         */

        $sql = "SELECT 

                c.`userid`,

                IF(

                  totalpayment IS NULL,

                  0,

                  totalpayment

                ) AS totalpayment,

                IF(

                  totalinvpayment IS NULL,

                  0,

                  totalinvpayment

                ) AS totalinvpayment,

                IF(

                  totalsales IS NULL,

                  0,

                  totalsales

                ) AS totalsales,

                IF(

                  totalsales IS NULL,

                  0,

                  totalsales

                )-IF(

                  totalinvpayment IS NULL,

                  0,

                  totalinvpayment

                ) AS remainng,

                IF(

                  totalpayment IS NULL,

                  0,

                  totalpayment

                ) -IF(

                  totalinvpayment IS NULL,

                  0,

                  totalinvpayment

                ) AS balance

              FROM

                `bs_users` AS c 

                LEFT JOIN 

                  (SELECT 

                    SUM(p.`payment_amount`) AS totalpayment,

                    p.`refrence_id` 

                  FROM

                    `payments` AS p 

                  WHERE p.`refrence_id` = '$id') AS pp 

                  ON pp.refrence_id = c.userid 

                LEFT JOIN 

                  (SELECT 

                    SUM(ip.`payment_amount`) AS totalinvpayment,

                    ii.`customer_id` 

                  FROM

                    `invoice_payments` AS ip 

                    INNER JOIN `an_invoice` AS ii 

                      ON ii.`invoice_id` = ip.`invoice_id` 

                  WHERE ii.`customer_id` = '$id') AS ipp 

                  ON ipp.customer_id = c.userid 

                   LEFT JOIN 

                  (

              SELECT 

                SUM(i.`sales_amount`) AS totalsales,

                i.`customer_id` 

              FROM

                `an_invoice` AS i 

              WHERE i.`customer_id` = '$id') AS inv 

                  ON inv.customer_id = c.userid 

              WHERE c.`userid` = '$id' 

              ";



        $q = $this->db->query($sql);



        if ($q->num_rows() > 0) {

            return $q->row();
        } else {

            return false;
        }
    }

    function invoice($id = null) {

        /*

          $this->db->select('an_invoice.*,invoice_payments.*');

          $this->db->where('an_invoice.customer_id',$id);

          //$this->db->where('bs_users_bank_transaction.payment_type',"");

          $this->db->join('invoice_payments','invoice_payments.invoice_id=an_invoice.invoice_id','LEFT');

          //$this->db->order_by();

          $Q=$this->db->get('an_invoice');

          //return $Q->result();



          $fquantity = 0;

          if($Q->result()){



          foreach($Q->result() as $quantity){



          $fquantity+=$quantity->invoice_total_amount;

          $fquantity2+=$quantity->payment_amount;

          }



          //var_dump($Q->result());

          }

          return array('fquantity'=>$fquantity,'fquantity2'=>$fquantity2);

         * 

         */



        $sql = "SELECT 

                i.`customer_id`,

                i.`invoice_id` AS invoice_id,

                i.`sales_amount`,

                i.`invoice_total_amount`,

                pamount,

                i.`invoice_date`,

                SUM(pamount) AS amount 

              FROM

                `an_invoice` AS i 

                INNER JOIN `bs_users` AS c 

                  ON c.`userid` = i.`customer_id` 

                LEFT JOIN 

                  (SELECT 

                    SUM(ip.`payment_amount`) AS pamount,

                    ip.`invoice_id` 

                  FROM

                    `invoice_payments` AS ip 

                    INNER JOIN `an_invoice` AS ii 

                      ON ii.`invoice_id` = ip.`invoice_id` 

                    INNER JOIN `bs_users` AS c 

                      ON c.`userid` = ii.`customer_id` 

                  WHERE c.`userid` = " . $id . " 

                  GROUP BY ip.`invoice_id`) d 

                  ON d.invoice_id = i.`invoice_id` 

              WHERE i.customer_id = " . $id . "  AND i.`invoice_status` = '0'

              GROUP BY i.`invoice_id` ";







        $total = 0;

        $pamount = 0;

        $q = $this->db->query($sql);



        if ($q->num_rows() > 0) {

            foreach ($q->result() as $value) {



                $total+=$value->invoice_total_amount;

                $pamount+=$value->pamount;
            }

            return array('total' => $total, 'pamount' => $pamount);
        } else {

            return false;
        }
    }

    function get_payment_invoice($id) {
        
    }

    function invoice_data($id) {

        /*

          $sql = "SELECT

          i.`customer_id`,

          i.`invoice_id` AS invoice_id,

          i.`sales_amount`,

          i.`invoice_total_amount`,

          i.`companyid` as company,

          i.`branchid` as branch,

          i.`invoice_totalDiscount` as invoice_totalDiscount,

          c.*,

          pamount,

          bii.invoice_item_discount_type AS discount_type,

          i.`invoice_date` AS i_date,

          SUM(pamount) AS amount,

          SUM(invoice_item_discount) AS iidiscount,

          SUM(invoice_item_price) AS iiprice,

          SUM(invoice_item_price_purchase) AS iiprice_purchase,

          sales_amount - pamount AS remaining,

          GROUP_CONCAT(ds.bs_user_id) AS cols,

          GROUP_CONCAT(bii.inovice_product_id) AS itemcols,

          SUM(bs_sales_commession) AS salescommession

          FROM

          `an_invoice` AS i

          INNER JOIN `bs_users` AS c

          ON c.`userid` = i.`customer_id`

          LEFT JOIN

          (SELECT

          SUM(ip.`payment_amount`) AS pamount,

          ip.`invoice_id`

          FROM

          `invoice_payments` AS ip

          INNER JOIN `an_invoice` AS ii

          ON ii.`invoice_id` = ip.`invoice_id`

          INNER JOIN `bs_users` AS c

          ON c.`userid` = ii.`customer_id`

          GROUP BY ip.`invoice_id`) d

          ON d.invoice_id = i.`invoice_id`



          LEFT JOIN bs_invoice_items AS bii

          ON bii.invoice_id = i.invoice_id

          LEFT JOIN bs_sales_invoice_persons AS spi

          ON spi.invoice_id = i.invoice_id

          LEFT JOIN bs_sales AS ds

          ON ds.bs_sales_id = spi.sales_id



          WHERE i.invoice_id='$id'

          GROUP BY i.`invoice_id`";

         */

        $sql = "SELECT 

                i.*,

                c.`userid`,

                c.`fullname`,

                c.`email_address`,

                c.`phone_number`,

                ino.*

              FROM

                `an_invoice` AS i 

                INNER JOIN `bs_users` AS c 

                  ON c.`userid` = i.`customer_id` 

                  

                LEFT JOIN bs_invoice_items AS bii 

                  ON bii.invoice_id = i.invoice_id 

                  

                LEFT JOIN bs_sales_invoice_persons AS spi 

                  ON spi.invoice_id = i.invoice_id 

                  

                LEFT JOIN bs_sales AS ds 

                  ON ds.bs_sales_id = spi.sales_id 

                  

                LEFT JOIN invoice_notes AS ino 

                  ON ino.invoice_id = i.invoice_id 

                  

              WHERE i.invoice_id='$id'    

              ";

        $query = $this->db->query($sql);

        return $query->result();
    }

    function updateRawData($arr, $pid, $qty,$store_id) {
        foreach ($arr as $item) {
            $query = $this->db->get_where('store_value', array('item_id' => $item->raw_id));
            if ($query->num_rows() > 0) {
                $quantity = $item->qty * $qty;
                echo $item->qty . " = " . $qty;
                $id = $query->row()->item_id;
                $query = "update store_value set totalq = totalq+$quantity where item_id = $id and store_id = $store_id";
                $this->db->query($query);
            }
        }
    }

    function delete_pay_order_item($id) {
//revert minus from store
        $query = "SELECT bs_invoice_items.*,large_price,medium_price,small_price FROM `bs_invoice_items` INNER JOIN `bs_item` ON itemid = invoice_item_id WHERE inovice_product_id = $id";
        $sql = $this->db->query($query)->row();

        $invSoledData = $this->getSoldItemsByInvId($sql->invoice_id)[0];
        $itemid = $sql->invoice_item_id;
        $qty = $sql->invoice_item_quantity;
        if ($sql->invoice_item_price == $sql->large_price) {
            $itemtype = 'large';
        } elseif ($sql->invoice_item_price == $sql->medium_price) {
            $itemtype = 'medium';
        } elseif ($sql->invoice_item_price == $sql->small_price) {
            $itemtype = 'small';
        }
        $query = $this->db->get_where('dish_content', array('item_id' => $itemid, 'size' => $itemtype));
        if ($query->num_rows() > 0) {
            $this->updateRawData($query->result(), $sql->invoice_id, $qty, $invSoledData['store_id']);
        }


        $this->db->where('inovice_product_id', $id);
        $this->db->delete('bs_invoice_items');
    }

    function get_files($id) {

        $sql = "SELECT 

                invoice_document.*

              FROM

                `invoice_document` 



                  

                 WHERE invoice_document.invoice_id='$id'    

              ";

        $query = $this->db->query($sql);

        return $query->result();
    }

    function getCustomersDues($customer_id) {

        $sql = "SELECT 

				  i.`customer_id`,

				    i.invoice_id,

				  SUM(sales_amount) AS sales_amount,

				  SUM(pamount) AS amount 

				FROM

				  `an_invoice` AS i 

				  INNER JOIN `bs_users` AS c 

					ON c.`userid` = i.`customer_id` 

				  LEFT JOIN 

					(SELECT 

					  SUM(ip.`payment_amount`) AS pamount,

					  ip.`invoice_id` 

					FROM

					  `invoice_payments` AS ip 

					  INNER JOIN `an_invoice` AS ii 

						ON ii.`invoice_id` = ip.`invoice_id` 

					  INNER JOIN `bs_users` AS c 

						ON c.`userid` = ii.`customer_id` 

					WHERE c.`userid` = " . $customer_id . " 

					GROUP BY ii.`customer_id`) d 

					ON d.invoice_id = i.`invoice_id` 

				WHERE i.customer_id = " . $customer_id . "  AND i.`invoice_status` = '0'

				GROUP BY i.`customer_id`";

        $q = $this->db->query($sql);

        return $q->row();
    }

    public function storelist($id = '', $pid = '') {

        /*

          if($id){$ex="where s.storeid='$id' and bi.item_id='$pid'";}else{$ex="";}

          $sql = "SELECT

          s.*,

          bs_company_branch.branchid,

          bs_company_branch.branchname,

          bs_company.companyid,

          bs_company.company_name,

          sum_purchase,

          sum_sale

          FROM

          `bs_store` AS s



          LEFT JOIN

          (SELECT

          si.*,

          SUM(si.`quantity`) AS sum_purchase

          FROM

          bs_store_items AS si

          LEFT JOIN bs_store AS ss ON ss.storeid=si.`store_id`



          WHERE si.purchase_id != '0' AND si.`store_id` = ss.`storeid` GROUP BY si.`store_id` ) bi

          ON bi.`store_id` = s.`storeid`



          LEFT JOIN

          (SELECT

          si.*,

          SUM(si.`quantity`) AS sum_sale

          FROM

          bs_store_items AS si

          LEFT JOIN bs_store AS ss ON ss.storeid=si.`store_id`



          WHERE si.receipt_id != '0' AND si.`store_id` = ss.`storeid` GROUP BY si.`store_id` ) bi2

          ON bi2.`store_id` = s.`storeid`









          LEFT JOIN bs_company

          ON bs_company.companyid = s.companyid

          LEFT JOIN bs_company_branch

          ON bs_company_branch.branchid = s.branchid



          $ex



          GROUP BY s.`storeid`

          ORDER BY s.`storeid` DESC  "; */



        if ($id) {
            $ex = "where s.storeid='$id'";
            if ($pid) {
                $ex.="and ssi.item_id='$pid'";
            }
        } else {
            $ex = "";
        }

        $sql = "SELECT 

                s.*,

                bs_company_branch.branchid,

                bs_company_branch.branchname,

                bs_company.companyid,

                bs_company.company_name,

                ssquantity,

                SUM(ssi.`quantity`) AS squantity

              FROM

                `bs_store` AS s 



              LEFT JOIN bs_store_items AS ssi ON ssi.`store_id`=s.`storeid`



                LEFT JOIN 

                  (SELECT 

                    *,

                    SUM(soldi.`soled_quantity`) AS ssquantity 

                  FROM

                    `store_soled_items` AS soldi) ss 

                  ON `ss`.`store_id` = '$id' and ss.item_id='$pid' 



                LEFT JOIN bs_company 

                  ON bs_company.companyid = s.companyid 

                LEFT JOIN bs_company_branch 

                  ON bs_company_branch.branchid = s.branchid 

                  

                   $ex



              GROUP BY s.`storeid` 

              ORDER BY s.`storeid` DESC  ";



        $q = $this->db->query($sql);

        return $q->result();
    }

//----------------------------------------------------------------------	
}
