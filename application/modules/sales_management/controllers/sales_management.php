<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sales_management extends CI_Controller {
    /*

     * Properties

     */

    private $_data = array();

//----------------------------------------------------------------------

    /*

     * Constructor

     */

    public function __construct() {

        parent::__construct();



        //load Home Model
        $this->load->helper('bs_helper');
        $this->load->model('sales_management_model', 'sales_management');

        $this->lang->load('main', get_set_value('site_lang'));

        $this->_data['udata'] = userinfo_permission();
    }

//----------------------------------------------------------------------



    /*

     * Home Page

     */

    public function index() {

        // Load Home View

        $this->load->view('sales-management', $this->_data);
    }

    public function newinvoice() {

        // Load Home View

        $this->load->view('new-add-invoice');
    }

    public function sales() {

        // Load Home View

        $this->_data['sales'] = $this->sales_management->getSalesPersonsData();

        $this->_data['inside'] = 'sales-management';

        $this->load->view('common/main', $this->_data);
    }

    function delete_sales_order() {
        $purchase_item_id = post('sales_item_id');
        $this->sales_management->delete_pay_order_item($purchase_item_id);
        echo true;
    }

//----------------------------------------------------------------------

    public function add_new_customer() {
        $this->sales_management->add_new_customer();
    }

    function getMostSellingItems($tp = '') {

        $this->_data['sales_items'] = $this->sales_management->getMostSellingItem($tp);
        $this->_data['inside'] = 'items_selling_list';
        $this->_data['tp'] = $tp;
        $this->load->view('common/main', $this->_data);
    }

    function edit_print_order($invid) {

        $this->_data['invoice'] = $this->sales_management->invoice_data($invid);
        $this->_data['inside'] = 'edit-invoice-print';
        $this->_data['id'] = $invid;

        $this->load->view('common/main', $this->_data);
    }

    function addsales($id = null) {



        if ($this->input->post()) {

            $data = array();

            //$data = $this->input->post();
            //print_r($data);
            //exit;
            //unset($data['submit_customer']);
            //$data['userid']=  post('bs_sales_id');

            $data['bs_user_id'] = post('bs_user_id');

            $data['bs_sales_commession_type'] = post('bs_sales_commession_type');

            $data['bs_sales_commession_how'] = post('bs_sales_commession_how');

            $data['bs_sales_commession'] = post('bs_sales_commession');



            // insert data into database

            if (post('userid') != '') {

                $this->_data['sdata'] = $this->sales_management->get_sales(post('userid'));

                $this->sales_management->update('bs_sales', $data, post('userid'));

                redirect(base_url() . 'sales_management/addsales/' . post('userid') . "?e=10");
            } else {

                $this->sales_management->inserData('bs_sales', $data);

                redirect(base_url() . 'sales_management/addsales/' . $id . "?e=11");
            }



            //redirect(base_url().'customers/options');
            //exit();
        } else {

            if ($id != '') {

                $this->_data['sdata'] = $this->sales_management->get_sales($id);
            }



            //$this->_data['emCode'];
            // Load Home View

            $this->_data['users'] = $this->sales_management->get_all_users();

            //$this->load->view('add-customer-form', $this->_data);

            $this->_data['inside'] = 'addsales';

            $this->load->view('common/main', $this->_data);
        }
    }

    /*

     * Add New Invoice

     */

    public function add_invoice() {

        // Load Home View
        //$this->_data['products'] = $this->sales_management->product_list('');

        $this->_data['sales'] = $this->sales_management->getSalesPersonsData();

        $this->_data['customers'] = $this->sales_management->users_list();



        $this->_data['bankAccount'] = $this->sales_management->getBankAccounts();

        $this->load->model('purchase_management/purchase_management_model', 'purchase_management');

        $this->_data['categories'] = $this->purchase_management->getCategories();



        //echo "asd";
        //echo "<pre>";
        //print_r($this->_data['customers']);
        //$this->load->view('add-invoice', $this->_data);

        $this->_data['inside'] = 'add-invoice';

        $this->load->view('common/main', $this->_data);
    }

    function edit_invoice($id) {

        $this->_data['invoice'] = $this->sales_management->invoice_data($id);
    }

    function editinvoice($id) {

        // Load Home View

        /*

          $this->_data['invoice'] = $this->sales_management->invoice_data($id);





          $this->_data['sales'] = $this->sales_management->getSalesPersonsData();

          $this->_data['customers'] = $this->sales_management->users_list();



          $this->_data['bankAccount'] = $this->sales_management->getBankAccounts();

          $this->load->model('purchase_management/purchase_management_model', 'purchase_management');

          $this->_data['categories'] = $this->purchase_management->getCategories();



          //echo "asd";

          //echo "<pre>";

          //print_r($this->_data['customers']);

          //$this->load->view('add-invoice', $this->_data);

          $this->_data['inside'] = 'edit-invoice';

          $this->load->view('common/main', $this->_data);

         */

        $this->load->model('purchase_management/purchase_management_model', 'purchase_management');

        $this->_data['categories'] = $this->purchase_management->getCategories();

        $this->_data['invoice'] = $this->sales_management->invoice_data($id);

        $this->_data['id'] = $id;

        $this->_data['inside'] = 'edit-invoice';

        $this->load->view('common/main', $this->_data);
    }

    function update_invoice() {
        $post = post();
        if ($post) {
            //$order_id$post['order_id'];	
            //$items = $post['items'];
            if ($post['items']) {
                $items = $post['items'];
                $len = count($items['productId']);
                for ($counter = 0; $counter < $len; $counter++) {

                    $pitem['invoice_id'] = $post['invocie_id'];
                    $pitem['invoice_item_quantity'] = $items['quantity'][$counter];
                    $pitem['invoice_item_id'] = $items['productId'][$counter];
                    $pitem['invoice_item_store_id'] = $items['store_id'][$counter];
                    $pitem['invoice_item_price'] = $items['productTotal'][$counter];
                    $pitem['customer_id'] = $post['customerid'];

                    $invitem_id_item = $this->db->insert('bs_invoice_items', $pitem);
                    $counter++;
                }
            }
            if ($post['purchase_item_quantity']) {
                $pitemQuantity = $post['purchase_item_quantity'];
                $purchase_item_price = $post['purchase_item_price'];
                $purchase_item_aedprice = $post['purchase_item_aedprice'];

                $totalpuchaseprice = 0;
                $totalpuchaseaedprice = 0;
                if ($pitemQuantity) {

                    foreach ($pitemQuantity as $in => $pq) {
                        $pItem['invoice_item_quantity'] = $pq;
                        $this->db->where('inovice_product_id', $in);
                        $this->db->update('bs_invoice_items', $pItem);

                        $totalpuchaseprice = $totalpuchaseprice + $purchase_item_price[$in] * $pq;
                        //	$totalpuchaseaedprice = $totalpuchaseaedprice+$purchase_item_aedprice[$in]*$pq;
                    }
                    // $post['order_id'];


                    $pOrders['invoice_total_amount'] = $totalpuchaseprice;
                    $this->db->where('invoice_id', $post['invocie_id']);
                    $this->db->update('an_invoice', $pOrders);
                    if ($post['newpayment']) {
                        $p['payment_type'] = 'receipt';
                        $p['refernce_type'] = 'against';
                        $p['payment_amount'] = $post['newpayment'];
                        $p['refrence_id'] = $post['invocie_id'];
                        $paymentid = $this->sales_management->inserData('payments', $p);



                        $inp['payment_amount'] = $post['newpayment'];
                        $inp['payment_id'] = $paymentid;
                        $inp['invoice_id'] = $post['invocie_id'];
                        $this->sales_management->inserData('invoice_payments', $inp);
                        //$this->db->where('invoice_id', $post['invocie_id']);
                        // $this->db->update('invoice_payments', $inp);
                    }
                }
            }
            redirect(base_url() . 'sales_management/sales_invoices?e=10');
        }

        redirect(base_url() . 'sales_management/sales_invoices?e=10');
    }

    function update_sinvoice() {

        $post = post();
        echo "<pre>";
        print_r($post);
    }

//----------------------------------------------------------------------



    /*

     * Sale Invoice

     */

    function update_cancel_invoice() {
        $post = post();

        $coupon = $this->sales_management->getCoupon($post['invocie_id']);
        $orderd_items = $this->sales_management->getItemIds($post['purchase_item_quantity']);
        $arr = pocess_coupon($coupon, $orderd_items[0], $orderd_items[1], $orderd_items[2]);

        $posInvoice = $this->sales_management->getInvoiceDataById($post['invocie_id']);
        unset($posInvoice['invoice_id']);
        $posInvoice['refrence_id'] = $post['invocie_id'];
        $posInvoice['coupon_value'] = $arr[1]; // coupon process

        $insertid = $this->sales_management->inserData('an_invoice', $posInvoice);

        $invItemData = $this->sales_management->getInvoiceItemsDataById($post['invocie_id']);
        $items_arr = [];
        foreach ($invItemData as $key => $value) {
            $items_arr[$value['inovice_product_id']] = $value['invoice_item_id'];
        }

        if ($invItemData) {
            foreach ($invItemData as $invItems) {
                $invItems['invoice_id'] = $insertid;
                unset($invItems['inovice_product_id']);
                $this->sales_management->inserData('bs_invoice_items', $invItems);
            }
        }

        $invItemPaymentData = $this->sales_management->getInvoicePaymentsById($post['invocie_id']);
        if ($invItemPaymentData) {
            foreach ($invItemPaymentData as $invpItems) {
                $invpItems['invoice_id'] = $insertid;
                $pmData = $this->sales_management->getPaymentDataById($invpItems['payment_id']);
                unset($pmData['id']);
                $invpItems['payment_id'] = $this->sales_management->inserData('payments', $pmData);
                unset($invpItems['inv_payment_id']);
                $this->sales_management->inserData('invoice_payments', $invpItems);
            }
        }

        // coupon process
        if ($arr[2]) {// if there is a coupon dicount
            $cop['invoice_id'] = $insertid;
            $cop['coupon_id'] = $arr[2];
            $cop['customer_id'] = $posInvoice['customer_id'];
            $this->sales_management->inserData('invoice_coupon', $cop);
        }

        $pitemQuantity = $post['purchase_item_quantity'];
        if ($pitemQuantity) {
            $purchase_item_price = $post['purchase_item_price'];
            $totalpuchaseprice = 0;
            foreach ($pitemQuantity as $in => $pq) {
                $pItem['invoice_item_quantity'] = $pq;
                $this->db->where(array('invoice_item_id' => $items_arr[$in], 'invoice_id' => $insertid));
                $this->db->update('bs_invoice_items', $pItem);
                $totalpuchaseprice += $purchase_item_price[$in] * $pq;
            }


            $pOrders['invoice_total_amount'] = $totalpuchaseprice - $arr[1]; // coupon process;
            $pOrders['sales_amount'] = $totalpuchaseprice - $arr[1]; // coupon process;
            $this->db->where('invoice_id', $insertid);
            $this->db->update('an_invoice', $pOrders);

            $pData['invoice_id'] = $insertid;
            $pData['user_id'] = $posInvoice['user_id'];
            $pData['order_amount'] = $totalpuchaseprice - $arr[1]; // coupon process
            $pData['order_date'] = date('Y-m-d H:i:s');
            $this->sales_management->inserData('orders', $pData);

            if (isset($post['newpayment'])) {
                $p['payment_type'] = 'receipt';
                $p['refernce_type'] = 'against';
                $p['payment_amount'] = $post['newpayment'];
                $p['refrence_id'] = $post['customerid'];
                $paymentid = $this->sales_management->inserData('payments', $p);

                $inp['payment_amount'] = $post['newpayment'];
                $inp['payment_id'] = $paymentid;
                $inp['invoice_id'] = $insertid;
                $this->sales_management->inserData('invoice_payments', $inp);
            }
        }

        $invSoledData = $this->sales_management->getSoldItemsByInvId($post['invocie_id'])[0];
        $invSoledData['recipt_id'] = $insertid;

        $query = "SELECT bs_invoice_items.*,large_price,medium_price,small_price FROM `bs_invoice_items` INNER JOIN `bs_item` ON itemid = invoice_item_id WHERE invoice_id = $insertid";
        $sql = $this->db->query($query);
        foreach ($sql->result() as $item) {
            $invSoledData['soled_quantity'] = $item->invoice_item_quantity;
            $invSoledData['item_id'] = $item->invoice_item_id;
            unset($invSoledData['soled_id']);
            $this->sales_management->inserData('store_soled_items', $invSoledData);

            $itemid = $item->invoice_item_id;
            $qty = $item->invoice_item_quantity;
            if ($item->invoice_item_price == $item->large_price) {
                $itemtype = 'large';
            } elseif ($item->invoice_item_price == $item->medium_price) {
                $itemtype = 'medium';
            } elseif ($item->invoice_item_price == $item->small_price) {
                $itemtype = 'small';
            }
            $query = $this->db->get_where('dish_content', array('item_id' => $itemid, 'size' => $itemtype));
            if ($query->num_rows() > 0) {
                insertRawData($query->result(), $itemid, $posInvoice['user_id'], $insertid, $invSoledData['store_id'], $qty);
            }
        }

        $this->cancel_sales_invoice($post['invocie_id']);
        redirect(base_url() . 'sales_management/sales_invoices?e=10');
    }

    function update_cancel_invoice_old() {
        $post = post();
        $coupon = $this->sales_management->getCoupon($post['invocie_id']);
        $orderd_items = $this->sales_management->getItemIds($post['purchase_item_quantity']);
        $arr = pocess_coupon($coupon, $orderd_items[0], $orderd_items[1], $orderd_items[2]);

        $invData = $this->sales_management->getInvoiceDataById($post['invocie_id']);
        unset($invData['invoice_id']);
        $invData['refrence_id'] = $post['invocie_id'];
        $invData['coupon_value'] = $arr[1]; // coupon process
        $newinvId = $this->sales_management->inserData('an_invoice', $invData);
        $invItemData = $this->sales_management->getInvoiceItemsDataById($post['invocie_id']);

        // coupon process
        if ($arr[2]) {// if there is a coupon dicount
            $cop['invoice_id'] = $newinvId;
            $cop['coupon_id'] = $arr[2];
            $cop['customer_id'] = $invData['customer_id'];
            $this->sales_management->inserData('invoice_coupon', $cop);
        }

        if ($invItemData) {
            foreach ($invItemData as $invItems) {
                $invItems['invoice_id'] = $newinvId;
                unset($invItems['inovice_product_id']);
                $this->sales_management->inserData('bs_invoice_items', $invItems);
            }
        }


        $invItemPaymentData = $this->sales_management->getInvoicePaymentsById($post['invocie_id']);
        if ($invItemPaymentData) {
            foreach ($invItemPaymentData as $invpItems) {
                $invpItems['invoice_id'] = $newinvId;
                //	$allpayments[] = $invpItems['payment_id'];
                $pmData = $this->sales_management->getPaymentDataById($invpItems['payment_id']);
                unset($pmData['id']);
                $invpItems['payment_id'] = $this->sales_management->inserData('payments', $pmData);
                unset($invpItems['inv_payment_id']);
                $this->sales_management->inserData('invoice_payments', $invpItems);
            }
        }


        /* $invSoledData = $this->sales_management->getSoldItemsByInvId($post['invocie_id']);

          if ($invSoledData) {
          foreach ($invSoledData as $soled) {
          $soled['recipt_id'] = $newinvId;
          unset($soled['soled_id']);
          $this->sales_management->inserData('store_soled_items', $soled);
          }
          } */


        $this->coupon_raw($post['invocie_id']);
        $pData['cancel_sales'] = 1;
        $pid = post('purchaseInvocie');
        $this->db->where('invoice_id', $post['invocie_id']);
        $this->db->update('an_invoice', $pData);

        $pData = array();
        $pData['cancel_status'] = 1;
        $this->db->where('invoice_id', $post['invocie_id']);
        $this->db->update('orders', $pData);

        $pData = array();
        $orderid = $this->sales_management->getOrderidByInvID($post['invocie_id']);
        $pData['cancel_status'] = 1;
        $this->db->where('order_id', $orderid);
        $this->db->update('clear_order_payment', $pData);


        $pData = array();
        $pData['sales_item_is_cancel'] = 1;
        $pid = post('purchaseInvocie');
        $this->db->where('invoice_id', $post['invocie_id']);
        $this->db->update('bs_invoice_items', $pData);

        $pData = array();
        $pData['sales_payment_is_cancel'] = 1;
        $pid = post('purchaseInvocie');
        $this->db->where('invoice_id', $post['invocie_id']);
        $this->db->update('invoice_payments', $pData);

        $pData = array();
        $pData['is_cancel'] = 1;
        $pid = post('purchaseInvocie');
        $this->db->where('recipt_id', $post['invocie_id']);
        $this->db->update('store_soled_items', $pData);

        $payments = $this->sales_management->getPaymentIdsForCancel($post['invocie_id']);
        if ($payments) {
            foreach ($payments as $pm) {
                $pm->payment_id;
                $pmData['status'] = 0;
                $this->db->where('id', $pm->payment_id);
                $this->db->update('payments', $pmData);
            }
        }

        if ($post) {
            if ($post['items']) {
                $items = $post['items'];
                $len = count($items['productId']);
                for ($counter = 0; $counter < $len; $counter++) {
                    $pitem['invoice_id'] = $newinvId;
                    $pitem['invoice_item_quantity'] = $items['quantity'][$counter];
                    $pitem['invoice_item_id'] = $items['productId'][$counter];
                    $pitem['invoice_item_store_id'] = $items['store_id'][$counter];
                    $pitem['invoice_item_price'] = $items['productTotal'][$counter];
                    $pitem['customer_id'] = $post['customerid'];
                    $pitem['refrence_id'] = $post['invocie_id'];
                    $invitem_id_item = $this->db->insert('bs_invoice_items', $pitem);
                    $counter++;
                }
            }

            if ($post['purchase_item_quantity']) {
                $pitemQuantity = $post['purchase_item_quantity'];
                $purchase_item_price = $post['purchase_item_price'];
                $purchase_item_aedprice = $post['purchase_item_aedprice'];

                $totalpuchaseprice = 0;
                $totalpuchaseaedprice = 0;
                if ($pitemQuantity) {

                    foreach ($pitemQuantity as $in => $pq) {
                        $pItem['invoice_item_quantity'] = $pq;
                        $this->db->where('inovice_product_id', $in);
                        $this->db->update('bs_invoice_items', $pItem);

                        $totalpuchaseprice = $totalpuchaseprice + $purchase_item_price[$in] * $pq;
                        //	$totalpuchaseaedprice = $totalpuchaseaedprice+$purchase_item_aedprice[$in]*$pq;
                    }
                    $totalpuchaseprice -= $arr[1];
                    $pOrders['invoice_total_amount'] = $totalpuchaseprice;
                    $this->db->where('invoice_id', $newinvId);
                    $this->db->update('an_invoice', $pOrders);
                    if ($post['newpayment']) {
                        $p['payment_type'] = 'receipt';
                        $p['refernce_type'] = 'against';
                        $p['payment_amount'] = $post['newpayment'];
                        $p['refrence_id'] = $post['customerid'];
                        $paymentid = $this->sales_management->inserData('payments', $p);

                        $inp['payment_amount'] = $post['newpayment'];
                        $inp['payment_id'] = $paymentid;
                        $inp['invoice_id'] = $newinvId;
                        $this->sales_management->inserData('invoice_payments', $inp);
                    }
                }
            }

            $invSoledData = $this->sales_management->getSoldItemsByInvId($post['invocie_id'])[0];
            $invSoledData['recipt_id'] = $newinvId;

            $query = "SELECT bs_invoice_items.*,large_price,medium_price,small_price FROM `bs_invoice_items` INNER JOIN `bs_item` ON itemid = invoice_item_id WHERE invoice_id = $newinvId";
            $sql = $this->db->query($query);
            foreach ($sql->result() as $item) {
                $invSoledData['soled_quantity'] = $item->invoice_item_quantity;
                $invSoledData['item_id'] = $item->invoice_item_id;
                unset($invSoledData['soled_id']);
                $this->sales_management->inserData('store_soled_items', $invSoledData);

                $itemid = $item->invoice_item_id;
                $qty = $item->invoice_item_quantity;
                if ($item->invoice_item_price == $item->large_price) {
                    $itemtype = 'large';
                } elseif ($item->invoice_item_price == $item->medium_price) {
                    $itemtype = 'medium';
                } elseif ($item->invoice_item_price == $item->small_price) {
                    $itemtype = 'small';
                }
                $query = $this->db->get_where('dish_content', array('item_id' => $itemid, 'size' => $itemtype));
                if ($query->num_rows() > 0) {
                    insertRawData($query->result(), $itemid, $invData['user_id'], $newinvId, $invSoledData['store_id'], $qty);
                }
            }
            //redirect(base_url() . 'sales_management/sales_invoices?e=10');
        }
    }

    /* function updateRawData($arr, $pid, $qty) {
      foreach ($arr as $item) {
      $query = $this->db->get_where('store_value', array('item_id' => $item->raw_id));
      if ($query->num_rows() > 0) {
      $quantity = $item->qty * $qty;
      echo $item->qty . " = " . $qty;
      $id = $query->row()->item_id;
      $query = "update store_value set totalq = totalq+$quantity where item_id = $id and store_id = (select storeid from bs_store where `default` = 1)";
      $this->db->query($query);
      }
      }
      } */

    public function coupon_raw($pid) {
        //coupon
        $sql = "SELECT * FROM invoice_coupon INNER JOIN `coupon` ON `invoice_coupon`.`coupon_id` = `coupon`.`coupon_id` where invoice_id = $pid";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $this->db->where(array('invoice_id' => $query->row()->invoice_id, 'coupon_id' => $query->row()->coupon_id));
            $this->db->delete('invoice_coupon');
        }

        $invSoledData = $this->sales_management->getSoldItemsByInvId($pid)[0];

        //revert minus from store
        $query = "SELECT bs_invoice_items.*,large_price,medium_price,small_price FROM `bs_invoice_items` INNER JOIN `bs_item` ON itemid = invoice_item_id WHERE invoice_id = $pid";
        $sql = $this->db->query($query);
        foreach ($sql->result() as $item) {
            $itemid = $item->invoice_item_id;
            $qty = $item->invoice_item_quantity;
            if ($item->invoice_item_price == $item->large_price) {
                $itemtype = 'large';
            } elseif ($item->invoice_item_price == $item->medium_price) {
                $itemtype = 'medium';
            } elseif ($item->invoice_item_price == $item->small_price) {
                $itemtype = 'small';
            }
            $query = $this->db->get_where('dish_content', array('item_id' => $itemid, 'size' => $itemtype));
            if ($query->num_rows() > 0) {
                $this->sales_management->updateRawData($query->result(), $pid, $qty,$invSoledData['store_id']);
            }
        }
    }

    function cancel_sales_invoice($pid = null) {
        if ($pid == null) {
            $pid = post('purchaseInvocie');
        }

        $this->coupon_raw($pid);

        $pData['cancel_sales'] = 1;
        $pData['coupon_value'] = 0;
        $this->db->where('invoice_id', $pid);
        $this->db->update('an_invoice', $pData);

        $pData = array();

        $pData['sales_item_is_cancel'] = 1;
        $this->db->where('invoice_id', $pid);
        $this->db->update('bs_invoice_items', $pData);


        $pData = array();
        $pData['cancel_status'] = 1;
        $this->db->where('invoice_id', $pid);
        $this->db->update('orders', $pData);


        $pData = array();
        $pData['sales_payment_is_cancel'] = 1;
        $this->db->where('invoice_id', $pid);
        $this->db->update('invoice_payments', $pData);

        $orderid = $this->sales_management->getOrderIdByInId($pid);
        $pData = array();
        $pData['cancel_status'] = 1;
        $this->db->where('order_id', $orderid);
        $this->db->update('clear_order_payment', $pData);



        $pData = array();
        $pData['is_cancel'] = 1;
        $this->db->where('recipt_id', $pid);
        $this->db->update('store_soled_items', $pData);
        $this->db->update('raw_soled_items', $pData);

        $payments = $this->sales_management->getPaymentIdsForCancel($pid);
        if ($payments) {
            foreach ($payments as $pm) {

                $pm->payment_id;
                $pmData['status'] = 0;
                $this->db->where('id', $pm->payment_id);
                $this->db->update('payments', $pmData);
            }
        }
        return true;
    }

    public function sales_invoices($id = '') {

        // Load Home View
        $mem = $this->session->userdata('bs_memtype');
        if ($mem != 5 && $mem != 1) {
            //echo "dsasd";
            $id = $this->session->userdata('bs_userid');
        } else {
            $id = "";
        }

        //echo $id;
        $this->_data['sale_invoices'] = $this->sales_management->getSaleInvocies_new($id);
        $this->_data['users_list'] = $this->sales_management->getUsersList();
        // exit;
        //$this->load->view('sales-invoices', $this->_data);

        $this->_data['inside'] = 'sales-invoices';

        $this->load->view('common/main', $this->_data);
    }
    
    public function sales_cancel_invoices($id = '') {

        // Load Home View
        $mem = $this->session->userdata('bs_memtype');
        if($mem !=5 && $mem !=1 && $mem !=2){
            //echo "dsasd";
            $id = $this->session->userdata('bs_userid');
        }
        else{
            $id = "";
        }

        //echo $id;
        $this->_data['sale_invoices'] = $this->sales_management->getSaleInvocies_cancelnew($id);
        $this->_data['users_list'] = $this->sales_management->getUsersList();


        //$this->load->view('sales-invoices', $this->_data);

        $this->_data['inside'] = 'sales-invoices-cancel';

        $this->load->view('common/main', $this->_data);

    }

    function payment_history($id) {
        //$this->_data['usersorders'] =  $this->users->getEmpPaymentHistory($id);
        $this->_data['inside'] = 'sales-invoices';
        $this->load->view('common/main', $this->_data);
    }

    function printed_invoice($invid) {
        $this->_data['sale_invoices'] = $this->sales_management->getInvoicedetailsByid($invid);
        $this->_data['inv_id'] = $invid;
        $this->load->view('printed_inv', $this->_data);
    }

    function viewp($id = null) {



        //$this->_data['invoice'] = $this->sales_management->getSaleInvocies($id);
        //$this->_data['p_invoices'] = $this->sales_management->p_invoices($id);

        $this->_data['invoice'] = $this->sales_management->invoice_data($id);

        $this->_data['inside'] = 'viewp';

        $this->_data['id'] = $id;

        $this->load->view('common/main', $this->_data);
    }

    function printed_iframe($id) {
        $this->_data['invoice'] = $this->sales_management->p_invoice($id);
        $this->_data['p_invoices'] = $this->sales_management->p_invoices($id);
        $this->_data['id'] = $id;
        $this->load->view('printediframe', $this->_data);
    }
    
     function printed_iframe_cancel($id){
        $this->_data['invoice'] = $this->sales_management->p_invoice($id);
        $this->_data['b_details'] = $this->sales_management->getBrnachDetails();
        $this->_data['p_invoices'] = $this->sales_management->p_invoices($id);
        $this->_data['id'] = $id;
        $this->load->view('printediframecancel', $this->_data);
    }

    function printed($id = null) {
        $this->_data['invoice'] = $this->sales_management->p_invoice($id);
        $this->_data['p_invoices'] = $this->sales_management->p_invoices($id);
        $this->_data['id'] = $id;
        $this->load->view('printed', $this->_data);
    }

    function postInvoices() {

        if (post('totalnet') != null && post('customerid') != null && post('items')['productId'] != null) {

            $invoiceData = $_POST['invoiceData'];

            $invoiceResponse = json_decode($invoiceData);

            unset($_POST['products']);

            $bs_userid = $this->session->userdata('bs_userid');

            if ($_POST['totalnet']) {

                //print_r($_POST['p_type']);
                //$invoice['ownerid'] = $_POST['ownerid'];
                //$invoice['generated_by'] = $this->_data['udata']['userid'];

                $invoice['companyid'] = $_POST['companyid'];

                $invoice['branchid'] = $_POST['branchid'];
                $invoice['user_id'] = $bs_userid;
                $invoice['customer_id'] = $_POST['customerid'];

                //$invoice['discountamount'] = $_POST['totalDiscount'];

                $invoice['invoice_total_amount'] = $_POST['totalnet'];

                $invoice['sales_amount'] = $_POST['totalnet'];

                $invoice['invoice_totalDiscount'] = $_POST['totalDiscount'];

                $invoice['invoice_totalDiscount_type'] = $_POST['type_ds'];



                if ($_POST['invoice_date']) {

                    $invoice['invoice_date'] = $_POST['invoice_date'];
                }



                //            if($invoice['receiverd_amount']==0){
                //                $invoice['invoice_status'] = "1";
                //            }    

                $invoice['generated_by'] = $this->session->userdata('bs_userid');

                $invoice['print_option_desc'] = $_POST['print_option_desc'];

                $invoice['print_option_note'] = $_POST['print_option_note'];

                $invoice['print_option_netprice'] = $_POST['print_option_netprice'];

                $invoice['print_option_dicount'] = $_POST['print_option_dicount'];

                $invoice['print_option_total'] = $_POST['print_option_total'];

                $invoice['print_option_pt'] = $_POST['print_option_pt'];

                $invoice['options_print_pages'] = serialize($_POST['options_print_pages']);

                if (post('sale_direct_store') == 1) {

                    //$invoice['confirm_status'] = 1;
                    $invoice2['confirm_status'] = 1;
                }
                $invoice_id = $this->sales_management->inserData('an_invoice', $invoice);
            }







            if (post('items')['productId']) {

                foreach (post('items')['productId'] as $key => $invresponsive) {

                    //print_r($invresponsive);

                    $invoice_items['invoice_id'] = $invoice_id;







                    $invoice_items['invoice_item_id'] = post('items')['productId'][$key];

                    $invoice_items['invoice_item_discount_type'] = post('items')['productDiscountType'][$key];

                    $invoice_items['invoice_item_discount'] = post('items')['productDiscount'][$key];

                    $invoice_items['invoice_item_notes'] = post('items')['productDiscription'][$key];

                    $invoice_items['invoice_item_onotes'] = post('items')['productONotes'][$key];

                    $invoice_items['invoice_item_quantity'] = post('items')['quantity'][$key];

                    $invoice_items['invoice_item_price'] = post('items')['productTotal'][$key];

                    $invoice_items['invoice_item_store_id'] = post('items')['store_id'][$key];

                    $invoice_items['invoice_item_price_purchase'] = post('items')['price_purchase'][$key];

                    $invoice_items['invoice_itype'] = post('items')['invoice_itype'][$key];

                    // $invoice_items['recipt_id'] = $_POST['customerid'];

                    if (post('sale_direct_store') == '1') {

                        $invoice_items['confirm_status'] = 1;


                        $postImageInvoice = array();
                        // $postImageInvoice['invoice_id'] = $insertid;
                        $postImageInvoice['soled_quantity'] = post('items')['quantity'][$key];
                        ;
                        $postImageInvoice['item_id'] = post('items')['productId'][$key];
                        $postImageInvoice['store_id'] = post('items')['store_id'][$key];
                        $postImageInvoice['userid'] = $bs_userid;
                        $postImageInvoice['recipt_id'] = $invoice_id;
                        //   $totalprice = $totalprice+$postData['itemtotalprice'][$a];
                        $invitemid = $this->sales_management->inserData('store_soled_items', $postImageInvoice);
                    }







                    //2

                    $invitem_id_item [] = $this->sales_management->inserData('bs_invoice_items', $invoice_items);





                    //print_r($invoice_items);
                }
            }









            $payment = array();

            $payment_cash = array();

            $payment_bank = array();





            if (post('payment')['p_type']) {





                foreach (post('payment')['p_type'] as $key => $val) {



                    if (post('payment')['p_amount'][$key] > 0 && post('payment')['p_amount'][$key] != null) {

                        $mpayment['payment_type'] = 'receipt';

                        $mpayment['refernce_type'] = 'advance';

                        $mpayment['payment_amount'] = post('payment')['p_amount'][$key];

                        $payment['invoice_id'] = $invoice_id;

                        $mpayment['refrence_id'] = $_POST['customerid'];

                        $mpayment['payment_date'] = post('payment')['p_date'][$key];



                        //3





                        $payment['payment_type'] = post('payment')['p_type'][$key];

                        $payment['method_payment'] = post('payment')['p_type2'][$key];

                        $payment['payment_amount'] = post('payment')['p_amount'][$key];

                        $payment['datetopay'] = post('payment')['p_date'][$key];

                        $payment['payment_date'] = post('payment')['p_date'][$key];

                        $payment['invoice_id'] = $invoice_id;

                        $payment['howpay'] = post('payment')['howpay'][$key];







                        //print_r($payment);
                        //4
                        //echo date('Y-m-d')."<br/>";
                        //echo post('payment')['p_date'][$key];

                        if (post('payment')['p_date'][$key] == date('Y-m-d')) {

                            //echo '1';
                            //$payment['status'] = '1';

                            $payment['payment_date'] = post('payment')['p_date'][$key];





                            $mpayment['status'] = '1';





                            if (post('payment')['howpay'][$key] == '1') {

                                //echo 'F';
                                //echo post('payment')['howpay'][$key];
                                //echo $_POST['last_payment_operation'];

                                if (post('receiverd_amount') == "0") {

                                    $payment['status'] = '1';
                                } else {

                                    $payment['status'] = '0';
                                }

                                $payment['payment_id'] = post('last_payment_operation');

                                $payment_id = $this->sales_management->inserData('invoice_payments', $payment);



                                //print_r($payment);
                            } else {

                                //echo 'L';
                                //echo post('payment')['howpay'][$key];    

                                $mpayment_id = $this->sales_management->inserData('payments', $mpayment);

                                $payment['payment_id'] = $mpayment_id;

                                $payment_id = $this->sales_management->inserData('invoice_payments', $payment);

                                //print_r($payment);
                            }
                        } else {





                            $mpayment['status'] = '0';





                            $payment['status'] = '0';

                            $payment['payment_date'] = post('payment')['p_date'][$key];

                            $payment['payment_id'] = $mpayment_id;







                            $payment_id2 = $this->sales_management->inserData('invoice_payments_later', $payment);







                            if (post('payment')['howpay'][$key] == '1') {



                                $payment['payment_id'] = post('last_payment_operation');

                                $payment_id = $this->sales_management->inserData('invoice_payments', $payment);
                            } else {



                                $mpayment_id = $this->sales_management->inserData('payments', $mpayment);

                                $payment['payment_id'] = $mpayment_id;

                                $payment_id = $this->sales_management->inserData('invoice_payments', $payment);
                            }
                        }





                        if (post('payment')['p_type'][$key] == '1') {

                            //this for cash

                            $payment_cash['payment_id'] = $payment_id;

                            $payment_cash['customer_id'] = $_POST['customerid'];

                            $payment_cash['value'] = post('payment')['p_amount'][$key];

                            $payment_cash['notes'] = post('payment')['p_note'][$key];

                            $payment_cash['purpose_of_payment'] = post('payment')['p_note'][$key];

                            $payment_cash['created'] = $this->_data['udata']['userid'];

                            $payment_cash['transaction_type'] = 'credit';

                            $payment_cash['payment_type'] = 'cash';

                            $payment_cash['refrence_type'] = 'payment_in';

                            $payment_cash['date'] = post('payment')['p_date'][$key];

                            $payment_cash['invoice_id'] = $invoice_id;

                            //5

                            $payment_id = $this->sales_management->inserData('an_cash_management', $payment_cash);

                            //print_r($payment_cash); 
                        } else {

                            //this for bank

                            $payment_bank['invoice'] = $invoice_id;

                            $payment_bank['customer_id'] = $_POST['customerid'];

                            $payment_bank['payment_id'] = $payment_id;

                            $payment_bank['amount_type'] = 'sales';

                            $payment_bank['refrence_type'] = 'sales';

                            $payment_bank['transaction_type'] = 'credit';



                            $payment_cash['value'] = post('payment')['p_amount'][$key];



                            $payment_bank['cheque_number'] = post('payment')['p_numper'][$key];

                            $payment_bank['deposite_recipt'] = post('payment')['p_numper'][$key];



                            $payment_bank['notes'] = post('payment')['p_note'][$key];

                            $payment_bank['purpose_of_payment'] = post('payment')['p_note'][$key];

                            $payment_bank['cheque_date'] = post('payment')['p_date'][$key];

                            $payment_bank['clearance_date'] = post('payment')['p_date'][$key];



                            $payment_bank['bank_id'] = post('payment')['p_bank'][$key];

                            $payment_bank['account_id'] = post('payment')['account_id'][$key];





                            //6

                            $payment_id = $this->sales_management->inserData('an_company_transaction', $payment_bank);

                            //print_r($payment_bank);
                        }
                    }
                }
            }



            $sales = array();

            if (post('salers')) {



                foreach (post('salers')['salerId'] as $key => $sale) {



                    $sales['sales_id'] = post('salers')['salerId'][$key];

                    $sales['amount'] = post('salers')['amount'][$key];

                    $sales['invoice_id'] = $invoice_id;

                    //7
                    //print_r($sales);

                    $salersid = $this->sales_management->inserData('bs_sales_invoice_persons', $sales);
                }
            }



            $expense = array();

            if (post('expense')['expense_title']) {



                foreach (post('expense')['expense_title'] as $key => $sale) {



                    $expense['expense_title'] = post('expense')['expense_title'][$key];

                    $expense['expense_charges_id'] = post('expense')['expense_charges_id'][$key];

                    $expense['type'] = 'direct';

                    $expense['expense_type'] = post('expense')['expense_type'][$key];

                    $expense['expense_period'] = post('expense')['expense_period'][$key];

                    $expense['value'] = post('expense')['value'][$key];

                    $expense['account_id'] = post('expense')['account_id'][$key];

                    $expense['bank_id'] = post('expense')['bank_id_ex'][$key];

                    $expense['Notes'] = post('expense')['Notes'][$key];

                    $expense['invoice_id'] = $invoice_id;

                    //8
                    //print_r($expense);

                    $expense_id = $this->sales_management->inserData('an_expenses', $expense);







                    $inv['expense_id'] = $expense_id;

                    $inv['sinvoice_id'] = post('invoice_id');

                    $this->sales_management->inserData('expense_sales', $inv);





                    $expenseid_bank['customer_id'] = $_POST['customerid'];

                    $expenseid_bank['payment_id'] = "";

                    $expenseid_bank['amount_type'] = 'expense';

                    $expenseid_bank['refrence_type'] = 'expense';

                    $expenseid_bank['transaction_type'] = 'credit';



                    //$expense_Accunts['branch_id'] = post('branch_id');



                    $expenseid_bank['cheque_number'] = post('expense')['p_numper'][$key];

                    $expenseid_bank['deposite_recipt'] = post('expense')['p_numper'][$key];



                    $expenseid_bank['notes'] = post('expense')['Notes'][$key];

                    $expenseid_bank['purpose_of_payment'] = post('expense')['Notes'][$key];





                    $expenseid_bank['cheque_date'] = date("Y-m-d");

                    //$expenseid_bank['clearance_date'] = date("Y-m-d");



                    $expenseid_bank['clearance_date'] = date('Y-m-d', strtotime(post('expense')['clearance_date'][$key]));

                    //$expense_Accunts['refrence_type'] = 'expense';



                    $expenseid_bank['value'] = post('expense')['value'][$key];



                    $expenseid_bank['bank_id'] = post('expense')['bank_id_ex'][$key];

                    $expenseid_bank['account_id'] = post('expense')['account_id'][$key];





                    //6

                    $this->sales_management->inserData('an_company_transaction', $expenseid_bank);
                }
            }





            $maintanace = array();

            if (post('maintanace')) {



                foreach (post('maintanace')['itemid'] as $key => $sale) {



                    $maintanace['m_itemid'] = post('maintanace')['itemid'][$key];

                    $maintanace['e_maintanaceTime'] = post('maintanace')['e_maintanaceTime'][$key];

                    $maintanace['e_maintanaceTimelist'] = post('maintanace')['e_maintanaceTimelist'][$key];

                    $maintanace['m_fdate'] = post('maintanace')['m_fdate'][$key];

                    $maintanace['per_maintanaceTime'] = post('maintanace')['per_maintanaceTime'][$key];

                    $maintanace['per_maintanaceTimelist'] = post('maintanace')['per_maintanaceTimelist'][$key];

                    $maintanace['m_invoice'] = $invoice_id;

                    //7
                    //print_r($sales);

                    $salersid = $this->sales_management->inserData('bs_maintenance', $maintanace);
                }
            }







            //print_r($expenseid);
            //print_r(post('expense'));
            //echo '111';
            //die();
            //}
            //$invocie_id

            $sale_product['productid'];

            //$net_amount

            unset($_POST['products']);

            //print_r($_POST);

            $ownerId = $_POST['ownerid'];

            $companyId = $_POST['companyid'];

            $branchId = $_POST['branchid'];



            //redirect(base_url().'sales_management/printed/'.$invoice_id);
            //redirect(base_url() . 'sales_management/sales_invoices?e=10');

            $redata['success'] = 1;

            $redata['redir'] = base_url() . 'sales_management/printed/' . $invoice_id;

            $redata['redir2'] = base_url() . 'sales_management/sales_invoices?e=10';

            $redata['message'].=$invoice_id;

            echo json_encode($redata);

            die();

            //exit;
        } else {

            $redata['success'] = 2;

            $redata['message'] = '<div class="notif red alert "> <span>خطأ يجب ادخال بيانات لتتم العملية</span></div>';

            echo json_encode($redata);

            die();
        }





        /*

         * echo '<script>window.open("' . base_url() . 'sales_management/sales_invoices?e=10", "_blank");';



          echo 'window.open("' . base_url() . 'sales_management/printed/' . $invoice_id . '", "_blank");<script>';

          exit; */
    }

    function insertinsolditems($data, $quantity, $userid = null, $inv = null) {
        $insertsoled['store_item_id'] = $data->store_item_id;
        $insertsoled['store_id'] = $data->store_id;
        $insertsoled['item_id'] = $data->item_id;
        $insertsoled['supplier_id'] = $data->supplier_id;
        $insertsoled['soled_quantity'] = $quantity;
        $insertsoled['recipt_id'] = $inv;
        $insertsoled['userid'] = $userid;
        return $this->db->insert('store_soled_items', $insertsoled);
    }

    function directSales() {

        foreach (post('product')['ids'] as $key => $sale) {

            //echo post('product')['itemid'][$key].','. post('product')['store_id'][$key].','.post('product')['quantity'][$key].','.post('userid');
            //$q=post('product')['quantity'][$key];
            //$q = 9;
            $get_store_its = $this->inventory2->get_store_it(post('product')['store_id'][$key], post('product')['itemid'][$key]);

            $this->sold_onebyone(post('product')['itemid'][$key], post('product')['store_id'][$key], post('product')['quantity'][$key], post('userid'), $id);
            /*
              $quantity=$get_store_its->squan-$get_store_its->soldquant;
              if($quantity-$q > 0){
              $fquantity=$q;
              }else{
              $fquantity=$quantity-$q;

              }
              $this->db->insert('store_soled_items',array('store_id'=>post('product')['store_id'][$key],'soled_quantity'=>$fquantity,'item_id'=>post('product')['itemid'][$key]));


             */
            $sitem['store_id'] = post('product')['store_id'][$key];
            $sitem['item_id'] = post('product')['ids'][$key];
            $sitem['receipt_id'] = post('product')['invoice_id'][$key];
            $sitem['note'] = post('product')['note'][$key];
            $sitem['quantity'] = post('product')['quantity'][$key];




            $invoice2['confirm_status'] = 1;
            $this->db->where('inovice_product_id', post('product')['ids'][$key]);
            $this->db->update('bs_invoice_items', $invoice2);

            $count++;
        }




        if ($count < post('countitem')) {
            $invoice['confirm_status'] = 0;
            $invoice['confirm_complete'] = (post('countitem') - $count);
        } else {
            $invoice['confirm_status'] = 1;
            $invoice['confirm_complete'] = (post('countitem') - $count);
        }


        $this->db->where('invoice_id', post('invoiceid'));
        $this->db->update('an_invoice', $invoice);
    }

    function updateInvoice($id) {



        $invoice['print_option_desc'] = $_POST['print_option_desc'];

        $invoice['print_option_note'] = $_POST['print_option_note'];

        $invoice['print_option_netprice'] = $_POST['print_option_netprice'];

        $invoice['print_option_dicount'] = $_POST['print_option_dicount'];

        $invoice['print_option_total'] = $_POST['print_option_total'];

        $invoice['print_option_pt'] = $_POST['print_option_pt'];

        $invoice['options_print_pages'] = serialize($_POST['options_print_pages']);



        $this->sales_management->updateData('an_invoice', $invoice, $id, 'invoice_id');



        redirect(base_url() . 'sales_management/viewp/' . $id);
    }

    function get_last_balance() {

        $this->load->model('customers/customers_model', 'customers');

        //$post = $this->input->post();

        if ($_POST['customerid']) {

            $post = array('customer_id' => $_POST['customerid']);
        } else {

            $post = array('customer_id' => 78);
        }



        $customerData = $this->customers->getCustomerPaymetns($post['customer_id']);

        // print_r($customerData);

        if ($customerData->totalpayment > $customerData->totalcharges) {

            $remaining = $customerData->totalpayment - $customerData->totalcharges;
        }

        if ($remaining > 0) {

            $last_payment = $this->customers->getLastpaymentIdByCustomer($post['customer_id'], 'opening');

            //print_r($last_payment);
            //exit;

            if (!empty($last_payment) && $last_payment->payment_amount > 0) {

                //echo "if";

                if ($last_payment->paidamount > 0)
                    $last_payment->payment_amount;

                if ($last_payment->payment_amount > $last_payment->paidamount) {

                    $last_payment->payment_amount = $last_payment->payment_amount - $last_payment->paidamount;
                } else {

                    $last_payment = $this->customers->getLastpaymentIdByCustomer($post['customer_id'], 'advance');

                    if ($last_payment->payment_amount > 0) {



                        if ($last_payment->paidamount > 0)
                            $last_payment->payment_amount;

                        elseif ($last_payment->payment_amount > $last_payment->paidamount) {

                            $last_payment->payment_amount = $last_payment->payment_amount - $last_payment->paidamount;
                        }
                    }
                }
            } else {

                // echo "else";

                $last_payment = $this->customers->getLastpaymentIdByCustomer($post['customer_id'], 'onaccount');

                if ($last_payment->payment_amount) {

                    if ($last_payment->paidamount > 0) {

                        $last_payment->payment_amount = $last_payment->payment_amount - $last_payment->paidamount;
                    }
                }
            }
        }

        /*

          echo "<pre>";

          print_r($last_payment);

          exit; */

//        //echo "<pre>";
//        $openbalance = $this->sales_management->openbalance($post['customer_id']);
//        //exit;
//        $customerData = $this->customers->getCustomerPaymetns($post['customer_id']);
//        //print_r($customerData);
//        if ($customerData->totalpayment > $customerData->totalcharges) {
//            $remaining = $customerData->totalpayment - $customerData->totalcharges;
//        }
//        if ($remaining > 0) {
//            $last_payment = $this->customers->getLastpaymentIdByCustomer($post['customer_id'], 'onaccount');
//            if (!empty($last_payment) && $last_payment->payment_amount>0) {
//                //echo "if";
//                if ($last_payment->paidamount != "")
//                    $last_payment->payment_amount;
//                elseif ($last_payment->payment_amount > $last_payment->paidamount) {
//                    $last_payment->payment_amount = $last_payment->payment_amount - $last_payment->paidamount;
//                } else {
//                    $last_payment = $this->customers->getLastpaymentIdByCustomer($post['customer_id'], 'onaccount');
//                    if ($last_payment->paidamount != "")
//                        $last_payment->payment_amount;
//                    elseif ($last_payment->payment_amount > $last_payment->paidamount) {
//                        $last_payment->payment_amount = $last_payment->payment_amount - $last_payment->paidamount;
//                    }
//                }
//            } else {
//                //echo "else";
//                $last_payment = $this->customers->getLastpaymentIdByCustomer($post['customer_id'], 'onaccount');
//                if ($last_payment->paidamount != "")
//                    $last_payment->payment_amount;
//                elseif ($last_payment->payment_amount > $last_payment->paidamount) {
//                    $last_payment->payment_amount = $last_payment->payment_amount - $last_payment->paidamount;
//                }
//            }
//        }
//        

        $openbalance = $this->sales_management->openbalance($post['customer_id']);

        if ($openbalance->remain) {

            if ($openbalance->remain != null) {

                $rem = $openbalance->remain;
            } else {

                $rem = 0;
            }
        } else {

            if ($openbalance->totalpayment != null) {

                $rem = $openbalance->totalpayment;
            } else {

                $rem = 0;
            }
        }

        $ret = array('id' => $last_payment->id, 'payment_amount' => $rem);

        echo json_encode($ret);

        //print_r($last_payment);
    }

    //----------------------------------------------------------------------



    /*

     * Sale Invoice

     */

    public function return_sales_invoices() {

        // Load Home View

        $this->_data['sale_invoices'] = $this->sales_management->getSaleInvocies();



        $this->_data['inside'] = 'return-sales-invoices';

        $this->load->view('common/main', $this->_data);
    }

    //----------------------------------------------------------------------



    function get_balance($vid = null) {



        if ($vid == null) {

            $id = $_POST['val'];
        } else {

            $id = $vid;
        }



        /*

          $openbalance = $this->sales_management->openbalance($id);

          $invoice = $this->sales_management->get_invoice_total($id);

         */



        $get_calculation = $this->sales_management->get_calculation($id);





        //var_dump($invoice);
        //die();





        /*

          //$Debit=$this->sales_management->Debit($id);

          //echo $currentbalance;

          //$balance1=$invoice->invoice_total_amount+$openbalance->totalcharges;

          $balance4 = $openbalance->remain + $openbalance->totalcharges;



          $balance1 = $balance4 + $openbalance->remain;

          //$balance1 = $openbalance->totalpayment;

          //$balance2=$openbalance;

          if ($openbalance->remain) {

          $balance2 = $openbalance->remain;

          } else {

          $balance2 = $openbalance->totalpayment;

          }



          //$balance3=$openbalance->totalcharges-$invoice->invoice_total_amount;

          //$balance3 = $openbalance->totalpayment - $openbalance->totalcharges;





          if ($openbalance->remain) {



          $balance3 = ($balance4 - $get_calculation->totalpayment) - $openbalance->remain;

          //$balance3="<br/>".$balance4."<br/>".$get_calculation->totalpayment."<br/>".$openbalance->remain;

          } else {



          $balance3 = $get_calculation->woamount - $get_calculation->totalpayment;

          //$balance3 = "w-t";

          }

         */





        $balance1 = $get_calculation->totalsales + $get_calculation->balance;

        $balance2 = $get_calculation->balance;

        $balance3 = $get_calculation->totalsales;

        $balance4 = $get_calculation->remainng;













        //echo '<div class="g3 tag green">' . lang('balance1') . $balance1 . '</div>';



        echo '<div class="g3 tag green">' . lang('balance2') . $balance2 . '</div>';



        //echo '<div class="g3 tag green">' . lang('balance4') . $balance3 . '</div>';



        echo '<div class="g3 tag red">' . lang('balance3') . $balance4 . ' </div>';
    }

    function get_store_quantity() {

        //$CI = & get_instance();

        $id = $_POST['val'];

        $pid = $_POST['pid'];





        //mahmoud// $fquantity = store_items_quan($id, $pid);
        //$query = "SELECT item_id,SUM(`quantity`) as quantity FROM `raw_store_items` WHERE `store_id` = $id AND `item_id` = $pid GROUP BY item_id";
        $query = "SELECT item_id,totalq as quantity FROM store_value WHERE `store_id` = $id AND `item_id` = $pid";
        $result = $this->db->query($query);
        if ($result->row()) {
            echo $result->row()->quantity;
        } else {
            echo 0;
        }

        //if($fquantity[0]->squantity || $fquantity[0]->ssquantity){
        //echo ($fquantity[0]->squantity)-($fquantity[0]->ssquantity);
        //}else{
        //echo '0';
        //}
    }

    //------------------------------------------------------------



    function add_note() {



        if (post('notes')) {

            //var_dump(post('notes'));

            foreach (post('notes')['title'] as $key => $notes) {



                $sdata['invoice_id'] = post('invoice_id');

                $sdata['in_title'] = post('notes')['title'][$key];

                $sdata['in_notes'] = post('notes')['note'][$key];



                //var_dump($sdata);

                $this->sales_management->inserData('invoice_notes', $sdata);
            }

            //die();

            redirect(base_url() . 'sales_management/viewp/' . post('invoice_id') . "/?e=11");
        } else {

            redirect(base_url() . 'sales_management/viewp/' . post('invoice_id') . "/?e=14");
        }
    }

    function uploadify() {



        $targetFolder = $this->config->item('bath'); // Relative to the root



        $verifyToken = md5('unique_salt' . $_POST['timestamp']);



        if (!empty($_FILES) && $_POST['token'] == $verifyToken) {

            $tempFile = $_FILES['Filedata']['tmp_name'];

            $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;

            $targetFile = rtrim($targetPath, '/') . '/' . $_FILES['Filedata']['name'];



            // Validate the file type

            $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // File extensions

            $fileParts = pathinfo($_FILES['Filedata']['name']);



            if (in_array($fileParts['extension'], $fileTypes)) {



                //$file=move_uploaded_file($tempFile,$targetFile);

                $path = APPPATH . '../uploads/invoicefiles/';

                $file_name = upload_file('Filedata', $path, true, 50, 50);



                $sdata['invoice_id'] = post('invoi_id');

                $sdata['ido_file'] = $file_name;



                //var_dump($sdata);

                $this->sales_management->inserData('invoice_document', $sdata);





                //print_r($file_name);
                //echo $tempFile."<br/>";
                //echo $targetFile;



                echo '1';
            } else {

                echo '<script>Invalid file type</script>';
            }
        }
    }

    function getfiles($id) {

        $invoice = $this->sales_management->get_files($id);

        if ($invoice) {

            foreach ($invoice as $d_invoice) {

                echo '

                        <tr>

                            <td>' . $d_invoice->ido_id . '</td>

                            <td><a href="' . base_url() . "/uploads/invoicefiles/" . $d_invoice->ido_file . '">' . $d_invoice->ido_file . '</a></td>

                            <td>' . $d_invoice->ido_date . '</td>

                       </tr>';
            }
        }
    }

    function add_expences() {



        $expense = array();

        if (post('expense')['expense_title']) {



            foreach (post('expense')['expense_title'] as $key => $sale) {

                $expense['expense_title'] = post('expense')['expense_title'][$key];

                $expense['expense_charges_id'] = post('expense')['expense_charges_id'][$key];

                $expense['type'] = 'direct';

                $expense['expense_type'] = post('expense')['expense_type'][$key];

                $expense['expense_period'] = post('expense')['expense_period'][$key];

                $expense['value'] = post('expense')['value'][$key];

                $expense['account_id'] = post('expense')['account_id'][$key];

                $expense['bank_id'] = post('expense')['bank_id_ex'][$key];

                $expense['Notes'] = post('expense')['Notes'][$key];

                $expense['sinvoice_id'] = post('invoice_id');



                $expense['clearance_date'] = date('Y-m-d', strtotime(post('expense')['clearance_date'][$key]));

                //8
                //print_r($expense);

                $expense_id = $this->sales_management->inserData('an_expenses', $expense);







                $inv['expense_id'] = $expense_id;

                $inv['invoice_id'] = post('invoice_id');

                $this->sales_management->inserData('expense_sales', $inv);





                $expenseid_bank['customer_id'] = $_POST['customerid'];

                $expenseid_bank['payment_id'] = "";

                $expenseid_bank['amount_type'] = 'expense';

                $expenseid_bank['refrence_type'] = 'expense';

                $expenseid_bank['transaction_type'] = 'credit';



                //$expense_Accunts['branch_id'] = post('branch_id');



                $expenseid_bank['cheque_number'] = post('expense')['p_numper'][$key];

                $expenseid_bank['deposite_recipt'] = post('expense')['p_numper'][$key];



                $expenseid_bank['notes'] = post('expense')['Notes'][$key];

                $expenseid_bank['purpose_of_payment'] = post('expense')['Notes'][$key];





                $expenseid_bank['cheque_date'] = date("Y-m-d");

                //$expenseid_bank['clearance_date'] = date("Y-m-d");



                $expenseid_bank['clearance_date'] = date('Y-m-d', strtotime(post('expense')['clearance_date'][$key]));

                //$expense_Accunts['refrence_type'] = 'expense';



                $expenseid_bank['value'] = post('expense')['value'][$key];



                $expenseid_bank['bank_id'] = post('expense')['bank_id_ex'][$key];

                $expenseid_bank['account_id'] = post('expense')['account_id'][$key];





                //6

                $this->sales_management->inserData('an_company_transaction', $expenseid_bank);

                //echo 's';
            }

            //echo post('invoice_id');
            //die();

            redirect(base_url() . 'sales_management/viewp/' . post('invoice_id') . "/?e=11");

            exit();
        } else {

            //echo 's3';

            redirect(base_url() . 'sales_management/viewp/' . post('invoice_id') . "/?e=11");

            exit();
        }

        //echo 's4';
    }

    function do_return_in($id) {



        if (post('return_tobalance') == '1') {


            echo "<pre>";
            print_r(post());
            exit;
            $invoicedata = array('invoice_status' => '0');

            $this->sales_management->updateData('an_invoice', $invoicedata, $id, 'invoice_id');



            $inpays = explode(',', post('invoice_payment'));

            foreach ($inpays as $inpay) {

                $invoicedata2 = array('status' => '0');

                //echo "inpay:".$inpay."<br/>";

                $this->sales_management->updateData('invoice_payments', $invoicedata2, $inpay, 'inv_payment_id');
            }

            $pays = explode(',', post('payment'));

            foreach ($pays as $pay) {

                $invoicedata3 = array('status' => '0');

                //echo "pay:".$pay."<br/>";

                $this->sales_management->updateData('payments', $invoicedata3, $pay, 'id');
            }

            $mpayment['payment_type'] = 'receipt';

            $mpayment['refernce_type'] = 'onaccount';

            $mpayment['payment_amount'] = post('totalpay');

            $mpayment['refrence_id'] = post('customerid');

            $mpayment['payment_date'] = date('Y-m-d');

            $mpayment['status'] = '1';



            //var_dump($mpayment);

            $this->sales_management->inserData('payments', $mpayment);
        }

        redirect(base_url() . 'sales_management/return_sales_invoices?e=10');
    }

}
