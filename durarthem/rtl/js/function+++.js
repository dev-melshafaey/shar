// JavaScript Document


function checkEnterPressed(obj, event) {
    //console.log(obj, event.keyCode);
    //we are checking for ENTER key here, 
    //you can check for other key Strokes here as well
    if (event.keyCode === 13) {
        alert("ENTER key!");
    }


}

function select_permission(val) {

    $.ajax({
        url: config.BASE_URL + "users/update_permission",
        type: "POST",
        data: {val: val},
        success: function (e) {

            $('#updatepermission').html(e);
            //getProductData(pid, '', val);
        }
    });
    //alert(val);
}
function gotonext(go, to) {

    //$().prop('class','show');
    $('#' + go).toggleClass("show");
    $('#' + to).toggleClass("show");

    $('.' + go).toggleClass("sel");
    $('.' + to).toggleClass("sel");



}
function select_child_main(sid, sclass, parent) {
    ///valp=$('.'+parent).val();
    //alert(parent);

    //$('.'+sclass+sid+':checkbox').not(this).prop('checked', this.checked);
    if (parent === true) {
        $('.' + sclass + sid).prop('checked', 'checked');
        $('.' + 'fchild2_' + sid).prop('checked', 'checked');
        //alert('t');
    } else {
        $('.' + sclass + sid).removeAttr('checked');
        $('.' + 'fchild2_' + sid).removeAttr('checked');
        //alert('f');
    }

}

function select_child(sid, sclass, parent) {
    ///valp=$('.'+parent).val();
    //alert(parent);

    //$('.'+sclass+sid+':checkbox').not(this).prop('checked', this.checked);
    if (parent === true) {
        $('.' + sclass + sid).prop('checked', 'checked');
        //alert('t');
    } else {
        $('.' + sclass + sid).removeAttr('checked');
        //alert('f');
    }

}

//calculae Price
function calculatePrice() {
    qntity = $("#quantity").val();
    pProduct = $("#item_total_price").val();
    if (qntity > 0) {
        //pProduct = $("#total_price").val();
        totalPrice = qntity * pProduct;
        $("#item_total_price").val(totalPrice);
    } else {

        $("#item_total_price").val(pProduct);
    }
}

function callc() {
    $(".ttamm").each(function (index) {
        console.log(index + ": " + $(this).text());
    });
}
function calculatePrice2(qntity, ob) {
    //qntity = $("#quantity").val();
    quantity = $("#quantity_tp" + ob).text();
    oneitem = $("#oneitem" + ob).text();
    totalLength = netTotals.length;
    totalDiscount = $("#totalDiscount").val();
    //callc();

    type_ds = $("#type_ds:checked").val();

    //ttamm

    console.log(totalLength);

    var editArray = geteditProductData(ob);
    //console.log(editArray);
    pProduct = $("#item_total_price").val();


    if (quantity > 0) {
        //pProduct = $("#total_price").val();
        //totalPrice = quantity * editArray['perPrice'];
        totalPrice = (quantity * oneitem);

        //console.log(qntity);
        $("#tr_" + ob + " #totalAmount" + ob + "").text(totalPrice);

        $("#invoice_input_" + ob + " #productQuantity" + ob + "").val(parseFloat(quantity));
        $("#invoice_input_" + ob + " #productTotal" + ob + "").val(parseFloat(totalPrice));

        ttprice = 0;
        if ($('.thth').length > 0) {
            len = $('.thth').length;
            for (a = 0; a < len; a++) {
                tprice = $('.thth').eq(a).html();
                ttprice = parseFloat(ttprice) + parseFloat(tprice);
                //console.log(tprice + 'tprice');
            }
        }




        $("#netTotal").text(ttprice);
        //$("#netTotal").text(totalPrice);
        /*
         for (a = 1; a < totalLength; a++) {
         //alert(netTotals[a]);
         //total_value = parseFloat(total_value) + parseFloat(netTotals[a]);
         
         totaltest+=$("#oneitem" + a).text()
         //minPrice = parseFloat(minPrice) + parseFloat(netTotals[a]);
         
         }
         */

        //$("#totalnet").val(totalPrice);

        //alert(totalPrice);
        if (totalDiscount !== "" && type_ds !== "") {
            if (type_ds === 2) {
                //$("#fnetTotal").html(totalPrice2);
                totalPrice2 = (quantity * oneitem) - totalDiscount;
                //alert(totalPrice);
            } else {

                //alert(totalPrice);
                //totalPrice2 = (quantity * oneitem)-totalDiscount;
                totalPrice2 = (oneitem * quantity - (((oneitem * quantity) * totalDiscount) / 100));
            }
        } else {

            totalPrice2 = 0;
        }
        //$("#netTotal").html(ttprice);
        //alert(totalPrice2);
        if (totalPrice2 > 0) {
            $("#totalnet").val(totalPrice2);
            $("#fnetTotal").html(totalPrice2);
            $("#view_all_total").html(totalPrice2);
            $("#remin_view_all_total").html(totalPrice2);
            $("#recievedamount").val(totalPrice2);
        } else {

            $("#totalnet").val(ttprice);
            //$("#fnetTotal").html(ttprice);
            $("#view_all_total").html(ttprice);
            $("#remin_view_all_total").html(ttprice);
            $("#recievedamount").val(ttprice);
            $("#totalAmount").text(ttprice);
        }

        $("#tr_" + ob + " #totalAmount" + ob + "").text(oneitem * quantity);


    } else {
        $("#tr_" + ob + " #totalAmount" + ob + "").text(oneitem);

        ttprice = 0;
        if ($('.thth').length > 0) {
            len = $('.thth').length;
            for (a = 0; a < len; a++) {
                tprice = $('.thth').eq(a).html();
                ttprice = parseFloat(ttprice) + parseFloat(tprice);
                //console.log(tprice + 'tprice');
            }
        }
        //$("#totalAmount").text(oneitem);

        $("#netTotal").text(ttprice);
        //$("#netTotal").text(oneitem);
        //$("#totalnet").val(oneitem);
    }
}


//get Product data
function getProductData(product_id, quant, bsstoreitems)
{
    //alert(bsstoreitems);
    if (product_id != "") {

        if (bsstoreitems != "" && bsstoreitems != 0) {
            $.ajax({
                url: config.BASE_URL + "ajax/getProductData",
                dataType: 'json',
                type: 'post',
                data: {pid: product_id, storeid: bsstoreitems},
                cache: false,
                success: function (data)
                {
                    //console.log('data');
                    dd = data;
                    console.log(dd);

                    // dd.product.sale_price;

                    //mi_descr
                    //alert('11');
                    purchase_price = dd.product.purchase_price;
                    min_sale_result = (dd.product.min_sale_price / 100) * purchase_price;
                    sale_result = (dd.product.sale_price / 100) * purchase_price;

                    sale_result = parseFloat(purchase_price) + parseFloat(sale_result);
                    //min_sale_result = parseFloat(purchase_price) + parseFloat(min_sale_result);
                    min_sale_result = parseFloat(dd.product.min_sale_price);

                    $("#min_price").val(min_sale_result);


                    $("#total_price").val(parseFloat(dd.product.sale_price).toFixed(3));

                    //$("#item_total_price").val(dd.product.purchase_price);

                    //alert($("#customer_ftotal").val());

                    if ($("#customer_ftotal").val() != "1") {
                        if (dd.totalval != 0) {
                            $("#item_total_price").val(parseFloat(dd.totalval).toFixed(3));
                        } else {
                            $("#item_total_price").val(parseFloat(dd.product.sale_price).toFixed(3));
                        }

                    } else {
                        $("#item_total_price").val(parseFloat(dd.product.salingbytotal).toFixed(3));
                        $("#fortaotal").html("سعر البيع للجمله");
                    }
                    //var json = $.parseJSON(dd.product.unit_title);
                    $("#quantity_text").html('(' + dd.unit + ')');
                    $("#quantity_text_in").val('(' + dd.unit + ')');
                    //alert(unit);

                    //var json = '[{"id":"1","tagName":"apple"},{"id":"2","tagName":"orange"},{"id":"3","tagName":"banana"},{"id":"4","tagName":"watermelon"},{"id":"5","tagName":"pineapple"}]';




                    //$("#item_total_price").val(dd.product.purchase_price);

                    $("#serialparseFloat").val(dd.product.serialparseFloat);
//                $("#quantity").val(1);

                    $("#price_product").val(parseFloat(dd.product.sale_price).toFixed(3));
                    if (dd.product.quantity == 0) {
                        $("#quantity").val('1');
                    } else {
                        $("#quantity").val();
                    }
                    $("#product_comment").text(dd.product.notes);
                    $("#product_picture").val(dd.product.product_picture);
                    //$("#storeid").val(dd.product.storeid);
                    $("input[name=storeid_p]").val(dd.product.storeid);

                    $("input[name=productid]").val(dd.product.itemid);

                    $("input[name=productname]").val(dd.itemname2);

                    $("#product_type").val(dd.product.itemtype);

                    //$("#price_product").val(dd.product.product_comment);
                    //`notes`
                    // $( "#branches" ).fadeOut( "slow" );

                    //$('select[name=store_id]').change(function () {

                    //alert('gp');


                    //});

                    var val = $('select[name=store_id]').val();
                    var pid = dd.product.itemid;

                    $.ajax({
                        url: config.BASE_URL + "sales_management/get_store_quantity",
                        type: "POST",
                        data: {val: val, pid: pid},
                        success: function (e) {

                            $('#storequantity').val(e);
                            //getProductData(pid, '', val);
                        }
                    });



                    addItemT(dd.product, quant, bsstoreitems, dd.itemname2);

                }
            });

        } else {
            alert('هناك خطأ يجب التاكد من اختيار مخزن');
            $("input[name=productname]").val('');
        }
    }
}

//get Product data
function getProductData3(product_id, par, store)
{
    //alert(store);
    if (product_id != "") {
        $.ajax({
            url: config.BASE_URL + "ajax/getProductData",
            dataType: 'json',
            type: 'post',
            data: {pid: product_id, storeid: store},
            cache: false,
            success: function (data)
            {
                console.log(data);
                dd = data;
                console.log(dd);

                // dd.product.sale_price;

                //mi_descr
                //alert();
                purchase_price = dd.product.purchase_price;
                min_sale_result = (dd.product.min_sale_price / 100) * purchase_price;
                sale_result = (dd.product.sale_price / 100) * purchase_price;

                sale_result = parseFloat(purchase_price) + parseFloat(sale_result);
                //min_sale_result = parseFloat(purchase_price) + parseFloat(min_sale_result);
                min_sale_result = parseFloat(dd.product.min_sale_price);

                $("#min_price").val(min_sale_result);


                $("#total_price").val(parseFloat(sale_result).toFixed(3));

                //$("#item_total_price").val(dd.product.purchase_price);

                //alert($("#customer_ftotal").val());

                if ($("#customer_ftotal").val() != "1") {
                    $("#item_total_price").val(parseFloat(dd.product.sale_price).toFixed(3));
                } else {
                    $("#item_total_price").val(parseFloat(dd.product.salingbytotal).toFixed(3));
                    $("#fortaotal").html("سعر البيع للجمله");
                }

                //$("#item_total_price").val(dd.product.purchase_price);

                $("#serialparseFloat").val(dd.product.serialparseFloat);
//                $("#quantity").val(1);
                $("#price_product").val(parseFloat(dd.product.sale_price).toFixed(3));
                if (dd.product.quantity == 0) {
                    $("#quantity").val('1');
                } else {
                    $("#quantity").val();
                }
                $("#product_comment").text(dd.product.notes);
                $("#product_picture").val(dd.product.product_picture);
                //$("#storeid").val(dd.product.storeid);
                $("input[name=storeid_p]").val(dd.product.storeid);

                $("input[name=productname]").val(dd.itemname2);
                //alert(dd.product.itemid);
                $("input[name=productid]").val(dd.product.itemid);

				$("#product_type").val(dd.product.itemtype);


                var val = $('select[name=store_id]').val();
                var pid = dd.product.itemid;

                $.ajax({
                    url: config.BASE_URL + "sales_management/get_store_quantity",
                    type: "POST",
                    data: {val: val, pid: pid},
                    success: function (e) {

                        $('#storequantity').val(e);
                        //getProductData(pid, '', val);
                    }
                });
                //$("#price_product").val(dd.product.product_comment);
                //`notes`
                // $( "#branches" ).fadeOut( "slow" );

                //addItemT(dd.product,quant,bsstoreitems,dd.itemname2);

            }
        });
    }
}
function addItemT(data, quant, bsstoreitems, itemname2) {
    dd = data;
    //console.log("ppppppppp" + dd.storeid);
    r = randomRange(10, 312);
    if (quant > 0) {
        if (dd.storeid != "") {
            img = '' + config.BASE_URL + 'uploads/item/' + dd.storeid + '/' + dd.product_picture + '';
        } else {
            img = '' + config.BASE_URL + 'uploads/item/' + dd.product_picture + '';
        }
        tbHtml = "<tr id='rti" + r + "' class=''>";

        tbHtml += '<td ><input type="hidden" class="" name="product[ids][]" id="u_" value="' + dd.itemid + '" />' + dd.itemid + '</td>';
        tbHtml += '<td ><input type="text" class="" name="product[itemname][]" id="u_" value="' + itemname2 + '" /></td>';
        tbHtml += '<td ><a href="' + img + '" class="aimg"><img src="' + img + '" alt="" style="width: 32px;height: 32px;"/></a></td>';

        //tbHtml+='<td ></td>';
        tbHtml += '<td ></td>';

        tbHtml += '<td ><input type="text" class="trtr" name="product[quantity][]"  id="u_quantity" value="' + quant + '" />';

        tbHtml += '<input type="hidden" class="" name="product[store_item_id][]" id="u_" value="' + bsstoreitems + '" /></td>';

        tbHtml += '<td >' + dd.notes + '</td>';
        tbHtml += '<td ><input type="hidden" class="purchase_price" name="product[purchase_price][]" id="u_" value="' + dd.purchase_price + '" />' + dd.purchase_price + '</td>';
        tbHtml += '<td ><textarea name="product[note][]"></textarea></td>';

        tbHtml += '<td ><a href="javascript:void" onclick="remove_transfer_item(' + r + ')"><span class="icon icon-remove" style="font-size: 10px;color: red;"></span></a></td>';
        tbHtml += "</tr>";
        $('#table-pend tr.grand-total').before(tbHtml);

        tbHtml2 = "<script type='text/javascript'>$('#product_list .aimg').fancybox();</script>";
        $("#product_list").before(tbHtml2);

        var quantityn = 0;
        var all_pricen = 0;
        len = $('.trtr').length;
        if ($('.trtr').length > 0) {

            //alert(len);
            for (a = 0; a < len; a++) {
                all_price = $('.purchase_price').eq(a).val();
                quantity = $('.trtr').eq(a).val();

                quantityn = parseFloat(quantityn) + parseFloat(quantity);
                all_pricen = parseFloat(all_pricen) + parseFloat(all_price * quantity);

                //console.log(tprice + 'tprice');
            }
        }

        $('#all_quant').text(quantityn);
        $('#all_price').text(all_pricen);
        $('#count').text(len);
    } else {
        //alert('خطأ هذا المنتج لا يملك كمية يمكن التحويل منها'); 
    }

}
function remove_transfer_item(id) {

    $('#rti' + id).remove();

}
/*
 //get Product data
 function getProductData2(product_id, store)
 {
 if (product_id != "") {
 $.ajax({
 url: config.BASE_URL + "ajax/getProductData2",
 dataType: 'json',
 type: 'post',
 data: {pid: product_id, storeid: store},
 cache: false,
 success: function (data)
 {
 //console.log('data');
 dd = data;
 console.log(dd);
 
 // dd.product.sale_price;
 
 //mi_descr
 //alert('sd');
 purchase_price = dd.product.purchase_price;
 min_sale_result = (dd.product.min_sale_price / 100) * purchase_price;
 sale_result = (dd.product.sale_price / 100) * purchase_price;
 
 sale_result = parseFloat(purchase_price) + parseFloat(sale_result);
 //min_sale_result = parseFloat(purchase_price) + parseFloat(min_sale_result);
 min_sale_result = parseFloat(dd.product.min_sale_price);
 
 $("#min_price").val(min_sale_result);
 
 $("#price_purchase").val(dd.product.purchase_price);
 
 
 $("#total_price").val(sale_result.toFixed(2));
 
 $("#item_total_price_prev").val(dd.product.purchase_price);
 
 
 $("#quantity_text").html('(' + dd.unit + ')');
 $("#quantity_text_in").val('(' + dd.unit + ')');
 
 //alert($("#customer_ftotal").val());
 
 
 //$("#item_total_price").val(sale_result.toFixed(2));
 
 $("#serialparseFloat").val(dd.product.serialparseFloat);
 //                $("#quantity").val(1);
 $("#price_product").val(dd.product.sale_price);
 if (dd.product.quantity == 0) {
 $("#quantity").val('1');
 } else {
 $("#quantity").val();
 }
 
 $("#product_comment").text(dd.product.notes);
 $("#product_picture").val(dd.product.product_picture);
 $("input[name=storeid_p]").val(dd.product.storeid);
 
 
 
 
 $("input[name=productname]").val(dd.itemname2);
 //alert(dd.product.itemid);
 $("input[name=productid]").val(dd.product.itemid);
 //alert(dd.product.storeid);
 //$("#price_product").val(dd.product.product_comment);
 //`notes`
 // $( "#branches" ).fadeOut( "slow" );
 
 }
 });
 }
 }
 
 function add_get_product(id, par, store) {
 
 if (par == "p") {
 //alert(store);
 getProductData2(id, store);
 } else {
 
 getProductData3(id, '', store);
 }
 $('.modal').removeClass('show').parents('.modal-ov').removeClass('show');
 
 }
 
 function add_get_customer(id) {
 view_balance_customer(id);
 $('.modal').removeClass('show').parents('.modal-ov').removeClass('show');
 
 }
 
 function view_balance_customer(uid) {
 
 $.ajax({
 url: config.BASE_URL + "ajax/getCustomerData",
 dataType: 'json',
 type: 'post',
 data: {uid: uid},
 cache: false,
 success: function (data)
 {
 
 if (data.blacklist === "1") {
 
 $(".viewmess").html(data.reson_blacklist);
 $(".get_balance").html("");
 $(".main_viewmess").css("display", "block");
 $(".submit_btn").hide();
 } else {
 $.ajax({
 url: config.BASE_URL + "sales_management/get_balance",
 type: "POST",
 data: {val: data.userid},
 success: function (e) {
 
 $('.get_balance').html(e);
 }
 });
 $(".main_viewmess").css("display", "none");
 $(".submit_btn").show();
 }
 
 
 
 
 $("#customerid").val(data.userid);
 $("#customername").val(data.fullname);
 $("#viewCustomer").html(data.fullname);
 $("#viewemail_address").html(data.email_address);
 $("#viewphone_parseFloat").html(data.phone_parseFloat);
 }
 });
 
 }
 */

//get Product data
function getProductData2(product_id, store)
{
    if (product_id != "") {
        $.ajax({
            url: config.BASE_URL + "ajax/getProductData",
            dataType: 'json',
            type: 'post',
            data: {pid: product_id, storeid: store, sid: $("#store_id").val()},
            cache: false,
            success: function (data)
            {
                //console.log('data');
                dd = data;
                console.log(dd);

                // dd.product.sale_price;

                //mi_descr
                //alert('sd');
                purchase_price = dd.product.purchase_price;
                min_sale_result = (dd.product.min_sale_price / 100) * purchase_price;
                sale_result = (dd.product.sale_price / 100) * purchase_price;

                sale_result = parseFloat(purchase_price) + parseFloat(sale_result);
                //min_sale_result = parseFloat(purchase_price) + parseFloat(min_sale_result);
                min_sale_result = parseFloat(dd.product.min_sale_price);

                $("#min_price").val(min_sale_result);

                $("#price_purchase").val(parseFloat(dd.product.purchase_price).toFixed(3));


                $("#total_price").val(sale_result.toFixed(3));

                $("#item_total_price_prev").val(parseFloat(dd.product.purchase_price).toFixed(3));


                $("#quantity_text").html('(' + dd.unit + ')');
                $("#quantity_text_in").val('(' + dd.unit + ')');

                //alert($("#customer_ftotal").val());
                /*
                 if($("#customer_ftotal").val()!="1"){
                 $("#item_total_price").val(dd.product.sale_price);
                 } else {
                 $("#item_total_price").val(dd.product.salingbytotal);
                 $("#fortaotal").html("سعر البيع للجمله");
                 }
                 */

                //$("#item_total_price").val(sale_result.toFixed(2));

                $("#serialparseFloat").val(dd.product.serialparseFloat);
//                $("#quantity").val(1);
                $("#price_product").val(parseFloat(dd.product.sale_price).toFixed(3));
                if (dd.product.quantity == 0) {
                    $("#quantity").val('1');
                } else {
                    $("#quantity").val();
                }

                $("#product_comment").text(dd.product.notes);
                $("#product_picture").val(dd.product.product_picture);
                $("input[name=storeid_p]").val(dd.product.storeid);


                $("#product_type").val(dd.product.itemtype);

                $("input[name=productname]").val(dd.itemname2);
                //alert(dd.product.itemid);
                $("input[name=productid]").val(dd.product.itemid);

                $("#avrage").val(dd.totalval);
                //alert(dd.product.storeid);
                //$("#price_product").val(dd.product.product_comment);
                //`notes`
                // $( "#branches" ).fadeOut( "slow" );

                var val = $('select[name=store_id]').val();
                var pid = dd.product.itemid;

                $.ajax({
                    url: config.BASE_URL + "sales_management/get_store_quantity",
                    type: "POST",
                    data: {val: val, pid: pid},
                    success: function (e) {

                        $('#storequantity').val(e);
                        //getProductData(pid, '', val);
                    }
                });

            }
        });
    }
}

function add_get_product(id, par, store) {

    if (par == "p") {
        //alert(par);
        getProductData2(id, store);
    } else {
        //alert('s');
        //getProductData3(id, '', store);
        getProductData(id, '', store);
    }
    $('.modal').removeClass('show').parents('.modal-ov').removeClass('show');

}

function add_get_customer(id) {
    view_balance_customer(id);

    $('.modal').removeClass('show').parents('.modal-ov').removeClass('show');

}

function view_balance_customer(uid) {

    $.ajax({
        url: config.BASE_URL + "ajax/getCustomerData",
        dataType: 'json',
        type: 'post',
        data: {uid: uid},
        cache: false,
        success: function (data)
        {

            if (data.blacklist === "1") {

                $(".viewmess").html(data.reson_blacklist);
                $(".get_balance").html("");
                $(".main_viewmess").css("display", "block");
                $(".submit_btn").hide();
            } else {
                $.ajax({
                    url: config.BASE_URL + "sales_management/get_balance",
                    type: "POST",
                    data: {val: data.userid},
                    success: function (e) {

                        $('.get_balance').html(e);
                    }
                });
                $(".main_viewmess").css("display", "none");
                $(".submit_btn").show();
            }



            $('#store_id option[value=' + data.storeid + ']').attr('selected', 'selected');
            $("#customerid").val(data.userid);
            $("#customername").val(data.fullname);
            $("#viewCustomer").html(data.fullname);
            $("#viewemail_address").html(data.email_address);
            $("#viewphone_parseFloat").html(data.phone_parseFloat);
        }
    });

}


///confrlm remove
function confirmRemove(id) {
    var r = confirm("Are you Sure You wnt to remove ?");
    if (r == true) {
        val = $("#productTotal" + id).val();
        //alert(val);
        //removeValueFromNet(val);

        $("#tr_" + id).remove();
        $(".pi" + id).remove();
        $("#invoice_input_" + id).remove();



        alterNetTotal_new();
        calculateNetTotal();
        //$("#maintenace_" + id).remove();

//        $("#netTotal").html("");
//        $("#totalnet_before").val("");
//        $("#totalmin").html("");
//        $("#totalmin").text("");
//        $("#totalnet").val("");
//        $("#fnetTotal").val("");

        $("#fff").html("");
        $("#product_maintenance").html("");
        $("#sales_persons").html("");

        //calculateNetTotal();
        //calculateProfitLoss();
        //array.splice(index, 1)
    } else {
        txt = "You pressed Cancel!";
    }
}

///confrlm remove
function confirmRemove2(id, pid) {
    var r = confirm("Are you Sure You wnt to remove ?");
    if (r == true) {
        val = $("#productTotal" + id).val();
        //alert(val);
        //removeValueFromNet(val);

        $("#tr_" + id).remove();
        $(".pi" + id).remove();
        $("#invoice_input_" + id).remove();



        alterNetTotal_new();
        calculateNetTotal();
        //$("#maintenace_" + id).remove();

//        $("#netTotal").html("");
//        $("#totalnet_before").val("");
//        $("#totalmin").html("");
//        $("#totalmin").text("");
//        $("#totalnet").val("");
//        $("#fnetTotal").val("");

        $("#fff").html("");
        $("#product_maintenance").html("");
        $("#sales_persons").html("");

        //calculateNetTotal();
        //calculateProfitLoss();
        //array.splice(index, 1)

        $.ajax({
            url: config.BASE_URL + "quotation/deleteitem",
            dataType: 'json',
            type: 'post',
            data: {pid: pid},
            cache: false,
            success: function (data)
            {

            }
        });

    } else {
        txt = "You pressed Cancel!";
    }
}

//for table data which has been added to the table 
function calculateCart(obj, id) {
    console.log(obj);
    console.log(id);
    ob = obj;
    //obj.id
    var val = $("#tr_" + id + " #" + obj.id + "").html();
    //alert(obj);
    //alert("#tr_"+id+" #"+obj.id+"");


    if (obj.id == 'quantity') {

        var quantity = val;
        //alert(quantity);
        //$("#tr_"+id+" #"+obj.id+"").html();
        //geteditProductData(id,quantity,'quantity');
        calculateTableProductData(id, quantity, 'quantity');
    }


}



function updateInput(obj) {


    $('#productQuantity' + obj.productId).val();
    $('#productTotal' + obj.productId).val();
    $('#productDiscount' + obj.productId).val();
    $('#productDiscription' + obj.productId).val();
    $('#productNotes' + obj.productId).val();

}
//discountType: "1"discription: ""id: 1length: 0minimum: "22.00"notes: "23.00"perPrice: "23.00"quantity: "2 "total: 46totalWithQ: 46__proto__: Array[0] function.js:110
function updateRow(nn) {
    console.log(nn);
    cv = nn;

    if (nn.discountType == 1) {
        dscType = "";
    }
    else {
        dscType = "";
    }

    if (nn.discount != "") {
    }
    else {
        discount = 0;
    }
    disc = discount + dscType;

    $('#tr_' + nn.id + ' #quantity').html(nn.quantity);
    $('#tr_' + nn.id + ' #totalAmount').html(nn.totalWithQ);
    $('#tr_' + nn.id + ' #discount').html(disc);
    //$('#tr_'+nn.id+' #discount').val();
    $('#tr_' + nn.id + ' #description').html(nn.discription);
    $('#tr_' + nn.id + ' #salePrice').html(nn.total);

}


//get  table procut data to eidit
function geteditProductData(id) {

    //productQuantity"+obj.productId+";
    //alert($("#productQuantity"+id).val());
    //alert($("#productQuantity"+id).val());

//	editArray['id'] = id;
//	editArray['quantity'] = $("#productQuantity"+id).val();
//	editArray['discount'] = $("#productDiscount"+id).val();
//	editArray['discountType'] = $("#productDiscountType"+id).val();
//	editArray['discription'] = $("#productDiscription"+id).val();
//	editArray['perPrice'] = $("#productPerPrice"+id).val();
//	editArray['notes'] = $("#productTotal"+id).val();
//	editArray['minimum'] = $("#minimum"+id).val();
    editArray['id'] = id;
    editArray['quantity'] = $('#tr_' + id + ' #quantity').text();
    editArray['discount'] = $('#tr_' + id + ' #discount').text();
    editArray['discountType'] = $('#tr_' + id + ' #discountType').text();
    editArray['description'] = $('#tr_' + id + ' #description').text();
    editArray['perPrice'] = $('#tr_' + id + ' #totalAmount').text();
    editArray['notes'] = $('#tr_' + id + ' #notes').text();
    editArray['minimum'] = $('#tr_' + id + ' #minimum').text();
    editArray['salePrice'] = $('#tr_' + id + ' #totalAmount').text();
    //productPerPrice"+obj.productId+"

    return editArray;



}


function maintenaceHml(mainten, id, pid) {
    //mainten
    htm = '<div class="g16 form-group main_div" id="maintenace_' + id + '" style="clear:both">\n\
<script>    $(".datapic_input").datepicker();    </script>\n\
\n\
\n\
\n\
\n\
  <div class="g2 form-group padding" ><div class=""><strong>  ' + config.maint1 + mainten + '</strong></div></div>';

    htm += '<input type="hidden" name="maintanace[itemid][]" value="' + pid + '"/><div class="g4 form-group padding">\n\
            <div class="g2 text-warning">' + config.maint2 + '</div>\n\
            <div class="g7"><input name="maintanace[e_maintanaceTime][]" id="e_maintanaceTime[]" type="text"  class="form-control"/></div>';

    htm += '<div class="g7">\n\
                <div class="ui-select"  style="height: 32px;"><select name="maintanace[e_maintanaceTimelist][]"><option selected="selected" value="days">Days </option><option value="weeks">Weeks</option><option value="months">Months</option><option value="years">Years</option></select></div>\n\
            </div></div>';

    htm += '<div class="g5 form-group padding">\n\
            <div class="g1 text-warning"> ' + config.maint3 + ' </div>';
    htm += '<div class="g13"><input name="maintanace[m_fdate][]" type="text"  class="datapic_input from_date form-control" style=""/></div>';
    htm += '<div class="g2" style="">\n\
                <a href="#"><img src="' + config.BASE_URL + 'images/internal/date_icon.png" width="22" height="24" border="0" /></a>\n\
            </div>\n\
            </div>';



    htm += '<div class="g5 form-group padding">\n\
            <div class="g2 text-warning">' + config.maint4 + '  </div>\n\
            <div class="g7"><input name="maintanace[per_maintanaceTime][]" type="text"  class="form-control"/></div>\n\
            <div class="g6"><div class="ui-select styled-select_filter" style="height: 32px;"><select name="maintanace[per_maintanaceTimelist][]">\n\
            <option selected="selected" value="days">Days </option><option value="weeks">Weeks</option>\n\
            <option value="months">Months</option><option  value="years">Years</option>\n\
            </select></div></div>\n\
            \n\
            \n\
            \n\
            ';


    htm += '<div class="g1 remove_main" style=""><a href="javascript:void()" style="text-decoration: none;" onclick="remove_maintenaceHml(' + id + ')"><i class="icon-remove-sign" style="font-size:20px;color: red;"></i></a></div> </div><br clear="all"/>';

    $("#product_maintenance").append(htm);
    /*$(".from_date").datepicker({
     defaultDate: "today",
     changeMonth: true,
     parseFloatOfMonths: 1,
     });*/

}

function remove_maintenaceHml(id) {

    $("#maintenace_" + id).remove();
}

function addMaintenance(id, mobj) {

    ck = $(mobj).is(":checked");
    //alert(dv);
    b = mobj;
    //b.attr("class");
    //$(mobj +".mainten").
    //$("#tr_"+id)
    name = $("#tr_" + id + " td").eq(2).html();
    pid = $("#tr_" + id + " td").eq(0).html();
    if (ck)
        //maintenaceHml(id);
        if ($('#product_maintenance').css('display') == 'none') {
            $('#product_maintenance').show();
        }
    maintenaceHml(name, id, pid);
    //$('html,body').animate({scrollTop: $(".mant_title").offset().top}, 800);

}
//calculate only table prduct discount
function calculateTableProductData(id, value, tp) {
//	var editArray = geteditProductData(id);
//	if(tp == "quantity"){
//		editArray['quantity'] = value ;
//	}
//	
//	quantity = editArray['quantity'];	
//	discount = editArray['discount'];
//	type = editArray['discountType'];
//	total = editArray['perPrice'];
//	minimum = editArray['minimum'];
//	total = total*quantity;
//	editArray['totalWithQ'] = total;
//	if(type ==1){
//		//alert(discount);
//		if(discount != ""){
//		final = parseFloat(total)*parseFloat(discount);
//			//alert(final);
//			final = final/100;
//			//alert(final);
//		final_total = total-final;
//		
//		}
//		else{
//			final_total = total;
//		}
//	}
//	else{
//		if(discount != ""){	
//		if(discount< total){
//		//om = total / discount;
//		//final = om*100;
//		final_total = total-discount;
//		
//		}
//		else{
//			alert("Discount is greater then total");	
//		 }
//		}
//		else{
//			final_total = total;
//		}
//		
//	}
//	
//	
//	editArray['total'] = final_total;
//	//console.log(editArray);
//	updateRow(editArray);
//	updateInput(editArray);
    //alert(final_total);
    //editArray['totalPrice'] = final_total;

    //alert(geteditProductData(id));
    var editArray = geteditProductData(id);
    console.log(editArray);

    //geteditProductData(id);

    if (tp == "quantity") {
        editArray['quantity'] = value;
    }

    quantity = editArray['quantity'];
    discount = editArray['discount'];
    type = editArray['discountType'];
    total = editArray['perPrice'];
    minimum = editArray['minimum'];
    description = editArray['description'];
    total = total * quantity;
    //editArray['totalWithQ'] = total;
    if (type == 1) {
        //alert(discount);
        if (discount != "") {
            final = parseFloat(total) * parseFloat(discount);
            //alert(final);
            final = final / 100;
            //alert(final);
            final_total = total - final;

        }
        else {
            final_total = total;
        }
    } else {
        if (discount != "") {



            if (parseFloat(discount) < total) {
                //om = total / discount;
                //final = om*100;
                //alert(parseFloat(discount));
                //alert(total);
                final_total = total - parseFloat(discount);

            } else {
                alert("Discount is greater then total");
            }

        }
        else {
            final_total = total;
        }

    }


    editArray['totalWithQ'] = total;
    editArray['total'] = total;
    editArray['totalnet'] = final_total;
    editArray['netTotal'] = final_total;
    editArray['salePrice'] = final_total;
    //console.log(editArray);
    updateRow(editArray);
    updateInput(editArray);
    //alert(final_total);
    //editArray['totalPrice'] = final_total;

}


// add new row in the table
function addrow(productData, rand) {
    tbHtml = '';
    console.log("productData" + productData);
    //console.log('productData');
    type = '';
    if (productData.discountType == '1') {
        if (productData.discount != "")
            disc = productData.discount + '%';
        else
            disc = '0 %';
    }
    else
    {

        if (productData.discount != "")
            disc = productData.discount + 'RO';
        else
            disc = productData.discount + 'RO';
    }
    if (productData.quantity == '')
        productData.quantity = 1;



    totalProductAmount = (parseFloat(productData.salePrice) * productData.quantity);

    //totalProductAmount = productData.total * productData.quantity;
    //productData.storeId;
    rowCounter++;


    //$("#totalDiscount").val(productData.storeid_p);
    //alert(productData.storeid_p);
    if (productData.storeid_p == "") {
        img = '' + config.BASE_URL + 'uploads/item/' + productData.pImage + '';

    } else {
        img = '' + config.BASE_URL + 'uploads/item/' + productData.storeid_p + '/' + productData.pImage + '';

        //img=''+config.BASE_URL+'uploads/item/'+productData.pImage+'';

    }



    tbHtml += '<tr id="tr_' + rand + '"><td >' + productData.productId + '</td>';
    //tbHtml += '<td ><a href="' + config.BASE_URL + '/uploads/item/' +productData.storeId+"/"+ productData.pImage + '" class="aimg"><img src="' + config.BASE_URL + '/uploads/item/' +productData.storeId+"/"+ productData.pImage + '" width="32" height="32" /></a></td>';
    tbHtml += '<td ><a href="' + img + '" class="aimg"><img src="' + img + '" alt="" style="width: 32px;height: 32px;"/></a></td>';
    tbHtml += '<td style="text-align:center"> ' + productData.productName + ' </td>';
    tbHtml += '<td id="description" contentEditable="true">' + productData.discription + ' </td>';


    tbHtml += '<td id="oneitem' + rand + '" style="text-align:center" onkeyup="calculate_pre_Price(this,' + rand + ')" contentEditable="true">' + productData.salePrice + '</td>';

    //onmouseout
    //tbHtml += '<td id="quantity" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculateCart(this,' + productData.productId + ')">' + productData.quantity + ' </td>';
    tbHtml += '<td id="quantity_tp' + rand + '" class="quantity_tp" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculatePrice2(this,' + rand + ')" >' + productData.quantity + ' </td>';
    //discount
    tbHtml += '<td id="totalAmount' + rand + '" style="text-align:center"  class="ttamm thth">' + totalProductAmount.toFixed(3) + '';
    //
    tbHtml += '<span class="thth_min" style="visibility: hidden;">' + productData.minPrice + '</span> </td>';


    tbHtml += '<td style="text-align:center;cursor:pointer;" ><img src="' + config.BASE_URL + '/images/internal/delete_small.png" width="7" height="8" border="0"   onclick="confirmRemove(' + rand + ')"/></td>';
    tbHtml += '<td align="center" valign="middle" style="text-align:center"><input class="mainten" name="editmainten" id="editmainten" type="checkbox" value="" onchange="addMaintenance(' + rand + ',this)" style="width:15%" /></td>';
    tbHtml += '</tr>';

    netTotals[netCouner] = productData.salePrice;
    Totalsmin[netCouner] = productData.minPrice;
    Totalsquan[netCouner] = productData.quantity;
    //Totalsmin[netCouner] = productData.minPrice;

    netCouner++;

    //alterNetTotal(rand);
    //$("#product_list").append(tbHtml);
    //console.log(tbHtml);
    $('#product_list tr.grand-total').before(tbHtml);

    //$(".quantity_tp").text('خدمة');

    $("#productid").val('');
    $("#quantity").val('');
    $("#total_price").val('');
    $("#item_total_price").val('');
    $("#imageProduct" + pid).val();
    $("#nameProduct" + pid).val();
    $("#item_discount").val('');
    $("#product_comment").val('');
    $("#product_notes").val('');
    $("#min_price").val('');
    $("#serialparseFloat").val('');
    $("#afterDiscount").val('');
    $("#productname").val('');
    $("#storequantity").val('');
    $("#store_id").val('');
    $("#item_total_price2").val('');
    $("#product_picture").val('');
    tbHtml2 = "<script type='text/javascript'>$('#product_list .aimg').fancybox();</script>";
    $("#product_list").before(tbHtml2);
}
// add new row in the table
function addrow_edit(productData, rand) {
    tbHtml = '';
    console.log("productData" + productData);
    //console.log('productData');
    type = '';
    if (productData.discountType == '1') {
        if (productData.discount != "")
            disc = productData.discount + '%';
        else
            disc = '0 %';
    }
    else
    {

        if (productData.discount != "")
            disc = productData.discount + 'RO';
        else
            disc = productData.discount + 'RO';
    }
    if (productData.quantity == '')
        productData.quantity = 1;


    totalProductAmount = (parseFloat(productData.salePrice) * productData.quantity);

    //totalProductAmount = productData.total * productData.quantity;
    //productData.storeId;
    rowCounter++;


    //$("#totalDiscount").val(productData.storeid_p);
    //alert(productData.storeid_p);
    if (productData.storeid_p == "") {
        img = '' + config.BASE_URL + 'uploads/item/' + productData.pImage + '';

    } else {
        img = '' + config.BASE_URL + 'uploads/item/' + productData.storeid_p + '/' + productData.pImage + '';

        //img=''+config.BASE_URL+'uploads/item/'+productData.pImage+'';

    }



    tbHtml += '<tr id="tr_' + rand + '"><td >' + productData.productId + '</td>';
    //tbHtml += '<td ><a href="' + config.BASE_URL + '/uploads/item/' +productData.storeId+"/"+ productData.pImage + '" class="aimg"><img src="' + config.BASE_URL + '/uploads/item/' +productData.storeId+"/"+ productData.pImage + '" width="32" height="32" /></a></td>';
    tbHtml += '<td ><a href="' + img + '" class="aimg"><img src="' + img + '" alt="" style="width: 32px;height: 32px;"/></a></td>';
    tbHtml += '<td style="text-align:center"> ' + productData.productName + ' </td>';
    tbHtml += '<td id="description" contentEditable="true">' + productData.discription + ' </td>';


    tbHtml += '<td id="oneitem' + rand + '" style="text-align:center" onkeyup="calculate_pre_Price(this,' + rand + ')" contentEditable="true">' + productData.salePrice + '</td>';

    //onmouseout
    //tbHtml += '<td id="quantity" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculateCart(this,' + productData.productId + ')">' + productData.quantity + ' </td>';
    tbHtml += '<td id="quantity_tp' + rand + '" class="quantity_tp" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculatePrice2(this,' + rand + ')" >' + productData.quantity + ' </td>';
    //discount
    tbHtml += '<td id="totalAmount' + rand + '" style="text-align:center"  class="ttamm thth">' + totalProductAmount.toFixed(3) + '</td>';
    //
    //tbHtml += '<td id="salePrice" style="text-align:center" contentEditable="true">' + productData.salePrice + '</td>';


    tbHtml += '<td style="text-align:center;cursor:pointer;" ><img src="' + config.BASE_URL + '/images/internal/delete_small.png" width="7" height="8" border="0"   onclick="confirmRemove(' + rand + ')"/></td>';
    tbHtml += '<td align="center" valign="middle" style="text-align:center"><input class="mainten" name="editmainten" id="editmainten" type="checkbox" value="" onchange="addMaintenance(' + rand + ',this)" style="width:15%" /></td>';
    tbHtml += '</tr>';

    netTotals[netCouner] = productData.salePrice;
    Totalsmin[netCouner] = productData.minPrice;
    Totalsquan[netCouner] = productData.quantity;
    //Totalsmin[netCouner] = productData.minPrice;

    netCouner++;

    //alterNetTotal_edit(rand);
    //$("#product_list").append(tbHtml);
    //console.log(tbHtml);
    $('#product_list tr.grand-total').before(tbHtml);

    $("#productid").val('');
    $("#quantity").val('');
    $("#total_price").val('');
    $("#item_total_price").val('');
    $("#imageProduct" + pid).val();
    $("#nameProduct" + pid).val();
    $("#item_discount").val('');
    $("#product_comment").val('');
    $("#product_notes").val('');
    $("#min_price").val('');
    $("#serialparseFloat").val('');
    $("#afterDiscount").val('');
    $("#productname").val('');
    $("#storequantity").val('');
    $("#store_id").val('');
    $("#item_total_price2").val('');
    $("#product_picture").val('');
    tbHtml2 = "<script type='text/javascript'>$('#product_list .aimg').fancybox();</script>";
    $("#product_list").before(tbHtml2);
}
// add new row in the table
function calculate_pre_Price(p, ob) {

    //alert(ob);
    chage = $("#oneitem" + ob).text();
    quantity_tp = $("#quantity_tp" + ob).text();
    totalDiscount = $("#totalDiscount").val();
    type_ds = $("#type_ds:checked").val();
    //if(chage>0){


    $("#totalAmount" + ob).text(parseFloat(chage) * quantity_tp);
    ttprice = 0;
    //console.log($('.thth').length);
    if ($('.thth').length > 0) {
        len = $('.thth').length;
        for (a = 0; a < len; a++) {
            tprice = $('.thth').eq(a).text();
            ttprice = parseFloat(ttprice) + parseFloat(tprice);
            //alert(tprice);
            //console.log(tprice + 'tprice');
        }
    }
    $("#netTotal").text(ttprice.toFixed(3));
    $("#netTotal_0").text(ttprice.toFixed(3));
    $("#totalnet").val(ttprice.toFixed(3));

    $("#view_all_total").html(ttprice.toFixed(3));
    $("#remin_view_all_total").html(ttprice.toFixed(3));
    $("#recievedamount").val(ttprice.toFixed(3));


    $("#invoice_input_" + ob + " #productQuantity" + ob + "").val(parseFloat(quantity_tp).toFixed(3));
    $("#invoice_input_" + ob + " #productTotal" + ob + "").val(parseFloat(chage).toFixed(3));

    //$("#totalAmount").text(ttprice);
    //$("#netTotal").html(ttprice);
    //    if(totalPrice!=null){
    //       $("#totalAmount").text(totalPrice);
    //    }else{
    //        $("#totalAmount").text(totalPrice);
    //    }



    //alert(type_ds);
    if (totalDiscount != "" && type_ds != "") {
        if (type_ds == 2) {
            fin = (parseFloat(chage) * quantity_tp) - totalDiscount;
            $("#fnetTotal").text(fin.toFixed(3));
            $("#view_all_total").html(fin.toFixed(3));
            $("#remin_view_all_total").html(fin.toFixed(3));
            $("#recievedamount").val(fin.toFixed(3));


            $("#invoice_input_" + ob + " #productQuantity" + ob + "").val(parseFloat(quantity_tp).toFixed(3));
            $("#invoice_input_" + ob + " #productTotal" + ob + "").val(parseFloat(fin).toFixed(3));

            //alert('-10');
        } else {
            //alert('10');
            fin = parseFloat(chage) * quantity_tp - (((parseFloat(chage) * quantity_tp) * totalDiscount) / 100);
            $("#fnetTotal").text(fin.toFixed(3));
            $("#view_all_total").html(fin.toFixed(3));
            $("#remin_view_all_total").html(fin.toFixed(3));
            $("#recievedamount").val(fin.toFixed(3));

            $("#invoice_input_" + ob + " #productQuantity" + ob + "").val(parseFloat(quantity_tp).toFixed(3));
            $("#invoice_input_" + ob + " #productTotal" + ob + "").val(parseFloat(fin).toFixed(3));
        }
    }
    //}

}
function addrow2(productData, rand) {
    /*tbHtml = '';
     console.log(productData);
     //console.log('productData');
     type = '';
     if (productData.discountType == '1') {
     if (productData.discount != "")
     disc = productData.discount + '%';
     else
     disc = '0 %';
     }
     else
     {
     
     if (productData.discount != "")
     disc = productData.discount + 'RO';
     else
     disc = productData.discount + 'RO';
     }
     
     //totalProductAmount = productData.total * productData.quantity;
     totalProductAmount = productData.unit_price * productData.quantity;
     //productData.storeId;
     rowCounter++;
     */
    tbHtml = '';
    console.log("productData" + productData);
    //console.log('productData');
    type = '';
    if (productData.discountType == '1') {
        if (productData.discount != "")
            disc = productData.discount + '%';
        else
            disc = '0 %';
    }
    else
    {

        if (productData.discount != "")
            disc = productData.discount + 'RO';
        else
            disc = productData.discount + 'RO';
    }
    if (productData.quantity == '')
        productData.quantity = 1;

    totalProductAmount = parseFloat(productData.salePrice) * productData.quantity;

    //totalProductAmount = productData.total * productData.quantity;
    //productData.storeId;
    rowCounter++;

    //alert(productData.storeid_p);
    if (productData.storeid_p == "") {
        img = '' + config.BASE_URL + 'uploads/item/' + productData.pImage + '';

    } else {
        img = '' + config.BASE_URL + 'uploads/item/' + productData.storeid_p + '/' + productData.pImage + '';

        //img=''+config.BASE_URL+'uploads/item/'+productData.pImage+'';

    }


    tbHtml += '<tr id="tr_' + rand + '"><td >' + productData.productId + '</td>';
    //tbHtml += '<td ><img src="' + config.BASE_URL + '/uploads/item/' +productData.storeId+"/"+ productData.product_picture + '" width="50" height="50" /></td>';
    tbHtml += '<td ><a href="' + img + '" class="aimg"><img src="' + img + '" alt="" style="width: 32px;height: 32px;"/></a></td>';
    tbHtml += '<td style="text-align:center"> ' + productData.productName + ' </td>';
    tbHtml += '<td id="description" contentEditable="true">' + productData.discription + ' </td>';


    tbHtml += '<td id="oneitem' + rand + '" style="text-align:center" onkeyup="calculate_pre_Price(this,' + rand + ')" contentEditable="true">' + productData.salePrice + '</td>';

    //onmouseout
    //tbHtml += '<td id="quantity" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculateCart(this,' + productData.productId + ')">' + productData.quantity + ' </td>';
    tbHtml += '<td id="quantity_tp' + rand + '" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculatePrice2(this,' + rand + ')" >' + productData.quantity + ' </td>';
    //discount
    tbHtml += '<td id="totalAmount' + rand + '" style="text-align:center" contentEditable="true"  class="ttamm thth">' + totalProductAmount.toFixed(3) + '</td>';
    //
    //tbHtml += '<td id="salePrice" style="text-align:center" contentEditable="true">' + productData.salePrice + '</td>';


    tbHtml += '<td style="text-align:center;cursor:pointer;" ><img src="' + config.BASE_URL + '/images/internal/delete_small.png" width="7" height="8" border="0"   onclick="confirmRemove(' + rand + ')"/></td>';
    tbHtml += '<td align="center" valign="middle" style="text-align:center"><input class="mainten" name="editmainten" id="editmainten" type="checkbox" value="" onchange="addMaintenance(' + rand + ',this)" style="width:15%" /></td>';
    tbHtml += '</tr>';

    netTotals[netCouner] = productData.salePrice;
    Totalsmin[netCouner] = productData.minPrice;
    Totalsquan[netCouner] = productData.quantity;

    //Totalsmin[netCouner] = productData.minPrice;

    netCouner++;

    alterNetTotal(rand);
    //$("#product_list").append(tbHtml);
    //console.log(tbHtml);
    $('#product_list tr.grand-total').before(tbHtml);

    $("#productid").val('');
    $("#quantity").val('');
    $("#total_price").val('');
    $("#item_total_price").val('');
    $("#imageProduct" + pid).val();
    $("#nameProduct" + pid).val();
    $("#item_discount").val('');
    $("#product_comment").val('');
    $("#product_notes").val('');
    $("#min_price").val('');
    $("#serialparseFloat").val('');
    $("#afterDiscount").val('');
    $("#productname").val('');
    $("#storequantity").val('');
    $("#store_id").val('');
    $("#item_total_price2").val('');
    $("#item_total_price_prev").val('');

    $("#product_picture").val('');
    $("#store_id").val('');
    tbHtml2 = "<script type='text/javascript'>$('#product_list .aimg').fancybox();</script>";
    $("#product_list").before(tbHtml2);
}

function calculatePaymentData() {
    //alert('asdasd');	
    total_pay = 0;
    if (jQuery('[name*="products[pay]"]').length > 0) {
        jQuery('[name*="products[pay]"]').each(function (index, value) {
            //var option = jQuery( this );
            //alert(index);
            //alert(value);
            //paymentValue = $(this).eq(index).val();
            pValue = jQuery('[name*="products[pay]"]').eq(index).val();
            total_pay = parseFloat(total_pay) + parseFloat(pValue);
            //alert(pValue);

            //alert(remainingAmount);
            //console.log(paymentValue+'paymentValue');
            //console.log(value+'value');
        })
    }
    $("#receiverd_amount").val(total_pay);
}

function addpurchaserow(productData) {
    tbHtml = '';
    console.log(productData);
    //console.log('productData');
    type = '';

    totalProductAmount = productData.unit_price * productData.quantity;
    //productData.storeId;
    rowCounter++;


    html = "<input type='text' class='pay" + rowCounter + " payment' id='productId" + rowCounter + "'  name='products[pay][]' value='' onchange='calculatePaymentData(" + prdocutCounter + ")'>";

    tbHtml += '<tr id="tr_' + prdocutCounter + '"><td >' + rowCounter + '</td>';
    tbHtml += '<td ><img src="' + config.BASE_URL + '/uploads/item//' + productData.productImage + '" width="50" height="50" /></td>';
    tbHtml += '<td style="text-align:center"> ' + productData.productName + ' </td>';
    tbHtml += '<td id="quantity" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculateCart(this,' + productData.productId + ')">' + productData.quantity + ' </td>';
    tbHtml += '<td id="description" contentEditable="true">' + productData.discription + ' </td>';
    tbHtml += '<td id="totalAmount" style="text-align:center">' + totalProductAmount + '</td>';
    tbHtml += '<td style="text-align:center;cursor:pointer;" ><img src="' + config.BASE_URL + '/images/internal/delete_small.png" width="7" height="8" border="0"   onclick="confirmRemove(' + prdocutCounter + ')"/></td>';
    tbHtml += '<td align="center" valign="middle" style="text-align:center"><input class="mainten" name="editmainten" id="editmainten" type="checkbox" value="" onchange="addMaintenance(' + productData.productId + ',this)" /></td>';
    tbHtml += '</tr>';

    netTotals[netCouner] = totalProductAmount;
    netCouner++;
    alterNetTotal2();
    //$("#product_list").append(tbHtml);
    //console.log(tbHtml);
    $('#product_list tr.grand-total').before(tbHtml);

    $("#productid").val('');
    $("#quantity").val('');
    $("#total_price").val('');
    $("#item_total_price").val('');
    $("#imageProduct" + pid).val();
    $("#nameProduct" + pid).val();
    $("#item_discount").val('');
    $("#product_comment").val('');
    $("#product_notes").val('');
}

//remove vlaue from net total
function removeValueFromNet(val) {
    ind = netTotals.indexOf(val);
    // alert(ind);
    //netTotals.splice(ind, 1);
}


function alterNetTotal_new(id) {
    //alert(id);
    totalLength = netTotals.length;

    minLength = Totalsmin.length;

    ttprice = 0;

    total_value = 0;
    total_min = 0;
    minPrice = 0;
    tprice = 0;
    tmin = 0;
    ttmin = 0;
    //console.log($('.thth').length);


    tprice = $('.thth').eq(0).text();
    quantity_tp = $('.quantity_tp').eq(0).text();
    total_value0 = parseFloat(ttprice) + parseFloat(tprice);

    //console.log(tprice + 'tprice');

    for (a = 0; a < totalLength; a++) {
        //alert(netTotals[a]);
        total_value = parseFloat(total_value) + parseFloat(netTotals[a] * Totalsquan[a]);
        //minPrice = parseFloat(minPrice) + parseFloat(netTotals[a]);

    }


    for (min = 0; min < minLength; min++) {

        total_min = parseFloat(total_min) + parseFloat(Totalsmin[min]);
        //minPrice = parseFloat(minPrice) + parseFloat(netTotals[a]);

    }


    if ($('.thth').length > 0) {
        len = $('.thth').length;
        for (a = 0; a < len; a++) {
            tprice = $('.thth').eq(a).html();
            ttprice = parseFloat(ttprice) + parseFloat(tprice);

        }
    }

    if ($('.thth_min').length > 0) {
        len = $('.thth_min').length;
        for (a = 0; a < len; a++) {
            tmin = $('.thth_min').eq(a).html();
            ttmin = parseFloat(ttmin) + parseFloat(tmin);

        }
    }
    console.log(ttprice.toFixed(3) + 'tprice');
    $("#netTotal").text(ttprice.toFixed(3) + "RO");

    $("#totalnet_before").val(ttprice.toFixed(3));


    //parseFloat(total_value);

    $("#totalmin").html(parseFloat(ttmin));

    $("#view_all_total").html(parseFloat(ttprice).toFixed(3) + " RO");
    $("#remin_view_all_total").html(parseFloat(ttprice).toFixed(3) + " RO");

    $("#totalnet").val(ttprice);

}

// change the value of net total
function alterNetTotal(id) {
    //alert(id);
    totalLength = netTotals.length;

    minLength = Totalsmin.length;

    ttprice = 0;

    total_value = 0;
    total_min = 0;
    minPrice = 0;
    tprice = 0;
    tmin = 0;
    //console.log($('.thth').length);





    tprice = $('.thth').eq(0).text();
    quantity_tp = $('.quantity_tp').eq(0).text();
    total_value0 = parseFloat(ttprice) + parseFloat(tprice);

    //console.log(tprice + 'tprice');

//    for (a = 0; a < totalLength; a++) {
//        //alert(netTotals[a]);
//        total_value = parseFloat(total_value) + parseFloat(netTotals[a] * Totalsquan[a]);
//        //minPrice = parseFloat(minPrice) + parseFloat(netTotals[a]);
//
//    }

    if ($('.thth').length > 0) {
        len = $('.thth').length;
        for (a = 0; a < len; a++) {
            tprice = $('.thth').eq(a).html();
            total_value = parseFloat(total_value) + parseFloat(tprice);

        }
    }

    //total_value=total_value+total_value0;



    /*
     for (min = 0; min < minLength; min++) {
     
     total_min = parseFloat(total_min) + parseFloat(Totalsmin[min]);
     //minPrice = parseFloat(minPrice) + parseFloat(netTotals[a]);
     
     }
     */

    if ($('.thth_min').length > 0) {
        len = $('.thth_min').length;
        for (a = 0; a < len; a++) {
            tmin = $('.thth_min').eq(a).html();
            minPrice = parseFloat(minPrice) + parseFloat(tmin);

        }
    }



    if (totalLength > 0) {



        if ($("#quantity").val() == 0 || $("#quantity").val() == '') {
            //alert("1");
            /**/

            $("#totalnet_before").val(parseFloat(total_value).toFixed(3));


            //parseFloat(total_value);
            $("#netTotal").html(parseFloat(total_value).toFixed(3));
            $("#totalmin").html(parseFloat(minPrice));

            $("#view_all_total").html(parseFloat(total_value).toFixed(3) + " RO");
            $("#remin_view_all_total").html(parseFloat(total_value).toFixed(3) + " RO");

            $("#totalnet").val(parseFloat(total_value).toFixed(3));

        } else {
            /*for (a = 0; a < totalLength; a++) {
             //alert(netTotals[a]);
             total_value = parseFloat(total_value) + (parseFloat(netTotals[a]) * $("#quantity").val());
             //alert(total_value);
             //alert(parseFloat(netTotals[a]));
             //alert($("#quantity").val());
             
             }
             
             for (min = 0; min < minLength; min++) {
             total_min = parseFloat(total_min) + parseFloat(Totalsmin[min]);
             //minPrice = parseFloat(minPrice) + parseFloat(netTotals[a]);
             
             }*/


            $("#totalnet_before").val(parseFloat(total_value).toFixed(3));


            $("#totalmin").html(parseFloat(minPrice));
            //parseFloat(total_value);
            $("#netTotal").html(parseFloat(total_value).toFixed(3));

            $("#view_all_total").html(parseFloat(total_value).toFixed(3) + " RO");
            $("#remin_view_all_total").html(parseFloat(total_value).toFixed(3) + " RO");

            $("#totalnet").val(parseFloat(total_value).toFixed(3));


        }
    }
    else {
        $("#netTotal").html("0 RO");
    }
}
// change the value of net total
function alterNetTotal_edit(id) {
    //alert(id);
    totalLength = netTotals.length;

    minLength = Totalsmin.length;

    ttprice = 0;

    total_value = 0;
    total_min = 0;
    minPrice = 0;
    tprice = 0;
    //console.log($('.thth').length);





    tprice = $('.thth').eq(0).text();
    quantity_tp = $('.quantity_tp').eq(0).text();
    total_value0 = parseFloat(ttprice) + parseFloat(tprice);

    //console.log(tprice + 'tprice');

    for (a = 0; a < totalLength; a++) {
        //alert(netTotals[a]);
        total_value = parseFloat(total_value) + parseFloat(netTotals[a] * Totalsquan[a]);
        //minPrice = parseFloat(minPrice) + parseFloat(netTotals[a]);

    }



    total_value = total_value + total_value0;




    for (min = 0; min < minLength; min++) {

        total_min = parseFloat(total_min) + parseFloat(Totalsmin[min]);
        //minPrice = parseFloat(minPrice) + parseFloat(netTotals[a]);

    }


    if (totalLength > 0) {



        if ($("#quantity").val() == 0 || $("#quantity").val() == '') {
            //alert("1");
            /**/

            $("#totalnet_before").val(total_value.toFixed(3));


            //parseFloat(total_value);
            $("#netTotal").html(parseFloat(total_value).toFixed(3) + " RO");
            $("#totalmin").html(parseFloat(total_min).toFixed(3) + " RO");

            $("#view_all_total").html(parseFloat(total_value).toFixed(3) + " RO");
            $("#remin_view_all_total").html(parseFloat(total_value).toFixed(3) + " RO");

            $("#totalnet").val(total_value.toFixed(3));

        } else {
            /*for (a = 0; a < totalLength; a++) {
             //alert(netTotals[a]);
             total_value = parseFloat(total_value) + (parseFloat(netTotals[a]) * $("#quantity").val());
             //alert(total_value);
             //alert(parseFloat(netTotals[a]));
             //alert($("#quantity").val());
             
             }
             
             for (min = 0; min < minLength; min++) {
             total_min = parseFloat(total_min) + parseFloat(Totalsmin[min]);
             //minPrice = parseFloat(minPrice) + parseFloat(netTotals[a]);
             
             }*/


            $("#totalnet_before").val(total_value.toFixed(3));


            $("#totalmin").html(parseFloat(total_min));
            //parseFloat(total_value);
            $("#netTotal").html(parseFloat(total_value).toFixed(3) + " RO");

            $("#view_all_total").html(parseFloat(total_value).toFixed(3) + " RO");
            $("#remin_view_all_total").html(parseFloat(total_value).toFixed(3) + " RO");

            $("#totalnet").val(total_value.toFixed(3));


        }
    }
    else {
        $("#netTotal").html("0 RO");
    }
}
function alterNetTotal2() {
    totalLength = netTotals.length;
    //alert(totalLength);
    if (totalLength > 0) {
        total_value = 0;
        for (a = 0; a < totalLength; a++) {
            //alert(netTotals[a]);
            total_value = parseFloat(total_value) + parseFloat(netTotals[a]);

        }
        //parseFloat(total_value);
        $("#netTotal").html(parseFloat(total_value) + " RO");

        $("#view_all_total").html(parseFloat(total_value) + " RO");
        $("#remin_view_all_total").html(parseFloat(total_value) + " RO");

        $("#totalnet").val(total_value);
    }
    else {
        $("#netTotal").html("0 RO");
    }
}


///calculate net total
function calculateNetTotal() {
    discountAmount = $("#totalDiscount").val();
    totalmin = $("#totalmin").text();
    totalnet = $("#net_total").text();
    type = $('input[name=type_ds]:checked').val();

    //alert(discountAmount);
    //alert(totalmin);
    //alert(total_value-discountAmount);
    total_value = parseFloat($("#netTotal").html());
    if (discountAmount != "") {
        if (type == 1) {
            //alert(discount);

            var finalt = parseFloat(total_value) * parseFloat(discountAmount);
            //alert(final);
            finalt = finalt / 100;
            //alert(final);
            finish = total_value - finalt;
            //alert(finish);
            //alert(totalmin);

            //alert(parseFloat(totalmin));

            if (parseFloat(finish) >= parseFloat(totalmin)) {

                net_total = finish;
                $("#totalnet").val(net_total);
                $("#fnetTotal").text(net_total);
                //$("#netTotal").text(net_total);

                $("#view_all_total").html(parseFloat(net_total) + " RO");
                $("#remin_view_all_total").html(parseFloat(net_total) + " RO");

            } else {

                alert('لا يمكن بيع المنتج اقل من الحد الادني للبيع  ');
                net_total = $("#totalnet_before").val();

                ttprice = 0;
                if ($('.thth').length > 0) {
                    len = $('.thth').length;
                    for (a = 0; a < len; a++) {
                        tprice = $('.thth').eq(a).html();
                        ttprice = parseFloat(ttprice) + parseFloat(tprice);
                        console.log(tprice + 'tprice');
                    }
                }


                //$("#netTotal").text(ttprice);
                //$("#netTotal").text("0");
                $("#fnetTotal").text("0");
                //$("#netTotal").html(ttprice);
                $("#totalDiscount").val('');

                $("#view_all_total").text($("#totalnet_before").val());
                $("#remin_view_all_total").text($("#totalnet_before").val());
                $("#recievedamount").text($("#totalnet_before").val());
                $("#totalrecievedamount").text($("#totalnet_before").val());

            }
            /*
             if(final_total <= minimum){
             
             alert('');
             
             //$("#item_discount").val('');
             //$("#afterDiscount").val('');
             
             }else{
             //$("#afterDiscount").val(final_total); 
             
             }*/


        } else {
            //alert(parseFloat(totalmin));

            if (parseFloat(total_value - discountAmount) >= parseFloat(totalmin)) {
                if (discountAmount < total_value) {
                    //om = total / discount;
                    //final = om*100;
                    //alert(total);
                    //alert(discount);
                    //final_total = total - discount;
                    net_total = total_value - discountAmount;
                    $("#totalnet").val(net_total);
                    //$("#netTotal").text(net_total);
                    $("#fnetTotal").text(net_total);
                    $("#view_all_total").html(parseFloat(net_total) + " RO");
                    $("#remin_view_all_total").html(parseFloat(net_total) + " RO");

                }
            } else {
                alert('لا يمكن بيع المنتج اقل من الحد الادني للبيع  ');
                net_total = "";
                net_total = $("#totalnet_before").val();


                ttprice = 0;
                if ($('.thth').length > 0) {
                    len = $('.thth').length;
                    for (a = 0; a < len; a++) {
                        tprice = $('.thth').eq(a).html();
                        ttprice = parseFloat(ttprice) + parseFloat(tprice);
                        console.log(tprice + 'tprice');
                    }
                }
                //$("#netTotal").text(ttprice);
                //$("#netTotal").text("0");
                $("#fnetTotal").text("0");

                $("#totalDiscount").val('');

                $("#view_all_total").text($("#totalnet_before").val());
                $("#remin_view_all_total").text($("#totalnet_before").val());
                $("#recievedamount").text($("#totalnet_before").val());
                $("#totalrecievedamount").text($("#totalnet_before").val());
                //$("#afterDiscount").val('');
            }

        }
        //


        $("#net_total").html(net_total);
        $("#rest_amount").html('');
        $("#receiverd_amount").val('');
    }

}

//caculate rest amount
function calculatePurchaseRestAmount() {

    receiveAmount = $("#receiverd_amount").val();
    rest_amount = total_value - receiveAmount;
    rest_amount = rest_amount.toFixed(2);
    $("#rest_amount").html(rest_amount + " RO");
}

function calculateRestAmount() {

    receiveAmount = $("#receiverd_amount").val();
    t_net = $("#totalnet").val();
    rest_amount = t_net - receiveAmount;
    $("#rest_amount").html(rest_amount + " RO");
}


netCouner = 0;
//adding new record in array
function randomRange(min, max) {
    return ~~(Math.random() * (max - min + 1)) + min
}
function get_payment_input_wrand() {

    r = randomRange(10, 312);
    $(this).html('<input name="' + r + '" type="" />');

}
function addInput(obj, rand) {
    //alert(counter);	

    // var newdiv = document.createElement('div');
    html = "<div id='invoice_input_" + rand + "'> <input type='hidden' class='pi" + rand + "' id='productId" + rand + "'  name='items[productId][]' value='" + obj.productId + "' />";
    //document.getElementById(divName).appendChild(newdiv);


    html += "<input type='hidden' class='pi" + rand + "' id='productQuantity" + rand + "'  name='items[quantity][]' value='" + obj.quantity + "' />";


    html += "<input type='hidden' class='pi" + rand + "' id='productTotal" + rand + "'  name='items[productTotal][]' value='" + obj.salePrice + "' />";


    html += "<input type='hidden' class='pi" + rand + "' id='minimum" + rand + "'  name='items[minimum][]' value='" + obj.minPrice + "'>";


    html += "<input type='hidden' class='pi" + rand + "' id='productPerPrice" + rand + "'  name='items[productperPrice][]' value='" + obj.total + "' />";


    html += "<input type='hidden' class='pi" + rand + "' id='productDiscount" + rand + "'  name='items[productDiscount][]' value='" + obj.discount + "' />";

    html += "<input type='hidden' class='pi" + rand + "' id='productDiscountType" + rand + "'  name='items[productDiscountType][]' value='" + obj.discountType + "' />";

    html += "<input type='hidden' class='pi" + rand + "' id='productDiscription" + rand + "'  name='items[productDiscription][]' value='" + obj.discription + "' />";

    html += "<input type='hidden' class='pi" + rand + "' id='productNotes" + rand + "'  name='items[productNotes][]' value='" + obj.note + "' />";


    html += "<input type='hidden' class='pi" + rand + "' id='supplier" + rand + "'  name='items[supplier_id][]' value='" + obj.supplier_id + "' />";


    html += "<input type='hidden' class='pi" + rand + "' id='unit_price" + rand + "'  name='items[unit_price][]' value='" + obj.unit_price + "' />";


    html += "<input type='hidden' class='pi" + rand + "' id='store_id" + rand + "'  name='items[store_id][]' value='" + obj.storeId + "' />";


    html += "<input type='hidden' class='pi" + rand + "' id='supplier_id" + rand + "'  name='items[supplier_id][]' value='" + obj.supplier_id + "' /> ";

    html += "<input type='hidden' class='pi" + rand + "' id='price_purchase" + rand + "'  name='items[price_purchase][]' value='" + obj.price_purchase + "' /> ";

    html += "<input type='hidden' class='pi" + rand + "' id='invoice_itype" + rand + "'  name='items[invoice_itype][]' value='" + obj.product_type + "' />";

    html += "<input type='hidden' class='pi" + rand + "' id='discount" + rand + "'  name='items[discount][]' value='" + obj.discount + "' />";
    html += "<input type='hidden' class='pi" + rand + "' id='expireddate" + rand + "'  name='items[expireddate][]' value='" + obj.expireddate + "' />";
    html += "<input type='hidden' class='pi" + rand + "' id='discountType" + rand + "'  name='items[discountType][]' value='" + obj.discountType + "' /> </div>";



    $(".invoice_raw_product_items").append(html);

    counter++;
}





//calculate discout for slected new table
function calculateDis() {

    minimum = $("#min_price").val();
    discount = $("#item_discount").val();
    //total = $("#total_price").val();
    total = $("input[name=item_total_price]").val();
    total2 = $("input[name=item_total_price_prev]").val();


    //alert(discount);

    type = $('input[name=type]:checked').val();
    //alert(type);
    if ($("#quantity").val() != 0) {

        //total = total * $("#quantity").val();
    }
    //alert(total);
    //alert(type);
    if (discount != "" && type != null && minimum != null)
    {
        //alert(type);
        //alert(discount);    
        if (type == 1) {


            var finalv = parseFloat(total) * parseFloat(discount);
            //alert(final);
            finalv = finalv / 100;

            //alert(finalv);

            final_total = total - finalv;
            if (final_total <= minimum) {

                alert('لا يمكن بيع المنتج اقل من الحد الادني للبيع ');

                $("#item_discount").val('');
                $("#afterDiscount").val('');

            } else {
                $("#afterDiscount").val(final_total.toFixed(3));

            }
        } else {

            final_total = total - discount;
            if (final_total < minimum) {

                alert('لا يمكن بيع المنتج اقل من الحد الادني للبيع ');

                $("#item_discount").val('');
                $("#afterDiscount").val('');

            } else {
                $("#afterDiscount").val(final_total.toFixed(3));
                //final_total.toFixed(2)
            }
        }

        //alert("discount : "+discount);
        //alert("minimum :"+minimum);

        //alert(total);
        //alert(discount);
        //alert(minimum);
        if (final_total > minimum) {
            //alert("Your Price is less then minimum Price");
        }
        //alert(final_total.toFixed(2));

        //$("#item_total_price").val(final_total.toFixed(2));
    } else {
        if (total != '') {
            //alert(total);
            if (type == 1) {


                var finalv = parseFloat(total) * parseFloat(discount);
                //alert(final);
                finalv = finalv / 100;
                //alert(final);
                final_total = total - finalv;

                $("#afterDiscount").val(final_total.toFixed(3));


            } else {

                final_total = total - discount;

                $("#afterDiscount").val(final_total.toFixed(3));
                //final_total.toFixed(2)

            }
        } else {
            //alert(total2);
            //$("#afterDiscount").val(total2-discount);
            if (type == 1) {


                var finalv = parseFloat(total2) * parseFloat(discount);
                //alert(final);
                finalv = finalv / 100;
                //alert(final);
                final_total = total2 - finalv;

                $("#afterDiscount").val(final_total.toFixed(3));


            } else {

                final_total = total2 - discount;
                $("#afterDiscount").val(final_total.toFixed(3));
                //final_total.toFixed(2)

            }
        }
    }
}

//change paymetn lable with type
/*
 function showPaymentLabel(method,ind){
 if(method == 2){
 var text = config.Cnum; 
 $(".bank").eq(ind).show();
 $(".label_class").eq(0).html(text);
 }
 if(method == 1){
 //var text = 'Receipt  parseFloat';
 var text = config.Rnum; 
 $(".bank").eq(ind).hide();
 $(".label_class").eq(0).html(text);
 }
 
 
 
 }
 */

//change paymetn lable with type
function showPaymentLabel(method, ind) {
    if (method == 1) {

        $("#bank").eq(ind).hide();
        $("#payment_parseFloat_1").eq(ind).hide();

        $("#div_banks").eq(ind).hide();
        $("#div_accounts").eq(ind).hide();
        $("#div_pdoc").eq(ind).hide();
        $("#div_TypePayment2").eq(ind).hide();
        $("#div_pnumper").eq(ind).hide();
    }
    if (method == 2) {
        $("#payment_parseFloat_1 .col-lg-2").eq(0).html(config.ptype1);
        $(".bank").eq(ind).show();

        $("#div_banks").eq(ind).show();
        $("#div_accounts").eq(ind).show();
        $("#div_pdoc").eq(ind).show();
        $("#div_TypePayment2").eq(ind).show();
        $("#div_pnumper").eq(ind).show();

        $("#payment_parseFloat_1").eq(ind).show();

    }
    /*if (method == 3) {
     var text = config.Cnum;
     $("#payment_parseFloat_1 .col-lg-2").eq(0).html(config.ptype2);
     $("#bank").eq(ind).show();
     //$(".label_class").eq(0).html(text);
     $("#payment_parseFloat_1").eq(ind).show();
     }
     if (method == 4) {
     //var text = 'Receipt  parseFloat';
     var text = config.Rnum;
     $("#payment_parseFloat_1 .col-lg-2").eq(0).html(config.ptype3);
     $("#bank").eq(ind).show();
     //$(".label_class").eq(0).html(text);
     $("#payment_parseFloat_1").eq(ind).show();
     }*/



}
//change paymetn lable with type
function showPaymentLabel2(method, ind) {
    //alert(config.ptype1);
    //alert(config.ptype2);
    //alert(config.ptype3);
    if (method == 1) {
        //alert(method);
        $(".more_add_new_payment" + ind + " #bank").hide();
        $(".more_add_new_payment" + ind + " #payment_parseFloat_1").hide();

        $(".more_add_new_payment" + ind + " #div_banks").hide();
        $(".more_add_new_payment" + ind + " #div_accounts").hide();
        $(".more_add_new_payment" + ind + " #div_pdoc").hide();
        $(".more_add_new_payment" + ind + " #div_TypePayment2").hide();
        $(".more_add_new_payment" + ind + " #div_pnumper").hide();

    }
    if (method == 2) {
        //alert(method);
        $(".more_add_new_payment" + ind + " #payment_parseFloat_1 .col-lg-2").html(config.ptype1);
        $(".more_add_new_payment" + ind + " #bank").show();
        $(".more_add_new_payment" + ind + " #payment_parseFloat_1").show();



        /**/

        $(".more_add_new_payment" + ind + " #div_banks").show();
        $(".more_add_new_payment" + ind + " #div_accounts").show();
        $(".more_add_new_payment" + ind + " #div_pdoc").show();
        $(".more_add_new_payment" + ind + " #div_TypePayment2").show();
        $(".more_add_new_payment" + ind + " #div_pnumper").show();

        $(".more_add_new_payment" + ind + " #payment_parseFloat_1").show();

    }
    /*if (method == 3) {
     //alert(method);
     var text = config.Cnum;
     $(".more_add_new_payment" + ind + " #payment_parseFloat_1 .col-lg-2").html(config.ptype2);
     $(".more_add_new_payment" + ind + " #bank").show();
     //$(".label_class").eq(0).html(text);
     $(".more_add_new_payment" + ind + " #payment_parseFloat_1").show();
     }
     if (method == 4) {
     //alert(method);
     //var text = 'Receipt  parseFloat';
     var text = config.Rnum;
     $(".more_add_new_payment" + ind + " #payment_parseFloat_1 .col-lg-2").html(config.ptype3);
     $(".more_add_new_payment" + ind + " #bank").show();
     //$(".label_class").eq(0).html(text);
     $(".more_add_new_payment" + ind + " #payment_parseFloat_1").show();
     }*/



}

//adding purchase price adn salre price will return you pr
function calculateProfitLoss() {
    len = allProductsData.length;
    totalpPrice = 0;
    totalProfit = 0;
    for (prc = 0; prc < len; prc++) {
        console.log(allProductsData[prc]['productPrice'] + "ppval");
        totalpPrice = parseFloat(totalpPrice) + parseFloat(allProductsData[prc]['productPrice']);
        tempTotal = parseFloat(allProductsData[prc]['salePrice']) - parseFloat(allProductsData[prc]['productPrice']);
        totalProfit = parseFloat(totalProfit) + tempTotal;
    }
    total_profit = totalProfit;
    //console.log(totalpPrice+"totalval");
    totalp = totalpPrice + ' RO';
    $("#total_purchase_price").html(totalp);
    totalProfit = totalProfit + ' RO';
    $("#total_profit").html(totalProfit);

}
function addSalesHml(salerId, finalSales) {


    salerrname = $("#salesName" + salerId).val();
    salesH = '<div class="raw" id="purchase_div' + salerId + '">   \n\
        <a href="javascript:void()" style="text-decoration: none;" onclick="remove_addSales(' + salerId + ')"><i class="icon-remove-sign" style="font-size:20px;color: red;"></i></a>';
    salesH += '<div class="form_title">' + config.SLang + ': </div>';
    salesH += '<div class="form_field form-group" id="total_purchase_price">' + salerrname + '</div>';
    salesH += '<div class="form_title">' + config.SMLang + ': </div>';
    salesH += '<div class="form_field form-group" id="total_purchase_price">' + finalSales + ' RO</div></div>';
    $("#sales_persons").append(salesH);
}
function remove_addSales(id) {

    $("#salerId" + id).remove();
    $("#salerId-1" + id).remove();
    $("#purchase_div" + id).remove();
}
function addInvoice() {
    //alert("asd");
    //allPData = json_encode(allProductsData);
    $("#invoiceData").val(JSON.stringify(allProductsData));

    //$("#invoiceData").val(JSON.stringify($(".invoice_raw" ).text()));
    //console.log(JSON.stringify($(".invoice_raw" ).text()));
    $("#supplier_id").attr("disabled", false);
    $("#frm_invoice").submit();
}
function addSales() {

    salerId = $("#sales_list").val();
    //alert(salerId);
    saler = $("#salerId" + salerId).length;
    if (saler == 0) {
        html = "<input type='hidden' class='salerId' id='salerId" + salerId + "'  name='salers[salerId][]' value='" + salerId + "'>";

        $("#fff").append(html);
        finalSales = addSalerAmount(salerId);
        addSalesHml(salerId, finalSales);

    }
    else {
        alert("You have already added this saler");
    }
}
totalAmountexComision = '';
totalnetAmountexComision = '';
salesCounter = 0;
netProfitCountSum = "";
netProfitCounter = 0;
function addSalerAmount(id) {
    //total = $("#total_price").val();

    comisison = $("#salescomission" + id).val();
    comisionType = $("#salescomissionType" + id).val();
    var final_r = 0;
    salesLen = allSalesData.length;
    for (a = 0; a < salesLen; a++) {
        id = allSalesData[a].id;
        comision = allSalesData[a].comision;
        saleType = allSalesData[a].type;

        //alert(total_value);
        if (saleType == "total") {

            final = parseFloat(total_value) * parseFloat(comision);
            //alert(final);
            final_sales = final / 100;
            //alert(final);
            if (totalAmountexComision == "") {
                totalAmountexComision = total_value;
            }

            totalAmountexComision = totalAmountexComision - final_sales;

        }
        else {
            //netProfitCountSum;
            netProfitData = {salerId: id, comision: comisison};
            netProfitCountSum[netProfitCounter] = netProfitData;
            netProfitData++;
        }


    }
    /*
     if (comisionType = "1") {
     final = parseFloat(total_value) * parseFloat(comisison);
     //alert(final);
     final_sales = final / 100;
     //alert(final);
     if (totalAmountexComision == "") {
     totalAmountexComision = total_value;
     }
     
     
     //allSalesData
     
     totalAmountexComision = total_value - final_sales;
     }
     else {
     
     final = parseFloat(total_profit) * parseFloat(comisison);
     //alert(final);
     final_sales = final / 100;
     //alert(final);
     //final_sales = total_profit-final;
     }
     */

    if (comisionType == "0") {

        final = parseFloat(comisison);
    }
    else {
        final_r = parseFloat($('#totalnet').val()) * parseFloat(comisison);
        final = parseFloat(final_r) / 100;

    }

    salesData = {salerId: id, comision: comisison, type: comisionType};

    allSalesData[salesCounter] = salesData;
    salesCounter++;


    html = "<input type='hidden' class='salerId' id='salerId-1" + salerId + "'  name='salers[amount][]' value='" + final + "'>";
    $("#fff").append(html);
    //alert(final);
    return final;
    //return final_sales;


}
function addingNewSales() {
    mergData = '';
    compId = $("#companyid").val();
    branchid = $("#branchid").val();
    ownerId = $("#ownerid").val();
    //alert(ownerId);
    if (ownerId != "")
        mergData += "&ownerId=" + ownerId + "";

    if (compId != "")
        mergData += "&companyid=" + compId;

    if (branchid != "")
        mergData += "&branchid=" + branchid + "";

    sales_data = $("#sales_form").serialize() + mergData;
    $.ajax({
        url: config.BASE_URL + "ajax/addSalesData",
        dataType: 'json',
        type: 'post',
        data: {sales: sales_data},
        cache: false,
        success: function (data)
        {
            salData = data;
            //salData =  $.parseJSON(data); 
            fName = $("#sales_form #fullname").val();
            htmlSales = '<option value="' + salData.saler_id + '">' + fName + '</option>';
            //$("#sales_form").serilaize();;
            $("#sales_list").append(htmlSales);
            html = "<input type='hidden'  id='salesCode" + salData.saler_id + "' value='" + salData.employee_code + "'>";
            $("#sales_names_list").append(html);

            html = "<input type='hidden' id='salesName" + salData.saler_id + "' value='" + fName + "'>";
            $("#sales_names_list").append(html);


            $("#osx2-overlay").trigger('click');
        }
    });


}

//codeid
function showCode(id) {
    salCode = $("#salesCode" + id).val();
    $("#sale_code").val(salCode);

}

//adding new record in table and array
prdocutCounter = 0;
prdocutCounter2 = 0;
function addItem() {

    pid = $("#productid").val();

    //alert(pid);
    if (pid != null) {
        qty = $("#quantity").val();
        tPrice = $("#total_price").val();


        //alert($("#afterDiscount").val());
        if ($("#afterDiscount").val() == "") {
            totalPrice = $("#item_total_price").val();
            //alert('item_total_price');
        } else {
            totalPrice = $("#afterDiscount").val();
            //alert('afterDiscount');
        }

        rand = randomRange(33, 800);
		
        pImage = $("#product_picture").val();
        pPrice = $("#purchasePrice" + pid).val();
        storeid_p = $("#storeid_p").val();
        storeId = $("#store_id").val();
        pName = $("#productname").val();
        itemDiscount = $("#item_discount").val();
        type = $('input[name=type]:checked').val();
        comment = $("#product_comment").val();
        minPrice = $("#min_price").val();
        notes = $("#product_notes").val();
        unit_price = $("#unit_price").val();
        price_purchase = $("#price_purchase").val();
        quantity_text_in = $("#quantity_text_in").val();
        product_type = $("#product_type").val();

        productData = {productId: pid, quantity: qty, salePrice: totalPrice, discount: itemDiscount, discountType: type, discription: comment, note: notes, total: tPrice, productName: pName, storeId: storeId, minPrice: minPrice, productPrice: price_purchase, unit_price: '', price_purchase: price_purchase, pImage: pImage, storeid_p: storeid_p, quantity_text_in: quantity_text_in, product_type: product_type};

        allProductsData[prdocutCounter2] = productData;

        /*
         addInput(productData, rand);
         console.log(productData);
         addrow(productData, rand);
         alterNetTotal(rand);
         */
        //show_popup();
        if (show_popup() == true) {
            addInput(productData, rand);
            console.log(productData);
            addrow(productData,rand);
            alterNetTotal(rand);
            //calculateProfitLoss();
        } else {

            return false;
        }
        prdocutCounter2++;

    }

}

function addItem_edit() {

    pid = $("#productid").val();
    if (pid != "") {
        qty = $("#quantity").val();
        tPrice = $("#total_price").val();


        //alert($("#afterDiscount").val());
        if ($("#afterDiscount").val() == "") {
            totalPrice = $("#item_total_price").val();
            //alert('item_total_price');
        } else {
            totalPrice = $("#afterDiscount").val();
            //alert('afterDiscount');
        }

        rand = randomRange(33, 800);
        pImage = $("#product_picture").val();
        pPrice = $("#purchasePrice" + pid).val();
        storeid_p = $("#storeid_p").val();
        storeId = $("#store_id").val();
        pName = $("#productname").val();
        itemDiscount = $("#item_discount").val();
        type = $('input[name=type]:checked').val();
        comment = $("#product_comment").val();
        minPrice = $("#min_price").val();
        notes = $("#product_notes").val();
        unit_price = $("#unit_price").val();
        price_purchase = $("#price_purchase").val();
        quantity_text_in = $("#quantity_text_in").val();
        productData = {productId: pid, quantity: qty, salePrice: totalPrice, discount: itemDiscount, discountType: type, discription: comment, note: notes, total: tPrice, productName: pName, storeId: storeId, minPrice: minPrice, productPrice: price_purchase, unit_price: '', price_purchase: price_purchase, pImage: pImage, storeid_p: storeid_p, quantity_text_in: quantity_text_in};

        allProductsData[prdocutCounter2] = productData;

        //show_popup();
        if (show_popup() == true) {
            addInput(productData, rand);
            console.log(productData);
            addrow_edit(productData, rand);
            alterNetTotal(rand);
            //calculateProfitLoss();
        } else {

            return false;
        }
        prdocutCounter2++;
    }
}
function show_popup() {
    quan = $("#quantity").val();
    store = $("#storequantity").val();


    storev = $("#store_id option:selected").val();
    purchase = $("input[name=purchase]").val();
    
    //alert($('select[name=store_id]').val());


    if ($('input[name=product_type]').val() == 'P') {



        if ($('select[name=store_id]').val() != '0') {
            
            
            
            
            if (purchase == 0) {
                return true;
                
            } else {
                
                if (storev != '0') {
                    if (quan > store) {
                        var r = confirm("هذه الكمية غير متاحة في المخزن  : الكمية المتاحة في المخزن    " + store);
                        if (r == true) {
                            x = "You pressed OK!";
                        } else {
                            x = "You pressed Cancel!";
                        }
                        
                        return false;
                    }else{
                        return true;
                    }
                    //return true;
                } else {

                    alert('يجب اختيار المخزن ');
                    return false;

                }
            }
            
        } else {

            alert('يجب اختيار المخزن ');
            return false;

        }

    } else {
        return true;
    }


}
function addItem2() {

    pid = $("#productid").val();
    if (pid != "") {
        qty = $("#quantity").val();
        tPrice = $("#total_price").val();

        if ($("#item_total_price").val() != "") {
            totalPrice = $("#item_total_price").val();
        } else {
            totalPrice = $("#item_total_price_prev").val();
        }
        rand = randomRange(33, 800);
        pImage = $("#product_picture").val();
        pPrice = $("#purchasePrice" + pid).val();
        storeid_p = $("#storeid_p").val();
        storeId = $("#store_id").val();
        pName = $("#productname").val();
        itemDiscount = $("#item_discount").val();
        type = $('input[name=type]:checked').val();
        comment = $("#product_comment").val();
        minPrice = $("#min_price").val();
        notes = $("#product_notes").val();
        unit_price = $("#unit_price").val();
        store_id = $("#store_id").val();
        supplier_id = $("#supplier_id").val();
        expireddate = $("#expireddate").val();
        product_type = $("#product_type").val();

        //alert(pImage);
        //productData = {productId: pid, quantity: qty, salePrice: totalPrice, discount: itemDiscount, discountType: type, discription: comment, note: notes, productImage: dd.product.product_picture, total: tPrice, productName: pName, storeId: storeId, minPrice: minPrice, productPrice: dd.product.purchase_price, unit_price: unit_price, store_id: store_id, supplier_id: supplier_id};
        productData = {productId: pid, quantity: qty, salePrice: totalPrice, discount: itemDiscount, discountType: type, discription: comment, note: notes, total: tPrice, productName: pName, storeId: storeId, minPrice: minPrice, productPrice: totalPrice, unit_price: '', price_purchase: totalPrice, storeid_p: storeid_p, pImage: pImage, expireddate: expireddate, product_type: product_type};
        allProductsData[prdocutCounter] = productData;
        prdocutCounter++;




        if (show_popup() == true) {
            addInput(productData, rand);
            console.log(productData, rand);
            addrow2(productData,rand);
            alterNetTotal(rand);
            //calculateProfitLoss();
        } else {

            return false;
        }


        //calculateProfitLoss();
    }
}

function addPurchaseItem() {

    pid = $("#productid").val();
    if (pid != "") {
        qty = $("#quantity").val();
        unit_price = $("#unit_price").val();
        pImage = $("#imageProduct" + pid).val();
        pPrice = $("#purchasePrice" + pid).val();
        storeId = $("#store_id").val();
        pName = $("#nameProduct" + pid).val();
        itemDiscount = $("#item_discount").val();
        supplier_id = $("#supplier_id").val();
        product_comment = $("#product_comment").val();
        minPrice = $("#min_price").val();
        notes = $("#product_notes").val();

        //console.log(dd+'ggggg');
        //alert(dd.product_picture+'ppp');
        productData = {productId: pid, quantity: qty, discription: product_comment, note: notes, productImage: dd.product.product_picture, productName: dd.product.itemname, storeId: storeId, supplier_id: supplier_id, unit_price: unit_price, payment_amount: ''};

        allProductsData[prdocutCounter] = productData;
        prdocutCounter++;
        //addInput(productData);
        console.log(productData);
        //addrow(productData);
        addpurchaserow(productData);
    }
    $("#productname").val('');
}


function addPaymetnHtml() {
    $("#osx2-overlay").trigger('click');

    if (payment_omr != "") {
        omrPayment = $("#payment_omr").val();
        percePayment = $("#payment_percentage").val();
        bankT = $("#bank_t").val();
        payment_label_parseFloatT = $("#payment_label_parseFloat_t").val();
        //payment_lgabel_parseFloatT = $("#payment_label_parseFloat_t").val();
        //period_t = $("#period_t").val();
        period_t = $("#period_t").val();
        paymentparseFloats_t = $("#paymentparseFloats_t").val();
        //paymentparseFloats_t = $("#paymentparseFloats_t").val();
        payment_date_t = $("#payment_date_t").val();
        omrVal = 'omr';
        perVal = 'percentage';
        pHtml = '<div class="form_field form-group payment_div"> ';
        pHtml += '<input name="total_payment_omr[]" type="text" id="total_payment_omr[]" onchange="calculatePayment(' + 1 + ',this)"  class="formtxtfield_small" value="' + omrPayment + '"/>';
        pHtml += '<span>RO.</span> </div>';
        pHtml += '<div class="form_field form-group payment_div">';
        pHtml += '<input name="total_payment_percentage[]" id="total_payment_percentage[]" type="text" onchange="calculatePayment(' + 2 + ',this)"  class="formtxtfield_small" value="' + percePayment + '"/>';
        pHtml += '<span>%</span> </div>';
        pHtml += '<div class="raw_payments"><div class="form_title">Type </div>';
        pHtml += '<div class="form_field form-group"><div class="dropmenu">';
        pHtml += '<div class="styled-select"><select name="payment_type" onchange="showPaymentLabel(this.value)"><option selected="selected" >Select Payment Method </option><option value="1">Cash</option><option  value="2">Cheque</option></select></div></div>';
        pHtml += '</div><div  class="form_title2" style="width:60px;">Bank </div><div class="form_field form-group">';
        pHtml += '<input name="bank" id="bank" type="text"  class="formtxtfield" value="' + bankT + '"/></div></div>';
        pHtml += '<div class="raw_payments">';
        pHtml += '<div class="form_title" id="payment_method_label">Cheque parseFloat </div>';
        pHtml += '<div class="form_field form-group"><input  type="text" id="payment_label_parseFloat" name="payment_label_parseFloat"  class="formtxtfield_small" value="' + payment_label_parseFloatT + '"/></div>';
        pHtml += '<div class="form_title2"style="width:60px;">Period </div><div class="form_field form-group">';
        pHtml += '<div class="formtxtfield_filter_small2"><input name="" type="text"  class="filter_field_small" value="' + period_t + '"/>';

        pHtml += '<div class="dropmenu_filter"><div class="styled-select_filter">';
        pHtml += '<select name=""><option selected="selected"  value="days">Days </option><option value="weeks">Weeks</option><option value="months">Months</option><option  value="years">Years</option></select></div></div></div></div>';
        pHtml += '<div class="form_title2">Payment  parseFloats </div><div class="form_field form-group"> <input name="input2" type="text"  class="formtxtfield_small" value="' + paymentparseFloats_t + '"/></div>';

        pHtml += '<div class="raw"><div class="form_title" id="payment_method_label">Payment Date </div>';
        pHtml += '<div class="form_field form-group"><input  type="text" id="payment_date" name="payment_date[]"  class="formtxtfield_small payment_date" value="' + payment_date_t + '"/></div></div></div>';
        //pHtml+='<div class="form_field form-group"><input  type="text" id="payment_date" name="payment_date[]"  class="formtxtfield_small payment_date"/>';
        $("#paymemts_div").append(pHtml);



        $(".payment_date").datepicker({
            defaultDate: "today",
            changeMonth: true,
            parseFloatOfMonths: 1,
        });

        $("#payment_omr").val('');
        $("#payment_percentage").val('');
        $("#bank_t").val('');
        $("#payment_label_parseFloat_t").val('');
        //payment_label_parseFloatT = $("#payment_label_parseFloat_t").val();
        //period_t = $("#period_t").val();
        $("#period_t").val('');
        $("#paymentparseFloats_t").val('');
        //paymentparseFloats_t = $("#paymentparseFloats_t").val();
        $("#payment_date_t").val('');

        remainingAmount = getRemainingPaymentAmount();
        $("#remaining_amount").html(remainingAmount);
        $("#remaining_t").html(remainingAmount);
    }
}

function calculatePaymentByAmountType() {

    calculatePaymentAmountPercentage();
    remainingAmount = getRemainingPaymentAmount();
    $("#remaining_amount").html(parseFloat(remainingAmount));
}


function calculatePaymentAmountPercentage() {
    remainingAmount = getRemainingPaymentAmount();
    if (jQuery('[name*="payments[total_payment_omr]"]').length > 0) {
        jQuery('[name*="total_payment_omr[]"]').each(function (index, value) {
            //var option = jQuery( this );
            //alert(index);
            //paymentValue = $(this).eq(index).val();
            pValue = jQuery('[name*="total_payment_omr[]"]').eq(index).val();
            //alert(pValue);
            if (pValue != "") {
                //remainingAmount = remainingAmount - pValue;
                final = parseFloat(pValue) / parseFloat(remainingAmount);
                final = final * 100;
                final = parseFloat(final);
                final.toFixed(2);
                jQuery('[name*="total_payment_percentage[]"]').eq(index).val(final);
            }
            else {
                total_payment_percentage = jQuery('[name*="total_payment_percentage[]"]').eq(index).val();//$("#total_payment_percentage").val();
                final = parseFloat(total_payment_percentage) * parseFloat(remainingAmount);
                final = final / 100;
                jQuery('[name*="total_payment_omr[]"]').eq(index).val(final);
            }
            //alert(remainingAmount);
            //console.log(paymentValue+'paymentValue');
            //console.log(value+'value');
        })
    }
}
function getRemainingPaymentAmount() {
    ind = 0;
    pType = $('input[name=paymentAmountType]:checked').val();

    if (pType == 1) {
        remainingAmount = net_total;
    }
    else {
        //net_total = ;
        remainingAmount = net_total - $("#receiverd_amount").val();
    }
    if (jQuery('[name*="total_payment_omr"]').length > 0) {
        jQuery('[name*="total_payment_omr"]').each(function (index, value) {
            //var option = jQuery( this );
            //alert(index);
            paymentValue = $(this).eq(index).val();
            pValue = jQuery('[name*="total_payment_omr"]').eq(index).val();
            //alert(pValue);

            remainingAmount = remainingAmount - pValue;
            //alert(remainingAmount);
            //console.log(paymentValue+'paymentValue');
            //console.log(value+'value');
        });
    }
    //alert(remainingAmount);
    payment_omr = $("#payment_omr").val();
    if (payment_omr != "") {
        remainingAmount = remainingAmount - payment_omr;
    }
    return remainingAmount;
}
///calculate payments 
totalpaymentOmr = 0;
function calculatePayment(paymentType, pOBj) {

    pp = pOBj;
    //alert('calculate');
    pId = $(pOBj).attr('id');
    pIndex = $(pOBj).index();
    receiveAmount = $("#receiverd_amount").val();
    if (pId == 'total_payment_omr[]') {
        //alert('asd');
        if (jQuery('[name*="total_payment_omr"]').length > 0) {
            jQuery('[name*="total_payment_omr"]').each(function (index, value) {
                //var option = jQuery( this );
                //alert(index);

                //paymentValue = $(this).eq(index).val();
                pValue = jQuery('[name*="total_payment_omr"]').eq(index).val();
                //alert(pValue);
                totalpaymentOmr = parseFloat(totalpaymentOmr) + parseFloat(pValue);

                //alert(pValue);

                //alert(remainingAmount);
                //console.log(paymentValue+'paymentValue');
                //console.log(value+'value');
            })
            remainingAmount = receiveAmount - totalpaymentOmr;
            perc = $(pOBj).val() / receiveAmount;
            percantage = perc * 100;

            $(".rem_amount").eq(pIndex).html(remainingAmount.toFixed(2));
            jQuery('[name*="total_payment_percentage"]').eq(pIndex).val(percantage);
            //$('#payment_percentage').val(percantage);
        }

    }

}
var counter = 0;
function add_new_payment() {

    var content = $(".hide_form_payment").html();
    counter++;
    var html = "<div class='more_add_new_payment" + counter + "'><div style='position: absolute;left: 0px;margin-left: 45px;margin-top: 10px;'>\n\
            <a href='javascript:void()' style='text-decoration: none;' onclick='remove_add_payment(" + counter + ")'><i class='icon-remove-sign' style='color:red;font-size:20px;'></i></a></div><hr class='hr_payment'>" + content + "</div>";

    $(".more_add_new_payment_main").append(html);
    //alert($(".more_add_new_payment"+counter+" .styled-select select").text());
    $(".more_add_new_payment" + counter + " .styled-select select").removeClass("1 " + counter + "");
    $(".more_add_new_payment" + counter + " .styled-select select").addClass("" + counter + "");


    $(".more_add_new_payment" + counter + " .styled-select2 select").attr("onchange", "showPaymentLabel2(this.value," + counter + ")");
    $(".more_add_new_payment" + counter + " .styled-select3 select").attr("onchange", "getBankAccounts3(this.value," + counter + ")");


    //alert(".more_add_new_payment"+counter+" .styled-select select");

}
function add_new_notes() {
    var content = $(".hide_form_notes").html();
    counter++;
    var html = "<br clear='all'/><div class='more_add_new_notes" + counter + "'><div style='position: absolute;left: 0px;margin-left: 45px;margin-top: 10px;'>\n\
            <a href='javascript:void()' style='text-decoration: none;' onclick='remove_add_notes(" + counter + ")'><i class='icon-remove-sign ' style='color:red;font-size:20px;'></i></a></div><hr class='hr_payment'>" + content + "</div>";

    $(".more_add_new_notes_main").append(html);


}
function add_new_files() {
    var content = $(".hide_form_files").html();
    counter++;
    var html = "<br clear='all'/><div class='more_add_new_files" + counter + "'><div style='position: absolute;left: 0px;margin-left: 45px;margin-top: 10px;'>\n\
            <a href='javascript:void()' style='text-decoration: none;' onclick='remove_add_files(" + counter + ")'><i class='icon-remove-sign' style='color:red;font-size:20px;'></i></a></div><hr class='hr_payment'>" + content + "</div>";

    $(".more_add_new_files_main").append(html);


}
function add_new_expences() {
    var content = $(".hide_form_expences").html();
    counter++;
    var html = "<br clear='all'/><div class='more_add_new_expences" + counter + "'><div style='position: absolute;left: 0px;margin-left: 45px;margin-top: 10px;'>\n\
            <a href='javascript:void()' style='text-decoration: none;' onclick='remove_add_expences(" + counter + ")'><i class='icon-remove-sign' style='color:red;font-size:20px;'></i></a></div><hr class='hr_payment'>" + content + "</div>";

    $(".more_add_new_expences_main").append(html);
    //alert($(".more_add_new_payment"+counter+" .styled-select select").text());
    $(".more_add_new_expences" + counter + " .styled-select select").removeClass("1 " + counter + "");
    $(".more_add_new_expences" + counter + " .styled-select select").addClass("" + counter + "");


    //$(".more_add_new_expences" + counter + " .styled-select2 select").attr("onchange", "showPaymentLabel2(this.value," + counter + ")");
    $(".more_add_new_expences" + counter + " .styled-select3 select").attr("onchange", "getBankAccounts4(this.value," + counter + ")");


    //alert(".more_add_new_payment"+counter+" .styled-select select");

}
function remove_add_payment(id) {

    $(".more_add_new_payment" + id).remove();

}
function remove_add_expences(id) {

    $(".more_add_new_expences" + id).remove();

}
function remove_add_notes(id) {

    $(".more_add_new_notes" + id).remove();

}
function remove_add_files(id) {

    $(".more_add_new_files" + id).remove();

}
function get_my_allaccount(iden) {


    //http://localhost/root/bsnew/

    if (iden == "1") {
        $.ajax({
            url: config.BASE_URL + "sales_management/get_last_balance",
            type: 'post',
            data: {customerid: $("input[name=customerid]").val()},
            cache: false,
            //dataType:"json",
            success: function (data)
            {
                var response = $.parseJSON(data);

                $('#get_my_allaccount').html(response.payment_amount);
                $('#lastpaymentoperation').val(response.id);
                //$('#storeid').html(response.store);
                //$('#customerid').html(response.customer);
            }
        });
    } else {
        $('#get_my_allaccount').html("");
        $('#lastpaymentoperation').val("");

    }
}

function get_option_price() {
    //val=$("#get_option_p:checked").val();
    val = $('#get_option_p:checked').val();
    purchase = $("#purchase_price").val();
    calpurchase1 = $("#cal_sale_price").val();
    salingbytotal = $("#cal_salingbytotal").val();
    min_sale_price = $("#cal_min_sale_price").val();


    if (val === "1") {

        cal1 = parseFloat(purchase) + parseFloat(calpurchase1);
        cal_salingbytotal = parseFloat(purchase) + parseFloat(salingbytotal);
        cal_min_sale_price = parseFloat(purchase) + parseFloat(min_sale_price);

        if (calpurchase1 != "") {
            $("#sale_price").val(cal1.toFixed(3));
        } else {
            $("#sale_price").val("");
        }
        if (salingbytotal != "") {
            $("#salingbytotal").val(cal_salingbytotal.toFixed(3));
        } else {
            $("#salingbytotal").val("");
        }

        if (min_sale_price != "") {
            $("#min_sale_price").val(cal_min_sale_price.toFixed(3));
        } else {
            $("#min_sale_price").val("");
        }

    } else if (val === "2") {

        cal2 = (parseFloat(purchase) * parseFloat(calpurchase1) / 100) + parseFloat(purchase);
        cal_salingbytotal2 = (parseFloat(purchase) * parseFloat(salingbytotal) / 100) + parseFloat(purchase);
        cal_min_sale_price2 = (parseFloat(purchase) * parseFloat(min_sale_price) / 100) + parseFloat(purchase);

        if (calpurchase1 != "") {
            $("#sale_price").val(cal2.toFixed(3));
        } else {
            $("#sale_price").val("");
        }
        if (salingbytotal != "") {
            $("#salingbytotal").val(cal_salingbytotal2.toFixed(3));
        } else {
            $("#salingbytotal").val("");
        }
        if (min_sale_price != "") {
            $("#min_sale_price").val(cal_min_sale_price2.toFixed(3));
        } else {
            $("#min_sale_price").val("");
        }

    }

}

function offer_dicount() {
    val = $('#offer_dicounte:checked').val();
    if (val == "1") {
        $(".related_offer_dicount1").show();
        $(".related_offer_dicount2").show();
        $(".related_offer_dicount3").show();
        $(".related_offer_dicount4").show();

    } else {
        $(".related_offer_dicount1").hide();
        $(".related_offer_dicount2").hide();
        $(".related_offer_dicount3").hide();
        $(".related_offer_dicount4").hide();

    }

}

function add_more_print() {
    r = randomRange(10, 312);
    html = '<div class="g16" id="remove_print_page' + r + '"><hr/><div style="" class="remove_print_page">\n\
            <a href="javascript:void()" style="text-decoration: none;" onclick="remove_more_print(' + r + ')"><i class="icon-remove-sign" style="color:red;font-size:20px;"></i></a>\n\
            </div><div class="g3"><label class="">عنوان الخاصية</label>\n\
            <input type="text" name="options_print[name][]" value=""/></div>   \n\
            <div class="g16"> <textarea name="options_print[text][]" class="ckeditor"></textarea></div> <br clear="all"/></div> ';

    $('#more_print_option').append(html);
}
function remove_more_print(id) {

    $("#remove_print_page" + id).remove();

}
/*function calculatePayment(paymentType,pOBj){
 
 pp = pOBj;
 //alert(paymentType);
 pIndex  = $(pOBj).index();
 pId = $(pOBj).attr('id');
 
 //alert(pId);
 remainingAmount = getRemainingPaymentAmount();
 //alert(remainingAmount);
 //alert(remainingAmount);
 if(pId == 'payment_omr[]'){
 
 totalpaymentOmr = totalpaymentOmr++$(pOBj).val();
 final = parseFloat(totalpaymentOmr)/parseFloat(remainingAmount);
 final = final*100;
 final = parseFloat(final);
 final.toFixed(2);
 
 //alert(final);
 $('#payment_percentage').val(final);
 
 }
 else if(pId == 'payment_percentage'){
 
 total_payment_percentage = $('#payment_percentage').val();//$("#total_payment_percentage").val();
 final = parseFloat(total_payment_percentage)*parseFloat(remainingAmount);
 //alert(final);
 final = final/100;
 //alert(final);
 //final_total = total-final;
 $('#payment_omr').val(final);
 
 
 }
 else{
 
 if(paymentType == 1){
 
 
 //total_payment_percentage = $("#total_payment_percentage").val();
 totalpaymentOmr = $(pOBj).eq(pIndex).val();
 final = parseFloat(totalpaymentOmr)/parseFloat(remainingAmount);
 final = final*100;
 final = parseFloat(final);
 final.toFixed(2);
 
 //alert(final);
 $('[name*="total_payment_percentage[]"]').eq(pIndex).val(final);
 }
 else{
 total_payment_percentage = $(pOBj).eq(pIndex).val();//$("#total_payment_percentage").val();
 final = parseFloat(total_payment_percentage)*parseFloat(remainingAmount);
 //alert(final);
 final = final/100;
 //alert(final);
 //final_total = total-final;
 
 $('[name*="total_payment_omr[]"]').eq(pIndex).val(final);
 
 }		
 
 }
 
 remainingAmount = getRemainingPaymentAmount();	
 $("#remaining_amount").html(parseFloat(remainingAmount));
 $("#remaining_t").html(parseFloat(remainingAmount));
 }*/






