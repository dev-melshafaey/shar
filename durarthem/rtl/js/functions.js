// ADMAX GLOBAL FUNCTIONS AND PLUGINS



// TOASTR NOTIFICATIONS



(function (n) {

    n(["jquery"], function (n) {

        var t = function () {

            var u = {tapToDismiss: !0, toastClass: "toast", containerId: "toast-container", debug: !1, fadeIn: 400, fadeOut: 400, extendedTimeOut: 1e3, iconClasses: {error: "toast-error red", info: "toast-info blue", success: "toast-success green", warning: "toast-warning orange"}, iconClass: "toast-info", positionClass: "toast-top-right", timeOut: 5e3, titleClass: "toast-title", messageClass: "toast-message"}, f = function (n, t, u) {

                return r({iconClass: i().iconClasses.error, message: n, optionsOverride: u, title: t})

            }, e = function (t) {

                var i = n("#" + t.containerId);

                return i.length ? i : (i = n("<div/>").attr("id", t.containerId).addClass(t.positionClass), i.appendTo(n("body")), i)

            }, i = function () {

                return n.extend({}, u, t.options)

            },

                    o = function (n, t, u) {

                        return r({iconClass: i().iconClasses.info, message: n, optionsOverride: u, title: t})

                    }, r = function (t) {

                var r = i(), o = t.iconClass || r.iconClass;

                typeof t.optionsOverride != "undefined" && (r = n.extend(r, t.optionsOverride), o = t.optionsOverride.iconClass || o);

                var s = null, h = e(r), u = n("<div/>"), c = n("<div/>"), l = n("<div/>"), a = {options: r, map: t};

                t.iconClass && u.addClass(r.toastClass).addClass(o), t.title && (c.append(t.title).addClass(r.titleClass), u.append(c)), t.message && (l.append(t.message).addClass(r.messageClass), u.append(l));

                var f = function () {

                    if (!(n(":focus", u).length > 0)) {

                        var t = function (n) {

                            return u.fadeOut(r.fadeOut, n)

                        }, i = function () {

                            u.is(":visible") || (u.remove(), h.children().length === 0 && h.remove())

                        };

                        t(i)

                    }

                },

                        v = function () {

                            (r.timeOut > 0 || r.extendedTimeOut > 0) && (s = setTimeout(f, r.extendedTimeOut))

                        }, y = function () {

                    clearTimeout(s), u.stop(!0, !0).fadeIn(r.fadeIn)

                };

                return u.hide(), h.prepend(u), u.fadeIn(r.fadeIn), r.timeOut > 0 && (s = setTimeout(f, r.timeOut)), u.hover(y, v), !r.onclick && r.tapToDismiss && u.click(f), r.onclick && u.click(function () {

                    r.onclick() && f()

                }), r.debug && console && console.log(a), u

            }, s = function (n, t, u) {

                return r({iconClass: i().iconClasses.success, message: n, optionsOverride: u, title: t})

            }, h = function (n, t, u) {

                return r({iconClass: i().iconClasses.warning, message: n, optionsOverride: u, title: t})

            }, c = function (t) {

                var u = i(), r = n("#" + u.containerId), f;

                if (t && n(":focus", t).length === 0) {

                    f = function () {

                        t.is(":visible") || (t.remove(),

                                r.children().length === 0 && r.remove())

                    }, t.fadeOut(u.fadeOut, f);

                    return

                }

                r.length && r.fadeOut(u.fadeOut, function () {

                    r.remove()

                })

            };

            return{clear: c, error: f, info: o, options: {}, success: s, version: "1.1.4.2", warning: h}

        }();

        return t

    })

})(typeof define == "function" && define.amd ? define : function (n, t) {

    typeof module != "undefined" && module.exports ? module.exports = t(require(n[0])) : window.toastr = t(window.jQuery)

});



// COOKIES



jQuery.cookie = function (a, b, c) {

    if (arguments.length == 0) {

        var d = (new Date).getTime();

        document.cookie = "__cookieprobe=" + d + ";path=/";

        return document.cookie.indexOf(d) !== -1

    } else if (arguments.length > 1 && String(b) !== "[object Object]") {

        c = jQuery.extend({}, c);

        if (b === null || b === undefined) {

            c.expires = -1

        }

        if (typeof c.expires === "number") {

            var e = c.expires, f = c.expires = new Date;

            f.setDate(f.getDate() + e)

        }

        b = String(b);

        return document.cookie = [encodeURIComponent(a), "=", c.raw ? b : encodeURIComponent(b), c.expires ? "; expires=" + c.expires.toUTCString() : "", c.path ? "; path=" + c.path : "", c.domain ? "; domain=" + c.domain : "", c.secure ? "; secure" : ""].join("")

    } else {

        c = b || {};

        var g, h = c.raw ? function (a) {

            return a

        } : decodeURIComponent;

        return(g = (new RegExp("(?:^|; )" + encodeURIComponent(a) + "=([^;]*)")).exec(document.cookie)) ? h(g[1]) : null

    }

};

jQuery.storage = function (a, b) {

    if (arguments.length == 0) {

        try {

            return"localStorage"in window && window["localStorage"] !== null

        } catch (c) {

            return false

        }

    } else if (arguments.length == 1) {

        return localStorage.getItem(a)

    } else {

        if (b === null) {

            return localStorage.removeItem(a)

        } else {

            return localStorage.setItem(a, b)

        }

    }

};

jQuery.clearStorage = function () {

    localStorage.clear()

};

jQuery.storageKey = function (a) {

    return localStorage.key(a)

};



// BASIC FUNCTIONS



$(function () {



    // SIDEBAR COLLAPSE



    $('#coll-sidebar').click(function () {

        //alert('hihi');

        if ($('#wrapper').hasClass('mini')) {

            $('#wrapper').removeClass('mini');



            $('#sidebar2').css('width', '71px');

        } else {

            $('#wrapper').addClass('mini');

            $('#sidebar2').css('width', '37px');

        }

        ;

    });



    // TOP BAR FUNCTIONS



    $('#tb-handle').click(function () {

        var topBar = $(this).parent('.top-bar');

        if (topBar.hasClass('hide')) {

            topBar.removeClass('hide');

        } else {

            topBar.addClass('hide');

        }

        ;

    });



    $('.expand-tg').click(function () {

        var trigger = $(this);

        target = trigger.data('expand');

        par = trigger.parents('.item');

        expCont = par.find('.expand-content');

        targetEl = expCont.children(target);

        targetEl.addClass('show').siblings('.show').removeClass('show');

        par.addClass('expand');

        par.find('.coll-handle').fadeIn(300);

    });



    $.fn.collapseBarItem = function () {

        var par = $('.top-bar').find('.item.expand');

        cont = par.find('.show');

        cont.removeClass('show');

        par.removeClass('expand');

        par.find('.coll-handle').fadeOut(300);

    };



    $('.coll-handle, #tb-handle').click(function () {

        $.fn.collapseBarItem();

    });



    var collTime = null;

    $('.top-bar').children('.item').mouseleave(function () {

        if ($(this).hasClass('expand'))

            collTime = setTimeout(function () {

                $.fn.collapseBarItem();

            }, 800);

    }).mouseenter(function () {

        clearTimeout(collTime);

        collTime = null;

    });



    // BOX COLLAPSE



    $('.box .header').click(function (e) {

        if ($(e.target).is('.header *')) {

            return;

        }

        var tgbox = $(this).parent('.box');

        tgcont = tgbox.children('.content');

        if (!tgbox.hasClass('coll-only')) {

            if (tgbox.hasClass('coll')) {

                tgbox.removeClass('coll');

                tgcont.slideDown(500, function () {

                    tgbox.removeClass('coll-ov');

                    tgcont.removeAttr('style')

                });

            }

            else {

                tgbox.addClass('coll-ov');

                tgcont.slideUp(300, function () {

                    tgbox.addClass('coll')

                });

            }

        }

        ;

    });



});



// CHAT CONVERSATION



$.fn.chat = function () {

    $('.chat-form').submit(function (e) {

        e.preventDefault();

        var chatBox = $(this).parents('.chat-box');

        chatCont = chatBox.find('.chat-cont');

        chatInp = $(this).children('.chat-inp');

        chatInpVal = chatInp.val();

        msgHeight = 0;

        if (chatCont.hasClass('nav-cont'))

            chatCont = chatCont.find('.chat-msg.show');

        var contHeight = chatCont.outerHeight();

        if (chatInpVal.length > 0) {

            var scrollTriggered = 0;

            if (chatCont.hasClass('scroll')) {

                chatCont = chatCont.find('.scroll-cont');

                scrollTriggered = 1;

            }

            ;

            $(chatCont).append('<p class="sent">' + chatInpVal + '<span class="msg-info">Sent just now</span></p>').children('p').each(function () {

                msgHeight += $(this).outerHeight(true);

                if (contHeight < msgHeight && scrollTriggered == 0) {

                    chatCont.addClass('scroll').wrapInner('<div class="scroll-cont"></div>').nanoScroller({scroll: 'bottom'});

                    scrollTriggered = 1;

                }

                if (scrollTriggered == 1)

                    chatCont.parent('.scroll').nanoScroller({scroll: 'bottom'});

            });

            chatInp.val('');

        }

    })

};


function getConvetData(){
//	alert('convert');
	$.ajax({

        url: config.BASE_URL + "ajax/getConvetData",
        dataType: 'html',
        type: 'get',
        cache: false,

        success: function (data){
			//alert(data);
			dd = data;
			$("#conversiontest").html(data);
 			ccv = $("#currency_converter_result .bld").html();
			ccv = parseFloat(ccv);
			//alert(ccv);
			
			if(!isNaN(ccv)){
			//	alert('iff');
				ccv = parseFloat(ccv);
				newvalus = ccv.toFixed(3);
			}
			else
			newvalus = data;
			
			//alert(newvalus);
			$("#dharamval").val(newvalus);
		}

    });

}
function getProductData2(product_id, store)
{
	
      if (product_id != "") {
        $.ajax({
            url: config.BASE_URL + "ajax/getProductData",
            dataType: 'json',
            type: 'post',
            data: {pid: product_id, storeid: store, sid: $("#store_id").val()},
            cache: false,
            success: function (data)
            {
                //console.log('data');
                dd = data;
                console.log(dd);

                // dd.product.sale_price;

                //mi_descr
                //alert('sd');
                purchase_price = dd.product.purchase_price;
                min_sale_result = (dd.product.min_sale_price / 100) * purchase_price;
                sale_result = (dd.product.sale_price / 100) * purchase_price;

                sale_result = parseFloat(purchase_price) + parseFloat(sale_result);
                //min_sale_result = parseFloat(purchase_price) + parseFloat(min_sale_result);
                min_sale_result = parseFloat(dd.product.min_sale_price);

                $("#min_price").val(min_sale_result);
				$("#item_aed_price").val(dd.product.purchase_price_dharam);

                $("#price_purchase").val(parseFloat(dd.product.purchase_price).toFixed(3));

                $("#perbox_quantity").val(dd.product.box_per_quantity);
				
                $("#box_serial").val(dd.product.box_barcodenumber);
                $("#item_total_price").val(dd.product.purchase_price);

                boxprice = dd.product.purchase_price*dd.product.box_per_quantity;

                $("#box_sale_price").val(parseFloat(boxprice).toFixed(3));
                $("#total_price").val(sale_result.toFixed(3));
			
                $("#item_total_price_prev").val(parseFloat(dd.product.purchase_price).toFixed(3));


                $("#quantity_text").html('(' + dd.unit + ')');
                $("#quantity_text_in").val('(' + dd.unit + ')');

                //alert($("#customer_ftotal").val());
                /*
                 if($("#customer_ftotal").val()!="1"){
                 $("#item_total_price").val(dd.product.sale_price);
                 } else {
                 $("#item_total_price").val(dd.product.salingbytotal);
                 $("#fortaotal").html("سعر البيع للجمله");
                 }
                 */

                //$("#item_total_price").val(sale_result.toFixed(2));

                $("#serialparseFloat").val(dd.product.serialparseFloat);
//                $("#quantity").val(1);
                $("#price_product").val(parseFloat(dd.product.sale_price).toFixed(3));
                if (dd.product.quantity == 0) {
                    $("#quantity").val('1');
                } else {
                    $("#quantity").val();
                }

                $("#product_comment").text(dd.product.notes);
                $("#product_picture").val(dd.product.product_picture);
                $("input[name=storeid_p]").val(dd.product.storeid);

                //alert(dd.product.itemtype);
                $("#product_type").val(dd.product.itemtype);

                $("input[name=productname]").val(dd.itemname2);
                //alert(dd.product.itemid);
                $("input[name=productid]").val(dd.product.itemid);

                $("#avrage").val(dd.totalval);
                //alert(dd.product.storeid);
                //$("#price_product").val(dd.product.product_comment);
                //`notes`
                // $( "#branches" ).fadeOut( "slow" );


                $("#shelf").val(dd.product.shelf);


                var val = $('select[name=store_id]').val();
                var pid = dd.product.itemid;

                $.ajax({
                    url: config.BASE_URL + "sales_management/get_store_quantity",
                    type: "POST",
                    data: {val: val, pid: pid},
                    success: function (e) {

                        $('#storequantity').val(e);
                        //getProductData(pid, '', val);
                    }
                });

            }
        });
    }
}
function get_option_price() {
	//alert("asdasd");
    //val=$("#get_option_p:checked").val();
    val = $('#get_option_p:checked').val();
    purchase = $("#purchase_price").val();
    calpurchase1 = $("#cal_sale_price").val();
    salingbytotal = $("#cal_salingbytotal").val();
    min_sale_price = $("#cal_min_sale_price").val();


    if (val === "1") {

        cal1 = parseFloat(purchase) + parseFloat(calpurchase1);
        cal_salingbytotal = parseFloat(purchase) + parseFloat(salingbytotal);
        cal_min_sale_price = parseFloat(purchase) + parseFloat(min_sale_price);

        if (calpurchase1 != "") {
            $("#sale_price").val(cal1.toFixed(3));
        } else {
            $("#sale_price").val("");
        }
        if (salingbytotal != "") {
            $("#salingbytotal").val(cal_salingbytotal.toFixed(3));
        } else {
            $("#salingbytotal").val("");
        }

        if (min_sale_price != "") {
            $("#min_sale_price").val(cal_min_sale_price.toFixed(3));
        } else {
            $("#min_sale_price").val("");
        }

    } else if (val === "2") {

        cal2 = (parseFloat(purchase) * parseFloat(calpurchase1) / 100) + parseFloat(purchase);
        cal_salingbytotal2 = (parseFloat(purchase) * parseFloat(salingbytotal) / 100) + parseFloat(purchase);
        cal_min_sale_price2 = (parseFloat(purchase) * parseFloat(min_sale_price) / 100) + parseFloat(purchase);

        if (calpurchase1 != "") {
            $("#sale_price").val(cal2.toFixed(3));
        } else {
            $("#sale_price").val("");
        }
        if (salingbytotal != "") {
            $("#salingbytotal").val(cal_salingbytotal2.toFixed(3));
        } else {
            $("#salingbytotal").val("");
        }
        if (min_sale_price != "") {
            $("#min_sale_price").val(cal_min_sale_price2.toFixed(3));
        } else {
            $("#min_sale_price").val("");
        }

    }

}
prdocutCounter = 0;

function addInput(obj,rand) {
    //alert(counter);	
    
    // var newdiv = document.createElement('div');
    html = "<div id='invoice_input_"+rand+"'> <input type='hidden' class='pi" + rand + "' id='productId" + rand + "'  name='items[productId][]' value='" + obj.productId + "' />";
    //document.getElementById(divName).appendChild(newdiv);
    

    html += "<input type='hidden' class='pi" + rand + "' id='productQuantity" + rand + "'  name='items[quantity][]' value='" + obj.quantity + "' />";
    

    html += "<input type='hidden' class='pi" + rand + "' id='productTotal" + rand + "'  name='items[productTotal][]' value='" + obj.salePrice + "' />";
    

    html += "<input type='hidden' class='pi" + rand + "' id='minimum" + rand + "'  name='items[minimum][]' value='" + obj.minPrice + "'>";
    

    html += "<input type='hidden' class='pi" + rand + "' id='productPerPrice" + rand + "'  name='items[productperPrice][]' value='" + obj.total + "' />";
    

    html += "<input type='hidden' class='pi" + rand + "' id='productDiscount" + rand + "'  name='items[productDiscount][]' value='" + obj.discount + "' />";
    
    html += "<input type='hidden' class='pi" + rand + "' id='productDiscountType" + rand + "'  name='items[productDiscountType][]' value='" + obj.discountType + "' />";
    
    html += "<input type='hidden' class='pi" + rand + "' id='productDiscription" + rand + "'  name='items[productDiscription][]' value='" + obj.discription + "' />";
    
    html += "<input type='hidden' class='pi" + rand + "' id='productNotes" + rand + "'  name='items[productNotes][]' value='" + obj.note + "' />";
    

    html += "<input type='hidden' class='pi" + rand + "' id='supplier" + rand + "'  name='items[supplier_id][]' value='" + obj.supplier_id + "' />";
    

    html += "<input type='hidden' class='pi" + rand + "' id='unit_price" + rand + "'  name='items[unit_price][]' value='" + obj.unit_price + "' />";
    

    html += "<input type='hidden' class='pi" + rand + "' id='store_id" + rand + "'  name='items[store_id][]' value='" + obj.storeId + "' />";
    

    html += "<input type='hidden' class='pi" + rand + "' id='supplier_id" + rand + "'  name='items[supplier_id][]' value='" + obj.supplier_id + "' /> ";
    
    html += "<input type='hidden' class='pi" + rand + "' id='price_purchase" + rand + "'  name='items[price_purchase][]' value='" + obj.price_purchase + "' /> ";
    
    html += "<input type='hidden' class='pi" + rand + "' id='discount" + rand + "'  name='items[discount][]' value='" + obj.discount + "' />";
    html += "<input type='hidden' class='pi" + rand + "' id='discountType" + rand + "'  name='items[discountType][]' value='" + obj.discountType + "' /> </div>";
    
    
    
    $(".invoice_raw_product_items").append(html);

    counter++;
}

function addrow2(productData) {
    tbHtml = '';
    console.log(productData);
    //console.log('productData');
    type = '';
    if (productData.discountType == '1') {
        if (productData.discount != "")
            disc = productData.discount + '%';
        else
            disc = '0 %';
    }
    else
    {

        if (productData.discount != "")
            disc = productData.discount + 'RO';
        else
            disc = productData.discount + 'RO';
    }

    //totalProductAmount = productData.total * productData.quantity;
    totalProductAmount = productData.unit_price * productData.quantity;
//	alert(totalProductAmount);
    //productData.storeId;
    rowCounter++;


    tbHtml += '<tr id="tr_' + productData.productId + '"><td >' + rowCounter + '</td>';
    tbHtml += '<td ><img src="' + config.BASE_URL + '/uploads/item//' + productData.productImage + '" width="50" height="50" /></td>';
    tbHtml += '<td style="text-align:center"> ' + productData.productName + ' </td>';
    tbHtml += '<td id="quantity" align="center" valign="middle" style="text-align:center" contentEditable="true" onkeyup="calculateCart(this,' + productData.productId + ')">' + productData.quantity + ' </td>';
    tbHtml += '<td id="description" contentEditable="true">' + productData.discription + ' </td>';

    tbHtml += '<td id="unit_price" style="text-align:center" contentEditable="true">' + productData.unit_price + '</td>';
    tbHtml += '<td id="totalAmount" style="text-align:center" contentEditable="true">' + totalProductAmount + '</td>';
//    tbHtml += '<td id="" style="text-align:center" contentEditable="true"></td>';
    tbHtml += '<td style="text-align:center;cursor:pointer;" ><img src="' + config.BASE_URL + '/images/internal/delete_small.png" width="7" height="8" border="0"   onclick="confirmRemove(' + prdocutCounter + ')"/></td>';
    tbHtml += '<td align="center" valign="middle" style="text-align:center"><input class="mainten" name="editmainten" id="editmainten" type="checkbox" value="" onchange="addMaintenance(' + productData.productId + ',this)" /></td>';
    tbHtml += '</tr>';

    netTotals[netCouner] = totalProductAmount;
    netCouner++;

    alterNetTotal();
    //$("#product_list").append(tbHtml);
    //console.log(tbHtml);
    $('#product_list tr.grand-total').before(tbHtml);

    $("#productid").val('');
    $("#quantity").val('');
    $("#total_price").val('');
    $("#item_total_price").val('');
    $("#imageProduct" + pid).val();
    $("#nameProduct" + pid).val();
    $("#item_discount").val('');
    $("#product_comment").val('');
    $("#product_notes").val('');
    $("#min_price").val('');
    $("#serialnumber").val('');
    $("#afterDiscount").val('');
    $("#productname").val('');
    $("#unit_price").val('');
    $("#store_id").val('');
}
function randomRange(min, max) {
  return ~~(Math.random() * (max - min + 1)) + min
}

function show_popup() {

    quan = $("#quantity").val();

    store = $("#storequantity").val();





    storev = $("#store_id option:selected").val();

    purchase = $("input[name=purchase]").val();



    //alert($('select[name=store_id]').val());





    if ($('input[name=product_type]').val() == 'P') {







        if ($('select[name=store_id]').val() != '0') {









            if (purchase == 0) {

                return true;



            } else {



                if (storev != '0') {

                    if (quan > store) {

                        var r = confirm("هذه الكمية غير متاحة في المخزن  : الكمية المتاحة في المخزن    " + store);

                        if (r == true) {

                            x = "You pressed OK!";

                        } else {

                            x = "You pressed Cancel!";

                        }



                        return false;

                    } else {

                        return true;

                    }

                    //return true;

                } else {



                    alert('يجب اختيار المخزن ');

                    return false;



                }

            }



        } else {



            alert('يجب اختيار المخزن ');

            return false;



        }



    } else {

        return true;

    }





}
function addItem2() {

    pid = $("#productid").val();
    if (pid != "") {
        qty = $("#total_quantity").val();
        tPrice = $("#total_price").val();

        if ($("#item_total_price").val() != "") {
            totalPrice = $("#item_total_price").val();
        } else {
            totalPrice = $("#item_total_price_prev").val();
        }
        rand = randomRange(33, 800);
        pImage = $("#product_picture").val();
        pPrice = $("#purchasePrice" + pid).val();
        storeid_p = $("#storeid_p").val();
        storeId = $("#store_id").val();
        pName = $("#productname").val();
        itemDiscount = $("#item_discount").val();
        type = $('input[name=type]:checked').val();
        comment = $("#product_comment").val();
        minPrice = $("#min_price").val();
        notes = $("#product_notes").val();
        perbox_quantity = $("#perbox_quantity").val();
        unit_price = $("#unit_price").val();
        box_price = $("#box_sale_price").val();
        totalbox = $("#totalbox").val();
        store_id = $("#store_id").val();
        supplier_id = $("#supplier_id").val();
        expireddate = $("#expireddate").val();

        product_type = $("#product_type").val();
        Alternate_Price = $("#Alternate_Price").val();
        //alert(product_type);
        //alert(pImage);
        //productData = {productId: pid, quantity: qty, salePrice: totalPrice, discount: itemDiscount, discountType: type, discription: comment, note: notes, productImage: dd.product.product_picture, total: tPrice, productName: pName, storeId: storeId, minPrice: minPrice, productPrice: dd.product.purchase_price, unit_price: unit_price, store_id: store_id, supplier_id: supplier_id};
        productData = {productId: pid, quantity: qty, salePrice: totalPrice, discount: itemDiscount, discountType: type, discription: comment, note: notes, total: tPrice, productName: pName, storeId: storeId, minPrice: minPrice, productPrice: totalPrice, unit_price: '', price_purchase: totalPrice, storeid_p: storeid_p, pImage: pImage, expireddate: expireddate, product_type: product_type, Alternate_Price: Alternate_Price,box_price:box_price,totalbox:totalbox,perbox_quantity:perbox_quantity};
        allProductsData[prdocutCounter] = productData;
        prdocutCounter++;


        $("#item_aed_price").val('');

        if (show_popup() == true) {
            addInput(productData, rand);
            console.log(productData, rand);
            addrow2(productData, rand);
            alterNetTotal(rand);
            //calculateProfitLoss();
        } else {

            return false;
        }


        //calculateProfitLoss();
    }
}

function changedharamtoOmr(obj){
		objid = $(obj).attr('id');
		cval = $(obj).val();
		//alert(objid);
		if(objid == 'purchase_price_dharam'){
			dd = $("#dharamval").val();
            alert(dd);
			if(dd !=""){
				
				result = cval/dd
				res = parseFloat(result);
				nval = res.toFixed(3);
				$("#purchase_price").val(nval);	
			}
		}
		else if(objid == 'item_aed_price'){
				dd = $("#dharamval").val();		
			if(dd !=""){
				
				result = cval/dd
				res = parseFloat(result);
				nval = res.toFixed(3);
				$("#item_total_price").val(nval);	
			}
		}
		else if(objid == 'item_total_price'){
				dd = $("#dharamval").val();		
			if(dd !=""){
				
				result = cval/dd
				res = parseFloat(result);
				nval = res.toFixed(3);
				$("#item_aed_price").val(nval);	
			}
		}
		else if(objid == 'purchase_price_dharam'){
				result = cval*dd
				res = parseFloat(result);
				nval = res.toFixed(3);
				$("#purchase_price").val(nval);	

		}
}
// MODAL WINDOWS

function show_dialog(id,text) {

        var trigger = id;

        modId = trigger.split(' ')[0];

        tabId = trigger.split(' ')[1];

        target = $(modId);

        $(target).addClass('show').parents('.modal-ov').addClass('show');

        $.fn.modNav = function () {

            var items = $(this).children('.mod-act').find('.mod-nav');

            $(items).children('li').click(function () {

                var navTarget = $(this).data('nav');

                $(this).addClass('sel').siblings('li').removeClass('sel');

                if ($(this).has('.unread-ind')) {

                    $(this).children('.unread-ind').fadeOut(200);

                }

                ;

                $(this).parents('.mod-act').siblings('.mod-body').children(navTarget).addClass('show').siblings('.show').removeClass('show');

            });

            if (trigger.split(' ').length > 1) {

                var navBtn = $(items).children('li[data-nav="' + tabId + '"]');

                $(navBtn).addClass('sel').siblings('li').removeClass('sel');

                $(navBtn).parents('.mod-act').siblings('.mod-body').children(tabId).addClass('show').siblings('.show').removeClass('show');

            }

            ;

            

            

        };

        if ($(target).has('.mod-nav')) {

            $(target).modNav()

        };

    $(id+" .mod-body").html(text);    

    $('#wrapper > .modal-ov, .modal .close, .modal .close *').click(function (e) {

        if ($(e.target).is('.modal.show, .modal.show *:not(.close):not(.close *)'))

            return;

        $('.modal').removeClass('show').parents('.modal-ov').removeClass('show');

    });



}

function show_dialog2(id,url,store) {

    //alert(store);

    if(store && store=="0"){

        

        alert('يجب اختيار مخزن لاكمال الاجراء');

        return false;

    }    

        var trigger = id;

        modId = trigger.split(' ')[0];

        tabId = trigger.split(' ')[1];

        target = $(modId);

        $(target).addClass('show').parents('.modal-ov').addClass('show');

        $.fn.modNav = function () {

            var items = $(this).children('.mod-act').find('.mod-nav');

            $(items).children('li').click(function () {

                var navTarget = $(this).data('nav');

                $(this).addClass('sel').siblings('li').removeClass('sel');

                if ($(this).has('.unread-ind')) {

                    $(this).children('.unread-ind').fadeOut(200);

                }

                ;

                $(this).parents('.mod-act').siblings('.mod-body').children(navTarget).addClass('show').siblings('.show').removeClass('show');

                //$(this).parents('.mod-act').siblings('.mod-body').children(navTarget).addClass('show2');

            });

            if (trigger.split(' ').length > 1) {

                var navBtn = $(items).children('li[data-nav="' + tabId + '"]');

                $(navBtn).addClass('sel').siblings('li').removeClass('sel');

                $(navBtn).parents('.mod-act').siblings('.mod-body').children(tabId).addClass('show').siblings('.show').removeClass('show');

                //$(navBtn).parents('.mod-act').siblings('.mod-body').children(tabId).addClass('show2');

            }

            ;

            

            

        };

        if ($(target).has('.mod-nav')) {

            $(target).modNav()

        };

    

    

    //$("#storevalue").val(store);

    $(id+" .mod-body").load(url);

    //$(id).css('bottom','25% !important');

    $('#wrapper > .modal-ov, .modal .close, .modal .close *').click(function (e) {

        if ($(e.target).is('.modal.show, .modal.show *:not(.close):not(.close *)'))

            return;

        $('.modal').removeClass('show').parents('.modal-ov').removeClass('show');

    });



}

$.fn.modal = function () {

    $(this).click(function () {

        var trigger = $(this).data('modal');

        modId = trigger.split(' ')[0];

        tabId = trigger.split(' ')[1];

        target = $(modId);

        $(target).addClass('show').parents('.modal-ov').addClass('show');

        $.fn.modNav = function () {

            var items = $(this).children('.mod-act').find('.mod-nav');

            $(items).children('li').click(function () {

                var navTarget = $(this).data('nav');

                $(this).addClass('sel').siblings('li').removeClass('sel');

                if ($(this).has('.unread-ind')) {

                    $(this).children('.unread-ind').fadeOut(200);

                }

                ;

                $(this).parents('.mod-act').siblings('.mod-body').children(navTarget).addClass('show').siblings('.show').removeClass('show');

            });

            if (trigger.split(' ').length > 1) {

                var navBtn = $(items).children('li[data-nav="' + tabId + '"]');

                $(navBtn).addClass('sel').siblings('li').removeClass('sel');

                $(navBtn).parents('.mod-act').siblings('.mod-body').children(tabId).addClass('show').siblings('.show').removeClass('show');

            }

            ;

        };

        if ($(target).has('.mod-nav')) {

            $(target).modNav()

        }

        ;

    });



    $('#wrapper > .modal-ov, .modal .close, .modal .close *').click(function (e) {

        if ($(e.target).is('.modal.show, .modal.show *:not(.close):not(.close *)'))

            return;

        $('.modal').removeClass('show').parents('.modal-ov').removeClass('show');

    });

};



// CONTENT FUNCTIONS



$.fn.tgclass = function () {

    $(this).click(function () {

        var trigger = $(this);

        triggerdata = $(trigger).data('tgcls');

        $.fn.togglefn = function () {

            var target = triggerdata.split(' ')[0];

            targetclass = triggerdata.split(' ')[1];

            $('body').find(target).toggleClass(targetclass);

        };

        if (typeof triggerdata == 'undefined') {

            var triggerdata = $(this).find('*[data-tgcls]').data('tgcls');

            if (typeof triggerdata == 'undefined') {

                return false

            } else {

                $.fn.togglefn();

            }

            ;

        } else {

            $.fn.togglefn();

        }

    })

};



$(function () {

    $('.nav-box').each(function () {

        var box = $(this);

        menu = box.children('.nav');

        body = box.children('.nav-cont');

        actbtn = box.find('.nav-act')



        $('.nav li').click(function () {

            var parent = $(this).parents('.nav');

            trigger = $(this).data('nav');

            target = $('#wrapper').find(trigger);

            if (!$(this).hasClass('no-sel') && !$(this).parent().hasClass('no-sel')) {

                parent.find('li.sel').removeClass('sel');

                $(this).addClass('sel');

            }

            ;

            $(target).addClass('show').siblings('.show').removeClass('show');

        });



        $('.nav-act .next').click(function () {

            var currSel = body.children('.nav-item.show');

            menuSel = menu.children('.sel');

            $(currSel).removeClass('show').next('.nav-item').addClass('show');

            $(menuSel).removeClass('sel').next('li').addClass('sel');

        });

        $('.nav-act .prev').click(function () {

            var currSel = body.children('.nav-item.show');

            menuSel = menu.children('.sel');

            $(currSel).removeClass('show').prev('.nav-item').addClass('show');

            $(menuSel).removeClass('sel').prev('li').addClass('sel');

        });



        if (actbtn.length > 0) {

            $.fn.wizardSteps = function () {

                steps = body.children('.nav-item');

                if ($(steps).first().hasClass('show')) {

                    actbtn.find('.prev').attr('disabled', 'disabled')

                } else {

                    actbtn.find('.prev').attr('disabled', false)

                }

                ;

                if ($(steps).last().hasClass('show')) {

                    actbtn.find('.next').attr('disabled', 'disabled')

                } else {

                    actbtn.find('.next').attr('disabled', false)

                }

                ;

            };

            $.fn.wizardSteps();

            $('.nav li, .nav-act .next, .nav-act .prev').click(function () {

                $.fn.wizardSteps();

                menu.find('li.complete').removeClass('complete');

                menu.find('li.sel').prevAll().addClass('complete');

            });

        }

    })

});



$.fn.accordion = function () {

    $(this).each(function () {

        var cont = $(this);

        contHeight = cont.innerHeight();

        sections = cont.children('.section');

        sectionHeight = sections.eq(0).outerHeight()

        sectionsHeight = sectionHeight * sections.length;



        $.fn.expand = function () {

            var height = $(this).data('height');

            $(this).addClass('expand').css('height', height);

        }

        $.fn.collapse = function () {

            $(this).removeClass('expand').removeAttr('style');

        }



        sections.each(function () {

            var content = $(this).children('.section-content');

            if (contHeight == sectionsHeight) {

                var height = $(this).children('.section-content').outerHeight() + sectionHeight;

            } else {

                var height = contHeight - sectionsHeight + sectionHeight;

            }

            $(this).attr('data-height', height);



            if (content.outerHeight() < height - sectionHeight) {

                content.css('height', height - sectionHeight);

            } else {

                content.wrap('<div class="scroll"></div>').wrap('<div class="scroll-cont"></div>');

                content.parents('.scroll').css('height', height - sectionHeight).nanoScroller();

            }

        })



        sections.eq(0).expand();



        sections.click(function () {

            $(this).expand();

            $(this).siblings('.expand').collapse();

        })

    })

};



// GLOBAL FUNCTION CALLS



$.fn.loadfns = function (specificfns) {



    // LOCALLY STORED SETTINGS



    if ($.storage() == true) {



        $('#coll-sidebar').click(function () {

            if ($('#wrapper').hasClass('mini')) {

                localStorage.removeItem('sidebar-mini');

            } else {

                localStorage.setItem('sidebar-mini', '1');

            }

            ;

        });



        $('#tb-handle').click(function () {

            if ($(this).parent('.top-bar').hasClass('hide')) {

                localStorage.removeItem('topbar-hidden');

            } else {

                localStorage.setItem('topbar-hidden', '1');

            }

            ;

        });



        var sidebarSize = localStorage.getItem('sidebar-mini');

        sidebarPosition = localStorage.getItem('sidebar-pos');

        topBarState = localStorage.getItem('topbar-hidden');

        userAvatar = localStorage.getItem('user-avatar');



        if (sidebarSize) {

            $('#wrapper').addClass('mini')

        }

        ;

        if (sidebarPosition) {

            if (sidebarPosition == 'top' || sidebarPosition == 'bottom') {

                $('#wrapper').addClass('sidebar-hz');

            }

            $('#wrapper').addClass('sidebar-' + sidebarPosition + '');

            $('#sidebar-pos .' + sidebarPosition + '').addClass('pressed').siblings('.pressed').removeClass('pressed');

        }

        ;

        $('#sidebar-pos .left').click(function () {

            localStorage.removeItem('sidebar-pos');

        });

        $('#sidebar-pos .top').click(function () {

            localStorage.setItem('sidebar-pos', 'top');

        });

        $('#sidebar-pos .right').click(function () {

            localStorage.setItem('sidebar-pos', 'right');

        });

        $('#sidebar-pos .bottom').click(function () {

            localStorage.setItem('sidebar-pos', 'bottom');

        });



        if (topBarState)

            $('.top-bar').addClass('hide');



        if (userAvatar) {

            $('#avatar img, #profile-av img').attr('src', userAvatar);

            $('#profile-av strong').text(userAvatar);

        }

        ;



        // COLOR SCHEME SETTINGS



        var colorScheme = localStorage.getItem('color-scheme');

        sidebarColor = localStorage.getItem('sidebar-color');

        sidebarLight = localStorage.getItem('sidebar-light');

        headerColor = localStorage.getItem('header-color');

        headerDark = localStorage.getItem('header-dark');



        if (colorScheme)

            $('body').addClass('dark');



        if (sidebarColor)

            $('#sidebar').css('background-color', sidebarColor);

        if (sidebarLight)

            $('#sidebar').addClass('light');



        if (headerColor)

            $('.box .header').css('background-color', headerColor);

        if (headerDark)

            $('.box:not(.mini) .header').addClass('dark');

    }

    ;



    // SAFARI VISIBILITY BUG FIX



    var browserSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);

    if (browserSafari) {

        $('#wrapper').addClass('safari-fix');

    }

    ;



    /* PAGE LOAD */



    $(window).bind("load", function () {

        $('#load').fadeOut(1000);

        // PAGE SPECIFIC FUNCTIONS TO RUN ON LOAD

        if (specificfns) {

            specificfns();

        }

    });



    // GLOBAL FUNCTION CALLS



    $.fn.inputs();

    $('.mod-tg').modal();

    $('.chat-box').chat();

    $('.ttip').tooltip();



    // RESPONSIVE FUNCTIONS



    $.fn.resFn = function () {

        var docWidth = $(document).width(),

                docHeight = $(document).height();

        if (docWidth < 726) {

            $('#sidebar-pos button').each(function () {

                $(this).removeClass('pressed').attr('disabled', 'disabled')

            });

            $('#sidebar-pos-row').hide();

        } else {

            $('#sidebar-pos-row').show();

            $('#sidebar-pos button').each(function () {

                $(this).removeAttr('disabled')

            });

        }

        ;

    };



    $.fn.checkTopBarHeight = function () {

        var tb = $('.top-bar');

        tbHeight = tb.outerHeight();

        if (tbHeight > 60 && !tb.is('.hide')) {

            tb.css('max-height', tbHeight);

        }

    };



    $.fn.resFn();

    $.fn.checkTopBarHeight();



    $(window).bind('resizeEnd', function () {

        $.fn.resFn();

        $.fn.checkTopBarHeight();

    });



    $(window).resize(function () {

        $('.top-bar').css('max-height', '');

        if (this.resizeTO)

            clearTimeout(this.resizeTO);

        this.resizeTO = setTimeout(function () {

            $(this).trigger('resizeEnd');

        }, 500);

    });



};