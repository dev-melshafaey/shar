<?php
//echo "<pre>";
//print_r($_POST);
//exit;

/* Call this file 'hello-world.php' */
require __DIR__ . '/../autoload.php';

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

//	echo "<pre>";

$itemsar = '';
$itemseng = $_POST['purchase_item_name'];
$quantity = $_POST['purchase_item_quantity'];
$itemtpcat = '';//$_POST['itemtpcat'];
if(isset($_POST['iteminv']))
$iteminv = $_POST['iteminv'];
else
    $iteminv = array();

if(isset($_POST['itemprev'])){
    $itemprev     = $_POST['itemprev'];
}
else{
    $itemprev = array();
}

//iteminv


$invid = $_POST['invocie_id'];

//$additionalItems = $_POST['additionalItems'];
if(isset($_POST['additionalItems'])){
    $additionalItems = $_POST['additionalItems'];
}
else{
    $additionalItems = array();
}
//$itemids = $_POST['itemid'];
header ('Content-Type: text/html; charset=UTF-8');
//exit;
$connector = new NetworkPrintConnector("192.168.1.10", 9100);
$printer = new Printer($connector);
//$printer->close();
//echo realpath("resources/escpos-php.png");

        try {
            $totalq = 0;
            foreach ($itemseng as $i => $itmData) {
                $itemname = $itemseng[$i];
                $itemq = $quantity[$i];
                $totalq = $totalq + $itemq;
                //$itemtpcat[$i];

                    $tmid = $itemids[$i];
                    //in_array($itemid,$itemprev);
                    // echo in_array($itemid,$itemprev);
                    //      exit;
                    if(!in_array($tmid,$itemprev)){
                        $iss = 1;
                        $item_data = new item($itemname, $itemq, $itemtp);
                        $items [] = $item_data;

                        //if()
                        if (isset($additionalItems[$tmid])) {
                            $additemsname = $additionalItems[$tmid]['item_name'];
                            $additemsval = $additionalItems[$tmid]['item_val'];
                            $additemsprice = $additionalItems[$tmid]['item_price'];
                            // $additemsval = $additionalItems[$tmid]['item_val'];

                            foreach ($additems as $adit) {

                                $item_data = new item($additemsname, $additemsval, '');
                            }
                        }
                    }


            }
            /*$items = array(


                new item("Another thing", "3.50"),
                new item("Something else", "1.00"),
                new item("A final item", "4.45"),
            );*/
            //$subtotal = new item('User', $_POST['usname']);
            $tax = new item('Customer', $_POST['customer_name']);
            $printer->feed();
            $total = new item('Total', $totalq, '');
            /* Date is kept the same for testing */
            $date = date('l jS \of F Y h:i:s A');
            //$date = "Monday 6th of April 2015 02:56:25 PM";

            /* Start the printer */
            // $logo = EscposImage::load(realpath("resources/escpos-php.png"), false);
            $printer = new Printer($connector);

            /* Print top logo */
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            // $printer -> graphics($logo);

            /* Name of shop */
            $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer->text("Shawarma Factory.\n");
            $printer->selectPrintMode();
            //$printer -> text("Shop No. 42.\n");
            $printer->feed();

            /* Barcodes - see barcode.php for more detail */
            $printer->setBarcodeHeight(80);
            $printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_BELOW);

            if($invid !=""){
                $printer->barcode($invid);

            }
            else{
                $printer->barcode($orderid);
            }

            $printer->feed();

            /* Title of receipt */
            $printer->setEmphasis(true);

            if($invid !=""){
                $printer->text("Order No " .$invid. " \n");
            }
            else{
                $printer->text("Order No " .$invid. " \n");
            }

            $printer->setEmphasis(false);


            /* Items */
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->setEmphasis(true);
            $printer->text(new item('Name', 'Qty', 'Size'));
            $printer->setEmphasis(false);
            foreach ($items as $item) {
                $printer->text($item);
            }
            $printer->setEmphasis(true);
            $printer->text($subtotal);
            $printer->setEmphasis(false);
            $printer->feed();

            /* Tax and total */
            $printer->text($tax);
            $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer->text($total);
            $printer->selectPrintMode();

            /* Footer */
            $printer->feed(2);
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("Thank you for Dianing at Showrma\n");
            //$printer -> text("For trading hours, please visit example.com\n");
            $printer->feed(2);
            $printer->text($date . "\n");

            /* Cut the receipt and open the cash drawer */
            // if($iss == 1){
            $printer->cut();
            $printer->pulse();

            //}

            $printer->close();

        } catch (Exception $e) {
            //$printer->close();
            echo $e->getMessage();
        }


/* A wrapper to do organise item names & prices into columns */
class item
{
    private $name;
    private $price;
	private $size;
    private $dollarSign;

    public function __construct($name = '', $price = '',$size='',$dollarSign = false)
    {
        $this -> name = $name;
        $this -> price = $price;
		$this -> size = $size;
        $this -> dollarSign = $dollarSign;
    }

    public function __toString()
    {
        $rightCols = 20;
        $leftCols = 28;
        if ($this -> dollarSign) {
            $leftCols = $leftCols / 3 - $rightCols / 3;
        }
        $left = str_pad($this -> name, $leftCols) ;

        $sign = ($this -> dollarSign ? 'OMR ' : '');
        $right = str_pad($this -> price.' '.$this ->size, $rightCols, ' ', STR_PAD_LEFT);
		//str_pad
        return "$left$right\n";
    }
}