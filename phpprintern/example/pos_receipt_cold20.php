<?php
/* Call this file 'hello-world.php' */
require __DIR__ . '/../autoload.php';

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;



$itemsar = $_POST['itemtar'];
$itemseng = $_POST['itemteng'];
$itemtype = $_POST['itemtype'];
$quantity = $_POST['quantity'];
$itemtpcat = $_POST['itemtpcat'];
$itemids = $_POST['itemid'];
$itemrand = $_POST['itemrand'];
$additionalItems = $_POST['additionalItems'];

if(isset($_POST['iteminv']))
    $iteminv = $_POST['iteminv'];
else
    $iteminv = array();

if(isset($_POST['itemprev'])){
    $itemprev     = $_POST['itemprev'];
}
else{
    $itemprev = array();
}

//iteminv

$orderid = $_POST['order_id'];

$invid = $_POST['inv_id'];

//$additionalItems = $_POST['additionalItems'];
if(isset($_POST['additionalItems'])){
    $additionalItems = $_POST['additionalItems'];
}
else{
    $additionalItems = array();
}

if(isset($_POST['moreItems'])){
    $moreItems = $_POST['moreItems'];
}
else{
    $moreItems = array();
}

$itemids = $_POST['itemid'];


/* Fill in your own connector here */
$connector = new NetworkPrintConnector("192.168.1.10", 9100);
//print_r($connector);
/* Information for the receipt */

$is_cold = in_array('cold',$itemtpcat);
$iss = 0;
if(!empty($itemids)){
    foreach($itemids as $indt=>$titem){
        if(!in_array($titem,$itemprev)) {
            //$i
            if($itemtpcat[$indt] == 'hot'){
                $iss = 1;
            }

        }
    }
}


if($iss ==1){
    if($is_cold) {
        try {
            // ... Print stuff
            if (!empty($itemsar)) {
                $totalq = 0;
                foreach ($itemseng as $i => $itmData) {
                    $itemname = $itemseng[$i] . ' ';
                    $itemq = $quantity[$i];
                    $totalq = $totalq + $itemq;
                    $itemtp = $itemtype[$i];
                    $randid = $itemrand[$i];
                    //$itemtpcat[$i];
                    if ($itemtpcat[$i] == 'hot') {

                        $tmid = $itemids[$i];
                        //in_array($itemid,$itemprev);
                        // echo in_array($itemid,$itemprev);
                        //      exit;
                        if(!in_array($tmid,$itemprev)){
                            $iss = 1;
                            $item_data = new item($itemname, $itemq, $itemtp);
                            //$item_data = new item($itemname, $itemq, number_format($itemtp, 3, '.', ''));
                            $items [] = $item_data;
                            //$tmid = $itemids[$i];
                            //if()
                            if(isset($additionalItems[$tmid])){
                                //$additemsname = $additionalItems[$tmid]['item_name'];
                                //$additemsval = $additionalItems[$tmid]['item_val'];
                                //$additemsprice = $additionalItems[$tmid]['item_price'];
                                $additemsname = $additionalItems[$tmid];
                                //$additemsval = $additionalItems[$tmid];
                                // $additemsprice = $additionalItems[$tmid];

                                // $additemsval = $additionalItems[$tmid]['item_val'];

                                foreach($additemsname as $adit){
                                    $aditmname = $adit['item_name'];
                                    $aditmval  = $adit['item_val'];
                                    $item_data = new item($aditmname,$aditmval,'');
                                    $items []= $item_data;

                                }
                            }


                            if(isset($moreItems[$randid])){
                                //$additemsname = $additionalItems[$tmid]['item_name'];
                                //$additemsval = $additionalItems[$tmid]['item_val'];
                                //$additemsprice = $additionalItems[$tmid]['item_price'];
                                $additemsname = $moreItems[$randid];
                                //$additemsval = $additionalItems[$tmid];
                                // $additemsprice = $additionalItems[$tmid];

                                // $additemsval = $additionalItems[$tmid]['item_val'];

                                foreach($additemsname as $adit){
                                    $aditmname = $adit['item_name'];
                                    $adn = explode('|',$aditmname);
                                    $aditmname = $adn[0];
                                    $aditmval  = $adit['item_val'];
                                    $item_data = new item($aditmname,$aditmval,'');
                                    $items []= $item_data;

                                }
                            }

                        }



                    }

                }
                //new item("Example item #1", "4.00");
            }
            /*$items = array(


                new item("Another thing", "3.50"),
                new item("Something else", "1.00"),
                new item("A final item", "4.45"),
            );*/

            //$subtotal = new item('Subtotal', '12.95');
//$tax = new item('A local tax', '1.30');
            $total = new item('Total',$totalq,number_format($totalp, 3, '.', ''));
            /* Date is kept the same for testing */
            $date = date('l jS \of F Y h:i:s A');
//$date = "Monday 6th of April 2015 02:56:25 PM";

            /* Start the printer */
//$logo = EscposImage::load("resources/escpos-php.png", false);
            $printer = new Printer($connector);

            /* Print top logo */
            $printer -> setJustification(Printer::JUSTIFY_CENTER);
//$printer -> graphics($logo);
            $printer -> selectPrintMode(Printer::MODE_FONT_A);

            /* Name of shop */
            $printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer -> text("Mr.Mishkak.\n");
            $printer -> selectPrintMode();
            $printer -> text("Tell: 94777606\n");
            $printer -> feed();

            /* Title of receipt */
            $printer -> setEmphasis(true);
//$printer -> text("SALES INVOICE\n");
            $printer -> setEmphasis(false);

            /* Items */
            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $printer -> setEmphasis(true);
            $d = date('d-m-Y');
            $tm = date('H:i:s');
            $printer -> text(new item('Date: '.$d,'', $tm));
            $printer -> setEmphasis(true);
            $printer -> text("------------------------------------------------\n");
//$printer -> text(new item('-------', '--------'));
            $printer -> setEmphasis(true);
//$printer -> setJustification(Printer::JUSTIFY_CENTER);
            $printer -> text("Order No: ".$invid."\n");
            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $printer -> setEmphasis(true);
            $printer -> text(new item('Ref: '.$_POST['customer_name'],'',''));
            $printer -> setEmphasis(true);
            $printer -> text(new item('Serverd By: '.$_POST['usname'],'',''));
            $printer -> setEmphasis(true);

            $printer -> text("------------------------------------------------\n");
//$printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer -> selectPrintMode();
            $printer -> setEmphasis(false);

            $printer -> setEmphasis(true);
            $printer -> text(new item("Name","Q","P"));
            $printer -> text("------------------------------------------------\n");
            foreach ($items as $item) {
                $printer -> text($item);
            }
            $printer -> setEmphasis(true);
//$printer -> text($subtotal);
            $printer -> setEmphasis(false);
            $printer -> feed();

            /* Tax and total */
//$printer -> text($tax);
            $printer -> text("------------------------------------------------\n");
//$printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer -> text($total);
            $printer -> selectPrintMode();

            /* Footer */
            $printer -> feed(2);
            $printer -> setJustification(Printer::JUSTIFY_CENTER);
            $printer -> text("Thank you & Come Again \n");
//$printer -> text("For trading hours, please visit example.com\n");
            $printer -> feed(2);
//$printer -> text($date . "\n");

            /* Cut the receipt and open the cash drawer */
            $printer -> cut();
            $printer -> pulse();

            $printer -> close();

        } catch (Exception $e) {
            //$printer->close();
            echo $e->getMessage();
        }
    }
}





/* A wrapper to do organise item names & prices into columns */
class item
{
    private $name;
    private $price;
    private $dollarSign;

    public function __construct($name = '',$size='',$price = '')
    {
        $this -> name = $name;
        $this -> price = $price;
        $this -> size = $size;
    }

    public function __toString()
    {
        $rightCols = 10;
        $leftCols = 28;
        if ($this -> dollarSign) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> name, $leftCols) ;
        $middle = str_pad($this ->size, $rightCols);
        $right = str_pad($this -> price, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$middle$right\n";
    }
}
